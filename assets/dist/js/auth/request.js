const req = {}
req.raw = function(obj = {}) {
    return JSON.stringify(obj)
}
req.data = function(json = "") {
    return JSON.parse(json)
}

req.money = function(angka, prefix){
    var number_string = angka.replace(/[^,\d]/g, '').toString(),
    split   		= number_string.split(','),
    sisa     		= split[0].length % 3,
    rupiah     		= split[0].substr(0, sisa),
    ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if(ribuan){
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
    }

    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    return prefix == undefined ? rupiah : (rupiah ? 'Rp ' + rupiah : '');
}

req.date = function(date="") {
    var dateSplit = date.split("-");
    var month = dateSplit[1];
    var bulan = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", 
    "Agustus", "September", "Oktober", "November", "Desember"];
    month = bulan[Number(month) - 1];
    dateSplit[1] = month;
    dateSplit = dateSplit.join(" ");
    dateSplit = dateSplit.split(" ");
    return dateSplit[2] + " " + dateSplit[1] + " " + dateSplit[0] + " " + dateSplit[3];
}

req.alertWarning = function(
    title,
    text,
    positiveText,
    negativeText,
    callbackPositive,
    callbackNegative,
) {
    swal({
        title: title,
        text: text,
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: 'btn-primary',
        confirmButtonText: positiveText,
        cancelButtonText: negativeText,
        closeOnConfirm: true,
        closeOnCancel: true
      },
      function(isConfirm){
        if (isConfirm){
            callbackPositive()
        } else {
            callbackNegative()
        }
      });
}

function checkIsAlreadySetupLocation() {
    $.ajax({
        url: base_url.value + "/dashboard/m/settings/load_general",
        data: null,
        type: "GET",
        contentType: false,
        processData: false,
        success: function(response) {
            response = req.data(response)
            if (response.code == 200) {
                var data = response.data;
                if (data.location == null) {
                    swal({
                        title: "Mohon maaf",
                        text: "Anda belum memasang lokasi dari toko anda, pelanggan sulit mencarinya.",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonClass: 'btn-danger',
                        confirmButtonText: 'Isi Lokasi Toko',
                        closeOnConfirm: false,
                        //closeOnCancel: false
                    },
                    function(){
                        location.assign(base_url.value + "/dashboard/m/settings");
                    });
                }
            }
        }
    });
}