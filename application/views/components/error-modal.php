<!-- Modal -->
<div class="modal fade" 
    id="errorModal" 
    tabindex="-1" 
    role="dialog" 
    aria-labelledby="exampleModalLongTitle1" 
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle1">
                    <?php echo(isset($errorModalTitle) ? $errorModalTitle : "Modal Title"); ?>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>
                    <?php echo(isset($errorModalContent) ? $errorModalContent : "Cras mattis consectetur purus sit amet fermentum."); ?>
                </p>
                <?php
                    if (isset($errorModalDetail)) {
                        ?>
                        <div class="row">
                            <div class="col-sm-12">
                                <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#errorModalDetail" aria-expanded="false" aria-controls="collapseErrorModalDetail">
                                    Lihat Detail
                                </button>
                                <div class="collapse" id="errorModalDetail">

                                    <ul class="list-group list-unstyled">
                                    <?php
                                        foreach ($errorModalDetail as $item) {
                                            ?>  
                                            <li class="p-2 zoom">
                                                <div class="media d-flex w-100">
                                                    <div class="media-body align-self-center pl-2">
                                                        <p class="mb-0 font-w-500 tx-s-12"><?= $item ?></p>
                                                    </div>
                                                </div>
                                            </li>
                                            <?php
                                        }
                                    ?>
                                    </ul> 
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary"
                <?php
                    if (isset($errorModalAction)) {
                        ?>
                        onclick="location.assign('<?= $errorModalAction ?>')"
                        <?php
                    }
                ?>
                data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>
    $('#errorModal').modal('show')
</script>