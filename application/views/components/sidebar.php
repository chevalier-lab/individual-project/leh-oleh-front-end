<!-- START: Main Menu-->
<div class="sidebar">
    <div class="site-width">
        <!-- START: Menu-->
        <ul id="side-menu" class="sidebar-menu">
            <?php
                if (isset($sidebar)) {
                    foreach ($sidebar as $parent) {
                        ?>
                        <li class="dropdown">
                            <a href="javascript:void(0);"><i class="icon-<?= $parent['icon']; ?> mr-1"></i> <?= $parent['label']; ?></a>                  
                            <ul>
                                <?php
                                    foreach ($parent["menus"] as $item) {
                                        ?>
                                        <li
                                            <?php 
                                                if ($item["is_active"]) echo 'class="active"';
                                            ?>
                                        ><a href="<?= $item['link']; ?>"><i class="icon-<?= $item['icon']; ?>"></i> <?= $item['label']; ?></a></li>
                                        <?php
                                    }
                                ?>
                            </ul>
                        </li>
                        <?php
                    }
                }
            ?>
        </ul>
        <!-- END: Menu-->
    </div>
</div>
<!-- END: Main Menu-->