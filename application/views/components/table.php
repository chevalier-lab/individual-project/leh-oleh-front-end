<div class="table-responsive">
    <table id="<?= $idTable ?>" class="display table table-bordered table-hover" style="width:100%">
        <thead>
            <?php
                if (isset($isCustomThead))
                    echo $thead;
                else {
                    ?>
                    <tr>
                        <?php foreach ($thead as $th) : ?>
                            <th scope="col"><?= $th ?></th>
                        <?php endforeach ?>
                    </tr>
                    <?php
                }
            ?>
        </thead>
        <tbody>
            <?= isset($tbody) ? $tbody : "" ?>
        </tbody>
    </table>
</div>