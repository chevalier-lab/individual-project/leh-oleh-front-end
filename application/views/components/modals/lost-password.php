<!-- Modal -->
<div class="modal fade" 
    id="modal-lost-password" 
    tabindex="-1" 
    role="dialog" 
    aria-labelledby="modalLostPassword" 
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <?php
                $statusModal = false;
                if (isset($isSuccess)) {
                    $statusModal = $isSuccess;
                }
                if (!$statusModal) {
                    ?>
                    <form method="POST" action="">
                        <div class="modal-body">
                            <center>
                                <p>
                                    <img src="<?= base_url('assets/dist/images/icons/031-email.png'); ?>"
                                    style="max-width: 100px;" />
                                </p>
                                    <div class="form-group">
                                        <input type="email" id="email-lost-password" placeholder="E-Mail" class="form-control"
                                            style="font-size: 20px !important;">
                                    </div>
                                <p>
                                <?php
                                    if (isset($titleModal)) echo $titleModal;
                                    else echo "Lupa password? masukkan email anda untuk mendapatkan password anda kembali";
                                ?>
                                </p>
                            </center>
                        </div>
                    </form>
                    <?php
                } else {
                    ?>
                    <div class="modal-body">
                        <center>
                            <p>
                                <img src="<?= base_url('assets/dist/images/icons/055-laptop.png'); ?>"
                                style="max-width: 100px;" />
                            </p>
                            <p>
                                Kami sudah mengirimkan link verifikasi ke email anda.
                            </p>
                        </center>
                    </div>
                    <?php
                }
            ?>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <?php
                if (!$statusModal) {
                    ?>
                    <button type="button" class="btn btn-primary">Kirim</button>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
</div>

<script>
    $('#modal-lost-password').modal('<?= isset($hideModal) ? $hideModal: 'show'; ?>')
</script>