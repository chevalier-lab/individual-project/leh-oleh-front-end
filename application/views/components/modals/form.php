<div class="modal fade" id="<?= isset($idElement) ? $idElement : 'example' ?>" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered <?= isset($modalType) ? $modalType : 'modal-md' ?>">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                    <i class="<?= isset($iconTitle) ? $iconTitle : '' ?>"></i> <?= isset($modalTitle) ? $modalTitle : '#' ?>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="icon-close"></i>
                </button>
            </div>
            <form method="post" action="<?= isset($modalActionForm) ? $modalActionForm : '#' ?>">
                <div class="modal-body">
                    <?= isset($modalContentForm) ? $modalContentForm : "" ?>
                </div>
                <div class="modal-footer">
                    <?= isset($modalButtonForm) ? $modalButtonForm : "" ?>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $('#<?= isset($idElement) ? $idElement : 'example' ?>').modal('<?= isset($hideModal) ? $hideModal : 'show'; ?>')
</script>