<!-- Modal -->
<div class="modal fade" 
    id="modal-categories" 
    tabindex="-1" 
    role="dialog" 
    aria-labelledby="modalCategories" 
    aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle1">Pilih Kategori</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <!-- START: Card Data-->
                <div class="row row-eq-height">
                    <div class="col-12 col-lg-4 todo-menu-bar flip-menu pr-lg-0">
                        <div class="card border h-100 todo-menu-section">

                            <ul class="nav flex-column nav-pills m-2"
                                id="categories-lists"
                                aria-orientation="vertical">
                            </ul>         

                        </div>  
                    </div>
                    <div class="col-12 col-lg-8 pl-lg-0">
                        <div class="card border h-100 todo-list-section">
                            <div class="card-body p-2" id="categories-lists-detail"></div>
                        </div>
                    </div>
                </div>
                <!-- END: Card DATA-->

            </div>
        </div>
    </div>
</div>

<script>

    var categories = [];

    (function ($) {
        "use strict";
        $(window).on("load", function () {
            // Load Bank First Time
            loadCategories()
        });
    })(jQuery);

    function loadCategories() {
        $.ajax({
            url: base_url.value + "/anonymous/landing/load_categories",
            data: null,
            type: "GET",
            contentType: false,
            processData: false,
            success: function(response) {
                response = req.data(response)
                if (response.code == 200) {
                    var data = response.data
                    categories = data;
                    $("#categories-lists").html(`${data.map(function(item, position) {
                        return `
                            <li class="nav-item">
                                <a class="nav-link categories-lists-item" href="javascript:void(0);"
                                    onclick="setCategories(${position})">
                                    <span class="${item.icon}"></span>
                                    ${item.category}
                                </a>
                            </li>
                        `;
                    }).join('')}`);

                    setCategories(0)
                }
            }
        });
    }

    function setCategories(position) {
        var selectedCategory = categories[position];
        $('.categories-lists-item').each(function(i) {
            if (position == i) $(this).addClass("active")
            else $(this).removeClass("active")
        });

        $("#categories-lists-detail").html(`
            <h3>${selectedCategory.category}</h3>
            <p>${selectedCategory.description}</p>
            <p>
                <button class="btn btn-primary"
                    onclick="location.assign('${base_url.value}/anonymous/landing/set_category_product_list/${selectedCategory.id}')">
                    Lihat Produk</button>
            </p>
        `);
    }
</script>