<!-- Modal -->
<div class="modal fade" 
    id="modal-otp" 
    tabindex="-1" 
    role="dialog" 
    aria-labelledby="modalOTP" 
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <?php
                $statusModal = false;
                if (isset($isSuccess)) {
                    $statusModal = $isSuccess;
                }
                if (!$statusModal) {
                    ?>
                    <form method="POST" action="">
                        <div class="modal-body">
                            <center>
                                <p>
                                    <img src="<?= base_url('assets/dist/images/icons/034-mobile.png'); ?>"
                                    style="max-width: 100px;" />
                                </p>
                                    <div class="form-group">
                                        <input type="number" id="otp" placeholder="Six Number OTP" class="form-control"
                                            style="font-size: 20px !important;">
                                    </div>
                                <p>
                                <?php
                                    if (isset($titleModal)) echo $titleModal;
                                    else echo "One Time Password, masukkan Kode OTP yang dikirimkan ke nomor anda.";
                                ?>
                                </p>
                            </center>
                        </div>
                    </form>
                    <?php
                } else {
                    ?>
                    <div class="modal-body">
                        <center>
                            <p>
                                <img src="<?= base_url('assets/dist/images/icons/012-credit-card.png'); ?>"
                                style="max-width: 100px;" />
                            </p>
                            <p>
                                Verifikasi berhasil
                            </p>
                        </center>
                    </div>
                    <?php
                }
            ?>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <?php
                if (!$statusModal) {
                    ?>
                    <button type="button" class="btn btn-primary">Kirim</button>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
</div>

<script>
    $('#modal-otp').modal('<?= isset($hideModal) ? $hideModal: 'show'; ?>')
</script>