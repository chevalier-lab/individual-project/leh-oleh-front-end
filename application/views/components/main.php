<!-- START: Main Content-->
<main>
    <div class="container-fluid site-width">
        <!-- START: Breadcrumbs-->
        <?php $this->load->view('components/heading'); ?>
        <hr />
        <!-- END: Breadcrumbs-->
        <!-- START: Load Page -->
        <?php 
            if (isset($page))
                $this->load->view($page); 
        ?>
        <!-- START: Load Page -->
    </div>
</main>
<!-- END: Main Content-->