<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?= $meta['title']; ?></title>
<link rel="icon" href="<?= base_url('assets/dist/images/fav-icon.png'); ?>" type="image/png" sizes="16x16"> 
<meta name="description" content="<?= $meta['description']; ?>">
<meta name=”robots” content="<?= $meta['robots'] ?>">

<style>
    .fixed {
        position: fixed;
        top:0; left:0;
        width: 100%; 
        -webkit-transition: all 0.5s ease;
        -moz-transition: all 0.5s ease;
        -o-transition: all 0.5s ease;
        transition: all 0.5s ease;
    }
</style>