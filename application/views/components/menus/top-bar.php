<div class="site-width">
    <nav class="navbar navbar-expand-lg  p-0">
        <div class="navbar-header  h-100 mb-0 align-self-center logo-bar text-left">
            <a href="<?= base_url('index.php'); ?>" class="horizontal-logo text-left">
                <center>
                    <img src="<?= base_url('assets/dist/images/logo.png'); ?>"
                    style="max-width: 60%; margin: 0 auto;" />
                </center>
            </a>
        </div>
        
        <div class="navbar-header h4 mb-0 text-center h-100 collapse-menu-bar">
            <a href="javascript:void(0);" class="sidebarCollapse text-danger" 
                data-toggle="modal" data-target="#modal-categories"
                id="collapse"><i class="icon-menu"></i></a>
        </div>

        <form class="float-left d-none d-lg-block search-form">
            <div class="form-group mb-0 position-relative">
                <input type="text" 
                id="search-product"
                class="form-control border-0 rounded bg-search pl-5" placeholder="Cari oleh-oleh disini...">
                <div class="btn-search position-absolute top-0">
                    <a href="javascript:void(0)"
                        onclick="doSearch()"><i class="h6 icon-magnifier text-danger"></i></a>
                </div>
                <a href="javascript:void(0);" class="position-absolute close-button mobilesearch d-lg-none" data-toggle="dropdown" aria-expanded="false"><i class="icon-close h5"></i>
                </a>
            </div>
        </form>

        <div class="navbar-right ml-auto h-100">
            <ul class="ml-auto p-0 m-0 list-unstyled d-flex top-icon h-100">

                <?php
                    if (isset($auth) && $auth != null && !empty($auth)) {
                        ?>
                        <li class="dropdown align-self-center d-inline-block">                    
                            <a 
                                href="javascript:void(0)"
                                data-toggle="dropdown" aria-expanded="false"
                                class="nav-link text-danger">Saldo: <span id="current-balance"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right border py-0">
                                <li>
                                    <a class="dropdown-item px-2 py-2 border border-top-0 border-left-0 border-right-0" 
                                    href="<?= base_url("index.php/dashboard/g/managementWallets/listsTopUp"); ?>">
                                        <div class="media">
                                            <div class="media-body">
                                                <p class="mb-0">Top Up
                                                </p>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a class="dropdown-item px-2 py-2 border border-top-0 border-left-0 border-right-0" 
                                    href="<?= base_url("index.php/dashboard/g/managementWallets/listsWithdraw"); ?>">
                                        <div class="media">
                                            <div class="media-body">
                                                <p class="mb-0">Withdraw
                                                </p>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <script>
                            (function ($) {
                                "use strict";
                                $.ajax({
                                    url: base_url.value + "/general/publicServices/current_balance",
                                    data: null,
                                    type: "GET",
                                    contentType: false,
                                    processData: false,
                                    success: function(response) {
                                        response = req.data(response)
                                        if (response.code == 200) {
                                            var data = response.data;
                                            $("#current-balance").html(req.money(data.balance.toString(), "Rp "));
                                        } else {
                                            $("#current-balance").html(req.money("0", "Rp "));
                                        }
                                    }
                                    // ,
                                    // error: function(response) {
                                    //     console.log("ERROR", response);
                                    //     $("#btn-login").removeAttr("disabled");
                                    // }
                                });
                            })(jQuery);
                        </script>
                        <?php
                    }
                ?>

                <li class="dropdown align-self-center d-inline-block">
                    <a 
                        href="
                        <?php
                            if (isset($auth) && $auth != null && !empty($auth)) 
                                echo base_url('index.php/anonymous/cart');
                            else echo base_url('index.php/general/auth/login');
                        ?>"
                        class="nav-link text-danger">
                        <span class="badge badge-default" id="cart-badge">
                        </span>
                        <i class="icon-basket h4"></i>
                    </a>

                    <?php
                        if (isset($auth) && $auth != null && !empty($auth)) {
                            ?>
                            <script>
                               (function ($) {
                                    "use strict";

                                    loadCartBadge();
                                    
                                })(jQuery); 

                                function loadCartBadge() {
                                    $.ajax({
                                        url: base_url.value + "/anonymous/cart/load_cart_active",
                                        data: null,
                                        type: "GET",
                                        contentType: false,
                                        processData: false,
                                        success: function(response) {
                                            response = req.data(response)
                                            if (response.code == 200) {
                                                var data = response.data

                                                // Render Product
                                                if (data.length > 0) {
                                                    $("#cart-badge").html(`
                                                    <span class="ring">
                                                        </span><span class="ring-point">
                                                    </span>
                                                    `);
                                                } else {
                                                    $("#cart-badge").html(``);
                                                }
                                            }
                                        }
                                    });
                                }
                            </script>
                            <?php
                        }
                    ?>
                </li>

                <li class="dropdown align-self-center d-inline-block">
                    <a href="javascript:void(0);"
                        class="nav-link text-danger" data-toggle="dropdown" aria-expanded="false"><i class="icon-bell h4"></i>
                        <span class="badge badge-default" id="notification-badge"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-right border py-0" id="notification-list">
                        <?php
                            if (isset($auth) && $auth != null && !empty($auth)) {
                                ?>
                                <script>
                                var notifications = [];
                                (function ($) {
                                    "use strict";

                                    loadNotifications();
                                    
                                })(jQuery); 

                                function loadNotifications() {
                                    $.ajax({
                                        url: base_url.value + "/services/notification/list",
                                        data: null,
                                        type: "GET",
                                        contentType: false,
                                        processData: false,
                                        success: function(response) {
                                            response = req.data(response)
                                            if (response.code == 200) {
                                                notifications = response.data

                                                // Render Product
                                                if (notifications.length > 0) {
                                                    $("#notification-badge").html(`
                                                    <span class="ring">
                                                        </span><span class="ring-point">
                                                    </span>
                                                    `);

                                                    $("#notification-list").html(`
                                                    ${notifications.map(function(item, position) {
                                                        return `
                                                        <li>
                                                            <a class="dropdown-item px-2 py-2 border border-top-0 border-left-0 border-right-0" href="javascript:void(0);"
                                                            data-toggle="modal" data-target="#modal-detail-notification"
                                                            onclick="setupDetailNotification(${position})">
                                                                <div class="media">
                                                                    <div class="media-body">
                                                                        <p class="mb-0">
                                                                        ${item.title}
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </a>
                                                        </li>
                                                        `;
                                                    }).join('')}
                                                    `);
                                                } else {
                                                    $("#notification-list").html(`
                                                    <li>
                                                        <a class="dropdown-item px-2 py-2 border border-top-0 border-left-0 border-right-0" href="javascript:void(0);">
                                                            <div class="media">
                                                                <div class="media-body">
                                                                    <p class="mb-0">Belum ada notifikasi
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </li>
                                                    `);
                                                }
                                            }
                                        }
                                    });
                                }

                                function setupDetailNotification(position) {
                                    $("#container-detail-notification").html(`${
                                        notifications[position].description
                                    }`);

                                    $.ajax({
                                        url: base_url.value + "/services/notification/update_is_read/" + notifications[position].id,
                                        data: null,
                                        type: "GET",
                                        contentType: false,
                                        processData: false,
                                        success: function(response) {
                                            loadNotifications();
                                        }
                                    });
                                }
                                </script>
                                <?php
                            } else {
                                ?>
                                <li>
                                    <a class="dropdown-item px-2 py-2 border border-top-0 border-left-0 border-right-0" href="javascript:void(0);">
                                        <div class="media">
                                            <div class="media-body">
                                                <p class="mb-0">Hi, ini bagian notifikasi anda
                                                </p>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <?php
                            }
                        ?>

                    </ul>
                </li>

                <?php
                    if (isset($auth) && $auth != null && !empty($auth)) {
                        ?>
                        <li class="dropdown user-profile align-self-center d-inline-block">
                            <a href="javascript:void(0);" class="nav-link py-0" data-toggle="dropdown" aria-expanded="false">
                                <div class="media">
                                    <img src="<?= base_url('assets/dist/images/author.jpg'); ?>" alt="" 
                                    class="d-flex img-fluid rounded-circle" width="29"
                                    id="navbar-face-profile">
                                </div>
                            </a>
        
                            <div class="dropdown-menu border dropdown-menu-right p-0">
                                <?php
                                    $profile_uri = base_url("index.php/dashboard/g/settings");
                                    if (!empty($auth)) {
                                        switch($auth->level) {
                                            case "admin":
                                                $profile_uri = base_url("index.php/dashboard/a/settings/profile");
                                            break;
                                            case "toko":
                                                $profile_uri = base_url("index.php/dashboard/m/settings/profile");
                                             break;
                                            case "guest":
                                                $profile_uri = base_url("index.php/dashboard/g/settings");
                                            break;
                                        }
                                    }
                                ?>
                                <a href="<?= $profile_uri ?>" class="dropdown-item px-2 align-self-center d-flex">
                                    <span class="icon-user mr-2 h6 mb-0"></span> Lihat Profil</a>
                                <div class="dropdown-divider"></div>
                                <a href="<?= base_url("index.php/general/auth/do_logout") ?>" class="dropdown-item px-2 text-danger align-self-center d-flex">
                                    <span class="icon-logout mr-2 h6  mb-0"></span> Sign Out</a>
                            </div>
        
                        </li>
                        <script>
                               (function ($) {
                                    "use strict";
                                    
                                    $.ajax({
                                        url: base_url.value + "/anonymous/profile/load_profile",
                                        data: null,
                                        type: "GET",
                                        contentType: false,
                                        processData: false,
                                        success: function(response) {
                                            response = req.data(response)
                                            if (response.code == 200) {
                                                var data = response.data
                                                $("#navbar-face-profile").attr("src", data.uri);
                                            }
                                        }
                                    });

                                })(jQuery); 
                        </script>
                        <?php
                    }
                ?>

            </ul>
        </div>
    </nav>
</div>

<?php
    // Detail Notification
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-detail-notification",
        "modalTitle" => "Detail Notifikasi",
        "iconTitle" => "icon-eye",
        "modalActionForm" => "#",
        "modalContentForm" => '
        <div id="container-detail-notification">
            
        </div>
        ',
        "modalButtonForm" => ''
    ));
?>

<script>
    function doSearch() {
        var search = $("#search-product").val();
        location.assign(base_url.value + "/anonymous/landing/set_search_product_list?search=" + search)
    }
</script>