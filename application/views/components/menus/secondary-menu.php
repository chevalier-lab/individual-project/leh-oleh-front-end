<!-- SECONDARY MENU -->
<div class="mt-0 bg-danger border z-index-1 p-2">
    <div class="d-sm-flex">
        <div class="align-self-center mr-auto text-center text-sm-right">
            <ul class="nav nav-pills flex-column flex-sm-row">
                <li class="nav-item ml-0">
                    <a class="nav-link py-2 px-2 px-lg-2 text-white"
                        href="<?= base_url('index.php'); ?>"> 
                        <i class="icon-home"></i>
                        Rumah
                    </a>
                </li>  
                <?php
                    if (isset($auth) && $auth != null && !empty($auth)) {
                        $dashboard = "";
                        switch($auth->level) {
                            case "admin":
                                $dashboard = base_url("index.php/dashboard/a/dashboard");
                            break;
                            case "toko":
                                $dashboard = base_url("index.php/dashboard/m/dashboard");
                             break;
                            case "guest":
                                $dashboard = base_url("index.php/dashboard/g/dashboard");
                            break;
                        }
                        ?>
                        <li class="nav-item ml-0">
                            <a class="nav-link py-2 px-2 px-lg-2 text-white"
                                href="<?= $dashboard; ?>"> 
                                <span class="icon-lock"></span> Dasbor
                            </a>
                        </li>
                        <li class="nav-item ml-0">
                            <a class="nav-link py-2 px-2 px-lg-2 text-white"
                                href="<?= base_url('index.php/anonymous/location'); ?>"> 
                                <span class="icon-location-pin"></span> Lokasi Saya
                            </a>
                        </li>
                        <?php
                    }
                    else {
                        ?>
                        <li class="nav-item ml-0">
                            <a class="nav-link py-2 px-2 px-lg-2 text-white"
                                href="<?= base_url('index.php/general/auth/registration'); ?>"> 
                                <span class="icon-lock"></span> Daftar
                            </a>
                        </li>                                                         
                        <li class="nav-item ml-0">
                            <a class="nav-link py-2 px-2 px-lg-2 text-white" 
                                href="<?= base_url('index.php/general/auth/login'); ?>"> 
                                <span class="icon-login"></span> Masuk
                            </a>
                        </li>                                                        
                        <?php
                    }
                ?>
                <li class="nav-item ml-0">
                    <a class="nav-link py-2 px-2 px-lg-2 text-white" href="<?= base_url('index.php/anonymous/order'); ?>"> 
                        <span class="icon-present"></span> Pesanan Saya
                    </a>
                </li>                                                        
                <li class="nav-item ml-0">
                    <a class="nav-link py-2 px-2 px-lg-2 text-white" href="<?= base_url('index.php/anonymous/chat'); ?>"> 
                        <span class="icon-speech"></span> Bantuan
                    </a>
                </li>                                                    
                <li class="nav-item ml-0">
                    <a class="nav-link py-2 px-2 px-lg-2 text-white" href="<?= base_url('index.php/anonymous/faq'); ?>"> 
                        <span class="icon-info"></span> F.A.Q
                    </a>
                </li>
            </ul>
        </div>

        <div class="align-self-center">
            <ul class="nav nav-pills flex-column flex-sm-row" id="secondary-menu">
            </ul>
        </div>
    </div>
</div>

<script>
    var secondaryMenu = [];

    (function($) {
        "use strict";
        $(window).on("load", function() {
            // Load Main Menu
            loadSecondaryMenu()
        });
    })(jQuery);

    function loadSecondaryMenu() {
        $.ajax({
            url: base_url.value + "/anonymous/landing/load_secondary_menu",
            data: null,
            type: "GET",
            contentType: false,
            processData: false,
            success: function(response) {
                response = req.data(response)
                if (response.code == 200) {
                    var data = response.data
                    data.forEach(function(item) {
                        secondaryMenu.push({
                            id: Number(item.id),
                            menu: item.menu,
                            url: item.url
                        });
                    });
                    setupSecondaryMenu();
                    // for (let j = 0; j < data.length; j++) {
                    //     $(`<div class="carousel-item"><img class="d-block w-100" src="${data[j].uri}"></div>`).appendTo('.carousel-inner');
                    //     $('<li data-target="#carouselExampleCaption" data-slide-to="' + j + '"></li>').appendTo('.carousel-indicators')
                    // }

                    // $('.carousel-item').first().addClass('active');
                    // $('.carousel-indicators > li').first().addClass('active');
                    // $('#carouselExampleCaption').carousel();
                }
            }
        });
    }

    function setupSecondaryMenu() {
        if (secondaryMenu.length > 0) {
            $("#secondary-menu").html(`${secondaryMenu.map(function(item) {
                return `
                <li class="nav-item">
                    <a class="nav-link text-white" 
                        href="${item.url}">
                        ${item.menu}
                    </a>
                </li>
                `;
            }).join('')}`);
        }
    }
</script>

<script>
    $(window).scroll(function(){
        var sticky = $('#header-fix'),
            scroll = $(window).scrollTop();

        if (scroll >= 1) sticky.addClass('fixed');
        else sticky.removeClass('fixed');
    });
</script>