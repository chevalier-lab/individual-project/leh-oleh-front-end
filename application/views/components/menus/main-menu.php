<ul class="nav nav-pills mb-4" id="main-menu">
</ul>

<script>

    var mainMenu = [];

    (function($) {
        "use strict";
        $(window).on("load", function() {
            // Load Main Menu
            // mainMenu.push({
            //     id: 0,
            //     menu: "Home",
            //     url: base_url.value
            // })
            loadMainMenu()
        });
    })(jQuery);

    function loadMainMenu() {
        $.ajax({
            url: base_url.value + "/anonymous/landing/load_main_menu",
            data: null,
            type: "GET",
            contentType: false,
            processData: false,
            success: function(response) {
                response = req.data(response)
                if (response.code == 200) {
                    var data = response.data
                    data.forEach(function(item) {
                        mainMenu.push({
                            id: Number(item.id),
                            menu: item.menu,
                            url: item.url
                        });
                    });
                    setupMainMenu();
                    // for (let j = 0; j < data.length; j++) {
                    //     $(`<div class="carousel-item"><img class="d-block w-100" src="${data[j].uri}"></div>`).appendTo('.carousel-inner');
                    //     $('<li data-target="#carouselExampleCaption" data-slide-to="' + j + '"></li>').appendTo('.carousel-indicators')
                    // }

                    // $('.carousel-item').first().addClass('active');
                    // $('.carousel-indicators > li').first().addClass('active');
                    // $('#carouselExampleCaption').carousel();
                }
            }
        });
    }

    function setupMainMenu() {
        if (mainMenu.length > 0) {
            $("#main-menu").html(`
            ${mainMenu.map(function(item) {
                return `
                <li class="nav-item mr-1">
                    <a class="nav-link ${(item.id == Number(_main_menu.value)) ? 'bg-danger text-white rounded-btn': 'btn btn-outline-secondary rounded-btn'}" 
                        href="${item.url}?main_menu=${item.id}">
                        ${item.menu}
                    </a>
                </li>
                `;
            }).join('')}
            `);
        }
    }

</script>