<!-- START: Header-->
<div id="header-fix" class="header fixed-top">
    <div class="site-width">
        <nav class="navbar navbar-expand-lg  p-0">
            <div class="navbar-header  h-100 h4 mb-0 align-self-center logo-bar text-left">  
                <a href="<?= base_url('index.php'); ?>" class="horizontal-logo text-left">
                    <center>
                        <img src="<?= base_url('assets/dist/images/logo.png'); ?>"
                        style="max-width: 60%; margin: 0 auto;" />
                    </center>
                </a>               
            </div>
            <div class="navbar-header h4 mb-0 text-center h-100 collapse-menu-bar">
                <a href="javascript:void(0);" class="sidebarCollapse" id="collapse"><i class="icon-menu"></i></a>
            </div>
            <div class="navbar-right ml-auto h-100">

                <ul class="ml-auto p-0 m-0 list-unstyled d-flex top-icon h-100">                    
                <?php
                    // Using Navbar Model
                    if (isset($navbar)) {
                        // Setup Navbar For Notification
                        if (isset($navbar["notification"])) {
                            $navNotification = $navbar["notification"];
                            ?>
                            <li class="dropdown align-self-center d-inline-block">
                                <a href="javascript:void(0);" class="nav-link" data-toggle="dropdown" aria-expanded="false">
                                    <i class="<?= $navNotification['icon']; ?> h4"></i>
                                    <?php
                                        // Display Ring Notification
                                        if (count($navNotification['items']) > 0) {
                                            ?>
                                            <span class="badge badge-default"> 
                                                <span class="ring"></span>
                                                <span class="ring-point"></span>
                                            </span>
                                            <?php
                                        }
                                    ?>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right border   py-0">
                                    <?php
                                        if (count($navNotification['items']) > 0) {
                                            foreach($navNotification['items'] as $item) {
                                                ?>
                                                <li>
                                                    <a class="dropdown-item px-2 py-2 border border-top-0 border-left-0 border-right-0" href="javascript:void(0);">
                                                        <div class="media">
                                                            <img src="dist/images/author.jpg" alt="" class="d-flex mr-3 img-fluid rounded-circle w-50">
                                                            <div class="media-body">
                                                                <p class="mb-0 text-success">john send a message</p>
                                                                12 min ago
                                                            </div>
                                                        </div>
                                                    </a>
                                                </li>
                                                <?php
                                            }
                                        } else {
                                            ?>
                                            <li>
                                                <a class="dropdown-item px-2 py-2 border border-top-0 border-left-0 border-right-0" href="javascript:void(0);">
                                                    <div class="media">
                                                        <div class="media-body">
                                                            Tidak ada notifikasi
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                            <?php
                                        }
                                    ?>
                                    <li><a class="dropdown-item text-center py-2" href="javascript:void(0);"> Baca Semua Notification <i class="icon-arrow-right pl-2 small"></i></a></li>
                                </ul>
                            </li>
                            <?php
                        }

                        // Setup Navbar For User
                        if (isset($navbar["user"])) {
                            $navUser = $navbar["user"];
                            ?>
                            <li class="dropdown user-profile align-self-center d-inline-block">
                                <a href="javascript:void(0);" class="nav-link py-0" data-toggle="dropdown" aria-expanded="false"> 
                                    <div class="media">                                   
                                        <img src="<?= $navUser["face"]; ?>" alt="" 
                                        id="navbar-face"
                                        class="d-flex img-fluid rounded-circle" width="29">
                                    </div>
                                </a>

                                <div class="dropdown-menu border dropdown-menu-right p-0">
                                <?php
                                    foreach($navUser["items"] as $item) {
                                        ?>
                                        <a href="<?= $item['link']; ?>" 
                                            class="dropdown-item px-2 align-self-center d-flex">
                                            <span class="<?= $item['icon']; ?> mr-2 h6 mb-0"></span> 
                                            <?= $item['label']; ?>
                                        </a>
                                        <?php
                                    }
                                ?>
                                </div>

                            </li>
                            <?php
                        }
                    }
                ?>
                </ul>
            </div>
        </nav>
    </div>
</div>

<script>
    $.ajax({
        url: base_url.value + "/dashboard/a/settings/load_profile",
        data: null,
        type: "GET",
        contentType: false,
        processData: false,
        success: function(response) {
            response = req.data(response)
            if (response.code == 200) {
                var data = response.data
                $("#navbar-face").attr("src", data.uri);
            }
        }
        // ,
        // error: function(response) {
        //     console.log("ERROR", response);
        //     $("#btn-login").removeAttr("disabled");
        // }
    });
</script>
<!-- END: Header-->