<!-- START: Main Menu-->
<div class="sidebar">
    <div class="site-width">
        <!-- START: Menu-->
        <ul id="side-menu" class="sidebar-menu">
            <?php
                if (isset($sidebar)) {
                    foreach ($sidebar as $sideItem) {
                        ?>
                        <li class="dropdown active">
                            <a href="javascript:void(0);">
                                <i class="<?= $sideItem['icon']; ?> mr-1"></i> 
                                <?= $sideItem['label']; ?>
                            </a>                  
                            <ul>
                                <!-- <li class="active"><a href="index.html"><i class="icon-rocket"></i> Dashboard</a></li> -->
                                <?php
                                    foreach ($sideItem['items'] as $item) {
                                        ?>
                                        <li <?= isset($item["is_active"]) ? ($item["is_active"]) ? 'class="active"' : "" : ""?>>
                                            <a href="<?= $item['link']; ?>">
                                                <i class="<?= $item['icon']; ?>"></i> 
                                                <?= $item['label']; ?>
                                            </a>
                                        </li>
                                        <?php
                                    }
                                ?>
                            </ul>
                        </li>
                        <?php
                    }
                }
            ?>
        </ul>
        <!-- END: Menu-->
        <ol class="breadcrumb bg-transparent align-self-center m-0 p-0 ml-auto">
            <li class="breadcrumb-item"><a href="javascript:void(0);">Application</a></li>
            <li class="breadcrumb-item active">Dashboard</li>
        </ol>
    </div>
</div>
<!-- END: Main Menu-->