<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view('components/head'); ?>

        <!-- START: Template CSS-->
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/bootstrap/css/bootstrap.min.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.theme.min.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/simple-line-icons/css/simple-line-icons.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/flags-icon/css/flag-icon.min.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/select2/css/select2.min.css') ?>" />
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/select2/css/select2-bootstrap.min.css') ?>" />
        <!-- END Template CSS-->     

        <!-- START: Page CSS-->   
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/social-button/bootstrap-social.css'); ?>"/>   
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/sweetalert/sweetalert.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/fancybox/jquery.fancybox.min.css'); ?>">  
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/toastr/toastr.min.css'); ?>"/>
        <!-- END: Page CSS-->

        <!-- START: Custom CSS-->
        <link rel="stylesheet" href="<?= base_url('assets/dist/css/main.css'); ?>">
        <!-- END: Custom CSS-->
    </head>
    <body id="main-container" class="default bg-white"
    oncontextmenu="return false;">

        <input type="hidden" name="base_url" 
            id="base_url" value="<?= base_url("index.php"); ?>">
        <input type="hidden" name="api_url" 
            id="api_url" value="<?= API_URI; ?>">
        <input type="hidden" name="auth" 
            id="auth" value="<?= isset($auth->token) ? $auth->token: ''; ?>">
        <input type="hidden" name="_main_menu" 
            id="_main_menu" value="<?= $main_menu; ?>">

        <!-- START: Pre Loader-->
        <div class="se-pre-con">
            <div class="loader"></div>
        </div>
        <!-- END: Pre Loader-->

        <!-- START: Template JS-->
        <script src="<?= base_url('assets/dist/vendors/jquery/jquery-3.3.1.min.js'); ?>"></script>
        <script src="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.js'); ?>"></script>
        <script src="<?= base_url('assets/dist/vendors/moment/moment.js'); ?>"></script>
        <script src="<?= base_url('assets/dist/vendors/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
        <script src="<?= base_url('assets/dist/vendors/slimscroll/jquery.slimscroll.min.js'); ?>"></script>
        <script src="<?= base_url('assets/dist/vendors/toastr/toastr.min.js'); ?>"></script>
        <script src="https://cdn.jsdelivr.net/npm/lozad/dist/lozad.min.js"></script>
        <!-- END: Template JS--> 

        <!-- START: APP JS-->
        <script src="<?= base_url('assets/dist/js/app.js'); ?>"></script>
        <script src="<?= base_url('assets/dist/js/auth/request.js'); ?>"></script>
        <script>
            var base_url = document.getElementById("base_url");
            var api_url = document.getElementById("api_url");
            var auth = document.getElementById("auth");
            var _main_menu = document.getElementById("_main_menu");
        </script>

        <!-- Facebook Pixel Code -->
        <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '3542009505894280');
        fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=3542009505894280&ev=PageView&noscript=1"
        /></noscript>
        <!-- End Facebook Pixel Code -->

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-E6E3SRPJ11"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-E6E3SRPJ11');
        </script>

        <!-- START: Main Content-->
        <?php
            // Load Secondary Menu
            echo ('<div id="header-fix" class="header">');
                $this->load->view("components/menus/secondary-menu");
                $this->load->view("components/menus/top-bar");
            echo ('</div>');

            // Load Page
            $this->load->view("components/modals/categories");
        ?>
        <div class="container mt-4">
            <?php
                $this->load->view("components/menus/main-menu");
            ?>
        </div>
        <div class="container mb-4">
            <div class="row">
                <?php 
                    // Load Cart Lists
                    $this->load->view('anonymous/content/cart-checkout');  
                ?>
            </div>
        </div>

        <div 
            style="background-color: #fbfbfb; 
                font-color: rgba(0,0,0,.54);
                border-top: 1px solid rgba(0,0,0,.1)">
            <div class="container">
                <?php
                    // Load Footer
                    $this->load->view('anonymous/content/footer');
                ?>
            </div>
        </div>
        <!-- END: Content-->

        <!-- START: Page Script JS-->
        <?php
            if (isset($error)) {
                $this->load->view("components/error-modal", array(
                    "errorModalTitle" => $error["title"],
                    "errorModalContent" => $error["content"],
                    "errorModalDetail" => $error["details"]
                ));
            }

            $this->load->view("components/modals/lost-password", array(
                "hideModal" => "hide",
                "isSuccess" => false
            ));

            // Create New Address
            $this->load->view("components/modals/form", array(
                "hideModal" => "hide",
                "idElement" => "modal-create-address",
                "modalTitle" => "Tambah Alamat Baru",
                "iconTitle" => "icon-plus",
                "modalActionForm" => "#",
                "modalContentForm" => '
                <div class="row">
                    <div class="col-md-6">
                        <label>Provinsi <span class="text-danger">*</span></label>
                        <div class="form-group">
                            <select class="style-select form-control select2" id="checkout-state">
                                <option label="select"></option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label>Kota / Kabupaten <span class="text-danger">*</span></label>
                        <div class="form-group">
                            <select class="style-select form-control select2" id="checkout-city">
                                <option label="select"></option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label>Kecamatan <span class="text-danger">*</span></label>
                        <div class="form-group">
                            <select class="style-select form-control select2" id="checkout-suburb">
                                <option label="select"></option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label>Kelurahan / Desa <span class="text-danger">*</span></label>
                        <div class="form-group">
                            <select class="style-select form-control select2" id="checkout-area">
                                <option label="select"></option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label>Kodepos</label>
                            <input type="text" class="form-control" placeholder="" id="checkout-zip" readonly>
                    </div>
                    <div class="col-md-6">
                        <label>Alamat Jalan <span class="text-danger">*</span></label>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="" id="checkout-street">
                        </div>
                    </div>
                </div>
                ',
                "modalButtonForm" => '
                    <button type="button" class="btn btn-primary add-todo" id="create-address">Tambah Alamat</button>
                    <div id="create-address-loader"></div>
                '
            ));

            // Edit Current Address
            $this->load->view("components/modals/form", array(
                "hideModal" => "hide",
                "idElement" => "modal-edit-address",
                "modalTitle" => "Ubah Alamat",
                "iconTitle" => "icon-pencil",
                "modalActionForm" => "#",
                "modalContentForm" => '
                <div style="height: 250px; overflow: auto" id="container-address">
                    
                </div>
                ',
                "modalButtonForm" => '<button type="button" class="btn btn-danger"data-dismiss="modal">Cancel</button>'
            ));
        ?>

        <!-- START: Page Vendor JS-->
        <script src="<?= base_url('assets/dist/vendors/fancybox/jquery.fancybox.min.js'); ?>"></script>
        <!-- END: Page Vendor JS-->

        <!-- START: Page Script JS-->
        <script src="<?= base_url('assets/dist/vendors/select2/js/select2.full.min.js') ?>"></script>
        <script>
            // Select2
            $(".select2").select2({
                theme: 'bootstrap4',
                width: 'style',
                placeholder: "select",
            });
        </script>
        <!-- END: Page Script JS-->
    </body>
</html>