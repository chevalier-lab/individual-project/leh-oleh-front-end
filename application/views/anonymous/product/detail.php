<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view('components/head'); ?>

        <!-- START: Template CSS-->
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/bootstrap/css/bootstrap.min.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.theme.min.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/simple-line-icons/css/simple-line-icons.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/flags-icon/css/flag-icon.min.css'); ?>"> 
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/fontawesome/css/all.min.css'); ?>">  
        <!-- END Template CSS-->     

        <!-- START: Page CSS-->   
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/social-button/bootstrap-social.css'); ?>"/>   
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/sweetalert/sweetalert.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/fancybox/jquery.fancybox.min.css'); ?>">  
        <!-- END: Page CSS-->

        <!-- START: Custom CSS-->
        <link rel="stylesheet" href="<?= base_url('assets/dist/css/main.css'); ?>">
        <!-- END: Custom CSS-->
    </head>
    <body id="main-container" class="default bg-white"
    oncontextmenu="return false;">

        <input type="hidden" name="base_url" 
            id="base_url" value="<?= base_url("index.php"); ?>">
        <input type="hidden" name="api_url" 
            id="api_url" value="<?= API_URI; ?>">
        <input type="hidden" name="id_product" 
            id="id_product" value="<?= $id_product; ?>">
        <input type="hidden" name="auth" 
            id="auth" value="<?= isset($auth->token) ? $auth->token: ''; ?>">
        <input type="hidden" name="_main_menu" 
            id="_main_menu" value="<?= $main_menu; ?>">

        <!-- START: Pre Loader-->
        <div class="se-pre-con">
            <div class="loader"></div>
        </div>
        <!-- END: Pre Loader-->

        <!-- START: Template JS-->
        <script src="<?= base_url('assets/dist/vendors/jquery/jquery-3.3.1.min.js'); ?>"></script>
        <script src="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.js'); ?>"></script>
        <script src="<?= base_url('assets/dist/vendors/moment/moment.js'); ?>"></script>
        <script src="<?= base_url('assets/dist/vendors/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
        <script src="<?= base_url('assets/dist/vendors/slimscroll/jquery.slimscroll.min.js'); ?>"></script>
        <script src="<?= base_url('assets/dist/vendors/sweetalert/sweetalert.min.js');?>"></script>
        <script src="https://cdn.jsdelivr.net/npm/lozad/dist/lozad.min.js"></script>
        <!-- END: Template JS--> 

        <!-- START: APP JS-->
        <script src="<?= base_url('assets/dist/js/app.js'); ?>"></script>
        <script src="<?= base_url('assets/dist/js/auth/request.js'); ?>"></script>
        <script>
            var base_url = document.getElementById("base_url");
            var api_url = document.getElementById("api_url");
            var id_product = document.getElementById("id_product");
            var auth = document.getElementById("auth");
            var _main_menu = document.getElementById("_main_menu");
        </script>
        <!-- END: APP JS-->

        <!-- Facebook Pixel Code -->
        <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '3542009505894280');
        fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=3542009505894280&ev=PageView&noscript=1"
        /></noscript>
        <!-- End Facebook Pixel Code -->

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-E6E3SRPJ11"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-E6E3SRPJ11');
        </script>

        <!-- START: Main Content-->
        <?php
            // Load Secondary Menu
            echo ('<div id="header-fix" class="header">');
                $this->load->view("components/menus/secondary-menu");
                $this->load->view("components/menus/top-bar");
            echo ('</div>');

            // Load Page
            $this->load->view("components/modals/categories");
        ?>
        <div class="container mt-4">
            <?php
                $this->load->view("components/menus/main-menu");
            ?>
        </div>
        <div class="container mb-4">
            <div class="row" id="container">
                <?php 
                    // Load Product View
                    $this->load->view('anonymous/content/product-view'); 

                    // Load Product Info
                    // $this->load->view('anonymous/content/product-info'); 

                    // Load Product Related
                    $this->load->view('anonymous/content/product-related'); 
                ?>
                <div class="container">
                <?php 
                    // Interesting Information
                    $this->load->view('anonymous/content/interest-info');
                ?>
                </div>
            </div>
            <div id="container-error"></div>
        </div>

        <div 
            style="background-color: #fbfbfb; 
                font-color: rgba(0,0,0,.54);
                border-top: 1px solid rgba(0,0,0,.1)">
            <div class="container">
                <?php
                    // Load Footer
                    $this->load->view('anonymous/content/footer');
                ?>
            </div>
        </div>
        <!-- END: Content-->

        <div style="position: fixed; bottom: 0; right: 0; z-index: 999; padding: 16px"
        onclick="gotoChat()">
            <button class="btn btn-danger btn-lg" type="button">
                <i class="icon-speech"></i>
                <span>Hubungi Toko?</span>
            </button>
        </div>

        <!-- START: Page Script JS-->
        <?php
            if (isset($error)) {
                $this->load->view("components/error-modal", array(
                    "errorModalTitle" => $error["title"],
                    "errorModalContent" => $error["content"],
                    "errorModalDetail" => $error["details"]
                ));
            }

            $this->load->view("components/modals/lost-password", array(
                "hideModal" => "hide",
                "isSuccess" => false
            ));
        ?>

        <!-- START: Page Vendor JS-->
        <script src="<?= base_url('assets/dist/vendors/fancybox/jquery.fancybox.min.js'); ?>"></script>
        <!-- END: Page Vendor JS-->

        <!-- START: Page JS-->
        <script src="<?= base_url('assets/dist/js/gallery.script.js'); ?>"></script>

        <script>

            var dataProduct = null;

            (function ($) {
                "use strict";
                $(window).on("load", function () {
                    loadDetailProduct()
                });
            })(jQuery);

            function gotoChat() {
                if (dataProduct != null) {
                    location.assign(base_url.value + "/anonymous/chatMerchant/index/" + dataProduct.id_merchant)
                }
            }

            function prepareToCart(type) {
                var qty = $("#qty").val();
                qty = Number(qty);
                if (qty <= 0) {
                    swal("Gagal!", "Jumlah barang pesanan minimal 1", "error");
                } else {
                    if (dataProduct != null) {
                        swal({
                            title: "Tambahkan Keranjang",
                            text: "Apa anda yakin ingin menambahkan " + dataProduct.product_name + "ke keranjang?",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonClass: "btn-danger",
                            confirmButtonText: 'Tambahkan',
                            cancelButtonText: "Batal",
                            cancelButtonClass: "btn-primary",
                            closeOnConfirm: false,
                            closeOnCancel: false
                        }, function(isConfirm){
                            if (isConfirm){
                                addToCart(qty, type)
                            } else {
                                swal("Gagal!", "Menambahkan keranjang", "error");
                            }
                        });
                    } else {
                        swal("Gagal!", "Gagal memuat data", "error");
                    }
                }
            }

            function addToCart(qty, type) {

                if(auth.value == '') {
                    swal("Gagal!", "Harap login terlebih dahulu", "error");
                    return;
                }

                var raw = req.raw({
                    idMProducts: dataProduct.id,
                    qty: qty
                })

                var formData = new FormData()
                formData.append("raw", raw);

                $.ajax({
                    url: base_url.value + "/anonymous/cart/add_to_cart",
                    data: formData,
                    type: "POST",
                    contentType: false,
                    processData: false,
                    success: function(response) {
                        response = req.data(response)
                        if (response.code == 200) {
                            swal("Berhasil", "Menambahkan produk keranjang", "success");
                            if (type == 1) location.assign(base_url.value + "/anonymous/cart/checkout")
                            else loadCartBadge()
                        } else {
                            swal("Gagal!", response.message, "error");
                        }
                    }
                });
            }

            function loadDetailProduct() {
                $("#container").hide();
                $("#container-error").hide();
                $.ajax({
                    url: base_url.value + "/anonymous/product/load_detail_product/" + id_product.value,
                    data: null,
                    type: "GET",
                    contentType: false,
                    processData: false,
                    success: function(response) {
                        response = req.data(response)
                        if (response.code == 200) {
                            var data = response.data
                            dataProduct = data;
                            var price = Number(data.price_selling)
                            var discount = Number(data.discount);
                            var price_divide_discount = ((price / 100) * discount);

                            price = (price - price_divide_discount)

                            // Render
                            $("#name-product-review").text(data.product_name)
                            var rating = (data.rating != null) ? Number(data.rating) : 0;
                            for (var i = 0; i < 5; i++) {
                                if (i < rating) $("#rating-product-review")
                                .append(`<li class="list-inline-item"><a href="javascript:void(0);" class="text-warning"><i class="fas fa-star"></i></a></li>`)
                                else $("#rating-product-review")
                                .append(`<li class="list-inline-item"><a href="javascript:void(0);"><i class="fas fa-star"></i></a></li>`)
                            }

                            $("#price-product-review").html(`
                            <table>
                                <tr>
                                    <th style="width: 100px">HARGA</th>
                                    <th> : </th>
                                    <td>
                                        ${(discount > 0) ? `
                                            <div class="float-left ml-2">
                                                <span class="badge badge-danger">Diskon ${discount}%</span>
                                                <del>${req.money(data.price_selling.toString(), "Rp ")}</del>
                                                <h4 class="lato-font mb-0 text-danger">${req.money(price.toString(), "Rp ")}</h4>
                                            </div>
                                        `: `
                                            <div class="float-left ml-2">
                                                <h4 class="lato-font body-color mb-0">${req.money(data.price_selling.toString(), "Rp ")}</h4>
                                            </div>
                                        `}
                                    </td>
                                </tr>
                            </table>
                            `);

                            $("#qty-product-review").html(`
                            <table>
                                <tr>
                                    <th style="width: 100px">
                                        JUMLAH
                                    </th>
                                    <th> : <th>
                                    <td>
                                    
                                        <div class="float-left mx-2">
                                        <input type="number" class="form-control" value="1"
                                                id="qty">
                                        </div>

                                    </td>
                                </tr>
                            </table>
                            `);

                            $("#info-product-review").html(`
                            <table>
                                <tr>
                                    <th style="width: 100px">
                                        INFO PRODUK
                                    </th>
                                    <th> : <th>
                                    <td>
                                    
                                        ${(data.info != null) ? `
                                            <div class="float-left mx-2">
                                                <span>Berat</span>
                                                <div>
                                                    <strong>${data.info.weight} kg</strong>
                                                </div>
                                            </div>
                                            <div class="float-left mx-2">
                                                <span>Dimensi</span>
                                                <div>
                                                    <strong>${data.info.length} cm x ${data.info.width} cm x ${data.info.height} cm</strong>
                                                </div>
                                            </div>
                                        ` : ''}

                                        <div class="float-left mx-2">
                                            <span>Toko</span>
                                            <div>
                                                <strong>${data.market_name}</strong>
                                            </div>
                                        </div>

                                    </td>
                                </tr>
                            </table>
                            `);

                            $("#location-product-review").html(`
                            <table>
                                <tr>
                                    <th style="width: 100px">
                                        LOKASI TOKO
                                    </th>
                                    <th> : <th>
                                    <td>
                                    
                                        ${(data.location.length > 0) ? `
                                            <div class="float-left mx-2">
                                            ${data.location.map(function(item, position) {
                                                return `
                                                    <span class="body-color font-weight-normal">${item.province_name}
                                                        ${(position < (data.location.length - 1)) ? ", " : ""}</span>
                                                `
                                            }).join('')}
                                            </div>
                                        ` : ''}

                                    </td>
                                </tr>
                            </table>
                            `);

                            $("#category-product-review").html(`
                            <table>
                                <tr>
                                    <th style="width: 100px">
                                        KATEGORI PRODUK
                                    </th>
                                    <th> : <th>
                                    <td>
                                    
                                        ${(data.categories.length > 0) ? `
                                            <div class="float-left mx-2">
                                            ${data.categories.map(function(item, position) {
                                                return `
                                                    <span class="body-color font-weight-normal">${item.category}
                                                        ${(position < (data.categories.length - 1)) ? ", " : ""}</span>
                                                `
                                            }).join('')}
                                            </div>
                                        ` : ''}

                                    </td>
                                </tr>
                            </table>
                            `);

                            $("#description-product-review").html(`${data.description}`)

                            $("#cover-product-review").html(`
                                <div class="lozad-img-bg"
                                    data-img-bg="${data.uri}"
                                    style="background-size: cover; height: 320px; 
                                    background-position: center;">
                                    <div class="loader-item"></div>
                                </div>
                                
                                ${(data.photos.length > 0) ? `
                                <div class="container-fluid mt-2">
                                    <div class="row">

                                    ${data.photos.map(function(item, position) {
                                        return `
                                        <div class="col-4 p-0">
                                            <div class="lozad-img-bg img-thumbnail"
                                                data-img-bg="${item.uri}"
                                                style="background-size: cover; height: 100px; 
                                                background-position: center;
                                                background-image: url('${base_url.value + '/../assets/dist/images/broken-256.png'}')">
                                            </div>
                                        </div>
                                        `;
                                    }).join('')}

                                    </div>
                                </div>
                                `: ""}
                            `);

                            setTimeout(() => {
                                lozad('.lozad-img-bg', {
                                    load: function(el) {
                                        el.style.backgroundImage = "url('"+el.getAttribute("data-img-bg")+"')";

                                        for (var i = 0; i < el.childNodes.length; i++) {
                                            if (el.childNodes[i].className == "loader-item") {
                                                notes = el.childNodes[i];
                                                el.removeChild(notes);
                                                break;
                                            }        
                                        }
                                        
                                    }
                                }).observe();
                            }, 1000);
                            
                            $("#container").show();
                        } else {
                            $("#container-error").html(`
                            <div class="jumbotron mb-0 text-center theme-background rounded">
                                <h1 class="display-3 font-weight-bold"> 404</h1>
                                <h5><i class="ion ion-alert pr-2"></i>Oops! Something went wrong</h5>
                                <p>The page you are looking for is not found, please try after some time or go back to home</p>
                                <a href="${base_url.value}" class="btn btn-danger">Go To Home</a>
                            </div>
                            `);
                            $("#container-error").show();
                        }
                    }
                    // ,
                    // error: function(response) {
                    //     console.log("ERROR", response);
                    //     $("#btn-login").removeAttr("disabled");
                    // }
                });
            }

        </script>
        <!-- END: Page Script JS-->
    </body>
</html>