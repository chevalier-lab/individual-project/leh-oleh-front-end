<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view('components/head'); ?>

        <!-- START: Template CSS-->
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/bootstrap/css/bootstrap.min.css'); ?>">

        <!-- START: Custom CSS-->
        <link rel="stylesheet" href="<?= base_url('assets/dist/css/main.css'); ?>">
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <!-- END: Custom CSS-->
    </head>
    <body id="main-container" class="default bg-white"
    oncontextmenu="return false;">

        <input type="hidden" name="base_url" 
            id="base_url" value="<?= base_url("index.php"); ?>">
        <input type="hidden" name="api_url" 
            id="api_url" value="<?= API_URI; ?>">
        <input type="hidden" name="auth" 
            id="auth" value="<?= isset($auth->token) ? $auth->token: ''; ?>">
        <input type="hidden" name="_main_menu" 
            id="_main_menu" value="<?= $main_menu; ?>">

        <!-- START: APP JS-->
        <script src="<?= base_url('assets/dist/js/auth/request.js'); ?>"></script>
        <script>
            var base_url = document.getElementById("base_url");
            var api_url = document.getElementById("api_url");
            var auth = document.getElementById("auth");
            var _main_menu = document.getElementById("_main_menu");
        </script>
        <!-- END: APP JS-->
        <script src="<?= base_url('assets/dist/js/app.js'); ?>"></script>

        <!-- Facebook Pixel Code -->
        <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '3542009505894280');
        fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=3542009505894280&ev=PageView&noscript=1"
        /></noscript>
        <!-- End Facebook Pixel Code -->

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-E6E3SRPJ11"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-E6E3SRPJ11');
        </script>

        <div style="width: 100%">
        <?php
            $this->load->view("anonymous/pages/content/merchant-ad-heading");
            $this->load->view("anonymous/pages/content/merchant-ad-footer");
        ?>
        </div>
        
    </body>
</html>