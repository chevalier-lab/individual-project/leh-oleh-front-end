<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view('components/head'); ?>

        <!-- START: Template CSS-->
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/bootstrap/css/bootstrap.min.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.theme.min.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/simple-line-icons/css/simple-line-icons.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/flags-icon/css/flag-icon.min.css'); ?>"> 
        <!-- END Template CSS-->     

        <!-- START: Page CSS-->   
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/social-button/bootstrap-social.css'); ?>"/>   
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/sweetalert/sweetalert.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/fancybox/jquery.fancybox.min.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/toastr/toastr.min.css'); ?>" />
        <!-- END: Page CSS-->

        <!-- START: Custom CSS-->
        <link rel="stylesheet" href="<?= base_url('assets/dist/css/main.css'); ?>">
        <!-- END: Custom CSS-->
    </head>
    <body id="main-container" class="default bg-white"
    oncontextmenu="return false;">

        <input type="hidden" name="base_url" 
            id="base_url" value="<?= base_url("index.php"); ?>">
        <input type="hidden" name="api_url" 
            id="api_url" value="<?= API_URI; ?>">
        <input type="hidden" name="auth" 
            id="auth" value="<?= isset($auth->token) ? $auth->token: ''; ?>">
        <input type="hidden" name="_main_menu" 
            id="_main_menu" value="<?= $main_menu; ?>">

        <!-- START: Pre Loader-->
        <div class="se-pre-con">
            <div class="loader"></div>
        </div>
        <!-- END: Pre Loader-->

        <!-- START: Template JS-->
        <script src="<?= base_url('assets/dist/vendors/jquery/jquery-3.3.1.min.js'); ?>"></script>
        <script src="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.js'); ?>"></script>
        <script src="<?= base_url('assets/dist/vendors/moment/moment.js'); ?>"></script>
        <script src="<?= base_url('assets/dist/vendors/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
        <script src="<?= base_url('assets/dist/vendors/slimscroll/jquery.slimscroll.min.js'); ?>"></script>
        <script src="<?= base_url('assets/dist/vendors/toastr/toastr.min.js'); ?>"></script>
        <!-- END: Template JS--> 

        <!-- Facebook Pixel Code -->
        <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '3542009505894280');
        fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=3542009505894280&ev=PageView&noscript=1"
        /></noscript>
        <!-- End Facebook Pixel Code -->

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-E6E3SRPJ11"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-E6E3SRPJ11');
        </script>

        <!-- START: APP JS-->
        <script src="<?= base_url('assets/dist/js/app.js'); ?>"></script>
        <!-- END: Template JS-->

        <!-- START: Page Vendor JS-->
        <script src="<?= base_url('assets/dist/js/auth/request.js'); ?>"></script>
        <script>
            var base_url = document.getElementById("base_url");
            var api_url = document.getElementById("api_url");
            var auth = document.getElementById("auth");
            var _main_menu = document.getElementById("_main_menu");
        </script>
        <!-- END: APP JS-->

        <!-- START: Main Content-->
        <?php
            // Load Secondary Menu
            echo ('<div id="header-fix" class="header">');
                $this->load->view("components/menus/secondary-menu");
                $this->load->view("components/menus/top-bar");
            echo ('</div>');

            // Create New Address
            $this->load->view("components/modals/form", array(
                "hideModal" => "hide",
                "idElement" => "modal-new-address",
                "modalTitle" => "Tambah Alamat Baru",
                "iconTitle" => "icon-plus",
                "modalActionForm" => "#",
                "modalContentForm" => '
                <div class="row">
                    <div class="col-md-6">
                        <label>Provinsi <span class="text-danger">*</span></label>
                        <div class="form-group">
                            <select class="style-select form-control select2" id="checkout-state">
                                <option label="select"></option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label>Kota / Kabupaten <span class="text-danger">*</span></label>
                        <div class="form-group">
                            <select class="style-select form-control select2" id="checkout-city">
                                <option label="select"></option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label>Kecamatan <span class="text-danger">*</span></label>
                        <div class="form-group">
                            <select class="style-select form-control select2" id="checkout-suburb">
                                <option label="select"></option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label>Kelurahan / Desa <span class="text-danger">*</span></label>
                        <div class="form-group">
                            <select class="style-select form-control select2" id="checkout-area">
                                <option label="select"></option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label>Kodepos</label>
                            <input type="text" class="form-control" placeholder="" id="checkout-zip" readonly>
                    </div>
                    <div class="col-md-6">
                        <label>Alamat Jalan <span class="text-danger">*</span></label>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="" id="checkout-street">
                        </div>
                    </div>
                </div>
                ',
                "modalButtonForm" => '<button type="button" class="btn btn-primary add-todo" 
                data-dismiss="modal"
                id="create-address">Tambah Alamat</button>'
            ));

            // Load Page
            $this->load->view("components/modals/categories");
        ?>
        <div class="container mt-4">
            <?php
                $this->load->view("components/menus/main-menu");
            ?>
        </div>
        <div class="container mb-4">
            <div class="row">      
                
            <div class="col-12 mt-0">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Lokasi Saya</h4> 
                    </div>
                    <div class="card-body">

                        <div class="pr-1 pl-1 mb-2">
                            <button class="btn btn-primary"
                            data-toggle="modal" data-target="#modal-new-address">Tambah Lokasi</button>
                        </div>

                        <div class="table-responsive pr-1 pl-1 mb-0">

                            <?php

                            $this->load->view('components/table', array(
                                "idTable" => "location-list",
                                "isCustomThead" => true,
                                "thead" => '
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Alamat</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Aksi</th>
                                </tr>
                                ',
                                "tbody" => ""
                            )) ?>

                        </div>

                    </div>
                </div>
            </div>

            </div>
        </div>

        <div 
            style="background-color: #fbfbfb; 
                font-color: rgba(0,0,0,.54);
                border-top: 1px solid rgba(0,0,0,.1)">
            <div class="container">
                <?php
                    // Load Footer
                    $this->load->view('anonymous/content/footer');
                ?>
            </div>
        </div>
        <!-- END: Content-->

        <!-- START: Page Script JS-->
        <?php
            if (isset($error)) {
                $this->load->view("components/error-modal", array(
                    "errorModalTitle" => $error["title"],
                    "errorModalContent" => $error["content"],
                    "errorModalDetail" => $error["details"]
                ));
            }

            $this->load->view("components/modals/lost-password", array(
                "hideModal" => "hide",
                "isSuccess" => false
            ));
        ?>

        <!-- START: Page Vendor JS-->
        <script src="<?= base_url('assets/dist/vendors/fancybox/jquery.fancybox.min.js'); ?>"></script>
        <!-- END: Page Vendor JS-->

        <!-- START: Page JS-->
        <script src="<?= base_url('assets/dist/js/gallery.script.js'); ?>"></script>
        <!-- END: Page Script JS-->

        <script>

            var province = [];
            var city = [];
            var suburb = [];
            var area = [];

            var address = {
                province: "",
                province_id: "",
                city: "",
                city_id: "",
                suburb: "",
                suburb_id: "",
                area: "",
                area_id: "",
                zip: "",
                street: ""
            };

            var locations = [];

            (function($) {
                "use strict";
                $(window).on("load", function() {
                    loadLocations();

                    // Load Province
                    loadProvince()

                    $("#checkout-street").on("keyup", function() {
                        address.street = $("#checkout-street").val()
                    })

                    $("#checkout-state").on("change", function() {
                        address.province = $(this).children("option:selected").val();
                        loadCity($(this).children("option:selected").val())
                    })

                    $("#checkout-city").on("change", function() {
                        address.city = $(this).children("option:selected").val();
                        loadSuburb($(this).children("option:selected").val())
                    })

                    $("#checkout-suburb").on("change", function() {
                        address.suburb = $(this).children("option:selected").val();
                        loadArea($(this).children("option:selected").val())
                    })

                    $("#checkout-area").on("change", function() {
                        address.area = $(this).children("option:selected").val();
                        setupZip($(this).children("option:selected").val())
                    })

                    $("#create-address").on("click", function(){
                        if(address.province == "" || address.city == "" || address.suburb == "" || address.area == "" || address.zip == "" || address.street == "")
                            toastr.error("Harap isi seluruh form yang tersedia");
                        else {
                            $("#create-address").attr("disabled", true);
                            var raw = req.raw({
                                province_id: address.province_id,
                                province: address.province,
                                city_id: address.city_id,
                                city: address.city,
                                suburb_id: address.suburb_id,
                                suburb: address.suburb,
                                area_id: address.area_id,
                                area: address.area,
                                postcode: address.zip,
                                address: address.street,
                                is_active: 1
                            })
            
                            var formData = new FormData()
                            formData.append("raw", raw)
            
                            $.ajax({
                                url: base_url.value + "/anonymous/cart/add_new_location",
                                data: formData,
                                type: "POST",
                                contentType: false,
                                processData: false,
                                success: function(response) {
                                    response = req.data(response)
                                    $("#create-address").removeAttr("disabled");
                                    if (response.code == 200) {
                                        toastr.success("Berhasil menambah lokasi")
                                        
                                        setTimeout(() => {
                                            location.reload();
                                        }, 1000);
                                    } else {
                                        toastr.error(response.message)
                                    }
                                }
                            });
                        }
                    })
                });
            })(jQuery);

            function loadLocations() {
                $.ajax({
                    url: base_url.value + "/services/locations/lists",
                    data: null,
                    type: "GET",
                    contentType: false,
                    processData: false,
                    success: function(response) {
                        response = req.data(response)
                        if (response.code == 200) {
                            locations = response.data;

                            $("#location-list tbody").html(`${locations.map(function(item, position) {
                                return `
                                    <tr>
                                        <td>${(position + 1)}</td>
                                        <td>${item.address + ", " + item.area + ", " + item.suburb + ", " + item.city + ", " + item.province + ", " + item.postcode}</td>
                                        <td>${(item.is_active == 1) ? "Aktif" : "-"}</td>
                                        <td>
                                            <button class="btn btn-warning" type="button">Ubah</button>
                                            ${(item.is_active == 0) ? `<button class="btn btn-primary" type="button"
                                                onclick="activateProvince(${item.id})">Aktifkan</button>` : ""}
                                        </td>
                                    </tr>
                                `;
                            }).join('')}`);
                        } else {
                            $("#location-list tbody").html(`
                                <tr>
                                    <td colspan="4">Belum ada data</td>
                                </tr>
                            `);
                        }
                    }
                });
            }

            function activateProvince(id) {
                $.ajax({
                    url: base_url.value + "/services/locations/activate/" + id,
                    data: null,
                    type: "GET",
                    contentType: false,
                    processData: false,
                    success: function(response) {
                        response = req.data(response)
                        if (response.code == 200) {
                            toastr.success("Berhasil mengaktifkan lokasi")
                            
                            setTimeout(() => {
                                location.reload();
                            }, 1000);
                        } else {
                            toastr.error(response.message)
                        }
                    }
                });
            }

            function loadProvince() {
                $.ajax({
                    url: base_url.value + "/services/shipper/locations/load_province",
                    data: null,
                    type: "GET",
                    contentType: false,
                    processData: false,
                    success: function(response) {
                        response = req.data(response)
                        if (response.status == "success") {
                            var data = response.data.rows
                            province = data;
                            setupProvince()
                        }
                    }
                });
            }

            function loadCity(prov) {
                var id = -1;
                province.forEach(function(item) {
                    if (item.name == prov) id = item.id;
                });
                address.province_id = id
                $.ajax({
                    url: base_url.value + "/services/shipper/locations/load_city?id=" + id,
                    data: null,
                    type: "GET",
                    contentType: false,
                    processData: false,
                    success: function(response) {
                        response = req.data(response)
                        if (response.status == "success") {
                            var data = response.data.rows
                            city = data;
                            setupCity()
                        }
                    }
                });
            }

            function loadSuburb(citySelected) {
                var id = -1;
                city.forEach(function(item) {
                    if (item.name == citySelected) id = item.id;
                });
                address.city_id = id
                $.ajax({
                    url: base_url.value + "/services/shipper/locations/load_suburbs/" + id,
                    data: null,
                    type: "GET",
                    contentType: false,
                    processData: false,
                    success: function(response) {
                        response = req.data(response)
                        if (response.status == "success") {
                            var data = response.data.rows
                            suburb = data;
                            setupSuburb()
                        }
                    }
                });
            }

            function loadArea(suburbSelected) {
                var id = -1;
                suburb.forEach(function(item) {
                    if (item.name == suburbSelected) id = item.id;
                });
                address.suburb_id = id
                $.ajax({
                    url: base_url.value + "/services/shipper/locations/load_areas/" + id,
                    data: null,
                    type: "GET",
                    contentType: false,
                    processData: false,
                    success: function(response) {
                        response = req.data(response)
                        if (response.status == "success") {
                            var data = response.data.rows
                            area = data;
                            setupArea()
                        }
                    }
                });
            }

            function setupProvince() {
                $("#checkout-state").html(`${province.map(function(item, position) {
                    if (position == 0) 
                    return `
                    <option label="select" selected disabled></option>
                    <option value="${item.name}">${item.name}</option>`;
                    else 
                    return `<option value="${item.name}">${item.name}</option>`;
                }).join('')}`);
            }

            function setupCity() {
                $("#checkout-city").html(`${city.map(function(item, position) {
                    if (position == 0) 
                    return `
                    <option label="select" selected disabled></option>
                    <option value="${item.name}">${item.name}</option>`;
                    else 
                    return `<option value="${item.name}">${item.name}</option>`;
                }).join('')}`);
            }

            function setupSuburb() {
                $("#checkout-suburb").html(`${suburb.map(function(item, position) {
                    if (position == 0) 
                    return `
                    <option label="select" selected disabled></option>
                    <option value="${item.name}">${item.name}</option>`;
                    else 
                    return `<option value="${item.name}">${item.name}</option>`;
                }).join('')}`);
            }

            function setupArea() {
                $("#checkout-area").html(`${area.map(function(item, position) {
                    if (position == 0) 
                    return `
                    <option label="select" selected disabled></option>
                    <option value="${item.name}">${item.name}</option>`;
                    else 
                    return `<option value="${item.name}">${item.name}</option>`;
                }).join('')}`);
            }

            function setupZip(areaSelected) {
                var id = -1;
                area.forEach(function(item) {
                    if (item.name == areaSelected){
                        id = item.postcode;
                        address.area_id = item.id;
                    } 
                });
                address.zip = id
                $("#checkout-zip").val(id);
            }
        </script>
    </body>
</html>