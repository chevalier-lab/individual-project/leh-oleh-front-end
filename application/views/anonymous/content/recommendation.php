<div class="d-flex justify-content-between align-items-center">
    <h4 class="card-title text-danger">Rekomendasi oleh - oleh</h4>
</div>

<div class="container-fluid mb-4">
    <div class="row" id="recommended-product"></div>
</div>

<div id="devider-load-product"></div>
<div id="load-product" class="mb-4">
    <center>
        Sedang memuat produk...
    </center>
</div>

<script>
    // Initial Pagination
    var page = -1;
    var isLoaded = false;
    var paginationClicked = false;
    var paginationDirection = "";
    var checkedData = 0;

    (function ($) {
        "use strict";

        $(document).scrollTop(0);
        loadNewProducts();

        if (
            "IntersectionObserver" in window &&
            "IntersectionObserverEntry" in window &&
            "intersectionRatio" in window.IntersectionObserverEntry.prototype
        ) {
            let observer = new IntersectionObserver(entries => {
                if (entries[0].boundingClientRect.y < 400) {
                    if (isLoaded == false && page != -1) {
                        page++;
                        isLoaded = true;
                        $("#load-product").show();
                        loadNewProducts();
                    }
                }
            });
            observer.observe(document.querySelector("#devider-load-product"));
        }
    })(jQuery);

    function loadNewProducts() {
        var status = false;
        if (page == -1) {
            page = 0;
        }

        var raw = req.raw({
            page: page,
            search: "",
            order_by: "id",
            order_direction: "DESC"
        })

        var formData = new FormData()
        formData.append("raw", raw)

        $.ajax({
            url: base_url.value + "/anonymous/landing/load_new_products",
            data: formData,
            type: "POST",
            contentType: false,
            processData: false,
            success: function(response) {
                response = req.data(response)
                if (response.code == 200) {
                    var data = response.data

                    if (checkedData == 0) {
                        if (data.length == 0) {
                            $("#recommended-product").html(`
                                <div class="alert alert-primary col-sm-12" role="alert">
                                    Belum ada produk saat ini
                                </div>
                            `);
                        } else $("#recommended-product").html(``);
                    }

                    if (data.length > 0) {
                        isLoaded = false;
                    }

                    checkedData++;

                    // Render Product
                    data.forEach(function(item) {

                        var price = Number(item.price_selling)
                        var discount = Number(item.discount);
                        var price_divide_discount = ((price / 100) * discount);

                        price = (price - price_divide_discount)

                        var product_location = base_url.value + "/anonymous/product/detail/" + item.id

                        $("#recommended-product").append(`
                            <div class="col-sm-6 col-md-4 col-lg-2 border py-3">
                                <div class="position-relative">
                                    <div class="lozad-img-bg"
                                    data-img-bg="${item.uri}"
                                    style="background-size: cover; height: 150px; 
                                    background-position: center;
                                    onclick="location.assign('${product_location}')">
                                    <div class="loader-item"></div>
                                    </div>

                                    ${
                                        (discount > 0) ? `
                                        <div class="mb-3" style="top: 0px; position: absolute">
                                            <a href="javascript:void(0);" class="rounded-right bg-warning px-3 py-2">-${discount}%</a>
                                        </div>
                                        ` : ""
                                    }
                                    <div class="caption-bg fade bg-transparent text-right">
                                        <div class="d-table w-100 h-100 ">
                                            <div class="d-table-cell align-bottom">
                                                <div class="mb-3">
                                                    <a href="javascript:void(0);" class="rounded-left bg-white px-3 py-2 text-danger"><i class="icon-heart"></i></a>
                                                </div>
                                                <div class="mb-4">
                                                    <a href="javascript:void(0);" class="rounded-left bg-white px-3 py-2 text-danger"><i class="icon-bag"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="pt-3"
                                    onclick="location.assign('${product_location}')">
                                    <p class="mb-2"><a href="javascript:void(0);" class="font-weight-bold text-danger">${(item.product_name.length > 25) ? item.product_name.substring(0, 25) : item.product_name}</a></p>
                                    <div class="clearfix">
                                        <div class="d-inline-block">${
                                            (discount > 0) ? req.money(item.price_selling.toString(), "Rp ") + ""
                                            : req.money(price.toString(), "Rp ")
                                        }</div>
                                        <ul class="list-inline mb-0 mt-2">
                                            <li class="list-inline-item"><a href="javascript:void(0);" class="text-warning"><i class="fas fa-star"></i></a></li>
                                            <li class="list-inline-item"><a href="javascript:void(0);" class="text-warning"><i class="fas fa-star"></i></a></li>
                                            <li class="list-inline-item"><a href="javascript:void(0);" class="text-warning"><i class="fas fa-star"></i></a></li>
                                            <li class="list-inline-item"><a href="javascript:void(0);"><i class="fas fa-star"></i></a></li>
                                            <li class="list-inline-item"><a href="javascript:void(0);"><i class="fas fa-star"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        `);
                    })

                    setTimeout(() => {
                        lozad('.lozad-img-bg', {
                            load: function(el) {
                                el.style.backgroundImage = "url('"+el.getAttribute("data-img-bg")+"')";

                                for (var i = 0; i < el.childNodes.length; i++) {
                                    if (el.childNodes[i].className == "loader-item") {
                                        notes = el.childNodes[i];
                                        el.removeChild(notes);
                                        break;
                                    }        
                                }
                                
                            }
                        }).observe();
                    }, 1000);
                }

                $("#load-product").hide();
            }
            // ,
            // error: function(response) {
            //     console.log("ERROR", response);
            //     $("#btn-login").removeAttr("disabled");
            // }
        });
    }
</script>