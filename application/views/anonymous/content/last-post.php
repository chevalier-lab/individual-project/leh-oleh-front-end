<div class="d-flex justify-content-between align-items-center">
    <h4 class="card-title">Info Terbaru</h4>
</div>
<hr>

<div class="container-fluid mb-4">
    <div class="row" id="last-info"></div>
</div>

<script>
    $(window).on("load", function() {
        // Load Bank First Time
        loadLastInfo()
    });

    function loadLastInfo() {
        var raw = req.raw({
            page: 0,
            search: "",
            order_by: "id",
            order_direction: "DESC"
        })
        var formData = new FormData()
        formData.append("raw", raw)

        $.ajax({
            url: base_url.value + "/anonymous/posts/load_posts",
            data: formData,
            type: "GET",
            contentType: false,
            processData: false,
            success: function(response) {
                response = req.data(response)
                console.log(response)
                if (response.code == 200) {
                    var data = response.data

                    if (data.length > 0) {
                        isLoaded = false;
                    }

                    // Render Content
                    data.forEach(function(item) {
                        var info_location = base_url.value + '/anonymous/posts/detail/' + item.id;
                        $("#last-info").append(`
                        <div class="media d-block d-sm-flex text-center text-sm-left mb-4"
                            style="cursor: pointer"
                            onclick="location.assign('${info_location}')">
                            <div class="lozad-img-bg d-md-flex mr-sm-4"
                                data-img-bg="${item.uri}"
                                style="background-size: cover; height: 100px; 
                                background-position: center;
                                width: 100px;
                                onclick="location.assign('${info_location}')">
                                <div class="loader-item"></div>
                                </div>
                            
                            <div class="media-body align-self-center redial-line-height-1_5">
                                <h6 class="my-2 my-sm-0 redial-line-height-1_5 mb-xl-2">${item.title}</h6>
                                ${req.date(item.created_at)}
                            </div>
                        </div>
                        `);
                    })

                    setTimeout(() => {
                        lozad('.lozad-img-bg', {
                            load: function(el) {
                                el.style.backgroundImage = "url('"+el.getAttribute("data-img-bg")+"')";

                                for (var i = 0; i < el.childNodes.length; i++) {
                                    if (el.childNodes[i].className == "loader-item") {
                                        notes = el.childNodes[i];
                                        el.removeChild(notes);
                                        break;
                                    }        
                                }

                            }
                        }).observe();
                    }, 1000);
                }
            }
        });
    }
</script>