<div class="col-12 col-md-3">
    <div class="card mb-4">
        <div class="card-body">
            <div id="detail-merchant"></div>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="d-flex justify-content-between align-items-center">
                <h6>
                    <span class="icon-book-open"></span> Filter</h6>
            </div>

            <hr>

            <div class="form-group">
                <input type="text" name="_search" id="_search" class="form-control"
                placeholder="Nama, lokasi, atau kategori">
            </div>

            <div class="form-group">
                <button class="btn btn-danger btn-block"
                onclick="setToLoadProduct()">Terapkan</button>
            </div>
        </div>
    </div>
</div>
<div class="col-12 col-md-9">
    <div class="profile-menu mb-4 theme-background border  z-index-1 p-2">
        <div class="d-sm-flex">
            <div class="align-self-center">
                <p class="flex-column flex-sm-row m-2">Urutkan</p>
            </div>
            <div class="align-self-center">
                <ul class="nav nav-pills flex-column flex-sm-row" id="myTab" role="tablist">
                    <li class="nav-item ml-0">
                        <a class="nav-link  py-2 px-3 px-lg-4 bg-danger text-white mx-2" data-toggle="tab" href="#timeline"> Populer</a>
                    </li>
                    <li class="nav-item ml-0">
                        <a class="nav-link  py-2 px-4 px-lg-4 border round mx-2" data-toggle="tab" href="#about"> Terbaru</a>
                    </li>
                    <li class="nav-item ml-0">
                        <a class="nav-link py-2 px-4 px-lg-4 border round mx-2" data-toggle="tab" href="#activities">Terlaris </a>
                    </li>
                    <li class="nav-item ml-0 mb-2 mb-sm-0">
                        <select name="" id="" class="form-control">
                            <option value="">Dari A-Z</option>
                            <option value="">Dari Z-A</option>
                        </select>
                    </li>                                                        
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row" id="lists-products">
        </div>
    </div>
</div>

<script>

    var locations = [];
    var showFilterLocationMax = 2;
    var selected_location = [];
    loadLocation();

    loadMerchant();

    function setSelectedLocation(id) {
        var index = selected_location.indexOf(id)
        if (index != -1) {
            selected_location.splice(index, 1);
        } else {
            selected_location.push(id);
        }

        console.log(id, index, selected_location)

        setupFilterLocation();
    }

    function showMoreFilterLocation() {
        showFilterLocationMax = locations.length;
        setupFilterLocation()
    }

    var interval = setInterval(() => {
        if (categories.length > 0) {
            setupFilterCategory()
        }
    }, 1000);

    var showFilterCategoryMax = 2;
    var selected_category = [];

    function setSelectedCategory(id) {
        var index = selected_category.indexOf(id)
        if (index != -1) {
            selected_category.splice(index, 1);
        } else {
            selected_category.push(id);
        }

        setupFilterCategory();
    }

    function showMoreFilterCategory() {
        showFilterCategoryMax = categories.length;
        setupFilterCategory()
    }

    function setupFilterCategory() {
        clearInterval(interval)

        var filterCategory = '';

        categories.forEach(function(item, position) {
            if (position < showFilterCategoryMax) {
                var isThere = selected_category.includes(Number(item.id))
                if (isThere)
                filterCategory += `
                    <div class="form-group">
                        <label class="chkbox"> ${item.category}
                            <input value="${item.id}" class="form-check-input" type="checkbox"
                            onclick="setSelectedCategory(${item.id})"
                            checked>
                            <span class="checkmark"></span>
                        </label>
                    </div>
                `;
                else 
                filterCategory += `
                    <div class="form-group">
                        <label class="chkbox"> ${item.category}
                            <input value="${item.id}" class="form-check-input" type="checkbox"
                            onclick="setSelectedCategory(${item.id})">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                `;
            }
        });
        
        $("#filter-categories").html(filterCategory);
    }

    function setupFilterLocation() {
        var filterLocation = '';

        locations.forEach(function(item, position) {

            if (position < showFilterLocationMax) {
                var isThere = selected_location.includes(Number(item.id))
                if (isThere)
                filterLocation += `
                    <div class="form-group">
                        <label class="chkbox"> ${item.province_name}
                            <input value="${item.id}" class="form-check-input" type="checkbox"
                            onclick="setSelectedLocation(${item.id})" checked>
                            <span class="checkmark"></span>
                        </label>
                    </div>
                `;
                else 
                filterLocation += `
                    <div class="form-group">
                        <label class="chkbox"> ${item.province_name}
                            <input name="testing" value="${item.id}" class="form-check-input" type="checkbox"
                            onclick="setSelectedLocation(${item.id})">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                `;
            }
        });

        $("#filter-locations").html(filterLocation);
    }

    function loadLocation() {
        $.ajax({
            url: base_url.value + "/anonymous/landing/load_locations",
            data: null,
            type: "GET",
            contentType: false,
            processData: false,
            success: function(response) {
                response = req.data(response)
                if (response.code == 200) {
                    var data = response.data
                    locations = data;

                    setupFilterLocation()
                }
            }
        });
    }

    var page = 0;
    var order_by = "id";
    var order_direction = "DESC";

    var load_products_interval = setInterval(() => {
        if (categories.length > 0 && locations.length > 0) {
            loadProducts()
        }
    }, 1000);

    function setToLoadProduct() {
        page = 0;
        loadProducts()
    }

    function loadProducts() {

        clearInterval(load_products_interval)

        var formData = new FormData();
        formData.append("page", page)
        formData.append("_search", $("#_search").val())
        formData.append("order_by", order_by)
        formData.append("order_direction", order_direction)

        $.ajax({
            url: base_url.value + "/anonymous/landing/load_product_merchant/" + id_merchant.value,
            data: formData,
            type: "POST",
            contentType: false,
            processData: false,
            success: function(response) {
                response = req.data(response)
                if (response.code == 200) {
                    var data = response.data
                    
                    if (page > 0) {

                    } else {
                        $("#lists-products").html(`${data.map(function(item) {

                            var price = Number(item.price_selling)
                            var discount = Number(item.discount);
                            var price_divide_discount = ((price / 100) * discount);

                            price = (price - price_divide_discount)

                            if (discount > 0) {
                                price = Number(item.price_selling);
                            }

                            var product_location = base_url.value + "/anonymous/product/detail/" + item.id

                            return `
                            <div class="col-sm-6 col-md-4 col-lg-3 border py-3">
                                <div class="position-relative">
                                <div class="lozad-img-bg"
                                    data-img-bg="${item.uri}"
                                    style="background-size: cover; height: 150px; 
                                    background-position: center;
                                    onclick="location.assign('${product_location}')">
                                    <div class="loader-item"></div>
                                    </div>
                                    ${
                                        (discount > 0) ? `
                                        <div class="mb-3" style="top: 0px; position: absolute">
                                            <a href="javascript:void(0);" class="rounded-left bg-white px-3 py-2 text-danger">-${discount}%</a>
                                        </div>
                                        ` : ""
                                    }
                                    <div class="caption-bg fade bg-transparent text-right">
                                        <div class="d-table w-100 h-100 ">
                                            <div class="d-table-cell align-bottom">
                                                <div class="mb-3">
                                                    <a href="javascript:void(0);" class="rounded-left bg-white px-3 py-2 text-danger"><i class="icon-heart"></i></a>
                                                </div>
                                                <div class="mb-4">
                                                    <a href="javascript:void(0);" class="rounded-left bg-white px-3 py-2 text-danger"><i class="icon-bag"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="pt-3"
                                    onclick="location.assign('${product_location}')">
                                    <p class="mb-2"><a href="javascript:void(0);" class="font-weight-bold text-danger">${item.product_name}</a></p>
                                    <div class="clearfix">
                                        <div class="d-inline-block">${
                                            req.money(price.toString(), "Rp ")
                                        }</div>
                                        <ul class="list-inline mb-0 mt-2">
                                            <li class="list-inline-item"><a href="javascript:void(0);" class="text-danger"><i class="fas fa-star"></i></a></li>
                                            <li class="list-inline-item"><a href="javascript:void(0);" class="text-danger"><i class="fas fa-star"></i></a></li>
                                            <li class="list-inline-item"><a href="javascript:void(0);" class="text-danger"><i class="fas fa-star"></i></a></li>
                                            <li class="list-inline-item"><a href="javascript:void(0);"><i class="fas fa-star"></i></a></li>
                                            <li class="list-inline-item"><a href="javascript:void(0);"><i class="fas fa-star"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            `;
                        }).join('')}`);

                        setInterval(() => {
                            const observer = lozad();
                            observer.observe();
                        }, 2000);

                        setTimeout(() => {
                            lozad('.lozad-img-bg', {
                                load: function(el) {
                                    el.style.backgroundImage = "url('"+el.getAttribute("data-img-bg")+"')";
                                    
                                    for (var i = 0; i < el.childNodes.length; i++) {
                                        if (el.childNodes[i].className == "loader-item") {
                                            notes = el.childNodes[i];
                                            el.removeChild(notes);
                                            break;
                                        }        
                                    }
                                    
                                }
                            }).observe();
                        }, 1000);
                    }
                } else if (response.code == 404) {
                    $("#lists-products").html(`
                        <div class="alert alert-primary col-sm-12" role="alert">
                            Belum ada produk saat ini
                        </div>
                    `);
                }
            }
        });
    }

    function loadMerchant() {
        $.ajax({
            url: base_url.value + "/anonymous/landing/detail_merchants/" + id_merchant.value,
            data: null,
            type: "GET",
            contentType: false,
            processData: false,
            success: function(response) {
                response = req.data(response)
                if (response.code == 200) {
                    var data = response.data
                    $("#detail-merchant").html(`
                        <div style="height: 200px; background-size: cover; 
                        background-image: url('${data.uri}');"></div>
                        <div class="text-danger pt-2 h6"><strong>${data.market_name}</strong></div>
                        <div class="py-2">Nomor Telepon Toko: ${data.market_phone_number}</div>
                        <div>Alamat Toko: ${data.market_address}</div>
                    `);
                }
            }
        });
    }

</script>