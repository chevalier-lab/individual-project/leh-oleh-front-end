<div class="col-12">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12 col-xl-12 mb-4 mb-xl-0">
                    <div class="table-responsive mb-0">

                    <?php

                    $this->load->view('components/table', array(
                        "idTable" => "cart-list",
                        "isCustomThead" => true,
                        "thead" => '
                        <tr>
                            <th scope="col">Foto Produk</th>
                            <th scope="col">Nama Produk</th>
                            <th scope="col">Subtotal</th>
                            <th scope="col">Jumlah</th>
                            <th scope="col"></th>
                        </tr>
                        ',
                        "tbody" => ""
                    )) ?>

                    </div>

                </div>
                <div class="col-lg-12 col-xl-12">
                    <div class=" border mb-3">
                        <div class="card-body border border-top-0 border-right-0 border-left-0">
                            <h4 class="f-weight-500 mb-0">Total Keranjang</h4>
                        </div>
                        <div id="detail-pricing">
                        </div>
                    </div>
                    <div class="clearfix d-sm-flex">
                        <div class="float-right w-100 text-center text-sm-right">
                            <p class="mb-0 h6"><a href="javascript:void(0)" class="btn btn-danger"
                                id="process-to-checkout">
                                <i class="icon-handbag h6"></i> Bayar Sekarang</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>

<script>
    var cart = {};

    (function($) {
        "use strict";
        $(window).on("load", function() {
            // Load Cart
            loadCart()

            $("#process-to-checkout").on("click", function() {
                if (cart.carts.length > 0) {
                    location.assign(base_url.value + "/anonymous/cart/checkout")
                }
            })
        });
    })(jQuery);

    (function($) {
    $.fn.inputFilter = function(inputFilter) {
        return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
        if (inputFilter(this.value)) {
            this.oldValue = this.value;
            this.oldSelectionStart = this.selectionStart;
            this.oldSelectionEnd = this.selectionEnd;
        } else if (this.hasOwnProperty("oldValue")) {
            this.value = this.oldValue;
            this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
        } else {
            this.value = "";
        }
        });
    };
    }(jQuery));

    function loadCart() {
        $.ajax({
            url: base_url.value + "/anonymous/cart/load_cart_active",
            data: null,
            type: "GET",
            contentType: false,
            processData: false,
            success: function(response) {
                cart.carts = []
                response = req.data(response)
                if (response.code == 200) {
                    var data = response.data
                    data.forEach(function(merchant) {
                        merchant.products.forEach(function(product){
                            cart.carts.push(product)
                        })
                    })
                    setupCart();
                    // for (let j = 0; j < data.length; j++) {
                    //     $(`<div class="carousel-item"><img class="d-block w-100" src="${data[j].uri}"></div>`).appendTo('.carousel-inner');
                    //     $('<li data-target="#carouselExampleCaption" data-slide-to="' + j + '"></li>').appendTo('.carousel-indicators')
                    // }

                    // $('.carousel-item').first().addClass('active');
                    // $('.carousel-indicators > li').first().addClass('active');
                    // $('#carouselExampleCaption').carousel();
                }
            }
        });
    }

    function deleteCartItem(id) {
        $.ajax({
            url: base_url.value + "/anonymous/cart/delete_cart/" + id,
            data: null,
            type: "GET",
            contentType: false,
            processData: false,
            success: function(response) {
                response = req.data(response)
                if (response.code == 200) {
                    loadCart()
                    loadCartBadge()
                }
            }
        });
    }

    function changeQty(qty, id) {

        var position = cart.carts.findIndex(x => x.id == id)

        if (position > -1) {
            if (qty <= 0) {
                // Todo Delete Item
                
                deleteCartItem(id)
            } else {
                var discount = 0;
                // Todo Update QTY
                cart.carts[position].qty = qty;
                if(Number(cart.carts[position].discount) > 0 ){
                    discount = qty * (Number(cart.carts[position].price_selling) * Number(cart.carts[position].discount) / 100)
                }
                cart.carts[position].subTotal = (qty * cart.carts[position].price_selling) - discount;

                setupCart()

                var raw = req.raw({
                    qty: qty
                });

                var formData = new FormData()
                formData.append('raw', raw);

                $.ajax({
                    url: base_url.value + "/anonymous/cart/change_qty/" + id,
                    data: formData,
                    type: "POST",
                    contentType: false,
                    processData: false,
                    success: function(response) {
                        response = req.data(response)
                        if (response.code == 200) {
                            
                        }
                    }
                });
            }
        }
    }

    function setupCart() {
        if (cart.carts.length > 0) {
            // Todo Display Cart Item
            $("#cart-list tbody").html(`${cart.carts.map(function(item) {
                return `
                    <tr>
                        <td><div class="lozad-img-bg rounded"
                        data-img-bg="${item.uri}"
                        style="background-size: cover; height: 80px; width: 80px; 
                        background-position: center;">
                        <div class="loader-item"></div>
                        </div></td>
                        <td class="align-middle">${item.product_name}</td>
                        <td class="align-middle">${req.money(Number(item.subTotal).toString(), "Rp ")}</td>
                        <td class="w-25 align-middle">
                            <input type="number" class="form-control qty-control" 
                            value="${Number(item.qty)}"
                            onchange="changeQty(this.value, ${item.id})">
                        </td>
                        <td class="align-middle"><a href="javascript:void(0)"
                        onclick="deleteCartItem(${item.id})"><i class="icon-trash"></i></a></td>
                    </tr>
                `;
            }).join('')}`)

            setTimeout(() => {
                lozad('.lozad-img-bg', {
                    load: function(el) {
                        el.style.backgroundImage = "url('"+el.getAttribute("data-img-bg")+"')";

                        for (var i = 0; i < el.childNodes.length; i++) {
                            if (el.childNodes[i].className == "loader-item") {
                                notes = el.childNodes[i];
                                el.removeChild(notes);
                                break;
                            }        
                        }
                        
                    }
                }).observe();
            }, 1000);

            setupPricing()
            
        } else {
            // Display Total Only
            $("#cart-list tbody").html(`
                <tr>
                    <td colspan="5">
                        <center>Belum ada barang di keranjang</center>
                    </td>
                </tr>
            `);

            $("#detail-pricing").html(``);
        }
    }

    function setupPricing() {

        var subTotal = 0;

        cart.carts.forEach(function(item) {
            subTotal += item.subTotal
        })

        $("#detail-pricing").html(`
            <div class="card-body border border-top-0 border-right-0 border-left-0">
                <div class="clearfix">
                    <div class="float-left">
                        <p class="mb-0 dark-color f-weight-600">Total : </p>
                    </div>
                    <div class="float-right">
                        <p class="mb-0 dark-color f-weight-600 h4">${req.money(subTotal.toString(), "Rp ")}</p>
                    </div>
                </div>
            </div>
        `);
    }
</script>