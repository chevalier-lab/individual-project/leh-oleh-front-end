<div class="col-12 col-sm-12 row" id="posts-body">

</div>

<div id="devider-load-content"></div>
<div id="load-content" class="mb-4">
    <center>
        Sedang memuat konten...
    </center>
</div>

<script>
    // Initial Pagination
    var page = -1;
    var isLoaded = false;
    var paginationClicked = false;
    var paginationDirection = "";

    (function($) {
        "use strict";

        $(document).scrollTop(0);

        // $(window).on("load", function() {
        //     // Load Content First Time
        //     $("#load-content").show();
        //     console.log("pertama")
        //     loadPosts()
        // });

        if (
            "IntersectionObserver" in window &&
            "IntersectionObserverEntry" in window &&
            "intersectionRatio" in window.IntersectionObserverEntry.prototype
        ) {
            let observer = new IntersectionObserver(entries => {
                if (entries[0].boundingClientRect.y < 300) {
                    if (isLoaded == false) {
                        page++;
                        console.log(page)
                        isLoaded = true;
                        $("#load-content").show();
                        loadPosts();
                    }
                }
            });
            observer.observe(document.querySelector("#devider-load-content"));
        }
    })(jQuery);

    function loadPosts() {
        var raw = req.raw({
            page: page,
            search: "",
            order_by: "id",
            order_direction: "DESC"
        })
        var formData = new FormData()
        formData.append("raw", raw)

        $.ajax({
            url: base_url.value + "/anonymous/posts/load_posts",
            data: formData,
            type: "GET",
            contentType: false,
            processData: false,
            success: function(response) {
                response = req.data(response)
                console.log(response)
                if (response.code == 200) {
                    var data = response.data

                    if (data.length > 0) {
                        isLoaded = false;
                    }

                    // Render Content
                    data.forEach(function(item) {
                        var info_location = base_url.value + '/anonymous/posts/detail/' + item.id;
                        $("#posts-body").append(`
                        <div class="col-12 col-sm-6 col-xl-3 mb-4 border py-3"
                            style="cursor: pointer"
                            onclick="location.assign('${info_location}')">
                            <div class="card border-0">
                                <div class="lozad-img-bg"
                                    data-img-bg="${item.uri}"
                                    style="background-size: cover; height: 150px; 
                                    background-position: center;
                                    onclick="location.assign('${info_location}')">
                                    <div class="loader-item"></div>
                                    </div>

                                <div class="pt-3">
                                    <p class="mb-2 h6"><a href="javascript:void(0);" class="font-weight-bold text-danger">${(item.title.length > 42) ? item.title.substring(0, 42) + "..." : item.title}</a></p>
                                    
                                    ${req.date(item.created_at)}
                                    <div class="clearfix"></div>
                                    <a href="${info_location}" class="btn btn-danger mt-2">Read More</a>
                                </div>
                            </div>
                        </div>
                        `);
                    })

                    setTimeout(() => {
                        lozad('.lozad-img-bg', {
                            load: function(el) {
                                el.style.backgroundImage = "url('"+el.getAttribute("data-img-bg")+"')";

                                for (var i = 0; i < el.childNodes.length; i++) {
                                    if (el.childNodes[i].className == "loader-item") {
                                        notes = el.childNodes[i];
                                        el.removeChild(notes);
                                        break;
                                    }        
                                }
                                
                            }
                        }).observe();
                    }, 1000);
                }
                $("#load-content").hide();
            }
        });
    }
</script>