<div class="row" id="load-detail-order">
</div>

<div class="my-4" id="load-tracking-order"></div>
<input type="hidden" id="redirect_url" value="<?= REDIRECT_MIDTRANS; ?>">

<div class="mb-4">

    <div class="row">
        <div class="col-lg-12 col-xl-8 table-responsive border pt-3">
            <?php

            $this->load->view('components/table', array(
                "idTable" => "cart-products",
                "isCustomThead" => true,
                "thead" => '
                    <tr>
                        <th scope="col" colspan="2">Produk</th>
                        <th scope="col" class="text-right">Jumlah</th>
                        <th scope="col" class="text-right">Total Harga</th>
                        <th scope="col">Aksi</th>
                    </tr>
                ',
                "tbody" => ""
            )) ?>
        </div>

        <div class="col-lg-12 col-xl-4 col-md-3 border">
            <div class=" mb-3">
                <div class="card-body border border-top-0 border-right-0 border-left-0">
                    <div class="clearfix">
                        <div class="float-left">
                            <p class="mb-0 dark-color f-weight-600">Subtotal : </p>
                        </div>
                        <div class="float-right">
                            <p class="mb-0 dark-color f-weight-600 h4" id="subtotal"></p>
                        </div>
                    </div>
                </div>

                <div class="card-body border border-top-0 border-right-0 border-left-0">
                    <div class="clearfix">
                        <div class="float-left">
                            <p  class="mb-0 dark-color f-weight-600">Biaya Pengiriman : </p>
                        </div>
                        <div class="float-right">
                            <p class="mb-0 dark-color f-weight-600 h4" id="additional"></p>
                        </div>
                    </div>
                </div>

                <div class="card-body border border-top-0 border-right-0 border-left-0">
                    <div class="clearfix">
                        <div class="float-left">
                            <p class="mb-0 dark-color f-weight-600">Total : </p>
                        </div>
                        <div class="float-right">
                            <p class="mb-0 dark-color f-weight-600 h4" id="final-price"></p>
                        </div>
                    </div>
                </div>
                <!-- <div class="card-body border border-top-0 border-right-0 border-left-0">
                    <h4 class="f-weight-500 mb-0">Total Harga</h4>
                </div>
                <div id="detail-pricing">
                </div> -->
            </div>
        </div>
    </div>

</div>

<?php
    // Detail Top Up Proof Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-detail-transaction",
        "modalTitle" => "Detail Transaksi",
        "modalType" => "modal-md",
        "iconTitle" => "icon-eye",
        "modalActionForm" => "#",
        "modalContentForm" => '
            <div class="row" id="detail-transaction"></div>
        ',
        "modalButtonForm" => ''
    ));

    // Refund Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-refund",
        "modalTitle" => "Batalkan Transaksi",
        "modalType" => "modal-md",
        "iconTitle" => "icon-info",
        "modalActionForm" => "#",
        "modalContentForm" => '
            <input type="hidden" id="id-transaction" />
            <p>Apakah anda yakin ingin membatalkan transaksi ini ?</p>
        ',
        "modalButtonForm" => '
        <button type="button" class="btn btn-danger" id="refund-transaction">Ya</button>
        <button type="button" class="btn btn-light" data-dismiss="modal">Tidak</button>
        '
    ));
?>

<script>
    var disabled = 'disabled';

    (function($) {
        "use strict";
        $(window).on("load", function() {
            loadDetailOrder()

            // Handle Refund Transaction
            $("#refund-transaction").on("click", function() {
                $("#refund-transaction").attr("disabled", true);

                var raw = req.raw({
                    idTransaction: $('#id-transaction').val()
                })

                var formData = new FormData()
                formData.append("raw", raw)

                $.ajax({
                    url: base_url.value + "/anonymous/order/request_refund",
                    data: formData,
                    type: "POST",
                    contentType: false,
                    processData: false,
                    success: function(response) {
                        $("#refund-transaction").removeAttr("disabled");
                        $("#modal-refund").modal('hide')

                        response = req.data(response)
                        if (response.code == 200) {
                            toastr.success("Sukses mengajukan pembatalan transaksi");
                            setTimeout(() => {
                                location.assign(base_url.value + "/anonymous/order")
                            }, 1000);
                        } else {
                            toastr.error(response.message);
                        }
                    }
                });
            });
        })
    })(jQuery);

    function loadDetailOrder() {
        renderToWaiting()

        if (order_id.value == "") {
            renderDetail(null)
            return
        }

        $.ajax({
            url: base_url.value + "/anonymous/order/detail_order/" + order_id.value,
            data: null,
            type: "GET",
            contentType: false,
            processData: false,
            success: function(response) {
                response = req.data(response)
                var subTotal = Number(response.data.products[0].total_price)
                var additional = Number(response.data.additional.additional)
                var total = subTotal + additional

                $('#subtotal').html(req.money(Number(subTotal).toString(), "Rp "))
                $('#additional').html(req.money(Number(additional).toString(), "Rp "))
                $('#final-price').html(req.money(Number(total).toString(), "Rp "))
                if (response.code == 200 && response.data.info != null) {
                    renderDetail(response.data)
                } else {
                    renderDetail(null)
                }
            }
        });
    }

    function renderToWaiting() {
        $("#preview-post-content").html(`
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">Harap menunggu, sedang memuat halaman..</div>
                    </div>
                </div>
            `);
    }

    function renderDetail(data) {
        if (data == null) {
            $("#load-detail-order").html(`
                    <div class="col-12">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <strong>Oops!</strong> Maaf halaman yang anda cari sudah dihapus, atau tidak ditemukan.
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    </div>
                `);
        } else {
            var status = Number(data.status)
            var statusRefund = Number(data.is_request_refund)

            if(statusRefund == 1 && status != 4){
                status = `<span class='badge badge-info'>Mengajukan pembatalan</span>`
            } else {   
                if (status == 0) status = `<span class='badge badge-secondary'>Belum dibayar</span><br>
                <div class="m-3">
                <a href="${$('#redirect_url').val() + data.info.token_payment}" class='btn btn-primary btn-block'
                target="_blank">Bayar Tagihan</a>
                </div>`;
                else if (status == 1) status = "<span class='badge badge-primary'>Pembayaran terverifikasi</span>";
                else if (status == 2) status = "<span class='badge badge-warning'>Pesanan diproses</span>";
                else if (status == 3) status = "<span class='badge badge-success'>Pesanan selesai</span>";
                else if (status == 4) status = "<span class='badge badge-danger'>Pesanan dibatalkan</span>";
            }

            $("#load-detail-order").html(`
            <div class="col-12 col-md-4 mt-3 card rounded-0 p-0">
                <div class="card-header justify-content-between align-items-center">
                    <h4 class="card-title">Informasi Toko</h4>
                </div>
                <div class="card-body p-0">
                    <div class="list-group list-group-flush mt-2">
                    <a href="javascript:void(0)" class="list-group-item flex-column align-items-start border-0 py-1">
                            <div class="d-flex w-100 justify-content-between">
                            <strong class="mb-0">Nama Merchant:</strong>
                            </div>
                            <small>${data.merchant_info.market_name}</small>
                        </a>
                        <a href="javascript:void(0)" class="list-group-item flex-column align-items-start border-0 py-1">
                            <div class="d-flex w-100 justify-content-between">
                            <strong class="mb-0">Nomor Telepon:</strong>
                            </div>
                            <small>${data.merchant_info.market_phone_number}</small>
                        </a>
                        <a href="javascript:void(0)" class="list-group-item flex-column align-items-start border-0 py-1">
                            <div class="d-flex w-100 justify-content-between">
                            <strong class="mb-0">Alamat:</strong>
                            </div>
                            <small>${data.merchant_info.location.address + ", " + data.merchant_info.location.area + 
                            ", " + data.merchant_info.location.suburb + ", " + data.merchant_info.location.city + ", " + 
                            data.merchant_info.location.province}</small>
                        </a>
                        <a href="javascript:void(0)" class="list-group-item flex-column align-items-start border-0 py-1">
                            <div class="d-flex w-100 justify-content-between">
                            <strong class="mb-0">Kode pos:</strong>
                            </div>
                            <small>${data.merchant_info.location.postcode}</small>
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-12 col-md-4 mt-3 card rounded-0 p-0">
                <div class="card-header justify-content-between align-items-center">
                    <h4 class="card-title">Informasi Pelanggan</h4>
                </div>
                <div class="card-body p-0">
                    <div class="list-group my-2">
                        <a href="javascript:void(0)" class="list-group-item flex-column align-items-start border-0 py-1">
                            <div class="d-flex w-100 justify-content-between">
                            <strong class="mb-0">Nama:</strong>
                            </div>
                            <small>${data.info.full_name}</small>
                        </a>
                        <a href="javascript:void(0)" class="list-group-item flex-column align-items-start border-0 py-1">
                            <div class="d-flex w-100 justify-content-between">
                            <strong class="mb-0">Nomor Telepon:</strong>
                            </div>
                            <small>${data.info.phone_number}</small>
                        </a>
                        <a href="javascript:void(0)" class="list-group-item flex-column align-items-start border-0 py-1">
                            <div class="d-flex w-100 justify-content-between">
                            <strong class="mb-0">Alamat:</strong>
                            </div>
                            <small>${data.info.location.address + ", " + data.info.location.area + 
                            ", " + data.info.location.suburb + ", " + data.info.location.city + ", " + 
                            data.info.location.province}</small>
                        </a>
                        <a href="javascript:void(0)" class="list-group-item flex-column align-items-start border-0 py-1">
                            <div class="d-flex w-100 justify-content-between">
                            <strong class="mb-0">Kode pos:</strong>
                            </div>
                            <small>${data.info.location.postcode}</small>
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-12 col-md-4 mt-3 card rounded-0 p-0">
                <div class="card-header justify-content-between align-items-center">
                    <h4 class="card-title">Informasi Transaksi</h4>
                </div>
                <div class="card-body p-0">
                    <div class="list-group my-2">
                        <a href="javascript:void(0)" class="list-group-item flex-column align-items-start border-0 py-1">
                            <div class="d-flex w-100 justify-content-between">
                            <strong class="mb-0">TRX ID:</strong>
                            </div>
                            <small>${data.transaction_token}</small>
                        </a>
                        <a href="javascript:void(0)" class="list-group-item flex-column align-items-start border-0 py-1">
                            <div class="d-flex w-100 justify-content-between">
                            <strong class="mb-0">Nama Akun:</strong>
                            </div>
                            <small>${data.full_name}</small>
                        </a>
                        <a href="javascript:void(0)" class="list-group-item flex-column align-items-start border-0 py-1">
                            <div class="row">
                                <div class="col">
                                    <div class="d-flex w-100 justify-content-between">
                                    <strong class="mb-0">Tanggal Pesan:</strong>
                                    </div>
                                    <small>${data.created_at}</small>
                                </div>
                                <div class="col">
                                    <div class="d-flex w-100 justify-content-between">
                                    <strong class="mb-0">Tanggal Perubahan:</strong>
                                    </div>
                                    <small>${data.updated_at}</small>
                                </div>
                            </div>
                        </a>
                        <a href="javascript:void(0)" class="list-group-item flex-column align-items-start border-0 py-1">
                            <div class="d-flex w-100 justify-content-between">
                            <strong class="mb-0">Status:</strong>
                            </div>
                            <small>${status}</small>
                        </a>
                    </div>
                </div>
            </div>

            `);

            setupTracking(data);

            setupProduct(data);

            checkIsAlreadyPayment(data);
        }
    }

    function checkIsAlreadyPayment(data) {
        var status = Number(data.status)
        if (status == 0) {
            $.ajax({
                url: base_url.value + "/anonymous/order/get_status_payment/" + data.transaction_token,
                data: null,
                type: "GET",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.status_code == "200") {
                        if (response.transaction_status == "settlement") {
                            // Do Update Status
                            doUpdateStatus(data)
                        }
                    }
                }
            });
        }
    }

    function doUpdateStatus(data) {
        data.products.forEach(function(item, position) {
            var raw = req.raw({
                status: 1
            })

            var formData = new FormData()
            formData.append("raw", raw)

            if (item.status == "0" || item.status == 0) {
                $.ajax({
                    url: base_url.value + "/anonymous/order/update_status_transaction/" + item.id,
                    data: formData,
                    type: "POST",
                    contentType: false,
                    processData: false,
                    success: function(response) {
                        response = req.data(response)
                        
                        if (position == (data.products.length - 1)) 
                        loadDetailOrder()
                    }
                });
            }
        });
    }

    function setupTracking(data) {
        
        var status = Number(data.status)

        var shipper = data.products[0].info;
        $.ajax({
            url: base_url.value + "/services/shipper/orders/load_order_detail/" + shipper.shipper_order_id,
            data: null,
            type: "GET",
            contentType: false,
            processData: false,
            success: function(response) {
                response = req.data(response)
                if (response.status == "success") {
                    var data = response.data;
                    if (data.statusCode == 200) {
                        var status = data.order.tracking[(data.order.tracking.length - 1)].logisticStatus
                        $("#detail-transaction").html(`
                        <div class="list-group my-2">
                            ${status.map(function(item) {
                                return `
                                <a href="javascript:void(0)" class="list-group-item flex-column align-items-start border-0 py-1">
                                    <div class="d-flex w-100 justify-content-between">
                                    <strong class="mb-0">${item.name}</strong>
                                    </div>
                                    <small>${item.description}</small>
                                </a>
                                `;
                            }).join('')}
                        </div>
                        `);
                    }
                }
            }
        });

        $("#load-tracking-order").html(`
            <div class="row">
                <div class="col-12 card rounded-0 p-0">

                    <div class="card-header">
                        <div class="d-flex justify-content-between align-items-center">
                            <h4 class="card-title">Traking Transaksi</h4>
                            <a href="javascript:void(0)" class="bg-danger rounded text-white text-center"
                            data-toggle="modal" data-target="#modal-detail-transaction">
                                <div class="p-2">
                                    <span>Lihat Detail</span>
                                    <i class="icon-arrow-right-circle align-middle text-white"></i>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="card-body">

                        <div class="wizard mb-4">

                            <div class="connecting-line"></div>
                            <ul class="nav nav-tabs d-flex mb-3">
                                <li class="nav-item mr-auto">
                                    <a class="nav-link position-relative round-tab text-left p-0 border-0 ${(status >= 0) ? 'active' : ''}">
                                        <i class="icon-handbag position-relative text-white h5 mb-3"></i>
                                        <small class="d-none d-md-block ">Pesanan Diterima</small>
                                    </a>
                                </li>
                                <li class="nav-item mx-auto">
                                    <a class="nav-link position-relative round-tab text-sm-center text-left p-0 border-0 ${(status >= 1) ? 'active' : ''}">
                                        <i class="icon-credit-card position-relative text-white h5 mb-3"></i>
                                        <small class="d-none d-md-block">Pembayaran Terverifikasi</small>
                                    </a>
                                </li>
                                <li class="nav-item mx-auto">
                                    <a class="nav-link position-relative round-tab text-sm-center text-left p-0 border-0 ${(status >= 2) ? 'active' : ''}">
                                        <i class="icon-present position-relative text-white h5 mb-3"></i>
                                        <small class="d-none d-md-block">Pesanan Diproses</small>
                                    </a>
                                </li>
                                <li class="nav-item mx-auto">
                                    <a class="nav-link position-relative round-tab text-sm-center text-left p-0 border-0 ${(status == 3) ? 'active' : ''}">
                                        <i class="icon-check position-relative text-white h5 mb-3"></i>
                                        <small class="d-none d-md-block">Pesanan Selesai</small>
                                    </a>
                                </li>
                                ${(status == 4) ? `
                                    <li class="nav-item ml-auto">
                                        <a class="nav-link position-relative round-tab text-sm-center text-left p-0 border-0 active">
                                            <i class="icon-close position-relative text-white h5 mb-3"></i>
                                            <small class="d-none d-md-block">Pesanan Dibatalkan</small>
                                        </a>
                                    </li>
                                ` : ``}
                            </ul>

                        </div>
                        ${status == 1 && data.is_request_refund == 0 ? setButtonRefund(data.id) : ''}
                    </div>
                </div>
            </div>
        `);
    }

    function setupProduct(data = []) {
        if (data.products.length > 0) {
            // Todo Display Product Item
            $("#cart-products tbody").html(`${data.products.map(function(item) {
                var status = Number(item.status)
                if (status == 3) disabled = ''
                return `
                    <tr>
                        <td class="text-center"><img data-src="${item.uri}" 
                        src="${base_url.value + '/../assets/dist/images/broken-256.png'}"
                        alt="" class="img-fluid lozad" width="48"></td>
                        <td>${item.product_name}</td>
                        <td class="text-right">x${item.qty}</td>
                        <td class="text-right">${req.money(Number(item.sub_total_price).toString(), "Rp ")}</td>
                        <td>
                            ${(status == 3) ? `
                                <a href="javascript:void(0);" data-toggle="modal" data-target="#modal-review" class="btn btn-success btn-block" onclick="loadIdProduct(${item.id})">Ulasan</a>
                                <a href="javascript:void(0);" data-toggle="modal" data-target="#modal-complain" class="btn btn-danger btn-block" onclick="loadIdProduct(${item.id})">Komplain</a>
                            ` : `
                                <a href="${base_url.value + '/anonymous/product/detail/' + item.id_m_products}" class="btn btn-primary btn-block">Beli Lagi</a>
                            `}
                        </td>
                    </tr>
                `;
            }).join('')}`)

            setInterval(() => {
                const observer = lozad();
                observer.observe();
            }, 2000);

            $("#detail-pricing").html(`
            <div class="card-body border border-top-0 border-right-0 border-left-0">
                <div class="clearfix">
                    <div class="float-left">
                        <p class="mb-0 dark-color f-weight-600">Total : </p>
                    </div>
                    <div class="float-right">
                        <p class="mb-0 dark-color f-weight-600 h4">${req.money(data.total_price.toString(), "Rp ")}</p>
                    </div>
                </div>
            </div>
        `);

        } else {
            // Display Total Only
            $("#cart-products tbody").html(`
                <tr>
                    <td colspan="5">
                        <center>Error mengambil data produk</center>
                    </td>
                </tr>
            `);

            $("#detail-pricing").html(``);
        }
    }

    function loadIdProduct(idBank) {
        $("#id-product-review").val(idBank)
        $("#id-product-complain").val(idBank)
    }

    function setButtonRefund(id){
        $('#id-transaction').val(id)
        return `
            <div class="card-footer">
                <a href="javascript:void(0);" data-toggle="modal" data-target="#modal-refund" class="btn btn-info btn-block">Batalkan Transaksi</a>
            </div>
        `
    }
</script>