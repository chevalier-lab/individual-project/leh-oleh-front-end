<div class="row p-4">
    <div class="col-12 col-md-3" id="setting-footer">
    </div>
    <div class="col-12 col-md-3">
        <ul class="nav flex-column">
            <li class="nav-item">
                <strong class="nav-link text-danger" href="javascript:void(0);">LAYANAN PELANGGAN</strong>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= base_url('index.php/anonymous/pages/contact'); ?>">Kontak Kami</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="javascript:void(0);">Cara Belanja</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="https://faq.shipper.id/">Pengiriman Barang</a>
            </li>
        </ul>
    </div>
    <div class="col-12 col-md-3">
        <ul class="nav flex-column" id="list-categories-foot">
            <li class="nav-item">
                <strong class="nav-link text-danger" href="javascript:void(0);">KATEGORI PRODUK</strong>
            </li>
        </ul>
    </div>
    <div class="col-12 col-md-3">
        <ul class="nav flex-column" id="list-sosmed">
            <li class="nav-item">
                <strong class="nav-link text-danger" href="javascript:void(0);">MEDIA SOSIAL</strong>
            </li>
        </ul>
    </div>

    <div class="col-12 col-md-12">
        <strong class="text-danger" href="javascript:void(0);">PEMBAYARAN YANG DIDUKUNG</strong>
        <div id="list-banks" class="mt-2"></div>
    </div>
</div>

<hr>

<div class="row p-4">
    <div class="col-12">
        <div class="profile-menu z-index-1 p-0">
            <div class="d-sm-flex">
                <div class="align-self-center">
                    <p>
                        &copy; Leh-oleh 2020. Hak Cipta Dilindungi
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var sosmed = [];
    var categoriesFoot = [];
    var banks = [];
    
    (function($) {
        "use strict";
        $(window).on("load", function() {
            // Load Banner
            loadSocialMedia()

            loadCategoriesFooter()

            loadBanks()

            loadSettings()
        });
    })(jQuery);

    function loadSocialMedia() {
        $.ajax({
            url: base_url.value + "/services/contents/load_sosmed",
            data: null,
            type: "GET",
            contentType: false,
            processData: false,
            success: function(response) {
                response = req.data(response)
                if (response.code == 200) {
                    var data = response.data
                    sosmed = data;
                    sosmed.forEach(function(item) {
                        $("#list-sosmed").append(`
                        <li class="nav-item">
                            <a class="nav-link" href="${item.uri}">
                                <i class="${item.icon}"></i>
                                ${item.social_media}
                            </a>
                        </li>
                        `);
                    });
                }
            }
        });
    }

    function loadCategoriesFooter() {
        $.ajax({
            url: base_url.value + "/anonymous/landing/load_categories",
            data: null,
            type: "GET",
            contentType: false,
            processData: false,
            success: function(response) {
                response = req.data(response)
                if (response.code == 200) {
                    var data = response.data
                    categoriesFoot = data;
                    categoriesFoot.forEach(function(item) {
                        $("#list-categories-foot").append(`
                        <li class="nav-item">
                            <a class="nav-link" href="${base_url.value}/anonymous/landing/set_category_product_list/${item.id}">
                                <i class="${item.icon}"></i>
                                ${item.category}
                            </a>
                        </li>
                        `);
                    });
                }
            }
        });
    }

    function loadBanks() {
        $.ajax({
            url: base_url.value + "/services/contents/load_banks",
            data: null,
            type: "GET",
            contentType: false,
            processData: false,
            success: function(response) {
                response = req.data(response)
                if (response.code == 200) {
                    var data = response.data
                    banks = data;
                    banks.forEach(function(item) {
                        $("#list-banks").append(`
                        <a href="javascript:void(0)">
                            <img src="${item.logo}" 
                            style="max-height: 36px;"
                            class="border border-secondary rounded p-1" />
                        </a>
                        `);
                    });
                }
            }
        });
    }

    function loadSettings() {
        $.ajax({
            url: base_url.value + "/services/contents/load_setting",
            data: null,
            type: "GET",
            contentType: false,
            processData: false,
            success: function(response) {
                response = req.data(response)
                if (response.code == 200) {
                    var data = response.data
                    $("#setting-footer").html(`
                    <div>
                        <img src="${data.uri}" alt="" style="max-width: 100%">
                    </div>
                    <p class="mt-3">
                        ${data.description}
                        <br>
                        <br>
                        <strong>Kontak Kami</strong><br>
                        No Telp / WA : ${data.phone_number}<br>
                        Email : ${data.email}
                        <br>
                        <br>
                        <strong>Lokasi Kami</strong><br>
                        ${data.address}
                    </p>
                    `);
                }
            }
        });
    }
</script>