<div class="col-12">                          
    <div class="card"> 
        <div class="card-body">
            <iframe class="w-100 height-350 border-0" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15841.301954849358!2d107.589227!3d-6.9708734!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x8ba686aebe0abb49!2sPT.%20Leholeh%20Rasa%20Nusantara!5e0!3m2!1sen!2sid!4v1608547029054!5m2!1sen!2sid" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
        </div>                                
    </div>
</div>

<div class="col-12 col-md-6 mt-4">                          
    <div class="card"> 
        <div class="card-header  justify-content-between align-items-center">                               
            <h4 class="card-title">Beri Kami Masukan</h4> 
        </div>
        <div class="card-body">
            <form>
                <div class="form-group">
                    <input id="full-name" type="text" class="form-control" placeholder="Nama:">
                </div>
                <div class="form-group">
                    <input id="email" type="text" class="form-control" placeholder="Email:">
                </div>
                <div class="form-group">
                    <textarea id="feedback" class="form-control" placeholder="Masukan:"></textarea>
                </div>

                <button type="button" class="btn btn-danger" id="send-button">Kirim
                <i class="icon-paper-plane"></i>
                </button>
            </form>
        </div>                                
    </div>
</div>
<div class="col-12 col-md-6 mt-4">                          
    <div class="card"> 
        <div class="card-header  justify-content-between align-items-center">                               
            <h4 class="card-title">Kontak Kami</h4> 
        </div>
        <div class="card-body">
            <div id="setting-contact"></div>
            <div class="text-left" id="list-social-media-contact">
            </div>
        </div>                         
    </div>
</div>

<script>
    var sosmedContact = [];
    
    (function($) {
        "use strict";
        $(window).on("load", function() {
            // Load Banner
            loadSocialMediaContact()

            loadSettingsContact()
        });
    })(jQuery);

    function loadSocialMediaContact() {
        $.ajax({
            url: base_url.value + "/services/contents/load_sosmed",
            data: null,
            type: "GET",
            contentType: false,
            processData: false,
            success: function(response) {
                response = req.data(response)
                if (response.code == 200) {
                    var data = response.data
                    sosmedContact = data;
                    sosmedContact.forEach(function(item) {
                        $("#list-social-media-contact").append(`
                        <a href="${item.uri}" class="btn btn-social btn-danger text-white mb-2">
                            <i class="${item.icon}"></i>
                        </a>
                        `);
                    });
                }
            }
        });

        // Handle Edit Feedback
        $("#send-button").on("click", function() {
            var fullName = $("#full-name").val()
            var email = $("#email").val()
            var feedback = $("#feedback").val()
            if(fullName.trim().length != 0 && email.trim().length != 0 && feedback.trim().length != 0){
                $("#send-button").attr("disabled", true);

                var raw = req.raw({
                    full_name: fullName,
                    email: email,
                    message: feedback,
                })

                var formData = new FormData();
                formData.append("raw", raw)

                $.ajax({
                    url: base_url.value + "/anonymous/pages/create_feedback",
                    data: formData,
                    type: "POST",
                    contentType: false,
                    processData: false,
                    success: function(response) {
                        response = req.data(response)

                        $("#send-button").removeAttr("disabled");

                        if (response.code == 200) {
                            toastr.success("Sukses Mengirim Masukan");
                            $("#full-name").val("")
                            $("#email").val("")
                            $("#feedback").val("")
                        } else {
                            toastr.error(response.message);
                        }
                    }
                });
            } else {
                toastr.error("Silahkan isi semua formulir yang tersedia");
            }
        });
    }

    function loadSettingsContact() {
        $.ajax({
            url: base_url.value + "/services/contents/load_setting",
            data: null,
            type: "GET",
            contentType: false,
            processData: false,
            success: function(response) {
                response = req.data(response)
                if (response.code == 200) {
                    var data = response.data
                    $("#setting-contact").html(`
                    <p>
                        ${data.description}
                        <br>
                        <br>
                        <strong>Kontak Kami</strong><br>
                        No Telp / WA : ${data.phone_number}<br>
                        Email : ${data.email}
                        <br>
                        <br>
                        <strong>Lokasi Kami</strong><br>
                        ${data.address}
                    </p>
                    `);
                }
            }
        });
    }
</script>