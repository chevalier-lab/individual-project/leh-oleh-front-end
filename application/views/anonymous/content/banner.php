<div class="col-12">
    <?php
        $this->load->view('components/general/slider');
    ?>
</div>
<script>
    (function($) {
        "use strict";
        $(window).on("load", function() {
            // Load Banner
            loadBanner()
        });
    })(jQuery);

    var banner = [];

    function loadBanner() {
        $.ajax({
            url: base_url.value + "/anonymous/landing/load_banner",
            data: null,
            type: "GET",
            contentType: false,
            processData: false,
            success: function(response) {
                response = req.data(response)
                if (response.code == 200) {
                    var data = response.data
                    banner = data;
                    setupBanner();
                    // for (let j = 0; j < data.length; j++) {
                    //     $(`<div class="carousel-item"><img class="d-block w-100" src="${data[j].uri}"></div>`).appendTo('.carousel-inner');
                    //     $('<li data-target="#carouselExampleCaption" data-slide-to="' + j + '"></li>').appendTo('.carousel-indicators')
                    // }

                    // $('.carousel-item').first().addClass('active');
                    // $('.carousel-indicators > li').first().addClass('active');
                    // $('#carouselExampleCaption').carousel();
                }
            }
        });
    }

    function setupBanner() {
        $('.owl-carousel').html(`${banner.map(function(item) {
            return `
                <img class="owl-lazy" data-src="${item.uri}" alt="">
            `;
        }).join('')}`);

        $('.owl-carousel').owlCarousel({
            items: 1,
            lazyLoad: true,
            lazyLoadEager: 1,
            loop: true,
            margin: 10,
            autoHeight: true,
            autoplay: true,
            autoplayTimeout: 10000,
            autoplayHoverPause: true,
            stagePadding: 50,
            nav: false,
            dots: false
        });
    }
</script>