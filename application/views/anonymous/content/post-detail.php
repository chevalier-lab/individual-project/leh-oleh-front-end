<div class="col-12 col-md-8 col-sm-12" id="preview-post-content">
    
</div>


<script>
    (function($) {
        "use strict";
        $(window).on("load", function() {
            // Load Bank First Time
            loadPostDetail()
        })
    })(jQuery);

    function loadPostDetail() {
        renderToWaiting()

        if (post_id.value == "") {
            renderToPost(null)
            return
        }

        $.ajax({
            url: base_url.value + "/anonymous/posts/load_detail_post/" + post_id.value,
            data: null,
            type: "GET",
            contentType: false,
            processData: false,
            success: function(response) {
                response = req.data(response)
                if (response.code == 200) {
                    renderToPost(response.data)
                } else {
                    renderToPost(null)
                }
            }
        });
    }

    function renderToWaiting() {
        $("#preview-post-content").html(`
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">Harap menunggu, sedang memuat halaman..</div>
                    </div>
                </div>
            `);
    }

    function renderToPost(data) {
        if (data == null) {
            $("#preview-post-content").html(`
                    <div class="col-12">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <strong>Oops!</strong> Maaf halaman yang anda cari sudah dihapus, atau tidak ditemukan.
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    </div>
                `);
        } else {
            var post = data
            var tags = data.tags

            $("#preview-post-content").html(`
                <div class="row">
                    <div class="col-12 col-xl-12 mb-5 mb-xl-0">
                        <div class="card mb-4">
                            <img data-src="${post.uri}" alt="" class="img-fluid rounded-top lozad"
                            src="${base_url.value + '/../assets/dist/images/broken-720.png'}">
                            <div class="card-body">
                                <a href="javascript:void(0);"><h4>${post.title}</h4></a>
                                ${post.content}
                                <hr>
                                <h4>Tags</h4>
                                <div class="card-body">
                                    ${tags.map(function(item) {
                                        return `<a href="javascript:void(0);" 
                                        class="redial-light border redial-border-light px-2 py-1 mb-2 d-inline-block redial-line-height-1_5 mr-2">
                                        ${item.tag}
                                        </a>`
                                    }).join('')}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            `)

            setInterval(() => {
                const observer = lozad();
                observer.observe();
            }, 2000);
        }
    }
</script>