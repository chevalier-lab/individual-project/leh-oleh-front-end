<div class="d-flex justify-content-between align-items-center">
    <h4 class="card-title text-danger">Informasi Menarik</h4>
</div>

<div class="container-fluid mb-4">
    <div class="row" id="interest-info"></div>
</div>

<div class="mb-4">
    <div class="row" id="load-info">
        <center>
            Sedang memuat info menarik...
        </center>
    </div>
</div>

<script>
    $(window).on("load", function() {
        // Load Bank First Time
        $("#load-info").show();
        loadPosts()
    });

    function loadPosts() {
        var raw = req.raw({
            page: 0,
            search: "",
            order_by: "id",
            order_direction: "DESC"
        })
        var formData = new FormData()
        formData.append("raw", raw)

        $.ajax({
            url: base_url.value + "/anonymous/posts/load_posts",
            data: formData,
            type: "GET",
            contentType: false,
            processData: false,
            success: function(response) {
                response = req.data(response)
                console.log(response)
                if (response.code == 200) {
                    var data = response.data

                    if (data.length > 0) {
                        isLoaded = false;
                    }

                    // Render Content
                    data.forEach(function(item) {
                        var info_location = base_url.value + '/anonymous/posts/detail/' + item.id;
                        $("#interest-info").append(`
                        <div class="col-12 col-sm-6 col-xl-3 mb-4 border py-3"
                            style="cursor: pointer"
                            onclick="location.assign('${info_location}')">
                            <div class="card border-0">
                                <div class="lozad-img-bg"
                                    data-img-bg="${item.uri}"
                                    style="background-size: cover; height: 150px; 
                                    background-position: center;
                                    onclick="location.assign('${info_location}')">
                                    <div class="loader-item"></div>
                                    </div>

                                <div class="pt-3">
                                    <p class="mb-2 h6"><a href="javascript:void(0);" class="font-weight-bold text-danger">${(item.title.length > 42) ? item.title.substring(0, 42) + "..." : item.title}</a></p>
                                    
                                    ${req.date(item.created_at)}
                                    <div class="clearfix"></div>
                                    <a href="${info_location}" class="btn btn-danger mt-2">Selengkapnya
                                        <i class="icon-arrow-right-circle"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        `);
                    })

                    setTimeout(() => {
                        lozad('.lozad-img-bg', {
                            load: function(el) {
                                el.style.backgroundImage = "url('"+el.getAttribute("data-img-bg")+"')";

                                for (var i = 0; i < el.childNodes.length; i++) {
                                    if (el.childNodes[i].className == "loader-item") {
                                        notes = el.childNodes[i];
                                        el.removeChild(notes);
                                        break;
                                    }        
                                }
                                
                            }
                        }).observe();
                    }, 1000);
                }
                $("#load-info").hide();
            }
        });
    }
</script>