<div class="row">
    <div class="col-12 mb-3">
        <div class="position-relative">
            <div class="background-image-maker py-5"></div>
            <div class="holder-image">
                <img src="<?= base_url('assets/dist/images/portfolio13.jpg'); ?>" alt="" class="img-fluid d-none">
            </div>
            <div class="position-relative px-3 py-5">
                <div class="media d-md-flex d-block">
                    <a href="javascript:void(0);"><img src="<?= base_url('assets/dist/images/contact-3.jpg'); ?>" width="100" alt="" class="img-fluid rounded-circle"></a>
                    <div class="media-body z-index-1">
                        <div class="pl-4">
                            <h1 class="display-4 text-uppercase text-white mb-0">Andy Maulana Yusuf</h1>
                            <h6 class="text-uppercase text-white mb-0">Administrator</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </div>
    <div class="col-md-6">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">General</h5>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">Username: aitek230</li>
                    <li class="list-group-item">Jenis Kelamin: Laki-laki</li>
                    <li class="list-group-item">Alamat: Purwakarta, Jawa Barat, Indonesia</li>
                    <li class="list-group-item">Tgl Lahir: 20 Juni 2000</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Contact</h5>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">Email: goingtoprofandy@gmail.com</li>
                    <li class="list-group-item">Phone Number: +6282119189690</li>
                </ul>
            </div>
        </div>
    </div>
</div>