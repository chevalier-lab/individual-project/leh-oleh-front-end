<div class="col-12">
    <div class="card">
        <div class="card-body">
            <div class="row">

                <div class="col-md-12 col-lg-5" id="cover-product-review"></div> 

                <div class="col-md-12 col-lg-7">
                    <div class="card-body border brd-gray border-top-0 border-right-0 border-left-0">
                        <h3 class="mb-0"><a href="javascript:void(0);" class="f-weight-500 text-danger"
                        id="name-product-review"></a></h3>
                        <div class="clearfix mt-1">
                            <div class="float-left mr-2">
                                <ul class="list-inline mb-0" id="rating-product-review"></ul>
                            </div>
                            <span id="count-product-review">(3 customer reviews)</span>
                        </div>
                    </div>

                    <div class="card-body border brd-gray border-top-0 border-right-0 border-left-0">
                        <div class="row">
                            <div class="col-12" id="price-product-review"></div>
                        </div>
                    </div>

                    <div class="card-body border brd-gray border-top-0 border-right-0 border-left-0">
                        <div class="row">
                            <div class="col-12" id="qty-product-review"></div>
                        </div>
                    </div>

                    <div class="card-body border brd-gray border-top-0 border-right-0 border-left-0">
                        <div class="row">
                            <div class="col-12" id="info-product-review"></div>
                        </div>
                    </div>

                    <div class="card-body border brd-gray border-top-0 border-right-0 border-left-0">
                        <div class="row">
                            <div class="col-12" id="location-product-review"></div>
                        </div>
                    </div>

                    <div class="card-body border brd-gray border-top-0 border-right-0 border-left-0">
                        <div class="row">
                            <div class="col-12" id="category-product-review"></div>
                        </div>
                    </div>

                    <div class="card-body">
                        <div class="row">

                        <div class="col-md-6 col-sm-12">
                            <a href="javascript:void(0)" class="btn btn-danger btn-lg btn-block"
                            onclick="prepareToCart(0)"><i class="icon-plus"></i> Keranjang</a>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <a href="javascript:void(0)" class="btn btn-warning btn-lg btn-block"
                            onclick="prepareToCart(1)"><i class="icon-plus"></i> Beli</a>
                        </div>

                        </div>
                    </div>

                </div> 
            </div>
        </div>
    </div>

</div>

<div class="col-12 mt-2">
    <div class="card">
        <div class="card-body">

            <h4>Deskripsi Produk</h4>

            <p class="mb-0" lang="ca" id="description-product-review"></p>

        </div>
    </div>
</div>