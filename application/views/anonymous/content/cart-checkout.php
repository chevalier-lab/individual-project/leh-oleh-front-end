<style>
:root {
    --primarycolor: #A4292C;
}

a:hover {
    color: #A4292C;
}
</style>

<div class="col-12 col-md-8 col-sm-12">
    <div class="card">
        <div class="card-body">

            <div class="row">
                <div class="col">
                    <h4 class="f-weight-500 mb-0">Data Pengiriman</h4>
                </div>
                <div style="float: right" class="mr-3">
                <button data-toggle="modal" data-target="#modal-create-address" class="btn btn-success">Tambah Alamat</button>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-md-9 w-100">
                    <b>Alamat Tujuan:</b>
                    <address id="address"></address>
                </div>
                <div class="col-md-3">
                    <button id="edit-address" data-toggle="modal" data-target="#modal-edit-address" class="btn btn-warning btn-block text-white">Ubah Alamat</button>
                </div>
            </div>
            <hr>
            <div class="w-100 mt-3">
                <b>Data Pemesan:</b>
                <div class="row">
                    <div class="col-md-4">
                        <label>Nama Lengkap<span class="text-danger">*</span></label>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="" id="full-name">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label>Nomor Telepon<span class="text-danger">*</span></label>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="" id="phone-number">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label>Email<span class="text-danger">*</span></label>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="" id="email-address">
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="card mt-3">
        <div class="card-body">

            <h4 class="f-weight-500 mb-2">Pilih Metode Pembayaran</h4>
            <div class="row">
                <div class="col-md-6">
                    <select id="payment_method" class="form-control">
                    <option value="false">Pembayaran Lainnya</option>
                    <option value="true">Leholeh Pay</option>
                    </select>
                </div>
            </div>

        </div>
    </div>

    <div class="card mt-3">
        <div class="card-body">

            <h4 class="f-weight-500 mb-2">Pilih Pengiriman</h4>
            <div id="list-product">
            </div>

        </div>
    </div>
</div>

<div class="col-12 col-md-4 col-sm-12">
    <div class="card">
        <div class="card-body border border-top-0 border-right-0 border-left-0">
            <h4 class="f-weight-500 mb-0">Cart Total</h4>
        </div>
        <div id="detail-pricing">
        </div>
        <div class="card-body">
            <div class="clearfix d-sm-flex">
                <div class="float-right w-100 text-center text-sm-right" id="order-loader">
                </div>
                <div class="float-right w-100 text-center text-sm-right">
                    <p class="mb-0 h6"><a href="javascript:void(0)" class="btn btn-danger disabled" id="order">
                        <i class="icon-handbag h6"></i> Pesan</a></p>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var listMerchantSlug = [];
    var currentAreaId;
    var idUserLocation;
    var listTransaction = [];

    var cart = {};
    var address = {
        province: "",
        province_id: "",
        city: "",
        city_id: "",
        suburb: "",
        suburb_id: "",
        area: "",
        area_id: "",
        zip: "",
        street: ""
    };
    var currentAddress = {}

    var dataOrder = {
        order : [],
        transaction_info : {
            full_name: "",
            phone_number: "",
            email: "",
            id_u_user_location: 0
        }
    }

    var positionCreateOrderSuccess = -1;

    var province = [];
    var city = [];
    var suburb = [];
    var area = [];

    (function($) {
        "use strict";
        $(window).on("load", function() {

            $("#checkout-street").on("keyup", function() {
                address.street = $("#checkout-street").val()
            })

            $("#checkout-state").on("change", function() {
                address.province = $(this).children("option:selected").val();
                loadCity($(this).children("option:selected").val())
            })

            $("#checkout-city").on("change", function() {
                address.city = $(this).children("option:selected").val();
                loadSuburb($(this).children("option:selected").val())
            })

            $("#checkout-suburb").on("change", function() {
                address.suburb = $(this).children("option:selected").val();
                loadArea($(this).children("option:selected").val())
            })

            $("#checkout-area").on("change", function() {
                address.area = $(this).children("option:selected").val();
                setupZip($(this).children("option:selected").val())
            })

            // Load Address
            loadAddress()

            // Load Province
            loadProvince()

            $("#create-address").on("click", function(){
                if(address.province == "" || address.city == "" || address.suburb == "" || address.area == "" || address.zip == "" || address.street == "")
                    toastr.error("Harap isi seluruh form yang tersedia");
                else {
                    $("#create-address").hide();
                    $("#create-address-loader").html(`
                        <div class="loader-item"></div>
                    `);

                    var raw = req.raw({
                        province_id: address.province_id,
                        province: address.province,
                        city_id: address.city_id,
                        city: address.city,
                        suburb_id: address.suburb_id,
                        suburb: address.suburb,
                        area_id: address.area_id,
                        area: address.area,
                        postcode: address.zip,
                        address: address.street,
                        is_active: 1
                    })
    
                    var formData = new FormData()
                    formData.append("raw", raw)
    
                    $.ajax({
                        url: base_url.value + "/anonymous/cart/add_new_location",
                        data: formData,
                        type: "POST",
                        contentType: false,
                        processData: false,
                        success: function(response) {
                            response = req.data(response)
                            if (response.code == 200) {
                                toastr.success("Berhasil menambah lokasi")
                                location.reload();
                            } else {
                                $("#create-address").show();
                                $("#create-address-loader").html(``);
                                toastr.error(response.message)
                            }
                        }
                    });
                }
            })

            $("#edit-address").on("click", function(){
                $.ajax({
                    url: base_url.value + "/anonymous/cart/load_location_user",
                    data: null,
                    type: "GET",
                    contentType: false,
                    processData: false,
                    success: function(response) {
                        response = req.data(response)
                        if (response.code == 200) {
                            renderListLocation(response.data)
                        } else {
                            toastr.error(response.message)
                        }
                    }
                });
            })

            $("#order").on("click", function() {
                var status = true;
                dataOrder.transaction_info.full_name = $('#full-name').val();
                dataOrder.transaction_info.phone_number = $('#phone-number').val();
                dataOrder.transaction_info.email = $('#email-address').val();

                if(dataOrder.transaction_info.full_name == "" || 
                dataOrder.transaction_info.phone_number == "" || 
                dataOrder.transaction_info.email == "" || 
                dataOrder.transaction_info.id_u_user_location == 0){
                    status = false
                }

                if (!status) {
                    toastr.error("Harap isi seluruh form data pemesan");
                    return
                } else {
                    // Todo Checkout
                    $("#order").hide();
                    $("#order-loader").html(`
                        <div class="loader-item"></div>
                    `);

                    var raw = req.raw({
                        order: dataOrder.order,
                        transaction_info: dataOrder.transaction_info,
                        is_leh_oleh_pay: ($("#payment_method").val() == "false" || $("#payment_method").val() == false) ? false : true
                    });

                    var formData = new FormData();
                    formData.append("raw", raw);

                    $.ajax({
                        url: base_url.value + "/anonymous/cart/do_checkout",
                        data: formData,
                        type: "POST",
                        contentType: false,
                        processData: false,
                        success: function(response) {
                            response = req.data(response)

                            if (response.code == 200) {
                                var data = response.data;
                                listTransaction = response.data;

                                data.forEach(function(item) {
                                    createOrder(item)
                                })
                            } else {
                                $("#order").show();
                                $("#order-loader").html(``);
                                toastr.error(response.message);
                            }
                        }
                    });
                }
            })
        });

        $("#list-product").on("change", function() {
            var addition = 0;
            var checkedLogistic = 0;
            listMerchantSlug.forEach(function(item) {
                var getRadio = $(`input[name='logistic-${item.slug}']:checked`)
                checkedLogistic += getRadio.length
                if (getRadio.val()) {
                    item.rate_id = getRadio.data("id")
                    addition += Number(getRadio.val())

                    var found = dataOrder.order.find(merchant => merchant.id == item.id)
                    if(found) found.additional = getRadio.val()
                }
            })
            if(listMerchantSlug.length == checkedLogistic){
                $('#order').removeClass('disabled')
            }
            setupPricing(addition)
        })
    })(jQuery);

    (function($) {
        $.fn.inputFilter = function(inputFilter) {
            return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
                if (inputFilter(this.value)) {
                    this.oldValue = this.value;
                    this.oldSelectionStart = this.selectionStart;
                    this.oldSelectionEnd = this.selectionEnd;
                } else if (this.hasOwnProperty("oldValue")) {
                    this.value = this.oldValue;
                    this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                } else {
                    this.value = "";
                }
            });
        };
    }(jQuery));

    function loadProvince() {
        $.ajax({
            url: base_url.value + "/services/shipper/locations/load_province",
            data: null,
            type: "GET",
            contentType: false,
            processData: false,
            success: function(response) {
                response = req.data(response)
                if (response.status == "success") {
                    var data = response.data.rows
                    province = data;
                    setupProvince()
                }
            }
        });
    }

    function loadCity(prov) {
        var id = -1;
        province.forEach(function(item) {
            if (item.name == prov) id = item.id;
        });
        address.province_id = id
        $.ajax({
            url: base_url.value + "/services/shipper/locations/load_city?id=" + id,
            data: null,
            type: "GET",
            contentType: false,
            processData: false,
            success: function(response) {
                response = req.data(response)
                if (response.status == "success") {
                    var data = response.data.rows
                    city = data;
                    setupCity()
                }
            }
        });
    }

    function loadSuburb(citySelected) {
        var id = -1;
        city.forEach(function(item) {
            if (item.name == citySelected) id = item.id;
        });
        address.city_id = id
        $.ajax({
            url: base_url.value + "/services/shipper/locations/load_suburbs/" + id,
            data: null,
            type: "GET",
            contentType: false,
            processData: false,
            success: function(response) {
                response = req.data(response)
                if (response.status == "success") {
                    var data = response.data.rows
                    suburb = data;
                    setupSuburb()
                }
            }
        });
    }

    function loadArea(suburbSelected) {
        var id = -1;
        suburb.forEach(function(item) {
            if (item.name == suburbSelected) id = item.id;
        });
        address.suburb_id = id
        $.ajax({
            url: base_url.value + "/services/shipper/locations/load_areas/" + id,
            data: null,
            type: "GET",
            contentType: false,
            processData: false,
            success: function(response) {
                response = req.data(response)
                if (response.status == "success") {
                    var data = response.data.rows
                    area = data;
                    setupArea()
                }
            }
        });
    }

    function setupProvince() {
        $("#checkout-state").html(`${province.map(function(item, position) {
            if (position == 0) 
            return `
            <option label="select" selected disabled></option>
            <option value="${item.name}">${item.name}</option>`;
            else 
            return `<option value="${item.name}">${item.name}</option>`;
        }).join('')}`);
    }

    function setupCity() {
        $("#checkout-city").html(`${city.map(function(item, position) {
            if (position == 0) 
            return `
            <option label="select" selected disabled></option>
            <option value="${item.name}">${item.name}</option>`;
            else 
            return `<option value="${item.name}">${item.name}</option>`;
        }).join('')}`);
    }

    function setupSuburb() {
        $("#checkout-suburb").html(`${suburb.map(function(item, position) {
            if (position == 0) 
            return `
            <option label="select" selected disabled></option>
            <option value="${item.name}">${item.name}</option>`;
            else 
            return `<option value="${item.name}">${item.name}</option>`;
        }).join('')}`);
    }

    function setupArea() {
        $("#checkout-area").html(`${area.map(function(item, position) {
            if (position == 0) 
            return `
            <option label="select" selected disabled></option>
            <option value="${item.name}">${item.name}</option>`;
            else 
            return `<option value="${item.name}">${item.name}</option>`;
        }).join('')}`);
    }

    function setupZip(areaSelected) {
        var id = -1;
        area.forEach(function(item) {
            if (item.name == areaSelected){
                id = item.postcode;
                address.area_id = item.id;
            } 
        });
        address.zip = id
        $("#checkout-zip").val(id);
    }

    function loadCart() {
        $.ajax({
            url: base_url.value + "/anonymous/cart/load_cart_active",
            data: null,
            type: "GET",
            contentType: false,
            processData: false,
            success: function(response) {
                response = req.data(response)
                if (response.code == 200) {
                    var data = response.data
                    cart = data;
                    setupCart();
                    setupPricing();
                    // for (let j = 0; j < data.length; j++) {
                    //     $(`<div class="carousel-item"><img class="d-block w-100" src="${data[j].uri}"></div>`).appendTo('.carousel-inner');
                    //     $('<li data-target="#carouselExampleCaption" data-slide-to="' + j + '"></li>').appendTo('.carousel-indicators')
                    // }

                    // $('.carousel-item').first().addClass('active');
                    // $('.carousel-indicators > li').first().addClass('active');
                    // $('#carouselExampleCaption').carousel();
                }
            }
        });
    }

    function setupCart() {
        if (cart.length > 0) {
            cart.map(function(item, index) {
                loadProductCart(item);
                // loadLogistic(item.product_info, item.price_selling, index)
            })
        }
    }

    function setupPricing(addition = 0) {

        var subTotal = 0;

        cart.forEach(function(item) {
            subTotal += Number(item.total)
        })

        $("#detail-pricing").html(`
            <div class="card-body border border-top-0 border-right-0 border-left-0">
                <div class="clearfix">
                    <div class="float-left">
                        <p class="mb-0 dark-color f-weight-600">Subtotal : </p>
                    </div>
                    <div class="float-right">
                        <p class="mb-0 dark-color f-weight-600 h4">${req.money(subTotal.toString(), "Rp ")}</p>
                    </div>
                </div>
            </div>

            <div class="card-body border border-top-0 border-right-0 border-left-0">
                <div class="clearfix">
                    <div class="float-left">
                        <p  class="mb-0 dark-color f-weight-600">Addtional : </p>
                    </div>
                    <div class="float-right">
                        <p class="mb-0 dark-color f-weight-600 h4">${req.money(addition.toString(), "Rp ")}</p>
                    </div>
                </div>
            </div>

            <div class="card-body border border-top-0 border-right-0 border-left-0">
                <div class="clearfix">
                    <div class="float-left">
                        <p class="mb-0 dark-color f-weight-600">Total : </p>
                    </div>
                    <div class="float-right">
                        <p class="mb-0 dark-color f-weight-600 h4">${req.money((addition + subTotal).toString(), "Rp ")}</p>
                    </div>
                </div>
            </div>
        `);
    }

    function loadProductCart(cart){
        var merchantSlug = cart.id_merchant
        var dataOrderMerchant = {
            id: cart.location.id_u_user_is_merchant,
            id_u_user_merchant: cart.id_u_user_merchant,
			list_id_product: [],
			additional: 0,
            market_name: cart.market_name,
            market_phone_number: cart.market_phone_number,
        }
        listMerchantSlug.push({id:cart.location.id_u_user_is_merchant ,slug: merchantSlug});
        var infoProduct = {
            area_id: cart.location.area_id,
            price: cart.total,
            length: 0,
            width: 0,
            height: 0,
            weight: 0
        }

        $("#list-product").append(`
            <div class="table-responsive pr-1 pl-1 mb-0 mt-3" id="cart-list">
                <div class="card bg-light">
                    <div class="card-header">${cart.market_name}</div>
                    <div class="card-body">
                    ${cart.products.map(function(item, position) {
                        dataOrderMerchant.list_id_product.push(item.id_m_products);
                        infoProduct.length += Number(item.product_info.length) * item.qty
                        infoProduct.width += Number(item.product_info.width) * item.qty
                        infoProduct.height += Number(item.product_info.height) * item.qty
                        infoProduct.weight += Number(item.product_info.weight) * item.qty
                        return `
                        <div class="media d-block d-flex text-left py-2">
                            <div class="lozad-img-bg rounded"
                            data-img-bg="${item.uri}"
                            style="background-size: cover; height: 80px; width: 80px; 
                            background-position: center;">
                            <div class="loader-item"></div>
                            </div>
                            <div class="ml-2 media-body align-self-center mt-0 color-primary d-flex">
                                <div class="message-content"> <b class="mb-1 font-weight-bold d-flex">${item.product_name}</b>
                                    <small class="body-color">${req.money(item.price_selling.toString(), "Rp ")}</small></div>
                                <div class="new-message ml-auto">x${item.qty}</div>
                            </div>
                        </div>
                        `
                    }).join('')}
                    </div>
                    <div class="card-footer" id="${merchantSlug}">
                    </div>
                </div>
            </div>
        `);
        setTimeout(() => {
            lozad('.lozad-img-bg', {
                load: function(el) {
                    el.style.backgroundImage = "url('"+el.getAttribute("data-img-bg")+"')";

                    for (var i = 0; i < el.childNodes.length; i++) {
                        if (el.childNodes[i].className == "loader-item") {
                            notes = el.childNodes[i];
                            el.removeChild(notes);
                            break;
                        }        
                    }

                }
            }).observe();
        }, 1000);

        dataOrder.order.push(dataOrderMerchant)
        loadLogistic(infoProduct, merchantSlug)
    }

    function loadLogistic(infoProduct, merchantSlug) {
        var formData = new FormData();
        formData.append("o", infoProduct.area_id);
        formData.append("d", currentAreaId); 
        formData.append("wt", infoProduct.weight);
        formData.append("v", infoProduct.price);
        formData.append("l", infoProduct.length);
        formData.append("w", infoProduct.width);
        formData.append("h", infoProduct.height);

        $.ajax({
            url: base_url.value + "/services/shipper/rates/load_domestic_rates",
            data: formData,
            type: "POST",
            contentType: false,
            processData: false,
            success: function(response) {
                response = req.data(response)
                if (response.status == 'success') {
                    var data = response.data;
                    setupLogistic(data.rates.logistic, merchantSlug)
                } else {
                    var data = response.data;
                    toastr.error(data.content);
                }
            }
        });
    }

    function setupLogistic(logistic, merchantSlug) {
        var parent = `<div id="logistic-accordion-${merchantSlug}" class="accordion-alt" role="tablist">`
        var listLogistic = ""

        if(logistic['regular'].length == 0 && 
        logistic['express'].length == 0 && 
        logistic['same day'].length == 0 &&
        logistic['instant'].length == 0 &&
        logistic['trucking'].length == 0) {
            $(`#${merchantSlug}`).append('<p class="text-center mt-2">Tidak ada pengiriman yang didukung</p>')
        } else {
            // Render regular
            if(logistic['regular'].length > 0)
                parent += setupLogisticReguler(logistic['regular'],merchantSlug)

            // Render Express
            if(logistic['express'].length > 0)
                parent += setupLogisticExpress(logistic['express'],merchantSlug)

            // Render Same Day
            if(logistic['same day'].length > 0)
                parent += setupLogisticSameDay(logistic['same day'],merchantSlug)

            // Render Instant
            if(logistic['instant'].length > 0)
                parent += setupLogisticInstant(logistic['instant'],merchantSlug)

            // Render Truck
            if(logistic['trucking'].length > 0)
                parent += setupLogisticTruck(logistic['trucking'],merchantSlug)

            parent += `</div>`
            $(`#${merchantSlug}`).append(parent)
        }
    }

    function setupLogisticReguler(regular, merchantSlug){
        return `
        <div class="mb-2">
            <h6 class="mb-0">
                <a class="d-block border collapsed" data-toggle="collapse" href="#regular-${merchantSlug}" aria-expanded="false" aria-controls="regular">
                    Regular
                </a>
            </h6>
            <div id="regular-${merchantSlug}" class="collapse" role="tabpanel" data-parent="#logistic-accordion-${merchantSlug}" style="">
                <div class="card-body">
                    ${regular.map(function(item, position) {
                        return `
                        <div class="custom-control custom-radio">
                            <input type="radio" name="logistic-${merchantSlug}" class="custom-control-input" id="reg${position}-${merchantSlug}" value="${item.finalRate}" data-id="${item.rate_id}">
                            <input type="hidden" name="logistic-${merchantSlug} value="tes">
                            <label class="custom-control-label checkbox-primary" for="reg${position}-${merchantSlug}">
                                ${item.name} ${item.rate_name} - ${req.money(item.finalRate.toString(), "Rp ")}
                            </label>
                        </div>
                        `
                    }).join('')}
                </div>
            </div>
        </div>
        `
    }

    function setupLogisticExpress(express, merchantSlug){
        return `
        <div class="mb-2">
            <h6 class="mb-0">
                <a class="d-block border collapsed" data-toggle="collapse" href="#express-${merchantSlug}" aria-expanded="false" aria-controls="regular">
                    Express
                </a>
            </h6>
            <div id="express-${merchantSlug}" class="collapse" role="tabpanel" data-parent="#logistic-accordion-${merchantSlug}" style="">
                <div class="card-body">
                    ${express.map(function(item, position) {
                        return `
                        <div class="custom-control custom-radio">
                            <input type="radio" name="logistic-${merchantSlug}" class="custom-control-input" id="express${position}-${merchantSlug}" value="${item.finalRate}">
                            <label class="custom-control-label checkbox-primary" for="express${position}-${merchantSlug}">
                                ${item.name} ${item.rate_name} - ${req.money(item.finalRate.toString(), "Rp ")}
                            </label>
                        </div>
                        `
                    }).join('')}
                </div>
            </div>
        </div>
        `
    }

    function setupLogisticSameDay(sameDay, merchantSlug){
        return `
        <div class="mb-2">
            <h6 class="mb-0">
                <a class="d-block border collapsed" data-toggle="collapse" href="#sameday-${merchantSlug}" aria-expanded="false" aria-controls="sameday">
                    Same Day
                </a>
            </h6>
            <div id="sameday-${merchantSlug}" class="collapse" role="tabpanel" data-parent="#logistic-accordion-${merchantSlug}" style="">
                <div class="card-body">
                    ${sameDay.map(function(item, position) {
                        return `
                        <div class="custom-control custom-radio">
                            <input type="radio" name="logistic-${merchantSlug}" class="custom-control-input" id="sameday${position}-${merchantSlug}" value="${item.finalRate}">
                            <label class="custom-control-label checkbox-primary" for="sameday${position}-${merchantSlug}">
                                ${item.name} ${item.rate_name} - ${req.money(item.finalRate.toString(), "Rp ")}
                            </label>
                        </div>
                        `
                    }).join('')}
                </div>
            </div>
        </div>
        `
    }

    function setupLogisticInstant(instant, merchantSlug){
        return `
        <div class="mb-2">
            <h6 class="mb-0">
                <a class="d-block border collapsed" data-toggle="collapse" href="#instant-${merchantSlug}" aria-expanded="false" aria-controls="sameday">
                    Instant
                </a>
            </h6>
            <div id="instant-${merchantSlug}" class="collapse" role="tabpanel" data-parent="#logistic-accordion-${merchantSlug}" style="">
                <div class="card-body">
                    ${instant.map(function(item, position) {
                        return `
                        <div class="custom-control custom-radio">
                            <input type="radio" name="logistic-${merchantSlug}" class="custom-control-input" id="instant${position}-${merchantSlug}" value="${item.finalRate}">
                            <label class="custom-control-label checkbox-primary" for="instant${position}-${merchantSlug}">
                                ${item.name} ${item.rate_name} - ${req.money(item.finalRate.toString(), "Rp ")}
                            </label>
                        </div>
                        `
                    }).join('')}
                </div>
            </div>
        </div>
        `
    }

    function loadAddress(){
        $.ajax({
            url: base_url.value + "/anonymous/cart/load_current_location_user",
            data: null,
            type: "GET",
            contentType: false,
            processData: false,
            success: function(response) {
                response = req.data(response)
                if (response.code == 200) {
                    var data = response.data
                    currentAddress = data;
                    currentAreaId = data.area_id
                    dataOrder.transaction_info.id_u_user_location = data.id
                    $('#address').html(`${data.address}, ${data.area}, ${data.suburb}, ${data.city}, ${data.province}, Indonesia, ${data.postcode}`) 
                    // Load Cart
                    loadCart()
                } else {
                    $('#address').html(`Belum menambah alamat`) 
                    $("#edit-address").attr("disabled", true);
                    toastr.error("Mohon tambah alamat terlebih dahulu")
                }
            }
        });
    }

    function renderListLocation(data){
        $("#container-address").html(`${data.map(function(item) {
            if (item.is_active == 1) 
            return `
            <div class="card border m-2">                            
                <div class="card-content">
                    <div class="card-body p-4">   
                        <div class="d-md-flex">
                            <div class="content px-md-3 my-3 my-md-0">                                           
                                <p class="mb-0 font-w-500 tx-s-12">${item.address}, ${item.area}, ${item.suburb}, ${item.city}, ${item.province}, Indonesia, ${item.postcode}</p>                                                    
                            </div>
                        </div>
                    </div>
                </div>
            </div>`;
            else 
            return `
            <div class="card border m-2">                            
                <div class="card-content">
                    <div class="card-body p-4">   
                        <div class="d-md-flex">
                            <div class="content px-md-3 my-3 my-md-0">                                           
                                <p class="mb-0 font-w-500 tx-s-12">${item.address}, ${item.area}, ${item.suburb}, ${item.city}, ${item.province}, Indonesia, ${item.postcode}</p>                                                    
                            </div>
                            <div class="my-auto">
                                <a href="javascript:void(0)" class="btn btn-outline-primary font-w-600 my-auto text-nowrap" onclick="changeLocation(${item.id})">Pilih</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            `;
        }).join('')}`);
    }

    function changeLocation(id){
        $.ajax({
            url: base_url.value + "/anonymous/cart/update_current_location_user/" + id,
            data: null,
            type: "GET",
            contentType: false,
            processData: false,
            success: function(response) {
                response = req.data(response)
                if (response.code == 200) {
                    location.reload();
                } else {
                    toastr.error(response.message);
                }
            }
        });
    }

    function createOrder(transaction){
        var found = listMerchantSlug.find(merchant => merchant.id == transaction.transaction_merchants.id_u_user_is_merchant)
        var product = [];

        transaction.transaction_products.forEach(function(item){
            product.push(item.product_name)
        })

        var raw = req.raw({
            id_user_transaction: transaction.transaction_info.id_u_user_transaction,
            id_u_user_is_merchant: transaction.transaction_merchants.id_u_user_is_merchant, 
            id_u_user_location: dataOrder.transaction_info.id_u_user_location,
            consigneeName: dataOrder.transaction_info.full_name,
            consigneePhoneNumber: dataOrder.transaction_info.phone_number,
            consignerName: dataOrder.order[(positionCreateOrderSuccess + 1)].market_name,
            consignerPhoneNumber: dataOrder.order[(positionCreateOrderSuccess + 1)].market_phone_number,
            rateID: found.rate_id,
            destinationAddress: $('#address').text(),
            destinationDirection: $('#address').text(),
            contents: `User membeli ${product.join(", ")}`,
            packageType: 1
        })

        var formData = new FormData();
        formData.append("raw", raw);

        $.ajax({
            url: base_url.value + "/anonymous/cart/create_order",
            data: formData,
            type: "POST",
            contentType: false,
            processData: false,
            success: function(response) {
                response = req.data(response)
                if (response.code == 200) {
                    positionCreateOrderSuccess++;
                    toastr.success("Berhasil melakukan checkout")
                    if (positionCreateOrderSuccess == (listMerchantSlug.length - 1)) {
                        if ($("#payment_method").val() == "false" || $("#payment_method").val() == false) {
                            sendTransactionMidtrans()
                        } else {
                            console.log("Berhasil pindah");
                            location.assign(base_url.value + "/anonymous/order")
                        }
                    }
                } else {
                    $("#order").show();
                    $("#order-loader").html(``);
                    toastr.error(response.message);
                }
            }
        });
    }

    function sendTransactionMidtrans() {
        listTransaction.forEach(function(item, position) {
            var totalPrice = Number(item.transaction.total_price) + Number(item.transaction_merchants.additional)
            var products = [{
                id: 9999,
                price: Number(item.transaction_merchants.additional),
                quantity: 1,
                name: "Ongkos Kirim"
            }];

            var selectedCart = cart[position]
            selectedCart.products.forEach(function(product) {
                products.push({
                    id: product.id_m_products,
                    price: product.subTotal,
                    quantity: product.qty,
                    name: product.product_name
                })
            });

            var raw = req.raw({
                transaction_details: {
                    order_id: item.transaction.transaction_token,
                    gross_amount: totalPrice
                },
                credit_card: {
                    secure: true
                },
                item_details: products,
                customer_details: {
                    first_name: item.transaction_info.full_name,
                    last_name: item.transaction_info.full_name,
                    email: item.transaction_info.email,
                    phone: item.transaction_info.phone_number,
                    billing_address: {
                        first_name: item.transaction_info.full_name,
                        last_name: item.transaction_info.full_name,
                        email: item.transaction_info.email,
                        phone: item.transaction_info.phone_number,
                        address: currentAddress.address,
                        city: currentAddress.city,
                        postal_code: currentAddress.postcode,
                        country_code: "IDN"
                    },
                    shipping_address: {
                        first_name: item.transaction_info.full_name,
                        last_name: item.transaction_info.full_name,
                        email: item.transaction_info.email,
                        phone: item.transaction_info.phone_number,
                        address: currentAddress.address,
                        city: currentAddress.city,
                        postal_code: currentAddress.postcode,
                        country_code: "IDN"
                    }
                }
            })

            var formData = new FormData()
            formData.append("raw", raw)

            $.ajax({
                url: base_url.value + "/anonymous/cart/create_payment_link",
                data: formData,
                type: "POST",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    updateTokenTransaction(item.transaction.id,
                    response.token, function() {
                        if (position == (listTransaction.length - 1))  {
                            console.log("Berhasil pindah pake midtrans");
                            location.assign(base_url.value + "/anonymous/order")
                        }
                    })
                }
            });

        });
    }

    function updateTokenTransaction(id, token, callback) {
        var raw = req.raw({
            token_payment: token
        })

        console.log(raw);

        var formData = new FormData()
        formData.append("raw", raw);

        $.ajax({
            url: base_url.value + "/anonymous/cart/update_payment_token/" + id,
            data: formData,
            type: "POST",
            contentType: false,
            processData: false,
            success: function(response) {
                response = req.data(response)
                callback();
            }
        });
    }

    function setupLogisticTruck(truck, merchantSlug){
        return `
        <div class="mb-2">
            <h6 class="mb-0">
                <a class="d-block border collapsed" data-toggle="collapse" href="#truck-${merchantSlug}" aria-expanded="false" aria-controls="sameday">
                    Truk
                </a>
            </h6>
            <div id="truck-${merchantSlug}" class="collapse" role="tabpanel" data-parent="#logistic-accordion-${merchantSlug}" style="">
                <div class="card-body">
                    ${truck.map(function(item, position) {
                        return `
                        <div class="custom-control custom-radio">
                            <input type="radio" name="logistic-${merchantSlug}" class="custom-control-input" id="truck${position}-${merchantSlug}" value="${item.finalRate}">
                            <label class="custom-control-label checkbox-primary" for="truck${position}-${merchantSlug}">
                                ${item.name} ${item.rate_name} - ${req.money(item.finalRate.toString(), "Rp ")}
                            </label>
                        </div>
                        `
                    }).join('')}
                </div>
            </div>
        </div>
        `
    }
</script>