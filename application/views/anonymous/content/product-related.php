
<div class="col-12 mt-3 mb-3">
    <div class="card" style="border: none;">
        <div class="card-body">
            <div class="row">
            <h4 class="card-title text-danger">Produk Sejenis</h4>
            </div>
            <div class="row" id="realted-product">
            </div>
        </div>
    </div>

</div>

<script>
(function ($) {
    "use strict";
    $(window).on("load", function () {
        loadRelatedProduct()
    });
})(jQuery);

function loadRelatedProduct() {
    var raw = req.raw({
        page: 0,
        search: "",
        order_by: "id",
        order_direction: "DESC"
    })

    var formData = new FormData()
    formData.append("raw", raw)

    $.ajax({
        url: base_url.value + "/anonymous/landing/load_new_products",
        data: formData,
        type: "POST",
        contentType: false,
        processData: false,
        success: function(response) {
            response = req.data(response)
            if (response.code == 200) {
                var data = response.data

                if (data.length > 0) {
                    isLoaded = false;
                }

                // Render Product
                data.forEach(function(item) {

                    var price = Number(item.price_selling)
                    var discount = Number(item.discount);
                    var price_divide_discount = ((price / 100) * discount);

                    price = (price - price_divide_discount)

                    var product_location = base_url.value + "/anonymous/product/detail/" + item.id

                    $("#realted-product").append(`
                        <div class="col-sm-6 col-md-4 col-lg-2 border py-3">
                            <div class="position-relative">
                                <div class="lozad-img-bg"
                                    data-img-bg="${item.uri}"
                                    style="background-size: cover; height: 150px; 
                                    background-position: center;
                                    onclick="location.assign('${product_location}')">
                                    <div class="loader-item"></div>
                                    </div>

                                ${
                                    (discount > 0) ? `
                                    <div class="mb-3" style="top: 0px; position: absolute">
                                        <a href="javascript:void(0);" class="rounded-left bg-white px-3 py-2 text-primary">-${discount}%</a>
                                    </div>
                                    ` : ""
                                }
                                <div class="caption-bg fade bg-transparent text-right">
                                    <div class="d-table w-100 h-100 ">
                                        <div class="d-table-cell align-bottom">
                                            <div class="mb-3">
                                                <a href="javascript:void(0);" class="rounded-left bg-white px-3 py-2 text-primary"><i class="icon-heart"></i></a>
                                            </div>
                                            <div class="mb-4">
                                                <a href="javascript:void(0);" class="rounded-left bg-white px-3 py-2 text-primary"><i class="icon-bag"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="pt-3"
                                onclick="location.assign('${product_location}')">
                                <p class="mb-2"><a href="javascript:void(0);" class="font-weight-bold text-primary">${item.product_name}</a></p>
                                <div class="clearfix">
                                    <div class="d-inline-block">${
                                        (discount > 0) ? req.money(item.price_selling.toString(), "Rp ") + ""
                                        : req.money(price.toString(), "Rp ")
                                    }</div>
                                    <ul class="list-inline mb-0 mt-2">
                                        <li class="list-inline-item"><a href="javascript:void(0);" class="text-primary"><i class="icon-star"></i></a></li>
                                        <li class="list-inline-item"><a href="javascript:void(0);" class="text-primary"><i class="icon-star"></i></a></li>
                                        <li class="list-inline-item"><a href="javascript:void(0);" class="text-primary"><i class="icon-star"></i></a></li>
                                        <li class="list-inline-item"><a href="javascript:void(0);"><i class="icon-star"></i></a></li>
                                        <li class="list-inline-item"><a href="javascript:void(0);"><i class="icon-star"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    `);
                })

                setTimeout(() => {
                    lozad('.lozad-img-bg', {
                        load: function(el) {
                            el.style.backgroundImage = "url('"+el.getAttribute("data-img-bg")+"')";

                            for (var i = 0; i < el.childNodes.length; i++) {
                                if (el.childNodes[i].className == "loader-item") {
                                    notes = el.childNodes[i];
                                    el.removeChild(notes);
                                    break;
                                }        
                            }

                        }
                    }).observe();
                }, 1000);
            }

            $("#load-product").hide();
        }
        // ,
        // error: function(response) {
        //     console.log("ERROR", response);
        //     $("#btn-login").removeAttr("disabled");
        // }
    });
}
</script>