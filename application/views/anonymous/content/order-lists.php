<div class="tab-content container-fluid" id="list-order-tab-content">
    <div class="tab-pane fade show active row" id="order-active" role="tabpanel" 
        aria-labelledby="order-active-tab">
        <div class="col-12 mt-3">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Pesanan Active</h4> 
                </div>
                <div class="card-body">

                    <div class="pt-0 pb-0 pl-3 pr-3 mb-3 row">
                        <div class="input-group col-12 col-md-6 p-1">
                            <input type="text" class="form-control p-2 w-100 h-100 contact-search" placeholder="Search ..." id="order-list-active-search">
                            <div class="input-group-append">
                                <span class="btn btn-outline-primary input-group-text" id="order-list-active-button-search"><i class="icon-magnifier" aria-hidden="true"></i></span>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 p-1">
                            <select class="form-control p-2 w-100 h-100 contact-search" id="order-list-active-order">
                                <option value="0">Urutkan berdasarkan</option>
                                <option value="1">Nama (A-Z)</option>
                                <option value="2">Nama (Z-A)</option>
                            </select>
                        </div>
                    </div>

                    <div class="table-responsive pr-1 pl-1 mb-0">

                        <?php

                        $this->load->view('components/table', array(
                            "idTable" => "order-list-active",
                            "isCustomThead" => true,
                            "thead" => '
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">TRX ID</th>
                                <th scope="col">Total Harga</th>
                                <th scope="col">Tanggal Order</th>
                                <th scope="col">Tanggal Perubahan</th>
                                <th scope="col">Status</th>
                                <th scope="col">Detail</th>
                            </tr>
                            ',
                            "tbody" => ""
                        )) ?>

                    </div>

                    <div class="row">
                        <div class="col-12">
                            <div class="p-1">
                                <div class="btn-group" role="group" aria-label="Pagination Group">
                                    <button class="btn btn-outline-secondary" type="button" id="order-list-active-prev"><i class="icon-arrow-left-circle"></i> Prev</button>
                                    <button class="btn btn-outline-secondary" type="button" id="order-list-active-next">Next <i class="icon-arrow-right-circle"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane fade row" id="order-history" role="tabpanel" 
        aria-labelledby="order-history-tab">
        <div class="col-12 mt-3">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Riwayat Pesanan</h4> 
                </div>
                <div class="card-body">

                    <div class="pt-0 pb-0 pl-3 pr-3 mb-3 row">
                        <div class="input-group col-12 col-md-6 p-1">
                            <input type="text" class="form-control p-2 w-100 h-100 contact-search" placeholder="Search ..." id="order-list-history-search">
                            <div class="input-group-append">
                                <span class="btn btn-outline-danger input-group-text" id="order-list-history-button-search"><i class="icon-magnifier" aria-hidden="true"></i></span>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 p-1">
                            <select class="form-control p-2 w-100 h-100 contact-search" id="order-list-history-order">
                                <option value="0">Urutkan berdasarkan</option>
                                <option value="1">Nama (A-Z)</option>
                                <option value="2">Nama (Z-A)</option>
                            </select>
                        </div>
                    </div>

                    <div class="table-responsive pr-1 pl-1 mb-0">

                        <?php

                        $this->load->view('components/table', array(
                            "idTable" => "order-list-history",
                            "isCustomThead" => true,
                            "thead" => '
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">TRX ID</th>
                                <th scope="col">Total Harga</th>
                                <th scope="col">Tanggal Order</th>
                                <th scope="col">Tanggal Perubahan</th>
                                <th scope="col">Status</th>
                                <th scope="col">Detail</th>
                            </tr>
                            ',
                            "tbody" => ""
                        )) ?>

                    </div>

                    <div class="row">
                        <div class="col-12">
                            <div class="p-1">
                                <div class="btn-group" role="group" aria-label="Pagination Group">
                                    <button class="btn btn-outline-secondary" type="button" id="order-list-history-prev"><i class="icon-arrow-left-circle"></i> Prev</button>
                                    <button class="btn btn-outline-secondary" type="button" id="order-list-history-next">Next <i class="icon-arrow-right-circle"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var orderActive = [];
    var orderHistory = [];

    // Initial Pagination
    var page = 0;
    var paginationClicked = false;
    var paginationDirection = "";

    (function($) {
        "use strict";
        $(window).on("load", function() {
            // Load Order
            loadOrder()

            // Handle Order By
            $("#order-list-active-order").on("change", function() {
                loadOrder();
            });
            // Handle Order By
            $("#order-list-history-order").on("change", function() {
                loadOrderHistory()
            });

            // Handle Search
            $("#order-list-active-search").on("keydown", function(event) {
                if (event.keyCode == 32 || event.which == 32) loadOrder();
                else if ($("#order-list-active-search").val() == "") loadOrder();
            });
            // Handle Search
            $("#order-list-history-search").on("keydown", function(event) {
                if (event.keyCode == 32 || event.which == 32) loadOrderHistory();
                else if ($("#order-list-history-search").val() == "") loadOrderHistory();
            });

            $("#order-list-active-button-search").on("click", function(event) {
                loadOrder();
            });
            $("#order-list-history-button-search").on("click", function(event) {
                loadOrderHistory();
            });

            // Handle Pagination
            $("#order-list-active-prev").on("click", function() {
                if (page > 0)
                    page -= 1;
                paginationClicked = true;
                paginationDirection = "prev";
                loadOrder();
            });
            // Handle Pagination
            $("#order-list-history-prev").on("click", function() {
                if (page > 0)
                    page -= 1;
                paginationClicked = true;
                paginationDirection = "prev";
                loadOrderHistory();
            });

            $("#order-list-active-next").on("click", function() {
                page += 1;
                paginationClicked = true;
                paginationDirection = "next";
                loadOrder();
            });
            $("#order-list-history-next").on("click", function() {
                page += 1;
                paginationClicked = true;
                paginationDirection = "next";
                loadOrderHistory();
            });
        });
    })(jQuery);

    function loadSwitch(action) {
        page = 0
        orderActive = [];
        orderHistory = [];
        if (action == 0) loadOrder()
        else loadOrderHistory()
    }

    function loadOrder() {

        var order = $("#order-list-active-order").val();
        var order_direction = "DESC";
        switch (Number(order)) {
            case 1: 
                order =  "ip_address";
                order_direction = "ASC";
            break;
            case 2: 
                order =  "ip_address";
                order_direction = "DESC";
            break;
            default:
                order =  "id";
                order_direction = "DESC";
            break;
        }
        var raw = req.raw({
            page: page,
            search: $("#order-list-active-search").val(),
            order_by: order,
            order_direction: order_direction
        })

        var formData = new FormData()
        formData.append("raw", raw)

        $.ajax({
            url: base_url.value + "/anonymous/order/load_order_active",
            data: null,
            type: "GET",
            contentType: false,
            processData: false,
            success: function(response) {
                response = req.data(response)
                if (response.code == 200) {
                    if(orderActive.length == 0) {
                        orderActive = response.data
                        setupOrder()
                    }else{
                        if (orderActive[0].transaction_token != response.data[0].transaction_token){
                            orderActive = response.data
                            setupOrder()
                        }
                        else{
                            page -= 1
                            toastr.warning("Tidak ada riwayat lagi")
                        }
                    }
                }
                console.log(page)
            }
        });
    }

    function setupOrder() {
        if (paginationClicked) {
            paginationClicked = false;
            if (orderActive.length == 0) {
                if (paginationDirection == "next") 
                    page -= 1;
                else page += 1;
            }
            paginationDirection = "";
            loadOrder();
            return 
        }

        var index = (page * 10) + 1;

        if (orderActive.length > 0) {
            // Todo Display Cart Item
            $("#order-list-active tbody").html(`${orderActive.map(function(item) {
                var status = Number(item.status)
                var statusRefund = Number(item.is_request_refund)

                if(statusRefund == 1 && status != 4){
                    status = `<span class='badge badge-info'>Mengajukan pembatalan</span>`
                } else {   
                    if (status == 0) status = `<span class='badge badge-secondary'>Belum dibayar</span>`;
                    else if (status == 1) status = "<span class='badge badge-primary'>Pembayaran terverifikasi</span>";
                    else if (status == 2) status = "<span class='badge badge-warning'>Pesanan diproses</span>";
                    else if (status == 3) status = "<span class='badge badge-success'>Pesanan selesai</span>";
                    else if (status == 4) status = "<span class='badge badge-danger'>Pesanan dibatalkan</span>";
                }

                return `
                    <tr>
                        <td>${index++}</td>
                        <td>${item.transaction_token}</td>
                        <td>${req.money(Number(item.total_price).toString(), "Rp ")}</td>
                        <td>${item.created_at}</td>
                        <td>${item.updated_at}</td>
                        <td>${status}</td>
                        <td>
                            <a class="btn btn-primary btn-block"
                            href="${base_url.value + '/anonymous/order/detail/' + item.id}"><i class="icon-eye"></i> Lihat</a>
                        </td>
                    </tr>
                `;
            }).join('')}`)
        } else {
            // Display Total Only
            $("#order-list-active tbody").html(`
                <tr>
                    <td colspan="8">
                        <center>Belum ada pesanan</center>
                    </td>
                </tr>
            `);
        }
    }

    function loadOrderHistory() {
        var order = $("#order-list-history-order").val();
        var order_direction = "DESC";
        switch (Number(order)) {
            case 1: 
                order =  "ip_address";
                order_direction = "ASC";
            break;
            case 2: 
                order =  "ip_address";
                order_direction = "DESC";
            break;
            default:
                order =  "id";
                order_direction = "DESC";
            break;
        }
        var raw = req.raw({
            page: page,
            search: $("#order-list-history-search").val(),
            order_by: order,
            order_direction: order_direction
        })

        console.log(raw)

        var formData = new FormData()
        formData.append("raw", raw)

        $.ajax({
            url: base_url.value + "/anonymous/order/load_order_history",
            data: null,
            type: "GET",
            contentType: false,
            processData: false,
            success: function(response) {
                response = req.data(response)
                if (response.code == 200) {
                    orderHistory = response.data

                    setupOrderHistory()
                }
            }
        });
    }

    function setupOrderHistory() {
        if (paginationClicked) {
            paginationClicked = false;
            if (orderHistory.length == 0) {
                if (paginationDirection == "next") 
                    page -= 1;
                else page += 1;
            }
            paginationDirection = "";
            loadOrder();
            return 
        }

        var index = (page * 10) + 1;

        if (orderHistory.length > 0) {
            // Todo Display Cart Item
            $("#order-list-history tbody").html(`${orderHistory.map(function(item) {
                var status = Number(item.status)

                if (status == 0) status = "<span class='badge badge-secondary'>Belum dibayar</span>";
                else if (status == 1) status = "<span class='badge badge-primary'>Pembayaran terverifikasi</span>";
                else if (status == 2) status = "<span class='badge badge-warning'>Pesanan diproses</span>";
                else if (status == 3) status = "<span class='badge badge-success'>Pesanan selesai</span>";
                else if (status == 4) status = "<span class='badge badge-danger'>Pesanan dibatalkan</span>";

                return `
                    <tr>
                        <td>${index++}</td>
                        <td>${item.transaction_token}</td>
                        <td>${req.money(Number(item.total_price).toString(), "Rp ")}</td>
                        <td>${item.created_at}</td>
                        <td>${item.updated_at}</td>
                        <td>${status}</td>
                        <td>
                            <a class="btn btn-primary btn-block"
                            href="${base_url.value + '/anonymous/order/detail/' + item.id}"><i class="icon-eye"></i> Lihat</a>
                        </td>
                    </tr>
                `;
            }).join('')}`)
        } else {
            // Display Total Only
            $("#order-list-active tbody").html(`
                <tr>
                    <td colspan="8">
                        <center>Belum ada pesanan</center>
                    </td>
                </tr>
            `);
        }
    }
</script>