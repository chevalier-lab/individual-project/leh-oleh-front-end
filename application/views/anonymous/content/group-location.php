<div class="d-flex justify-content-between align-items-center">
    <h4 class="card-title text-danger">Oleh - oleh daerah</h4>
</div>


<ul class="nav nav-tabs row mb-4" id="location-product">

</ul>

<div class="container-fluid mb-4 tab-content" id="product-tab">

</div>

<script>
    // Add border bottom after click location
    $('#location-product').on('click', '.nav-item', function() {
        var element = document.getElementById('selected-location')
        if (element) {
            element.removeAttribute('id')
            element.removeAttribute('style')
        }
        $(this).find('span').attr('id', 'selected-location');
        $(this).find('span').attr('style', 'border-bottom: 4px solid #FFF');
    });

    (function($) {
        "use strict";
        $(window).on("load", function() {
            // Load Product Location
            loadLocationProduct()

        });
    })(jQuery);

    function loadLocationProduct() {
        $.ajax({
            url: base_url.value + "/anonymous/landing/load_product_location",
            data: null,
            type: "GET",
            contentType: false,
            processData: false,
            success: function(response) {
                response = req.data(response)
                var data = response.data
                console.log(data)
                if (response.code == 200) {

                    var tabContent = ''

                    for (let i = 0; i < data.length; i++) {
                        tabContent += `<div class="tab-pane fade ${i == 0 ? 'active show' : ''}" id="tab${i}">
                        <div class="row">`
                        if (data[i].products.length == 0) {

                            tabContent += `
                                <div class="alert alert-primary col-sm-12" role="alert">
                                    Belum ada produk untuk provinsi ini
                                </div>
                            `;

                        } else {

                            for (let j = 0; j < data[i].products.length; j++) {
                            item = data[i].products[j];
                            var price = Number(item.price_selling)
                            var discount = Number(item.discount);
                            var price_divide_discount = ((price / 100) * discount);

                            price = (price - price_divide_discount)

                            if (discount > 0) {
                                price = Number(item.price_selling);
                            }

                            var product_location = base_url.value + "/anonymous/product/detail/" + item.id_m_products

                            tabContent += `
                            <div class="col-sm-6 col-md-4 col-lg-2 border py-3">
                                <div class="position-relative">

                                    <div class="lozad-img-bg"
                                    data-img-bg="${item.uri}"
                                    style="background-size: cover; height: 150px; 
                                    background-position: center;
                                    onclick="location.assign('${product_location}')">
                                    <div class="loader-item"></div>
                                    </div>

                                        ${
                                        (discount > 0) ? `
                                        <div class="mb-3" style="top: 0px; position: absolute">
                                            <a href="javascript:void(0);" class="rounded-right bg-warning px-3 py-2">-${discount}%</a>
                                        </div>
                                        ` : ""
                                    }
                                    <div class="caption-bg fade bg-transparent text-right">
                                        <div class="d-table w-100 h-100 ">
                                            <div class="d-table-cell align-bottom">
                                                <div class="mb-3">
                                                    <a href="javascript:void(0);" class="rounded-left bg-white px-3 py-2 text-danger"><i class="icon-heart"></i></a>
                                                </div>
                                                <div class="mb-4">
                                                    <a href="javascript:void(0);" class="rounded-left bg-white px-3 py-2 text-danger"><i class="icon-bag"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="pt-3"
                                    onclick="location.assign('${product_location}')">
                                    <p class="mb-2"><a href="javascript:void(0);" class="font-weight-bold text-danger">${item.product_name}</a></p>
                                    <div class="clearfix">
                                        <div class="d-inline-block">${
                                            req.money(price.toString(), "Rp ")
                                        }</div>
                                        <ul class="list-inline mb-0 mt-2">
                                            <li class="list-inline-item"><a href="javascript:void(0);" class="text-warning"><i class="fas fa-star"></i></a></li>
                                            <li class="list-inline-item"><a href="javascript:void(0);" class="text-warning"><i class="fas fa-star"></i></a></li>
                                            <li class="list-inline-item"><a href="javascript:void(0);" class="text-warning"><i class="fas fa-star"></i></a></li>
                                            <li class="list-inline-item"><a href="javascript:void(0);"><i class="fas fa-star"></i></a></li>
                                            <li class="list-inline-item"><a href="javascript:void(0);"><i class="fas fa-star"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            `
                        }

                        }
                        tabContent += `
                            </div>
                        </div>`
                    }

                    $('#product-tab').html(tabContent)

                    $("#location-product").html(`${data.map(function(item, index) {
                        return `
                        <li class="nav-item col-md-6 col-lg-3">
                            <a href="#tab${index}" class="rounded d-block" style="background-image: url('${item.uri}');
                                background-size: cover; background-position: center; opacity: 0.9" data-toggle="tab">
                                <div class="p-3 rounded" style="background-color: #222; opacity: 0.8">
                                    <span class="text-white h6" ${index == 0 ? 'style= "border-bottom: 4px solid #FFF" id="selected-location"' : ''}>${(item.province_name.length > 20) ? item.province_name.substring(0, 20) + ".." : item.province_name}</span>
                                </div>
                            </a>
                        </li>
                        `;
                    }).join('')}`);

                    setTimeout(() => {
                        lozad('.lozad-img-bg', {
                            load: function(el) {
                                el.style.backgroundImage = "url('"+el.getAttribute("data-img-bg")+"')";

                                for (var i = 0; i < el.childNodes.length; i++) {
                                    if (el.childNodes[i].className == "loader-item") {
                                        notes = el.childNodes[i];
                                        el.removeChild(notes);
                                        break;
                                    }        
                                }
                                
                            }
                        }).observe();
                    }, 1000);
                }
            }
        });
    }
</script>