<div class="d-flex justify-content-between align-items-center">
    <h4 class="card-title text-danger">Toko Kami</h4>
    <a href="<?= base_url('index.php/anonymous/merchant') ?>" class="bg-danger rounded text-white text-center mb-2">
        <div class="p-2">
            <span>Lihat semua</span>
            <i class="icon-arrow-right-circle align-middle text-white"></i>
        </div>
    </a>
</div>

<div class="container-fluid mb-4">
    <div class="row" id="merchant-list"></div>
</div>

<script>
    (function ($) {
        "use strict";

        $(document).scrollTop(0);

        loadMerchants();
    })(jQuery);

    function loadMerchants() {

        $.ajax({
            url: base_url.value + "/anonymous/landing/load_merchant_random",
            data: null,
            type: "GET",
            contentType: false,
            processData: false,
            success: function(response) {
                response = req.data(response)
                if (response.code == 200) {
                    var data = response.data

                    // Render Product
                    data.forEach(function(item, position) {
                        var market_location = base_url.value + "/anonymous/landing/detailMerchant/" + item.id;
                        var address = item.address + ", " + item.area + ", " + item.suburb + ", " + item.city + ", " + item.province + ", " + item.postcode

                        if (position < 6) {

                            $("#merchant-list").append(`
                                <div class="col-sm-6 col-md-4 col-lg-2 border py-3">
                                    <div class="position-relative">
                                        <div class="lozad-img-bg"
                                        data-img-bg="${item.market_uri}"
                                        style="background-size: cover; height: 150px; 
                                        background-position: center;
                                        onclick="location.assign('${market_location}')">
                                        <div class="loader-item"></div>
                                        </div>

                                        <div class="caption-bg fade bg-transparent text-right">
                                            <div class="d-table w-100 h-100 ">
                                                <div class="d-table-cell align-bottom">
                                                    <div class="mb-3">
                                                        <a href="javascript:void(0);" class="rounded-left bg-white px-3 py-2 text-danger"><i class="icon-heart"></i></a>
                                                    </div>
                                                    <div class="mb-4">
                                                        <a href="javascript:void(0);" class="rounded-left bg-white px-3 py-2 text-danger"><i class="icon-bag"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="pt-3"
                                        onclick="location.assign('${market_location}')">
                                        <p class="mb-2"><a href="javascript:void(0);" class="font-weight-bold text-danger">${(item.market_name.length > 25) ? item.market_name.substring(0, 25) + "..." : item.market_name}</a></p>
                                        <div class="clearfix">
                                            <div class="d-inline-block">${(address.length > 30) ? address.substring(0, 30) + "..." : address}</div>
                                            <ul class="list-inline mb-0 mt-2">
                                                <li class="list-inline-item"><a href="javascript:void(0);" class="text-warning"><i class="fas fa-star"></i></a></li>
                                                <li class="list-inline-item"><a href="javascript:void(0);" class="text-warning"><i class="fas fa-star"></i></a></li>
                                                <li class="list-inline-item"><a href="javascript:void(0);" class="text-warning"><i class="fas fa-star"></i></a></li>
                                                <li class="list-inline-item"><a href="javascript:void(0);"><i class="fas fa-star"></i></a></li>
                                                <li class="list-inline-item"><a href="javascript:void(0);"><i class="fas fa-star"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            `);
                            
                        }
                    })

                    setTimeout(() => {
                        lozad('.lozad-img-bg', {
                            load: function(el) {
                                el.style.backgroundImage = "url('"+el.getAttribute("data-img-bg")+"')";

                                for (var i = 0; i < el.childNodes.length; i++) {
                                    if (el.childNodes[i].className == "loader-item") {
                                        notes = el.childNodes[i];
                                        el.removeChild(notes);
                                        break;
                                    }        
                                }
                                
                            }
                        }).observe();
                    }, 1000);
                }
            }
        });
    }
</script>