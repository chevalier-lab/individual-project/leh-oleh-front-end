<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view('components/head'); ?>

        <!-- START: Template CSS-->
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/bootstrap/css/bootstrap.min.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.theme.min.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/simple-line-icons/css/simple-line-icons.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/flags-icon/css/flag-icon.min.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/fontawesome/css/all.min.css'); ?>">
        <!-- END Template CSS-->     

        <!-- START: Page CSS-->   
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/social-button/bootstrap-social.css'); ?>"/>   
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/sweetalert/sweetalert.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/fancybox/jquery.fancybox.min.css'); ?>">  
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/toastr/toastr.min.css'); ?>" />
        <!-- END: Page CSS-->

        <!-- START: Custom CSS-->
        <link rel="stylesheet" href="<?= base_url('assets/dist/css/main.css'); ?>">
        <!-- END: Custom CSS-->

        <style>
            .star-rating .fas{color: yellow;}
            .star-rating .fa-star:hover{cursor:pointer;}
        </style>
    </head>
    <body id="main-container" class="default bg-white"
    oncontextmenu="return false;">

    <input type="hidden" name="base_url" id="base_url" value="<?= base_url("index.php"); ?>">
    <input type="hidden" name="api_url" id="api_url" value="<?= API_URI; ?>">

    <input type="hidden" name="order_id" id="order_id" value="<?= isset($order_id) ? $order_id : ""; ?>">

        <!-- START: Pre Loader-->
        <div class="se-pre-con">
            <div class="loader"></div>
        </div>
        <!-- END: Pre Loader-->

        <!-- START: Template JS-->
        <script src="<?= base_url('assets/dist/vendors/jquery/jquery-3.3.1.min.js'); ?>"></script>
        <script src="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.js'); ?>"></script>
        <script src="<?= base_url('assets/dist/vendors/moment/moment.js'); ?>"></script>
        <script src="<?= base_url('assets/dist/vendors/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
        <script src="<?= base_url('assets/dist/vendors/slimscroll/jquery.slimscroll.min.js'); ?>"></script>
        <script src="<?= base_url('assets/dist/js/auth/request.js'); ?>"></script>
        <script src="<?= base_url('assets/dist/vendors/toastr/toastr.min.js'); ?>"></script>
        <script src="https://cdn.jsdelivr.net/npm/lozad/dist/lozad.min.js"></script>
        <script>
            var base_url = document.getElementById("base_url");
            var api_url = document.getElementById("api_url");
            var order_id = document.getElementById("order_id");
            var auth = document.getElementById("auth");
        </script>
        <!-- END: Template JS--> 

        <!-- Facebook Pixel Code -->
        <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '3542009505894280');
        fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=3542009505894280&ev=PageView&noscript=1"
        /></noscript>
        <!-- End Facebook Pixel Code -->
        
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-E6E3SRPJ11"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-E6E3SRPJ11');
        </script>

        <!-- START: APP JS-->
        <script src="<?= base_url('assets/dist/js/app.js'); ?>"></script>
        <!-- END: APP JS-->

        <!-- START: Main Content-->
        <?php
            // Load Secondary Menu
            echo ('<div id="header-fix" class="header">');
                $this->load->view("components/menus/secondary-menu");
                $this->load->view("components/menus/top-bar");
            echo ('</div>');

            // Load Page
            $this->load->view("components/modals/categories");
        ?>
        <div class="container mt-4 mb-4">
            <?php 
                // Load Cart Lists
                $this->load->view('anonymous/content/order-detail');  
            ?>
        </div>

        <div 
            style="background-color: #fbfbfb; 
                font-color: rgba(0,0,0,.54);
                border-top: 1px solid rgba(0,0,0,.1)">
            <div class="container">
                <?php
                    // Load Footer
                    $this->load->view('anonymous/content/footer');
                ?>
            </div>
        </div>
        <!-- END: Content-->

        <!-- START: Page Script JS-->
        <?php
            if (isset($error)) {
                $this->load->view("components/error-modal", array(
                    "errorModalTitle" => $error["title"],
                    "errorModalContent" => $error["content"],
                    "errorModalDetail" => $error["details"]
                ));
            }

            // Review Modal
            $this->load->view("components/modals/form", array(
                "hideModal" => "hide",
                "idElement" => "modal-review",
                "modalTitle" => "Ulasan Produk",
                "iconTitle" => "icon-pencil",
                "modalActionForm" => "#",
                "modalContentForm" => '
                <input type="hidden" id="id-product-review">
                <div class="form-group mb-3">
                    <label>Rating</label>
                    <div class="star-rating">
                        <span class="h5 far fa-star" data-rating="1" data-toggle="tooltip" data-original-title="Sangat Buruk"></span>
                        <span class="h5 far fa-star" data-rating="2" data-toggle="tooltip" data-original-title="Buruk"></span>
                        <span class="h5 far fa-star" data-rating="3" data-toggle="tooltip" data-original-title="Bagus"></span>
                        <span class="h5 far fa-star" data-rating="4" data-toggle="tooltip" data-original-title="Sangat Bagus"></span>
                        <span class="h5 far fa-star" data-rating="5" data-toggle="tooltip" data-original-title="Sempurna"></span>
                        <input type="hidden" id="rating" name="rating" class="rating-value" value="0">
                    </div>
                </div>
                <div class="form-group mb-3">
                    <label for="review">Ulasan</label>
                    <textarea class="form-control" id="review" rows="3"></textarea>
                </div>
                ',
                "modalButtonForm" => '<button type="button" 
                    class="btn btn-danger add-todo"
                    id="review-button">Kirim Ulasan</button>
                    <div id="review-button-loader">
                    </div>
                    '
            ));

            // Complain Modal
            $this->load->view("components/modals/form", array(
                "hideModal" => "hide",
                "idElement" => "modal-complain",
                "modalTitle" => "Komplain Produk",
                "iconTitle" => "icon-pencil",
                "modalActionForm" => "#",
                "modalContentForm" => '
                <input type="hidden" id="id-product-complain">
                <div class="form-group mb-3">
                    <label for="subject">Subjek</label>
                    <input type="text" id="subject" class="form-control" placeholder="Contoh: Barang rusak"/> 
                </div>
                <div class="form-group mb-3">
                    <label for="description">Deskripsi</label>
                    <textarea class="form-control" id="description" rows="3" placeholder="Contoh: Barang tidak berfungsi"></textarea>
                </div>
                ',
                "modalButtonForm" => '<button type="button" 
                    class="btn btn-danger add-todo"
                    id="complain-button">Kirim Komplain</button>
                    <div id="complain-button-loader">
                    </div>
                    '
            ));
        ?>

        <!-- START: Page Vendor JS-->
        <script src="<?= base_url('assets/dist/vendors/fancybox/jquery.fancybox.min.js'); ?>"></script>
        <!-- END: Page Vendor JS-->

        <!-- START: Page JS-->
        <script src="<?= base_url('assets/dist/js/app.js'); ?>"></script>
        <script>
        var $star_rating = $('.star-rating .fa-star');

        var SetRatingStar = function() {
        return $star_rating.each(function() {
            if (parseInt($star_rating.siblings('input.rating-value').val()) >= parseInt($(this).data('rating'))) {
            return $(this).removeClass('far').addClass('fas');
            } else {
            return $(this).removeClass('fas').addClass('far');
            }
        });
        };

        $star_rating.on('click', function() {
            $star_rating.siblings('input.rating-value').val($(this).data('rating'));
            console.log($star_rating.siblings('input.rating-value').val())
            return SetRatingStar();
        });

        SetRatingStar();
        $(document).ready(function() {
            console.log
        });

        (function($) {
            "use strict";
            $(window).on("load", function() {
                // Review Product
                $("#review-button").on("click", function() {
                    if ($('#rating').val() == 0) {
                        toastr.error("Error: Rating kosong")
                    } else if($('#review').val().trim().length == 0){
                        toastr.error("Error: Ulasan kosong")
                    }else {
                        $("#review-button").hide();
                        $("#review-button-loader").html(`
                            <div class="loader-item"></div>
                        `);

                        var raw = req.raw({
                            id_u_user_transaction_products: $("#id-product-review").val(),
                            rating: $("#rating").val(),
                            review: $("#review").val(),
                        })
                        var formData = new FormData();
                        formData.append("raw", raw)

                        $.ajax({
                            url: base_url.value + "/anonymous/order/review_product",
                            data: formData,
                            type: "POST",
                            contentType: false,
                            processData: false,
                            success: function(response) {
                                response = req.data(response)

                                if (response.code == 200) {
                                    toastr.success("Berhasil mengirim ulasan");
                                    setTimeout(() => {
                                        location.reload();
                                    }, 1000);
                                } else {
                                    toastr.error(response.message);
                                    $("#review-button").show();
                                    $("#review-button-loader").html(``);
                                }
                            }
                        });
                    }
                });
                // Complain Product
                $("#complain-button").on("click", function() {
                    if ($('#subject').val().trim().length == 0) {
                        toastr.error("Error: Subjek harus diisi")
                    } else if($('#description').val().trim().length == 0){
                        toastr.error("Error: Deskripsi harus diisi")
                    }else {
                        
                        $("#complain-button").hide();
                        $("#complain-button-loader").html(`
                            <div class="loader-item"></div>
                        `);

                        var raw = req.raw({
                            id_u_user_transaction_products: $("#id-product-complain").val(),
                            subject: $("#subject").val(),
                            description: $("#description").val(),
                            status: 1
                        })
                        var formData = new FormData();
                        formData.append("raw", raw)

                        $.ajax({
                            url: base_url.value + "/anonymous/order/complain_product",
                            data: formData,
                            type: "POST",
                            contentType: false,
                            processData: false,
                            success: function(response) {
                                response = req.data(response)
                                console.log(response)

                                if (response.code == 200) {
                                    toastr.success("Berhasil mengirim komplain");
                                    setTimeout(() => {
                                        location.reload();
                                    }, 1000);
                                } else {
                                    $("#complain-button").show();
                                    $("#complain-button-loader").html(``);
                                    toastr.error(response.message);
                                }
                            }
                        });
                    }
                });
            });
        })(jQuery);
        </script>
        <!-- END: Page Script JS-->
    </body>
</html>