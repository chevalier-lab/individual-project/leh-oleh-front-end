<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view('components/head'); ?>

        <!-- START: Template CSS-->
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/bootstrap/css/bootstrap.min.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.theme.min.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/simple-line-icons/css/simple-line-icons.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/flags-icon/css/flag-icon.min.css'); ?>"> 
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/fontawesome/css/all.min.css'); ?>"> 
        <!-- END Template CSS-->     

        <!-- START: Page CSS-->   
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/social-button/bootstrap-social.css'); ?>"/>   
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/sweetalert/sweetalert.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/fancybox/jquery.fancybox.min.css'); ?>">  
        <!-- END: Page CSS-->

        <!-- START: Custom CSS-->
        <link rel="stylesheet" href="<?= base_url('assets/dist/css/main.css'); ?>">
        <!-- END: Custom CSS-->
    </head>
    <body id="main-container" class="default bg-white"
    oncontextmenu="return false;">

        <input type="hidden" name="base_url" 
            id="base_url" value="<?= base_url("index.php"); ?>">
        <input type="hidden" name="api_url" 
            id="api_url" value="<?= API_URI; ?>">
        <input type="hidden" name="auth" 
            id="auth" value="<?= isset($auth->token) ? $auth->token : ''; ?>">

        <!-- START: Pre Loader-->
        <div class="se-pre-con">
            <div class="loader"></div>
        </div>
        <!-- END: Pre Loader-->

        <!-- START: Template JS-->
        <script src="<?= base_url('assets/dist/vendors/jquery/jquery-3.3.1.min.js'); ?>"></script>
        <script src="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.js'); ?>"></script>
        <script src="<?= base_url('assets/dist/vendors/moment/moment.js'); ?>"></script>
        <script src="<?= base_url('assets/dist/vendors/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
        <script src="<?= base_url('assets/dist/vendors/slimscroll/jquery.slimscroll.min.js'); ?>"></script>
        <script src="https://cdn.jsdelivr.net/npm/lozad/dist/lozad.min.js"></script>
        <!-- END: Template JS-->

        <!-- START: Page Vendor JS-->
        <script src="<?= base_url('assets/dist/js/auth/request.js'); ?>"></script>
        <script>
            var base_url = document.getElementById("base_url");
            var api_url = document.getElementById("api_url");
            var auth = document.getElementById("auth");
        </script>
        <!-- END: Template JS--> 

        <!-- Facebook Pixel Code -->
        <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '3542009505894280');
        fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=3542009505894280&ev=PageView&noscript=1"
        /></noscript>
        <!-- End Facebook Pixel Code -->

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-E6E3SRPJ11"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-E6E3SRPJ11');
        </script>

        <!-- START: APP JS-->
        <script src="<?= base_url('assets/dist/js/app.js'); ?>"></script>
        <!-- END: APP JS-->

        <!-- START: Main Content-->
        <?php
            // Load Secondary Menu
            echo ('<div id="header-fix" class="header">');
                $this->load->view("components/menus/secondary-menu");
                $this->load->view("components/menus/top-bar");
            echo ('</div>');

            // Load Page
            $this->load->view("components/modals/categories");
        ?>
        <div class="container mt-4 mb-2">
            <div class="row">
                
                <div class="d-flex justify-content-between align-items-center">
                    <h4 class="card-title text-danger">Toko Kami</h4>
                </div>

            </div>
        </div>

        <div class="container mb-4">
            <div class="row" id="merchant-list"></div>
        </div>

        <div 
            style="background-color: #fbfbfb; 
                font-color: rgba(0,0,0,.54);
                border-top: 1px solid rgba(0,0,0,.1)">
            <div class="container">
                <?php
                    // Load Footer
                    $this->load->view('anonymous/content/footer');
                ?>
            </div>
        </div>
        <!-- END: Content-->

        <!-- START: Page Script JS-->
        <?php
            if (isset($error)) {
                $this->load->view("components/error-modal", array(
                    "errorModalTitle" => $error["title"],
                    "errorModalContent" => $error["content"],
                    "errorModalDetail" => $error["details"]
                ));
            }

            $this->load->view("components/modals/lost-password", array(
                "hideModal" => "hide",
                "isSuccess" => false
            ));
        ?>

        <!-- START: Page Vendor JS-->
        <script src="<?= base_url('assets/dist/vendors/fancybox/jquery.fancybox.min.js'); ?>"></script>
        <!-- END: Page Vendor JS-->

        <!-- START: Page JS-->
        <script src="<?= base_url('assets/dist/js/gallery.script.js'); ?>"></script>
        <!-- END: Page Script JS-->

        <script>
            (function ($) {
                "use strict";

                $(document).scrollTop(0);

                loadMerchants();
            })(jQuery);

            function loadMerchants() {
                $.ajax({
                    url: base_url.value + "/anonymous/landing/load_merchants",
                    data: null,
                    type: "GET",
                    contentType: false,
                    processData: false,
                    success: function(response) {
                        response = req.data(response)
                        if (response.code == 200) {
                            var data = response.data

                            // Render Product
                            data.forEach(function(item, position) {
                                var market_location = base_url.value + "/anonymous/landing/detailMerchant/" + item.id;
                                var address = item.address + ", " + item.area + ", " + item.suburb + ", " + item.city + ", " + item.province + ", " + item.postcode

                                $("#merchant-list").append(`
                                    <div class="col-sm-6 col-md-4 col-lg-2 border py-3">
                                        <div class="position-relative">
                                            <div class="lozad-img-bg"
                                            data-img-bg="${item.market_uri}"
                                            style="background-size: cover; height: 150px; 
                                            background-position: center;
                                            onclick="location.assign('${market_location}')">
                                            <div class="loader-item"></div>
                                            </div>

                                            <div class="caption-bg fade bg-transparent text-right">
                                                <div class="d-table w-100 h-100 ">
                                                    <div class="d-table-cell align-bottom">
                                                        <div class="mb-3">
                                                            <a href="javascript:void(0);" class="rounded-left bg-white px-3 py-2 text-danger"><i class="icon-heart"></i></a>
                                                        </div>
                                                        <div class="mb-4">
                                                            <a href="javascript:void(0);" class="rounded-left bg-white px-3 py-2 text-danger"><i class="icon-bag"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="pt-3"
                                            onclick="location.assign('${market_location}')">
                                            <p class="mb-2"><a href="javascript:void(0);" class="font-weight-bold text-danger">${(item.market_name.length > 25) ? item.market_name.substring(0, 25) + "..." : item.market_name}</a></p>
                                            <div class="clearfix">
                                                <div class="d-inline-block">${(address.length > 30) ? address.substring(0, 30) + "..." : address}</div>
                                                <ul class="list-inline mb-0 mt-2">
                                                    <li class="list-inline-item"><a href="javascript:void(0);" class="text-warning"><i class="fas fa-star"></i></a></li>
                                                    <li class="list-inline-item"><a href="javascript:void(0);" class="text-warning"><i class="fas fa-star"></i></a></li>
                                                    <li class="list-inline-item"><a href="javascript:void(0);" class="text-warning"><i class="fas fa-star"></i></a></li>
                                                    <li class="list-inline-item"><a href="javascript:void(0);"><i class="fas fa-star"></i></a></li>
                                                    <li class="list-inline-item"><a href="javascript:void(0);"><i class="fas fa-star"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                `);
                                    
                            })

                            setTimeout(() => {
                                lozad('.lozad-img-bg', {
                                    load: function(el) {
                                        el.style.backgroundImage = "url('"+el.getAttribute("data-img-bg")+"')";

                                        for (var i = 0; i < el.childNodes.length; i++) {
                                            if (el.childNodes[i].className == "loader-item") {
                                                notes = el.childNodes[i];
                                                el.removeChild(notes);
                                                break;
                                            }        
                                        }

                                    }
                                }).observe();
                            }, 1000);
                        }
                    }
                });
            }
        </script>
    </body>
</html>