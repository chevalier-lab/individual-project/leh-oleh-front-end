<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('components/head'); ?>

    <!-- START: Template CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.theme.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/simple-line-icons/css/simple-line-icons.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/flags-icon/css/flag-icon.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/fontawesome/css/all.min.css'); ?>">
    <!-- END Template CSS-->

    <!-- START: Page CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/social-button/bootstrap-social.css'); ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/sweetalert/sweetalert.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/fancybox/jquery.fancybox.min.css'); ?>">
    <!-- END: Page CSS-->

    <!-- START: Custom CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/css/main.css'); ?>">
    <!-- END: Custom CSS-->
</head>

<body id="main-container" class="default bg-white" oncontextmenu="return false;">

    <input type="hidden" name="base_url" id="base_url" value="<?= base_url("index.php"); ?>">
    <input type="hidden" name="api_url" id="api_url" value="<?= API_URI; ?>">
    <input type="hidden" name="auth" id="auth" value="<?= isset($auth->token) ? $auth->token : ''; ?>">
    <input type="hidden" name="_main_menu" id="_main_menu" value="<?= $main_menu; ?>">
    <input type="hidden" name="id_merchant" id="id_merchant" value="<?= $id_merchant; ?>">

    <!-- START: Pre Loader-->
    <div class="se-pre-con">
        <div class="loader"></div>
    </div>
    <!-- END: Pre Loader-->

    <!-- START: Template JS-->
    <script src="<?= base_url('assets/dist/vendors/jquery/jquery-3.3.1.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/moment/moment.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/slimscroll/jquery.slimscroll.min.js'); ?>"></script>
    <script src="https://cdn.jsdelivr.net/npm/lozad/dist/lozad.min.js"></script>
    <!-- END: Template JS-->

    <!-- START: APP JS-->
    <script src="<?= base_url('assets/dist/js/app.js'); ?>"></script>
    <!-- END: Template JS-->

    <!-- START: Page Vendor JS-->
    <script src="<?= base_url('assets/dist/js/auth/request.js'); ?>"></script>
    <script>
        var base_url = document.getElementById("base_url");
        var api_url = document.getElementById("api_url");
        var auth = document.getElementById("auth");
        var _main_menu = document.getElementById("_main_menu");
        var id_merchant = document.getElementById("id_merchant");
    </script>

    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window, document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '3542009505894280');
    fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=3542009505894280&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-E6E3SRPJ11"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-E6E3SRPJ11');
        </script>

    <!-- START: Main Content-->
    <?php
    // Load Secondary Menu
    echo ('<div id="header-fix" class="header">');
    $this->load->view("components/menus/secondary-menu");
    $this->load->view("components/menus/top-bar");
    echo ('</div>');
    ?>

    <div class="chat-screen mt-2 mb-4">
        <a href="javascript:void(0);" class="chat-contact round-button d-inline-block d-lg-none"><i class="icon-menu"></i></a>
        <!-- <a href="javascript:void(0);" class="chat-profile d-inline-block d-lg-none"><img class="img-fluid  rounded-circle" src="dist/images/team-3.jpg" width="30" alt=""></a> -->
        <div class="row row-eq-height d-flex justify-content-center">
            <?php $this->load->view("anonymous/chat/chat-list"); ?>
            <?php $this->load->view("anonymous/chat/chat-room"); ?>
        </div>
    </div>

    <div style="background-color: #fbfbfb; 
                font-color: rgba(0,0,0,.54);
                border-top: 1px solid rgba(0,0,0,.1)">
        <div class="container">
            <?php
            // Load Footer
            $this->load->view('anonymous/content/footer');
            ?>
        </div>
    </div>
    <!-- END: Content-->

    <!-- START: Page Script JS-->
    <?php
    if (isset($error)) {
        $this->load->view("components/error-modal", array(
            "errorModalTitle" => $error["title"],
            "errorModalContent" => $error["content"],
            "errorModalDetail" => $error["details"]
        ));
    }

    $this->load->view("components/modals/lost-password", array(
        "hideModal" => "hide",
        "isSuccess" => false
    ));

    // Create Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "new-contact",
        "modalTitle" => "Add New Contact",
        "iconTitle" => "icon-user-follow",
        "modalActionForm" => "#",
        "modalContentForm" => '
        <div class="form-group">
            <label for="emails" class="col-form-label">Name</label>
            <input type="text" class="form-control" id="name">
        </div>
        <div class="form-group">
            <label for="emails" class="col-form-label">Email addresses</label>
            <input type="text" class="form-control" id="emails">
        </div>
        <div class="form-group">
            <label for="emails" class="col-form-label">Phone</label>
            <input type="text" class="form-control" id="phone">
        </div>
        <div class="form-group">
            <label for="message" class="col-form-label">Message</label>
            <textarea class="form-control" id="message"></textarea>
        </div>
        ',
        "modalButtonForm" => '<button type="submit" class="btn btn-primary add-todo" id="create-category-button">Create Category</button>'
    ));
    ?>

    <!-- START: Page Vendor JS-->
    <script src="<?= base_url('assets/dist/vendors/fancybox/jquery.fancybox.min.js'); ?>"></script>
    <!-- END: Page Vendor JS-->

    <!-- START: Page JS-->
    <script src="<?= base_url('assets/dist/js/gallery.script.js'); ?>"></script>
    <!-- <script src="https://cdn.socket.io/socket.io-3.0.1.min.js"></script> -->
    <!-- <script src="https://cdn.socket.io/socket.io-1.0.6.js"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.3.0/socket.io.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/rollups/aes.js"></script>
    <!-- END: Page Script JS-->

    <script>
        // Toggle active chat list
        $('ul.nav li').click(function() {
            $('ul.nav > li').removeClass('active');
            $(this).addClass('active');
        });

        var chatRoom;
        var chats = [];

        // const socket = io('http://213.190.4.40:2020', { path: '/leh-oleh/chat' });
        const socket = io('https://leholeh-websocket.herokuapp.com', { path: '/leh-oleh/chat' });

        (function($) {
            "use strict";
            $(window).on("load", function() {
                // Load Banner
                loadChatRoom();
                socket.heartbeatTimeout = 20000

                $("#btn-send").on("click", function() {
                    var message = $("#detail-chat-message").val();
                    var enc = CryptoJS.AES.encrypt(message, 'secret-chat').toString()
                    console.log(message);
                    socket.emit('chat-message', {
                        message: message, 
                        date: "<?= date('Y-m-d H:i:s'); ?>"
                    })
                    $("#detail-chat-message").val('')

                    var raw = req.raw({                        
                        content: enc
                    });

                    console.log(raw);

                    var formData = new FormData();
                    formData.append("raw", raw);

                    $.ajax({
                        url: base_url.value + "/services/chatMerchant/send_message/" + chatRoom.id,
                        data: formData,
                        type: "POST",
                        contentType: false,
                        processData: false,
                        success: function(response) {
                            response = req.data(response)
                            if (response.code == 200) {
                            }
                        }
                    });
                });
            });
        })(jQuery);

        function loadChatRoom() {
            $.ajax({
                url: base_url.value + "/services/chatMerchant/check_chat_room/" + id_merchant.value,
                data: null,
                type: "GET",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.code == 200) {
                        var data = response.data

                        if (data == null) {
                            doCreateChatRoom();
                        } else {
                            chatRoom = data
                            setupChatRoom()
                        }
                    }
                }
            });
        }

        function doCreateChatRoom() {
            var raw = req.raw({
                id_u_user_is_merchant: id_merchant.value
            });

            var formData = new FormData();
            formData.append("raw", raw);

            $.ajax({
                url: base_url.value + "/services/chatMerchant/create_chat_room",
                data: formData,
                type: "POST",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.code == 200) {
                        loadChatRoom()
                    }
                }
            });
        }

        function setupChatRoom() {
            var data = chatRoom;

            $("#chat-tab-person").html(`
                <img class="img-fluid mr-3 rounded-circle lozad" 
                style="max-width: 48px"
                data-src="${data.face_user_merchant}"
                src="${base_url.value + '/../assets/dist/images/author2.jpg'}" alt="">
                <div class="media-body align-self-center mt-0 color-primary d-flex">
                    <div class="message-content"> <b class="mb-1 font-weight-bold d-flex">${data.market_name}</b>
                    ${data.full_name_user_merchant}
                        <br>
                        <small class="body-color">${data.updated_at}</small></div>
                </div>
            `);

            $("#detail-chat-topbar").html(`
                <img class="img-fluid  mr-3 rounded-circle lozad" 
                data-src="${data.face_user_merchant}"
                src="${base_url.value + '/../assets/dist/images/author2.jpg'}" width="54" alt="">
                <div class="media-body align-self-center mt-0  d-flex">
                    <div class="message-content">
                        <h6 class="mb-1 font-weight-bold d-flex">${data.market_name}</h6>
                        ${data.full_name_user_merchant}
                        <br>
                        <small class="body-color" id="friend-status">${data.updated_at}</small></div>
                    </div>
                </div>
            `);

            loadChatDetail();

            setupSocket();

            setInterval(() => {
                const observer = lozad();
                observer.observe();
            }, 2000);
        }

        function loadChatDetail() {
            var data = chatRoom;

            $.ajax({
                url: base_url.value + "/services/chatMerchant/detail_chat_room/" + data.id,
                data: null,
                type: "GET",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.code == 200) {
                        var data = response.data
                        chats = data.chat;

                        setupChatDetail();
                    }
                }
            });
        }

        function setupChatDetail() {
            // if (chats.length == 0) {
            //     $("#detail-chat-worksheet").html(`
            //         <center>
            //             <p>Belum ada chat</p>
            //         </center>
            //     `);
            // }

            var data = chatRoom;

            function decrypt(msg) {
                let bytes = CryptoJS.AES.decrypt(msg, 'secret-chat')
                return bytes.toString(CryptoJS.enc.Utf8)
            }

            $("#detail-chat-worksheet").html(`${chats.map(function(item) {
                var text = decrypt(item.content);
                if (item.id_m_users == data.id_m_user_customer) {
                    return `
                        <div class="media d-flex mb-4">
                            <div class="p-3 ml-auto speech-bubble">
                                ${text}<br>
                                <small>${req.date(item.created_at)}</small>
                            </div>
                            <div class="ml-4"><a href="javascript:void(0);"><img 
                                data-src="${item.uri}"
                                style="max-width: 48px"
                                src="${base_url.value + '/../assets/dist/images/author2.jpg'}" alt="" 
                            class="img-fluid rounded-circle lozad" /></a></div>
                        </div>
                    `
                } else {
                    return `
                        <div class="media d-flex mb-4">
                            <div class="mr-4 thumb-img"><a href="javascript:void(0);"><img 
                                style="max-width: 48px"
                                data-src="${item.uri}"
                                src="${base_url.value + '/../assets/dist/images/author2.jpg'}" alt="" 
                            class="img-fluid rounded-circle lozad" /></a></div>
                            <div class="p-3 mr-auto speech-bubble alt">
                                ${text}
                                <small>${req.date(item.created_at)}</small>
                            </div>
                        </div>
                    `
                }
            }).join('')}`);
            
        }

        function setupSocket() {
            var data = chatRoom;
            var prepareEmit = { userId: data.username_customer, token: data.chat_token };
            console.log("prepareEmit", prepareEmit);
            socket.emit('token', prepareEmit)

            // get user info
            socket.on('user-info', ({ token, friend }) => {
                // get friend data
                const user = friend.filter(user => user.userId !== data.username_customer)

                // set user-status
                if (user.length !== 0) {
                    $("#friend-status").text("online")
                } else {
                    $("#friend-status").text("offline")
                }
                
                console.log(user);
            })

            // get status message from server 
            socket.on('status', ({ status, user }) => { 
                if (user.userId !== data.username_customer && status == 'offline') {
                    $("#friend-status").text("offline")
                }

                // log status
                console.log(`${user.userId} is ${status}`);
            })

            // get message from server
            socket.on('message', res => {
                console.log(res)

                // check user
                if (res.username != data.username_merchant) {
                    $("#detail-chat-worksheet").append(`
                        <div class="media d-flex  mb-4">
                            <div class="p-3 ml-auto speech-bubble">
                                ${res.text}<br>
                                <small>${req.date(res.time)}</small>
                            </div>
                            <div class="ml-4"><a href="javascript:void(0);"><img 
                                data-src="${data.face_user_customer}"
                                style="max-width: 48px"
                                src="${base_url.value + '/../assets/dist/images/author2.jpg'}" alt="" 
                            class="img-fluid rounded-circle lozad" /></a></div>
                        </div>
                    `);
                } else {
                    $("#detail-chat-worksheet").append(`
                        <div class="media d-flex mb-4">
                            <div class="mr-4 thumb-img"><a href="javascript:void(0);"><img 
                                style="max-width: 48px"
                                data-src="${data.face_user_merchant}"
                                src="${base_url.value + '/../assets/dist/images/author2.jpg'}" alt="" 
                            class="img-fluid rounded-circle lozad" /></a></div>
                            <div class="p-3 mr-auto speech-bubble alt">
                                ${res.text}<br>
                                <small>${req.date(res.time)}</small>
                            </div>
                        </div>
                    `);
                }

                // scroll down automatically
                document.getElementById('detail-chat-worksheet').scrollTop = document.getElementById('detail-chat-worksheet').scrollHeight
            })
        }


    </script>
</body>

</html>