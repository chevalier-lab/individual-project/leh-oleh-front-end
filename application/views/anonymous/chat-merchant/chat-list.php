<div class="col-12 col-lg-4 col-xl-3 mt-lg-3 pr-lg-0">
    <div class="card border h-100 chat-contact-list">
        <div class="card-header d-flex justify-content-between align-items-center">
            <ul class="nav nav-tabs" id="tabs-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active font-weight-bold" id="tabs-day-tab" data-toggle="tab" href="#tabs-day" role="tab" aria-controls="tabs-day" aria-selected="true">
                    Chat Dengan Toko <span id="merchant-name-chat"></span>
                    </a>
                </li>
            </ul>
        </div>
        <div class="tab-content">
            <div class="tab-pane fade show active" id="tabs-day" role="tabpanel" aria-labelledby="tabs-day-tab">
                <ul class="nav flex-column chat-menu" id="myTab" role="tablist">
                    <li class="nav-item px-3">
                        <a class="nav-link online-status" data-toggle="tab" href="#tab1" role="tab" aria-selected="true">
                            <div class="media d-block d-flex text-left py-2" id="chat-tab-person">
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>