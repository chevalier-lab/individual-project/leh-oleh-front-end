<div class="col-12 col-lg-4 col-xl-6 mt-3 pl-lg-0 pr-lg-0">
    <div class="card border h-100 rounded-0">
        <div class="card-body p-0">
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show" role="tabpanel">
                    <div class="scrollerchat p-3">
                        <div class="media d-flex  mb-4">
                            <div class="p-3 h1">
                                Select chat
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane fade show active" id="tab1" role="tabpanel">
                    <ul class="nav flex-column chat-menu" id="myTab3" role="tablist">
                        <li class="nav-item active px-3 px-md-1 px-xl-3">
                            <div class="media d-block d-flex text-left py-2" id="detail-chat-topbar">
                            </div>
                        </li>
                    </ul>
                    <div class="scrollerchat p-3" id="detail-chat-worksheet"
                    style="overflow-y: auto; height: 400px">
                    </div>
                    <div class="border-top theme-border px-2 py-3 d-flex position-relative chat-box">
                        <input type="text" class="form-control mr-2" placeholder="Type message here ..."
                        id="detail-chat-message" />
                        <a href="javascript:void(0)" class="p-2 ml-2 rounded line-height-21 bg-primary text-white"
                        id="btn-send"><i class="icon-cursor align-middle"></i> Kirim</a>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>