<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('components/head'); ?>

    <!-- START: Template CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.theme.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/simple-line-icons/css/simple-line-icons.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/flags-icon/css/flag-icon.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/fontawesome/css/all.min.css'); ?>">
    <!-- END Template CSS-->

    <!-- START: Page CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/social-button/bootstrap-social.css'); ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/sweetalert/sweetalert.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/fancybox/jquery.fancybox.min.css'); ?>">
    <!-- END: Page CSS-->

    <!-- START: Custom CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/css/main.css'); ?>">
    <!-- END: Custom CSS-->
</head>

<body id="main-container" class="default bg-white" oncontextmenu="return false;">

    <input type="hidden" name="base_url" id="base_url" value="<?= base_url("index.php"); ?>">
    <input type="hidden" name="api_url" id="api_url" value="<?= API_URI; ?>">
    <input type="hidden" name="auth" id="auth" value="<?= isset($auth->token) ? $auth->token : ''; ?>">
    <input type="hidden" name="_main_menu" id="_main_menu" value="<?= $main_menu; ?>">

    <!-- START: Pre Loader-->
    <div class="se-pre-con">
        <div class="loader"></div>
    </div>
    <!-- END: Pre Loader-->

    <!-- START: Template JS-->
    <script src="<?= base_url('assets/dist/vendors/jquery/jquery-3.3.1.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/moment/moment.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/slimscroll/jquery.slimscroll.min.js'); ?>"></script>
    <script src="https://cdn.jsdelivr.net/npm/lozad/dist/lozad.min.js"></script>
    <!-- END: Template JS-->

    <!-- START: APP JS-->
    <script src="<?= base_url('assets/dist/js/app.js'); ?>"></script>
    <!-- END: Template JS-->

    <!-- START: Page Vendor JS-->
    <script src="<?= base_url('assets/dist/js/auth/request.js'); ?>"></script>
    <script>
        var base_url = document.getElementById("base_url");
        var api_url = document.getElementById("api_url");
        var auth = document.getElementById("auth");
        var _main_menu = document.getElementById("_main_menu");
    </script>

    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window, document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '3542009505894280');
    fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=3542009505894280&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-E6E3SRPJ11"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-E6E3SRPJ11');
        </script>

    <!-- START: Main Content-->
    <?php
    // Load Secondary Menu
    echo ('<div id="header-fix" class="header">');
    $this->load->view("components/menus/secondary-menu");
    $this->load->view("components/menus/top-bar");
    echo ('</div>');
    ?>

    <div class="container mt-4">
        <?php
            $this->load->view("components/menus/main-menu");
        ?>
    </div>

    <div class="container my-3">

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <div class="input-group col-12 p-1 mb-3">
                        <input type="text" class="form-control p-2 w-100 h-100 contact-search" placeholder="Search ..." id="search">
                        <div class="input-group-append">
                            <span class="btn btn-outline-primary input-group-text" id="doSearch"><i class="icon-magnifier" aria-hidden="true"></i></span>
                        </div>
                    </div>

                    <div id="accordion2" class="accordion-alt" role="tablist">
                    </div>

                    <div class="p-1 mt-3">
                        <div class="btn-group" role="group" aria-label="Pagination Group">
                            <button class="btn btn-outline-secondary" type="button" 
                            id="prev"><i class="icon-arrow-left-circle"></i> Sebelumnya</button>
                            <button class="btn btn-outline-secondary" type="button" 
                            id="next">Selanjutnya <i class="icon-arrow-right-circle"></i></button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    </div>

    <div style="background-color: #fbfbfb; 
                font-color: rgba(0,0,0,.54);
                border-top: 1px solid rgba(0,0,0,.1)">
        <div class="container">
            <?php
            // Load Footer
            $this->load->view('anonymous/content/footer');
            ?>
        </div>
    </div>
    <!-- END: Content-->

    <script>

        var page = 0;

        (function($) {
            "use strict";
            $(window).on("load", function() {
                filter()

                $("#doSearch").on("click", function() {
                    filter();
                });

                $("#prev").on("click", function() {
                    if (page > 0) page -= 1;
                    filter();
                });

                $("#next").on("click", function() {
                    page += 1;
                    filter();
                })

            });
        })(jQuery);

        function filter() {

            var formData = new FormData();
            formData.append("page", page);
            formData.append("search", $("#search").val());

            $.ajax({
                url: base_url.value + "/services/faq/filter",
                data: formData,
                type: "POST",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.code == 200) {

                        if (response.data.length == 0) {
                            page -= 1;
                            filter();
                            return;
                        }

                        $("#accordion2").html(`
                            ${response.data.map(function(item, position) {
                                if (position == 0)
                                return `
                                <div class="mb-2">
                                    <h6 class="mb-0">
                                        <a class="d-block border" data-toggle="collapse" href="#collapse-${position}" aria-expanded="true" aria-controls="collapse4">
                                            ${item.question}
                                        </a>
                                    </h6>
                                    <div id="collapse-${position}" class="collapse show" role="tabpanel" data-parent="#accordion2">
                                        <div class="card-body">
                                            <small style="">${item.created_at}</small><br>
                                            <div>${item.answer}</div>
                                        </div>
                                    </div>
                                </div>
                                `;
                                else 
                                return `
                                <div class="mb-2">
                                    <h6 class="mb-0">
                                        <a class="collapsed text-uppercase redial-dark d-block border redial-border-light" data-toggle="collapse" href="#collapse-${position}" aria-expanded="false" aria-controls="collapse5">
                                            ${item.question}
                                        </a>
                                    </h6>
                                    <div id="collapse-${position}" class="collapse" role="tabpanel"  data-parent="#accordion2">
                                        <div class="card-body">
                                            <small style="">${item.created_at}</small><br>
                                            <div>${item.answer}</div>
                                        </div>
                                    </div>
                                </div>
                                `;
                            }).join('')}
                        `);
                    } else {
                        $("#accordion2").html(`
                            <center>
                                <p>Belum ada F.A.Q</p>
                            </center>
                        `);
                    }
                }
            });
        }

    </script>
</body>

</html>