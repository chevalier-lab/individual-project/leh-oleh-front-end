<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view('components/head'); ?>

        <!-- START: Template CSS-->
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/bootstrap/css/bootstrap.min.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.theme.min.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/simple-line-icons/css/simple-line-icons.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/flags-icon/css/flag-icon.min.css'); ?>"> 
        <!-- END Template CSS-->     

        <!-- START: Page CSS-->   
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/social-button/bootstrap-social.css'); ?>"/>   
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/sweetalert/sweetalert.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/fancybox/jquery.fancybox.min.css'); ?>">  
        <!-- END: Page CSS-->

        <!-- START: Custom CSS-->
        <link rel="stylesheet" href="<?= base_url('assets/dist/css/main.css'); ?>">
        <!-- END: Custom CSS-->
    </head>
    <body id="main-container" class="default bg-white"
    oncontextmenu="return false;">

        <!-- START: Pre Loader-->
        <div class="se-pre-con">
            <div class="loader"></div>
        </div>
        <!-- END: Pre Loader-->

        <!-- START: Template JS-->
        <script src="<?= base_url('assets/dist/vendors/jquery/jquery-3.3.1.min.js'); ?>"></script>
        <script src="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.js'); ?>"></script>
        <script src="<?= base_url('assets/dist/vendors/moment/moment.js'); ?>"></script>
        <script src="<?= base_url('assets/dist/vendors/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
        <script src="<?= base_url('assets/dist/vendors/slimscroll/jquery.slimscroll.min.js'); ?>"></script>
        <!-- END: Template JS--> 

        <!-- START: APP JS-->
        <script src="<?= base_url('assets/dist/js/app.js'); ?>"></script>
        <!-- END: APP JS-->

        <!-- START: Main Content-->
        <?php
            // Load Secondary Menu
            echo ('<div id="header-fix" class="header">');
                $this->load->view("components/menus/secondary-menu");
                $this->load->view("components/menus/top-bar");
            echo ('</div>');

            // Load Page
            $this->load->view("components/modals/categories");
        ?>
        <div class="container mt-4 mb-4">
            <div class="row">
                <?php 
                    // Load Profile
                    $this->load->view('anonymous/content/profile'); 
                ?>
            </div>
        </div>

        <div 
            style="background-color: #fbfbfb; 
                font-color: rgba(0,0,0,.54);
                border-top: 1px solid rgba(0,0,0,.1)">
            <div class="container">
                <?php
                    // Load Footer
                    $this->load->view('anonymous/content/footer');
                ?>
            </div>
        </div>
        <!-- END: Content-->

        <!-- START: Page Script JS-->
        <?php
            if (isset($error)) {
                $this->load->view("components/error-modal", array(
                    "errorModalTitle" => $error["title"],
                    "errorModalContent" => $error["content"],
                    "errorModalDetail" => $error["details"]
                ));
            }

            $this->load->view("components/modals/lost-password", array(
                "hideModal" => "hide",
                "isSuccess" => false
            ));
        ?>

        <!-- START: Page Vendor JS-->
        <script src="<?= base_url('assets/dist/vendors/fancybox/jquery.fancybox.min.js'); ?>"></script>
        <!-- END: Page Vendor JS-->

        <!-- START: Page JS-->
        <script src="<?= base_url('assets/dist/js/gallery.script.js'); ?>"></script>
        <!-- END: Page Script JS-->
    </body>
</html>