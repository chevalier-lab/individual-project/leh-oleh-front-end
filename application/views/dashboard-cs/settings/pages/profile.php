<div class="row">
    <div class="col-12 col-md-3">
        <div class="card">
            <div class="card-body" id="profile-face">
            </div>
        </div>
    </div>
    <div class="col-12 col-md-9">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <button class="btn btn-primary"
                        data-toggle="modal" data-target="#modal-edit-profile"
                        id="btn-modal-edit-profile">Ubah Profil</button>
                        <button class="btn btn-primary"
                        data-toggle="modal" data-target="#modal-change-photo">Ubah Foto Profil</button>
                        <button class="btn btn-primary"
                        id="btn-modal-delete-profile"
                        data-toggle="modal" data-target="#modal-delete-account">Hapus Akun</button>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="card mt-3">
                            <div class="card-body">
                                <h5 class="card-title">Umum</h5>
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item">Nama Lengkap: <span id="profile-full_name"></span></li>
                                    <li class="list-group-item">Username: <span id="profile-username"></span></li>
                                    <li class="list-group-item">Alamat: <span id="profile-address"></span></li>
                                    <li class="list-group-item">Tgl Lahir: <span id="profile-birth_of_date"></span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="card mt-3">
                            <div class="card-body">
                                <h5 class="card-title">Kontak</h5>
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item">Email: <span id="profile-email"></span></li>
                                    <li class="list-group-item">Nomor Telepon: <span id="profile-phone_number"></span></li>
                                    <li class="list-group-item">Jenis AKun: <span id="profile-type"></span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>