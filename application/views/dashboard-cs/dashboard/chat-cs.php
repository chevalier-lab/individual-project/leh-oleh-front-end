<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('components/head'); ?>

    <!-- START: Template CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.theme.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/simple-line-icons/css/simple-line-icons.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/flags-icon/css/flag-icon.min.css'); ?>">
    <!-- END Template CSS-->

    <!-- START: Page CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/social-button/bootstrap-social.css'); ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/sweetalert/sweetalert.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/toastr/toastr.min.css'); ?>" />
    <!-- END: Page CSS-->

    <!-- START: Custom CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/css/main.css'); ?>">
    <!-- END: Custom CSS-->
</head>

<body id="main-container" class="default">

    <input type="hidden" name="base_url" id="base_url" value="<?= base_url("index.php"); ?>">
    <input type="hidden" name="api_url" id="api_url" value="<?= API_URI; ?>">

    <!-- START: Pre Loader-->
    <div class="se-pre-con">
        <div class="loader"></div>
    </div>
    <!-- END: Pre Loader-->

    <!-- START: Template JS-->
    <script src="<?= base_url('assets/dist/vendors/jquery/jquery-3.3.1.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/moment/moment.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/slimscroll/jquery.slimscroll.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/toastr/toastr.min.js'); ?>"></script>
    <!-- END: Template JS-->

    <!-- START: Page Vendor JS-->
    <script src="<?= base_url('assets/dist/js/auth/request.js'); ?>"></script>
    <script>
        var base_url = document.getElementById("base_url");
        var api_url = document.getElementById("api_url");
    </script>
    <!-- END: Template JS-->

    <!-- START: Main Content-->
    <?php

    // Load Navigation Bar
    $this->load->view('components/menus/navbar-cs');

    // Load Sidebar
    $this->load->view('components/menus/sidebar');

    // Load Page
    $this->load->view('dashboard/template', array(
        'page' => "Daftar Chat",
        'pageMap' => array(
            array(
                "label" => "Dasbor",
                "is_current" => false
            ),
            array(
                "label" => "Daftar Chat",
                "is_current" => true
            )
        ),
        'pageURI' => "dashboard-cs/dashboard/pages/chat-cs"
    ));

    ?>
    <!-- END: Content-->

    <!-- START: Page Script JS-->
    <?php
    if (isset($error)) {
        $this->load->view("components/error-modal", array(
            "errorModalTitle" => $error["title"],
            "errorModalContent" => $error["content"],
            "errorModalDetail" => $error["details"]
        ));
    }
    ?>
    <!-- END: Page Script JS-->

    <!-- START: APP JS-->
    <script src="<?= base_url('assets/dist/js/app.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/js/home.script.js'); ?>"></script>
    <!-- END: APP JS-->

    <script>
        (function($) {
            "use strict";
            $(window).on("load", function() {
                loadListChatRoom()
            });
        })(jQuery);

        function loadListChatRoom() {
            renderToWaiting()
            $.ajax({
                url: base_url.value + "/services/chats/list_chat_room_cs",
                data: null,
                type: "GET",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.code == 200) {
                        renderToTable(response.data)
                    } else {
                        renderToTable([])
                    }
                }
            });
        }

        function renderToWaiting() {
            $("#master-chats tbody").html(`
                <tr>
                    <td class="text-center" colspan="5">Sedang memuat data</td>
                </tr>
            `);
        }

        function renderToTable(data = []) {
            index = 1;

            if (data.length == 0) {
                $("#master-chats tbody").html(`
                    <tr>
                        <td class="text-center" colspan="5">Chat Kosong</td>
                    </tr>
                `);
                return;
            }

            $("#master-chats tbody").html(`${data.map(function(item) {
                return `
                    <tr>
                        <td class="align-middle">${index++}</td>
                        <td class="align-middle">${item.full_name_user_1}</td>
                        <td class="align-middle">${item.created_at}</td>
                        <td class="align-middle">${item.updated_at}</td>
                        <td class="align-middle">
                            <h4>
                                <a href="<?= base_url('index.php/dashboard/cs/dashboard/detailRoomChat/') ?>${item.id}" class="text-success"><i class="icon-eye"></i></a>
                            </h4>
                        </td>
                    </tr>
                `;
            }).join('')}`);
        }
    </script>
</body>

</html>