<div class="card mb-3">
    <div class="card-header">
        <h4 class="card-title"
        id="merchant-name">Lists Product</h4>
    </div>
    <div class="card-body">

        <div class="pt-0 pb-0 pl-3 pr-3 mb-3 row">
            <div class="input-group col-12 col-md-6 p-1">
                <input type="text" class="form-control p-2 w-100 h-100 contact-search" 
                    placeholder="Cari ..."
                    id="lists-products-merchant-search">
                    <div class="input-group-append">
                        <span class="btn btn-outline-primary input-group-text" id="lists-products-merchant-button-search"><i class="icon-magnifier"
                            aria-hidden="true"></i></span>
                    </div>
            </div>
            <div class="col-12 col-md-6 p-1">
                <select class="form-control p-2 w-100 h-100 contact-search"
                    id="lists-products-merchant-order">
                    <option value="0">Urutkan berdasarkan</option>
                    <option value="1">Nama (A-Z)</option>
                    <option value="2">Nama (Z-A)</option>
                </select>
            </div>
        </div>

        <div class="table-responsive pr-1 pl-1 mb-0">
        <?php

        $this->load->view('components/table', array(
            "idTable" => "lists-products-merchant",
            "isCustomThead" => true,
            "thead" => '
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nama Produk</th>
                <th scope="col">Slug</th>
                <th scope="col">Deskripsi</th>
                <th scope="col">Harga Beli</th>
                <th scope="col">Harga Jual</th>
                <th scope="col">Stok</th>
                <th scope="col">Diskon</th>
            </tr>
            ',
            "tbody" => ""
        )) ?>

        </div>

        <div class="row">
            <div class="col-12">
                <div class="p-1">
                    <div class="btn-group" role="group" aria-label="Pagination Group">
                        <button class="btn btn-outline-secondary" type="button" 
                        id="lists-products-merchant-prev"><i class="icon-arrow-left-circle"></i> Sebelumnya</button>
                        <button class="btn btn-outline-secondary" type="button" 
                        id="lists-products-merchant-next">Selanjutnya <i class="icon-arrow-right-circle"></i></button>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<script>
    // Initial Pagination
    var page = 0;
    var paginationClicked = false;
    var paginationDirection = "";

    var selected_merchant = null;
    var storage = window.sessionStorage;
    if (storage != null) {
        if (storage.getItem("selected_merchant")) {
            selected_merchant = JSON.parse(storage.getItem("selected_merchant"));
            $("#merchant-name").text(selected_merchant.market_name)
        }
    }

    (function ($) {
        "use strict";
        $(window).on("load", function () {
            // Load Bank First Time
            loadMerchantsProduct()

            // Handle Order By
            $("#lists-products-merchant-order").on("change", function() {
                loadMerchantsProduct();
            });

            // Handle Search
            $("#lists-products-merchant-search").on("keydown", function(event) {
                if (event.keyCode == 32 || event.which == 32) loadMerchantsProduct();
                else if ($("#lists-products-merchant-search").val() == "") loadMerchantsProduct();
            });

            $("#lists-products-merchant-button-search").on("click", function(event) {
                loadMerchantsProduct();
            });

            // Handle Pagination
            $("#lists-products-merchant-prev").on("click", function() {
                if (page > 0)
                    page -= 1;
                paginationClicked = true;
                paginationDirection = "prev";
                loadMerchantsProduct();
            });

            $("#lists-products-merchant-next").on("click", function() {
                page += 1;
                paginationClicked = true;
                paginationDirection = "next";
                loadMerchantsProduct();
            });
        });
    })(jQuery);

    function loadMerchantsProduct() {
        renderToWaiting()

        var order = $("#lists-products-merchant-order").val();
        var order_direction = "DESC";
        switch (Number(order)) {
            case 1: 
                order =  "market_name";
                order_direction = "ASC";
            break;
            case 2: 
                order =  "market_name";
                order_direction = "DESC";
            break;
            default:
                order =  "id";
                order_direction = "DESC";
            break;
        }
        var raw = req.raw({
            page: page,
            search: $("#lists-products-merchant-search").val(),
            order_by: order,
            order_direction: order_direction
        })

        var formData = new FormData()
        formData.append("raw", raw)

        $.ajax({
            url: base_url.value + "/dashboard/a/managementProducts/load_merchant_product/" + id_merchant.value,
            data: formData,
            type: "POST",
            contentType: false,
            processData: false,
            success: function(response) {
                response = req.data(response)
                if (response.code == 200) {
                    renderToTable(response.data)
                } else {
                    renderToTable([])
                }
            }
        });
    }

    function renderToWaiting() {
        $("#lists-products-merchant tbody").html(`
            <tr>
                <td class="align-middle" colspan="8">Sedang memuat data</td>
            </tr>
        `);
    }

    function renderToTable(data=[]) {
        
        if (paginationClicked) {
            paginationClicked = false;
            if (data.length == 0) {
                if (paginationDirection == "next") 
                    page -= 1;
                else page += 1;
            }
            paginationDirection = "";
            loadMerchants();
            return 
        }

        var index = (page * 10) + 1;

        merchant = data;

        if (data.length == 0) {
            $("#lists-products-merchant tbody").html(`
                <tr>
                    <td class="align-middle" colspan="8">Belum ada produk pada toko ini</td>
                </tr>
            `);
            return;
        }

        $("#lists-products-merchant tbody").html(`${data.map(function(item) {
            return `
                <tr>
                    <td class="align-middle">${index++}</td>
                    <td class="align-middle">${item.product_name}</td>
                    <td class="align-middle">${item.slug}</td>
                    <td class="align-middle">${item.description}</td>
                    <td class="align-middle">${req.money(Number(item.price_default).toString(), "Rp ")}</td>
                    <td class="align-middle">${req.money(Number(item.price_selling).toString(), "Rp ")}</td>
                    <td class="align-middle">${item.qty}</td>
                    <td class="align-middle">${item.discount + "%"}</td>
                </tr>
            `;
        }).join('')}`);
    }
</script>