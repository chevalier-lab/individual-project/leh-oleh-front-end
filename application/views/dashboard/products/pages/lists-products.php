<div class="card mb-3">
    <div class="card-header">
        <h4 class="card-title">Lists Products</h4>
    </div>
    <div class="card-body">

        <div class="pt-0 pb-0 pl-3 pr-3 mb-3 row">
            <div class="input-group col-12 col-md-6 p-1">
                <input type="text" class="form-control p-2 w-100 h-100 contact-search" 
                    placeholder="Cari ..."
                    id="lists-product-search">
                    <div class="input-group-append">
                        <span class="btn btn-outline-primary input-group-text" id="lists-product-button-search"><i class="icon-magnifier"
                            aria-hidden="true"></i></span>
                    </div>
            </div>
            <div class="col-12 col-md-6 p-1">
                <select class="form-control p-2 w-100 h-100 contact-search"
                    id="lists-product-order">
                    <option value="0">Urutkan berdasarkan</option>
                    <option value="1">Nama (A-Z)</option>
                    <option value="2">Nama (Z-A)</option>
                </select>
            </div>
        </div>

        <div class="table-responsive pr-1 pl-1 mb-0">
            <?php

            $this->load->view('components/table', array(
                "idTable" => "lists-product",
                "isCustomThead" => true,
                "thead" => '
                <tr>
                    <th scope="col" rowspan="2">#</th>
                    <th scope="col" rowspan="2">Nama Produk</th>
                    <th scope="col" rowspan="2">Slug</th>
                    <th scope="col" rowspan="2">Harga Beli</th>
                    <th scope="col" rowspan="2">Harga Jual</th>
                    <th scope="col" rowspan="2">Stok</th>
                    <th scope="col" rowspan="2">Tgl Dibuat</th>
                    <th scope="col" rowspan="2">Nama Toko</th>
                    <th scope="col" rowspan="2">Status</th>
                    <th scope="col" colspan="2">Aksi</th>
                </tr>
                <tr>
                    <th>Detail</th>
                    <th>Blokir</th>
                </tr>
                ',
                "tbody" => ''
            )) ?>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="p-1">
                    <div class="btn-group" role="group" aria-label="Pagination Group">
                        <button class="btn btn-outline-secondary" type="button" 
                        id="lists-product-prev"><i class="icon-arrow-left-circle"></i> Sebelumnya</button>
                        <button class="btn btn-outline-secondary" type="button" 
                        id="lists-product-next">Selanjutnya <i class="icon-arrow-right-circle"></i></button>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>