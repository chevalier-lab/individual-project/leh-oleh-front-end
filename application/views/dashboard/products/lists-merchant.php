<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('components/head'); ?>

    <!-- START: Template CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.theme.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/simple-line-icons/css/simple-line-icons.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/flags-icon/css/flag-icon.min.css'); ?>">
    <!-- END Template CSS-->

    <!-- START: Page CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/social-button/bootstrap-social.css'); ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/sweetalert/sweetalert.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/toastr/toastr.min.css'); ?>"/>
    <!-- END: Page CSS-->

    <!-- START: Custom CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/css/main.css'); ?>">
    <!-- END: Custom CSS-->
</head>

<body id="main-container" class="default">

    <input type="hidden" name="base_url" 
        id="base_url" value="<?= base_url("index.php"); ?>">
    <input type="hidden" name="api_url" 
        id="api_url" value="<?= API_URI; ?>">

    <!-- START: Pre Loader-->
    <div class="se-pre-con">
        <div class="loader"></div>
    </div>
    <!-- END: Pre Loader-->

    <!-- START: Template JS-->
    <script src="<?= base_url('assets/dist/vendors/jquery/jquery-3.3.1.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/moment/moment.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/slimscroll/jquery.slimscroll.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/toastr/toastr.min.js'); ?>"></script>
    <!-- END: Template JS-->

    <!-- START: Page Vendor JS-->
    <script src="<?= base_url('assets/dist/js/auth/request.js'); ?>"></script>
    <script>
        var base_url = document.getElementById("base_url");
        var api_url = document.getElementById("api_url");
    </script>
    <!-- END: Page Vendor JS-->

    <!-- START: Main Content-->
    <?php

    // Load Navigation Bar
    $this->load->view('components/menus/navbar');

    // Load Sidebar
    $this->load->view('components/menus/sidebar');

    // Load Page
    $this->load->view('dashboard/template', array(
        'page' => "Daftar Toko",
        'pageMap' => array(
            array(
                "label" => "Produk",
                "is_current" => false
            ),
            array(
                "label" => "Daftar Toko",
                "is_current" => true
            ),
        ),
        'pageURI' => "dashboard/products/pages/lists-merchant",
    ));

    ?>
    <!-- END: Content-->

    <!-- START: Page Script JS-->
    <?php
    if (isset($error)) {
        $this->load->view("components/error-modal", array(
            "errorModalTitle" => $error["title"],
            "errorModalContent" => $error["content"],
            "errorModalDetail" => $error["details"]
        ));
    }

    // Detail Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-detail-merchant",
        "modalTitle" => "Detail Toko",
        "modalType" => "modal-lg",
        "iconTitle" => "icon-check",
        "modalActionForm" => "#",
        "modalContentForm" => '
            <div class="row">
                <div class="col-12 mb-3">
                    <div class="position-relative">
                        <div class="background-image-maker py-5"></div>
                        <div class="holder-image">
                            <img 
                            id="detail-merchant-cover"
                            alt="" class="img-fluid d-none">
                        </div>
                        <div class="position-relative px-3 py-5">
                            <div class="media d-md-flex d-block">
                                <div class="media-body z-index-1">
                                    <div>
                                        <h1 class="display-5 text-uppercase text-white mb-0"
                                        id="detail-merchant-author"></h1>
                                        <h6 class="text-uppercase text-white mb-0"
                                        id="detail-merchant-merchant"></h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Data Toko</h5>
                            <ul class="list-group list-group-flush"
                            id="detail-merchant-bio-merchant">
                            </ul>
                        </div>
                    </div>
                </div>  
            </div>
        ',
        "modalButtonForm" => ''
    ));

    // Banned Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-banned-merchant",
        "modalTitle" => "Blokir Toko",
        "modalType" => "modal-md",
        "iconTitle" => "icon-close",
        "modalActionForm" => "#",
        "modalContentForm" => '
        <p>Apakah Anda yakin untuk blokir toko <strong id="banned-merchant-name"></strong>?</p>
        ',
        "modalButtonForm" => '<button type="button" 
        id="btn-banned-merchant"
        class="btn btn-danger add-todo">Blokir</button>'
    ));

    ?>
    <!-- END: Page Script JS-->

    <!-- START: APP JS-->
    <script src="<?= base_url('assets/dist/js/app.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/js/home.script.js'); ?>"></script>

    <script>
        // Initial Pagination
        var page = 0;
        var paginationClicked = false;
        var paginationDirection = "";
        var selectedMerchantPosition = -1;
        var selectedMerchant = null;
        var merchant = [];

        (function ($) {
            "use strict";
            $(window).on("load", function () {
                // Load Bank First Time
                loadMerchants()

                // Handle Order By
                $("#lists-merchants-order").on("change", function() {
                    loadMerchants();
                });

                // Handle Search
                $("#lists-merchants-search").on("keydown", function(event) {
                    if (event.keyCode == 32 || event.which == 32) loadMerchants();
                    else if ($("#lists-merchants-search").val() == "") loadMerchants();
                });

                $("#lists-merchants-button-search").on("click", function(event) {
                    loadMerchants();
                });

                // Handle Pagination
                $("#lists-merchants-prev").on("click", function() {
                    if (page > 0)
                        page -= 1;
                    paginationClicked = true;
                    paginationDirection = "prev";
                    loadMerchants();
                });

                $("#lists-merchants-next").on("click", function() {
                    page += 1;
                    paginationClicked = true;
                    paginationDirection = "next";
                    loadMerchants();
                });

                $("#btn-banned-merchant").on("click", function() {

                    var raw = req.raw({
                        type: 0,
                        is_visible: 3
                    })

                    var formData = new FormData()
                    formData.append("raw", raw)

                    $.ajax({
                        url: base_url.value + "/dashboard/a/managementUsers/change_status_user_request/" + selectedMerchant.id,
                        data: formData,
                        type: "POST",
                        contentType: false,
                        processData: false,
                        success: function(response) {
                            response = req.data(response)
                            if (response.code == 200) {
                                toastr.success("Berhasil blokir toko");
                                loadMerchants();
                            } else {
                                toastr.error(response.message)
                            }
                            $("#modal-banned-merchant").modal("hide");
                        }
                    });
                });
            });
        })(jQuery);

        function loadMerchants() {
            renderToWaiting()

            var order = $("#lists-merchants-order").val();
            var order_direction = "DESC";
            switch (Number(order)) {
                case 1: 
                    order =  "market_name";
                    order_direction = "ASC";
                break;
                case 2: 
                    order =  "market_name";
                    order_direction = "DESC";
                break;
                default:
                    order =  "id";
                    order_direction = "DESC";
                break;
            }
            var raw = req.raw({
                page: page,
                search: $("#lists-merchants-search").val(),
                order_by: order,
                order_direction: order_direction
            })

            var formData = new FormData()
            formData.append("raw", raw)

            $.ajax({
                url: base_url.value + "/dashboard/a/managementProducts/load_merchant",
                data: formData,
                type: "POST",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.code == 200) {
                        renderToTable(response.data)
                    } else {
                        renderToTable([])
                    }
                }
            });
        }

        function renderToWaiting() {
            $("#lists-merchants tbody").html(`
                <tr>
                    <td class="text-center" colspan="8">Sedang memuat data</td>
                </tr>
            `);
        }

        function renderToTable(data=[]) {
            
            if (paginationClicked) {
                paginationClicked = false;
                if (data.length == 0) {
                    if (paginationDirection == "next") 
                        page -= 1;
                    else page += 1;
                }
                paginationDirection = "";
                loadMerchants();
                return 
            }

            var index = (page * 10) + 1;

            merchant = data;

            if (data.length == 0) {
                $("#lists-merchants tbody").html(`
                    <tr>
                        <td class="text-center" colspan="8">Belum ada data toko</td>
                    </tr>
                `);
                return;
            }

            $("#lists-merchants tbody").html(`${data.map(function(item, position) {
                return `
                    <tr>
                        <td>${index++}</td>
                        <td>${item.market_name}</td>
                        <td>${item.market_address}</td>
                        <td>${item.market_phone_number}</td>
                        <td>${item.full_name}</td>
                        <td class="text-center">
                            <h4>
                                <a href="javascript:void(0);" data-toggle="modal" data-target="#modal-detail-merchant" class="btn text-primary"
                                onclick="setSelectedMerchant(${position})"><i class="icon-eye"></i></a>
                            </h4>
                        </td>
                        <td class="text-center">
                            <h4>
                                <a href="javascript:void(0)" 
                                onclick="setSessionMerchant(${position})"
                                class="btn text-primary"><i class="icon-list"></i></a>
                            </h4>
                        </td>
                        <td class="text-center">
                            <h4>
                                <a href="javascript:void(0);" data-toggle="modal" 
                                onclick="setDeletedMerchant(${position})"
                                data-target="#modal-banned-merchant" class="btn text-danger"><i class="icon-close"></i></a>
                            </h4>
                        </td>
                    </tr>
                `;
            }).join('')}`);
        }

        function setDeletedMerchant(position) {
            selectedMerchant = merchant[position];
            $("#banned-merchant-name").html(selectedMerchant.market_name);
        }

        function setSessionMerchant(position) {
            var selectedMerchant = merchant[position];
            var storage = window.sessionStorage;
            if (storage != null) {
                storage.setItem("selected_merchant", JSON.stringify(selectedMerchant))
                location.assign(base_url.value + '/dashboard/a/ManagementProducts/listProductMerchant/' + selectedMerchant.id);
            }
        }

        function setSelectedMerchant(position) {
            selectedMerchantPosition = position;
            var selectedMerchant = merchant[position];
            
            $.ajax({
                url: base_url.value + "/dashboard/a/managementProducts/load_detail_merchant/" + selectedMerchant.id,
                data: null,
                type: "GET",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.code == 200) {
                        var data = response.data
                        var status = Number(data.is_visible)
                        if (status == 0) status = "<span class='badge badge-secondary'>Terblokir</span>"
                        else if (status == 1) status = "<span class='badge badge-primary'>Aktif</span>"
                        else if (status == 2) status = "<span class='badge badge-secondary'>Tertunda</span>"

                        $("#detail-merchant-author").text(selectedMerchant.full_name)
                        $("#detail-merchant-merchant").text(selectedMerchant.market_name)
                        $("#detail-merchant-bio-merchant").html(`
                            <li class="list-group-item">No Identitas: ${status}</li>
                            <li class="list-group-item">Alamat: ${data.market_address}</li>
                            <li class="list-group-item">No HP: ${data.market_phone_number}</li>
                        `);
                    } else {
                        $("#modal-detail-merchant").modal("hide");
                    }
                }
            });
        }
    </script>
    <!-- END: APP JS-->
</body>

</html>