<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('components/head'); ?>

    <!-- START: Template CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.theme.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/simple-line-icons/css/simple-line-icons.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/flags-icon/css/flag-icon.min.css'); ?>">
    <!-- END Template CSS-->

    <!-- START: Page CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/social-button/bootstrap-social.css'); ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/sweetalert/sweetalert.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/toastr/toastr.min.css'); ?>"/>
    <!-- END: Page CSS-->

    <!-- START: Custom CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/css/main.css'); ?>">
    <!-- END: Custom CSS-->
</head>

<body id="main-container" class="default">

    <input type="hidden" name="base_url" 
        id="base_url" value="<?= base_url("index.php"); ?>">
    <input type="hidden" name="api_url" 
        id="api_url" value="<?= API_URI; ?>">

    <!-- START: Pre Loader-->
    <div class="se-pre-con">
        <div class="loader"></div>
    </div>
    <!-- END: Pre Loader-->

    <!-- START: Template JS-->
    <script src="<?= base_url('assets/dist/vendors/jquery/jquery-3.3.1.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/moment/moment.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/slimscroll/jquery.slimscroll.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/toastr/toastr.min.js'); ?>"></script>
    <!-- END: Template JS-->

    <!-- START: Page Vendor JS-->
    <script src="<?= base_url('assets/dist/js/auth/request.js'); ?>"></script>
    <script>
        var base_url = document.getElementById("base_url");
        var api_url = document.getElementById("api_url");
    </script>
    <!-- END: Page Vendor JS-->

    <!-- START: Main Content-->
    <?php

    // Load Navigation Bar
    $this->load->view('components/menus/navbar');

    // Load Sidebar
    $this->load->view('components/menus/sidebar');

    // Load Page
    $this->load->view('dashboard/template', array(
        'page' => "Daftar Produk",
        'pageMap' => array(
            array(
                "label" => "Produk",
                "is_current" => false
            ),
            array(
                "label" => "Daftar Produk",
                "is_current" => true
            ),
        ),
        'pageURI' => "dashboard/products/pages/lists-products",
    ));

    ?>
    <!-- END: Content-->

    <!-- START: Page Script JS-->
    <?php
    if (isset($error)) {
        $this->load->view("components/error-modal", array(
            "errorModalTitle" => $error["title"],
            "errorModalContent" => $error["content"],
            "errorModalDetail" => $error["details"]
        ));
    }

    // Detail Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-detail-product",
        "modalTitle" => "Detail Produk",
        "modalType" => "modal-lg",
        "iconTitle" => "icon-check",
        "modalActionForm" => "#",
        "modalContentForm" => '
            <div id="container-error"></div>
            <div class="row" id="container">
                <div class="col-12 mt-3">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12 col-lg-4">
                                    <img class="img-fluid" alt="product detail"
                                    id="cover-product-review">
                                </div> 
                                <div class="col-md-12 col-lg-8">
                                    <div class="card-body border brd-gray border-top-0 border-right-0 border-left-0">
                                        <h3 class="mb-0"><a href="javascript:void(0);" class="f-weight-500 text-primary"
                                        id="name-product-review"></a></h3>
                                    </div>
                                    <div class="card-body border border-top-0 border-right-0 border-left-0">
                                        <div class="clearfix">
                                            <div class="float-left mr-2">
                                                <ul class="list-inline mb-0" id="rating-product-review"></ul>
                                            </div>
                                            <span id="count-product-review">(3 customer reviews)</span>
                                        </div>
                                    </div>
                                    <div class="card-body border brd-gray border-top-0 border-right-0 border-left-0">
                                        <div class="row">
                                            <div class="col-12" id="price-product-review"></div>
                                        </div>
                                    </div>
                                    <div class="card-body border brd-gray border-top-0 border-right-0 border-left-0">
                                        <p class="mb-0" lang="ca" id="description-product-review"></p>
                                    </div>
                                    <div class="card-body border brd-gray border-top-0 border-right-0 border-left-0">
                                        <div class="d-inline-block mr-3">
                                            <p class="dark-color f-weight-600">Stok: </p>
                                        </div>
                                        <div class="d-inline-block mr-3">
                                            <div class="form-group">
                                                <span
                                                id="qty"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <ul class="list-unstyled" id="lists-product-review">
                                        </ul>
                                    </div>

                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        ',
        "modalButtonForm" => ''
    ));

    // Banned Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-banned-product",
        "modalTitle" => "Blokir Produk",
        "modalType" => "modal-md",
        "iconTitle" => "icon-close",
        "modalActionForm" => "#",
        "modalContentForm" => '
        <p>Apakah Anda yakin untuk blokir produk <strong id="banned-product-name"></strong>?</p>
        ',
        "modalButtonForm" => '<button type="button" 
        id="btn-banned-product"
        class="btn btn-danger add-todo">Blokir</button>'
    ));

    ?>
    <!-- END: Page Script JS-->

    <!-- START: APP JS-->
    <script src="<?= base_url('assets/dist/js/app.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/js/home.script.js'); ?>"></script>

    <script>
        // Initial Pagination
        var page = 0;
        var paginationClicked = false;
        var paginationDirection = "";
        var product = [];
        var selectedProduct = null;

        (function ($) {
            "use strict";
            $(window).on("load", function () {
                // Load Bank First Time
                loadProducts()

                // Handle Order By
                $("#lists-product-order").on("change", function() {
                    loadProducts();
                });

                // Handle Search
                $("#lists-product-search").on("keydown", function(event) {
                    if (event.keyCode == 32 || event.which == 32) loadProducts();
                    else if ($("#lists-product-search").val() == "") loadProducts();
                });

                $("#lists-product-button-search").on("click", function(event) {
                    loadProducts();
                });

                // Handle Pagination
                $("#lists-product-prev").on("click", function() {
                    if (page > 0)
                        page -= 1;
                    paginationClicked = true;
                    paginationDirection = "prev";
                    loadProducts();
                });

                $("#lists-product-next").on("click", function() {
                    page += 1;
                    paginationClicked = true;
                    paginationDirection = "next";
                    loadProducts();
                });

                // Handle Delete Page
                $("#btn-banned-product").on("click", function() {
                    $("#btn-banned-product").attr("disabled", true)
                    var raw = req.raw({
                        id_m_products: selectedProduct.id,
                        is_visible: 0
                    });

                    var formData = new FormData()
                    formData.append("raw", raw)

                    $.ajax({
                    url: base_url.value + "/dashboard/a/managementProducts/change_status_product",
                        data: formData,
                        type: "POST",
                        contentType: false,
                        processData: false,
                        success: function(response) {
                            response = req.data(response)
                            if (response.code == 200) {
                                loadProducts()
                                toastr.success(response.message);
                            } else {
                                toastr.error(response.message);
                            }
                            $("#modal-banned-product").modal('hide');
                            $("#btn-banned-product").removeAttr("disabled")
                        }
                    });
                });
            });
        })(jQuery);

        function loadProducts() {
            renderToWaiting()

            var order = $("#lists-product-order").val();
            var order_direction = "DESC";
            switch (Number(order)) {
                case 1: 
                    order =  "product_name";
                    order_direction = "ASC";
                break;
                case 2: 
                    order =  "product_name";
                    order_direction = "DESC";
                break;
                default:
                    order =  "id";
                    order_direction = "DESC";
                break;
            }
            var raw = req.raw({
                page: page,
                search: $("#lists-product-search").val(),
                order_by: order,
                order_direction: order_direction
            })

            var formData = new FormData()
            formData.append("raw", raw)

            $.ajax({
                url: base_url.value + "/dashboard/a/managementProducts/load_product_validate",
                data: formData,
                type: "POST",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.code == 200) {
                        renderToTable(response.data)
                    } else {
                        renderToTable([])
                    }
                }
            });
        }

        function renderToWaiting() {
            $("#lists-product tbody").html(`
                <tr>
                    <td class="align-middle" colspan="11">Sedang memuat data</td>
                </tr>
            `);
        }

        function renderToTable(data=[]) {
            
            if (paginationClicked) {
                paginationClicked = false;
                if (data.length == 0) {
                    if (paginationDirection == "next") 
                        page -= 1;
                    else page += 1;
                }
                paginationDirection = "";
                loadProducts();
                return 
            }

            product = data;

            var index = (page * 10) + 1;

            if (data.length == 0) {
                $("#lists-product tbody").html(`
                    <tr>
                        <td class="align-middle" colspan="11">Belum ada data, 
                        tekan <span class="icon-plus"></span> untuk menambahkan</td>
                    </tr>
                `);
                return;
            }

            $("#lists-product tbody").html(`${data.map(function(item, position) {
                var status = Number(item.is_visible);
                if (status == 0) status = '<span class="badge outline-badge-danger">Terblokir</span>';
                else if (status == 1) status = '<span class="badge outline-badge-success">Aktif</span>';
                else if (status == 2) status = '<span class="badge outline-badge-success">Tertunda</span>';
                return `
                    <tr>
                        <td class="align-middle">${index++}</td>
                        <td class="align-middle">${item.product_name}</td>
                        <td class="align-middle">${item.slug}</td>
                        <td class="align-middle">${req.money(Number(item.price_default).toString(), "Rp ")}</td>
                        <td class="align-middle">${req.money(Number(item.price_selling).toString(), "Rp ")}</td>
                        <td class="align-middle">${item.qty}</td>
                        <td class="align-middle">${item.created_at}</td>
                        <td class="align-middle">${item.market_name}</td>
                        <td class="align-middle">${status}</td>
                        <td class="align-middle">
                            <h4>
                                <a href="javascript:void(0);" data-toggle="modal" data-target="#modal-detail-product" 
                                onclick="loadDetailProduct(${position})"
                                class="text-primary"><i class="icon-eye"></i></a>
                            </h4>
                        </td>
                        <td class="align-middle">
                            <h4>
                                <a href="javascript:void(0)" 
                                onclick="setBannedProduct(${position})"
                                data-toggle="modal" data-target="#modal-banned-product" 
                                class="btn text-danger ${(Number(item.is_visible) == "") ? 'disabled': ''}"><i class="icon-close"></i></a>
                            </h4>
                        </td>
                    </tr>
                `;
            }).join('')}`);
        }

        function setBannedProduct(position) {
            selectedProduct = product[position];
            $("#banned-product-name").html(selectedProduct.product_name);
        }

        function loadDetailProduct(position) {
            selectedProduct = product[position];

            $("#container").hide();
            $("#container-error").hide();
            $.ajax({
                url: base_url.value + "/dashboard/a/managementProducts/load_detail_product/" + selectedProduct.id,
                data: null,
                type: "GET",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.code == 200) {
                        var data = response.data
                        dataProduct = data;
                        var price = Number(data.price_selling)
                        var discount = Number(data.discount);
                        var price_divide_discount = ((price / 100) * discount);

                        price = (price - price_divide_discount)

                        // Render
                        $("#name-product-review").text(data.product_name)
                        $("#rating-product-review").html(``)
                        var rating = (data.rating != null) ? Number(data.rating) : 0;
                        for (var i = 0; i < 5; i++) {
                            if (i < rating) $("#rating-product-review")
                            .append(`<li class="list-inline-item"><a href="javascript:void(0);" class="text-warning"><i class="icon-star"></i></a></li>`)
                            else $("#rating-product-review")
                            .append(`<li class="list-inline-item"><a href="javascript:void(0);"><i class="icon-star"></i></a></li>`)
                        }

                        $("#cover-product-review").attr("src", data.uri)

                        if (discount > 0) $("#price-product-review").html(`
                            <div class="float-left">
                                <h4 class="lato-font body-color mb-0"><del>${req.money(data.price_selling.toString(), "Rp ")}</del></h4>
                            </div>
                            <div class="float-left ml-2">
                                <h4 class="lato-font mb-0 text-danger">${req.money(price.toString(), "Rp ")}</h4>
                            </div>
                        `); else $("#price-product-review").html(`
                            <div class="float-left">
                                <h4 class="lato-font body-color mb-0">${req.money(data.price_selling.toString(), "Rp ")}</h4>
                            </div>
                        `);

                        $("#description-product-review").html(`${data.description}`)
                        $("#qty").html(`${data.qty}`)

                        $("#lists-product-review").html(`<li class="font-weight-bold dark-color mb-2">Toko: 
                        <span class="body-color font-weight-normal">${data.market_name}</span></li>`);

                        if (data.location.length > 0) {
                            $("#lists-product-review").append(`
                                <li class="font-weight-bold dark-color mb-2">Lokasi:
                                    ${data.location.map(function(item, position) {
                                        return `
                                            <span class="body-color font-weight-normal">${item.province_name}
                                                ${(position < (data.location.length - 1)) ? ", " : ""}</span>
                                        `
                                    }).join('')}
                                </li>
                            `);
                        }

                        if (data.categories.length > 0) {
                            $("#lists-product-review").append(`
                                <li class="font-weight-bold dark-color mb-2">Kategori:
                                    ${data.categories.map(function(item, position) {
                                        return `
                                            <span class="body-color font-weight-normal">${item.category}
                                                ${(position < (data.categories.length - 1)) ? ", " : ""}</span>
                                        `
                                    }).join('')}
                                </li>
                            `);
                        }
                        
                        $("#container").show();
                    } else {
                        $("#container-error").html(`
                        <div class="jumbotron mb-0 text-center theme-background rounded">
                            <h1 class="display-3 font-weight-bold"> 404</h1>
                            <h5><i class="ion ion-alert pr-2"></i>Oops! Something went wrong</h5>
                            <p>The page you are looking for is not found, please try after some time or go back to home</p>
                            <a href="${base_url.value}" class="btn btn-primary">Go To Home</a>
                        </div>
                        `);
                        $("#container-error").show();
                    }
                }
            });
        }
    </script>
    <!-- END: APP JS-->
</body>

</html>