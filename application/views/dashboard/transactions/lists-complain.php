<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('components/head'); ?>

    <!-- START: Template CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.theme.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/simple-line-icons/css/simple-line-icons.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/flags-icon/css/flag-icon.min.css'); ?>">
    <!-- END Template CSS-->

    <!-- START: Page CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/social-button/bootstrap-social.css'); ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/sweetalert/sweetalert.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/toastr/toastr.min.css'); ?>"/>
    <!-- END: Page CSS-->

    <!-- START: Custom CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/css/main.css'); ?>">
    <!-- END: Custom CSS-->
</head>

<body id="main-container" class="default">

    <input type="hidden" name="base_url" 
        id="base_url" value="<?= base_url("index.php"); ?>">
    <input type="hidden" name="api_url" 
        id="api_url" value="<?= API_URI; ?>">

    <!-- START: Pre Loader-->
    <div class="se-pre-con">
        <div class="loader"></div>
    </div>
    <!-- END: Pre Loader-->

    <!-- START: Template JS-->
    <script src="<?= base_url('assets/dist/vendors/jquery/jquery-3.3.1.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/moment/moment.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/slimscroll/jquery.slimscroll.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/toastr/toastr.min.js'); ?>"></script>
    <!-- END: Template JS-->

    <!-- START: Page Vendor JS-->
    <script src="<?= base_url('assets/dist/js/auth/request.js'); ?>"></script>
    <script>
        var base_url = document.getElementById("base_url");
        var api_url = document.getElementById("api_url");
    </script>

    <!-- START: Main Content-->
    <?php

    // Load Navigation Bar
    $this->load->view('components/menus/navbar');

    // Load Sidebar
    $this->load->view('components/menus/sidebar');

    // Load Page
    $this->load->view('dashboard/template', array(
        'page' => "Daftar Komplain",
        'pageMap' => array(
            array(
                "label" => "Transaksi",
                "is_current" => false
            ),
            array(
                "label" => "Daftar Komplain",
                "is_current" => true
            )
        ),
        'pageURI' => "dashboard/transactions/pages/lists-complain",
    ));

    ?>
    <!-- END: Content-->

    <!-- START: Page Script JS-->
    <?php
    if (isset($error)) {
        $this->load->view("components/error-modal", array(
            "errorModalTitle" => $error["title"],
            "errorModalContent" => $error["content"],
            "errorModalDetail" => $error["details"]
        ));
    }

    // Detail Transaction Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-detail-transaction",
        "modalTitle" => "Detail Transaksi",
        "modalType" => "modal-xl",
        "iconTitle" => "icon-eye",
        "modalActionForm" => "#",
        "modalContentForm" => '
            <div class="row"
                id="detail-transaction"></div>
        ',
        "modalButtonForm" => '<button type="button" class="btn btn-primary" data-dismiss="modal">Tutup</button>'
    ));

    ?>
    <!-- END: Content-->
    ?>
    <!-- END: Page Script JS-->

    <!-- START: APP JS-->
    <script src="<?= base_url('assets/dist/js/app.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/js/home.script.js'); ?>"></script>

    <script>
        // Initial Pagination
        var page = 0;
        var paginationClicked = false;
        var paginationDirection = "";

        (function ($) {
            "use strict";
            $(window).on("load", function () {
                // Load Bank First Time
                loadTransaction()

                // Handle Order By
                $("#lists-transaction-order").on("change", function() {
                    loadTransaction();
                });

                // Handle Search
                $("#lists-transaction-search").on("keydown", function(event) {
                    if (event.keyCode == 32 || event.which == 32) loadTransaction();
                    else if ($("#lists-transaction-search").val() == "") loadTransaction();
                });

                $("#lists-transaction-button-search").on("click", function(event) {
                    loadTransaction();
                });

                // Handle Pagination
                $("#lists-transaction-prev").on("click", function() {
                    if (page > 0)
                        page -= 1;
                    paginationClicked = true;
                    paginationDirection = "prev";
                    loadTransaction();
                });

                $("#lists-transaction-next").on("click", function() {
                    page += 1;
                    paginationClicked = true;
                    paginationDirection = "next";
                    loadTransaction();
                });
            });
        })(jQuery);

        function loadTransaction() {
            renderToWaiting()

            var order = $("#lists-transaction-order").val();
            var order_direction = "DESC";
            switch (Number(order)) {
                case 1: 
                    order =  "m_users.full_name";
                    order_direction = "ASC";
                break;
                case 2: 
                    order =  "m_users.full_name";
                    order_direction = "DESC";
                break;
                default:
                    order =  "id";
                    order_direction = "DESC";
                break;
            }
            var raw = req.raw({
                page: page,
                search: $("#lists-transaction-search").val(),
                order_by: order,
                order_direction: order_direction
            })

            var formData = new FormData()
            formData.append("raw", raw)

            $.ajax({
                url: base_url.value + "/dashboard/a/managementTransactions/complain_transaction",
                data: formData,
                type: "POST",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.code == 200) {
                        renderToTable(response.data)
                    } else {
                        renderToTable([])
                    }
                }
            });
        }

        function renderToWaiting() {
            $("#lists-transaction tbody").html(`
                <tr>
                    <td class="align-middle" colspan="8">Sedang memuat data</td>
                </tr>
            `);
        }

        var selectedTransaction = [];

        function renderToTable(data=[]) {

            selectedTransaction = data
            
            if (paginationClicked) {
                paginationClicked = false;
                if (data.length == 0) {
                    if (paginationDirection == "next") 
                        page -= 1;
                    else page += 1;
                }
                paginationDirection = "";
                loadTransaction();
                return 
            }

            var index = (page * 10) + 1;

            if (data.length == 0) {
                $("#lists-transaction tbody").html(`
                    <tr>
                        <td class="align-middle" colspan="8">Belum ada data
                    </tr>
                `);
                return;
            }

            $("#lists-transaction tbody").html(`${data.map(function(item, position) {
                var status = Number(item.status);
                switch (status) {
                    case 0: status = '<span class="badge outline-badge-secondary">Belum membayar</span>'; break;
                    case 1: status = '<span class="badge outline-badge-info">Pembayaran Tervalidasi</span>'; break;
                    case 2: status = '<span class="badge outline-badge-warning">Pesanan Diproses</span>'; break;
                    case 3: status = '<span class="badge outline-badge-success">Pesanan Selesai</span>'; break;
                    default: status = '<span class="badge outline-badge-danger">Pesanan Dikembalikan</span>'; break;
                }
                return `
                    <tr>
                        <td class="align-middle">${index++}</td>
                        <td class="align-middle">${item.full_name}</td>
                        <td class="align-middle">${item.transaction_token}</td>
                        <td class="align-middle">${req.money(Number(item.total_price).toString(), "Rp ")}</td>
                        <td class="align-middle">${status}</td>
                        <td class="align-middle">${item.created_at}</td>
                        <td class="align-middle">${item.updated_at}</td>
                        <td class="align-middle">
                            <h4>
                                <a href="javascript:void(0);" data-toggle="modal" data-target="#modal-detail-transaction"
                                onclick="loadDetailTransaction(${position})"
                                class="btn text-primary"><i class="icon-eye"></i></a>
                            </h4>
                        </td>
                    </tr>
                `;
            }).join('')}`);
        }

        function loadDetailTransaction(position) {
            var complain = selectedTransaction[position].complain;
            $("#detail-transaction").html(`
                <div class="table-responsive">
                    <div class="row">
                        <div class="col-12">
                            <table class="display table table-bordered table-hover mx-2" style="width:100%">
                                <thead>
                                    <th>#</th>
                                    <th>ID TRX</th>
                                    <th>Foto Produk</th>
                                    <th>Produk</th>
                                    <th>Subjek</th>
                                    <th>Deskripsi</th>
                                </thead>
                                <tbody>
                                    ${complain.map(function(item, position) {
                                    return `
                                        <tr>
                                            <td>${(position + 1)}</td>
                                            <td>${item.transaction_token}</td>
                                            <td>
                                                <img src="${item.uri}" alt="${item.label}"
                                                    style="max-width: 80px">
                                            </td>
                                            <td>${item.product_name}</td>
                                            <td>${item.subject}</td>
                                            <td>${item.description}</td>
                                        </tr>
                                    `;
                                }).join('')}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                `)
        }
    </script>
    <!-- END: APP JS-->
</body>

</html>