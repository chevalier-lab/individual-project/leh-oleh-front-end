<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('components/head'); ?>

    <!-- START: Template CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.theme.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/simple-line-icons/css/simple-line-icons.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/flags-icon/css/flag-icon.min.css'); ?>">
    <!-- END Template CSS-->

    <!-- START: Page CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/social-button/bootstrap-social.css'); ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/sweetalert/sweetalert.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/toastr/toastr.min.css'); ?>"/>
    <!-- END: Page CSS-->

    <!-- START: Custom CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/css/main.css'); ?>">
    <!-- END: Custom CSS-->
</head>

<body id="main-container" class="default">

    <input type="hidden" name="base_url" 
        id="base_url" value="<?= base_url("index.php"); ?>">
    <input type="hidden" name="api_url" 
        id="api_url" value="<?= API_URI; ?>">

    <!-- START: Pre Loader-->
    <div class="se-pre-con">
        <div class="loader"></div>
    </div>
    <!-- END: Pre Loader-->

    <!-- START: Template JS-->
    <script src="<?= base_url('assets/dist/vendors/jquery/jquery-3.3.1.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/moment/moment.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/slimscroll/jquery.slimscroll.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/toastr/toastr.min.js'); ?>"></script>
    <!-- END: Template JS-->

    <!-- START: Page Vendor JS-->
    <script src="<?= base_url('assets/dist/js/auth/request.js'); ?>"></script>
    <script>
        var base_url = document.getElementById("base_url");
        var api_url = document.getElementById("api_url");
    </script>
    <!-- END: Page Vendor JS-->

    <!-- START: Main Content-->
    <?php

    // Load Navigation Bar
    $this->load->view('components/menus/navbar');

    // Load Sidebar
    $this->load->view('components/menus/sidebar');

    // Load Page
    $this->load->view('dashboard/template', array(
        'page' => "Daftar Transaksi",
        'pageMap' => array(
            array(
                "label" => "Transaksi",
                "is_current" => false
            ),
            array(
                "label" => "Daftar Transaksi",
                "is_current" => true
            ),
        ),
        'pageURI' => "dashboard/transactions/pages/lists-transaction",
    ));

    ?>
    <!-- END: Content-->

    <!-- START: Page Script JS-->
    <?php
    if (isset($error)) {
        $this->load->view("components/error-modal", array(
            "errorModalTitle" => $error["title"],
            "errorModalContent" => $error["content"],
            "errorModalDetail" => $error["details"]
        ));
    }

    // Detail Transaction Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-detail-transaction",
        "modalTitle" => "Detail Transaksi",
        "modalType" => "modal-xl",
        "iconTitle" => "icon-eye",
        "modalActionForm" => "#",
        "modalContentForm" => '
            <div
                id="detail-transaction">
            </div>
        ',
        "modalButtonForm" => '<button type="button" class="btn btn-primary" data-dismiss="modal">Tutup</button>'
    ));

    // Update Status Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-update-status",
        "modalTitle" => "Perbarui Status",
        "modalType" => "modal-md",
        "iconTitle" => "icon-pencil",
        "modalActionForm" => "#",
        "modalContentForm" => '
        <input type="hidden" id="id-transaction" val>
        <input type="hidden" id="val-status">
        <p>Apakah Anda yakin untuk memperbarui transaksi status menjadi <strong id="update-status-text"></strong> ?</p>
        ',
        "modalButtonForm" => '<button type="submit" class="btn btn-primary add-todo" id="update-status">Perbarui Status</button>'
    ));

    ?>
    <!-- END: Page Script JS-->

    <!-- START: APP JS-->
    <script src="<?= base_url('assets/dist/js/app.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/js/home.script.js'); ?>"></script>

    <script>
        // Initial Pagination
        var page = 0;
        var paginationClicked = false;
        var paginationDirection = "";

        (function ($) {
            "use strict";
            $(window).on("load", function () {
                // Load Transaction First Time
                loadTransaction()

                // Handle Order By
                $("#lists-transaction-order").on("change", function() {
                    loadTransaction();
                });

                // Handle Search
                $("#lists-transaction-search").on("keydown", function(event) {
                    if (event.keyCode == 32 || event.which == 32) loadTransaction();
                    else if ($("#lists-transaction-search").val() == "") loadTransaction();
                });

                $("#lists-transaction-button-search").on("click", function(event) {
                    loadTransaction();
                });

                // Handle Pagination
                $("#lists-transaction-prev").on("click", function() {
                    if (page > 0)
                        page -= 1;
                    paginationClicked = true;
                    paginationDirection = "prev";
                    loadTransaction();
                });

                $("#lists-transaction-next").on("click", function() {
                    page += 1;
                    paginationClicked = true;
                    paginationDirection = "next";
                    loadTransaction();
                });

                // Update Status Transaction
                $("#update-status").on("click", function() {
                    $("#update-status").attr("disabled", true);

                    var raw = req.raw({
                        status: $('#val-status').val(),
                    })

                    var formData = new FormData()
                    formData.append("raw", raw)

                    $.ajax({
                        url: base_url.value + "/dashboard/a/managementTransactions/update_status_transaction/" + $('#id-transaction').val(),
                        data: formData,
                        type: "POST",
                        contentType: false,
                        processData: false,
                        success: function(response) {
                            response = req.data(response)

                            $("#update-status").removeAttr("disabled");
                            $("#modal-update-status").modal('hide')

                            if (response.code == 200) {
                                loadTransaction()
                                toastr.success("Berhasil mengubah status")
                            } else {
                                toastr.error("Gagal mengubah status")
                            }
                            
                        }
                    });
                });
            });
        })(jQuery);

        function loadTransaction() {
            renderToWaiting()

            var order = $("#lists-transaction-order").val();
            var order_direction = "DESC";
            switch (Number(order)) {
                case 1: 
                    order =  "m_users.full_name";
                    order_direction = "ASC";
                break;
                case 2: 
                    order =  "m_users.full_name";
                    order_direction = "DESC";
                break;
                default:
                    order =  "id";
                    order_direction = "DESC";
                break;
            }
            var raw = req.raw({
                page: page,
                search: $("#lists-transaction-search").val(),
                order_by: order,
                order_direction: order_direction
            })

            var formData = new FormData()
            formData.append("raw", raw)

            $.ajax({
                url: base_url.value + "/dashboard/a/managementTransactions/load_transaction",
                data: formData,
                type: "POST",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.code == 200) {
                        renderToTable(response.data)
                    } else {
                        renderToTable([])
                    }
                }
            });
        }

        function renderToWaiting() {
            $("#lists-transaction tbody").html(`
                <tr>
                    <td class="align-middle" colspan="9">Sedang memuat data</td>
                </tr>
            `);
        }

        function renderToTable(data=[]) {
            
            if (paginationClicked) {
                paginationClicked = false;
                if (data.length == 0) {
                    if (paginationDirection == "next") 
                        page -= 1;
                    else page += 1;
                }
                paginationDirection = "";
                loadTransaction();
                return 
            }

            var index = (page * 10) + 1;

            if (data.length == 0) {
                $("#lists-transaction tbody").html(`
                    <tr>
                        <td class="align-middle" colspan="9">Belum ada data
                    </tr>
                `);
                return;
            }

            $("#lists-transaction tbody").html(`${data.map(function(item) {
                var status = Number(item.status);
                switch (status) {
                    case 0: status = '<span class="badge outline-badge-secondary">Belum membayar</span>'; break;
                    case 1: status = '<span class="badge outline-badge-info">Pembayaran Tervalidasi</span>'; break;
                    case 2: status = '<span class="badge outline-badge-warning">Pesanan Diproses</span>'; break;
                    case 3: status = '<span class="badge outline-badge-success">Pesanan Selesai</span>'; break;
                    default: status = '<span class="badge outline-badge-danger">Pesanan Dikembalikan</span>'; break;
                }
                return `
                    <tr>
                        <td class="align-middle">${index++}</td>
                        <td class="align-middle">${item.full_name}</td>
                        <td class="align-middle">${item.transaction_token}</td>
                        <td class="align-middle">${req.money(Number(item.total_price).toString(), "Rp ")}</td>
                        <td class="align-middle">${status}</td>
                        <td class="align-middle">${item.created_at}</td>
                        <td class="align-middle">${item.updated_at}</td>
                        <td class="align-middle">
                            <h4>
                                <a href="javascript:void(0);" data-toggle="modal" data-target="#modal-detail-transaction" 
                                onclick="loadDetailTransaction(${item.id})"
                                class="btn text-primary"><i class="icon-eye"></i></a>
                            </h4>
                        </td>
                        <td class="align-middle">
                            <div class="btn-group mb-3">
                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Perbarui Status</button>
                                <div class="dropdown-menu p-0" style="">
                                    <a class="dropdown-item" data-toggle="modal" data-target="#modal-update-status" href="javascript:void(0);" onclick="loadIdTransaction(${item.id}, 0, this.textContent)">Belum membayar</a>
                                    <a class="dropdown-item" data-toggle="modal" data-target="#modal-update-status" href="javascript:void(0);" onclick="loadIdTransaction(${item.id}, 1, this.textContent)">Pembayaran Tervalidasi</a>
                                    <a class="dropdown-item" data-toggle="modal" data-target="#modal-update-status" href="javascript:void(0);" onclick="loadIdTransaction(${item.id}, 2, this.textContent)">Pesanan Diproses</a>
                                    <a class="dropdown-item" data-toggle="modal" data-target="#modal-update-status" href="javascript:void(0);" onclick="loadIdTransaction(${item.id}, 3, this.textContent)">Pesanan Selesai</a>
                                    <a class="dropdown-item" data-toggle="modal" data-target="#modal-update-status" href="javascript:void(0);" onclick="loadIdTransaction(${item.id}, 4, this.textContent)">Pesanan Dikembalikan</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                `;
            }).join('')}`);
        }

        function loadDetailTransaction(id) {
            renderDetailTransaction()
            $.ajax({
                url: base_url.value + "/dashboard/a/managementTransactions/detail_transaction/" + id,
                data: null,
                type: "GET",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.code == 200) {
                        var data = response.data
                        var products = data.products
                        $("#detail-transaction").html(`
                        <div class="table-responsive">
                            <div class="row">
                                <div class="col-12">
                                    <table class="display table table-bordered table-hover mx-2" style="width:100%">
                                        <thead>
                                            <th>#</th>
                                            <th>ID TRX</th>
                                            <th>Foto Produk</th>
                                            <th>Produk</th>
                                            <th>Jumlah</th>
                                            <th>Subtotal Harga</th>
                                            <th>Status</th>
                                            <th>Aksi</th>
                                        </thead>
                                        <tbody>
                                            ${products.map(function(item, position) {
                                            var status = Number(item.status);
                                            switch (status) {
                                                case 0: status = '<span class="badge outline-badge-secondary">Belum membayar</span>'; break;
                                                case 1: status = '<span class="badge outline-badge-info">Pembayaran Tervalidasi</span>'; break;
                                                case 2: status = '<span class="badge outline-badge-warning">Pesanan Diproses</span>'; break;
                                                case 3: status = '<span class="badge outline-badge-success">Pesanan Selesai</span>'; break;
                                                default: status = '<span class="badge outline-badge-danger">Pesanan Dikembalikan</span>'; break;
                                            }
                                            return `
                                                <tr>
                                                    <td>${(position + 1)}</td>
                                                    <td>${item.token_products}</td>
                                                    <td>
                                                        <img src="${item.uri}" alt="${item.label}"
                                                            style="max-width: 80px">
                                                    </td>
                                                    <td>${item.product_name}</td>
                                                    <td>${item.qty}</td>
                                                    <td>${req.money(Number(item.sub_total_price).toString(), "Rp ")}</td>
                                                    <td>${status}</td>
                                                    <td>
                                                    <div class="btn-group mb-3">
                                                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Perbarui Status</button>
                                                        <div class="dropdown-menu p-0" style="">
                                                            <a class="dropdown-item" href="javascript:void(0);" 
                                                            onclick="changeStatusProduct(${id}, ${item.id}, 0, this.textContent)">Belum membayar</a>
                                                            <a class="dropdown-item" href="javascript:void(0);" 
                                                            onclick="changeStatusProduct(${id}, ${item.id}, 1, this.textContent)">Pembayaran Tervalidasi</a>
                                                            <a class="dropdown-item" href="javascript:void(0);" 
                                                            onclick="changeStatusProduct(${id}, ${item.id}, 2, this.textContent)">Pesanan Diproses</a>
                                                            <a class="dropdown-item" href="javascript:void(0);" 
                                                            onclick="changeStatusProduct(${id}, ${item.id}, 3, this.textContent)">Pesanan Selesai</a>
                                                            <a class="dropdown-item" href="javascript:void(0);" 
                                                            onclick="changeStatusProduct(${id}, ${item.id}, 4, this.textContent)">Pesanan Dikembalikan</a>
                                                        </div>
                                                    </div>
                                                    </td>
                                                </tr>
                                            `;
                                        }).join('')}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        `)
                    } else {
                        $("modal-detail-transaction").modal('hide');
                        toastr.error(response.message);
                    }
                }
            });
        }

        function changeStatusProduct(parentID, id, status, text){
            var raw = req.raw({
                status: status
            })

            var formData = new FormData()
            formData.append("raw", raw)

            $.ajax({
                url: base_url.value + "/dashboard/a/managementTransactions/update_status_product/" + id,
                data: formData,
                type: "POST",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.code == 200) {
                        loadDetailTransaction(parentID)
                        toastr.success("Berhasil mengubah status")
                    } else {
                        toastr.error("Gagal mengubah status")
                    }
                    
                }
            });
        }

        function loadIdTransaction(id, status, text){
            $('#id-transaction').val(id)
            $('#val-status').val(status)
            $('#update-status-text').text(text);
        }

        function renderDetailTransaction() {
            $("#detail-transaction").html(`
                <p class="text-center">Sedang memuat data transaksi</p>
            `);
        }
    </script>
    <!-- END: APP JS-->
</body>

</html>