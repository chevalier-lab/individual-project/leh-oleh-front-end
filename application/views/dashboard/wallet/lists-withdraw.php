<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('components/head'); ?>

    <!-- START: Template CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.theme.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/simple-line-icons/css/simple-line-icons.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/flags-icon/css/flag-icon.min.css'); ?>">
    <!-- END Template CSS-->

    <!-- START: Page CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/social-button/bootstrap-social.css'); ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/sweetalert/sweetalert.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/toastr/toastr.min.css'); ?>"/>
    <!-- END: Page CSS-->

    <!-- START: Custom CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/css/main.css'); ?>">
    <!-- END: Custom CSS-->
</head>

<body id="main-container" class="default">

    <input type="hidden" name="base_url" 
        id="base_url" value="<?= base_url("index.php"); ?>">
    <input type="hidden" name="api_url" 
        id="api_url" value="<?= API_URI; ?>">

    <!-- START: Pre Loader-->
    <div class="se-pre-con">
        <div class="loader"></div>
    </div>
    <!-- END: Pre Loader-->

    <!-- START: Template JS-->
    <script src="<?= base_url('assets/dist/vendors/jquery/jquery-3.3.1.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/moment/moment.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/slimscroll/jquery.slimscroll.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/toastr/toastr.min.js'); ?>"></script>
    <!-- END: Template JS-->

    <!-- START: Page Vendor JS-->
    <script src="<?= base_url('assets/dist/js/auth/request.js'); ?>"></script>
    <script>
        var base_url = document.getElementById("base_url");
        var api_url = document.getElementById("api_url");
    </script>
    <!-- END: Page Vendor JS-->

    <!-- START: Main Content-->
    <?php

    // Load Navigation Bar
    $this->load->view('components/menus/navbar');

    // Load Sidebar
    $this->load->view('components/menus/sidebar');

    // Load Page
    $this->load->view('dashboard/template', array(
        'page' => "Daftar Tarik Saldo",
        'pageMap' => array(
            array(
                "label" => "Dompet",
                "is_current" => false
            ),
            array(
                "label" => "Daftar Tarik Saldo",
                "is_current" => true
            ),
        ),
        'pageURI' => "dashboard/wallet/pages/lists-withdraw",
    ));

    ?>
    <!-- END: Content-->

    <!-- START: Page Script JS-->
    <?php
    if (isset($error)) {
        $this->load->view("components/error-modal", array(
            "errorModalTitle" => $error["title"],
            "errorModalContent" => $error["content"],
            "errorModalDetail" => $error["details"]
        ));
    }

    // Detail Top Up Proof Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-detail-top-up",
        "modalTitle" => "Detail Tarik Saldo",
        "modalType" => "modal-md",
        "iconTitle" => "icon-eye",
        "modalActionForm" => "#",
        "modalContentForm" => '
        <div class="container-fluid"
        id="detail-top-up-container"></div>
        ',
        "modalButtonForm" => ''
    ));

    // Update Status Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-update-status",
        "modalTitle" => "Perbarui Status",
        "modalType" => "modal-md",
        "iconTitle" => "icon-pencil",
        "modalActionForm" => "#",
        "modalContentForm" => '
        <input type="hidden" id="id-withdraw">
        <input type="hidden" id="status-withdraw">
        <p>Apakah Anda yakin untuk memperbarui status penarikan menjadi <strong id="update-status-text"></strong> ?</p>
        ',
        "modalButtonForm" => '<button type="button" 
        id="btn-update-status"
        class="btn btn-primary add-todo">Update Status</button>'
    ));

    ?>
    <!-- END: Page Script JS-->

    <!-- START: APP JS-->
    <script src="<?= base_url('assets/dist/js/app.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/js/home.script.js'); ?>"></script>

    <script>
        // Initial Pagination
        var page = 0;
        var paginationClicked = false;
        var paginationDirection = "";
        var selectedTopUp = null;
        var selectedTopUpType = -1;
        var topUp = [];

        (function ($) {
            "use strict";
            $(window).on("load", function () {
                // Load Bank First Time
                loadWithdraw()

                // Handle Order By
                $("#lists-withdraw-order").on("change", function() {
                    loadWithdraw();
                });

                // Handle Search
                $("#lists-withdraw-search").on("keydown", function(event) {
                    if (event.keyCode == 32 || event.which == 32) loadWithdraw();
                    else if ($("#lists-withdraw-search").val() == "") loadWithdraw();
                });

                $("#lists-withdraw-button-search").on("click", function(event) {
                    loadWithdraw();
                });

                // Handle Pagination
                $("#lists-withdraw-prev").on("click", function() {
                    if (page > 0)
                        page -= 1;
                    paginationClicked = true;
                    paginationDirection = "prev";
                    loadWithdraw();
                });

                $("#lists-withdraw-next").on("click", function() {
                    page += 1;
                    paginationClicked = true;
                    paginationDirection = "next";
                    loadWithdraw();
                });

                $("#btn-update-status").on("click", function() {
                    $("#btn-update-status").attr("disabled", true)
                    var raw = req.raw({
                        status: selectedTopUpType
                    })

                    var formData = new FormData()
                    formData.append("raw", raw)

                    $.ajax({
                        url: base_url.value + "/dashboard/a/managementWallets/change_status_withdraw/" + selectedTopUp.id,
                        data: formData,
                        type: "POST",
                        contentType: false,
                        processData: false,
                        success: function(response) {
                            response = req.data(response)
                            if (response.code == 200) {
                                toastr.success("Berhasil mengubah status withdraw")
                                loadWithdraw();
                            } else {
                                toastr.error(response.message)
                            }
                            $("#btn-update-status").removeAttr("disabled")
                            $("#modal-update-status").modal("hide");
                        }
                    });
                });
            });
        })(jQuery);

        function loadWithdraw() {
            renderToWaiting()

            var order = $("#lists-withdraw-order").val();
            var order_direction = "DESC";
            switch (Number(order)) {
                case 1: 
                    order =  "full_name";
                    order_direction = "ASC";
                break;
                case 2: 
                    order =  "full_name";
                    order_direction = "DESC";
                break;
                default:
                    order =  "id";
                    order_direction = "DESC";
                break;
            }
            var raw = req.raw({
                page: page,
                search: $("#lists-withdraw-search").val(),
                order_by: order,
                order_direction: order_direction
            })

            var formData = new FormData()
            formData.append("raw", raw)

            $.ajax({
                url: base_url.value + "/dashboard/a/managementWallets/load_withdraw",
                data: formData,
                type: "POST",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.code == 200) {
                        renderToTable(response.data)
                    } else {
                        renderToTable([])
                    }
                }
            });
        }

        function renderToWaiting() {
            $("#lists-withdraw tbody").html(`
                <tr>
                    <td class="align-middle" colspan="10">Sedang memuat data</td>
                </tr>
            `);
        }

        function renderToTable(data=[]) {
            
            if (paginationClicked) {
                paginationClicked = false;
                if (data.length == 0) {
                    if (paginationDirection == "next") 
                        page -= 1;
                    else page += 1;
                }
                paginationDirection = "";
                loadWithdraw();
                return 
            }

            topUp = data;

            var index = (page * 10) + 1;

            if (data.length == 0) {
                $("#lists-withdraw tbody").html(`
                    <tr>
                        <td class="align-middle" colspan="10">Belum ada data</td>
                    </tr>
                `);
                return;
            }

            $("#lists-withdraw tbody").html(`${data.map(function(item, position) {
                
                var status = Number(item.status)
                var action = ``;
                if (status == 0) {
                    status = '<span class="badge outline-badge-danger">Gagal</span>';
                    action = `
                    <div class="btn-group mb-3">
                        <button type="button" class="btn btn-primary" disabled>Ubah Status</button>
                    </div>
                    `;
                }
                else if (status == 1) {
                    status = '<span class="badge outline-badge-success">Berhasil</span>';
                    action = `
                    <div class="btn-group mb-3">
                        <button type="button" class="btn btn-primary" disabled>Ubah Status</button>
                    </div>
                    `;
                }
                else if (status == 2) {
                    status = '<span class="badge outline-badge-warning">Tertunda</span>';
                    action = `
                    <div class="btn-group mb-3">
                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Ubah Status</button>
                        <div class="dropdown-menu p-0" style="">
                            <a class="dropdown-item"
                            onclick="setSelectedTopUp(${position}, 0); $('#update-status-text').text('Ditolak')"
                            data-toggle="modal" data-target="#modal-update-status" href="javascript:void(0);">Tolak</a>
                            <a class="dropdown-item" 
                            onclick="setSelectedTopUp(${position}, 1); $('#update-status-text').text('Diterima')"
                            data-toggle="modal" data-target="#modal-update-status" href="javascript:void(0);">Terima</a>
                        </div>
                    </div>
                    `;
                }

                return `
                    <tr>
                        <td class="align-middle">${index++}</td>
                        <td class="align-middle">${item.full_name}</td>
                        <td class="align-middle">${item.wallet_name}</td>
                        <td class="align-middle">${req.money(Number(item.balance_request).toString(), "Rp ")}</td>
                        <td class="align-middle">${status}</td>
                        <td class="align-middle">${(item.created_at != null) ? item.created_at: '-'}</td>
                        <td class="align-middle">${(item.updated_at != null) ? item.updated_at: '-'}</td>
                        <td class="align-middle">
                            <h4>
                                <a href="javascript:void(0)" 
                                data-toggle="modal" 
                                onclick="setDetailTopUp(${position})"
                                data-target="#modal-detail-top-up" class="text-primary"><i class="icon-eye"></i></a>
                            </h4>
                        </td>
                        <td class="align-middle">
                            ${action}
                        </td>
                    </tr>
                `;
            }).join('')}`);
        }

        function setDetailTopUp(position) {
            selectedTopUp = topUp[position]
            $("#detail-top-up-container").html("Sedang memuat...")
            $.ajax({
                url: base_url.value + "/dashboard/a/managementWallets/detail_withdraw/" + selectedTopUp.id,
                data: null,
                type: "GET",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.code == 200) {
                        var data = response.data;
                        var status = Number(data.status)
                        if (status == 0) status = '<span class="badge outline-badge-danger">Gagal</span>';
                        else if (status == 1) status = '<span class="badge outline-badge-success">Berhasil</span>';
                        else if (status == 2) status = '<span class="badge outline-badge-warning">Tertunda</span>';
                        
                        $("#detail-top-up-container").html(`
                            <div class="row">
                                <div class="col-12">
                                    <h4>Informasi Withdraw</h4>
                                    <ul class="list-group list-group-flush">
                                        <li class="list-group-item"><strong>Nama pengguna: </strong> ${(data.full_name != null) ? data.full_name: '-'}</li>
                                        <li class="list-group-item"><strong>Jumlah withdraw: </strong> ${req.money(Number(data.balance_request).toString(), "Rp ")}</li>
                                        <li class="list-group-item"><strong>Status: </strong> ${status}</li>
                                        <li class="list-group-item"><strong>Tanggal pengajuan: </strong> ${(data.created_at != null) ? data.created_at: '-'}</li>
                                        <li class="list-group-item"><strong>Tanggal perubahan: </strong> ${(data.updated_at != null) ? data.updated_at: '-'}</li>
                                        <li class="list-group-item"><strong>Nama bank: </strong> ${(data.bank_name != null) ? data.bank_name: '-'}</li>
                                        <li class="list-group-item"><strong>Catatan: </strong> ${(data.note != null) ? data.note: '-'}</li>
                                    </ul>
                                </div>
                            </div>
                        `);
                    } else {
                        toastr.error("Terjadi kesalahan, gagal memuat data");
                        // $("#modal-detail-top-up").modal("hide");
                    }
                }
            });
        }

        function setSelectedTopUp(position, type) {
            selectedTopUp = topUp[position]
            selectedTopUpType = type;
        }
    </script>
    <!-- END: APP JS-->
</body>

</html>