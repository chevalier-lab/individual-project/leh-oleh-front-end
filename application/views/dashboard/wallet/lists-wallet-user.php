<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('components/head'); ?>

    <!-- START: Template CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.theme.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/simple-line-icons/css/simple-line-icons.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/flags-icon/css/flag-icon.min.css'); ?>">
    <!-- END Template CSS-->

    <!-- START: Page CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/social-button/bootstrap-social.css'); ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/sweetalert/sweetalert.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/toastr/toastr.min.css'); ?>"/>
    <!-- END: Page CSS-->

    <!-- START: Custom CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/css/main.css'); ?>">
    <!-- END: Custom CSS-->
</head>

<body id="main-container" class="default">

    <input type="hidden" name="base_url" 
        id="base_url" value="<?= base_url("index.php"); ?>">
    <input type="hidden" name="api_url" 
        id="api_url" value="<?= API_URI; ?>">

    <!-- START: Pre Loader-->
    <div class="se-pre-con">
        <div class="loader"></div>
    </div>
    <!-- END: Pre Loader-->

    <!-- START: Template JS-->
    <script src="<?= base_url('assets/dist/vendors/jquery/jquery-3.3.1.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/moment/moment.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/slimscroll/jquery.slimscroll.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/toastr/toastr.min.js'); ?>"></script>
    <!-- END: Template JS-->

    <!-- START: Page Vendor JS-->
    <script src="<?= base_url('assets/dist/js/auth/request.js'); ?>"></script>
    <script>
        var base_url = document.getElementById("base_url");
        var api_url = document.getElementById("api_url");
    </script>
    <!-- END: Page Vendor JS-->

    <!-- START: Main Content-->
    <?php

    // Load Navigation Bar
    $this->load->view('components/menus/navbar');

    // Load Sidebar
    $this->load->view('components/menus/sidebar');

    // Load Page
    $this->load->view('dashboard/template', array(
        'page' => "Daftar Dompet Pengguna",
        'pageMap' => array(
            array(
                "label" => "Dompet",
                "is_current" => false
            ),
            array(
                "label" => "Daftar Dompet Pengguna",
                "is_current" => true
            ),
        ),
        'pageURI' => "dashboard/wallet/pages/lists-wallet-user",
    ));

    ?>
    <!-- END: Content-->

    <!-- START: Page Script JS-->
    <?php
    if (isset($error)) {
        $this->load->view("components/error-modal", array(
            "errorModalTitle" => $error["title"],
            "errorModalContent" => $error["content"],
            "errorModalDetail" => $error["details"]
        ));
    }

    // Detail Transaction Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-detail-transaction",
        "modalTitle" => "Riwayat Dompet",
        "modalType" => "modal-lg",
        "iconTitle" => "icon-eye",
        "modalActionForm" => "#",
        "modalContentForm" => '
        <input type="hidden" id="id-detail-wallet">
        <div class="pt-0 pb-0 pl-3 pr-3 mb-3 row">
            <div class="input-group col-12 col-md-6 p-1">
                <input type="text" class="form-control p-2 w-100 h-100 contact-search" 
                    placeholder="Cari ..."
                    id="detail-wallet-user-search">
                    <div class="input-group-append">
                        <span class="btn btn-outline-primary input-group-text" id="detail-wallet-user-button-search"><i class="icon-magnifier"
                            aria-hidden="true"></i></span>
                    </div>
            </div>
            <div class="col-12 col-md-6 p-1">
                <select class="form-control p-2 w-100 h-100 contact-search"
                    id="detail-wallet-user-order">
                    <option value="0">Urutkan berdasarkan</option>
                    <option value="1">Jenis Transaksi (Keluar)</option>
                    <option value="2">Jenis Transaksi (Masuk)</option>
                    <option value="3">Tgl Pembuatan (Terbaru)</option>
                    <option value="4">Tgl Pembuatan (Terlama)</option>
                </select>
            </div>
        </div>
        <div class="table-responsive">
            <table id="table-detail-wallet" class="display table table-bordered table-hover" style="width:100%">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nama Pengguna</th>
                        <th>Dompet</th>
                        <th>Jenis Trasaksi</th>
                        <th>Saldo</th>
                        <th>Status</th>
                        <th>Tgl Pembuatan</th>
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="p-1">
                    <div class="btn-group" role="group" aria-label="Pagination Group">
                        <button class="btn btn-outline-secondary" type="button" 
                        id="detail-wallet-user-prev"><i class="icon-arrow-left-circle"></i> Sebelumnya</button>
                        <button class="btn btn-outline-secondary" type="button" 
                        id="detail-wallet-user-next">Selanjutnya <i class="icon-arrow-right-circle"></i></button>
                    </div>
                </div>
            </div>
        </div>
        ',
        "modalButtonForm" => '<button type="button" class="btn btn-primary" data-dismiss="modal">Tutup</button>'
    ));

    // Update Status Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-update-status",
        "modalTitle" => "Update Status",
        "modalType" => "modal-md",
        "iconTitle" => "icon-pencil",
        "modalActionForm" => "#",
        "modalContentForm" => '
        <p>Are you sure to update status wallet ?</p>
        ',
        "modalButtonForm" => '<button type="submit" class="btn btn-primary add-todo">Update Status</button>'
    ));

    ?>
    <!-- END: Page Script JS-->

    <!-- START: APP JS-->
    <script src="<?= base_url('assets/dist/js/app.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/js/home.script.js'); ?>"></script>

    <script>
        // Initial Pagination
        var page = 0;
        var paginationClicked = false;
        var paginationDirection = "";

        var pageDetail = 0;
        var pageDetailClicked = false;
        var pageDetailDirection = "";

        (function ($) {
            "use strict";
            $(window).on("load", function () {
                // Load Bank First Time
                loadWalletUser()

                // Handle Order By
                $("#lists-wallet-user-order").on("change", function() {
                    loadWalletUser();
                });

                // Handle Search
                $("#lists-wallet-user-search").on("keydown", function(event) {
                    if (event.keyCode == 32 || event.which == 32) loadWalletUser();
                    else if ($("#lists-wallet-user-search").val() == "") loadWalletUser();
                });

                $("#lists-wallet-user-button-search").on("click", function(event) {
                    loadWalletUser();
                });

                // Handle Pagination
                $("#lists-wallet-user-prev").on("click", function() {
                    if (page > 0)
                        page -= 1;
                    paginationClicked = true;
                    paginationDirection = "prev";
                    loadWalletUser();
                });

                $("#lists-wallet-user-next").on("click", function() {
                    page += 1;
                    paginationClicked = true;
                    paginationDirection = "next";
                    loadWalletUser();
                });

                // Handle Detail Order By
                $("#detail-wallet-user-order").on("change", function() {
                    loadDetailWallet();
                });

                // Handle Detail Search
                $("#detail-wallet-user-search").on("keydown", function(event) {
                    if (event.keyCode == 32 || event.which == 32) loadDetailWallet();
                    else if ($("#detail-wallet-user-search").val() == "") loadDetailWallet();
                });

                $("#detail-wallet-user-button-search").on("click", function(event) {
                    loadDetailWallet();
                });

                // Handle Detail Pagination
                $("#detail-wallet-user-prev").on("click", function() {
                    if (pageDetail > 0)
                        pageDetail -= 1;
                    pageDetailClicked = true;
                    pageDetailDirection = "prev";
                    loadDetailWallet();
                });

                $("#detail-wallet-user-next").on("click", function() {
                    pageDetail += 1;
                    pageDetailClicked = true;
                    pageDetailDirection = "next";
                    loadDetailWallet();
                });

                // Handle Delete Page
                $("#delete-page").on("click", function() {
                    $.ajax({
                        url: base_url.value + "/dashboard/a/managementPages/delete_page/" + $("#delete-page").val(),
                        data: null,
                        type: "GET",
                        contentType: false,
                        processData: false,
                        success: function(response) {
                            response = req.data(response)
                            if (response.code == 200) {
                                loadWalletUser()
                                toastr.success(response.message);
                            } else {
                                toastr.error(response.message);
                            }
                        }
                    });
                });
            });
        })(jQuery);

        function loadWalletUser() {
            renderToWaiting()

            var order = $("#lists-wallet-user-order").val();
            var order_direction = "DESC";
            switch (Number(order)) {
                case 1: 
                    order =  "full_name";
                    order_direction = "ASC";
                break;
                case 2: 
                    order =  "full_name";
                    order_direction = "DESC";
                break;
                default:
                    order =  "id";
                    order_direction = "DESC";
                break;
            }
            var raw = req.raw({
                page: page,
                search: $("#lists-wallet-user-search").val(),
                order_by: order,
                order_direction: order_direction
            })

            var formData = new FormData()
            formData.append("raw", raw)

            $.ajax({
                url: base_url.value + "/dashboard/a/managementWallets/load_users_wallet",
                data: formData,
                type: "POST",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.code == 200) {
                        renderToTable(response.data)
                    } else {
                        renderToTable([])
                    }
                }
            });
        }

        function renderToWaiting() {
            $("#lists-wallet-user tbody").html(`
                <tr>
                    <td class="align-middle" colspan="8">Sedang memuat data</td>
                </tr>
            `);
        }

        function renderToTable(data=[]) {
            
            if (paginationClicked) {
                paginationClicked = false;
                if (data.length == 0) {
                    if (paginationDirection == "next") 
                        page -= 1;
                    else page += 1;
                }
                paginationDirection = "";
                loadWalletUser();
                return 
            }

            var index = (page * 10) + 1;

            if (data.length == 0) {
                $("#lists-wallet-user tbody").html(`
                    <tr>
                        <td class="align-middle" colspan="8">Belum ada data, 
                        tekan <span class="icon-plus"></span> untuk menambahkan</td>
                    </tr>
                `);
                return;
            }

            $("#lists-wallet-user tbody").html(`${data.map(function(item) {
                $status = Number(item["is_visible"]);

                if ($status == 0) {
                    $status = `<span class="badge badge-danger">Terblokir</span>`;
                }
                else if ($status == 1) {
                    $status = `<span class="badge badge-primary">Aktif</span>`;
                }
                else {
                    $status = `<span class="badge badge-warning">Menunggu Keputusan</span>`;;
                }

                return `
                    <tr>
                        <td class="align-middle">${index++}</td>
                        <td class="align-middle">${item.full_name}</td>
                        <td class="align-middle">${item.wallet_name}</td>
                        <td class="align-middle">${req.money(item.balance.toString(), "Rp ")}</td>
                        <td class="align-middle">${$status}</td>
                        <td class="align-middle">${item.created_at}</td>
                        <td class="align-middle">${item.updated_at}</td>
                        <td class="align-middle">
                            <h4>
                                <a href="javascript:void(0);" data-toggle="modal" data-target="#modal-detail-transaction" class="text-primary" onclick="loadDetailWallet(${item.id})"><i class="icon-eye"></i></a>
                            </h4>
                        </td>
                    </tr>
                `;
            }).join('')}`);
        }

        function loadDetailWallet(id = -1) {
            var idReq;
            if(id === -1){
                idReq = Number($('#id-detail-wallet').val())
            } else {
                $('#id-detail-wallet').val(id)
                idReq = id
            }
            renderToWaitingDetail()

            var order = $("#detail-wallet-user-order").val();
            var order_direction = "DESC";
            switch (Number(order)) {
                case 1: 
                    order =  "type";
                    order_direction = "ASC";
                break;
                case 2: 
                    order =  "type";
                    order_direction = "DESC";
                break;
                case 3: 
                    order =  "created_at";
                    order_direction = "ASC";
                break;
                case 4: 
                    order =  "created_at";
                    order_direction = "DESC";
                break;
                default:
                    order =  "id";
                    order_direction = "DESC";
                break;
            }
            var raw = req.raw({
                page: pageDetail,
                search: $("#detail-wallet-user-search").val(),
                order_by: order,
                order_direction: order_direction
            })

            var formData = new FormData()
            formData.append("raw", raw)

            $.ajax({
                url: base_url.value + "/dashboard/a/managementWallets/detail_wallet/" + idReq,
                data: formData,
                type: "POST",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.code == 200) {
                        renderToTableDetail(response.data)
                    } else {
                        renderToTableDetail([])
                    }
                }
            });
        }

        function renderToWaitingDetail() {
            $("#table-detail-wallet tbody").html(`
                <tr>
                    <td class="align-middle" colspan="7">Sedang memuat data riwayat</td>
                </tr>
            `);
        }

        function renderToTableDetail(data=[]) {
            
            if (pageDetailClicked) {
                pageDetailClicked = false;
                if (data.logs.length == 0) {
                    if (pageDetailDirection == "next") 
                        pageDetail -= 1;
                    else pageDetail += 1;
                }
                pageDetailDirection = "";
                loadDetailWallet();
                return 
            }

            var index = (pageDetail * 10) + 1;

            if (data.logs.length == 0) {
                $("#table-detail-wallet tbody").html(`
                    <tr>
                        <td class="align-middle" colspan="7">Belum ada riwayat</td>
                    </tr>
                `);
                return;
            }

            $("#table-detail-wallet tbody").html(`${data.logs.map(function(item, position) {
                status = Number(item.status);

                if (status == 0) {
                    status = '<span class="badge outline-badge-danger">Gagal</span>';
                }
                else{
                    status = '<span class="badge outline-badge-success">Sukses</span>';
                }

                return `
                    <tr>
                        <td>${position + 1}</td>
                        <td>${data.full_name}</td>
                        <td>${data.wallet_name}</td>
                        <td>${Number(item.type) == 0 ? "Masuk" : "Keluar"}</td>
                        <td>${item.balance}</td>
                        <td>${status}</td>
                        <td>${item.created_at}</td>
                    </tr>`;
            }).join('')}`);
        }
    </script>
    <!-- END: APP JS-->
</body>

</html>