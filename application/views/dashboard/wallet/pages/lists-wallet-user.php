<div class="card mb-3">
    <div class="card-header">
        <h4 class="card-title">Daftar Dompet Pengguna</h4>
    </div>
    <div class="card-body">

        <div class="pt-0 pb-0 pl-3 pr-3 mb-3 row">
            <div class="input-group col-12 col-md-6 p-1">
                <input type="text" class="form-control p-2 w-100 h-100 contact-search" 
                    placeholder="Cari ..."
                    id="lists-wallet-user-search">
                    <div class="input-group-append">
                        <span class="btn btn-outline-primary input-group-text" id="lists-wallet-user-button-search"><i class="icon-magnifier"
                            aria-hidden="true"></i></span>
                    </div>
            </div>
            <div class="col-12 col-md-6 p-1">
                <select class="form-control p-2 w-100 h-100 contact-search"
                    id="lists-wallet-user-order">
                    <option value="0">Urutkan berdasarkan</option>
                    <option value="1">Nama (A-Z)</option>
                    <option value="2">Nama (Z-A)</option>
                </select>
            </div>
        </div>

        <div class="table-responsive pr-1 pl-1 mb-0">

            <?php

            $this->load->view('components/table', array(
                "idTable" => "lists-wallet-user",
                "isCustomThead" => true,
                "thead" => '
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nama Lengkap</th>
                    <th scope="col">Dompet</th>
                    <th scope="col">Saldo</th>
                    <th scope="col">Status</th>
                    <th scope="col">Tgl Pembuatan</th>
                    <th scope="col">Tgl Perubahan</th>
                    <th scope="col">Detail</th>
                </tr>
                ',
                "tbody" => ''
            )) ?>

        </div>

        <div class="row">
            <div class="col-12">
                <div class="p-1">
                    <div class="btn-group" role="group" aria-label="Pagination Group">
                        <button class="btn btn-outline-secondary" type="button" 
                        id="lists-wallet-user-prev"><i class="icon-arrow-left-circle"></i> Sebelumnya</button>
                        <button class="btn btn-outline-secondary" type="button" 
                        id="lists-wallet-user-next">Selanjutnya <i class="icon-arrow-right-circle"></i></button>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>