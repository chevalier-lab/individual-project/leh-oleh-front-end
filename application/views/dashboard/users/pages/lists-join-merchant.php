<div class="card mb-3">
    <div class="card-header">
        <h4 class="card-title">Daftar Permintaan Menjadi Toko</h4>
    </div>
    <div class="card-body">

        <div class="pt-0 pb-0 pl-3 pr-3 mb-3 row">
            <div class="input-group col-12 col-md-6 p-1">
                <input type="text" class="form-control p-2 w-100 h-100 contact-search" 
                    placeholder="Cari ..."
                    id="lists-users-search">
                    <div class="input-group-append">
                        <span class="btn btn-outline-primary input-group-text" id="lists-users-button-search"><i class="icon-magnifier"
                            aria-hidden="true"></i></span>
                    </div>
            </div>
            <div class="col-12 col-md-6 p-1">
                <select class="form-control p-2 w-100 h-100 contact-search"
                    id="lists-users-order">
                    <option value="0">Urutkan berdasarkan</option>
                    <option value="1">Nama (A-Z)</option>
                    <option value="2">Nama (Z-A)</option>
                </select>
            </div>
        </div>

        <div class="table-responsive pr-1 pl-1 mb-0">
        <?php
        $tableBody = "";

        $this->load->view('components/table', array(
            "idTable" => "lists-users",
            "isCustomThead" => true,
            "thead" => '
            <tr>
                <th scope="col" rowspan="2">#</th>
                <th scope="col" rowspan="2">Nama Lengkap</th>
                <th scope="col" rowspan="2">Jenis Kelamin</th>
                <th scope="col" rowspan="2">Alamat</th>
                <th scope="col" rowspan="2">Tgl Lahir</th>
                <th scope="col" rowspan="2">Email</th>
                <th scope="col" rowspan="2">No HP</th>
                <th scope="col" colspan="3" class="text-center">Aksi</th>
            </tr>
            <tr>
                <th class="text-center">Detail</th>
                <th class="text-center">Terima</th>
                <th class="text-center">Tolak</th>
            </tr>
            ',
            "tbody" => $tableBody
        )) ?>

        </div>

        <div class="row">
            <div class="col-12">
                <div class="p-1">
                    <div class="btn-group" role="group" aria-label="Pagination Group">
                        <button class="btn btn-outline-secondary" type="button" 
                        id="lists-users-prev"><i class="icon-arrow-left-circle"></i> Sebelumnya</button>
                        <button class="btn btn-outline-secondary" type="button" 
                        id="lists-users-next">Selanjutnya <i class="icon-arrow-right-circle"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>