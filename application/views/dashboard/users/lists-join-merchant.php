<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('components/head'); ?>

    <!-- START: Template CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.theme.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/simple-line-icons/css/simple-line-icons.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/flags-icon/css/flag-icon.min.css'); ?>">
    <!-- END Template CSS-->

    <!-- START: Page CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/social-button/bootstrap-social.css'); ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/sweetalert/sweetalert.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/toastr/toastr.min.css'); ?>"/>
    <!-- END: Page CSS-->

    <!-- START: Custom CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/css/main.css'); ?>">
    <!-- END: Custom CSS-->
</head>

<body id="main-container" class="default">

    <input type="hidden" name="base_url" 
        id="base_url" value="<?= base_url("index.php"); ?>">
    <input type="hidden" name="api_url" 
        id="api_url" value="<?= API_URI; ?>">

    <!-- START: Pre Loader-->
    <div class="se-pre-con">
        <div class="loader"></div>
    </div>
    <!-- END: Pre Loader-->

    <!-- START: Template JS-->
    <script src="<?= base_url('assets/dist/vendors/jquery/jquery-3.3.1.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/moment/moment.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/slimscroll/jquery.slimscroll.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/toastr/toastr.min.js'); ?>"></script>
    <!-- END: Template JS-->

    <!-- START: Page Vendor JS-->
    <script src="<?= base_url('assets/dist/js/auth/request.js'); ?>"></script>
    <script>
        var base_url = document.getElementById("base_url");
        var api_url = document.getElementById("api_url");
    </script>
    <!-- END: Page Vendor JS-->

    <!-- START: Main Content-->
    <?php

    // Load Navigation Bar
    $this->load->view('components/menus/navbar');

    // Load Sidebar
    $this->load->view('components/menus/sidebar');

    // Load Page
    $this->load->view('dashboard/template', array(
        'page' => "Permintaan Toko",
        'pageMap' => array(
            array(
                "label" => "Pengguna",
                "is_current" => false
            ),
            array(
                "label" => "Permintaan Toko",
                "is_current" => true
            ),
        ),
        'pageURI' => "dashboard/users/pages/lists-join-merchant",
    ));

    ?>
    <!-- END: Content-->

    <!-- START: Page Script JS-->
    <?php
    if (isset($error)) {
        $this->load->view("components/error-modal", array(
            "errorModalTitle" => $error["title"],
            "errorModalContent" => $error["content"],
            "errorModalDetail" => $error["details"]
        ));
    }
    
    // Detail Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-detail-user",
        "modalTitle" => "Detail user",
        "modalType" => "modal-lg",
        "iconTitle" => "icon-check",
        "modalActionForm" => "#",
        "modalContentForm" => '
            <div class="row">
                <div class="col-12 mb-3">
                    <div class="position-relative">
                        <div class="background-image-maker py-5"></div>
                        <div class="holder-image">
                            <img src="'.base_url("assets/dist/images/portfolio13.jpg").'" alt="" class="img-fluid d-none">
                        </div>
                        <div class="position-relative px-3 py-5">
                            <div class="media d-md-flex d-block">
                                <a href="javascript:void(0);"><img 
                                id="detail-user-face"
                                width="100" alt="" class="img-fluid rounded-circle"></a>
                                <div class="media-body z-index-1">
                                    <div class="pl-4">
                                        <h1 class="display-4 text-uppercase text-white mb-0"
                                        id="detail-user-name">Andy Maulana Yusuf</h1>
                                        <h6 class="text-uppercase text-white mb-0"
                                        id="detail-user-type">Administrator</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 

                    <div class="profile-menu mt-4 theme-background border  z-index-1 p-2">
                        <div class="d-sm-flex">
                            <div class="align-self-center">
                                <ul class="nav nav-pills flex-column flex-sm-row" id="detail-user-tabs" role="tablist">
                                    <li class="nav-item ml-0">
                                        <a class="nav-link  py-2 px-3 px-lg-4  active" data-toggle="tab" href="#detail-user-profile"
                                        aria-controls="detail-user-profile" aria-selected="true"
                                        id="detail-user-profile-tab"> Profil</a>
                                    </li>
                                    <li class="nav-item ml-0">
                                        <a class="nav-link  py-2 px-4 px-lg-4 " data-toggle="tab" href="#detail-user-document"
                                        aria-controls="detail-user-document" aria-selected="true"
                                        id="detail-user-document-tab"> Dokumen</a>
                                    </li>                                                        
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="tab-content mt-3" id="detail-user-tab-content">
                        <div class="tab-pane fade show active" id="detail-user-profile" role="tabpanel" 
                            aria-labelledby="detail-user-profile-tab">

                            <div class="row">

                                <div class="col-sm-12 col-md-6">

                                    <div class="card">
                                        <div class="card-body">
                                            <h5 class="card-title">General</h5>
                                            <ul class="list-group list-group-flush"
                                            id="detail-user-bio"></ul>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-sm-12 col-md-6">

                                    <div class="card">
                                        <div class="card-body">
                                            <h5 class="card-title">Kontak</h5>
                                            <ul class="list-group list-group-flush"
                                            id="detail-user-contact"></ul>
                                        </div>
                                    </div>

                                </div>

                            </div>

                        </div>

                        <div class="tab-pane fade show" id="detail-user-document" role="tabpanel" 
                            aria-labelledby="detail-user-document-tab">
                            
                            <div class="row">

                                <div class="col-sm-12 col-md-6 mb-3">

                                    <div class="card">
                                        <div class="card-body">
                                            <h5 class="card-title">General</h5>
                                            <ul class="list-group list-group-flush"
                                            id="detail-user-doc-bio"></ul>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-sm-12 col-md-6 mb-3">

                                    <div class="card">
                                        <div class="card-body">
                                            <h5 class="card-title">Foto KTP</h5>
                                            <ul class="list-group list-group-flush"
                                            id="detail-user-doc-identity"></ul>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-sm-12 col-md-6 mb-3">

                                    <div class="card">
                                        <div class="card-body">
                                            <h5 class="card-title">Foto Pengguna</h5>
                                            <ul class="list-group list-group-flush"
                                            id="detail-user-doc-author"></ul>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-sm-12 col-md-6 mb-3">

                                    <div class="card">
                                        <div class="card-body">
                                            <h5 class="card-title">Foto Toko</h5>
                                            <ul class="list-group list-group-flush"
                                            id="detail-user-doc-market"></ul>
                                        </div>
                                    </div>

                                </div>

                            </div>
                            
                        </div>
                    </div>

                </div>
            </div>
        ',
        "modalButtonForm" => '<button type="button" class="btn btn-primary" data-dismiss="modal">Tutup</button>'
    ));

    // Accept Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-accept-user",
        "modalTitle" => "Accept user",
        "modalType" => "modal-md",
        "iconTitle" => "icon-check",
        "modalActionForm" => "#",
        "modalContentForm" => '
        <p>Are you sure to accept user to join merchant <strong id="accept-user-market_name"></strong>?</p>
        ',
        "modalButtonForm" => '<button type="button" class="btn btn-success add-todo"
        id="btn-accept-user">Accept</button>'
    ));

    // Accept Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-reject-user",
        "modalTitle" => "Reject user",
        "modalType" => "modal-md",
        "iconTitle" => "icon-close",
        "modalActionForm" => "#",
        "modalContentForm" => '
        <p>Are you sure to reject user to join merchant <strong id="reject-user-market_name"></strong>?</p>
        <div class="form-group">
            <textarea id="reject-user-reason" 
            class="form-control"
            placeholder="Masukkan alasan reject disini"></textarea>
        </div>
        ',
        "modalButtonForm" => '<button type="button" 
        id="btn-reject-user"
        class="btn btn-danger add-todo">Reject</button>'
    ));

    ?>
    <!-- END: Page Script JS-->

    <!-- START: APP JS-->
    <script src="<?= base_url('assets/dist/js/app.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/js/home.script.js'); ?>"></script>
    <!-- END: APP JS-->

    <script>
        // Initial Pagination
        var page = 0;
        var paginationClicked = false;
        var paginationDirection = "";
        var merchant = [];
        var selectedMerchant = null;

        (function ($) {
            "use strict";
            $(window).on("load", function () {
                // Load Bank First Time
                loadUsers()

                // Handle Order By
                $("#lists-users-order").on("change", function() {
                    loadUsers();
                });

                // Handle Search
                $("#lists-users-search").on("keydown", function(event) {
                    if (event.keyCode == 32 || event.which == 32) loadUsers();
                    else if ($("#lists-users-search").val() == "") loadUsers();
                });

                $("#lists-users-button-search").on("click", function(event) {
                    loadUsers();
                });

                // Handle Pagination
                $("#lists-users-prev").on("click", function() {
                    if (page > 0)
                        page -= 1;
                    paginationClicked = true;
                    paginationDirection = "prev";
                    loadUsers();
                });

                $("#lists-users-next").on("click", function() {
                    page += 1;
                    paginationClicked = true;
                    paginationDirection = "next";
                    loadUsers();
                });

                $("#btn-reject-user").on("click", function() {
                    $("#btn-reject-user").attr("disabled", true);

                    if ($("#reject-user-reason").val() == "") {
                        toastr.error("Harap isi alasan reject terlebih dahulu")
                        $("#btn-reject-user").removeAttr("disabled");
                        return 
                    }

                    var raw = req.raw({
                        type: 0,
                        is_visible: 0
                    })

                    var formData = new FormData()
                    formData.append("raw", raw)

                    $.ajax({
                        url: base_url.value + "/dashboard/a/managementUsers/change_status_user_request/" + selectedMerchant.id_u_user,
                        data: formData,
                        type: "POST",
                        contentType: false,
                        processData: false,
                        success: function(response) {
                            response = req.data(response)
                            $("#btn-reject-user").removeAttr("disabled");
                            if (response.code == 200) {
                                rejectUser()
                            } else {
                                toastr.error(response.message)
                            }
                            $("#modal-reject-user").modal("hide");
                        }
                    });
                });

                $("#btn-accept-user").on("click", function() {
                    $("#btn-accept-user").attr("disabled", true);
                    var raw = req.raw({
                        type: 1,
                        is_visible: 1
                    })

                    var formData = new FormData()
                    formData.append("raw", raw)

                    $.ajax({
                        url: base_url.value + "/dashboard/a/managementUsers/change_status_user_request/" + selectedMerchant.id_u_user,
                        data: formData,
                        type: "POST",
                        contentType: false,
                        processData: false,
                        success: function(response) {
                            response = req.data(response)
                            $("#btn-accept-user").removeAttr("disabled");
                            if (response.code == 200) {
                                toastr.success("Berhasil membanned merchant");
                                loadUsers();
                            } else {
                                toastr.error(response.message)
                            }
                            $("#modal-accept-user").modal("hide");
                        }
                    });
                });
            });
        })(jQuery);

        function rejectUser() {
            var raw = req.raw({
                id_u_user_is_merchant: selectedMerchant.id_u_user_is_merchant,
                reason: $("#reject-user-reason").val()
            })

            var formData = new FormData()
            formData.append("raw", raw)

            $.ajax({
                url: base_url.value + "/dashboard/a/managementUsers/reject_reason_user_request",
                data: formData,
                type: "POST",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.code == 200) {
                        toastr.success("Berhasil mereject merchant");
                        
                    } else {
                        toastr.error(response.message)
                    }
                }
            });
        }

        function loadUsers() {
            renderToWaiting()

            var order = $("#lists-users-order").val();
            var order_direction = "DESC";
            switch (Number(order)) {
                case 1: 
                    order =  "a.full_name";
                    order_direction = "ASC";
                break;
                case 2: 
                    order =  "a.full_name";
                    order_direction = "DESC";
                break;
                default:
                    order =  "a.id";
                    order_direction = "DESC";
                break;
            }
            var raw = req.raw({
                page: page,
                search: $("#lists-users-search").val(),
                order_by: order,
                order_direction: order_direction
            })

            var formData = new FormData()
            formData.append("raw", raw)

            $.ajax({
                url: base_url.value + "/dashboard/a/managementUsers/load_request_users",
                data: formData,
                type: "POST",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.code == 200) {
                        renderToTable(response.data)
                    } else {
                        renderToTable([])
                    }
                }
            });
        }

        function renderToWaiting() {
            $("#lists-users tbody").html(`
                <tr>
                    <td class="text-center" colspan="11">Sedang memuat data</td>
                </tr>
            `);
        }

        function renderToTable(data=[]) {
            
            if (paginationClicked) {
                paginationClicked = false;
                if (data.length == 0) {
                    if (paginationDirection == "next") 
                        page -= 1;
                    else page += 1;
                }
                paginationDirection = "";
                loadUsers();
                return 
            }

            merchant = data;

            var index = (page * 10) + 1;

            if (data.length == 0) {
                $("#lists-users tbody").html(`
                    <tr>
                        <td class="text-center" colspan="10">Tidak ada pengguna yang melakukan permintaan toko</td>
                    </tr>
                `);
                return;
            }

            $("#lists-users tbody").html(`${data.map(function(item, position) {
                return `
                    <tr>
                        <td>${index++}</td>
                        <td>${item.full_name}</td>
                        <td>${item.jk}</td>
                        <td>${item.alamat}</td>
                        <td>${item.tgl_lahir}</td>
                        <td>${(item.email == null) ? "-" : item.email}</td>
                        <td>${(item.phone_number == null) ? "-" : item.phone_number}</td>
                        <td>
                            <h4>
                                <a href="javascript:void(0)" 
                                data-toggle="modal" 
                                data-target="#modal-detail-user" 
                                class="text-primary"
                                onclick="loadDetailUser(${position})"><i class="icon-eye"></i></a>
                            </h4>
                        </td>
                        <td class="align-middle">
                            <h4>
                                <a href="javascript:void(0)" 
                                data-toggle="modal" 
                                data-target="#modal-accept-user" 
                                class="text-success"
                                onclick="setActivateUser(${position})"><i class="icon-check"></i></a>
                            </h4>
                        </td>
                        <td class="align-middle">
                            <h4>
                                <a href="javascript:void(0)" 
                                data-toggle="modal" 
                                data-target="#modal-reject-user" 
                                class="text-danger"
                                onclick="setRejectUser(${position})"><i class="icon-close"></i></a>
                            </h4>
                        </td>
                    </tr>
                `;
            }).join('')}`);
        }

        function setActivateUser(position) {
            selectedMerchant = merchant[position];
            $("#accept-user-market_name").html(selectedMerchant.full_name)
        }

        function setRejectUser(position) {
            selectedMerchant = merchant[position];
            $("#reject-user-market_name").html(selectedMerchant.full_name)
        }

        function loadDetailUser(position) {
            selectedMerchant = merchant[position];
            $.ajax({
                url: base_url.value + "/dashboard/a/managementUsers/detail_user_request/" + selectedMerchant.id_u_user,
                data: null,
                type: "GET",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.code == 200) {
                        var data = response.data;
                        var type = Number(data.type)

                        switch(type) {
                            case 0: type = "Guest"; break;
                            case 1: type = "Merchant"; break;
                            case 2: type = "Administrator"; break;
                            case 3: type = "Banned"; break;
                            default: type = "CS"; break;
                        }

                        $("#detail-user-face").attr("src", data.uri)

                        $("#detail-user-bio").html(`
                            <li class="list-group-item">Username: ${data.username}</li>
                            <li class="list-group-item">Alamat: ${data.alamat}</li>
                            <li class="list-group-item">Tgl Lahir: ${data.tgl_lahir}</li>
                        `);

                        $("#detail-user-contact").html(`
                            <li class="list-group-item">Email: ${(data.email != null) ? data.email : "-"}</li>
                            <li class="list-group-item">Phone Number: ${(data.phone_number != null) ? data.phone_number : "-"}</li>
                        `);

                        $("#detail-user-name").text(`${data.full_name}`)
                        $("#detail-user-type").text(`${type}`)

                        $("#detail-user-doc-bio").html(`
                            <li class="list-group-item">No KTP: ${data.request_info.no_identity}</li>
                            <li class="list-group-item">Nama Toko: ${data.request_info.market_name}</li>
                            <li class="list-group-item">No Telpon Toko: ${data.request_info.market_phone_number}</li>
                            <li class="list-group-item">Alamat Toko: ${data.request_info.market_address}</li>
                        `);

                        $("#detail-user-doc-identity").html(`
                            <li class="list-group-item">
                                <img src="${data.request_info.identity_uri}" class="img-fluid" title="${data.request_info.identity_label}" />
                            </li>
                        `); 

                        $("#detail-user-doc-market").html(`
                            <li class="list-group-item">
                                <img src="${data.request_info.market_uri}" class="img-fluid" title="${data.request_info.market_label}" />
                            </li>
                        `); 

                        $("#detail-user-doc-author").html(`
                            <li class="list-group-item">
                                <img src="${data.request_info.author_uri}" class="img-fluid" title="${data.request_info.author_label}" />
                            </li>
                        `); 
                    } else {
                        $("#modal-detail-user").modal("hide");
                    }
                }
            });
        }
    </script>
</body>

</html>