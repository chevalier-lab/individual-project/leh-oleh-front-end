<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('components/head'); ?>

    <!-- START: Template CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.theme.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/simple-line-icons/css/simple-line-icons.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/flags-icon/css/flag-icon.min.css'); ?>">
    <!-- END Template CSS-->

    <!-- START: Page CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/social-button/bootstrap-social.css'); ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/sweetalert/sweetalert.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/toastr/toastr.min.css'); ?>"/>
    <!-- END: Page CSS-->

    <!-- START: Custom CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/css/main.css'); ?>">
    <!-- END: Custom CSS-->
</head>

<body id="main-container" class="default">

    <input type="hidden" name="base_url" 
        id="base_url" value="<?= base_url("index.php"); ?>">
    <input type="hidden" name="api_url" 
        id="api_url" value="<?= API_URI; ?>">

    <!-- START: Pre Loader-->
    <div class="se-pre-con">
        <div class="loader"></div>
    </div>
    <!-- END: Pre Loader-->

    <!-- START: Template JS-->
    <script src="<?= base_url('assets/dist/vendors/jquery/jquery-3.3.1.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/moment/moment.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/slimscroll/jquery.slimscroll.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/toastr/toastr.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/sweetalert/sweetalert.min.js'); ?>"></script>
    <!-- END: Template JS-->

    <!-- START: Page Vendor JS-->
    <script src="<?= base_url('assets/dist/js/auth/request.js'); ?>"></script>
    <script>
        var base_url = document.getElementById("base_url");
        var api_url = document.getElementById("api_url");
    </script>
    <!-- END: Page Vendor JS-->

    <!-- START: Main Content-->
    <?php

    // Load Navigation Bar
    $this->load->view('components/menus/navbar');

    // Load Sidebar
    $this->load->view('components/menus/sidebar');

    // Load Page
    $this->load->view('dashboard/template', array(
        'page' => "Daftar Pengguna",
        'pageMap' => array(
            array(
                "label" => "Pengguna",
                "is_current" => false
            ),
            array(
                "label" => "Daftar Pengguna",
                "is_current" => true
            ),
        ),
        'pageURI' => "dashboard/users/pages/lists-users",
    ));

    ?>
    <!-- END: Content-->

    <!-- START: Page Script JS-->
    <?php
    if (isset($error)) {
        $this->load->view("components/error-modal", array(
            "errorModalTitle" => $error["title"],
            "errorModalContent" => $error["content"],
            "errorModalDetail" => $error["details"]
        ));
    }

    // Create Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-create-user",
        "modalTitle" => "Menambah Pengguna",
        "modalType" => "modal-lg",
        "iconTitle" => "icon-plus",
        "modalActionForm" => "#",
        "modalContentForm" => '
        <div class="row">
            <div class="col-md-6">
                <div class="form-group mb-3">
                    <label for="create-full_name">Nama Lengkap</label>
                    <input type="text" id="create-full_name" 
                        name="create-full_name" 
                        class="form-control" placeholder="Contoh: Andy Maulana"/> 
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group mb-3">
                    <label for="create-username">Username</label>
                    <input type="text" id="create-username" 
                        name="create-username" 
                        class="form-control" placeholder="Contoh: aitekxxx"/> 
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group mb-3">
                    <label for="create-password">Password</label>
                    <input type="password" id="create-password" 
                        name="create-password" 
                        class="form-control" placeholder="Contoh: password123"/> 
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group mb-3">
                    <label for="create-gender">Jenis Kelamin</label>
                    <select id="create-gender" 
                        name="create-gender" 
                        class="form-control">
                        <option value="l">Pilih Jenis Kelamin</option>
                        <option value="l">Laki - laki</option>
                        <option value="p">Perempuan</option>
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group mb-3">
                    <label for="create-address">Alamat</label>
                    <input type="text" id="create-address" 
                        name="create-address" 
                        class="form-control" placeholder="Contoh: Jl. Telekomunikasi no.xxx"/> 
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group mb-3">
                    <label for="create-birth_of_date">Tanggal Lahir</label>
                    <input type="date" id="create-birth_of_date" 
                        name="create-birth_of_date" 
                        class="form-control"/> 
                </div>
            </div>
        </div>
        ',
        "modalButtonForm" => '<button type="button" class="btn btn-primary add-todo"
            id="create-user">Tambah Pengguna</button>'
    ));

    // Detail Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-detail-user",
        "modalTitle" => "Detail Pengguna",
        "modalType" => "modal-lg",
        "iconTitle" => "icon-check",
        "modalActionForm" => "#",
        "modalContentForm" => '
            <div class="row">
                <div class="col-12 mb-3">
                    <div class="position-relative">
                        <div class="background-image-maker py-5"></div>
                        <div class="holder-image">
                            <img src="'.base_url("assets/dist/images/portfolio13.jpg").'" alt="" class="img-fluid d-none">
                        </div>
                        <div class="position-relative px-3 py-5">
                            <div class="media d-md-flex d-block">
                                <a href="javascript:void(0);"><img 
                                id="detail-user-face"
                                width="100" alt="" class="img-fluid rounded-circle"></a>
                                <div class="media-body z-index-1">
                                    <div class="pl-4">
                                        <h1 class="display-4 text-uppercase text-white mb-0"
                                        id="detail-user-name">Andy Maulana Yusuf</h1>
                                        <h6 class="text-uppercase text-white mb-0"
                                        id="detail-user-type"></h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Umum</h5>
                            <ul class="list-group list-group-flush"
                            id="detail-user-bio"></ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Kontak</h5>
                            <ul class="list-group list-group-flush"
                            id="detail-user-contact"></ul>
                        </div>
                    </div>
                </div>
            </div>
        ',
        "modalButtonForm" => ''
    ));

    // Activate Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-activate-user",
        "modalTitle" => "Activate user",
        "modalType" => "modal-md",
        "iconTitle" => "icon-check",
        "modalActionForm" => "#",
        "modalContentForm" => '
        <p>Apakah Anda yakin untuk mengaktifkan pengguna ?</p>
        ',
        "modalButtonForm" => '<button type="submit" class="btn btn-success add-todo">Aktifkan</button>'
    ));

    // Banned Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-banned-user",
        "modalTitle" => "Banned user",
        "modalType" => "modal-md",
        "iconTitle" => "icon-close",
        "modalActionForm" => "#",
        "modalContentForm" => '
        <p>Apakah Anda yakin akan memblokir pengguna ?</p>
        ',
        "modalButtonForm" => '<button type="submit" class="btn btn-danger add-todo">Blokir</button>'
    ));

    ?>
    <!-- END: Page Script JS-->

    <!-- START: APP JS-->
    <script src="<?= base_url('assets/dist/js/app.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/js/home.script.js'); ?>"></script>

    <script>
        // Initial Pagination
        var page = 0;
        var paginationClicked = false;
        var paginationDirection = "";
        var users = [];
        var selectedUser = null;

        (function ($) {
            "use strict";
            $(window).on("load", function () {
                // Load Bank First Time
                loadUsers()

                // Handle Order By
                $("#lists-users-order").on("change", function() {
                    loadUsers();
                });

                // Handle Search
                $("#lists-users-search").on("keydown", function(event) {
                    if (event.keyCode == 32 || event.which == 32) loadUsers();
                    else if ($("#lists-users-search").val() == "") loadUsers();
                });

                $("#lists-users-button-search").on("click", function(event) {
                    loadUsers();
                });

                // Handle Pagination
                $("#lists-users-prev").on("click", function() {
                    if (page > 0)
                        page -= 1;
                    paginationClicked = true;
                    paginationDirection = "prev";
                    loadUsers();
                });

                $("#lists-users-next").on("click", function() {
                    page += 1;
                    paginationClicked = true;
                    paginationDirection = "next";
                    loadUsers();
                });

                // Create User
                $("#create-user").on("click", function() {
                    $("#create-user").attr("disabled", true);

                    var raw = req.raw({
                        full_name: $("#create-full_name").val(),
                        username: $("#create-username").val(),
                        password: $("#create-password").val(),
                        gender: $("#create-gender").val(),
                        address: $("#create-address").val(),
                        birth_date: $("#create-birth_of_date").val()
                    })

                    var formData = new FormData()
                    formData.append("raw", raw)

                    $.ajax({
                        url: base_url.value + "/dashboard/a/managementUsers/create_user",
                        data: formData,
                        type: "POST",
                        contentType: false,
                        processData: false,
                        success: function(response) {
                            response = req.data(response)

                            $("#create-user").removeAttr("disabled");
                            $("#modal-create-user").modal('hide')

                            if (response.code == 200) {
                                toastr.success(response.message);
                                // Refresh Table
                                loadUsers()
                            } else {
                                toastr.error(response.message);
                            }
                        }
                    });
                });
            });
        })(jQuery);

        function loadUsers() {
            renderToWaiting()

            var order = $("#lists-users-order").val();
            var order_direction = "DESC";
            switch (Number(order)) {
                case 1: 
                    order =  "a.full_name";
                    order_direction = "ASC";
                break;
                case 2: 
                    order =  "a.full_name";
                    order_direction = "DESC";
                break;
                default:
                    order =  "a.id";
                    order_direction = "DESC";
                break;
            }
            var raw = req.raw({
                page: page,
                search: $("#lists-users-search").val(),
                order_by: order,
                order_direction: order_direction
            })

            var formData = new FormData()
            formData.append("raw", raw)

            $.ajax({
                url: base_url.value + "/dashboard/a/managementUsers/load_users",
                data: formData,
                type: "POST",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.code == 200) {
                        renderToTable(response.data)
                    } else {
                        renderToTable([])
                    }
                }
            });
        }

        function renderToWaiting() {
            $("#lists-users tbody").html(`
                <tr>
                    <td class="align-middle" colspan="11">Sedang memuat data</td>
                </tr>
            `);
        }

        function renderToTable(data=[]) {
            
            if (paginationClicked) {
                paginationClicked = false;
                if (data.length == 0) {
                    if (paginationDirection == "next") 
                        page -= 1;
                    else page += 1;
                }
                paginationDirection = "";
                loadUsers();
                return 
            }

            var index = (page * 10) + 1;

            if (data.length == 0) {
                $("#lists-users tbody").html(`
                    <tr>
                        <td class="align-middle" colspan="11">Belum ada data, 
                        tekan <span class="icon-plus"></span> untuk menambahkan</td>
                    </tr>
                `);
                return;
            }

            var newData = []
            data.forEach(function(item) {
                if (item.type == "Guest" || item.type == "Admin" || item.type == "Toko" || 
                    item.type == "Merchant" ||
                    item.type == "CS") {
                        newData.push(item)
                    }
            })

            data = newData;
            users = data;

            $("#lists-users tbody").html(`${data.map(function(item, position) {
                actionBanned = "";

                if (item.type == "Guest" || item.type == "Admin" || item.type == "Toko" || 
                    item.type == "Merchant" ||
                    item.type == "CS") 
                    actionBanned = "";
                else actionBanned = "disabled";
                return `
                    <tr>
                        <td>${index++}</td>
                        <td>${item.full_name}</td>
                        <td>${item.jk}</td>
                        <td>${item.alamat}</td>
                        <td>${item.tgl_lahir}</td>
                        <td>${(item.email == null) ? "-" : item.email}</td>
                        <td>${(item.phone_number == null) ? "-" : item.phone_number}</td>
                        <td>
                        <button 
                            type="button"
                            class="btn btn-primary btn-block dropdown-toggle ${actionBanned}" 
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                ${item.type == "Merchant" ? "Toko" : item.type}
                            </button>
                            <div class="dropdown-menu p-0">
                                <a class="dropdown-item ${(item.type == 'Guest') ? 'disabled': ''}" href="javascript:void(0);"
                                    onclick="changeUserType(${position}, 0)">Guest</a>
                                <a class="dropdown-item ${(item.type == 'Toko' || item.type == 'Merchant') ? 'disabled': ''}" href="javascript:void(0);"
                                    onclick="changeUserType(${position}, 1)">Toko</a>
                                <a class="dropdown-item ${(item.type == 'Admin') ? 'disabled': ''}" href="javascript:void(0);"
                                    onclick="changeUserType(${position}, 2)">Admin</a>
                                <a class="dropdown-item ${(item.type == 'Banned') ? 'disabled': ''}" href="javascript:void(0);"
                                    onclick="changeUserType(${position}, 3)">Banned</a>
                                <a class="dropdown-item ${(item.type == 'CS') ? 'disabled': ''}" href="javascript:void(0);"
                                    onclick="changeUserType(${position}, 4)">CS</a>
                            </div>
                        </td>
                        <td class="align-middle">
                            <h4>
                                <a href="javascript:void(0)" 
                                data-toggle="modal" 
                                data-target="#modal-detail-user" 
                                class="text-primary"
                                onclick="loadDetailUser(${Number(item.id_m_users)})"><i class="icon-eye"></i></a>
                            </h4>
                        </td>
                    </tr>
                `;
            }).join('')}`);
        }

        function doChangeUserType(type) {
            var typeStr = "Guest";
            if (type == 0) typeStr = "Guest";
            else if (type == 1) typeStr = "Toko";
            else if (type == 2) typeStr = "Admin";
            else if (type == 3) typeStr = "Banned";
            else if (type == 4) typeStr = "CS";

            req.alertWarning(
                "Ubah Jenis Pengguna",
                "Ubah " + selectedUser.full_name + " menjadi " + typeStr + "?",
                "Ubah",
                "Batal",
                function() {
                    var raw = req.raw({
                        type: type
                    })

                    var formData = new FormData()
                    formData.append("raw", raw)

                    $.ajax({
                        url: base_url.value + "/dashboard/a/managementUsers/change_type_user/" + selectedUser.id_u_user,
                        data: formData,
                        type: "POST",
                        contentType: false,
                        processData: false,
                        success: function(response) {
                            response = req.data(response)
                            if (response.code == 200) {
                                toastr.success("Berhasil mengubah jenis pengguna");
                                loadUsers();
                            } else {
                                toastr.error(response.message);
                            }
                        }
                    });
                },
                function() {}
            );
        }

        function checkRegisterAsMerchant(type) {
            $.ajax({
                url: base_url.value + "/dashboard/a/managementUsers/detail_user_request/" + selectedUser.id_u_user,
                data: null,
                type: "GET",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.code == 200) {
                        if(response.data != null) {
                            if (response.data.request_info != null && response.data.request_info.length != 0) {
                                doChangeUserType(type)
                                return;
                            }
                        } 
                    } 
                    toastr.error("Gagal, pengguna belum melakukan permintaan toko");
                }
            });
        }

        function changeUserType(position, type) {
            selectedUser = users[position];

            if (type == 1) checkRegisterAsMerchant(type)
            else doChangeUserType(type)
        }

        function loadDetailUser(id) {
            $.ajax({
                url: base_url.value + "/dashboard/a/managementUsers/load_detail_user/" + id,
                data: null,
                type: "GET",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.code == 200) {
                        var data = response.data
                        var type = Number(data.type)

                        switch(type) {
                            case 0: type = "Guest"; break;
                            case 1: type = "Toko"; break;
                            case 2: type = "Administrator"; break;
                            case 3: type = "Banned"; break;
                            default: type = "CS"; break;
                        }

                        $("#detail-user-face").attr("src", data.uri)
                        $("#detail-user-bio").html(`
                            <li class="list-group-item">Username: ${data.username}</li>
                            <li class="list-group-item">Alamat: ${data.alamat}</li>
                            <li class="list-group-item">Tgl Lahir: ${data.tgl_lahir}</li>
                        `);
                        $("#detail-user-contact").html(`
                            <li class="list-group-item">Email: ${(data.email != null) ? data.email : "-"}</li>
                            <li class="list-group-item">No HP: ${(data.phone_number != null) ? data.phone_number : "-"}</li>
                        `);
                        $("#detail-user-name").text(`${data.full_name}`)
                        $("#detail-user-type").text(`${type}`)
                    } else {
                        $("#modal-detail-user").modal("hide");
                    }
                }
            });
        }
    </script>
    <!-- END: APP JS-->
</body>

</html>