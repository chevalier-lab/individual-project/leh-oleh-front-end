<!-- START: Main Content-->
<main>
    <div class="container-fluid site-width">
        <!-- START: Breadcrumbs-->
        <div class="row ">
            <div class="col-12  align-self-center">
                <div class="sub-header mt-3 py-3 align-self-center d-sm-flex w-100 rounded">
                    <div class="w-sm-100 mr-auto">
                        <h4 class="mb-0">
                            <?php
                                if (isset($page)) echo $page;
                                else "Template Page";
                            ?>
                        </h4>
                    </div>

                    <ol class="breadcrumb bg-transparent align-self-center m-0 p-0">

                        <?php
                            if (isset($pageMap)) {
                                foreach ($pageMap as $item) {
                                    if ($item["is_current"]) {
                                        ?>
                                        <li class="breadcrumb-item active">
                                            <a href="javascript:void(0);"><?= $item["label"]; ?></a>
                                        </li>
                                        <?php
                                    } else {
                                        ?>
                                        <li class="breadcrumb-item"><?= $item["label"]; ?></li>
                                        <?php
                                    }
                                }
                            }
                        ?>
                        
                    </ol>
                </div>
            </div>
        </div>
        <!-- END: Breadcrumbs-->

        <!-- START: Card Data-->
        <?php
            if (isset($pageURI)) {
                $this->load->view($pageURI);
            }
        ?>
        <!-- END: Card DATA-->
    </div>
</main>
<!-- END: Content-->