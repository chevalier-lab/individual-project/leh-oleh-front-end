<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('components/head'); ?>

    <!-- START: Template CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.theme.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/simple-line-icons/css/simple-line-icons.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/flags-icon/css/flag-icon.min.css'); ?>">
    <!-- END Template CSS-->

    <!-- START: Page CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/social-button/bootstrap-social.css'); ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/sweetalert/sweetalert.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/toastr/toastr.min.css'); ?>"/>
    <!-- END: Page CSS-->

    <!-- START: Custom CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/css/main.css'); ?>">
    <!-- END: Custom CSS-->
</head>

<body id="main-container" class="default">

    <input type="hidden" name="base_url" 
        id="base_url" value="<?= base_url("index.php"); ?>">
    <input type="hidden" name="api_url" 
        id="api_url" value="<?= API_URI; ?>">

    <input type="hidden" name="page_id" 
        id="page_id" value="<?= isset($page_id) ? $page_id : ""; ?>">

    <!-- START: Pre Loader-->
    <div class="se-pre-con">
        <div class="loader"></div>
    </div>
    <!-- END: Pre Loader-->

    <!-- START: Template JS-->
    <script src="<?= base_url('assets/dist/vendors/jquery/jquery-3.3.1.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/moment/moment.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/slimscroll/jquery.slimscroll.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/toastr/toastr.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/sweetalert/sweetalert.min.js'); ?>"></script>
    <!-- END: Template JS-->

    <!-- START: Page Vendor JS-->
    <script src="<?= base_url('assets/dist/js/auth/request.js'); ?>"></script>
    <script>
        var base_url = document.getElementById("base_url");
        var api_url = document.getElementById("api_url");
        var page_id = document.getElementById("page_id");
    </script>
    <!-- END: Page Vendor JS-->

    <!-- START: Main Content-->
    <?php

    // Load Navigation Bar
    $this->load->view('components/menus/navbar');

    // Load Sidebar
    $this->load->view('components/menus/sidebar');

    // Load Page
    $this->load->view('dashboard/template', array(
        'page' => "Pratinjau Halaman",
        'pageMap' => array(
            array(
                "label" => "Halaman dan Pos",
                "is_current" => false
            ),
            array(
                "label" => "Pratinjau Halaman",
                "is_current" => true
            )
        ),
        'pageURI' => "dashboard/pages-and-posts/pages/preview-page",
    ));

    // Delete Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-delete-page",
        "modalTitle" => "Hapus Halaman",
        "modalType" => "modal-md",
        "iconTitle" => "icon-trash",
        "modalActionForm" => "javascript:void(0)",
        "modalContentForm" => '
        <p>Apakah Anda yakin untuk menghapus halaman <strong id="delete-page-name"></strong> ?</p>
        ',
        "modalButtonForm" => '<button type="button" class="btn btn-danger add-todo"
            id="delete-page">Hapus</button>'
    ));

    ?>
    <!-- END: Content-->

    <!-- START: Page Script JS-->
    <?php
    if (isset($error)) {
        $this->load->view("components/error-modal", array(
            "errorModalTitle" => $error["title"],
            "errorModalContent" => $error["content"],
            "errorModalDetail" => $error["details"]
        ));
    }

    ?>
    <!-- END: Page Script JS-->

    <!-- START: APP JS-->
    <script src="<?= base_url('assets/dist/js/app.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/js/home.script.js'); ?>"></script>

    <script>
        (function ($) {
            "use strict";
            $(window).on("load", function () {
                // Load Bank First Time
                loadPageDetail()

                // Handle Delete Page
                $("#delete-page").on("click", function() {
                    $.ajax({
                        url: base_url.value + "/dashboard/a/managementPages/delete_page/" + $("#delete-page").val(),
                        data: null,
                        type: "GET",
                        contentType: false,
                        processData: false,
                        success: function(response) {
                            response = req.data(response)
                            if (response.code == 200) {
                                toastr.success("Sukses menghapus halaman");
                                setTimeout(() => {
                                    location.assign(base_url.value + "/dashboard/a/managementPages/index")
                                }, 1000);
                            } else {
                                toastr.error(response.message);
                            }
                        }
                    });
                });
            })
        })(jQuery);

        function loadPageDetail() {
            renderToWaiting()

            if (page_id.value == "") {
                renderToPage(null)
                return
            }

            $.ajax({
                url: base_url.value + "/dashboard/a/managementPages/load_detail_page/" + page_id.value,
                data: null,
                type: "GET",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.code == 200) {
                        renderToPage(response.data)
                    } else {
                        renderToPage(null)
                    }
                }
            });
        }

        function renderToWaiting() {
            $("#preview-page-content").html(`
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">Harap menunggu, sedang memuat halaman..</div>
                    </div>
                </div>
            `);
        }

        function renderToPage(data) {
            if (data == null) {
                $("#preview-page-content").html(`
                    <div class="col-12">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <strong>Oops!</strong> Maaf halaman yang anda cari sudah dihapus, atau tidak ditemukan.
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    </div>
                `);
            } else {
                var page = data.page
                var tags = data.tags

                if (Number(page.is_visible) == 0) 
                    location.assign(base_url.value + "/dashboard/a/managementPages");

                $("#preview-page-content").html(`
                <div class="col-12 col-xl-9 mb-5 mb-xl-0">
                    <div class="card mb-4">
                        <img src="${page.uri}" alt="" class="img-fluid rounded-top">
                        <div class="card-body">
                            <a href="javascript:void(0)"><h4>${page.title}</h4></a>
                            ${page.content}
                            <hr>
                            <h4>Tags</h4>
                            ${tags.map(function(item) {
                                return `<a href="javascript:void(0)" 
                                class="redial-light border redial-border-light px-2 py-1 mb-2 d-inline-block redial-line-height-1_5 mr-2">
                                ${item.tag}
                                </a>`
                            }).join('')}
                        </div>
                    </div>
                </div>
                <div class="col-12 col-xl-3">
                    <div class="card mb-3">
                        <div class="card-body">
                            <a class="btn btn-warning btn-block"
                            href="${base_url.value + '/dashboard/a/ManagementPages/edit/' + page.id}">Edit <span class="icon-pencil"></span></a>
                            <button class="btn btn-danger btn-block"
                            data-toggle="modal" data-target="#modal-delete-page"
                            onclick="deletePage(${page.id}, '${page.title}')">Hapus <span class="icon-trash"></span></button>
                        </div>
                    </div>
                </div>
                `)
            }
        }

        function deletePage(idPage, item) {
            $("#delete-page").val(idPage)
            $("#delete-page-name").text(item)
        }
        
    </script>
    <!-- END: APP JS-->
</body>

</html>