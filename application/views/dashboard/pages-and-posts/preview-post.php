<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('components/head'); ?>

    <!-- START: Template CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.theme.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/simple-line-icons/css/simple-line-icons.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/flags-icon/css/flag-icon.min.css'); ?>">
    <!-- END Template CSS-->
    
    <!-- START: post CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/social-button/bootstrap-social.css'); ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/sweetalert/sweetalert.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/toastr/toastr.min.css'); ?>"/>
    <!-- END: post CSS-->

    <!-- START: Custom CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/css/main.css'); ?>">
    <!-- END: Custom CSS-->
</head>

<body id="main-container" class="default">

    <input type="hidden" name="base_url" 
        id="base_url" value="<?= base_url("index.php"); ?>">
    <input type="hidden" name="api_url" 
        id="api_url" value="<?= API_URI; ?>">

    <input type="hidden" name="post_id" 
        id="post_id" value="<?= isset($post_id) ? $post_id : ""; ?>">

    <!-- START: Pre Loader-->
    <div class="se-pre-con">
        <div class="loader"></div>
    </div>
    <!-- END: Pre Loader-->

    <!-- START: Template JS-->
    <script src="<?= base_url('assets/dist/vendors/jquery/jquery-3.3.1.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/moment/moment.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/slimscroll/jquery.slimscroll.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/toastr/toastr.min.js'); ?>"></script>
    <!-- END: Template JS-->

    <!-- START: post Vendor JS-->
    <script src="<?= base_url('assets/dist/js/auth/request.js'); ?>"></script>
    <script>
        var base_url = document.getElementById("base_url");
        var api_url = document.getElementById("api_url");
        var post_id = document.getElementById("post_id");
    </script>
    <!-- END: post Vendor JS-->

    <!-- START: Main Content-->
    <?php

    // Load Navigation Bar
    $this->load->view('components/menus/navbar');

    // Load Sidebar
    $this->load->view('components/menus/sidebar');

    // Load post
    $this->load->view('dashboard/template', array(
        'page' => "Preview Post",
        'pageMap' => array(
            array(
                "label" => "Pages And Posts",
                "is_current" => false
            ),
            array(
                "label" => "Preview Post",
                "is_current" => true
            )
        ),
        'pageURI' => "dashboard/pages-and-posts/pages/preview-post",
    ));

    // Delete Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-delete-post",
        "modalTitle" => "Delete Post",
        "modalType" => "modal-md",
        "iconTitle" => "icon-trash",
        "modalActionForm" => "#",
        "modalContentForm" => '
        <p>Are you sure to delete post <strong id="delete-post-name"></strong> ?</p>
        ',
        "modalButtonForm" => '<button type="button" class="btn btn-danger add-todo"
        id="delete-post">Delete</button>'
    ));

    ?>
    <!-- END: Content-->

    <!-- START: post Script JS-->
    <?php
    if (isset($error)) {
        $this->load->view("components/error-modal", array(
            "errorModalTitle" => $error["title"],
            "errorModalContent" => $error["content"],
            "errorModalDetail" => $error["details"]
        ));
    }

    ?>
    <!-- END: post Script JS-->

    <!-- START: APP JS-->
    <script src="<?= base_url('assets/dist/js/app.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/js/home.script.js'); ?>"></script>

    <script>
        (function ($) {
            "use strict";
            $(window).on("load", function () {
                // Load Bank First Time
                loadPostDetail()

                // Handle Delete Page
                $("#delete-post").on("click", function() {
                    $.ajax({
                        url: base_url.value + "/dashboard/a/ManagementPosts/delete_post/" + $("#delete-post").val(),
                        data: null,
                        type: "GET",
                        contentType: false,
                        processData: false,
                        success: function(response) {
                            response = req.data(response)
                            if (response.code == 200) {
                                toastr.success("Sukses menghapus pos");
                                setTimeout(() => {
                                    location.assign(base_url.value + "/dashboard/a/managementPosts");
                                }, 1000);
                            } else {
                                toastr.error(response.message);
                            }
                        }
                    });
                });
            })
        })(jQuery);

        function loadPostDetail() {
            renderToWaiting()

            if (post_id.value == "") {
                renderToPost(null)
                return
            }

            $.ajax({
                url: base_url.value + "/dashboard/a/managementPosts/load_detail_post/" + post_id.value,
                data: null,
                type: "GET",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.code == 200) {
                        renderToPost(response.data)
                    } else {
                        renderToPost(null)
                    }
                }
            });
        }

        function renderToWaiting() {
            $("#preview-post-content").html(`
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">Harap menunggu, sedang memuat halaman..</div>
                    </div>
                </div>
            `);
        }

        function renderToPost(data) {
            if (data == null) {
                $("#preview-post-content").html(`
                    <div class="col-12">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <strong>Oops!</strong> Maaf halaman yang anda cari sudah dihapus, atau tidak ditemukan.
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    </div>
                `);
            } else {
                var post = data
                var tags = data.tags

                if (post.status == "block") 
                    location.assign(base_url.value + "/dashboard/a/managementPosts");

                $("#preview-post-content").html(`
                <div class="col-12 col-xl-9 mb-5 mb-xl-0">
                    <div class="card mb-4">
                        <img src="${post.cover}" alt="" class="img-fluid rounded-top">
                        <div class="card-body">
                            <a href="javascript:void(0);"><h4>${post.title}</h4></a>
                            ${post.content}
                            <hr>
                            <h4>Tags</h4>
                            ${tags.map(function(item) {
                                return `<a href="javascript:void(0);" 
                                class="redial-light border redial-border-light px-2 py-1 mb-2 d-inline-block redial-line-height-1_5 mr-2">
                                ${item.tag}
                                </a>`
                            }).join('')}
                        </div>
                    </div>
                </div>
                <div class="col-12 col-xl-3">
                    <div class="card mb-3">
                        <div class="card-body">
                            <a 
                            href="${base_url.value + '/dashboard/a/ManagementPosts/edit/' + post.id}"
                            onclick="deletePost(${post.id}, '${post.title}')"
                            class="btn btn-warning btn-block">Ubah <span class="icon-pencil"></span></a>
                            <button class="btn btn-danger btn-block"
                            onclick="deletePost(${post.id}, '${post.title}')"
                            data-toggle="modal" data-target="#modal-delete-post">Hapus <span class="icon-trash"></span></button>
                        </div>
                    </div>
                </div>
                `)
            }
        }

        function deletePost(idPage, item) {
            $("#delete-post").val(idPage)
            $("#delete-post-name").text(item)
        }
        
    </script>
    <!-- END: APP JS-->
</body>

</html>