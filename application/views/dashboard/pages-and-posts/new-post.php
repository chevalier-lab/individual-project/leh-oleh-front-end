<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('components/head'); ?>

    <!-- START: Template CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.theme.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/simple-line-icons/css/simple-line-icons.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/flags-icon/css/flag-icon.min.css'); ?>">
    <!-- END Template CSS-->

    <!-- START: Page CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/social-button/bootstrap-social.css'); ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/sweetalert/sweetalert.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/toastr/toastr.min.css'); ?>"/>
    <!-- END: Page CSS-->

    <!-- START: Page CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/select2/css/select2.min.css'); ?>"/>
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/select2/css/select2-bootstrap.min.css'); ?>"/>
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/summernote/summernote-bs4.css'); ?>" />
    <!-- END: Page CSS-->

    <!-- START: Page CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/summernote/summernote-bs4.css'); ?>" />
    <!-- END: Page CSS-->

    <!-- START: Custom CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/css/main.css'); ?>">
    <!-- END: Custom CSS-->
</head>

<body id="main-container" class="default">

    <input type="hidden" name="base_url" 
        id="base_url" value="<?= base_url("index.php"); ?>">
    <input type="hidden" name="api_url" 
        id="api_url" value="<?= API_URI; ?>">

    <!-- START: Pre Loader-->
    <div class="se-pre-con">
        <div class="loader"></div>
    </div>
    <!-- END: Pre Loader-->

    <!-- START: Template JS-->
    <script src="<?= base_url('assets/dist/vendors/jquery/jquery-3.3.1.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/moment/moment.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/slimscroll/jquery.slimscroll.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/toastr/toastr.min.js'); ?>"></script>
    <!-- END: Template JS-->

    <!-- START: Page Vendor JS-->
    <script src="<?= base_url('assets/dist/js/auth/request.js'); ?>"></script>
    <!-- END: Page Script JS-->
    <script>
        var base_url = document.getElementById("base_url");
        var api_url = document.getElementById("api_url");
    </script>
    <!-- END: Page Vendor JS-->

    <!-- START: Main Content-->
    <?php

    // Load Navigation Bar
    $this->load->view('components/menus/navbar');

    // Load Sidebar
    $this->load->view('components/menus/sidebar');

    // Load Page
    $this->load->view('dashboard/template', array(
        'page' => "Pos Baru",
        'pageMap' => array(
            array(
                "label" => "Halaman dan Pos",
                "is_current" => false
            ),
            array(
                "label" => "Pos Baru",
                "is_current" => true
            )
        ),
        'pageURI' => "dashboard/pages-and-posts/pages/new-post",
    ));

    // Create Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-create-tag",
        "modalTitle" => "Membuat Label",
        "modalType" => "modal-lg",
        "iconTitle" => "icon-plus",
        "modalActionForm" => "#",
        "modalContentForm" => '
        
        <ul class="nav nav-pills mb-3" role="tablist">
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#new-post-tag-create" role="tab" 
                    aria-controls="new-post-tag-create" aria-selected="false">Label Baru</a>
            </li>
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#new-post-tag-select" role="tab" 
                    aria-controls="new-post-tag-select" aria-selected="true">Pilih Label</a>
            </li>
        </ul>

        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade" id="new-post-tag-create" role="tabpanel" aria-labelledby="tag-create-tab">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group mb-3">
                            <label for="create-tag">Buat Label</label>
                            <input type="text" id="create-tag" 
                                name="create-tag" 
                                class="form-control" placeholder="Contoh: Fakta Unik"/> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade show active" id="new-post-tag-select" role="tabpanel" aria-labelledby="tag-select-tab">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group mb-3">
                            <label for="select-tag">Pilih Tag</label>
                            <input type="text" id="select-tag" 
                                name="select-tag" class="form-control"
                                placeholder="Ketik untuk mencari label" />
                        </div>

                        <div class="from-group mb-3" id="selected-tag">
                            Belum ada label yang dipilih
                        </div>

                        <div class="list-group" id="list-select-tag"></div>
                    </div>
                </div>
            </div>
        </div>
        ',
        "modalButtonForm" => '<button type="button" class="btn btn-primary add-todo"
            id="add-tag">Tambah Label</button>'
    ));

    ?>
    <!-- END: Content-->

    <!-- START: Page Script JS-->
    <?php
    if (isset($error)) {
        $this->load->view("components/error-modal", array(
            "errorModalTitle" => $error["title"],
            "errorModalContent" => $error["content"],
            "errorModalDetail" => $error["details"]
        ));
    }

    ?>
    <!-- END: Page Script JS-->

    <!-- START: APP JS-->
    <script src="<?= base_url('assets/dist/js/app.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/js/home.script.js'); ?>"></script>
    <!-- END: APP JS-->

    <!-- START: Page Vendor JS-->
    <script src="<?= base_url('assets/dist/vendors/summernote/summernote-bs4.js'); ?>"></script> 
    <script src="<?= base_url('assets/dist/js/summernote.script.js'); ?>"></script>

    <script>
    var selectedTag = [];
    var listSelectedTag = [];

    (function ($) {
        "use strict";
        $(window).on("load", function () {
            // Load Tags
            loadTags()

            $("#select-tag").on("keydown", function() {
                loadTags()
            });

            $("#add-tag").on("click", function() {
                $("#add-tag").attr("disabled", true);

                var newTag = $("#create-tag").val()
                if (newTag != "") {
                    createTag(newTag)
                    return
                }

                addTags()
                $("#modal-create-tag").modal("hide")
            });

            $("#selected-cover").on("change", function(event) {
                previewCover(event)
            });

            $("#create-post").on("click", function() {

                $("#create-post").attr("disabled", true)

                var tags = []
                listSelectedTag.forEach(function(item) {
                    tags.push(item.name)
                });
                var title = $("#title-create-post").val();
                var content = $('#content-create-post').summernote('code');
                var cover = $("#selected-cover")[0].files

                if (cover.length == 0) {
                    toastr.error("Harap pilih sampul terlebih dahulu");
                    $("#create-post").removeAttr("disabled")
                    return
                } else if (title == "") {
                    toastr.error("Harap isi judul pos terlebih dahulu");
                    $("#create-post").removeAttr("disabled")
                    return
                } else if (content == "" || content == "<br>") {
                    toastr.error("Harap isi konten pos terlebih dahulu");
                    $("#create-post").removeAttr("disabled")
                    return
                }
                else if (tags.length == 0) {
                    toastr.error("Harap pilih / tambahkan label terlebih dahulu");
                    $("#create-post").removeAttr("disabled")
                    return
                }

                var formData = new FormData()
                formData.append("title", title)
                formData.append("slug", title.toLowerCase().replace(" ", "-"))
                formData.append("content", content)
                formData.append("tags", JSON.stringify(tags))
                formData.append("cover", cover[0])

                $.ajax({
                    url: base_url.value + "/dashboard/a/managementPosts/create_post",
                    data: formData,
                    type: "POST",
                    contentType: false,
                    processData: false,
                    success: function(response) {
                        response = req.data(response)

                        if (response.code == 200) {

                            toastr.success("Sukses menambah pos");
                            setTimeout(() => {
                                location.assign(base_url.value + "/dashboard/a/managementPosts")
                            }, 1000);

                        } else {
                            $("#create-post").removeAttr("disabled")
                            toastr.error(response.message);
                        }
                    }
                });
            })
        });
    })(jQuery);

    // Handle Cover Start
    function previewCover(event) {
        var reader = new FileReader();
        reader.onload = function()
        {
            var output = document.getElementById('widget-selected-cover');
            output.src = reader.result;
        }
        reader.readAsDataURL(event.target.files[0]);
        $("#widget-selected-cover").addClass("mb-3")
    }
    // Handle Cover Stop

    // Handle Tags Start
    function createTag(newTag) {

        var raw = req.raw({
            tag: newTag,
            slug: newTag.toLowerCase().replace(" ", "-")
        })

        var formData = new FormData()
        formData.append("raw", raw)

        $.ajax({
            url: base_url.value + "/dashboard/a/masterData/create_tags",
            data: formData,
            type: "POST",
            contentType: false,
            processData: false,
            success: function(response) {
                response = req.data(response)

                if (response.code == 200) {

                    $("#modal-create-tag").modal("hide")

                    selectedTag.push({
                        id: response.data.id,
                        name: response.data.tag
                    });

                    addTags()

                } else {
                    toastr.error(response.message);
                }
            }
        });
    }

    function addTags() {
        selectedTag.forEach(function(item) {
            isFind = listSelectedTag.map(function(e) 
                { return e.id; }).indexOf(item.id);

            if (isFind == -1) listSelectedTag.push(item)
        })

        $("#widget-selected-tag").html(`${listSelectedTag.map(function(item, index) {
            return `<a href="javascript:void(0);" 
            onclick="removeTag(${index})"
            class="badge badge-primary">${item.name}</a>`;
        }).join('')}`)

        selectedTag = []
        $("#create-tag").val('')
        $("#selected-tag").html('')
        $("#add-tag").removeAttr("disabled");
    }

    function removeTag(position) {
        if (position > -1) {
            listSelectedTag.splice(position, 1);

            addTags();
        }
    }

    function loadTags() {
        var raw = req.raw({
            page: 0,
            search: $("#select-tag").val(),
            order_by: "tag",
            order_direction: "DESC"
        })

        var formData = new FormData()
        formData.append("raw", raw)

        $.ajax({
            url: base_url.value + "/dashboard/a/masterData/load_tags",
            data: formData,
            type: "POST",
            contentType: false,
            processData: false,
            success: function(response) {
                response = req.data(response)
                if (response.code == 200) {
                    renderToList(response.data)
                } else {
                    renderToList([])
                }
            }
        });
    }

    function renderToList(data=[]) {
        $("#list-select-tag").html(`${data.map(function(item) {
            return `<a href="javascript:void(0);" 
            onclick="selectIDTag(${item.id}, '${item.tag}')"
            class="list-group-item list-group-item-action">${item.tag}</a>`
        }).join('')}`);
    }

    function selectIDTag(idTag, itemTag) {
        var index = selectedTag.map(function(x) {return x.id; }).indexOf(idTag);
        selectedTag.push({
            id: idTag,
            name: itemTag
        });

        renderSelectedTag();
    }

    function removeIDTag(position) {
        if (position > -1) {
            selectedTag.splice(position, 1);

            renderSelectedTag();
        }
    }

    function renderSelectedTag() {
        $("#selected-tag").html(`${selectedTag.map(function(item, index) {
            return `<a href="javascript:void(0);" 
            onclick="removeIDTag(${index})"
            class="badge badge-primary">${item.name}</a>`;
        }).join('')}`)
    }
    // Handle Tags Stop
    </script>
    <!-- END: Page Script JS-->
</body>

</html>