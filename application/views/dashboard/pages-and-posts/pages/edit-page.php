<!-- START: Card Data-->
<div class="row mt-3">
    <div class="col-12 col-sm-12">

        <div class="row" id="preview-page-content"></div>
        <div class="row" id="page-content">
            <div class="col-12 col-xl-8 mb-5 mb-xl-0">

                <div class="card mb-4">
                    <div class="card-body pb-0">
                        <div class="row">
                            <div class="form-group col-12">
                                <input type="text" 
                                    name="title-edit-page" id="title-edit-page" 
                                    placeholder="Your title here"
                                    class="form-control">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card mb-4">
                    <div class="card-body">
                        <textarea name="content-edit-page"
                        id="content-edit-page" class="summernote"><p>Coming Soon!</p></textarea>
                    </div>
                </div>

            </div>
            <div class="col-12 col-xl-4">

                <div class="card mb-3">
                    <div class="card-body">
                        <button 
                            type="button"
                            id="edit-page"
                            class="btn btn-primary btn-block dropdown-toggle" 
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Save <span class="icon-paper-plane"></span>
                            </button>
                            <div class="dropdown-menu p-0">
                                <a class="dropdown-item" href="javascript:void(0);"
                                    onclick="editPage(1)">Aktif</a>
                                <a class="dropdown-item" href="javascript:void(0);"
                                    onclick="editPage(2)">Tertunda</a>
                                <a class="dropdown-item" href="javascript:void(0);"
                                    onclick="editPage(0)">Tidak Aktif</a>
                            </div>
                    </div>
                </div>

                <div class="card mb-3">
                    <div class="card-header d-flex justify-content-between align-items-center">                               
                        <h4 class="card-title">
                            <i class="icon-tag pr-2"></i> Label
                        </h4>                                
                        <a href="javascript:void(0);" 
                            data-toggle="modal" data-target="#modal-create-tag"
                            class="bg-primary rounded text-white text-center">
                            <div class="p-2">
                                <i class="icon-plus align-middle text-white"></i>
                                <span>Tambah Label</span>
                            </div>
                        </a>
                    </div>
                    <div class="card-body" id="widget-selected-tag"></div>
                </div>

                <div class="card mb-3">
                    <div class="card-header d-flex justify-content-between align-items-center">                               
                        <h4 class="card-title">
                            <i class="icon-picture pr-2"></i> Sampul
                        </h4>    
                    </div>
                    <div class="card-body">
                        <div>
                            <img id="widget-selected-cover" src="" class="img-fluid">
                        </div>
                        <label for="selected-cover" 
                            class="file-upload btn btn-primary btn-block" id="btn-selected-cover">
                                <i class="fa fa-upload mr-2"></i>Pilih Sampul ...
                                <input id="selected-cover" type="file">
                        </label>
                    </div>
                </div>

            </div>
        </div>

    </div>  
</div>