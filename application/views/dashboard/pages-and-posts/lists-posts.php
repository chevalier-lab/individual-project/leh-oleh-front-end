<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('components/head'); ?>

    <!-- START: Template CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.theme.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/simple-line-icons/css/simple-line-icons.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/flags-icon/css/flag-icon.min.css'); ?>">
    <!-- END Template CSS-->

    <!-- START: Page CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/social-button/bootstrap-social.css'); ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/sweetalert/sweetalert.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/toastr/toastr.min.css'); ?>"/>
    <!-- END: Page CSS-->

    <!-- START: Custom CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/css/main.css'); ?>">
    <!-- END: Custom CSS-->
</head>

<body id="main-container" class="default">

    <input type="hidden" name="base_url" 
        id="base_url" value="<?= base_url("index.php"); ?>">
    <input type="hidden" name="api_url" 
        id="api_url" value="<?= API_URI; ?>">

    <!-- START: Pre Loader-->
    <div class="se-pre-con">
        <div class="loader"></div>
    </div>
    <!-- END: Pre Loader-->

    <!-- START: Template JS-->
    <script src="<?= base_url('assets/dist/vendors/jquery/jquery-3.3.1.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/moment/moment.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/slimscroll/jquery.slimscroll.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/toastr/toastr.min.js'); ?>"></script>
    <!-- END: Template JS-->

    <!-- START: Page Vendor JS-->
    <script src="<?= base_url('assets/dist/js/auth/request.js'); ?>"></script>
    <script>
        var base_url = document.getElementById("base_url");
        var api_url = document.getElementById("api_url");
    </script>
    <!-- END: Page Vendor JS-->

    <!-- START: Main Content-->
    <?php

    // Load Navigation Bar
    $this->load->view('components/menus/navbar');

    // Load Sidebar
    $this->load->view('components/menus/sidebar');

    // Load Page
    $this->load->view('dashboard/template', array(
        'page' => "Daftar Pos",
        'pageMap' => array(
            array(
                "label" => "Halaman dan Pos",
                "is_current" => false
            ),
            array(
                "label" => "Daftar Pos",
                "is_current" => true
            )
        ),
        'pageURI' => "dashboard/pages-and-posts/pages/lists-posts",
    ));

    // Delete Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-delete-post",
        "modalTitle" => "Hapus Pos",
        "modalType" => "modal-md",
        "iconTitle" => "icon-trash",
        "modalActionForm" => "#",
        "modalContentForm" => '
        <p>Apakah Anda yakin akan menghapus pos <strong id="delete-post-name"></strong> ?</p>
        ',
        "modalButtonForm" => '<button type="button" class="btn btn-danger add-todo"
        id="delete-post">Delete</button>'
    ));

    ?>
    <!-- END: Content-->

    <!-- START: Page Script JS-->
    <?php
    if (isset($error)) {
        $this->load->view("components/error-modal", array(
            "errorModalTitle" => $error["title"],
            "errorModalContent" => $error["content"],
            "errorModalDetail" => $error["details"]
        ));
    }

    ?>
    <!-- END: Page Script JS-->

    <!-- START: APP JS-->
    <script src="<?= base_url('assets/dist/js/app.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/js/home.script.js'); ?>"></script>

    <script>
        // Initial Pagination
        var page = 0;
        var paginationClicked = false;
        var paginationDirection = "";

        (function ($) {
            "use strict";
            $(window).on("load", function () {
                // Load Bank First Time
                loadPosts()

                // Handle Order By
                $("#lists-posts-order").on("change", function() {
                    loadPosts();
                });

                // Handle Search
                $("#lists-posts-search").on("keydown", function(event) {
                    if (event.keyCode == 32 || event.which == 32) loadPosts();
                    else if ($("#lists-posts-search").val() == "") loadPosts();
                });

                $("#lists-posts-button-search").on("click", function(event) {
                    loadPosts();
                });

                // Handle Pagination
                $("#lists-posts-prev").on("click", function() {
                    if (page > 0)
                        page -= 1;
                    paginationClicked = true;
                    paginationDirection = "prev";
                    loadPosts();
                });

                $("#lists-posts-next").on("click", function() {
                    page += 1;
                    paginationClicked = true;
                    paginationDirection = "next";
                    loadPosts();
                });

                // Handle Delete Page
                $("#delete-post").on("click", function() {
                    $.ajax({
                        url: base_url.value + "/dashboard/a/ManagementPosts/delete_post/" + $("#delete-post").val(),
                        data: null,
                        type: "GET",
                        contentType: false,
                        processData: false,
                        success: function(response) {
                            response = req.data(response)
                            if (response.code == 200) {
                                loadPosts()
                                toastr.success("Sukses menghapus pos");
                            } else {
                                toastr.error(response.message);
                            }

                            $("#modal-delete-post").modal("hide");
                        }
                    });
                });
            });
        })(jQuery);

        function loadPosts() {
            renderToWaiting()

            var order = $("#lists-posts-order").val();
            var order_direction = "DESC";
            switch (Number(order)) {
                case 1: 
                    order =  "m_posts.title";
                    order_direction = "ASC";
                break;
                case 2: 
                    order =  "m_posts.title";
                    order_direction = "DESC";
                break;
                default:
                    order =  "m_posts.id";
                    order_direction = "DESC";
                break;
            }
            var raw = req.raw({
                page: page,
                search: $("#lists-posts-search").val(),
                order_by: order,
                order_direction: order_direction
            })

            var formData = new FormData()
            formData.append("raw", raw)

            $.ajax({
                url: base_url.value + "/dashboard/a/managementPosts/load_posts",
                data: formData,
                type: "POST",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.code == 200) {
                        renderToTable(response.data)
                    } else {
                        renderToTable([])
                    }
                }
            });
        }

        function renderToWaiting() {
            $("#lists-posts tbody").html(`
                <tr>
                    <td class="align-middle" colspan="11">Sedang memuat data</td>
                </tr>
            `);
        }

        function renderToTable(data=[]) {
            
            if (paginationClicked) {
                paginationClicked = false;
                if (data.length == 0) {
                    if (paginationDirection == "next") 
                        page -= 1;
                    else page += 1;
                }
                paginationDirection = "";
                loadPosts();
                return 
            }

            var index = (page * 10) + 1;

            if (data.length == 0) {
                $("#lists-posts tbody").html(`
                    <tr>
                        <td class="align-middle" colspan="11">Belum ada data, 
                        tekan <span class="icon-plus"></span> untuk menambahkan</td>
                    </tr>
                `);
                return;
            }

            $("#lists-posts tbody").html(`${data.map(function(item) {
                status = item.status
                isBlock = "";

                if (status == "block") {
                    status = "<span class='badge badge-danger'>Tidak Aktif</span>";
                    isBlock = "disabled";
                }
                else if (status == "visible") {
                    status = "<span class='badge badge-primary'>Aktif</span>";
                }
                else {
                    status = "<span class='badge badge-warning'>Tertunda</span>";
                }

                return `
                    <tr>
                        <td class="align-middle">${index++}</td>
                        <td class="align-middle">${item.title}</td>
                        <td class="align-middle">${item.slug}</td>
                        <td class="align-middle"><a href="${base_url.value + '/dashboard/a/ManagementPosts/preview/' + item.id}"
                            class="btn ${isBlock}">Lihat halaman</a></td>
                        <td class="align-middle">${item.updated_at}</td>
                        <td class="align-middle">${status}</td>
                        <td class="align-middle">
                            <h4>
                                <a href="${base_url.value + '/dashboard/a/ManagementPosts/preview/' + item.id}" class="btn text-primary ${isBlock}"><i class="icon-eye"></i></a>
                            </h4>
                        </td>
                        <td class="align-middle">
                            <h4>
                                <a href="${base_url.value + '/dashboard/a/ManagementPosts/edit/' + item.id}" class="btn text-success"><i class="icon-pencil"></i></a>
                            </h4>
                        </td>
                        <td class="align-middle">
                            <h4>
                                <a href="javascript:void(0);" 
                                onclick="deletePost(${item.id}, '${item.title}')"
                                data-toggle="modal" data-target="#modal-delete-post" class="btn text-danger ${isBlock}"><i class="icon-trash"></i></a>
                            </h4>
                        </td>
                    </tr>
                `;
            }).join('')}`);
        }

        function deletePost(idPage, item) {
            $("#delete-post").val(idPage)
            $("#delete-post-name").text(item)
        }
    </script>
    <!-- END: APP JS-->
</body>

</html>