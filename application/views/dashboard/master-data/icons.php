<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('components/head'); ?>

    <!-- START: Template CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.theme.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/simple-line-icons/css/simple-line-icons.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/flags-icon/css/flag-icon.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/select2/css/select2.min.css') ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/select2/css/select2-bootstrap.min.css') ?>" />
    <!-- END Template CSS-->

    <!-- START: Page CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/social-button/bootstrap-social.css'); ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/sweetalert/sweetalert.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/toastr/toastr.min.css'); ?>"/>
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/fontawesome/css/all.min.css') ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/toastr/toastr.min.css'); ?>"/>
    <!-- END: Page CSS-->

    <!-- START: Custom CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/css/main.css'); ?>">
    <!-- END: Custom CSS-->
</head>

<body id="main-container" class="default">

    <input type="hidden" name="base_url" id="base_url" value="<?= base_url("index.php"); ?>">
    <input type="hidden" name="api_url" id="api_url" value="<?= API_URI; ?>">

    <!-- START: Pre Loader-->
    <div class="se-pre-con">
        <div class="loader"></div>
    </div>
    <!-- END: Pre Loader-->

    <!-- START: Template JS-->
    <script src="<?= base_url('assets/dist/vendors/jquery/jquery-3.3.1.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/moment/moment.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/slimscroll/jquery.slimscroll.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/toastr/toastr.min.js'); ?>"></script>
    <!-- END: Template JS-->

    <!-- START: Page Vendor JS-->
    <script src="<?= base_url('assets/dist/js/auth/request.js'); ?>"></script>
    <script>
        var base_url = document.getElementById("base_url");
        var api_url = document.getElementById("api_url");
    </script>
    <!-- END: Page Vendor JS-->

    <!-- START: Main Content-->
    <?php

    // Load Navigation Bar
    $this->load->view('components/menus/navbar');

    // Load Sidebar
    $this->load->view('components/menus/sidebar');

    // Load Page
    $this->load->view('dashboard/template', array(
        'page' => "Ikon",
        'pageMap' => array(
            array(
                "label" => "Data Master",
                "is_current" => false
            ),
            array(
                "label" => "Ikon",
                "is_current" => true
            ),
        ),
        'pageURI' => "dashboard/master-data/pages/icons",
    ));

    ?>
    <!-- END: Content-->

    <!-- START: Page Script JS-->
    <?php
    if (isset($error)) {
        $this->load->view("components/error-modal", array(
            "errorModalTitle" => $error["title"],
            "errorModalContent" => $error["content"],
            "errorModalDetail" => $error["details"]
        ));
    }

    // Create Icon Container
    $iconContainer = '';
    foreach ($icons as $icon) {
        $iconContainer .=
            '<div class="add-icon col font-icon-list p-2 border mx-1 mb-2 btn btn-outline-dark"><div class="preview">' .
            '<i class="' . $icon . ' icons"></i> ' . $icon .
            '</div></div>';
    }

    // Create Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-create-icon",
        "modalTitle" => "Tambah Ikon",
        "iconTitle" => "icon-plus",
        "modalActionForm" => "#",
        "modalContentForm" => '<div class="icon-container card-body text-center row" style="width: 100%; height:300px; overflow: scroll;overflow-x: hidden;">' .
        $iconContainer .
        '</div>',
        "modalButtonForm" => '<button type="button" class="btn btn-primary add-todo" id="create-icon-button">Tambah</button>'
    ));

    // Edit Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-edit-icon",
        "modalTitle" => "Mengubah Ikon",
        "iconTitle" => "icon-pencil",
        "modalActionForm" => "#",
        "modalContentForm" => '<div class="icon-container card-body text-center row" style="width: 100%; height:300px; overflow: scroll;overflow-x: hidden;">
        <div class="col font-icon-list p-2 border mx-1 mb-2 btn btn-danger disabled">
            <div class="preview">
            <i id="preview-edit" class="icons"></i>
            </div>
        </div>' .
        $iconContainer .
        '</div>',
        "modalButtonForm" => '
        <input type="hidden" id="id-icon"/>
        <input type="hidden" id="old-icon"/>
        <div class="btn-group btn-block">
            <button type="button"
            id="edit-icon"
            class="btn btn-success btn-block dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Simpan <span class="icon-paper-plane"></span>
            </button>
            <div class="dropdown-menu p-0" style="">
            <a class="dropdown-item" href="javascript:void(0);"
                onclick="editIcon(1)">Aktif</a>
            <a class="dropdown-item" href="javascript:void(0);"
                onclick="editIcon(2)">Tertunda</a>
            <a class="dropdown-item" href="javascript:void(0);"
                onclick="editIcon(0)">Tidak Aktif</a>
            </div>
        </div>'
    ));

    // Delete Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-delete-icon",
        "modalTitle" => "Hapus Ikon",
        "iconTitle" => "icon-trash",
        "modalActionForm" => "#",
        "modalContentForm" => '
        <p>Apakah Anda yakin untuk menghapus ikon <strong id="delete-icon-name"></strong> ?</p>
        ',
        "modalButtonForm" => '<button type="submit" class="btn btn-danger add-todo" id="delete-icon">Hapus</button>'
    ));


    ?>
    <!-- END: Page Script JS-->

    <!-- START: APP JS-->
    <script src="<?= base_url('assets/dist/js/app.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/js/home.script.js'); ?>"></script>

    <script>
        var selected = 0;
        $('.icon-container').on('click', '.add-icon', function() {
            if (selected == 0) {
                selected = 1
                $(this).removeClass("btn-outline-dark").addClass("btn-success disabled")
                $(this).attr('id', 'selected');
            } else {
                var element = document.getElementById('selected')
                if(element){
                    element.removeAttribute('id')
                    element.classList.remove('btn-success')
                    element.classList.remove('disabled')
                    element.classList.add('btn-outline-dark')
                }
                $(this).removeClass("btn-outline-dark").addClass("btn-success disabled")
                $(this).attr('id', 'selected');
            }
        })

        // Initial Pagination
        var page = 0;
        var paginationClicked = false;
        var paginationDirection = "";

        (function($) {
            "use strict";
            $(window).on("load", function() {
                // Load Icons First Time
                loadIcons()
                renderCreateIcon()

                // Handle Order By
                $("#master-icons-order").on("change", function() {
                    loadIcons();
                });

                // Handle Search
                $("#master-icons-search").on("keydown", function(event) {
                    if (event.keyCode == 32 || event.which == 32) loadIcons();
                    else if ($("#master-icons-search").val() == "") loadIcons();
                });

                $("#master-icons-button-search").on("click", function(event) {
                    loadIcons();
                });

                // Handle Pagination
                $("#master-icon-prev").on("click", function() {
                    if (page > 0)
                        page -= 1;
                    paginationClicked = true;
                    paginationDirection = "prev";
                    loadIcons();
                });

                $("#master-icon-next").on("click", function() {
                    page += 1;
                    paginationClicked = true;
                    paginationDirection = "next";
                    loadIcons();
                });

                // Create Icons
                $("#create-icon-button").on("click", function() {
                    if(!document.getElementById('selected')){
                        toastr.warning("Silahkan pilih ikon terlebih dahulu")
                    }else {
                        $("#create-icon-button").attr("disabled", true);

                        var raw = req.raw({
                            icon: document.getElementById('selected').children[0].children[0].classList[0],
                        })
                        var formData = new FormData();
                        formData.append("raw", raw)

                        $.ajax({
                            url: base_url.value + "/dashboard/a/masterData/create_icons",
                            data: formData,
                            type: "POST",
                            contentType: false,
                            processData: false,
                            success: function(response) {
                                response = req.data(response)

                                // $("#create-icon-button").removeAttr("disabled");
                                // $("#modal-create-icon").modal('hide')

                                if (response.code == 200) {
                                    toastr.success("Sukses menambah ikon");
                                    // Refresh Table
                                    // loadIcons()
                                    setTimeout(function(){
                                        location.reload()
                                    },1000)
                                } else {
                                    toastr.error(response.message);
                                }
                            }
                        });
                    }
                });

                // Handle Delete Icon
                $("#delete-icon").on("click", function() {
                    $.ajax({
                        url: base_url.value + "/dashboard/a/masterData/delete_icons/" + $("#delete-icon").val(),
                        data: null,
                        type: "GET",
                        contentType: false,
                        processData: false,
                        success: function(response) {
                            response = req.data(response)
                            if (response.code == 200) {
                                loadPages()
                                toastr.success("Sukses menghapus ikon");
                            } else {
                                toastr.error(response.message);
                            }
                        }
                    });
                });
            });
        })(jQuery);

        function loadIcons() {
            renderToWaiting()

            var order = $("#master-icons-order").val();
            var order_direction = "DESC";
            switch (Number(order)) {
                case 1:
                    order = "icon";
                    order_direction = "ASC";
                    break;
                case 2:
                    order = "icon";
                    order_direction = "DESC";
                    break;
                default:
                    order = "id";
                    order_direction = "DESC";
                    break;
            }
            var raw = req.raw({
                page: page,
                search: $("#master-icons-search").val(),
                order_by: order,
                order_direction: order_direction
            })

            var formData = new FormData()
            formData.append("raw", raw)

            $.ajax({
                url: base_url.value + "/dashboard/a/masterData/load_icons",
                data: formData,
                type: "POST",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.code == 200) {
                        renderToTable(response.data)
                    } else {
                        renderToTable([])
                    }
                }
            });
        }

        function renderToWaiting() {
            $("#master-icons tbody").html(`
                <tr>
                    <td class="align-middle" colspan="7">Sedang memuat data</td>
                </tr>
            `);
        }

        function renderToTable(data = []) {

            if (paginationClicked) {
                paginationClicked = false;
                if (data.length == 0) {
                    if (paginationDirection == "next")
                        page -= 1;
                    else page += 1;
                }
                paginationDirection = "";
                loadIcons();
                return
            }

            var index = (page * 10) + 1;

            if (data.length == 0) {
                $("#master-icons tbody").html(`
                    <tr>
                        <td class="align-middle" colspan="4">Belum ada data ikon, 
                        tekan <span class="icon-plus"></span> untuk menambahkan</td>
                    </tr>
                `);
                return;
            }

            $("#master-icons tbody").html(`${data.map(function(item) {
                status = item.is_visible
                isBlock = "";

                if (status == "0") {
                    status = "<span class='badge badge-danger'>Tidak Aktif</span>";
                    isBlock = "disabled";
                }
                else if (status == "1") {
                    status = "<span class='badge badge-primary'>Aktif</span>";
                }
                else {
                    status = "<span class='badge badge-warning'>Tertunda</span>";
                }
                return `
                    <tr>
                        <td class="align-middle">${index++}</td>
                        <td class="align-middle h4"><i class="${item.icon}"></td>
                        <td class="align-middle">${item.icon}</td>
                        <td class="align-middle">${item.updated_at}</td>
                        <td class="align-middle">${status}</td>
                        <td class="align-middle">
                            <h4>
                                <a id="refresh-temp" href="javascript:void(0);" data-toggle="modal" data-target="#modal-edit-icon" class="btn text-success edit-icon-button" onclick="loadOneIcon(${item.id}, '${item.icon}')"><i class="icon-pencil"></i></a>
                            </h4>
                        </td>
                        <td class="align-middle">
                            <h4>
                                <a href="javascript:void(0);" data-toggle="modal" data-target="#modal-delete-icon" onclick="deleteIcon(${item.id}, '${item.icon}')" class="btn text-danger ${isBlock}"><i class="icon-trash"></i></a>
                            </h4>
                        </td>
                    </tr>
                `;
            }).join('')}`);
        }

        function renderCreateIcon(){
            $.ajax({
                url: base_url.value + "/dashboard/a/masterData/load_icons",
                data: null,
                type: "GET",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.code == 200) {
                        response.data.map(function(item) {
                            // Delete icon already exists
                            var childrenModal = document.getElementById('modal-create-icon').children[0].children[0].children[1].children[0].children[0]
                            for(i = 0; i < childrenModal.children.length; i++){
                                var iconElement = childrenModal.children[i].children[0].children[0]
                                if (iconElement.classList[0] == item.icon) {
                                    // childrenModal.children[i].remove()
                                    childrenModal.children[i].classList.remove("add-icon");
                                    childrenModal.children[i].classList.remove("btn-outline-dark");
                                    childrenModal.children[i].classList.add("btn-danger");
                                    childrenModal.children[i].classList.add("disabled");
                                }
                            }

                            // Delete icon already exists
                            var childrenModal = document.getElementById('modal-edit-icon').children[0].children[0].children[1].children[0].children[0]
                            for(i = 0; i < childrenModal.children.length; i++){
                                var iconElement = childrenModal.children[i].children[0].children[0]
                                if (iconElement.classList[0] == item.icon) {
                                    childrenModal.children[i].remove()
                                }
                            }
                        })
                        $("#button-icon-modal").removeAttr("disabled");
                    }else{
                        toastr.error(response.message)
                    }
                }
            });
        }

        function deleteIcon(idIcon, item) {
            $("#delete-icon").val(idIcon)
            $("#delete-icon-name").text(item)
        }

        function loadOneIcon(idIcon, icon) {
            $("#id-icon").val(idIcon)
            $("#old-icon").val(icon)
            $("#preview-edit").replaceWith(`<div id="preview-edit"><i class="${icon} icons"></i>${icon}</div>`)
            // $('.icon-container').prepend(`
                // <div class="temp col font-icon-list p-2 border mx-1 mb-2 btn btn-danger disabled">
                //     <div class="preview">
                //     <i class="${icon} icons"></i> ${icon}
                //     </div>
                // </div>
            // `)
            
        }

        function editIcon(is_visible){
            $("#edit-icon").attr("disabled", true);

            var element = document.getElementById('selected')

            if(element){
                var raw = req.raw({
                    icon: element.children[0].children[0].classList[0],
                    is_visible: is_visible == 0 ? "0" : is_visible
                })
            }else {
                var raw = req.raw({
                    icon: $('#old-icon').val(),
                    is_visible: is_visible == 0 ? "0" : is_visible
                })
            }

            // Update Category
            var formData = new FormData();
            formData.append("raw", raw)

            $.ajax({
                url: base_url.value + "/dashboard/a/masterData/edit_icon/" + $('#id-icon').val(),
                data: formData,
                type: "POST",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)

                    // $("#edit-icon").removeAttr("disabled");
                    // $("#modal-edit-icon").modal('hide')

                    if (response.code == 200) {
                        toastr.success("Sukses mengubah ikon");
                        // Refresh Table
                        // loadIcons()
                        setTimeout(function(){
                            location.reload()
                        },1000)
                    } else {
                        toastr.error(response.message);
                    }
                }
            });
        }
    </script>
    <!-- END: APP JS-->

    <!-- START: Page Script JS-->
    <script src="<?= base_url('assets/dist/vendors/select2/js/select2.full.min.js') ?>"></script>
    <script>
        // Select2
        $(".select2").select2({
            theme: 'bootstrap4',
            width: 'style',
            minimumResultsForSearch: Infinity
        });
    </script>
    <!-- END: Page Script JS-->
</body>

</html>