<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('components/head'); ?>

    <!-- START: Template CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.theme.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/simple-line-icons/css/simple-line-icons.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/flags-icon/css/flag-icon.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/select2/css/select2.min.css') ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/select2/css/select2-bootstrap.min.css') ?>" />
    <!-- END Template CSS-->

    <!-- START: Page CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/social-button/bootstrap-social.css'); ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/sweetalert/sweetalert.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/select2/css/select2.min.css') ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/select2/css/select2-bootstrap.min.css') ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/toastr/toastr.min.css'); ?>" />
    <!-- END: Page CSS-->

    <!-- START: Custom CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/css/main.css'); ?>">
    <!-- END: Custom CSS-->
</head>

<body id="main-container" class="default">

    <input type="hidden" name="base_url" id="base_url" value="<?= base_url("index.php"); ?>">
    <input type="hidden" name="api_url" id="api_url" value="<?= API_URI; ?>">

    <!-- START: Pre Loader-->
    <div class="se-pre-con">
        <div class="loader"></div>
    </div>
    <!-- END: Pre Loader-->

    <!-- START: Template JS-->
    <script src="<?= base_url('assets/dist/vendors/jquery/jquery-3.3.1.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/moment/moment.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/slimscroll/jquery.slimscroll.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/toastr/toastr.min.js'); ?>"></script>
    <!-- END: Template JS-->

    <!-- START: Page Vendor JS-->
    <script src="<?= base_url('assets/dist/vendors/select2/js/select2.full.min.js') ?>"></script>
    <script src="<?= base_url('assets/dist/js/auth/request.js'); ?>"></script>
    <script>
        var base_url = document.getElementById("base_url");
        var api_url = document.getElementById("api_url");
    </script>
    <!-- END: Page Vendor JS-->

    <!-- START: Main Content-->
    <?php

    // Load Navigation Bar
    $this->load->view('components/menus/navbar');

    // Load Sidebar
    $this->load->view('components/menus/sidebar');

    // Load Page
    $this->load->view('dashboard/template', array(
        'page' => "Kategori",
        'pageMap' => array(
            array(
                "label" => "Data Master",
                "is_current" => false
            ),
            array(
                "label" => "Kategori",
                "is_current" => true
            ),
        ),
        'pageURI' => "dashboard/master-data/pages/categories",
    ));

    ?>
    <!-- END: Content-->

    <!-- START: Page Script JS-->
    <?php
    if (isset($error)) {
        $this->load->view("components/error-modal", array(
            "errorModalTitle" => $error["title"],
            "errorModalContent" => $error["content"],
            "errorModalDetail" => $error["details"]
        ));
    }

    // Create Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-create-category",
        "modalTitle" => "Membuat Kategori",
        "iconTitle" => "icon-plus",
        "modalActionForm" => "#",
        "modalContentForm" => '
        <div class="form-group mb-3">
            <label for="create-category">Kategori</label>
            <input type="text" id="create-category" class="form-control" placeholder="Contoh: Makanan Khas" required/> 
        </div>
        <div class="form-group mb-3">
            <label for="create-description">Deskripsi</label>
            <input type="text" id="create-description" class="form-control" placeholder="Contoh: Makanan khas daerah tertentu" required/> 
        </div>
        <label>Ikon</label>
        <div class="input-group col-12 p-1">
            <input type="text" class="form-control p-2 w-100 h-100 contact-search" placeholder="Cari ..." id="icon-search-create">
            <div class="input-group-append">
                <span class="btn btn-outline-primary input-group-text" id="icon-button-search-create"><i class="icon-magnifier" aria-hidden="true"></i></span>
            </div>
        </div>
        <div class="icon-container card-body text-center row" style="width: 100%; max-height:280px; overflow: scroll;overflow-x: hidden;">

        </div>
        ',
        "modalButtonForm" => '<button type="button" class="btn btn-primary add-todo" id="create-category-button">Buat Kategori</button>'
    ));

    // Edit Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-edit-category",
        "modalTitle" => "Mengubah Kategori",
        "iconTitle" => "icon-pencil",
        "modalActionForm" => "#",
        "modalContentForm" => '
        <input type="hidden" id="id-category"/>
        <input type="hidden" id="id-icon"/>
        <div class="form-group mb-3">
            <label for="edit-category">Kategori</label>
            <input type="text" id="edit-category" class="form-control" placeholder="Contoh: Makanan Khas"/> 
        </div>
        <div class="form-group mb-3">
            <label for="edit-description">Deskripsi</label>
            <input type="text" id="edit-description" class="form-control" placeholder="Contoh: Makanan khas daerah tertentu"/> 
        </div>
        <label>Ikon</label>
        <div class="input-group col-12 p-1">
            <input type="text" class="form-control p-2 w-100 h-100 contact-search" placeholder="Cari ..." id="icon-search-edit">
            <div class="input-group-append">
                <span class="btn btn-outline-primary input-group-text" id="icon-button-search-edit"><i class="icon-magnifier" aria-hidden="true"></i></span>
            </div>
        </div>
        <div class="icon-container card-body text-center row" style="width: 100%; max-height:280px; overflow: scroll;overflow-x: hidden;">

        </div>
        ',
        "modalButtonForm" => '
        <button 
        type="button"
        id="edit-category-button"
        class="btn btn-success btn-block dropdown-toggle" 
        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Simpan <span class="icon-paper-plane"></span>
        </button>
        <div class="dropdown-menu p-0" style="">
            <a class="dropdown-item" href="javascript:void(0);"
                onclick="editCategory(1)">Aktif</a>
            <a class="dropdown-item" href="javascript:void(0);"
                onclick="editCategory(2)">Tertunda</a>
            <a class="dropdown-item" href="javascript:void(0);"
                onclick="editCategory(0)">Tidak Aktif</a>
        </div>'
    ));

    // Delete Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-delete-category",
        "modalTitle" => "Hapus Kategori",
        "iconTitle" => "icon-trash",
        "modalActionForm" => "#",
        "modalContentForm" => '
        <p>Apakah Anda yakin untuk menghapus kategori <strong id="delete-category-name"></strong> ?</p>
        ',
        "modalButtonForm" => '<button type="submit" class="btn btn-danger add-todo" id="delete-category">Hapus</button>'
    ));


    ?>
    <!-- END: Page Script JS-->

    <!-- START: APP JS-->
    <script src="<?= base_url('assets/dist/js/app.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/js/home.script.js'); ?>"></script>

    <script>
        var selected = 0;
        $('.icon-container').on('click', '.add-icon', function() {
            if (selected == 0) {
                selected = 1
                $(this).removeClass("btn-outline-dark").addClass("btn-success disabled")
                $(this).attr('id', 'selected');
            } else {
                var element = document.getElementById('selected')
                element.removeAttribute('id')
                element.classList.remove('btn-success')
                element.classList.remove('disabled')
                element.classList.add('btn-outline-dark')
                $(this).removeClass("btn-outline-dark").addClass("btn-success disabled")
                $(this).attr('id', 'selected');
            }
        })

        var searchIcon = ""
        var classIconClicked = ""

        // Initial Pagination
        var page = 0;
        var paginationClicked = false;
        var paginationDirection = "";

        (function($) {
            "use strict";
            $(window).on("load", function() {
                // Load Categories First Time
                loadCategories()

                // Handle Order By
                $("#master-categories-order").on("change", function() {
                    loadCategories();
                });

                // Handle Search Icon
                $("#icon-button-search-create").on("click", function() {
                    searchIcon = $('#icon-search-create').val();
                    loadIcons()
                });

                // Handle Search Icon
                $("#icon-button-search-edit").on("click", function() {
                    searchIcon = $('#icon-search-edit').val();
                    loadIcons()
                });

                // Load Icon
                $("#trigger-icon").on("click", function() {
                    loadIcons()
                });

                // Handle Search
                $("#master-categories-search").on("keydown", function(event) {
                    if (event.keyCode == 32 || event.which == 32) loadCategories();
                    else if ($("#master-categories-search").val() == "") loadCategories();
                });

                $("#master-categories-button-search").on("click", function(event) {
                    loadCategories();
                });

                // Handle Pagination
                $("#master-category-prev").on("click", function() {
                    if (page > 0)
                        page -= 1;
                    paginationClicked = true;
                    paginationDirection = "prev";
                    loadCategories();
                });

                $("#master-category-next").on("click", function() {
                    page += 1;
                    paginationClicked = true;
                    paginationDirection = "next";
                    loadCategories();
                });

                // Create Categories
                $("#create-category-button").on("click", function() {
                    if($("#create-category").val().trim() == ""){
                        toastr.warning("Silahkan isi nama kategori terlebih dahulu")
                    }else if($("#create-description").val().trim() == ""){
                        toastr.warning("Silahkan isi deskripsi kategori terlebih dahulu")
                    }else if(!document.getElementById('selected')){
                        toastr.warning("Silahkan pilih ikon terlebih dahulu")
                    }else {
                        $("#create-category-button").attr("disabled", true);

                        var raw = req.raw({
                            category: $("#create-category").val(),
                            slug: $("#create-category").val().replace(/\s+/g, '-').toLowerCase(),
                            description: $("#create-description").val(),
                            id_icon: document.getElementById('selected').children[0].children[0].classList[0]
                        })

                        var formData = new FormData();
                        formData.append("raw", raw)

                        $.ajax({
                            url: base_url.value + "/dashboard/a/masterData/create_categories",
                            data: formData,
                            type: "POST",
                            contentType: false,
                            processData: false,
                            success: function(response) {
                                response = req.data(response)

                                $("#create-category-button").removeAttr("disabled");
                                $("#modal-create-category").modal('hide')

                                if (response.code == 200) {
                                    toastr.success("Sukses menambah kategori");
                                    // Refresh Table
                                    loadCategories()
                                    document.querySelectorAll('form').forEach(el => el.reset())
                                } else {
                                    toastr.error(response.message);
                                }
                            }
                        });
                    }
                });

                // Handle Delete Categories
                $("#delete-category").on("click", function() {
                    $("#delete-category").attr("disabled", true);

                    $.ajax({
                        url: base_url.value + "/dashboard/a/masterData/delete_categories/" + $("#delete-category").val(),
                        data: null,
                        type: "GET",
                        contentType: false,
                        processData: false,
                        success: function(response) {

                            $("#delete-category").removeAttr("disabled");
                            $("#modal-delete-category").modal('hide')

                            response = req.data(response)
                            if (response.code == 200) {
                                loadCategories()
                                toastr.success("Sukses menghapus kategori");
                            } else {
                                toastr.error(response.message);
                            }
                        }
                    });
                });
            });
        })(jQuery);

        function loadCategories() {
            renderToWaiting()

            var order = $("#master-categories-order").val();
            var order_direction = "DESC";
            switch (Number(order)) {
                case 1:
                    order = "category";
                    order_direction = "ASC";
                    break;
                case 2:
                    order = "category";
                    order_direction = "DESC";
                    break;
                case 3:
                    order = "description";
                    order_direction = "ASC";
                    break;
                case 4:
                    order = "description";
                    order_direction = "DESC";
                    break;
                default:
                    order = "id";
                    order_direction = "DESC";
                    break;
            }
            var raw = req.raw({
                page: page,
                search: $("#master-categories-search").val(),
                order_by: order,
                order_direction: order_direction
            })

            var formData = new FormData()
            formData.append("raw", raw)

            $.ajax({
                url: base_url.value + "/dashboard/a/masterData/load_categories",
                data: formData,
                type: "POST",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.code == 200) {
                        renderToTable(response.data)
                    } else {
                        renderToTable([])
                    }
                }
            });
        }

        function loadIcons() {
            selected = 0
            $(".icon-container").html('Sedang memuat data icon')
            var raw = req.raw({
                page: -1,
                search: searchIcon,
                order_by: "icon",
                order_direction: "DESC"
            });
            var formData = new FormData();
            formData.append("raw", raw);

            $.ajax({
                url: base_url.value + "/dashboard/a/masterData/load_icons",
                data: formData,
                type: "POST",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.code == 200) {
                        renderListIcon(response.data)
                    } else {
                        renderListIcon([])
                    }
                }
            });
            searchIcon = ""
        }

        function renderListIcon(data = []) {
            if(data.length == 0) {
                $(".icon-container").html('Tidak ada data ikon')
            } else {
                $(".icon-container").html(`${data.map(function(item) {
                    if(item.is_visible == 1){
                        if(item.icon == classIconClicked)
                            $("#id-icon").val(item.id)
                        return `
                        <div class="add-icon col font-icon-list p-2 border mx-1 mb-2 btn btn-outline-dark">
                            <div class="preview">
                                <i class="${item.id} ${item.icon} icons"></i> ${item.icon}   
                            </div>
                        </div>`
                    }
                }).join('')}`);
            }
            if(classIconClicked != ""){
                $(`.${classIconClicked}`).parent().parent().trigger("click")
            }
            classIconClicked = ""
        }

        function renderToWaiting() {
            $("#master-categories tbody").html(`
                <tr>
                    <td class="text-center" colspan="9">Sedang memuat data</td>
                </tr>
            `);
        }

        function renderToTable(data = []) {

            if (paginationClicked) {
                paginationClicked = false;
                if (data.length == 0) {
                    if (paginationDirection == "next")
                        page -= 1;
                    else page += 1;
                }
                paginationDirection = "";
                loadCategories();
                return
            }

            var index = (page * 10) + 1;

            if (data.length == 0) {
                $("#master-categories tbody").html(`
                    <tr>
                        <td class="align-middle" colspan="4">Belum ada data kategori, 
                        tekan <span class="icon-plus"></span> untuk menambahkan</td>
                    </tr>
                `);
                return;
            }

            $("#master-categories tbody").html(`${data.map(function(item) {
                status = item.is_visible
                isBlock = "";

                if (status == "0") {
                    status = "<span class='badge badge-danger'>Tidak Aktif</span>";
                    isBlock = "disabled";
                }
                else if (status == "1") {
                    status = "<span class='badge badge-primary'>Aktif</span>";
                }
                else {
                    status = "<span class='badge badge-warning'>Tertunda</span>";
                }
                return `
                    <tr>
                        <td class="align-middle">${index++}</td>
                        <td class="align-middle">${item.category}</td>
                        <td class="align-middle">${item.slug}</td>
                        <td class="align-middle">${item.description}</td>
                        <td class="align-middle h4"><i class="icon ${item.icon}"></td>
                        <td class="align-middle">${item.updated_at}</td>
                        <td class="align-middle">${status}</td>
                        <td class="align-middle">
                            <h4>
                                <a href="javascript:void(0);" data-toggle="modal" data-target="#modal-edit-category" class="text-success"><i class="icon-pencil" onclick="loadOneCategory(${item.id})"></i></a>
                            </h4>
                        </td>
                        <td class="align-middle">
                            <h4>
                                <a href="javascript:void(0);" data-toggle="modal" data-target="#modal-delete-category" class="btn text-danger ${isBlock}" onclick="deleteCategory(${item.id}, '${item.category}')"><i class="icon-trash"></i></a>
                            </h4>
                        </td>
                    </tr>
                `;
            }).join('')}`);
        }

        function deleteCategory(idCategory, item) {
            $("#delete-category").val(idCategory)
            $("#delete-category-name").text(item)
        }

        function loadOneCategory(idCategory) {
            $("#id-category").val(idCategory)
            $(".icon-container").html('Sedang memuat data icon')
            $("#edit-category-button").attr("disabled", true);
            $("#edit-category").val("")
            $("#edit-description").val("")
            $.ajax({
                url: base_url.value + "/dashboard/a/masterData/load_detail_category/" + idCategory,
                data: null,
                type: "GET",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.code == 200) {
                        classIconClicked = response.data.icon
                        loadIcons()
                        // $(`.${response.data.icon}`).parent().parent().trigger("click")
                        $("#edit-category").val(response.data.category)
                        $("#edit-description").val(response.data.description)
                        $("#edit-category-button").removeAttr("disabled");
                    } else {
                        toastr.error(response.message);
                    }
                }
            });
        }

        function editCategory(is_visible){
            if($("#edit-category").val().trim() == ""){
                toastr.warning("Silahkan isi nama kategori terlebih dahulu")
            }else if($("#edit-description").val().trim() == ""){
                toastr.warning("Silahkan isi deskripsi kategori terlebih dahulu")
            }else {
                $("#edit-category-button").attr("disabled", true);

                var raw = req.raw({
                    category: $("#edit-category").val(),
                    slug: $("#edit-category").val().replace(/\s+/g, '-').toLowerCase(),
                    description: $("#edit-description").val(),
                    id_icon: document.getElementById('selected') ? document.getElementById('selected').children[0].children[0].classList[0] : $("#id-icon").val(),
                    is_visible: is_visible == 0 ? "0" : is_visible
                })

                var formData = new FormData()
                formData.append("raw", raw)

                // Update Category
                $.ajax({
                    url: base_url.value + "/dashboard/a/masterData/edit_category/" + $('#id-category').val(),
                    data: formData,
                    type: "POST",
                    contentType: false,
                    processData: false,
                    success: function(response) {

                        response = req.data(response)

                        $("#modal-edit-category").modal("hide")
                        $("#edit-category-button").removeAttr("disabled");

                        if (response.code == 200) {
                            toastr.success("Sukses mengubah kategori");
                            loadCategories()
                            document.querySelectorAll('form').forEach(el => el.reset())
                        } else {
                            toastr.error(response.message);
                        }
                    }
                });
            }
        }
    </script>
    <!-- END: APP JS-->

    <!-- START: Page Script JS-->
    <script src="<?= base_url('assets/dist/vendors/select2/js/select2.full.min.js') ?>"></script>
    <script>
        // Select2
        $(".select2").select2({
            theme: 'bootstrap4',
            width: 'style',
            minimumResultsForSearch: Infinity
        });
    </script>
    <!-- END: Page Script JS-->
</body>

</html>