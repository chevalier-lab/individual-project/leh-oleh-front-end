<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('components/head'); ?>

    <!-- START: Template CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.theme.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/simple-line-icons/css/simple-line-icons.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/flags-icon/css/flag-icon.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/select2/css/select2.min.css') ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/select2/css/select2-bootstrap.min.css') ?>" />
    <!-- END Template CSS-->

    <!-- START: Page CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/social-button/bootstrap-social.css'); ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/sweetalert/sweetalert.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/toastr/toastr.min.css'); ?>"/>
    <!-- END: Page CSS-->

    <!-- START: Custom CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/css/main.css'); ?>">
    <!-- END: Custom CSS-->
</head>

<body id="main-container" class="default">
    <input type="hidden" name="base_url" id="base_url" value="<?= base_url("index.php"); ?>">
    <input type="hidden" name="api_url" id="api_url" value="<?= API_URI; ?>">

    <!-- START: Pre Loader-->
    <div class="se-pre-con">
        <div class="loader"></div>
    </div>
    <!-- END: Pre Loader-->

    <!-- START: Template JS-->
    <script src="<?= base_url('assets/dist/vendors/jquery/jquery-3.3.1.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/moment/moment.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/slimscroll/jquery.slimscroll.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/toastr/toastr.min.js'); ?>"></script>
    <!-- END: Template JS-->

    <!-- START: Page Vendor JS-->
    <script src="<?= base_url('assets/dist/js/auth/request.js'); ?>"></script>
    <script>
        var base_url = document.getElementById("base_url");
        var api_url = document.getElementById("api_url");
    </script>
    <!-- END: Page Vendor JS-->

    <!-- START: Main Content-->
    <?php

    // Load Navigation Bar
    $this->load->view('components/menus/navbar');

    // Load Sidebar
    $this->load->view('components/menus/sidebar');

    // Load Page
    $this->load->view('dashboard/template', array(
        'page' => "Label",
        'pageMap' => array(
            array(
                "label" => "Data Master",
                "is_current" => false
            ),
            array(
                "label" => "Label",
                "is_current" => true
            ),
        ),
        'pageURI' => "dashboard/master-data/pages/tags",
    ));

    ?>
    <!-- END: Content-->

    <!-- START: Page Script JS-->
    <?php
    if (isset($error)) {
        $this->load->view("components/error-modal", array(
            "errorModalTitle" => $error["title"],
            "errorModalContent" => $error["content"],
            "errorModalDetail" => $error["details"]
        ));
    }

    // Create Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-create-tag",
        "modalTitle" => "Membuat Label",
        "iconTitle" => "icon-plus",
        "modalActionForm" => "#",
        "modalContentForm" => '
        <div class="form-group mb-3">
            <label for="create-tag">Label</label>
            <input type="text" id="create-tag" class="form-control" placeholder="Contoh: Fakta Unik"/> 
        </div>
        ',
        "modalButtonForm" => '<button type="button" class="btn btn-primary add-todo" id="create-tag-button">Buat Label</button>'
    ));

    // Edit Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-edit-tag",
        "modalTitle" => "Mengubah Label",
        "iconTitle" => "icon-pencil",
        "modalActionForm" => "#",
        "modalContentForm" => '
        <input type="hidden" id="id-tag"/>
        <div class="form-group mb-3">
            <label for="edit-tag">Tag</label>
            <input type="text" id="edit-tag" class="form-control" placeholder="Contoh: Fakta Unik"/> 
        </div>
        ',
        "modalButtonForm" => '
        <button 
        type="button"
        id="edit-tag-button"
        class="btn btn-success btn-block dropdown-toggle" 
        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Simpan <span class="icon-paper-plane"></span>
        </button>
        <div class="dropdown-menu p-0" style="">
            <a class="dropdown-item" href="javascript:void(0);"
                onclick="editTag(1)">Aktif</a>
            <a class="dropdown-item" href="javascript:void(0);"
                onclick="editTag(2)">Tertunda</a>
            <a class="dropdown-item" href="javascript:void(0);"
                onclick="editTag(0)">Tidak Aktif</a>
        </div>'
    ));

    // Delete Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-delete-tag",
        "modalTitle" => "Menghapus Label",
        "iconTitle" => "icon-trash",
        "modalActionForm" => "#",
        "modalContentForm" => '
        <p>Apakah Anda yakin akan menghapus label <strong id="delete-tag-name"></strong> ?</p>
        ',
        "modalButtonForm" => '<button type="button" class="btn btn-danger add-todo" id="delete-tag">Hapus</button>'
    ));


    ?>
    <!-- END: Page Script JS-->

    <!-- START: APP JS-->
    <script src="<?= base_url('assets/dist/js/app.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/js/home.script.js'); ?>"></script>

    <script>
        // Initial Pagination
        var page = 0;
        var paginationClicked = false;
        var paginationDirection = "";

        (function($) {
            "use strict";
            $(window).on("load", function() {
                // Load Tags First Time
                loadTags()

                // Handle Order By
                $("#master-tags-order").on("change", function() {
                    loadTags();
                });

                // Handle Search
                $("#master-tags-search").on("keydown", function(event) {
                    if (event.keyCode == 32 || event.which == 32) loadTags();
                    else if ($("#master-tags-search").val() == "") loadTags();
                });

                $("#master-tags-button-search").on("click", function(event) {
                    loadTags();
                });

                // Handle Pagination
                $("#master-tag-prev").on("click", function() {
                    if (page > 0)
                        page -= 1;
                    paginationClicked = true;
                    paginationDirection = "prev";
                    loadTags();
                });

                $("#master-tag-next").on("click", function() {
                    page += 1;
                    paginationClicked = true;
                    paginationDirection = "next";
                    loadTags();
                });

                // Create Tag
                $("#create-tag-button").on("click", function() {
                    if($("#create-tag").val().trim() == ""){
                        toastr.warning("Silahkan isi nama label terlebih dahulu")
                    } else {
                        $("#create-tag-button").attr("disabled", true);

                        var raw = req.raw({
                            tag: $("#create-tag").val(),
                            slug: $("#create-tag").val().replace(/\s+/g, '-').toLowerCase(),
                        })

                        var formData = new FormData();
                        formData.append("raw", raw)

                        $.ajax({
                            url: base_url.value + "/dashboard/a/masterData/create_tags",
                            data: formData,
                            type: "POST",
                            contentType: false,
                            processData: false,
                            success: function(response) {
                                response = req.data(response)

                                $("#create-tag-button").removeAttr("disabled");
                                $("#modal-create-tag").modal('hide')

                                if (response.code == 200) {
                                    toastr.success("Sukses menambah label");
                                    // Refresh Table
                                    loadTags()
                                    document.querySelectorAll('form').forEach(el => el.reset())
                                } else {
                                    toastr.error(response.message);
                                }
                            }
                        });
                    }
                });

                // Handle Delete Tags
                $("#delete-tag").on("click", function() {
                    $("#delete-tag").attr("disabled", true);

                    $.ajax({
                        url: base_url.value + "/dashboard/a/masterData/delete_tags/" + $("#delete-tag").val(),
                        data: null,
                        type: "GET",
                        contentType: false,
                        processData: false,
                        success: function(response) {
                            
                            $("#modal-delete-tag").modal("hide")
                            $("#delete-tag").removeAttr("disabled");

                            response = req.data(response)
                            if (response.code == 200) {
                                loadTags()
                                toastr.success("Sukses menghapus label");
                            } else {
                                toastr.error(response.message);
                            }
                        }
                    });
                });
            });
        })(jQuery);

        function loadTags() {
            renderToWaiting()

            var order = $("#master-tags-order").val();
            var order_direction = "DESC";
            switch (Number(order)) {
                case 1:
                    order = "tag";
                    order_direction = "ASC";
                    break;
                case 2:
                    order = "tag";
                    order_direction = "DESC";
                    break;
                default:
                    order = "id";
                    order_direction = "DESC";
                    break;
            }
            var raw = req.raw({
                page: page,
                search: $("#master-tags-search").val(),
                order_by: order,
                order_direction: order_direction
            })

            var formData = new FormData()
            formData.append("raw", raw)

            $.ajax({
                url: base_url.value + "/dashboard/a/masterData/load_tags",
                data: formData,
                type: "POST",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.code == 200) {
                        renderToTable(response.data)
                    } else {
                        renderToTable([])
                    }
                }
            });
        }

        function renderToWaiting() {
            $("#master-tags tbody").html(`
                <tr>
                    <td class="text-center" colspan="7">Sedang memuat data</td>
                </tr>
            `);
        }

        function renderToTable(data = []) {

            if (paginationClicked) {
                paginationClicked = false;
                if (data.length == 0) {
                    if (paginationDirection == "next")
                        page -= 1;
                    else page += 1;
                }
                paginationDirection = "";
                loadTags();
                return
            }

            var index = (page * 10) + 1;

            if (data.length == 0) {
                $("#master-tags tbody").html(`
                    <tr>
                        <td class="align-middle" colspan="4">Belum ada data tag, 
                        tekan <span class="icon-plus"></span> untuk menambahkan</td>
                    </tr>
                `);
                return;
            }

            $("#master-tags tbody").html(`${data.map(function(item) {
                status = item.is_visible
                isBlock = "";

                if (status == "0") {
                    status = "<span class='badge badge-danger'>Tidak Aktif</span>";
                    isBlock = "disabled";
                }
                else if (status == "1") {
                    status = "<span class='badge badge-primary'>Aktif</span>";
                }
                else {
                    status = "<span class='badge badge-warning'>Tertunda</span>";
                }
                return `
                    <tr>
                        <td class="align-middle">${index++}</td>
                        <td class="align-middle">${item.tag}</td>
                        <td class="align-middle">${item.slug}</td>
                        <td class="align-middle">${item.updated_at}</td>
                        <td class="align-middle">${status}</td>
                        <td class="align-middle">
                            <h4>
                                <a href="javascript:void(0);" data-toggle="modal" data-target="#modal-edit-tag" class="text-success"><i class="icon-pencil" onclick="loadOneTag(${item.id})"></i></a>
                            </h4>
                        </td>
                        <td class="align-middle">
                            <h4>
                                <a href="javascript:void(0);" data-toggle="modal" data-target="#modal-delete-tag" class="btn text-danger ${isBlock}" onclick="deleteTags(${item.id}, '${item.tag}')"><i class="icon-trash"></i></a>
                            </h4>
                        </td>
                    </tr>
                `;
            }).join('')}`);
        }

        function deleteTags(idTag, item) {
            $("#delete-tag").val(idTag)
            $("#delete-tag-name").text(item)
        }

        function loadOneTag(idTag) {
            $("#edit-tag").val("")
            $("#id-tag").val(idTag)
            $("#edit-tag-button").attr("disabled", true);
            $.ajax({
                url: base_url.value + "/dashboard/a/masterData/load_detail_tag/" + idTag,
                data: null,
                type: "GET",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.code == 200) {
                        $("#edit-tag").val(response.data.tag)
                    } else {
                        toastr.error(response.message);
                    }
                    $("#edit-tag-button").removeAttr("disabled");
                }
            });
        }

        function editTag(is_visible){
            if($("#edit-tag").val().trim() == ""){
                toastr.warning("Silahkan isi nama label terlebih dahulu")
            } else {
                $("#edit-tag-button").attr("disabled", true);

                var raw = req.raw({
                    tag: $("#edit-tag").val(),
                    slug: $("#edit-tag").val().replace(/\s+/g, '-').toLowerCase(),
                    is_visible: is_visible == 0 ? "0" : is_visible
                })

                var formData = new FormData()
                formData.append("raw", raw)

                // Update tag
                $.ajax({
                    url: base_url.value + "/dashboard/a/masterData/edit_tag/" + $('#id-tag').val(),
                    data: formData,
                    type: "POST",
                    contentType: false,
                    processData: false,
                    success: function(response) {

                        response = req.data(response)

                        $("#modal-edit-tag").modal("hide")
                        $("#edit-tag-button").removeAttr("disabled")

                        if (response.code == 200) {
                            toastr.success("Sukses mengubah label");
                            loadTags()
                        } else {
                            toastr.error(response.message);
                        }
                    }
                });
            }
        }
    </script>
    <!-- END: APP JS-->

    <!-- START: Page Script JS-->
    <script src="<?= base_url('assets/dist/vendors/select2/js/select2.full.min.js') ?>"></script>
    <script>
        // Select2
        $(".select2").select2({
            theme: 'bootstrap4',
            width: 'style',
            minimumResultsForSearch: Infinity
        });
    </script>
    <!-- END: Page Script JS-->
</body>

</html>