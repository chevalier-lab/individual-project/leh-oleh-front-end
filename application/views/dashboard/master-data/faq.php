<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('components/head'); ?>

    <!-- START: Template CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.theme.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/simple-line-icons/css/simple-line-icons.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/flags-icon/css/flag-icon.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/select2/css/select2.min.css') ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/select2/css/select2-bootstrap.min.css') ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/ionicons/css/ionicons.min.css') ?>">
    <!-- END Template CSS-->

    <!-- START: Page CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/social-button/bootstrap-social.css'); ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/sweetalert/sweetalert.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/toastr/toastr.min.css'); ?>"/>
    <!-- END: Page CSS-->

    <!-- START: Custom CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/css/main.css'); ?>">
    <!-- END: Custom CSS-->
</head>

<body id="main-container" class="default">
    <input type="hidden" name="base_url" id="base_url" value="<?= base_url("index.php"); ?>">
    <input type="hidden" name="api_url" id="api_url" value="<?= API_URI; ?>">
    <input type="hidden" name="full_name" id="full_name" value="<?= $full_name ?>">

    <!-- START: Pre Loader-->
    <div class="se-pre-con">
        <div class="loader"></div>
    </div>
    <!-- END: Pre Loader-->

    <!-- START: Template JS-->
    <script src="<?= base_url('assets/dist/vendors/jquery/jquery-3.3.1.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/moment/moment.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/slimscroll/jquery.slimscroll.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/toastr/toastr.min.js'); ?>"></script>
    <!-- END: Template JS-->

    <!-- START: Page Vendor JS-->
    <script src="<?= base_url('assets/dist/js/auth/request.js'); ?>"></script>
    <script>
        var base_url = document.getElementById("base_url");
        var api_url = document.getElementById("api_url");
        var full_name = document.getElementById("full_name");
    </script>
    <!-- END: Page Vendor JS-->

    <!-- START: Main Content-->
    <?php

    // Load Navigation Bar
    $this->load->view('components/menus/navbar');

    // Load Sidebar
    $this->load->view('components/menus/sidebar');

    // Load Page
    $this->load->view('dashboard/template', array(
        'page' => "FAQ",
        'pageMap' => array(
            array(
                "label" => "Data Master",
                "is_current" => false
            ),
            array(
                "label" => "FAQ",
                "is_current" => true
            ),
        ),
        'pageURI' => "dashboard/master-data/pages/faq",
    ));

    ?>
    <!-- END: Content-->

    <!-- START: Page Script JS-->
    <?php
    if (isset($error)) {
        $this->load->view("components/error-modal", array(
            "errorModalTitle" => $error["title"],
            "errorModalContent" => $error["content"],
            "errorModalDetail" => $error["details"]
        ));
    }

    // Detail Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-detail-faq",
        "modalTitle" => "Detail FAQ",
        "modalType" => "modal-lg",
        "iconTitle" => "icon-question",
        "modalActionForm" => "#",
        "modalContentForm" => '
            <div class="card-content">
                <div class="card-body">  
                    <h5 class="card-title mb-3 mt-2" id="question">Vintage Italian Restaurant</h5>
                    <p class="card-text" id="answer">Phasellus ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia. Phasellus ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia.</p>
                    <div class="row mt-4 mb-3">
                        <div class="col-6">
                            <b><i class="ion ion-person"></i> Dibuat oleh &nbsp: </b>
                            <span id="created-by"></span>
                            <br>
                            <b><i class="ion ion-person"></i> Diubah oleh : </b>
                            <span id="updated-by"></span>
                        </div>
                        <div class="col-6">
                            <b><i class="ion ion-calendar"></i> Dibuat tanggal &nbsp: </b>
                            <span id="created-at"></span>
                            <br>
                            <b><i class="ion ion-calendar"></i> Diubah tanggal : </b>
                            <span id="updated-at"></span>
                        </div>
                    </div>
                </div>
            </div>
        ',
        "modalButtonForm" => '<button type="button" class="btn btn-primary add-todo" data-dismiss="modal">Tutup</button>'
    ));

    // Create Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-create-faq",
        "modalTitle" => "Membuat FAQ",
        "modalType" => "modal-md",
        "iconTitle" => "icon-plus",
        "modalActionForm" => "#",
        "modalContentForm" => '
        <div class="form-group mb-3">
            <label for="create-faq">Pertanyaan</label>
            <input type="text" id="create-faq" class="form-control" placeholder="Contoh: Bagaimana cara belanja di leholeh?"/> 
        </div>
        <div class="form-group mb-3">
            <label for="create-answer">Jawaban</label>
            <textarea type="text" id="create-answer" class="form-control" rows="6"/></textarea>
        </div>
        ',
        "modalButtonForm" => '<button type="button" class="btn btn-primary add-todo" id="create-faq-button">Buat FAQ</button>'
    ));

    // Edit Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-edit-faq",
        "modalTitle" => "Mengubah FAQ",
        "modalType" => "modal-md",
        "iconTitle" => "icon-pencil",
        "modalActionForm" => "#",
        "modalContentForm" => '
        <input type="hidden" id="id-faq"/>
        <div class="form-group mb-3">
            <label for="edit-faq">Pertanyaan</label>
            <input type="text" id="edit-faq" class="form-control" placeholder="Contoh: Bagaimana cara belanja di leholeh?"/> 
        </div>
        <div class="form-group mb-3">
            <label for="edit-answer">Jawaban</label>
            <textarea type="text" id="edit-answer" class="form-control" rows="6"/></textarea>
        </div>
        ',
        "modalButtonForm" => '<button type="button" class="btn btn-success add-todo" id="edit-faq-button">Simpan</button>'
    ));

    // Delete Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-delete-faq",
        "modalTitle" => "Menghapus Label",
        "modalType" => "modal-md",
        "iconTitle" => "icon-trash",
        "modalActionForm" => "#",
        "modalContentForm" => '
        <p>Apakah Anda yakin akan menghapus FAQ ini ?</p>
        ',
        "modalButtonForm" => '<button type="button" class="btn btn-danger add-todo" id="delete-faq">Hapus</button>'
    ));


    ?>
    <!-- END: Page Script JS-->

    <!-- START: APP JS-->
    <script src="<?= base_url('assets/dist/js/app.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/js/home.script.js'); ?>"></script>

    <script>
        // Initial Pagination
        var page = 0;
        var paginationClicked = false;
        var paginationDirection = "";

        (function($) {
            "use strict";
            $(window).on("load", function() {
                // Load FAQ First Time
                loadFaq()

                // Handle Order By
                $("#master-faqs-order").on("change", function() {
                    loadFaq();
                });

                // Handle Search
                $("#master-faqs-search").on("keydown", function(event) {
                    if (event.keyCode == 32 || event.which == 32) loadFaq();
                    else if ($("#master-faqs-search").val() == "") loadFaq();
                });

                $("#master-faqs-button-search").on("click", function(event) {
                    loadFaq();
                });

                // Handle Pagination
                $("#master-faq-prev").on("click", function() {
                    if (page > 0)
                        page -= 1;
                    paginationClicked = true;
                    paginationDirection = "prev";
                    loadFaq();
                });

                $("#master-faq-next").on("click", function() {
                    page += 1;
                    paginationClicked = true;
                    paginationDirection = "next";
                    loadFaq();
                });

                // Create Faq
                $("#create-faq-button").on("click", function() {
                    if($("#create-faq").val().trim() == ""){
                        toastr.warning("Silahkan isi pertanyaan terlebih dahulu")
                    }else if($("#create-answer").val().trim() == ""){
                        toastr.warning("Silahkan isi jawaban terlebih dahulu")
                    }else{
                        $("#create-faq-button").attr("disabled", true);

                        var raw = req.raw({
                            question: $("#create-faq").val(),
                            answer: $("#create-answer").val(),
                            created_by: full_name.value,
                            updated_by: full_name.value
                        })

                        var formData = new FormData();
                        formData.append("raw", raw)

                        $.ajax({
                            url: base_url.value + "/dashboard/a/masterData/create_faq",
                            data: formData,
                            type: "POST",
                            contentType: false,
                            processData: false,
                            success: function(response) {
                                response = req.data(response)

                                $("#create-faq-button").removeAttr("disabled");
                                $("#modal-create-faq").modal('hide')

                                if (response.code == 200) {
                                    toastr.success("Sukses menambah FAQ");
                                    // Refresh Table
                                    loadFaq()
                                    document.querySelectorAll('form').forEach(el => el.reset())
                                } else {
                                    toastr.error(response.message);
                                }
                            }
                        });
                    }
                });

                // Edit Faq
                $("#edit-faq-button").on("click", function() {
                    if($("#edit-faq").val().trim() == ""){
                        toastr.warning("Silahkan isi pertanyaan terlebih dahulu")
                    }else if($("#edit-answer").val().trim() == ""){
                        toastr.warning("Silahkan isi jawaban terlebih dahulu")
                    }else{
                        $("#edit-faq-button").attr("disabled", true);

                        var raw = req.raw({
                            question: $("#edit-faq").val(),
                            answer: $("#edit-answer").val(),
                            updated_by: full_name.value
                        })

                        var formData = new FormData();
                        formData.append("raw", raw)

                        $.ajax({
                            url: base_url.value + "/dashboard/a/masterData/edit_faq/" + $("#id-faq").val(),
                            data: formData,
                            type: "POST",
                            contentType: false,
                            processData: false,
                            success: function(response) {
                                response = req.data(response)

                                $("#edit-faq-button").removeAttr("disabled");
                                $("#modal-edit-faq").modal('hide')

                                if (response.code == 200) {
                                    toastr.success("Sukses mengubah FAQ");
                                    // Refresh Table
                                    loadFaq()
                                    document.querySelectorAll('form').forEach(el => el.reset())
                                } else {
                                    toastr.error(response.message);
                                }
                            }
                        });
                    }
                });

                // Handle Delete FAQ
                $("#delete-faq").on("click", function() {
                    $("#delete-faq").attr("disabled", true);

                    $.ajax({
                        url: base_url.value + "/dashboard/a/masterData/delete_faq/" + $("#delete-faq").val(),
                        data: null,
                        type: "GET",
                        contentType: false,
                        processData: false,
                        success: function(response) {
                            
                            $("#modal-delete-faq").modal("hide")
                            $("#delete-faq").removeAttr("disabled");

                            response = req.data(response)
                            if (response.code == 200) {
                                loadFaq()
                                toastr.success("Sukses menghapus FAQ");
                            } else {
                                toastr.error(response.message);
                            }
                        }
                    });
                });
            });
        })(jQuery);

        function loadFaq() {
            renderToWaiting()

            var order = $("#master-faqs-order").val();
            var order_direction = "DESC";
            switch (Number(order)) {
                case 1:
                    order = "question";
                    order_direction = "ASC";
                    break;
                case 2:
                    order = "question";
                    order_direction = "DESC";
                    break;
                case 1:
                    order = "created_at";
                    order_direction = "ASC";
                    break;
                case 2:
                    order = "updated_at";
                    order_direction = "DESC";
                    break;
                default:
                    order = "id";
                    order_direction = "ASC";
                    break;
            }
            var raw = req.raw({
                page: page,
                search: $("#master-faqs-search").val(),
                order_by: order,
                order_direction: order_direction
            })

            var formData = new FormData()
            formData.append("raw", raw)

            $.ajax({
                url: base_url.value + "/dashboard/a/masterData/load_faq",
                data: formData,
                type: "POST",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.code == 200) {
                        renderToTable(response.data)
                    } else {
                        renderToTable([])
                    }
                }
            });
        }

        function renderToWaiting() {
            $("#master-faqs tbody").html(`
                <tr>
                    <td class="text-center" colspan="7">Sedang memuat data</td>
                </tr>
            `);
        }

        function renderToTable(data = []) {

            if (paginationClicked) {
                paginationClicked = false;
                if (data.length == 0) {
                    if (paginationDirection == "next")
                        page -= 1;
                    else page += 1;
                }
                paginationDirection = "";
                loadFaq();
                return
            }

            var index = (page * 10) + 1;

            if (data.length == 0) {
                $("#master-faqs tbody").html(`
                    <tr>
                        <td class="align-middle" colspan="4">Belum ada data FAQ, 
                        tekan <span class="icon-plus"></span> untuk menambahkan</td>
                    </tr>
                `);
                return;
            }

            $("#master-faqs tbody").html(`${data.map(function(item) {
                return `
                    <tr>
                        <td class="align-middle">${index++}</td>
                        <td class="align-middle">${item.question}</td>
                        <td class="align-middle">${item.created_by}</td>
                        <td class="align-middle">${item.created_at}</td>
                        <td class="align-middle">${item.updated_at}</td>
                        <td class="align-middle">
                            <h4 class="text-center">
                                <a href="javascript:void(0);" data-toggle="modal" data-target="#modal-detail-faq" class="btn text-primary"><i class="icon-eye" onclick="loadDetailFaq(${item.id})"></i></a>
                            </h4>
                        </td>
                        <td class="align-middle">
                            <h4 class="text-center">
                                <a href="javascript:void(0);" data-toggle="modal" data-target="#modal-edit-faq" class="btn text-success" onclick="editFaq(${item.id})"><i class="icon-pencil"></i></a>
                            </h4>
                        </td>
                        <td class="align-middle">
                            <h4 class="text-center">
                                <a href="javascript:void(0);" data-toggle="modal" data-target="#modal-delete-faq" class="btn text-danger" onclick="deleteFaq(${item.id})"><i class="icon-trash"></i></a>
                            </h4>
                        </td>
                    </tr>
                `;
            }).join('')}`);
        }

        function loadDetailFaq(idFaq) {
            $("#question").html("Sedang membuat data pertanyaan")
            $("#answer").html("Sedang memuat data jawaban")
            $.ajax({
                url: base_url.value + "/dashboard/a/masterData/load_detail_faq/" + idFaq,
                data: null,
                type: "GET",
                contentType: false,
                processData: false,
                success: function(response) {
                    console.log(response)
                    response = req.data(response)
                    if (response.code == 200) {
                        $("#question").html(response.data.question)
                        $("#answer").html(response.data.answer)
                        $("#created-by").html(response.data.created_by)
                        $("#updated-by").html(response.data.updated_by)
                        $("#created-at").html(response.data.created_at)
                        $("#updated-at").html(response.data.updated_at)
                    } else {
                        toastr.error(response.message);
                    }
                }
            });
        }

        function editFaq(idFaq) {
            $("#edit-faq").val("")
            $("#edit-answer").val("")
            $("#id-faq").val(idFaq)
            $("#edit-faq-button").attr("disabled", true);
            $.ajax({
                url: base_url.value + "/dashboard/a/masterData/load_detail_faq/" + idFaq,
                data: null,
                type: "GET",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.code == 200) {
                        $("#edit-faq").val(response.data.question)
                        $("#edit-answer").val(response.data.answer)
                    } else {
                        toastr.error(response.message);
                    }
                    $("#edit-faq-button").removeAttr("disabled");
                }
            });
        }

        function deleteFaq(idFaq) {
            $("#delete-faq").val(idFaq)
        }

        
    </script>
    <!-- END: APP JS-->

    <!-- START: Page Script JS-->
    <script src="<?= base_url('assets/dist/vendors/select2/js/select2.full.min.js') ?>"></script>
    <script>
        // Select2
        $(".select2").select2({
            theme: 'bootstrap4',
            width: 'style',
            minimumResultsForSearch: Infinity
        });
    </script>
    <!-- END: Page Script JS-->
</body>

</html>