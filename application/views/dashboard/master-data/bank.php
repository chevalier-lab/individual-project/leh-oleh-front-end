<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('components/head'); ?>

    <!-- START: Template CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.theme.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/simple-line-icons/css/simple-line-icons.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/flags-icon/css/flag-icon.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/select2/css/select2.min.css') ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/select2/css/select2-bootstrap.min.css') ?>" />
    <!-- END Template CSS-->

    <!-- START: Page CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/social-button/bootstrap-social.css'); ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/sweetalert/sweetalert.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/toastr/toastr.min.css'); ?>" />
    <!-- END: Page CSS-->

    <!-- START: Custom CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/css/main.css'); ?>">
    <!-- END: Custom CSS-->
</head>

<body id="main-container" class="default">

    <input type="hidden" name="base_url" id="base_url" value="<?= base_url("index.php"); ?>">
    <input type="hidden" name="api_url" id="api_url" value="<?= API_URI; ?>">

    <!-- START: Pre Loader-->
    <div class="se-pre-con">
        <div class="loader"></div>
    </div>
    <!-- END: Pre Loader-->

    <!-- START: Template JS-->
    <script src="<?= base_url('assets/dist/vendors/jquery/jquery-3.3.1.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/moment/moment.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/slimscroll/jquery.slimscroll.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/toastr/toastr.min.js'); ?>"></script>
    <script src="https://cdn.jsdelivr.net/npm/lozad/dist/lozad.min.js"></script>
    <!-- END: Template JS-->

    <!-- START: Page Vendor JS-->
    <script src="<?= base_url('assets/dist/js/auth/request.js'); ?>"></script>
    <script>
        var base_url = document.getElementById("base_url");
        var api_url = document.getElementById("api_url");
    </script>
    <!-- END: Page Vendor JS-->

    <!-- START: Main Content-->
    <?php

    // Load Navigation Bar
    $this->load->view('components/menus/navbar');

    // Load Sidebar
    $this->load->view('components/menus/sidebar');

    // Load Page
    $this->load->view('dashboard/template', array(
        'page' => "Bank",
        'pageMap' => array(
            array(
                "label" => "Data Master",
                "is_current" => false
            ),
            array(
                "label" => "Bank",
                "is_current" => true
            ),
        ),
        'pageURI' => "dashboard/master-data/pages/bank",
    ));

    ?>
    <!-- END: Content-->

    <!-- START: Page Script JS-->
    <?php
    if (isset($error)) {
        $this->load->view("components/error-modal", array(
            "errorModalTitle" => $error["title"],
            "errorModalContent" => $error["content"],
            "errorModalDetail" => $error["details"]
        ));
    }

    // Create Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-create-bank",
        "modalTitle" => "Membuat Bank",
        "iconTitle" => "icon-plus",
        "modalActionForm" => "#",
        "modalContentForm" => '
        <div class="form-group mb-3">
            <label>Logo Bank</label>
            <div>
                <img id="create-logo-preview" src="" class="img-fluid" width="200">
            </div>
            <label for="create-logo-bank" 
                class="file-upload btn btn-primary btn-block">
                    <i class="fa fa-upload mr-2"></i>Pilh Logo ...
                    <input id="create-logo-bank" type="file" required>
            </label>
        </div>
        <div class="form-group mb-3">
            <label for="create-name-bank">Nama Bank</label>
            <input type="text" id="create-name-bank" class="form-control" placeholder="Contoh: BRI"/> 
        </div>
        ',
        "modalButtonForm" => '<button type="button" 
            class="btn btn-primary add-todo"
            id="create-bank">Buat Bank</button>'
    ));

    // Utility Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-utility-bank",
        "modalTitle" => "Langkah Transfer Bank",
        "iconTitle" => "icon-plus",
        "modalActionForm" => "#",
        "modalType" => "modal-xl",
        "modalContentForm" => '
            <div class="row">
                <div class="col-12 col-md-8 col-sm-12" id="list-utility-bank">
                </div>

                <div class="col-12 col-md-4 col-sm-12" id="form-utility-bank">
                </div>
            </div>
        ',
        "modalButtonForm" => ''
    ));

    // Account Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-account-bank",
        "modalTitle" => "Akun Bank",
        "iconTitle" => "icon-plus",
        "modalActionForm" => "#",
        "modalType" => "modal-xl",
        "modalContentForm" => '
            <div class="row">
                <div class="col-12 col-md-8 col-sm-12" id="list-account-bank">
                </div>

                <div class="col-12 col-md-4 col-sm-12" id="form-account-bank">
                </div>
            </div>
        ',
        "modalButtonForm" => ''
    ));

    // Edit Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-edit-bank",
        "modalTitle" => "Mengubah Bank",
        "iconTitle" => "icon-pencil",
        "modalType" => "modal-md",
        "modalActionForm" => "#",
        "modalContentForm" => '
        <div class="form-group mb-3">
            <input type="hidden" id="id-bank"/>
            <label>Logo Bank</label>
            <div>
                <img id="edit-logo-preview" src="" class="img-fluid" width="200">
            </div>
            <label for="edit-logo-bank" 
                class="file-upload btn btn-success btn-block">
                    <i class="fa fa-upload mr-2"></i>Pilih logo ...
                    <input id="edit-logo-bank" type="file" required>
            </label>
        </div>
        <div class="form-group mb-3">
            <label for="edit-name-bank">Nama Bank</label>
            <input type="text" id="edit-name-bank" class="form-control"/> 
        </div>
        ',
        "modalButtonForm" => '
        <button 
        type="button"
        id="edit-bank"
        class="btn btn-success btn-block dropdown-toggle" 
        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Simpan <span class="icon-paper-plane"></span>
        </button>
        <div class="dropdown-menu p-0" style="">
            <a class="dropdown-item" href="javascript:void(0)"
                onclick="editBank(1)">Aktif</a>
            <a class="dropdown-item" href="javascript:void(0)"
                onclick="editBank(2)">Tertunda</a>
            <a class="dropdown-item" href="javascript:void(0)"
                onclick="editBank(0)">Tidak Aktif</a>
        </div>'
    ));

    // Delete Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-delete-bank",
        "modalTitle" => "Hapus Bank",
        "iconTitle" => "icon-trash",
        "modalActionForm" => "#",
        "modalContentForm" => '
        <p>Anda yakin ingin menghapus bank <strong id="delete-bank-name"></strong> ?</p>
        ',
        "modalButtonForm" => '<button type="submit" class="btn btn-danger add-todo" id="delete-bank">Hapus</button>'
    ));

    ?>
    <!-- END: Page Script JS-->

    <!-- START: APP JS-->
    <script src="<?= base_url('assets/dist/js/app.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/js/home.script.js'); ?>"></script>

    <script>
        // Initial Pagination
        var page = 0;
        var paginationClicked = false;
        var paginationDirection = "";

        (function($) {
            "use strict";
            $(window).on("load", function() {
                // Load Bank First Time
                loadBanks()

                // Handle Order By
                $("#master-bank-order").on("change", function() {
                    loadBanks();
                });

                // Handle Search
                $("#master-bank-search").on("keydown", function(event) {
                    if (event.keyCode == 32 || event.which == 32) loadBanks();
                    else if ($("#master-bank-search").val() == "") loadBanks();
                });

                $("#master-bank-button-search").on("click", function(event) {
                    loadBanks();
                });

                // Handle Pagination
                $("#master-bank-prev").on("click", function() {
                    if (page > 0)
                        page -= 1;
                    paginationClicked = true;
                    paginationDirection = "prev";
                    loadBanks();
                });

                $("#master-bank-next").on("click", function() {
                    page += 1;
                    paginationClicked = true;
                    paginationDirection = "next";
                    loadBanks();
                });

                // Logo Create Preview
                $("#create-logo-bank").on("change", function(event) {
                    previewCover(event, "create-logo-preview")
                });

                // Logo Edit Preview
                $("#edit-logo-bank").on("change", function(event) {
                    previewCover(event, "edit-logo-preview")
                });

                // Create Bank
                $("#create-bank").on("click", function() {
                    if(!$("#create-logo-bank").prop("files")[0]){
                        toastr.warning("Silahkan pilih logo bank terlebih dahulu")
                    }else if($("#create-name-bank").val().trim() == ""){
                        toastr.warning("Silahkan isi nama bank terlebih dahulu")
                    }else {
                        $("#create-bank").attr("disabled", true);

                        var formData = new FormData();
                        formData.append("name", $("#create-name-bank").val());
                        formData.append("logo", $("#create-logo-bank").prop("files")[0]);

                        $.ajax({
                            url: base_url.value + "/dashboard/a/masterData/create_bank",
                            data: formData,
                            type: "POST",
                            contentType: false,
                            processData: false,
                            success: function(response) {
                                response = req.data(response)

                                $("#create-bank").removeAttr("disabled");
                                $("#modal-create-bank").modal('hide')

                                if (response.code == 200) {
                                    toastr.success("Sukses menambah bank");
                                    // Refresh Table
                                    loadBanks()
                                    $("#create-name-bank").val('')
                                    $("#create-logo-bank").val('')
                                    $('#create-logo-preview').attr('src', '')
                                } else {
                                    toastr.error(response.message);
                                }
                            }
                        });
                    }
                });

                // Handle Delete Bank
                $("#delete-bank").on("click", function() {
                    $("#delete-bank").attr("disabled", true);

                    $.ajax({
                        url: base_url.value + "/dashboard/a/masterData/delete_bank/" + $("#delete-bank").val(),
                        data: null,
                        type: "GET",
                        contentType: false,
                        processData: false,
                        success: function(response) {
                            $("#delete-bank").removeAttr("disabled");
                            $("#modal-delete-bank").modal('hide')

                            response = req.data(response)
                            if (response.code == 200) {
                                loadBanks()
                                toastr.success("Sukses menghapus bank");
                            } else {
                                toastr.error(response.message);
                            }
                        }
                    });
                });
            });
        })(jQuery);

        // Handle Cover Start
        function previewCover(event,idElement) {
            var reader = new FileReader();
            reader.onload = function() {
                var output = document.getElementById(idElement);
                output.src = reader.result;
            }
            reader.readAsDataURL(event.target.files[0]);
            $(`#${idElement}`).addClass("mb-3")
        }
        // Handle Cover Stop

        function loadBanks() {
            renderToWaiting()

            var order = $("#master-bank-order").val();
            var order_direction = "DESC";
            switch (Number(order)) {
                case 1:
                    order = "bank_name";
                    order_direction = "ASC";
                    break;
                case 2:
                    order = "bank_name";
                    order_direction = "DESC";
                    break;
                default:
                    order = "id";
                    order_direction = "DESC";
                    break;
            }
            var raw = req.raw({
                page: page,
                search: $("#master-bank-search").val(),
                order_by: order,
                order_direction: order_direction
            })

            var formData = new FormData()
            formData.append("raw", raw)

            $.ajax({
                url: base_url.value + "/dashboard/a/masterData/load_banks",
                data: formData,
                type: "POST",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.code == 200) {
                        renderToTable(response.data)
                    } else {
                        renderToTable([])
                    }
                }
            });
        }

        function renderToWaiting() {
            $("#master-bank tbody").html(`
                <tr>
                    <td class="align-middle" colspan="5">Sedang memuat data</td>
                </tr>
            `);
        }

        function renderToTable(data = []) {

            if (paginationClicked) {
                paginationClicked = false;
                if (data.length == 0) {
                    if (paginationDirection == "next")
                        page -= 1;
                    else page += 1;
                }
                paginationDirection = "";
                loadBanks();
                return
            }

            var index = (page * 10) + 1;

            if (data.length == 0) {
                $("#master-bank tbody").html(`
                    <tr>
                        <td class="align-middle" colspan="4">Belum ada data bank, 
                        tekan <span class="icon-plus"></span> untuk menambahkan</td>
                    </tr>
                `);
                return;
            }

            $("#master-bank tbody").html(`${data.map(function(item) {
                status = item.is_visible
                isBlock = "";

                if (status == "0") {
                    status = "<span class='badge badge-danger'>Tidak Aktif</span>";
                    isBlock = "disabled";
                }
                else if (status == "1") {
                    status = "<span class='badge badge-primary'>Aktif</span>";
                }
                else {
                    status = "<span class='badge badge-warning'>Tertunda</span>";
                }
                return `
                    <tr>
                        <td class="align-middle">${index++}</td>
                        <td class="align-middle"><img data-src="${item.logo}" class="img-fluid img-thumbnail lozad" 
                        src="${base_url.value + '/../assets/dist/images/broken-256.png'}"
                        width="100"></td>
                        <td class="align-middle">${item.bank_name}</td>
                        <td class="align-middle">${status}</td>
                        <td class="align-middle">
                            <a href="javascript:void(0)" data-toggle="modal" data-target="#modal-utility-bank" 
                                class="btn btn-sm btn-primary" onclick="loadUtilityBank(${item.id})"><i class="icon-list"></i></a>
                            <a href="javascript:void(0)" data-toggle="modal" data-target="#modal-account-bank" 
                                class="btn btn-sm btn-info" onclick="loadAccountBank(${item.id})"><i class="icon-user"></i></a>
                            <a href="javascript:void(0)" data-toggle="modal" data-target="#modal-edit-bank" 
                                class="btn btn-sm btn-warning" onclick="loadOneBank(${item.id})"><i class="icon-pencil"></i></a>
                            <a href="javascript:void(0)" data-toggle="modal" data-target="#modal-delete-bank" 
                                class="btn btn-sm btn-danger ${isBlock}" onclick="deleteBank(${item.id}, '${item.bank_name}')"><i class="icon-trash"></i></a>
                        </td>
                    </tr>
                `;
            }).join('')}`);

            setInterval(() => {
                const observer = lozad();
                observer.observe();
            }, 2000);
        }

        function deleteBank(idBank, item) {
            $("#delete-bank").val(idBank)
            $("#delete-bank-name").text(item)
        }

        function utilityBankCreateMode(idBank) {
            $("#form-utility-bank").html(`
                <h4>Tambah Langkah</h4>

                <div class="form-group mb-3">
                    <label for="create-step-utility-bank">Langkah</label>
                    <input type="text" id="create-step-utility-bank" class="form-control" placeholder="Masukkan Kartu ATM ke Mesin ATM"/> 
                </div>
                <div class="form-group mb-3">
                    <button class="btn btn-primary" type="button"
                    id="create-utility-bank"
                    onclick="createUtilityBank(${idBank})">Simpan</button>
                </div>
            `);
        }

        function utilityBankEditMode(idUBank, step, idBank) {
            $("#form-utility-bank").html(`
                <h4>Ubah Langkah</h4>

                <div class="form-group mb-3">
                    <label for="update-step-utility-bank">Langkah</label>
                    <input type="text" id="update-step-utility-bank" class="form-control" placeholder="Masukkan Kartu ATM ke Mesin ATM"
                    value="${step}"/> 
                </div>
                <div class="form-group mb-3">
                    <button class="btn btn-primary" type="button"
                    id="update-utility-bank"
                    onclick="updateUtilityBank(${idUBank}, ${idBank})">Simpan</button>
                    <button class="btn btn-danger" type="button"
                    onclick="utilityBankCreateMode(${idBank})">Batal</button>
                </div>
            `);
        }

        function createUtilityBank(idBank) {
            if($("#create-step-utility-bank").val().trim() == ""){
                toastr.warning("Silahkan isi langkah transaksi terlebih dahulu")
            }else {
                $("#create-utility-bank").attr("disabled", true);
                $('.button-delete-step-bank').attr("disabled", true);

                var raw = req.raw({
                    id_m_banks: idBank,
                    step: $("#create-step-utility-bank").val()
                })

                var formData = new FormData()
                formData.append("raw", raw)

                $.ajax({
                    url: base_url.value + "/dashboard/a/masterData/create_utility_bank",
                    data: formData,
                    type: "POST",
                    contentType: false,
                    processData: false,
                    success: function(response) {
                        response = req.data(response)
                        if (response.code == 200) {
                            toastr.success("Berhasil menambahkan langkah");
                            loadUtilityBank(idBank);
                            $("#create-step-utility-bank").val("")
                        } else {
                            toastr.error(response.message);
                        }
                        $("#create-utility-bank").removeAttr("disabled");
                        $(".button-delete-step-bank").removeAttr("disabled");
                    }
                });
            }
        }

        function updateUtilityBank(idUBank, idBank) {
            if($("#update-step-utility-bank").val().trim() == ""){
                toastr.warning("Silahkan isi langkah transaksi terlebih dahulu")
            }else {
                $("#update-utility-bank").attr("disabled", true);
                $('.button-delete-step-bank').attr("disabled", true);

                var raw = req.raw({
                    id_m_banks: idBank,
                    step: $("#update-step-utility-bank").val()
                })

                var formData = new FormData()
                formData.append("raw", raw)

                $.ajax({
                    url: base_url.value + "/dashboard/a/masterData/update_utility_bank/" + idUBank,
                    data: formData,
                    type: "POST",
                    contentType: false,
                    processData: false,
                    success: function(response) {
                        response = req.data(response)
                        if (response.code == 200) {
                            toastr.success("Berhasil mengubah langkah");
                            loadUtilityBank(idBank);
                            $("#update-step-utility-bank").val("")
                        } else {
                            toastr.error(response.message);
                        }
                        $("#update-utility-bank").removeAttr("disabled");
                        $(".button-delete-step-bank").removeAttr("disabled");
                    }
                });
            }
        }

        function loadUtilityBank(idBank) {
            $("#list-utility-bank").html(`
                <table class="display table table-bordered table-hover" style="width:100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Langkah</th>
                            <th>Aksi</th>
                        </tr>
                        <tr>
                            <td colspan="3" class="text-center">
                                Sedang memuat data langkah transaksi
                            </td>
                        </tr>
                    </thead>
                </table>
            `);
            utilityBankCreateMode(idBank);
            $("#create-utility-bank").attr("disabled", true);

            $.ajax({
                url: base_url.value + "/dashboard/a/masterData/list_utility_bank/" + idBank,
                data: null,
                type: "GET",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.code == 200) {
                        var data = response.data
                        if (data.length == 0) $("#list-utility-bank").html(`
                            <table class="display table table-bordered table-hover" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Langkah</th>
                                        <th>Aksi</th>
                                    </tr>
                                    <tr>
                                        <td colspan="3" class="text-center">
                                            Belum ada langkah transaksi untuk bank ini
                                        </td>
                                    </tr>
                                </thead>
                            </table>
                        `);
                        else $("#list-utility-bank").html(`
                            <table class="display table table-bordered table-hover" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Langkah</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                ${data.map(function(item, position) {
                                    return `
                                    <tr>
                                        <td>${(position + 1)}</td>
                                        <td>${item.step}</td>
                                        <td>
                                            <button class="btn btn-warning btn-sm button-delete-step-bank" type="button"
                                            onclick="utilityBankEditMode(${item.id}, '${item.step}', ${item.id_m_banks})"><i class="icon-pencil"></i></button>
                                            <button class="btn btn-danger btn-sm button-delete-step-bank" type="button"
                                            onclick="deleteUtilityBank(${Number(item.id)}, ${Number(item.id_m_banks)})"><i class="icon-trash"></i></button>
                                        </td>
                                    </tr>
                                    `;
                                }).join('')}
                                </tbody>
                            </table>
                        `);
                    } else {
                        $("#list-utility-bank").html(`
                            <table class="display table table-bordered table-hover" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Langkah</th>
                                        <th>Aksi</th>
                                    </tr>
                                    <tr>
                                        <td colspan="3" class="text-center">
                                            Belum ada langkah transaksi untuk bank ini
                                        </td>
                                    </tr>
                                </thead>
                            </table>
                        `);
                    }
                    $("#create-utility-bank").removeAttr("disabled");
                }
            });
        }

        function deleteUtilityBank(idUBank, idBank) {
            $('.button-delete-step-bank').attr("disabled", true);
            $("#create-utility-bank").attr("disabled", true);
            $("#update-utility-bank").attr("disabled", true);
            $.ajax({
                url: base_url.value + "/dashboard/a/masterData/delete_utility_bank/" + idUBank,
                data: null,
                type: "GET",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.code == 200) {
                        toastr.success("Berhasil menghapus langkah");
                        loadUtilityBank(idBank);
                    } else {
                        toastr.error(response.message);
                        $(".button-delete-step-bank").removeAttr("disabled");
                        $("#create-utility-bank").removeAttr("disabled");
                        $("#update-utility-bank").removeAttr("disabled");
                    }
                }
            });
        }

        function accountBankCreateMode(idBank) {
            $("#form-account-bank").html(`
                <h4>Tambah Akun</h4>

                <div class="form-group mb-3">
                    <label for="create-account-number-bank">Nomor Rekening</label>
                    <input type="text" id="create-account-number-bank" class="form-control" placeholder="1301198513"/> 
                </div>
                <div class="form-group mb-3">
                    <label for="create-account-name-bank">Nama Akun</label>
                    <input type="text" id="create-account-name-bank" class="form-control" placeholder="Androxxx"/> 
                </div>
                <div class="form-group mb-3">
                    <label for="create-account-status-bank">Status</label>
                    <select id="create-account-status-bank" class="form-control">
                        <option value="0">Tidak Aktif</option>
                        <option value="1">Aktif</option>
                        <option value="2">Tertunda</option>
                    </select>
                </div>
                <div class="form-group mb-3">
                    <button class="btn btn-primary" type="button"
                    id="create-account-bank"
                    onclick="createAccountBank(${idBank})">Simpan</button>
                </div>
            `);
        }

        function createAccountBank(idBank) {
            if($("#create-account-number-bank").val().trim() == ""){
                toastr.warning("Silahkan isi nomor rekening terlebih dahulu")
            } else if($("#create-account-name-bank").val().trim() == ""){
                toastr.warning("Silahkan isi nama akun terlebih dahulu")
            } else {
                $("#create-account-bank").attr("disabled", true);
                $('.button-delete-account-bank').attr("disabled", true);
                
                var raw = req.raw({
                    id_m_banks: idBank,
                    account_number: $("#create-account-number-bank").val(),
                    account_name: $("#create-account-name-bank").val(),
                    is_visible: $("#create-account-status-bank").val()
                })

                var formData = new FormData()
                formData.append("raw", raw)

                $.ajax({
                    url: base_url.value + "/dashboard/a/masterData/create_bank_account",
                    data: formData,
                    type: "POST",
                    contentType: false,
                    processData: false,
                    success: function(response) {
                        response = req.data(response)
                        if (response.code == 200) {
                            toastr.success("Berhasil menambahkan akun bank");
                            loadAccountBank(idBank);
                            $("#create-account-number-bank").val("")
                            $("#create-account-name-bank").val("")
                        } else {
                            toastr.error(response.message);
                            $("#create-account-bank").removeAttr("disabled");
                        }
                        $("#update-account-bank").removeAttr("disabled");
                        $(".button-delete-account-bank").removeAttr("disabled");
                    }
                });
            }
        }

        function accountBankEditMode(idAccountBank, accountNumber, accountName, status, idBank) {
            $("#form-account-bank").html(`
                <h4>Ubah Akun</h4>

                <div class="form-group mb-3">
                    <label for="update-account-number-bank">Nomor Rekening</label>
                    <input type="text" id="update-account-number-bank" class="form-control" placeholder="1301198513"
                    value="${accountNumber}"/> 
                </div>
                <div class="form-group mb-3">
                    <label for="update-account-name-bank">Nama Akun</label>
                    <input type="text" id="update-account-name-bank" class="form-control" placeholder="Androxxx"
                    value="${accountName}"/> 
                </div>
                <div class="form-group mb-3">
                    <label for="update-account-status-bank">Status</label>
                    <select id="update-account-status-bank" class="form-control">
                        <option value="0" ${(status == 0) ? "selected" : ""}>Tidak Aktif</option>
                        <option value="1" ${(status == 1) ? "selected" : ""}>Aktif</option>
                        <option value="2" ${(status == 2) ? "selected" : ""}>Tertunda</option>
                    </select>
                </div>
                <div class="form-group mb-3">
                    <button class="btn btn-primary" type="button"
                    id="update-account-bank"
                    onclick="updateAccountBank(${idAccountBank}, ${idBank})">Simpan</button>
                    <button class="btn btn-danger" type="button"
                    onclick="loadAccountBank(${idBank})">Batal</button>
                </div>
            `);
        }

        function updateAccountBank(idAccountBank, idBank) {
            if($("#update-account-number-bank").val().trim() == ""){
                toastr.warning("Silahkan isi nomor rekening terlebih dahulu")
            } else if($("#update-account-name-bank").val().trim() == ""){
                toastr.warning("Silahkan isi nama akun terlebih dahulu")
            } else {
                $("#update-account-bank").attr("disabled", true);
                $('.button-delete-account-bank').attr("disabled", true);
                var raw = req.raw({
                    id_m_banks: idBank,
                    account_number: $("#update-account-number-bank").val(),
                    account_name: $("#update-account-name-bank").val(),
                    is_visible: $("#update-account-status-bank").val()
                })

                var formData = new FormData()
                formData.append("raw", raw)

                $.ajax({
                    url: base_url.value + "/dashboard/a/masterData/update_bank_account/" + idAccountBank,
                    data: formData,
                    type: "POST",
                    contentType: false,
                    processData: false,
                    success: function(response) {
                        response = req.data(response)
                        if (response.code == 200) {
                            toastr.success("Berhasil mengubah akun bank");
                            loadAccountBank(idBank);
                        } else {
                            toastr.error(response.message);
                        }                    
                        $("#update-account-bank").removeAttr("disabled");
                        $(".button-delete-account-bank").removeAttr("disabled");
                    }
                });
            }
        }

        function loadAccountBank(idBank) {
            $("#list-account-bank").html(`
                <table class="display table table-bordered table-hover" style="width:100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nomor Rekening</th>
                            <th>Nama Akun</th>
                            <th>Status</th>
                            <th>Aksi</th>
                        </tr>
                        <tr>
                            <td colspan="5" class="text-center">
                                Sedang memuat data akun bank
                            </td>
                        </tr>
                    </thead>
                </table>
            `);
            accountBankCreateMode(idBank)
            $("#create-account-bank").attr("disabled", true);

            $.ajax({
                url: base_url.value + "/dashboard/a/masterData/list_bank_account/" + idBank,
                data: null,
                type: "GET",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.code == 200) {
                        var data = response.data
                        if (data == null) {
                            $("#list-account-bank").html(`
                                <table class="display table table-bordered table-hover" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nomor Rekening</th>
                                            <th>Nama Akun</th>
                                            <th>Status</th>
                                            <th>Aksi</th>
                                        </tr>
                                        <tr>
                                            <td colspan="5" class="text-center">
                                                Belum ada akun untuk bank ini
                                            </td>
                                        </tr>
                                    </thead>
                                </table>
                            `);
                            $("#create-account-bank").removeAttr("disabled");
                        }
                        else {
                            var is_visible = ""
                            switch(Number(data.is_visible)){
                                case 0:
                                    is_visible = "<span class='badge badge-danger'>Tidak Aktif</span>"
                                    break;
                                case 1:
                                    is_visible = "<span class='badge badge-primary'>Aktif</span>"
                                    break;
                                case 2:
                                    is_visible = "<span class='badge badge-warning'>Tertunda</span>"
                                    break;
                            }
                            $("#list-account-bank").html(`
                            <table class="display table table-bordered table-hover" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Nomor Rekening</th>
                                        <th>Nama Akun</th>
                                        <th>Status</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>${data.account_number}</td>
                                        <td>${data.account_name}</td>
                                        <td>${is_visible}</td>
                                        <td>
                                            <button class="btn btn-warning btn-sm button-delete-account-bank" type="button"
                                            onclick="accountBankEditMode(${data.id}, '${data.account_number}', '${data.account_name}', '${data.is_visible}', ${data.id_m_banks})"><i class="icon-pencil"></i></button>
                                            <button class="btn btn-danger btn-sm button-delete-account-bank" type="button"
                                            onclick="deleteAccountBank(${Number(data.id)}, ${Number(data.id_m_banks)})"><i class="icon-trash"></i></button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            `);
                            $("#create-account-bank").attr("disabled", true);
                        }
                    } else {
                        $("#list-account-bank").html(`
                            <table class="display table table-bordered table-hover" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nomor Rekening</th>
                                        <th>Nama Akun</th>
                                        <th>Status</th>
                                        <th>Aksi</th>
                                    </tr>
                                    <tr>
                                        <td colspan="5" class="text-center">
                                            Belum ada akun untuk bank ini
                                        </td>
                                    </tr>
                                </thead>
                            </table>
                        `);
                        $("#create-account-bank").removeAttr("disabled");
                    }
                }
            });
        }

        function deleteAccountBank(idUBank, idBank) {
            $('.button-delete-account-bank').attr("disabled", true);
            $('#create-account-bank').attr("disabled", true);
            $('#update-account-bank').attr("disabled", true);
            $.ajax({
                url: base_url.value + "/dashboard/a/masterData/delete_bank_account/" + idUBank,
                data: null,
                type: "GET",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.code == 200) {
                        toastr.success("Berhasil menghapus akun bank");
                        loadAccountBank(idBank);
                    } else {
                        toastr.error(response.message);
                        $(".button-delete-account-bank").removeAttr("disabled");
                    }
                    $("#create-account-bank").removeAttr("disabled");
                    $("#update-account-bank").removeAttr("disabled");
                }
            });
        }

        function loadOneBank(idBank) {
            $("#edit-logo-preview").attr("src", base_url.value + '/../assets/dist/images/broken-256.png')
            $("#edit-name-bank").val("")
            $("#id-bank").val(idBank)
            $("#edit-bank").attr("disabled", true);
            $.ajax({
                url: base_url.value + "/dashboard/a/masterData/load_detail_bank/" + idBank,
                data: null,
                type: "GET",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.code == 200) {
                        $("#edit-bank").removeAttr("disabled")
                        $("#edit-name-bank").val(response.data.bank_name)
                        $("#edit-logo-preview").attr("src", response.data.logo)
                        $("#edit-logo-preview").addClass("mb-3")
                    } else {
                        toastr.error(response.message);
                    }
                }
            });
        }

        function editBank(is_visible){
            $("#edit-bank").attr("disabled", true);

            var raw = req.raw({
                bank_name: $("#edit-name-bank").val(),
                is_visible: is_visible == 0 ? "0" : is_visible
            })

            var formData = new FormData()
            formData.append("raw", raw)

            var logoData = new FormData()
            var logo = $("#edit-logo-bank")[0].files

            // Update bank
            $.ajax({
                url: base_url.value + "/dashboard/a/masterData/edit_bank/" + $('#id-bank').val(),
                data: formData,
                type: "POST",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.code == 200) {
                        toastr.success("Sukses mengubah data bank");

                        // Update logo if exist
                        if(logo.length != 0){
                            logoData.append("logo", logo[0])
                            $.ajax({
                                url: base_url.value + "/dashboard/a/masterData/edit_logo_bank/" + $('#id-bank').val(),
                                data: logoData,
                                type: "POST",
                                contentType: false,
                                processData: false,
                                success: function(response) {
                                    response = req.data(response)
                                    $("#modal-edit-bank").modal("hide")

                                    if (response.code == 200) {
                                        toastr.success("Sukses mengubah logo bank");
                                        // Refresh Table
                                        loadBanks()
                                    } else {
                                        toastr.error(response.message);
                                    }
                                    $("#edit-bank").removeAttr("disabled")
                                }
                            });
                        } else {
                            $("#modal-edit-bank").modal("hide")
                            loadBanks()
                        }
                    } else {
                        toastr.error(response.message);
                    }
                }
            });
        }
    </script>
    <!-- END: APP JS-->

    <!-- START: Page Script JS-->
    <script src="<?= base_url('assets/dist/vendors/select2/js/select2.full.min.js') ?>"></script>
    <script>
        // Select2
        $(".select2").select2({
            theme: 'bootstrap4',
            width: 'style',
            minimumResultsForSearch: Infinity
        });
    </script>
    <!-- END: Page Script JS-->
</body>

</html>