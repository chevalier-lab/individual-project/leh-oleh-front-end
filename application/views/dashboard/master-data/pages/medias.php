<div class="card">
    <div class="card-header">
        <h4 class="card-title">Data Media</h4>
    </div>
    <div class="card-body">

        <div class="pt-0 pb-0 pl-3 pr-3 mb-3 row">
            <div class="input-group col-12 col-md-6 p-1">
                <input type="text" class="form-control p-2 w-100 h-100 contact-search" placeholder="Cari ..." id="master-medias-search">
                <div class="input-group-append">
                    <span class="btn btn-outline-primary input-group-text" id="master-medias-button-search"><i class="icon-magnifier" aria-hidden="true"></i></span>
                </div>
            </div>
            <div class="col-12 col-md-5 p-1">
                <select class="form-control p-2 w-100 h-100 contact-search select2" id="master-medias-order">
                    <option value="0">Urutkan berdasarkan</option>
                    <option value="1">Nama file (A-Z)</option>
                    <option value="2">Nama file (Z-A)</option>
                </select>
            </div>
            <div class="col-12 col-md-1 pl-1 pr-1 my-2">
                <button data-toggle="modal" data-target="#modal-create-media" class="btn btn-outline-secondary btn-block"><i class="icon-plus"></i> Media</button>
            </div>
        </div>

        <div class="table-responsive pr-1 pl-1 mb-0">

            <?php

            $this->load->view('components/table', array(
                "idTable" => "master-medias",
                "isCustomThead" => true,
                "thead" => '
                <tr>
                    <th scope="col" rowspan="2">#</th>
                    <th scope="col" rowspan="2">Lihat Gambar</th>
                    <th scope="col" rowspan="2">Nama File</th>
                    <th scope="col" rowspan="2">Tgl dibuat</th>
                    <th scope="col" rowspan="2">Status</th>
                    <th scope="col" colspan="2">Aksi</th>
                </tr>
                <tr>
                    <th>Ubah</th>
                    <th>Hapus</th>
                </tr>
                ',
                "tbody" => ""
            )) ?>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="p-1">
                    <div class="btn-group" role="group" aria-label="Pagination Group">
                        <button class="btn btn-outline-secondary" type="button" id="master-media-prev"><i class="icon-arrow-left-circle"></i> Sebelumnya</button>
                        <button class="btn btn-outline-secondary" type="button" id="master-media-next">Selanjutnya <i class="icon-arrow-right-circle"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>