<div class="card">
    <div class="card-header">
        <h4 class="card-title">Data Masukan</h4>
    </div>
    <div class="card-body">

        <div class="pt-0 pb-0 pl-3 pr-3 mb-3 row">
            <div class="col-12 col-md-5 p-1">
                <select class="form-control p-2 w-100 h-100 contact-search select2" id="master-feedbacks-order">
                    <option value="0">Urutkan berdasarkan</option>
                    <option value="1">Nama (A-Z)</option>
                    <option value="2">Nama (Z-A)</option>
                    <option value="3">Email (A-Z)</option>
                    <option value="4">Email (Z-A)</option>
                </select>
            </div>
        </div>

        <div class="table-responsive pr-1 pl-1 mb-0">

            <?php

            $this->load->view('components/table', array(
                "idTable" => "master-feedbacks",
                "isCustomThead" => true,
                "thead" => '
                <tr>
                    <th scope="col" rowspan="2">#</th>
                    <th scope="col" rowspan="2">Nama</th>
                    <th scope="col" rowspan="2">Email</th>
                    <th scope="col" rowspan="2">Masukan</th>
                    <th scope="col" colspan="2">Aksi</th>
                </tr>
                <tr>
                    <th>Ubah</th>
                    <th>Hapus</th>
                </tr>
                ',
                "tbody" => ""
            )) ?>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="p-1">
                    <div class="btn-group" role="group" aria-label="Pagination Group">
                        <button class="btn btn-outline-secondary" type="button" id="master-feedback-prev"><i class="icon-arrow-left-circle"></i> Sebelumnya</button>
                        <button class="btn btn-outline-secondary" type="button" id="master-feedback-next">Selanjutnya <i class="icon-arrow-right-circle"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>