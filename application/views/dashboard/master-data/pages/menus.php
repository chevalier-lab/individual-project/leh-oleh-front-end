<div class="card">
    <div class="card-header">
        <h4 class="card-title">Data Menu</h4>
    </div>
    <div class="card-body">

        <div class="pt-0 pb-0 pl-3 pr-3 mb-3 row">
            <div class="input-group col-12 col-md-5 p-1">
                <input type="text" class="form-control p-2 w-100 h-100 contact-search" placeholder="Cari ..." id="master-menus-search">
                <div class="input-group-append">
                    <span class="btn btn-outline-primary input-group-text" id="master-menus-button-search"><i class="icon-magnifier" aria-hidden="true"></i></span>
                </div>
            </div>
            <div class="col-12 col-md-5 p-1">
                <select class="form-control p-2 w-100 h-100 contact-search select2" id="master-menus-order">
                    <option value="0">Urutkan berdasarkan</option>
                    <option value="1">Menu (A-Z)</option>
                    <option value="2">Menu (Z-A)</option>
                    <option value="3">Type (primary)</option>
                    <option value="4">Type (secondary)</option>
                    <option value="5">Slug (A-Z)</option>
                    <option value="6">Slug (Z-A)</option>
                    <option value="7">Description (A-Z)</option>
                    <option value="8">Description (Z-A)</option>
                </select>
            </div>
            <div class="col-12 col-md-2 pl-1 pr-1 my-2">
                <button data-toggle="modal" data-target="#modal-create-menu" class="btn btn-outline-secondary btn-block" id="trigger-icon"><i class="icon-plus"></i> Menu</button>
            </div>
        </div>

        <div class="table-responsive pr-1 pl-1 mb-0">

            <?php

            $this->load->view('components/table', array(
                "idTable" => "master-menus",
                "isCustomThead" => true,
                "thead" => '
                <tr>
                    <th scope="col" rowspan="2">#</th>
                    <th scope="col" rowspan="2">Menu</th>
                    <th scope="col" rowspan="2">Slug</th>
                    <th scope="col" rowspan="2">Deskripsi</th>
                    <th scope="col" rowspan="2">URL</th>
                    <th scope="col" rowspan="2">Ikon</th>
                    <th scope="col" rowspan="2">Tipe</th>
                    <th scope="col" rowspan="2">Induk</th>
                    <th scope="col" colspan="2">Aksi</th>
                </tr>
                <tr>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
                ',
                "tbody" => ""
            )) ?>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="p-1">
                    <div class="btn-group" role="group" aria-label="Pagination Group">
                        <button class="btn btn-outline-secondary" type="button" id="master-menu-prev"><i class="icon-arrow-left-circle"></i> Sebelumnya</button>
                        <button class="btn btn-outline-secondary" type="button" id="master-menu-next">Selanjutnya <i class="icon-arrow-right-circle"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>