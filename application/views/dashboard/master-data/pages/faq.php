<div class="card">
    <div class="card-header">
        <h4 class="card-title">Data Pertanyaan yang Sering Diajukan</h4>
    </div>
    <div class="card-body">

        <div class="pt-0 pb-0 pl-3 pr-3 mb-3 row">
            <div class="input-group col-12 col-md-6 p-1">
                <input type="text" class="form-control p-2 w-100 h-100 contact-search" placeholder="Cari ..." id="master-faqs-search">
                <div class="input-group-append">
                    <span class="btn btn-outline-primary input-group-text" id="master-faqs-button-search"><i class="icon-magnifier" aria-hidden="true"></i></span>
                </div>
            </div>
            <div class="col-12 col-md-5 p-1">
                <select class="form-control p-2 w-100 h-100 contact-search select2" id="master-faqs-order">
                    <option value="0">Urutkan berdasarkan</option>
                    <option value="1">Pertanyaan (A-Z)</option>
                    <option value="2">Pertanyaan (Z-A)</option>
                    <option value="3">Tanggal Dibuat</option>
                    <option value="4">Terakhir Diubah</option>
                </select>
            </div>
            <div class="col-12 col-md-1 pl-1 pr-1 my-2">
                <button data-toggle="modal" data-target="#modal-create-faq" class="btn btn-outline-secondary btn-block"><i class="icon-plus"></i> FAQ</button>
            </div>
        </div>

        <div class="table-responsive pr-1 pl-1 mb-0">

            <?php

            $this->load->view('components/table', array(
                "idTable" => "master-faqs",
                "isCustomThead" => true,
                "thead" => '
                <tr>
                    <th scope="col" rowspan="2" class="text-center align-middle">#</th>
                    <th scope="col" rowspan="2" class="text-center align-middle">Pertanyaan</th>
                    <th scope="col" rowspan="2" class="text-center align-middle">Dibuat Oleh</th>
                    <th scope="col" rowspan="2" class="text-center align-middle">Tanggal Dibuat</th>
                    <th scope="col" rowspan="2" class="text-center align-middle">Terakhir Diubah</th>
                    <th scope="col" colspan="3" class="text-center">Aksi</th>
                </tr>
                <tr>
                    <th class="text-center">Lihat</th>
                    <th class="text-center">Ubah</th>
                    <th class="text-center">Hapus</th>
                </tr>
                ',
                "tbody" => ""
            )) ?>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="p-1">
                    <div class="btn-group" role="group" aria-label="Pagination Group">
                        <button class="btn btn-outline-secondary" type="button" id="master-faq-prev"><i class="icon-arrow-left-circle"></i> Sebelumnya</button>
                        <button class="btn btn-outline-secondary" type="button" id="master-faq-next">Selanjutnya <i class="icon-arrow-right-circle"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>