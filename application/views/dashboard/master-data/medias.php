<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('components/head'); ?>

    <!-- START: Template CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.theme.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/simple-line-icons/css/simple-line-icons.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/flags-icon/css/flag-icon.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/select2/css/select2.min.css') ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/select2/css/select2-bootstrap.min.css') ?>" />
    <!-- END Template CSS-->

    <!-- START: Page CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/social-button/bootstrap-social.css'); ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/sweetalert/sweetalert.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/toastr/toastr.min.css'); ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/fancybox/jquery.fancybox.min.css'); ?>" />
    <!-- END: Page CSS-->

    <!-- START: Custom CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/css/main.css'); ?>">
    <!-- END: Custom CSS-->
</head>

<body id="main-container" class="default">
    <input type="hidden" name="base_url" id="base_url" value="<?= base_url("index.php"); ?>">
    <input type="hidden" name="api_url" id="api_url" value="<?= API_URI; ?>">

    <!-- START: Pre Loader-->
    <div class="se-pre-con">
        <div class="loader"></div>
    </div>
    <!-- END: Pre Loader-->

    <!-- START: Template JS-->
    <script src="<?= base_url('assets/dist/vendors/jquery/jquery-3.3.1.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/moment/moment.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/slimscroll/jquery.slimscroll.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/toastr/toastr.min.js'); ?>"></script>
    <!-- END: Template JS-->

    <!-- START: Page Vendor JS-->
    <script src="<?= base_url('assets/dist/js/auth/request.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/fancybox/jquery.fancybox.min.js') ?>"></script>
    <script>
        var base_url = document.getElementById("base_url");
        var api_url = document.getElementById("api_url");
    </script>
    <!-- END: Page Vendor JS-->

    <!-- START: Main Content-->
    <?php

    // Load Navigation Bar
    $this->load->view('components/menus/navbar');

    // Load Sidebar
    $this->load->view('components/menus/sidebar');

    // Load Page
    $this->load->view('dashboard/template', array(
        'page' => "Media",
        'pageMap' => array(
            array(
                "label" => "Data Master",
                "is_current" => false
            ),
            array(
                "label" => "Media",
                "is_current" => true
            ),
        ),
        'pageURI' => "dashboard/master-data/pages/medias",
    ));

    ?>
    <!-- END: Content-->

    <!-- START: Page Script JS-->
    <?php
    if (isset($error)) {
        $this->load->view("components/error-modal", array(
            "errorModalTitle" => $error["title"],
            "errorModalContent" => $error["content"],
            "errorModalDetail" => $error["details"]
        ));
    }

    // Create Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-create-media",
        "modalTitle" => "Menambah Media",
        "iconTitle" => "icon-plus",
        "modalActionForm" => "#",
        "modalContentForm" => '
        <div>
            <img id="create-media-preview" src="" class="img-fluid">
        </div>
        <label for="create-media" 
            class="file-upload btn btn-primary btn-block">
                <i class="fa fa-upload mr-2"></i>Pilih Gambar ...
                <input id="create-media" type="file" required>
        </label>
        ',
        "modalButtonForm" => '<button type="button" class="btn btn-primary add-todo" id="create-media-button">Tambah</button>'
    ));

    // Edit Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-edit-media",
        "modalTitle" => "Mengubah Media",
        "iconTitle" => "icon-pencil",
        "modalActionForm" => "#",
        "modalContentForm" => '
        <input type="hidden" id="id-media" class="form-control"/>
        <div>
            <img id="edit-media-preview" src="" class="img-fluid" alt="Image Not Found" onerror="altImage()">
        </div>
        <label for="edit-media" 
            class="edit-media file-upload btn btn-success btn-block">
                <i class="fa fa-upload mr-2"></i>Pilih Gambar ...
                <input id="edit-media" type="file" required>
        </label>
        ',
        "modalButtonForm" => '
        <button 
        type="button"
        id="edit-media-button"
        class="btn btn-success btn-block dropdown-toggle" 
        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Simpan <span class="icon-paper-plane"></span>
        </button>
        <div class="dropdown-menu p-0" style="">
            <a class="dropdown-item" href="javascript:void(0);"
                onclick="editMedia(1)">Aktif</a>
            <a class="dropdown-item" href="javascript:void(0);"
                onclick="editMedia(2)">Tertunda</a>
            <a class="dropdown-item" href="javascript:void(0);"
                onclick="editMedia(0)">Tidak Aktif</a>
        </div>'
    ));

    // Delete Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-delete-media",
        "modalTitle" => "Menghapus Media",
        "iconTitle" => "icon-trash",
        "modalActionForm" => "#",
        "modalContentForm" => '
        <p>Apakah Anda yakin untuk menghapus media <strong id="delete-media-name"></strong> ?</p>
        ',
        "modalButtonForm" => '<button type="submit" class="btn btn-danger add-todo" id="delete-media">Hapus</button>'
    ));


    ?>
    <!-- END: Page Script JS-->

    <!-- START: APP JS-->
    <script src="<?= base_url('assets/dist/js/app.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/js/home.script.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/js/gallery.script.js'); ?>"></script>

    <script>
        // Initial Pagination
        var page = 0;
        var paginationClicked = false;
        var paginationDirection = "";

        (function($) {
            "use strict";
            $(window).on("load", function() {
                // Load Media First Time
                loadMedias()

                // Handle Order By
                $("#master-medias-order").on("change", function() {
                    loadMedias();
                });

                // Handle Search
                $("#master-medias-search").on("keydown", function(event) {
                    if (event.keyCode == 32 || event.which == 32) loadMedias();
                    else if ($("#master-medias-search").val() == "") loadMedias();
                });

                $("#master-medias-button-search").on("click", function(event) {
                    loadMedias();
                });

                // Handle Pagination
                $("#master-media-prev").on("click", function() {
                    if (page > 0)
                        page -= 1;
                    paginationClicked = true;
                    paginationDirection = "prev";
                    loadMedias();
                });

                $("#master-media-next").on("click", function() {
                    page += 1;
                    paginationClicked = true;
                    paginationDirection = "next";
                    loadMedias();
                });

                // Media Create Preview
                $("#create-media").on("change", function(event) {
                    previewCover(event, "create-media-preview")
                });

                // Media Edit Preview
                $("#edit-media").on("change", function(event) {
                    previewCover(event, "edit-media-preview")
                });

                // Create Media
                $("#create-media-button").on("click", function() {
                    if(!$("#create-media").prop("files")[0]){
                        toastr.warning("Silahkan pilih media terlebih dahulu")
                    } else {
                        $("#create-media-button").attr("disabled", true);

                        var formData = new FormData();
                        formData.append("file-media", $("#create-media").prop("files")[0]);

                        $.ajax({
                            url: base_url.value + "/dashboard/a/masterData/create_medias",
                            data: formData,
                            type: "POST",
                            contentType: false,
                            processData: false,
                            success: function(response) {
                                response = req.data(response)

                                $("#create-media-button").removeAttr("disabled");
                                $("#modal-create-media").modal('hide')

                                if (response.code == 200) {
                                    toastr.success("Sukses menambah media");
                                    // Refresh Table
                                    loadMedias()
                                    $("#create-media-preview").attr("src", '')
                                } else {
                                    toastr.error(response.message);
                                }
                            }
                        });
                    }
                });

                // Handle Delete Media
                $("#delete-media").on("click", function() {
                    $("#delete-media").attr("disabled", true);

                    $.ajax({
                        url: base_url.value + "/dashboard/a/masterData/delete_medias/" + $("#delete-media").val(),
                        data: null,
                        type: "GET",
                        contentType: false,
                        processData: false,
                        success: function(response) {

                            $("#delete-media").removeAttr("disabled");
                            $("#modal-delete-media").modal('hide')

                            response = req.data(response)
                            if (response.code == 200) {
                                loadMedias()
                                toastr.success("Sukses menghapus media");
                            } else {
                                toastr.error(response.message);
                            }
                        }
                    });
                });
            });
        })(jQuery);

        // Handle Cover Start
        function previewCover(event,idElement) {
            var reader = new FileReader();
            reader.onload = function() {
                var output = document.getElementById(idElement);
                output.src = reader.result;
            }
            reader.readAsDataURL(event.target.files[0]);
            $(`#${idElement}`).addClass("mb-3")
        }
        // Handle Cover Stop

        function loadMedias() {
            renderToWaiting()

            var order = $("#master-medias-order").val();
            var order_direction = "DESC";
            switch (Number(order)) {
                case 1:
                    order = "label";
                    order_direction = "ASC";
                    break;
                case 2:
                    order = "label";
                    order_direction = "DESC";
                    break;
                default:
                    order = "id";
                    order_direction = "DESC";
                    break;
            }
            var raw = req.raw({
                page: page,
                search: $("#master-medias-search").val(),
                order_by: order,
                order_direction: order_direction
            })

            var formData = new FormData()
            formData.append("raw", raw)

            $.ajax({
                url: base_url.value + "/dashboard/a/masterData/load_medias",
                data: formData,
                type: "POST",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.code == 200) {
                        renderToTable(response.data)
                    } else {
                        renderToTable([])
                    }
                }
            });
        }

        function renderToWaiting() {
            $("#master-medias tbody").html(`
                <tr>
                    <td class="text-center" colspan="7">Sedang memuat data</td>
                </tr>
            `);
        }

        function renderToTable(data = []) {

            if (paginationClicked) {
                paginationClicked = false;
                if (data.length == 0) {
                    if (paginationDirection == "next")
                        page -= 1;
                    else page += 1;
                }
                paginationDirection = "";
                loadMedias();
                return
            }

            var index = (page * 10) + 1;

            if (data.length == 0) {
                $("#master-medias tbody").html(`
                    <tr>
                        <td class="align-middle" colspan="4">Belum ada data media, 
                        tekan <span class="icon-plus"></span> untuk menambahkan</td>
                    </tr>
                `);
                return;
            }

            $("#master-medias tbody").html(`${data.map(function(item) {
                status = item.is_visible
                isBlock = "";

                if (status == "0") {
                    status = "<span class='badge badge-danger'>Tidak Aktif</span>";
                    isBlock = "disabled";
                }
                else if (status == "1") {
                    status = "<span class='badge badge-primary'>Aktif</span>";
                }
                else {
                    status = "<span class='badge badge-warning'>Tertunda</span>";
                }
                return `
                    <tr>
                        <td class="align-middle">${index++}</td>
                        <td class="align-middle">
                            <a data-fancybox="images" data-caption="${item.label}" href="${item.uri}" class="fancybox btn rounded-0 btn-dark">Lihat</a></td>
                        <td class="align-middle">${item.label}</td>
                        <td class="align-middle">${item.created_at}</td>
                        <td class="align-middle">${status}</td>
                        <td class="align-middle">
                            <h4>
                                <a href="javascript:void(0);" data-toggle="modal" data-target="#modal-edit-media" class="text-success" onclick="loadOneMedia(${item.id})"><i class="icon-pencil"></i></a>
                            </h4>
                        </td>
                        <td class="align-middle">
                            <h4>
                                <a href="javascript:void(0);" data-toggle="modal" data-target="#modal-delete-media" class="text-danger btn ${isBlock}" onclick="deleteMedia(${item.id}, '${item.label}')"><i class="icon-trash"></i></a>
                            </h4>
                        </td>
                    </tr>
                `;
            }).join('')}`);
        }

        function deleteMedia(idMedia, item) {
            $("#delete-media").val(idMedia)
            $("#delete-media-name").text(item)
        }

        function loadOneMedia(idMedia) {
            $("#edit-media-preview").attr("src", base_url.value + '/../assets/dist/images/broken-256.png')
            $("#edit-media-button").attr("disabled", true);
            $("#id-media").val(idMedia)
            $.ajax({
                url: base_url.value + "/dashboard/a/masterData/load_detail_media/" + idMedia,
                data: null,
                type: "GET",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.code == 200) {
                        document.getElementById("edit-media-preview").src = response.data.uri
                        $('#edit-media-preview').addClass("mb-3")
                        $("#edit-media-button").removeAttr("disabled")
                    } else {
                        toastr.error(response.message);
                    }
                }
            });
        }

        function editMedia(is_visible){
            $("#edit-media-button").attr("disabled", true);

            var raw = req.raw({
                is_visible: is_visible == 0 ? "0" : is_visible
            })

            var formData = new FormData()
            formData.append("raw", raw)

            var mediaData = new FormData()
            var media = $("#edit-media")[0].files
            // Update Media
            $.ajax({
                url: base_url.value + "/dashboard/a/masterData/edit_visible_media/" + $('#id-media').val(),
                data: formData,
                type: "POST",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.code == 200) {
                        toastr.success("Sukses mengubah status media");

                        // Update media if exist
                        if(media.length != 0){
                            mediaData.append("media", media[0])
                            $.ajax({
                                url: base_url.value + "/dashboard/a/masterData/edit_media/" + $('#id-media').val(),
                                data: mediaData,
                                type: "POST",
                                contentType: false,
                                processData: false,
                                success: function(response) {
                                    
                                    response = req.data(response)
                                    $("#modal-edit-media").modal("hide")

                                    if (response.code == 200) {
                                        toastr.success("Sukses mengubah media");
                                        // Refresh Table
                                        loadMedias()
                                    } else {
                                        toastr.error("Failed update media");
                                    }
                                    $("#edit-media-button").removeAttr("disabled")
                                }
                            });
                        } else {
                            $("#edit-media-button").removeAttr("disabled")
                            $("#modal-edit-media").modal("hide")
                            loadMedias()
                        }
                    } else {
                        toastr.error(response.message);
                    }
                }
            });
        }
        function altImage(){
            $("#edit-media-preview").attr("src", base_url.value + '/../assets/dist/images/broken-256.png')
        }
    </script>

    <!-- END: APP JS-->

    <!-- START: Page Script JS-->
    <script src="<?= base_url('assets/dist/vendors/select2/js/select2.full.min.js') ?>"></script>
    <script>
        // Select2
        $(".select2").select2({
            theme: 'bootstrap4',
            width: 'style',
            minimumResultsForSearch: Infinity
        });
    </script>
    <!-- END: Page Script JS-->
</body>

</html>