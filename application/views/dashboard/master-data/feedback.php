<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('components/head'); ?>

    <!-- START: Template CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.theme.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/simple-line-icons/css/simple-line-icons.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/flags-icon/css/flag-icon.min.css'); ?>">
    <!-- END Template CSS-->

    <!-- START: Page CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/social-button/bootstrap-social.css'); ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/sweetalert/sweetalert.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/toastr/toastr.min.css'); ?>" />
    <!-- END: Page CSS-->

    <!-- START: Custom CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/css/main.css'); ?>">
    <!-- END: Custom CSS-->
</head>

<body id="main-container" class="default">

    <input type="hidden" name="base_url" id="base_url" value="<?= base_url("index.php"); ?>">
    <input type="hidden" name="api_url" id="api_url" value="<?= API_URI; ?>">

    <!-- START: Pre Loader-->
    <div class="se-pre-con">
        <div class="loader"></div>
    </div>
    <!-- END: Pre Loader-->

    <!-- START: Template JS-->
    <script src="<?= base_url('assets/dist/vendors/jquery/jquery-3.3.1.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/moment/moment.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/slimscroll/jquery.slimscroll.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/toastr/toastr.min.js'); ?>"></script>
    <!-- END: Template JS-->

    <!-- START: Page Vendor JS-->
    <script src="<?= base_url('assets/dist/js/auth/request.js'); ?>"></script>
    <script>
        var base_url = document.getElementById("base_url");
        var api_url = document.getElementById("api_url");
    </script>
    <!-- END: Page Vendor JS-->

    <!-- START: Main Content-->
    <?php

    // Load Navigation Bar
    $this->load->view('components/menus/navbar');

    // Load Sidebar
    $this->load->view('components/menus/sidebar');

    // Load Page
    $this->load->view('dashboard/template', array(
        'page' => "Masukan",
        'pageMap' => array(
            array(
                "label" => "Dasbor",
                "is_current" => false
            ),
            array(
                "label" => "Masukan",
                "is_current" => true
            ),
        ),
        'pageURI' => "dashboard/master-data/pages/feedback",
    ));

    ?>
    <!-- END: Content-->

    <!-- START: Page Script JS-->
    <?php
    if (isset($error)) {
        $this->load->view("components/error-modal", array(
            "errorModalTitle" => $error["title"],
            "errorModalContent" => $error["content"],
            "errorModalDetail" => $error["details"]
        ));
    }

    // Edit Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-edit-feedback",
        "modalTitle" => "Ubah Masukan",
        "iconTitle" => "icon-pencil",
        "modalActionForm" => "#",
        "modalContentForm" => '
        <input type="hidden" id="id-feedback"/>
        <div class="form-group mb-3">
            <label for="edit-name">Nama</label>
            <input type="text" id="edit-name" class="form-control" placeholder="Contoh: Budi" required/> 
        </div>
        <div class="form-group mb-3">
            <label for="edit-email">Email</label>
            <input type="text" id="edit-email" class="form-control" placeholder="Contoh: budi@gmail.com" required/> 
        </div>
        <div class="form-group mb-3">
            <label for="edit-feedback">Masukan</label>
            <input type="text" id="edit-feedback" class="form-control" placeholder="Contoh: Bagus" required/> 
        </div>
        ',
        "modalButtonForm" => '<button type="button" class="btn btn-success add-todo" id="edit-feedback-button">Simpan</button>'
    ));

    // Delete Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-delete-feedback",
        "modalTitle" => "Hapus Masukan",
        "iconTitle" => "icon-trash",
        "modalActionForm" => "#",
        "modalContentForm" => '
        <p>Yakin ingin menghapus masukan ini ?</p>
        ',
        "modalButtonForm" => '<button type="submit" class="btn btn-danger add-todo" id="delete-feedback">Hapus</button>'
    ));


    ?>
    <!-- END: Page Script JS-->

    <!-- START: APP JS-->
    <script src="<?= base_url('assets/dist/js/app.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/js/home.script.js'); ?>"></script>

    <script>
        // Initial Pagination
        var page = 0;
        var paginationClicked = false;
        var paginationDirection = "";

        (function($) {
            "use strict";
            $(window).on("load", function() {
                // Load Feedback First Time
                loadFeedback()

                // Handle Order By
                $("#master-feedbacks-order").on("change", function() {
                    loadFeedback();
                });

                // Handle Pagination
                $("#master-feedback-prev").on("click", function() {
                    if (page > 0)
                        page -= 1;
                    paginationClicked = true;
                    paginationDirection = "prev";
                    loadFeedback();
                });

                $("#master-feedback-next").on("click", function() {
                    page += 1;
                    paginationClicked = true;
                    paginationDirection = "next";
                    loadFeedback();
                });

                // Handle Delete Categories
                $("#delete-feedback").on("click", function() {
                    $("#delete-feedback").attr("disabled", true);

                    $.ajax({
                        url: base_url.value + "/dashboard/a/masterData/delete_feedback/" + $("#delete-feedback").val(),
                        data: null,
                        type: "GET",
                        contentType: false,
                        processData: false,
                        success: function(response) {
                            $("#delete-feedback").removeAttr("disabled");
                            $("#modal-delete-feedback").modal('hide')

                            response = req.data(response)
                            if (response.code == 200) {
                                loadFeedback()
                                toastr.success("Sukses Menghapus Masukan");
                            } else {
                                toastr.error(response.message);
                            }
                        }
                    });
                });

                // Handle Edit Feedback
                $("#edit-feedback-button").on("click", function() {
                    if($("#edit-name").val().trim() == ""){
                        toastr.warning("Silahkan isi nama terlebih dahulu")
                    } else if($("#edit-email").val().trim() == ""){
                        toastr.warning("Silahkan isi email terlebih dahulu")
                    } else if($("#edit-feedback").val().trim() == ""){
                        toastr.warning("Silahkan isi masukan terlebih dahulu")
                    }else {
                        $("#edit-feedback-button").attr("disabled", true);

                        var raw = req.raw({
                            full_name: $("#edit-name").val(),
                            email: $("#edit-email").val(),
                            message: $("#edit-feedback").val(),
                        })
                        var formData = new FormData();
                        formData.append("raw", raw)

                        $.ajax({
                            url: base_url.value + "/dashboard/a/masterData/edit_feedback/" + $("#id-feedback").val(),
                            data: formData,
                            type: "POST",
                            contentType: false,
                            processData: false,
                            success: function(response) {
                                response = req.data(response)

                                $("#edit-feedback-button").removeAttr("disabled");
                                $("#modal-edit-feedback").modal('hide')

                                if (response.code == 200) {
                                    toastr.success("Sukses Mengubah Masukan");
                                    // Refresh Table
                                    loadFeedback()
                                } else {
                                    toastr.error(response.message);
                                }
                            }
                        });
                    }
                });
            });
        })(jQuery);

        function loadFeedback() {
            renderToWaiting()

            var order = $("#master-feedbacks-order").val();
            var order_direction = "DESC";
            switch (Number(order)) {
                case 1:
                    order = "full_name";
                    order_direction = "ASC";
                    break;
                case 2:
                    order = "full_name";
                    order_direction = "DESC";
                    break;
                case 3:
                    order = "email";
                    order_direction = "ASC";
                    break;
                case 4:
                    order = "email";
                    order_direction = "DESC";
                    break;
                default:
                    order = "id";
                    order_direction = "DESC";
                    break;
            }
            var raw = req.raw({
                page: page,
                order_by: order,
                order_direction: order_direction
            })

            var formData = new FormData()
            formData.append("raw", raw)

            $.ajax({
                url: base_url.value + "/dashboard/a/masterData/load_feedback",
                data: formData,
                type: "POST",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.code == 200) {
                        renderToTable(response.data)
                    } else {
                        renderToTable([])
                    }
                }
            });
        }

        function renderToWaiting() {
            $("#master-feedbacks tbody").html(`
                <tr>
                    <td class="text-center" colspan="6">Sedang memuat data</td>
                </tr>
            `);
        }

        function renderToTable(data = []) {

            if (paginationClicked) {
                paginationClicked = false;
                if (data.length == 0) {
                    if (paginationDirection == "next")
                        page -= 1;
                    else page += 1;
                }
                paginationDirection = "";
                loadFeedback();
                return
            }

            var index = (page * 10) + 1;

            if (data.length == 0) {
                $("#master-feedbacks tbody").html(`
                    <tr>
                        <td class="align-middle" colspan="5">Belum ada masukan</td>
                    </tr>
                `);
                return;
            }

            $("#master-feedbacks tbody").html(`${data.map(function(item) {
                return `
                    <tr>
                        <td class="align-middle">${index++}</td>
                        <td class="align-middle">${item.full_name}</td>
                        <td class="align-middle">${item.email}</td>
                        <td class="align-middle">${item.message}</td>
                        <td class="align-middle">
                            <h4>
                                <a href="javascript:void(0)" data-toggle="modal" data-target="#modal-edit-feedback" class="text-success"><i class="icon-pencil" onclick="loadOneFeedback(${item.id})"></i></a>
                            </h4>
                        </td>
                        <td class="align-middle">
                            <h4>
                                <a href="javascript:void(0)" data-toggle="modal" data-target="#modal-delete-feedback" class="btn text-danger" onclick="deleteFeedback(${item.id})"><i class="icon-trash"></i></a>
                            </h4>
                        </td>
                    </tr>
                `;
            }).join('')}`);
        }

        function deleteFeedback(idFeedback) {
            $("#delete-feedback").val(idFeedback)
        }

        function loadOneFeedback(idFeedback) {
            document.querySelectorAll('form').forEach(el => el.reset())
            $("#id-feedback").val(idFeedback)
            $("#edit-feedback-button").attr("disabled", true);
            $.ajax({
                url: base_url.value + "/dashboard/a/masterData/load_detail_feedback/" + idFeedback,
                data: null,
                type: "GET",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.code == 200) {
                        $("#edit-name").val(response.data.full_name)
                        $("#edit-email").val(response.data.email)
                        $("#edit-feedback").val(response.data.message)
                        $("#edit-feedback-button").removeAttr("disabled");
                    } else {
                        toastr.error(response.message);
                    }
                }
            });
        }
    </script>
    <!-- END: APP JS-->
</body>

</html>