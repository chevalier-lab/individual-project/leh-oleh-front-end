<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('components/head'); ?>

    <!-- START: Template CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.theme.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/simple-line-icons/css/simple-line-icons.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/flags-icon/css/flag-icon.min.css'); ?>">
    <!-- END Template CSS-->

    <!-- START: Page CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/social-button/bootstrap-social.css'); ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/sweetalert/sweetalert.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/select2/css/select2.min.css') ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/select2/css/select2-bootstrap.min.css') ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/toastr/toastr.min.css'); ?>"/>
    <!-- END: Page CSS-->

    <!-- START: Custom CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/css/main.css'); ?>">
    <!-- END: Custom CSS-->
</head>

<body id="main-container" class="default">

    <input type="hidden" name="base_url" id="base_url" value="<?= base_url("index.php"); ?>">
    <input type="hidden" name="api_url" id="api_url" value="<?= API_URI; ?>">

    <!-- START: Pre Loader-->
    <div class="se-pre-con">
        <div class="loader"></div>
    </div>
    <!-- END: Pre Loader-->

    <!-- START: Template JS-->
    <script src="<?= base_url('assets/dist/vendors/jquery/jquery-3.3.1.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/moment/moment.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/slimscroll/jquery.slimscroll.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/toastr/toastr.min.js'); ?>"></script>
    <!-- END: Template JS-->

    <!-- START: Page Vendor JS-->
    <script src="<?= base_url('assets/dist/js/auth/request.js'); ?>"></script>
    <script>
        var base_url = document.getElementById("base_url");
        var api_url = document.getElementById("api_url");
    </script>
    <!-- END: Page Vendor JS-->

    <!-- START: Main Content-->
    <?php

    // Load Navigation Bar
    $this->load->view('components/menus/navbar');

    // Load Sidebar
    $this->load->view('components/menus/sidebar');

    // Load Page
    $this->load->view('dashboard/template', array(
        'page' => "Menu",
        'pageMap' => array(
            array(
                "label" => "Data Master",
                "is_current" => false
            ),
            array(
                "label" => "Menu",
                "is_current" => true
            ),
        ),
        'pageURI' => "dashboard/master-data/pages/menus",
    ));

    ?>
    <!-- END: Content-->

    <!-- START: Page Script JS-->
    <?php
    if (isset($error)) {
        $this->load->view("components/error-modal", array(
            "errorModalTitle" => $error["title"],
            "errorModalContent" => $error["content"],
            "errorModalDetail" => $error["details"]
        ));
    }

    // Create Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-create-menu",
        "modalTitle" => "Membuat Menu",
        "iconTitle" => "icon-plus",
        "modalType" => "modal-lg",
        "modalActionForm" => "#",
        "modalContentForm" => '
        <div class="row">
            <div class="col-md-6">
                <div class="form-group mb-3">
                    <label for="create-menu">Menu</label>
                    <input type="text" id="create-menu" class="form-control" placeholder="Contoh: Kontak Kami"/> 
                </div>
                <div class="form-group mb-3">
                    <label for="create-description">Deskripsi</label>
                    <input type="text" id="create-description" class="form-control" placeholder="Contoh: Menu menuju halaman kontak kami"/> 
                </div>
                <div class="form-group mb-3">
                    <label for="create-url">URL</label>
                    <input type="text" id="create-url" class="form-control" placeholder="Contoh: https://leholeh.co.id/contact-us"/> 
                </div>
                <div class="form-group mb-3">
                    <label for="create-type">Tipe</label>
                    <select id="create-type" name="create-type" class="form-control select2">
                        <option selected disabled>Pilih Tipe</option>
                        <option value="0">Utama</option>
                        <option value="1">Sekunder</option>
                    </select>
                </div>
                <div class="form-group mb-3">
                    <label for="create-parent">Induk</label>
                    <select id="create-parent" name="create-parent" class="list-parent form-control select2">
                        
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <label>Ikon</label>
                <div class="input-group col-12 p-1">
                    <input type="text" class="form-control p-2 w-100 h-100 contact-search" placeholder="Cari ..." id="icon-search-create">
                    <div class="input-group-append">
                        <span class="btn btn-outline-primary input-group-text" id="icon-button-search-create"><i class="icon-magnifier" aria-hidden="true"></i></span>
                    </div>
                </div>
                <div class="icon-container card-body text-center row" style="width: 100%; max-height:300px; overflow: scroll;overflow-x: hidden;">

                </div>
            </div>
        </div>
        ',
        "modalButtonForm" => '<button type="button" class="btn btn-primary add-todo" id="create-menu-button">Buat Menu</button>'
    ));

    // Edit Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-edit-menu",
        "modalTitle" => "Mengubah Menu",
        "iconTitle" => "icon-pencil",
        "modalType" => "modal-lg",
        "modalActionForm" => "#",
        "modalContentForm" => '
        <input type="hidden" id="id-menu"/>
        <input type="hidden" id="id-icon"/>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group mb-3">
                    <label for="edit-menu">Menu</label>
                    <input type="text" id="edit-menu" class="form-control" placeholder="Contoh: Kontak Kami"/> 
                </div>
                <div class="form-group mb-3">
                    <label for="edit-description">Deskripsi</label>
                    <input type="text" id="edit-description" class="form-control" placeholder="Contoh: Menu menuju halaman kontak kami"/> 
                </div>
                <div class="form-group mb-3">
                    <label for="edit-url">URL</label>
                    <input type="text" id="edit-url" class="form-control" placeholder="Contoh: https://leholeh.co.id/contact-us"/> 
                </div>
                <div class="form-group mb-3">
                    <label for="edit-type">Tipe</label>
                    <select id="edit-type" name="edit-type" class="form-control select2">
                        <option disabled selected>Pilih Tipe</option>
                        <option value="0">Utama</option>
                        <option value="1">Sekunder</option>
                    </select>
                </div>
                <div class="form-group mb-3">
                    <label for="edit-parent">Induk</label>
                    <select id="edit-parent" name="edit-parent" class="list-parent form-control select2">
                        
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <label>Ikon</label>
                <div class="input-group col-12 p-1">
                    <input type="text" class="form-control p-2 w-100 h-100 contact-search" placeholder="Cari ..." id="icon-search-edit">
                    <div class="input-group-append">
                        <span class="btn btn-outline-primary input-group-text" id="icon-button-search-edit"><i class="icon-magnifier" aria-hidden="true"></i></span>
                    </div>
                </div>
                <div class="icon-container card-body text-center row mt-1" style="width: 100%; max-height:300px; overflow: scroll; overflow-x: hidden;">

                </div>
            </div>
        </div>
        ',
        "modalButtonForm" => '<button type="button" class="btn btn-success add-todo" id="edit-menu-button">Simpan</button>'
    ));

    // Delete Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-delete-menu",
        "modalTitle" => "Delete Menu",
        "iconTitle" => "icon-trash",
        "modalActionForm" => "#",
        "modalContentForm" => '
        <p>Apakah Anda yakin untuk menghapus menu <strong id="delete-menu-name"></strong> ?</p>
        ',
        "modalButtonForm" => '<button type="submit" class="btn btn-danger add-todo" id="delete-menu">Hapus</button>'
    ));


    ?>
    <!-- END: Page Script JS-->

    <!-- START: APP JS-->
    <script src="<?= base_url('assets/dist/js/app.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/js/home.script.js'); ?>"></script>

    <script>
        // Select Icon
        var selected = 0;
        $('.icon-container').on('click', '.add-icon', function() {
            if (selected == 0) {
                selected = 1
                $(this).removeClass("btn-outline-dark").addClass("btn-success disabled")
                $(this).attr('id', 'selected');
            } else {
                var element = document.getElementById('selected')
                element.removeAttribute('id')
                element.classList.remove('btn-success')
                element.classList.remove('disabled')
                element.classList.add('btn-outline-dark')
                $(this).removeClass("btn-outline-dark").addClass("btn-success disabled")
                $(this).attr('id', 'selected');
            }
        })
        
        var searchIcon = ""
        var classIconClicked = ""

        // Initial Pagination
        var page = 0;
        var paginationClicked = false;
        var paginationDirection = "";

        (function($) {
            "use strict";
            $(window).on("load", function() {
                // Load Menus First Time
                loadMenus()

                // Handle Order By
                $("#master-menus-order").on("change", function() {
                    loadMenus();
                });

                // Handle Search Icon
                $("#icon-button-search-create").on("click", function() {
                    searchIcon = $('#icon-search-create').val();
                    loadIcons()
                });

                // Handle Search Icon
                $("#icon-button-search-edit").on("click", function() {
                    searchIcon = $('#icon-search-edit').val();
                    loadIcons()
                });

                // Load Icon
                $("#trigger-icon").on("click", function() {
                    searchIcon = $('#icon-search-edit').val();
                    loadIcons()
                });

                // Handle Search
                $("#master-menus-search").on("keydown", function(event) {
                    if (event.keyCode == 32 || event.which == 32) loadMenus();
                    else if ($("#master-menus-search").val() == "") loadMenus();
                });

                $("#master-menus-button-search").on("click", function(event) {
                    loadMenus();
                });

                // Handle Pagination
                $("#master-menu-prev").on("click", function() {
                    if (page > 0)
                        page -= 1;
                    paginationClicked = true;
                    paginationDirection = "prev";
                    loadMenus();
                });

                $("#master-menu-next").on("click", function() {
                    page += 1;
                    paginationClicked = true;
                    paginationDirection = "next";
                    loadMenus();
                });

                // Create Menu
                $("#create-menu-button").on("click", function() {
                    if($("#create-menu").val().trim() == ""){
                        toastr.warning("Silahkan isi nama menu terlebih dahulu")
                    }else if($("#create-description").val().trim() == ""){
                        toastr.warning("Silahkan isi deskripsi menu terlebih dahulu")
                    }else if($("#create-url").val().trim() == ""){
                        toastr.warning("Silahkan isi URL terlebih dahulu")
                    }else if(!document.getElementById('selected')){
                        toastr.warning("Silahkan pilih ikon terlebih dahulu")
                    }else if(!$("#create-type").val()){
                        toastr.warning("Silahkan pilih tipe terlebih dahulu")
                    }else {
                        $("#create-menu-button").attr("disabled", true);

                        var raw = req.raw({
                            menu: $("#create-menu").val(),
                            slug: $("#create-menu").val().replace(/\s+/g, '-').toLowerCase(),
                            description: $("#create-description").val(),
                            url: $("#create-url").val(),
                            id_icon: document.getElementById('selected').children[0].children[0].classList[0],
                            type: $("#create-type").val() == 0 ? "0" : $("#create-type").val(),
                            parent: $("#create-parent").val(),
                        })
                        var formData = new FormData();
                        formData.append("raw", raw)

                        $.ajax({
                            url: base_url.value + "/dashboard/a/masterData/create_menus",
                            data: formData,
                            type: "POST",
                            contentType: false,
                            processData: false,
                            success: function(response) {
                                response = req.data(response)

                                $("#create-menu-button").removeAttr("disabled");
                                $("#modal-create-menu").modal('hide')

                                if (response.code == 200) {
                                    toastr.success("Berhasil Menambah Menu");
                                    // Refresh Table
                                    loadMenus()
                                    document.querySelectorAll('form').forEach(el => el.reset())
                                } else {
                                    toastr.error(response.message);
                                }
                            }
                        });
                    }
                });

                // Edit Menu
                $("#edit-menu-button").on("click", function() {
                    if($("#edit-menu").val().trim() == ""){
                        toastr.warning("Silahkan isi nama menu terlebih dahulu")
                    }else if($("#edit-description").val().trim() == ""){
                        toastr.warning("Silahkan isi deskripsi menu terlebih dahulu")
                    }else if($("#edit-url").val().trim() == ""){
                        toastr.warning("Silahkan isi URL terlebih dahulu")
                    }else if(!$("#edit-type").val()){
                        toastr.warning("Silahkan pilih tipe terlebih dahulu")
                    }else {
                        $("#edit-menu-button").attr("disabled", true);

                        var raw = req.raw({
                            menu: $("#edit-menu").val(),
                            slug: $("#edit-menu").val().replace(/\s+/g, '-').toLowerCase(),
                            description: $("#edit-description").val(),
                            url: $("#edit-url").val(),
                            id_icon: document.getElementById('selected') ? document.getElementById('selected').children[0].children[0].classList[0] : $("#id-icon").val(),
                            type: $("#edit-type").val() == 0 ? "0" : $("#edit-type").val(),
                            parent: $("#edit-parent").val() == 0 ? "null" : $("#edit-parent").val(),
                        })
                        var formData = new FormData();
                        formData.append("raw", raw)

                        $.ajax({
                            url: base_url.value + "/dashboard/a/masterData/edit_menu/" + $("#id-menu").val(),
                            data: formData,
                            type: "POST",
                            contentType: false,
                            processData: false,
                            success: function(response) {
                                response = req.data(response)

                                $("#edit-menu-button").removeAttr("disabled");
                                $("#modal-edit-menu").modal('hide')

                                if (response.code == 200) {
                                    toastr.success("Berhasil Mengubah Menu");
                                    // Refresh Table
                                    loadMenus()
                                    $('#icon-search-edit').val("")
                                } else {
                                    toastr.error(response.message);
                                }
                            }
                        });
                    }
                });

                // Handle Delete Menus
                $("#delete-menu").on("click", function() {
                    $("#delete-menu").attr("disabled", true);

                    $.ajax({
                        url: base_url.value + "/dashboard/a/masterData/delete_menus/" + $("#delete-menu").val(),
                        data: null,
                        type: "GET",
                        contentType: false,
                        processData: false,
                        success: function(response) {

                            $("#delete-menu").removeAttr("disabled");
                            $("#modal-delete-menu").modal('hide')

                            response = req.data(response)
                            if (response.code == 200) {
                                loadMenus()
                                toastr.success("Berhasil Menghapus Menu");
                            } else {
                                toastr.error(response.message);
                            }
                        }
                    });
                });
            });
        })(jQuery);

        function loadMenus() {
            renderToWaiting()

            var order = $("#master-menus-order").val();
            var order_direction = "DESC";
            switch (Number(order)) {
                case 1:
                    order = "menu";
                    order_direction = "ASC";
                    break;
                case 2:
                    order = "menu";
                    order_direction = "DESC";
                    break;
                case 3:
                    order = "type";
                    order_direction = "ASC";
                    break;
                case 4:
                    order = "type";
                    order_direction = "DESC";
                    break;
                case 5:
                    order = "slug";
                    order_direction = "ASC";
                    break;
                case 6:
                    order = "slug";
                    order_direction = "DESC";
                    break;
                case 7:
                    order = "description";
                    order_direction = "ASC";
                    break;
                case 8:
                    order = "description";
                    order_direction = "DESC";
                    break;
                default:
                    order = "id";
                    order_direction = "DESC";
                    break;
            }
            var raw = req.raw({
                page: page,
                search: $("#master-menus-search").val(),
                order_by: order,
                order_direction: order_direction
            })

            var formData = new FormData()
            formData.append("raw", raw)

            $.ajax({
                url: base_url.value + "/dashboard/a/masterData/load_menus",
                data: formData,
                type: "POST",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.code == 200) {
                        renderToTable(response.data)
                        renderListMenu(response.data)
                    } else {
                        renderToTable([])
                    }
                }
            });
        }

        function loadIcons() {
            selected = 0
            $(".icon-container").html('Sedang memuat data ikon')
            var raw = req.raw({
                page: -1,
                search: searchIcon,
                order_by: "icon",
                order_direction: "DESC"
            });
            var formData = new FormData();
            formData.append("raw", raw);

            $.ajax({
                url: base_url.value + "/dashboard/a/masterData/load_icons",
                data: formData,
                type: "POST",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.code == 200) {
                        renderListIcon(response.data)
                    } else {
                        renderListIcon([])
                    }
                }
            });
            searchIcon = ""
        }

        function renderListIcon(data = []) {
            if(data.length == 0) {
                $(".icon-container").html('Tidak ada data ikon')
            } else {
                $(".icon-container").html(`${data.map(function(item) {
                    if(item.is_visible == 1){
                        if(item.icon == classIconClicked)
                            $("#id-icon").val(item.id)
                        return `
                        <div class="add-icon col font-icon-list p-2 border mx-1 mb-2 btn btn-outline-dark">
                            <div class="preview">
                                <i class="${item.id} ${item.icon} icons"></i> ${item.icon}   
                            </div>
                        </div>`
                    }
                }).join('')}`);
            }
            if(classIconClicked != ""){
                $(`.${classIconClicked}`).parent().parent().trigger("click")
            }
            classIconClicked = ""
        }

        function renderListMenu(data = []) {
            $(".list-parent").html(`<option selected value="0">Tidak ada induk</option>
                ${data.map(function(item) {
                return `
                    <option value="${item.id}">${item.menu}</option>
                `;
            }).join('')}`);
        }

        function renderToWaiting() {
            $("#master-menus tbody").html(`
                <tr>
                    <td class="align-middle" colspan="10">Sedang memuat data</td>
                </tr>
            `);
        }

        function renderToTable(data = []) {

            if (paginationClicked) {
                paginationClicked = false;
                if (data.length == 0) {
                    if (paginationDirection == "next")
                        page -= 1;
                    else page += 1;
                }
                paginationDirection = "";
                loadMenus();
                return
            }

            var index = (page * 10) + 1;

            if (data.length == 0) {
                $("#master-menus tbody").html(`
                    <tr>
                        <td class="align-middle" colspan="10">Belum ada data menu, 
                        tekan <span class="icon-plus"></span> untuk menambahkan</td>
                    </tr>
                `);
                return;
            }

            $("#master-menus tbody").html(`${data.map(function(item) {
                type = item.type

                if (type == 0) {
                    type = "<span class='badge badge-primary'>Utama</span>";
                }
                else {
                    type = "<span class='badge badge-info'>Sekunder</span>";
                }
                return `
                    <tr>
                        <td class="align-middle">${index++}</td>
                        <td class="align-middle">${item.menu}</td>
                        <td class="align-middle">${item.slug}</td>
                        <td class="align-middle">${item.description}</td>
                        <td class="align-middle">${item.url}</td>
                        <td class="align-middle"><h4><i class="icon ${item.icon}"></h4></td>
                        <td class="align-middle">${type}</td>
                        <td class="align-middle">${item.parent}</td>
                        <td class="align-middle">
                            <h4>
                                <a href="javascript:void(0);" data-toggle="modal" data-target="#modal-edit-menu" class="text-success" onclick="loadOneMenu(${item.id})"><i class="icon-pencil"></i></a>
                            </h4>
                        </td>
                        <td class="align-middle">
                            <h4>
                                <a href="javascript:void(0);" data-toggle="modal" data-target="#modal-delete-menu" class="text-danger" onclick="deleteMenu(${item.id}, '${item.menu}')"><i class="icon-trash"></i></a>
                            </h4>
                        </td>
                    </tr>
                `;
            }).join('')}`);
        }

        function deleteMenu(idMenu, item) {
            $("#delete-menu").val(idMenu)
            $("#delete-menu-name").text(item)
        }

        function loadOneMenu(idMenu) {
            $("#id-menu").val(idMenu)
            $(".icon-container").html('Sedang memuat data ikon')
            $("#edit-menu-button").attr("disabled", true);
            document.querySelectorAll('form').forEach(el => el.reset())
            $.ajax({
                url: base_url.value + "/dashboard/a/masterData/load_detail_menu/" + idMenu,
                data: null,
                type: "GET",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.code == 200) {
                        classIconClicked = response.data.icon
                        loadIcons()
                        // $(`.${classIconClicked}`).parent().parent().trigger("click")
                        $("#edit-menu").val(response.data.menu)
                        $("#edit-description").val(response.data.description)
                        $("#edit-url").val(response.data.description)
                        $("#edit-type").val(response.data.type).change()
                        $("#edit-parent").val(response.data.parent).change()
                        $("#edit-menu-button").removeAttr("disabled");
                    } else {
                        toastr.error(response.message);
                    }
                }
            });
        }
    </script>
    <!-- END: APP JS-->

    <!-- START: Page Script JS-->
    <script src="<?= base_url('assets/dist/vendors/select2/js/select2.full.min.js') ?>"></script>
    <script>
        // Select2
        $(".select2").select2({
            theme: 'bootstrap4',
            width: 'style',
            minimumResultsForSearch: Infinity
        });
    </script>
    <!-- END: Page Script JS-->
</body>

</html>