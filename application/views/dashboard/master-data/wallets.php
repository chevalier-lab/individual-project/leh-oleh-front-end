<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('components/head'); ?>

    <!-- START: Template CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.theme.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/simple-line-icons/css/simple-line-icons.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/flags-icon/css/flag-icon.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/select2/css/select2.min.css') ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/select2/css/select2-bootstrap.min.css') ?>" />
    <!-- END Template CSS-->

    <!-- START: Page CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/social-button/bootstrap-social.css'); ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/sweetalert/sweetalert.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/toastr/toastr.min.css'); ?>"/>

    <!-- END: Page CSS-->

    <!-- START: Custom CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/css/main.css'); ?>">
    <!-- END: Custom CSS-->
</head>

<body id="main-container" class="default">
    <input type="hidden" name="base_url" id="base_url" value="<?= base_url("index.php"); ?>">
    <input type="hidden" name="api_url" id="api_url" value="<?= API_URI; ?>">

    <!-- START: Pre Loader-->
    <div class="se-pre-con">
        <div class="loader"></div>
    </div>
    <!-- END: Pre Loader-->

    <!-- START: Template JS-->
    <script src="<?= base_url('assets/dist/vendors/jquery/jquery-3.3.1.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/moment/moment.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/slimscroll/jquery.slimscroll.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/toastr/toastr.min.js'); ?>"></script>
    <script src="https://cdn.jsdelivr.net/npm/lozad/dist/lozad.min.js"></script>
    <!-- END: Template JS-->

    <!-- START: Page Vendor JS-->
    <script src="<?= base_url('assets/dist/js/auth/request.js'); ?>"></script>
    <script>
        var base_url = document.getElementById("base_url");
        var api_url = document.getElementById("api_url");
    </script>
    <!-- END: Page Vendor JS-->

    <!-- START: Main Content-->
    <?php

    // Load Navigation Bar
    $this->load->view('components/menus/navbar');

    // Load Sidebar
    $this->load->view('components/menus/sidebar');

    // Load Page
    $this->load->view('dashboard/template', array(
        'page' => "Dompet",
        'pageMap' => array(
            array(
                "label" => "Data Master",
                "is_current" => false
            ),
            array(
                "label" => "Dompet",
                "is_current" => true
            ),
        ),
        'pageURI' => "dashboard/master-data/pages/wallets",
    ));

    ?>
    <!-- END: Content-->

    <!-- START: Page Script JS-->
    <?php
    if (isset($error)) {
        $this->load->view("components/error-modal", array(
            "errorModalTitle" => $error["title"],
            "errorModalContent" => $error["content"],
            "errorModalDetail" => $error["details"]
        ));
    }

    // Create Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-create-wallet",
        "modalTitle" => "Membuat Dompet",
        "iconTitle" => "icon-plus",
        "modalActionForm" => "#",
        "modalContentForm" => '
        <div class="form-group mb-3">
            <label for="create-name">Nama</label>
            <input type="text" id="create-name" class="form-control" placeholder="Contoh: Leh Oleh Wallet"/> 
        </div>
        <div class="form-group mb-3">
            <label for="create-description">Deskripsi</label>
            <input type="text" id="create-description" class="form-control" placeholder="Contoh: Dompet virtual milik leh oleh"/> 
        </div>
        <div class="form-group mb-3">
            <label>Logo Dompet</label>
            <div>
                <img id="create-logo-preview" src="" class="img-fluid" width="200">
            </div>
            <label for="create-logo-wallet" 
                class="file-upload btn btn-primary btn-block">
                    <i class="fa fa-upload mr-2"></i>Pilih logo ...
                    <input id="create-logo-wallet" type="file" required>
            </label>
        </div>
        ',
        "modalButtonForm" => '<button type="submit" class="btn btn-primary add-todo" id="create-wallet-button">Buat Dompet</button>'
    ));

    // Edit Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-edit-wallet",
        "modalTitle" => "Mengubah Dompet",
        "iconTitle" => "icon-pencil",
        "modalActionForm" => "#",
        "modalContentForm" => '
        <div class="form-group mb-3">
            <input type="hidden" id="id-wallet"/>
            <label>Logo Dompet</label>
            <div>
                <img id="edit-logo-preview" src="" class="img-fluid" width="200">
            </div>
            <label for="edit-logo-wallet" 
                class="file-upload btn btn-success btn-block">
                    <i class="fa fa-upload mr-2"></i>Pilih logo ...
                    <input id="edit-logo-wallet" type="file" required>
            </label>
        </div>
        <div class="form-group mb-3">
            <label for="edit-name">Nama</label>
            <input type="text" id="edit-name" class="form-control" placeholder="Contoh: Leh Oleh Wallet"/> 
        </div>
        <div class="form-group mb-3">
            <label for="edit-description">Deskripsi</label>
            <input type="text" id="edit-description" class="form-control" placeholder="Contoh: Dompet virtual milik leh oleh"/> 
        </div>
        ',
        "modalButtonForm" => '
        <button 
        type="button"
        id="edit-wallet"
        class="btn btn-success btn-block dropdown-toggle" 
        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Simpan <span class="icon-paper-plane"></span>
        </button>
        <div class="dropdown-menu p-0" style="">
            <a class="dropdown-item" href="javascript:void(0);"
                onclick="editWallet(1)">Aktif</a>
            <a class="dropdown-item" href="javascript:void(0);"
                onclick="editWallet(2)">Tertunda</a>
            <a class="dropdown-item" href="javascript:void(0);"
                onclick="editWallet(0)">Tidak Aktif</a>
        </div>'
    ));

    // Delete Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-delete-wallet",
        "modalTitle" => "Menghapus Dompet",
        "iconTitle" => "icon-trash",
        "modalActionForm" => "#",
        "modalContentForm" => '
        <p>Apakah Anda yakin akan menghapus dompet <strong id="delete-wallet-name"></strong> ?</p>
        ',
        "modalButtonForm" => '<button type="submit" class="btn btn-danger add-todo" id="delete-wallet">Hapus</button>'
    ));


    ?>
    <!-- END: Page Script JS-->

    <!-- START: APP JS-->
    <script src="<?= base_url('assets/dist/js/app.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/js/home.script.js'); ?>"></script>

    <script>
        // Initial Pagination
        var page = 0;
        var paginationClicked = false;
        var paginationDirection = "";

        (function($) {
            "use strict";
            $(window).on("load", function() {
                // Load Wallets First Time
                loadWallets()

                // Handle Order By
                $("#master-wallets-order").on("change", function() {
                    loadWallets();
                });

                // Handle Search
                $("#master-wallets-search").on("keydown", function(event) {
                    if (event.keyCode == 32 || event.which == 32) loadWallets();
                    else if ($("#master-wallets-search").val() == "") loadWallets();
                });

                $("#master-wallets-button-search").on("click", function(event) {
                    loadWallets();
                });

                // Handle Pagination
                $("#master-wallet-prev").on("click", function() {
                    if (page > 0)
                        page -= 1;
                    paginationClicked = true;
                    paginationDirection = "prev";
                    loadWallets();
                });

                $("#master-wallet-next").on("click", function() {
                    page += 1;
                    paginationClicked = true;
                    paginationDirection = "next";
                    loadWallets();
                });

                // Logo Create Preview
                $("#create-logo-wallet").on("change", function(event) {
                    previewCover(event, "create-logo-preview")
                });

                // Logo Edit Preview
                $("#edit-logo-wallet").on("change", function(event) {
                    previewCover(event, "edit-logo-preview")
                });

                // Create Wallet
                $("#create-wallet-button").on("click", function() {
                    if($("#create-name").val().trim() == "") {
                        toastr.warning("Silahkan isi nama dompet terlebih dahulu")
                    } else if($("#create-description").val().trim() == ""){
                        toastr.warning("Silahkan isi deskripsi dompet terlebih dahulu")
                    } else if(!$("#create-logo-wallet").prop("files")[0]){
                        toastr.warning("Silahkan pilih logo dompet terlebih dahulu")
                    } else {
                        $("#create-wallet-button").attr("disabled", true);

                        var formData = new FormData();
                        formData.append("name", $("#create-name").val());
                        formData.append("description", $("#create-description").val());
                        formData.append("logo", $("#create-logo-wallet").prop("files")[0]);

                        $.ajax({
                            url: base_url.value + "/dashboard/a/masterData/create_wallets",
                            data: formData,
                            type: "POST",
                            contentType: false,
                            processData: false,
                            success: function(response) {
                                response = req.data(response)

                                $("#create-wallet-button").removeAttr("disabled");
                                $("#modal-create-wallet").modal('hide')

                                if (response.code == 200) {
                                    toastr.success("Sukses menambah dompet");
                                    // Refresh Table
                                    loadWallets()
                                    document.querySelectorAll('form').forEach(el => el.reset())
                                    $("#create-logo-preview").attr("src", '')
                                } else {
                                    toastr.error(response.message);
                                }
                            }
                        });
                    }
                });

                // Handle Delete Wallet
                $("#delete-wallet").on("click", function() {
                    $("#delete-wallet").attr("disabled", true);

                    $.ajax({
                        url: base_url.value + "/dashboard/a/masterData/delete_wallet/" + $("#delete-wallet").val(),
                        data: null,
                        type: "GET",
                        contentType: false,
                        processData: false,
                        success: function(response) {
                            response = req.data(response)

                            $("#delete-wallet").removeAttr("disabled");
                            $("#modal-delete-wallet").modal('hide')

                            if (response.code == 200) {
                                loadWallets()
                                toastr.success("Sukses menghapus dompet");
                            } else {
                                toastr.error(response.message);
                            }
                        }
                    });
                });
            });
        })(jQuery);

        // Handle Cover Start
        function previewCover(event,idElement) {
            var reader = new FileReader();
            reader.onload = function() {
                var output = document.getElementById(idElement);
                output.src = reader.result;
            }
            reader.readAsDataURL(event.target.files[0]);
            $(`#${idElement}`).addClass("mb-3")
        }
        // Handle Cover Stop

        function loadWallets() {
            renderToWaiting()

            var order = $("#master-wallets-order").val();
            var order_direction = "DESC";
            switch (Number(order)) {
                case 1:
                    order = "wallet_name";
                    order_direction = "ASC";
                    break;
                case 2:
                    order = "wallet_name";
                    order_direction = "DESC";
                    break;
                case 3:
                    order = "wallet_description";
                    order_direction = "ASC";
                    break;
                case 4:
                    order = "wallet_description";
                    order_direction = "DESC";
                    break;
                default:
                    order = "id";
                    order_direction = "DESC";
                    break;
            }
            var raw = req.raw({
                page: page,
                search: $("#master-wallets-search").val(),
                order_by: order,
                order_direction: order_direction
            })

            var formData = new FormData()
            formData.append("raw", raw)

            $.ajax({
                url: base_url.value + "/dashboard/a/masterData/load_wallets",
                data: formData,
                type: "POST",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.code == 200) {
                        renderToTable(response.data)
                    } else {
                        renderToTable([])
                    }
                }
            });
        }

        function renderToWaiting() {
            $("#master-wallets tbody").html(`
                <tr>
                    <td class="align-middle" colspan="4">Sedang memuat data</td>
                </tr>
            `);
        }

        function renderToTable(data = []) {

            if (paginationClicked) {
                paginationClicked = false;
                if (data.length == 0) {
                    if (paginationDirection == "next")
                        page -= 1;
                    else page += 1;
                }
                paginationDirection = "";
                loadWallets();
                return
            }

            var index = (page * 10) + 1;

            if (data.length == 0) {
                $("#master-wallets tbody").html(`
                    <tr>
                        <td class="align-middle" colspan="4">Belum ada data wallet, 
                        tekan <span class="icon-plus"></span> untuk menambahkan</td>
                    </tr>
                `);
                return;
            }

            $("#master-wallets tbody").html(`${data.map(function(item) {
                status = item.is_visible
                isBlock = "";

                if (status == "0") {
                    status = "<span class='badge badge-danger'>Tidak Aktif</span>";
                    isBlock = "disabled";
                }
                else if (status == "1") {
                    status = "<span class='badge badge-primary'>Aktif</span>";
                }
                else {
                    status = "<span class='badge badge-warning'>Tertunda</span>";
                }
                return `
                    <tr>
                        <td class="align-middle">${index++}</td>
                        <td class="align-middle"><img data-src="${item.uri}" class="img-fluid img-thumbnail lozad" 
                        src="${base_url.value + '/../assets/dist/images/broken-256.png'}"
                        width="100"></td>
                        <td class="align-middle">${item.wallet_name}</td>
                        <td class="align-middle">${item.wallet_description}</td>
                        <td class="align-middle">${item.updated_at}</td>
                        <td class="align-middle">${status}</td>
                        <td class="align-middle">
                            <h4>
                                <a href="javascript:void(0);" data-toggle="modal" data-target="#modal-edit-wallet" class="text-success" onclick="loadOneWallet(${item.id})"><i class="icon-pencil"></i></a>
                            </h4>
                        </td>
                        <td class="align-middle">
                            <h4>
                                <a href="javascript:void(0);" data-toggle="modal" data-target="#modal-delete-wallet" class="btn text-danger ${isBlock}" onclick="deleteWallet(${item.id}, '${item.wallet_name}')"><i class="icon-trash"></i></a>
                            </h4>
                        </td>
                    </tr>
                `;
            }).join('')}`);

            setInterval(() => {
                const observer = lozad();
                observer.observe();
            }, 2000);
        }

        function deleteWallet(idWallet, item) {
            $("#delete-wallet").val(idWallet)
            $("#delete-wallet-name").text(item)
        }

        function loadOneWallet(idWallet) {
            $("#edit-logo-preview").attr("src", base_url.value + '/../assets/dist/images/broken-256.png')
            $("#edit-name").val("")
            $("#edit-description").val("")
            $("#edit-wallet").attr("disabled", true);
            $("#id-wallet").val(idWallet)
            $.ajax({
                url: base_url.value + "/dashboard/a/masterData/load_detail_wallet/" + idWallet,
                data: null,
                type: "GET",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.code == 200) {
                        $("#edit-name").val(response.data.wallet_name)
                        $("#edit-description").val(response.data.wallet_description)
                        $("#edit-logo-preview").attr("src", response.data.uri)
                        $("#edit-logo-preview").addClass("mb-3")
                        $("#edit-wallet").removeAttr("disabled")
                    } else {
                        toastr.error(response.message);
                    }
                }
            });
        }

        function editWallet(is_visible){
            $("#edit-wallet").attr("disabled", true);

            var raw = req.raw({
                name: $("#edit-name").val(),
                desc: $("#edit-description").val(),
                is_visible: is_visible == 0 ? "0" : is_visible
            })

            var formData = new FormData()
            formData.append("raw", raw)

            var logoData = new FormData()
            var logo = $("#edit-logo-wallet")[0].files

            // Update wallet
            $.ajax({
                url: base_url.value + "/dashboard/a/masterData/edit_wallet/" + $('#id-wallet').val(),
                data: formData,
                type: "POST",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.code == 200) {
                        toastr.success("Sukses mengubah data dompet");

                        // Update logo if exist
                        if(logo.length != 0){
                            logoData.append("logo", logo[0])
                            $.ajax({
                                url: base_url.value + "/dashboard/a/masterData/edit_logo_wallet/" + $('#id-wallet').val(),
                                data: logoData,
                                type: "POST",
                                contentType: false,
                                processData: false,
                                success: function(response) {
                                    response = req.data(response)
                                    $("#modal-edit-wallet").modal("hide")

                                    if (response.code == 200) {
                                        toastr.success("Sukses mengubah logo dompet");
                                        // Refresh Table
                                        loadWallets()
                                    } else {
                                        toastr.error(response.message);
                                    }
                                    $("#edit-wallet").removeAttr("disabled")
                                }
                            });
                        } else {
                            $("#edit-wallet").removeAttr("disabled")
                            $("#modal-edit-wallet").modal("hide")
                            loadWallets()
                        }
                    } else {
                        toastr.error(response.message);
                    }
                }
            });
        }
    </script>
    <!-- END: APP JS-->

    <!-- START: Page Script JS-->
    <script src="<?= base_url('assets/dist/vendors/select2/js/select2.full.min.js') ?>"></script>
    <script>
        // Select2
        $(".select2").select2({
            theme: 'bootstrap4',
            width: 'style',
            minimumResultsForSearch: Infinity
        });
    </script>
    <!-- END: Page Script JS-->
</body>

</html>