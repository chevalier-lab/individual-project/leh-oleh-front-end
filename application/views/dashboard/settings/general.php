<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('components/head'); ?>

    <!-- START: Template CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.theme.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/simple-line-icons/css/simple-line-icons.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/flags-icon/css/flag-icon.min.css'); ?>">
    <!-- END Template CSS-->

    <!-- START: Page CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/social-button/bootstrap-social.css'); ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/sweetalert/sweetalert.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/toastr/toastr.min.css'); ?>"/>
    <!-- END: Page CSS-->

    <!-- START: Custom CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/css/main.css'); ?>">
    <!-- END: Custom CSS-->
</head>

<body id="main-container" class="default">

    <input type="hidden" name="base_url" 
        id="base_url" value="<?= base_url("index.php"); ?>">
    <input type="hidden" name="api_url" 
        id="api_url" value="<?= API_URI; ?>">

    <!-- START: Pre Loader-->
    <div class="se-pre-con">
        <div class="loader"></div>
    </div>
    <!-- END: Pre Loader-->

    <!-- START: Template JS-->
    <script src="<?= base_url('assets/dist/vendors/jquery/jquery-3.3.1.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/moment/moment.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/slimscroll/jquery.slimscroll.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/toastr/toastr.min.js'); ?>"></script>
    <script src="https://cdn.jsdelivr.net/npm/lozad/dist/lozad.min.js"></script>
    <!-- END: Template JS-->

    <!-- START: Page Vendor JS-->
    <script src="<?= base_url('assets/dist/js/auth/request.js'); ?>"></script>
    <script>
        var base_url = document.getElementById("base_url");
        var api_url = document.getElementById("api_url");
    </script>
    <!-- END: Page Vendor JS-->

    <!-- START: Main Content-->
    <?php

    // Load Navigation Bar
    $this->load->view('components/menus/navbar');

    // Load Sidebar
    $this->load->view('components/menus/sidebar');

    // Load Page
    $this->load->view('dashboard/template', array(
        'page' => "Umum",
        'pageMap' => array(
            array(
                "label" => "Pengaturan",
                "is_current" => false
            ),
            array(
                "label" => "Umum",
                "is_current" => true
            )
        ),
        'pageURI' => "dashboard/settings/pages/general",
    ));

    // Change Logo
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-change-logo",
        "modalTitle" => "Ubah Logo",
        "modalType" => "modal-md",
        "iconTitle" => "icon-picture",
        "modalActionForm" => "#",
        "modalContentForm" => '
        <div>
            <img id="widget-selected-logo" src="" class="img-fluid">
        </div>
        <label for="fileUpload" 
            class="file-upload btn btn-primary btn-block">
                <i class="fa fa-upload mr-2"></i>Pilih logo ...
                <input id="fileUpload" type="file">
        </label>
        ',
        "modalButtonForm" => '<button type="button"
        id="btn-change-logo"
        class="btn btn-primary add-todo">Ubah Logo</button>'
    ));

    ?>
    <!-- END: Content-->

    <!-- START: Page Script JS-->
    <?php
    if (isset($error)) {
        $this->load->view("components/error-modal", array(
            "errorModalTitle" => $error["title"],
            "errorModalContent" => $error["content"],
            "errorModalDetail" => $error["details"]
        ));
    }

    ?>
    <!-- END: Page Script JS-->

    <!-- START: APP JS-->
    <script src="<?= base_url('assets/dist/js/app.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/js/home.script.js'); ?>"></script>

    <script>
        // Initial Pagination
        var page = 0;
        var paginationClicked = false;
        var paginationDirection = "";

        (function ($) {
            "use strict";
            $(window).on("load", function () {
                // Load Bank First Time
                loadGeneral()

                $("#fileUpload").on("change", function(event) {
                    var reader = new FileReader();
                    reader.onload = function()
                    {
                        var output = document.getElementById('widget-selected-logo');
                        output.src = reader.result;
                    }
                    reader.readAsDataURL(event.target.files[0]);
                    $("#widget-selected-logo").addClass("mb-3")
                });

                $("#btn-change-logo").on("click", function() {
                    $("#btn-change-logo").attr("disabled", true)

                    var formData = new FormData()
                    var cover = $("#fileUpload")[0].files
                    if (cover.length == 0) {
                        toastr.error("Harap pilih logo terlebih dahulu");
                        $("#btn-change-logo").removeAttr("disabled")
                        return
                    } else {
                        formData.append("logo", cover[0])
                        $.ajax({
                            url: base_url.value + "/dashboard/a/Settings/edit_logo/",
                            data: formData,
                            type: "POST",
                            contentType: false,
                            processData: false,
                            success: function(response) {
                                response = req.data(response)

                                if (response.code != 200) {
                                    toastr.error(response.message);
                                } else {
                                    toastr.success(response.message);
                                }
                                $("#modal-change-logo").modal("hide")
                                $("#btn-change-logo").removeAttr("disabled")

                                setTimeout(() => {
                                    location.assign(base_url.value + "/dashboard/a/Settings")
                                }, 1000);
                            }
                        });
                    }
                })

                $("#general-btn-save").on("click", function() {

                    $("#general-btn-save").attr("disabled", true)

                    var application_name = $("#create-application_name").val()
                    var email = $("#create-email").val()
                    var description = $("#create-description").val()
                    var address = $("#create-address").val()
                    var phone_number = $("#create-phone_number").val()

                    if (application_name == "") {
                        toastr.error("Harap isi application_name terlebih dahulu");
                        $("#general-btn-save").removeAttr("disabled")
                        return
                    } else if (phone_number == "") {
                        toastr.error("Harap isi phone_number terlebih dahulu");
                        $("#general-btn-save").removeAttr("disabled")
                        return
                    } else if (address == "") {
                        toastr.error("Harap isi address terlebih dahulu");
                        $("#general-btn-save").removeAttr("disabled")
                        return
                    } else if (description == "") {
                        toastr.error("Harap isi description terlebih dahulu");
                        $("#general-btn-save").removeAttr("disabled")
                        return
                    } else if (email == "") {
                        toastr.error("Harap isi email terlebih dahulu");
                        $("#general-btn-save").removeAttr("disabled")
                        return
                    }

                    var raw = req.raw({
                        application_name: application_name,
                        description: description,
                        address: address,
                        phone_number: phone_number,
                        email: email
                    });

                    var formData = new FormData()
                    formData.append("raw", raw)

                    $.ajax({
                        url: base_url.value + "/dashboard/a/settings/update_general",
                        data: formData,
                        type: "POST",
                        contentType: false,
                        processData: false,
                        success: function(response) {
                            response = req.data(response)

                            if (response.code == 200) {

                                toastr.success(response.message);
                                setTimeout(() => {
                                    location.assign(base_url.value + "/dashboard/a/Settings")
                                }, 1000);

                            } else {
                                $("#general-btn-save").removeAttr("disabled")
                                toastr.error(response.message);
                            }
                        }
                    });
                });
            });
        })(jQuery);

        function loadGeneral() {
            renderToWaiting()

            $.ajax({
                url: base_url.value + "/dashboard/a/settings/load_general",
                data: null,
                type: "GET",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.code == 200) {
                        renderToTable(response.data)
                    } else {
                        renderToTable(null)
                    }
                }
            });
        }

        function renderToWaiting() {
            $("#lists-pages tbody").html(`
                <tr>
                    <td class="align-middle" colspan="11">Sedang memuat data</td>
                </tr>
            `);
        }

        function renderToTable(data=null) {
            
            if (data == null) return

            $("#general-logo").html(`
                <img 
                    alt="" class="img-fluid lozad"
                    data-src="${data.uri}"
                    src="${base_url.value + '/../assets/dist/images/broken-256.png'}">
            `);
            $("#create-application_name").val(data.application_name)
            $("#create-email").val(data.email)
            $("#create-description").val(data.description)
            $("#create-address").val(data.address)
            $("#create-phone_number").val(data.phone_number)

            setInterval(() => {
                const observer = lozad();
                observer.observe();
            }, 2000);
        }
    </script>
    <!-- END: APP JS-->
</body>

</html>