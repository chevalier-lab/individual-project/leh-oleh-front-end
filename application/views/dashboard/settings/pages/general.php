<div class="row">
    <div class="col-12 col-md-3">
        <div class="card">
            <div class="card-body" id="general-logo">
            </div>
        </div>
    </div>
    <div class="col-12 col-md-9">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-12 mb-3">
                        <button class="btn btn-primary"
                        data-toggle="modal" data-target="#modal-change-logo">Ubah Logo</button>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group mb-3">
                            <label for="create-application_name">Nama Website</label>
                            <input type="text" id="create-application_name" 
                                name="create-application_name" 
                                class="form-control" placeholder="Contoh: Leh Oleh"/> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group mb-3">
                            <label for="create-email">Email</label>
                            <input type="text" id="create-email" 
                                name="create-email" 
                                class="form-control" placeholder="Contoh: aitek@email.com"/> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group mb-3">
                            <label for="create-phone_number">Nomor HP</label>
                            <input type="text" id="create-phone_number" 
                                name="create-phone_number" 
                                class="form-control" placeholder="Contoh: 6282119189xxx"/> 
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group mb-3">
                            <label for="create-address">Alamat</label>
                            <textarea type="text" id="create-address" 
                                name="create-address" 
                                class="form-control" placeholder="Contoh: Jl. Telekomunikasi No.01"></textarea>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group mb-3">
                            <label for="create-description">Deskripsi Website</label>
                            <textarea type="text" id="create-description" 
                                name="create-description" 
                                class="form-control" placeholder="Tulisakan deskripsi website disini"></textarea>
                        </div>
                    </div>
                    <div class="col-12">
                    <button class="btn btn-primary"
                        type="button"
                        id="general-btn-save">Simpan <span class="icon-paper-plane"></span></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>