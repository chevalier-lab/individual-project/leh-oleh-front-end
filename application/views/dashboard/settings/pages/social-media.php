<div class="card">
    <div class="card-header">
        <h4 class="card-title">Data Sosial Media</h4>
    </div>
    <div class="card-body">

        <div class="pt-0 pb-0 pl-3 pr-3 mb-3 row">
            <div class="input-group col-12 col-md-5 p-1">
                <input type="text" class="form-control p-2 w-100 h-100 contact-search" 
                    placeholder="Cari ..."
                    id="social-media-search">
                    <div class="input-group-append">
                        <span class="btn btn-outline-primary input-group-text" id="social-media-button-search"><i class="icon-magnifier"
                            aria-hidden="true"></i></span>
                    </div>
            </div>
            <div class="col-12 col-md-5 p-1">
                <select class="form-control p-2 w-100 h-100 contact-search"
                    id="social-media-order">
                    <option value="0">Urutkan berdasarkan</option>
                    <option value="1">Nama (A-Z)</option>
                    <option value="2">Nama (Z-A)</option>
                </select>
            </div>
            <div class="col-12 col-md-2 pl-1 pr-1 my-2">
                <button data-toggle="modal" 
                data-target="#modal-create-sosmed" class="btn btn-outline-secondary btn-block"><i class="icon-plus"></i> Sosmed</button>
            </div>
        </div>

        <div class="table-responsive pr-1 pl-1 mb-0">
        <?php

            $this->load->view('components/table', array(
                "idTable" => "social-media",
                "isCustomThead" => true,
                "thead" => '
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Sosial Media</th>
                    <th scope="col">Ikon</th>
                    <th scope="col">URL</th>
                    <th scope="col">Hapus</th>
                </tr>
                ',
                "tbody" => ''
            )) ?>

        </div>

        <div class="row">
            <div class="col-12">
                <div class="p-1">
                    <div class="btn-group" role="group" aria-label="Pagination Group">
                        <button class="btn btn-outline-secondary" type="button" 
                        id="social-media-prev"><i class="icon-arrow-left-circle"></i> Sebelumnya</button>
                        <button class="btn btn-outline-secondary" type="button" 
                        id="social-media-next">Selanjutnya <i class="icon-arrow-right-circle"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>