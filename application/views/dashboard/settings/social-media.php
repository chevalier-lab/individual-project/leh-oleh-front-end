<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('components/head'); ?>

    <!-- START: Template CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.theme.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/simple-line-icons/css/simple-line-icons.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/flags-icon/css/flag-icon.min.css'); ?>">
    <!-- END Template CSS-->

    <!-- START: Page CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/social-button/bootstrap-social.css'); ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/sweetalert/sweetalert.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/toastr/toastr.min.css'); ?>"/>
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/fontawesome/css/all.min.css') ?>" />
    <!-- END: Page CSS-->

    <!-- START: Custom CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/css/main.css'); ?>">
    <!-- END: Custom CSS-->
</head>

<body id="main-container" class="default">

    <input type="hidden" name="base_url" 
        id="base_url" value="<?= base_url("index.php"); ?>">
    <input type="hidden" name="api_url" 
        id="api_url" value="<?= API_URI; ?>">

    <!-- START: Pre Loader-->
    <div class="se-pre-con">
        <div class="loader"></div>
    </div>
    <!-- END: Pre Loader-->

    <!-- START: Template JS-->
    <script src="<?= base_url('assets/dist/vendors/jquery/jquery-3.3.1.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/moment/moment.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/slimscroll/jquery.slimscroll.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/toastr/toastr.min.js'); ?>"></script>
    <!-- END: Template JS-->

    <!-- START: Page Vendor JS-->
    <script src="<?= base_url('assets/dist/js/auth/request.js'); ?>"></script>
    <script>
        var base_url = document.getElementById("base_url");
        var api_url = document.getElementById("api_url");
    </script>
    <!-- END: Page Vendor JS-->

    <!-- START: Main Content-->
    <?php

    // Load Navigation Bar
    $this->load->view('components/menus/navbar');

    // Load Sidebar
    $this->load->view('components/menus/sidebar');

    // Load Page
    $this->load->view('dashboard/template', array(
        'page' => "Sosial Media",
        'pageMap' => array(
            array(
                "label" => "Pengaturan",
                "is_current" => false
            ),
            array(
                "label" => "Sosial Media",
                "is_current" => true
            ),
        ),
        'pageURI' => "dashboard/settings/pages/social-media",
    ));

    ?>
    <!-- END: Content-->

    <!-- START: Page Script JS-->
    <?php
    if (isset($error)) {
        $this->load->view("components/error-modal", array(
            "errorModalTitle" => $error["title"],
            "errorModalContent" => $error["content"],
            "errorModalDetail" => $error["details"]
        ));
    }

    // Create Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-create-sosmed",
        "modalTitle" => "Tambah Sosial Media",
        "iconTitle" => "icon-plus",
        "modalType" => "modal-xl",
        "modalActionForm" => "#",
        "modalContentForm" => '

        <div class="row">
            <div class="col-12 col-md-8">
                <h4>Cari Icon</h4>
                <div class="pt-0 pb-0 pl-3 pr-3 mb-3 row">
                    <div class="input-group col-12 col-md-6 p-1">
                        <input type="text" class="form-control p-2 w-100 h-100 contact-search" 
                            placeholder="Cari ..."
                            id="lists-icon-search">
                            <div class="input-group-append">
                                <span class="btn btn-outline-primary input-group-text" id="lists-icon-button-search"><i class="icon-magnifier"
                                    aria-hidden="true"></i></span>
                            </div>
                    </div>
                    <div class="col-12 col-md-6 p-1">
                        <select class="form-control p-2 w-100 h-100 contact-search"
                            id="lists-icon-order">
                            <option value="0">Urutkan berdasarkan</option>
                            <option value="1">Nama (A-Z)</option>
                            <option value="2">Nama (Z-A)</option>
                        </select>
                    </div>
                </div>
                
                <div class="text-center lists-icon">
                </div>
            </div>
            <div class="col-12 col-md-4">
                <h4>Formulir Sosial Media</h4>
                <div class="form-group">
                    <label for="social-media-name">Sosial Media</label>
                    <input type="text" name="social-media-name" id="social-media-name"
                        class="form-control">
                </div>
                <div class="form-group">
                    <label for="social-media-uri">URL</label>
                    <input type="text" name="social-media-uri" id="social-media-uri"
                        class="form-control">
                </div>
                <div class="form-group">
                    <label for="social-media-icon">Icon</label>
                    <div class="text-left"
                        id="selected-icon">
                    </div>
                </div>
            </div>
        </div>
        ',
        "modalButtonForm" => '<button type="button" 
        id="create-icon"
        class="btn btn-primary add-todo">Create Icon</button>'
    ));

    // Delete Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-delete-sosmed",
        "modalTitle" => "Hapus Sosial Media",
        "iconTitle" => "icon-trash",
        "modalActionForm" => "#",
        "modalType" => "modal-md",
        "modalContentForm" => '
        <p>Are you sure to delete sosmed <strong id="sosmed-delete"></strong> ?</p>
        ',
        "modalButtonForm" => '<button type="button" 
        id="btn-delete-sosmed"
        class="btn btn-danger add-todo">Delete</button>'
    ));


    ?>
    <!-- END: Page Script JS-->

    <!-- START: APP JS-->
    <script src="<?= base_url('assets/dist/js/app.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/js/home.script.js'); ?>"></script>

    <script>
        // Initial Pagination
        var page = 0;
        var paginationClicked = false;
        var paginationDirection = "";
        var icons = []
        var selectedIcon = -1;

        (function ($) {
            "use strict";
            $(window).on("load", function () {
                // Load Bank First Time
                loadSocialMedia()

                loadIcons()

                // Handle Order By
                $("#social-media-order").on("change", function() {
                    loadSocialMedia();
                });

                // Handle Search
                $("#social-media-search").on("keydown", function(event) {
                    if (event.keyCode == 32 || event.which == 32) loadSocialMedia();
                    else if ($("#social-media-search").val() == "") loadSocialMedia();
                });

                $("#social-media-button-search").on("click", function(event) {
                    loadSocialMedia();
                });

                // Handle Pagination
                $("#social-media-prev").on("click", function() {
                    if (page > 0)
                        page -= 1;
                    paginationClicked = true;
                    paginationDirection = "prev";
                    loadSocialMedia();
                });

                $("#social-media-next").on("click", function() {
                    page += 1;
                    paginationClicked = true;
                    paginationDirection = "next";
                    loadSocialMedia();
                });

                $("#create-icon").on("click", function() {
                    $("#create-icon").attr("disabled", true);
                    var name = $("#social-media-name").val();
                    var uri = $("#social-media-uri").val();
                    var icon = icons[selectedIcon]

                    if (name == "") {
                        toastr.error("Harap isi sosial media terlebih dahulu");
                        $("#create-icon").removeAttr("disabled")
                        return
                    } else if (uri == "") {
                        toastr.error("Harap isi URL terlebih dahulu");
                        $("#create-icon").removeAttr("disabled")
                        return
                    } else if (icon == null) {
                        toastr.error("Harap isi ikon terlebih dahulu");
                        $("#create-icon").removeAttr("disabled")
                        return
                    }

                    var raw = req.raw({
                        name: name,
                        id_icon: icon.id,
                        uri: uri
                    });

                    var formData = new FormData()
                    formData.append("raw", raw)

                    $.ajax({
                        url: base_url.value + "/dashboard/a/settings/create_sosmed",
                        data: formData,
                        type: "POST",
                        contentType: false,
                        processData: false,
                        success: function(response) {
                            response = req.data(response)

                            if (response.code == 200) {

                                toastr.success(response.message);
                                setTimeout(() => {
                                    location.assign(base_url.value + "/dashboard/a/settings/socialMedia")
                                }, 1000);

                            } else {
                                $("#create-icon").removeAttr("disabled")
                                toastr.error(response.message);
                            }
                        }
                    });
                })

                // Handle Delete Page
                $("#btn-delete-sosmed").on("click", function() {
                    $.ajax({
                        url: base_url.value + "/dashboard/a/settings/delete_sosmed/" + $("#btn-delete-sosmed").val(),
                        data: null,
                        type: "GET",
                        contentType: false,
                        processData: false,
                        success: function(response) {
                            response = req.data(response)
                            if (response.code == 200) {
                                toastr.success(response.message);

                                $("#modal-delete-sosmed").modal('hide');

                                setTimeout(() => {
                                    location.assign(base_url.value + "/dashboard/a/settings/socialMedia")
                                }, 1000);
                            } else {
                                toastr.error(response.message);
                            }
                        }
                    });
                });
            });
        })(jQuery);

        function loadSocialMedia() {
            renderToWaiting()

            var order = $("#social-media-order").val();
            var order_direction = "DESC";
            switch (Number(order)) {
                case 1: 
                    order =  "social_media";
                    order_direction = "ASC";
                break;
                case 2: 
                    order =  "social_media";
                    order_direction = "DESC";
                break;
                default:
                    order =  "id";
                    order_direction = "DESC";
                break;
            }
            var raw = req.raw({
                page: page,
                search: $("#social-media-search").val(),
                order_by: order,
                order_direction: order_direction
            })

            var formData = new FormData()
            formData.append("raw", raw)

            $.ajax({
                url: base_url.value + "/dashboard/a/settings/load_sosmed",
                data: formData,
                type: "POST",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.code == 200) {
                        renderToTable(response.data)
                    } else {
                        renderToTable([])
                    }
                }
            });
        }

        function renderToWaiting() {
            $("#social-media tbody").html(`
                <tr>
                    <td class="align-middle" colspan="5">Sedang memuat data</td>
                </tr>
            `);
        }

        function renderToTable(data=[]) {
            
            if (paginationClicked) {
                paginationClicked = false;
                if (data.length == 0) {
                    if (paginationDirection == "next") 
                        page -= 1;
                    else page += 1;
                }
                paginationDirection = "";
                loadSocialMedia();
                return 
            }

            var index = (page * 10) + 1;

            if (data.length == 0) {
                $("#social-media tbody").html(`
                    <tr>
                        <td class="align-middle" colspan="5">Belum ada data, 
                        tekan <span class="icon-plus"></span> untuk menambahkan</td>
                    </tr>
                `);
                return;
            }

            $("#social-media tbody").html(`${data.map(function(item) {
                return `
                    <tr>
                        <td class="align-middle">${index++}</td>
                        <td class="align-middle">${item.social_media}</td>
                        <td class="align-middle">
                            <h4><span class="${item.icon}"></span></h4>
                        </td>
                        <td class="align-middle">
                            <a target="_blank" href="${item.uri}" class="text-primary">Lihat Halaman</a>
                        </td>
                        <td class="align-middle">
                            <h4>
                                <a href="javascript:void(0);" data-toggle="modal" 
                                data-target="#modal-delete-sosmed" 
                                onclick="deletePage(${item.id}, '${item.social_media}')"
                                class="btn text-danger"><i class="icon-trash"></i></a>
                            </h4>
                        </td>
                    </tr>
                `;
            }).join('')}`);
        }

        function deletePage(idPage, item) {
            $("#btn-delete-sosmed").val(idPage)
            $("#sosmed-delete").text(item)
        }

        function loadIcons() {
            var order = $("#lists-icon-order").val();
            var order_direction = "DESC";
            switch (Number(order)) {
                case 1: 
                    order = "icon";
                    order_direction = "ASC";
                break;
                case 2: 
                    order = "icon";
                    order_direction = "DESC";
                break;
                default:
                    order = "id";
                    order_direction = "DESC";
                break;
            }
            var raw = req.raw({
                page: page,
                search: $("#lists-icon-search").val(),
                order_by: order,
                order_direction: order_direction
            })

            var formData = new FormData()
            formData.append("raw", raw)

            $.ajax({
                url: base_url.value + "/dashboard/a/masterData/load_icons",
                data: formData,
                type: "POST",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.code == 200) {
                        var data = response.data
                        icons = data;
                        $(".lists-icon").html(`${data.map(function(item, position) {
                            return `
                            <div class="font-icon-list p-2 border mx-1 mb-2"
                                style="cursor: pointer"
                                onclick="setIcon(${position})">
                                <div class="preview">
                                    <i class="${item.icon}"></i>
                                    ${item.icon}
                                </div>
                            </div>
                            `;
                        }).join('')}`)
                    }
                }
            });
        }

        function setIcon(position) {
            selectedIcon = position;
            var icon = icons[position];
            $("#selected-icon").html(`
            <div class="font-icon-list p-2 border mx-1 mb-2"
                style="cursor: pointer">
                <div class="preview">
                    <i class="${icon.icon}"></i>
                </div>
            </div>
            `);
        }
    </script>
    <!-- END: APP JS-->
</body>

</html>