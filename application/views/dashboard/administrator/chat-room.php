<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('components/head'); ?>

    <!-- START: Template CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.theme.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/simple-line-icons/css/simple-line-icons.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/flags-icon/css/flag-icon.min.css'); ?>">
    <!-- END Template CSS-->

    <!-- START: Page CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/social-button/bootstrap-social.css'); ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/sweetalert/sweetalert.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/toastr/toastr.min.css'); ?>" />
    <!-- END: Page CSS-->

    <!-- START: Custom CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/css/main.css'); ?>">
    <!-- END: Custom CSS-->
</head>

<body id="main-container" class="default">

    <input type="hidden" name="base_url" id="base_url" value="<?= base_url("index.php"); ?>">
    <input type="hidden" name="api_url" id="api_url" value="<?= API_URI; ?>">

    <!-- utility room  -->
    <input type="hidden" value="<?= $idRoom ?>" id="room_id">

    <!-- START: Pre Loader-->
    <div class="se-pre-con">
        <div class="loader"></div>
    </div>
    <!-- END: Pre Loader-->

    <!-- START: Template JS-->
    <script src="<?= base_url('assets/dist/vendors/jquery/jquery-3.3.1.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/moment/moment.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/slimscroll/jquery.slimscroll.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/toastr/toastr.min.js'); ?>"></script>
    <script src="https://cdn.jsdelivr.net/npm/lozad/dist/lozad.min.js"></script>
    <!-- END: Template JS-->

    <!-- START: Page Vendor JS-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.3.1/socket.io.js" integrity="sha512-AcZyhRP/tbAEsXCCGlziPun5iFvcSUpEz2jKkx0blkYKbxU81F+iq8FURwPn1sYFeksJ+sDDrI5XujsqSobWdQ==" crossorigin="anonymous"></script>
    <script src="https://cdn.rawgit.com/CryptoStore/crypto-js/3.1.2/build/rollups/aes.js"></script>
    <script src="<?= base_url('assets/dist/js/auth/request.js'); ?>"></script>
    <script>
        var base_url = document.getElementById("base_url");
        var api_url = document.getElementById("api_url");
        var room_id = document.getElementById("room_id")
    </script>
    <!-- END: Template JS-->

    <!-- START: Main Content-->
    <?php

    // Load Navigation Bar
    $this->load->view('components/menus/navbar');

    // Load Sidebar
    $this->load->view('components/menus/sidebar');

    // Load Page
    $this->load->view('dashboard/template', array(
        'page' => "Ruang Obrolan",
        'pageMap' => array(
            array(
                "label" => "Beranda",
                "is_current" => false
            ),
            array(
                "label" => "Ruang Obrolan",
                "is_current" => true
            )
        ),
        'pageURI' => "dashboard/administrator/pages/chat-room"
    ));

    ?>
    <!-- END: Content-->

    <!-- START: Page Script JS-->
    <?php
    if (isset($error)) {
        $this->load->view("components/error-modal", array(
            "errorModalTitle" => $error["title"],
            "errorModalContent" => $error["content"],
            "errorModalDetail" => $error["details"]
        ));
    }

    // Delete Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-update-chat",
        "modalTitle" => "Nonaktifkan Obrolan",
        "iconTitle" => "icon-pencil",
        "modalActionForm" => "#",
        "modalContentForm" => '
        <p>Apakah anda yakin ingin mengakhiri ruang obrolan ini ?</p>
        ',
        "modalButtonForm" => '<button type="submit" class="btn btn-danger add-todo" id="update-room-status">Nonaktif</button>'
    ));
    ?>
    <!-- END: Page Script JS-->

    <!-- START: APP JS-->
    <script src="<?= base_url('assets/dist/js/app.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/js/home.script.js'); ?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.3.0/socket.io.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/rollups/aes.js"></script>
    <!-- END: APP JS-->

    <script>
        var dataRoom;
        var dataChats;
        var chats = [];

        // const socket = io('http://213.190.4.40:2020', { path: '/leh-oleh/chat' });
        const socket = io('https://leholeh-websocket.herokuapp.com', { path: '/leh-oleh/chat' });

        (function($) {
            "use strict";
            $(window).on("load", function() {
                // Load Info Room
                loadChatRoom();
                socket.heartbeatTimeout = 20000

                // Trigger enter for send chat
                $("#detail-chat-message").keypress(function(event) { 
                    if (event.keyCode === 13) { 
                        $("#btn-send").click(); 
                    } 
                }); 

                // Handle Send message
                $("#btn-send").on("click", function() {
                    if($('#detail-chat-message').val() !== ""){
                        var message = $("#detail-chat-message").val();
                        var enc = CryptoJS.AES.encrypt(message, 'secret-chat').toString()
                        socket.emit('chat-message', {
                            message: message, 
                            date: "<?= date('Y-m-d H:i:s'); ?>"
                        })
                        $("#detail-chat-message").val('')

                        var raw = req.raw({
                            id_u_user_chat: dataRoom.id,
                            content: enc
                        });

                        var formData = new FormData();
                        formData.append("raw", raw);

                        $.ajax({
                            url: base_url.value + "/services/chats/send_message",
                            data: formData,
                            type: "POST",
                            contentType: false,
                            processData: false,
                            success: function(response) {
                                response = req.data(response)
                                if (response.code == 200) {
                                }
                            }
                        });
                    }
                });

                // Handle Update Room
                $("#update-room-status").on("click", function() {
                    $("#update-room-status").attr("disabled", true);

                    var raw = req.raw({
                        id_u_user_chat: room_id.value,
                    })

                    var formData = new FormData()
                    formData.append("raw", raw)

                    $.ajax({
                        url: base_url.value + "/services/chats/update_status_room",
                        data: formData,
                        type: "POST",
                        contentType: false,
                        processData: false,
                        success: function(response) {

                            $("#update-room-status").removeAttr("disabled");
                            $("#modal-update-chat").modal('hide')
                            
                            response = req.data(response)

                            if (response.code == 200) {
                                toastr.success(response.message);
                                window.location = "<?= base_url('index.php/dashboard/a/dashboard/chatCS/') ?>"
                            } else {
                                toastr.error(response.message);
                            }
                        }
                    });
                });
            });
        })(jQuery);

        function loadChatRoom() {
            $.ajax({
                url: base_url.value + "/services/chats/room_detail_cs/" + room_id.value,
                data: null,
                type: "GET",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.code == 200) {
                        var data = response.data

                        dataRoom = data[0];

                        setupChatRoom();
                    }
                }
            });
        }

        function loadChatDetail() {
            $.ajax({
                url: base_url.value + "/services/chats/list_chat_detail/" + room_id.value,
                data: null,
                type: "GET",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.code == 200) {
                        var data = response.data

                        dataChats = data;

                        setupChatDetail();
                    }
                }
            });
        }

        function setupChatRoom() {
            var data = dataRoom;

            $("#chat-tab-person").prepend(`
                <img class="img-fluid mr-3 rounded-circle lozad" 
                style="max-width: 48px"
                data-src="${data.face_user_1}"
                src="${base_url.value + '/../assets/dist/images/author2.jpg'}" alt="">
                <div class="media-body align-self-center mt-0 color-primary d-flex">
                    <div class="message-content"> <b class="mb-1 font-weight-bold d-flex">${data.full_name_user_1}</b>
                        <small class="body-color">${data.updated_at}</small></div>
                </div>
            `);

            $("#detail-chat-topbar").prepend(`
                <img class="img-fluid  mr-3 rounded-circle lozad" 
                data-src="${data.face_user_1}"
                src="${base_url.value + '/../assets/dist/images/author2.jpg'}" width="54" alt="">
                <div class="media-body align-self-center mt-0  d-flex">
                    <div class="message-content">
                        <h6 class="mb-1 font-weight-bold d-flex">${data.full_name_user_1}</h6>
                        <small class="body-color" id="friend-status">${data.updated_at}</small></div>
                    </div>
                </div>
            `);

            loadChatDetail();

            setupSocket();

            setInterval(() => {
                const observer = lozad();
                observer.observe();
            }, 2000);
        }

        function setupChatDetail() {

            var data = dataRoom;

            function decrypt(msg) {
                let bytes = CryptoJS.AES.decrypt(msg, 'secret-chat')
                return bytes.toString(CryptoJS.enc.Utf8)
            }

            $("#detail-chat-worksheet").html(`${dataChats.map(function(item) {
                var text = decrypt(item.content);
                if (item.id_m_users != data.id_m_users_1) {
                    return `
                        <div class="media d-flex mb-4">
                            <div class="p-3 ml-auto speech-bubble">
                                ${text}<br>
                                <small>${req.date(item.created_at)}</small>
                            </div>
                            <div class="ml-4"><a href="javascript:void(0);"><img 
                                data-src="${item.uri}"
                                style="max-width: 48px"
                                src="${base_url.value + '/../assets/dist/images/author2.jpg'}" alt="" 
                            class="img-fluid rounded-circle lozad" /></a></div>
                        </div>
                    `
                } else {
                    return `
                        <div class="media d-flex mb-4">
                            <div class="mr-4 thumb-img"><a href="javascript:void(0);"><img 
                                style="max-width: 48px"
                                data-src="${item.uri}"
                                src="${base_url.value + '/../assets/dist/images/author2.jpg'}" alt="" 
                            class="img-fluid rounded-circle lozad" /></a></div>
                            <div class="p-3 mr-auto speech-bubble alt">
                                ${text}<br>
                                <small>${req.date(item.created_at)}</small>
                            </div>
                        </div>
                    `
                }
            }).join('')}`);

            document.getElementById('detail-chat-worksheet').scrollTop = document.getElementById('detail-chat-worksheet').scrollHeight
        }

        function setupSocket() {
            var data = dataRoom;
            var prepareEmit = { userId: data.username_2, token: data.chat_token };
            socket.emit('token', prepareEmit)

            // get user info
            socket.on('user-info', ({ token, friend }) => {

                // get friend data
                const user = friend.filter(user => user.userId !== data.username_2)

                // set user-status
                if (user.length !== 0) {
                    $("#friend-status").text("online")
                } else {
                    $("#friend-status").text("offline")
                }

            })

            // get status message from server 
            socket.on('status', ({ status, user }) => { 

                if (user.userId !== data.username_2 && status == 'offline') {
                    $("#friend-status").text("offline")
                }
            })

            // get message from server
            socket.on('message', res => {
                // check user
                if (res.username != data.username_1) {
                    $("#detail-chat-worksheet").append(`
                        <div class="media d-flex  mb-4">
                            <div class="p-3 ml-auto speech-bubble">
                                ${res.text}<br>
                                <small>${res.time}</small>
                            </div>
                            <div class="ml-4"><a href="javascript:void(0);"><img 
                                data-src="${data.face_user_2}"
                                style="max-width: 48px"
                                src="${base_url.value + '/../assets/dist/images/author2.jpg'}" alt="" 
                            class="img-fluid rounded-circle lozad" /></a></div>
                        </div>
                    `);
                } else {
                    $("#detail-chat-worksheet").append(`
                        <div class="media d-flex mb-4">
                            <div class="mr-4 thumb-img"><a href="javascript:void(0);"><img 
                                style="max-width: 48px"
                                data-src="${data.face_user_1}"
                                src="${base_url.value + '/../assets/dist/images/author2.jpg'}" alt="" 
                            class="img-fluid rounded-circle lozad" /></a></div>
                            <div class="p-3 mr-auto speech-bubble alt">
                                ${res.text}<br>
                                <small>${res.time}</small>
                            </div>
                        </div>
                    `);
                }

                // scroll down automatically
                document.getElementById('detail-chat-worksheet').scrollTop = document.getElementById('detail-chat-worksheet').scrollHeight
            })
        }
    </script>
</body>

</html>