<div class="row">
    <div class="col-12 col-sm-6 col-xl-4 mt-3">
        <div class="card">
            <div class="card-body">
                <div class='d-flex px-0 px-lg-2 py-2 align-self-center'>
                    <span class="card-liner-icon mt-1">Rp</span>
                    <div class='card-liner-content'>
                        <h2 class="card-liner-title" id="income-today"></h2>
                        <h6 class="card-liner-subtitle">Penghasilan Hari Ini</h6>                                       
                    </div>                                
                </div>                          
            </div>
        </div>
    </div>
    <div class="col-12 col-sm-6 col-xl-4 mt-3">
        <div class="card">
            <div class="card-body">
                <div class='d-flex px-0 px-lg-2 py-2 align-self-center'>
                    <span class="card-liner-icon mt-1">Rp</span>
                    <div class='card-liner-content'>
                        <h2 class="card-liner-title" id="income-month"></h2>
                        <h6 class="card-liner-subtitle">Penghasilan Bulan Ini</h6> 
                    </div>                                
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 col-sm-6 col-xl-4 mt-3">
        <div class="card">
            <div class="card-body">
                <div class='d-flex px-0 px-lg-2 py-2 align-self-center'>
                    <span class="card-liner-icon mt-1">Rp</span>
                    <div class='card-liner-content'>
                        <h2 class="card-liner-title" id="income-year"></h2>
                        <h6 class="card-liner-subtitle">Penghasilan Tahun Ini</h6> 
                    </div>                                
                </div>
            </div>
        </div>
    </div>
</div>

<script>

    for (var i = 1; i <= 3; i++)
        getIncome(i);

    function getIncome(type=1) {
        $.ajax({
            url: base_url.value + "/dashboard/a/dashboard/income/" + type,
            data: null,
            type: "GET",
            contentType: false,
            processData: false,
            success: function(response) {
                response = req.data(response)
                if (response.code == 200) {
                    var data = response.data
                    var price = (data.total_price != null) ? Number(data.total_price) : 0;
                    price = req.money(price.toString(), "Rp ");
                    switch (type) {
                        case 1: 
                            $("#income-today").text(price)
                            break;
                        case 2: 
                            $("#income-month").text(price)
                            break;
                        default:
                            $("#income-year").text(price)
                            break;
                    }
                }
            }
        });
    }
</script>