<div class="col-lg-8">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Transaksi Terbaru</h4>
        </div>
        <div class="card-body">

            <div class="table-responsive pr-1 pl-1 mb-0">

            <?php

            $this->load->view('components/table', array(
                "idTable" => "new-transaction-box",
                "isCustomThead" => true,
                "thead" => '
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Token Transaksi</th>
                    <th scope="col">Total Harga</th>
                    <th scope="col">Status</th>
                    <th scope="col">Tgl Pembuatan</th>
                    <th scope="col">Tgl Perubahan</th>
                </tr>
                ',
                "tbody" => ""
            )) ?>
            </div>

        </div>
    </div>
</div>

<script>
loadTransactions();

function loadTransactions() {
    
    order =  "u_user_transaction.id";
    order_direction = "DESC";

    var raw = req.raw({
        page: 0,
        search: "",
        order_by: order,
        order_direction: order_direction
    })

    var formData = new FormData()
    formData.append("raw", raw)

    $.ajax({
        url: base_url.value + "/dashboard/a/managementTransactions/load_transaction",
        data: formData,
        type: "POST",
        contentType: false,
        processData: false,
        success: function(response) {
            response = req.data(response)
            if (response.code == 200) {
                var data = response.data
                $("#new-transaction-box tbody").html(`${data.map(function(item, index) {
                    var status = Number(item.status);
                    switch (status) {
                        case 0: status = '<span class="badge outline-badge-secondary">Belum membayar</span>'; break;
                        case 1: status = '<span class="badge outline-badge-info">Pembayaran Tervalidasi</span>'; break;
                        case 2: status = '<span class="badge outline-badge-warning">Pesanan Diproses</span>'; break;
                        case 3: status = '<span class="badge outline-badge-success">Pesanan Selesai</span>'; break;
                        default: status = '<span class="badge outline-badge-danger">Pesanan Dikembalikan</span>'; break;
                    }
                    return `
                    <tr>
                        <td>${(index + 1)}</td>
                        <td>${item.full_name}</td>
                        <td>${item.transaction_token}</td>
                        <td>${req.money(Number(item.total_price).toString(), "Rp ")}</td>
                        <td>${status}</td>
                        <td>${item.created_at}</td>
                        <td>${item.updated_at}</td>
                    </tr>
                    `
                }).join('')}`)
            }
        }
    });
}
</script>