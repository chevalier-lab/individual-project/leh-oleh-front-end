<div class="col-12 mb-3">
    <div class="card border rounded-0 h-100">
        <div class="card-body p-0">
            <ul class="nav flex-column chat-menu" id="myTab3" role="tablist">
                <li class="nav-item active px-3 px-md-1 px-xl-3">
                    <div class="media d-block d-flex text-left py-2" id="detail-chat-topbar">
                        <a href="javascript:void(0)" data-toggle="modal" data-target="#modal-update-chat" class="btn btn-danger text-white mt-2">Non Aktif</a>
                    </div>
                </li>
            </ul>
            <div class="scrollerchat p-3" id="detail-chat-worksheet" style="overflow-y: auto; height: 400px">
            </div>
            <div class="border-top theme-border px-2 py-3 d-flex position-relative chat-box">
                <input type="text" class="form-control mr-2" placeholder="Ketik pesan di sini ..." id="detail-chat-message" />
                <a href="javascript:void(0)" class="p-2 ml-2 rounded line-height-21 bg-primary text-white" id="btn-send"><i class="icon-cursor align-middle"></i> Kirim</a>
            </div>
        </div>
    </div>
</div>