<div class="col-lg-4">
    <div class="card">
        <div class="card-header d-flex justify-content-between align-items-center">                               
            <h6 class="card-title">Pengguna Baru</h6>
        </div>
        <div class="card-content">
            <div class="card-body p-0">
                <ul class="list-group list-unstyled"
                    id="new-user-box"></ul> 
            </div>
        </div>
    </div>
</div>

<script>
loadUsers();

function loadUsers() {
    
    order =  "a.id";
    order_direction = "DESC";

    var raw = req.raw({
        page: 0,
        search: "",
        order_by: order,
        order_direction: order_direction
    })

    var formData = new FormData()
    formData.append("raw", raw)

    $.ajax({
        url: base_url.value + "/dashboard/a/managementUsers/load_users",
        data: formData,
        type: "POST",
        contentType: false,
        processData: false,
        success: function(response) {
            response = req.data(response)
            if (response.code == 200) {
                var data = response.data
                $("#new-user-box").html(`${data.map(function(item) {
                    return `
                    <li class="p-2 border-bottom">
                        <div class="media d-flex w-100">
                            <div class="media-body align-self-center pl-2">
                                <span class="mb-0 font-w-600 text-primary">${item.full_name}</span><br>
                                <p class="mb-0 font-w-500 tx-s-12">${item.alamat}</p>                                                    
                            </div>
                        </div> 
                    </li>
                    `;
                }).join('')}`);
            }
        }
    });
}
</script>