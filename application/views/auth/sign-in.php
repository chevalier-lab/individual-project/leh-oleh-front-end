<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view('components/head'); ?>

        <!-- START: Template CSS-->
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/bootstrap/css/bootstrap.min.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.theme.min.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/simple-line-icons/css/simple-line-icons.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/flags-icon/css/flag-icon.min.css'); ?>"> 
        <!-- END Template CSS-->     

        <!-- START: Page CSS-->   
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/social-button/bootstrap-social.css'); ?>"/>   
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/sweetalert/sweetalert.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/toastr/toastr.min.css'); ?>"/>
        <!-- END: Page CSS-->

        <!-- START: Custom CSS-->
        <link rel="stylesheet" href="<?= base_url('assets/dist/css/main.css'); ?>">
        <!-- END: Custom CSS-->
    </head>
    <body id="main-container" class="default bg-white"
    oncontextmenu="return false;">

        <input type="hidden" name="base_url" 
            id="base_url" value="<?= base_url("index.php"); ?>">
        <input type="hidden" name="api_url" 
            id="api_url" value="<?= API_URI; ?>">

        <!-- START: Pre Loader-->
        <div class="se-pre-con">
            <div class="loader"></div>
        </div>
        <!-- END: Pre Loader-->

        <!-- START: Template JS-->
        <script src="<?= base_url('assets/dist/vendors/jquery/jquery-3.3.1.min.js'); ?>"></script>
        <script src="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.js'); ?>"></script>
        <script src="<?= base_url('assets/dist/vendors/moment/moment.js'); ?>"></script>
        <script src="<?= base_url('assets/dist/vendors/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
        <script src="<?= base_url('assets/dist/vendors/slimscroll/jquery.slimscroll.min.js'); ?>"></script>
        <script src="<?= base_url('assets/dist/vendors/toastr/toastr.min.js'); ?>"></script>
        <script>
            var base_url = document.getElementById("base_url");
            var api_url = document.getElementById("api_url");
        </script>
        <!-- END: Template JS--> 

        <!-- Facebook Pixel Code -->
        <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '3542009505894280');
        fbq('track', 'SignInMerchant');
        </script>
        <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=3542009505894280&ev=SignInMerchant&noscript=1"
        /></noscript>
        <!-- End Facebook Pixel Code -->

        <!-- START: APP JS-->
        <script src="<?= base_url('assets/dist/js/app.js'); ?>"></script>
        <script src="<?= base_url('assets/dist/js/auth/request.js'); ?>"></script>
        <!-- END: APP JS-->

        <!-- START: Main Content-->
        <?php
            // Load Secondary Menu
            echo ('<div id="header-fix" class="header">');
                $this->load->view("components/menus/secondary-menu");
                $this->load->view("components/menus/top-bar");
            echo ('</div>');

            // Load Page
             $this->load->view("auth/pages/portal", array(
              "portalActionForm" => "#",
              "portalContentForm" => array(
                    array(
                        "id" => "login-email", 
                        "label" => "Email", 
                        "hint" => "androxxx@email.com",
                        "type" => "text",
                        "is_required" => true
                    ),
                  array(
                      "id" => "login-username", 
                      "label" => "Username", 
                      "hint" => "androxxx",
                      "type" => "text",
                      "is_required" => true
                  ),
                  array(
                      "id" => "login-password", 
                      "label" => "Password", 
                      "hint" => "xxxxxxxxx",
                      "type" => "password",
                      "is_required" => true
                  )
              ),
              "portalButtonForm" => '
              <div class="form-group mb-3">
                  <button class="btn btn-github text-white" 
                    type="button"
                    data-toggle="modal" data-target="#modal-lost-password"> Lupa Password </button>
                  <button class="ml-auto btn btn-primary" type="button" id="btn-login"> Masuk </button>
                  <div id="btn-login-loader"></div>
              </div>
              <p>----- Atau masuk dengan -----</p>',
              "portalUtilityForm" => '
              <a class="btn btn-social btn-facebook text-white mb-2"
                onclick="signInWithFacebook()">
                  <i class="icon-social-facebook align-middle"></i>
                  Facebook
              </a>
              <a class="btn btn-social btn-google text-white mb-2"
                id="sign-in-with-google"
                onclick="signInWithGoogle()">
                  <i class="icon-social-google align-middle"></i>
                  Google
              </a>
              
              <div class="mt-2">Belum punya akun? 
                  <a href="'.base_url("index.php/general/auth/registration").'">Daftar sekarang</a>
              </div>'
             ));
        ?>
        <!-- END: Content-->

        <!-- START: Page Script JS-->
        <?php
            if (isset($error)) {
                $this->load->view("components/error-modal", array(
                    "errorModalTitle" => $error["title"],
                    "errorModalContent" => $error["content"],
                    "errorModalDetail" => $error["details"]
                ));
            }

            $this->load->view("components/modals/lost-password", array(
                "hideModal" => "hide",
                "isSuccess" => false
            ));
        ?>

        <!-- The core Firebase JS SDK is always required and must be listed first -->
        <script src="https://www.gstatic.com/firebasejs/8.2.1/firebase-app.js"></script>

        <!-- TODO: Add SDKs for Firebase products that you want to use
            https://firebase.google.com/docs/web/setup#available-libraries -->
        <script src="https://www.gstatic.com/firebasejs/8.2.1/firebase-auth.js"></script>
        <script src="https://www.gstatic.com/firebasejs/8.2.1/firebase-analytics.js"></script>

        <script>
        // Your web app's Firebase configuration
        // For Firebase JS SDK v7.20.0 and later, measurementId is optional
        var firebaseConfig = {
            apiKey: "AIzaSyBwtK96JsLn0q5CntJs37eQmK0LsUubzc0",
            authDomain: "leholeh-26b25.firebaseapp.com",
            projectId: "leholeh-26b25",
            storageBucket: "leholeh-26b25.appspot.com",
            messagingSenderId: "57383597470",
            appId: "1:57383597470:web:6bb509b9535b02ae47128f",
            measurementId: "G-LQ2YXY4Q83"
        };
        var webConfig = {
            clientId: "57383597470-h44qso9ti8ur0nt0ii8tkqkq8eand1eu.apps.googleusercontent.com",
            clientSecret: "PyYAnkr52jH6prtaznSBryUx"
        }
        // Initialize Firebase
        firebase.initializeApp(firebaseConfig);
        firebase.analytics();
        </script>

        <script>
            (function ($) {
                "use strict";
                $(window).on("load", function () {
                    $("#group-login-email").hide();

                    // Handle Login
                    $("#btn-login").on("click", function() {
                        
                        signIn(0);

                        // $("#sign-in-with-google").on("click", function() {
                        //     signInWithGoogle();
                        // });

                    });
                });

                document.getElementById('login-password').addEventListener('keyup', ({key}) => {
                    if (key === "Enter") document.getElementById('btn-login').click();
                })
            })(jQuery);

            function signIn(type) {

                $("#btn-login").hide();
                $("#btn-login-loader").html(`
                <div class="loader-item"></div>
                `);

                var dataRaw = {
                    "username": $("#login-username").val(),
                    "password": $("#login-password").val(),
                }

                if (type == 1) {
                    dataRaw.email = $("#login-email").val()
                }

                var json = req.raw(dataRaw)

                var formData = new FormData();
                formData.append("raw", json);

                $.ajax({
                    url: base_url.value + "/general/auth/do_login",
                    data: formData,
                    type: "POST",
                    contentType: false,
                    processData: false,
                    success: function(response) {
                        console.log("SUCCESS", response);
                        response = req.data(response)
                        if (response.code == 200) {
                            toastr.success("Berhasil Masuk");
                            setTimeout(() => {
                                location.assign(base_url.value + 
                                "/general/auth/set_auth?raw=" + req.raw(response.data))
                            }, 2000);
                        } else {

                            if ($("#login-email").val() != "") {
                                firebase.auth().signOut().then(function() {
                                // Sign-out successful.
                                }).catch(function(error) {
                                // An error happened.
                                });
                            }

                            $("#btn-login").show();
                            $("#btn-login-loader").html(``);
                            $("#form-loader").html(``);
                            $("#form-portal").show();

                            $("#login-email").val("")
                            $("#login-username").val("")
                            $("#login-password").val("")

                            toastr.error(response.message);
                        }
                    }
                    // ,
                    // error: function(response) {
                    //     console.log("ERROR", response);
                    //     $("#btn-login").removeAttr("disabled");
                    // }
                });

            }

            function signInWithFacebook() {
                var provider = new firebase.auth.FacebookAuthProvider();

                $("#form-loader").html(`
                <div class="loader-item"></div>`);
                $("#form-portal").hide();

                firebase.auth().signInWithPopup(provider).then(function(result) {
                    // This gives you a Facebook Access Token. You can use it to access the Facebook API.
                    var token = result.credential.accessToken;
                    // The signed-in user info.
                    var user = result.user;
                    // ...
                    console.log("SUCCESS LOGIN", user);
                    console.log("SUCCESS LOGIN 2", result);

                    $("#login-email").val(user.email)
                    $("#login-username").val(user.uid)
                    $("#login-password").val(user.uid)

                    signIn(1)

                }).catch(function(error) {
                    // Handle Errors here.
                    var errorCode = error.code;
                    var errorMessage = error.message;
                    // The email of the user's account used.
                    var email = error.email;
                    // The firebase.auth.AuthCredential type that was used.
                    var credential = error.credential;
                    // ...

                    $("#form-loader").html(``);
                    $("#form-portal").show();

                    $("#login-email").val("")
                    $("#login-username").val("")
                    $("#login-password").val("")

                    console.log("FAILED LOGIN", error);
                    toastr.error("Gagal melakukan authentikasi dengan facebook");
                });

            }

            function signInWithGoogle() {
                var provider = new firebase.auth.GoogleAuthProvider();

                $("#form-loader").html(`
                <div class="loader-item"></div>`);
                $("#form-portal").hide();

                firebase.auth().signInWithPopup(provider).then(function(result) {
                    // This gives you a Google Access Token. You can use it to access the Google API.
                    var token = result.credential.accessToken;
                    // The signed-in user info.
                    var user = result.user;
                    // ...
                    console.log("SUCCESS LOGIN", user);
                    console.log("SUCCESS LOGIN 2", result);

                    $("#login-email").val(user.email)
                    $("#login-username").val(user.uid)
                    $("#login-password").val(user.uid)

                    signIn(1)

                }).catch(function(error) {
                    // Handle Errors here.
                    var errorCode = error.code;
                    var errorMessage = error.message;
                    // The email of the user's account used.
                    var email = error.email;
                    // The firebase.auth.AuthCredential type that was used.
                    var credential = error.credential;

                    $("#form-loader").html(``);
                    $("#form-portal").show();

                    $("#login-email").val("")
                    $("#login-username").val("")
                    $("#login-password").val("")

                    console.log("FAILED LOGIN", error);
                    toastr.error("Gagal melakukan authentikasi dengan facebook");
                    // ...
                });

            }
        </script>
        <!-- END: Page Script JS-->
    </body>
</html>