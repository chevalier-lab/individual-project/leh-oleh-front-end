<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('components/head'); ?>

    <!-- START: Template CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.theme.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/simple-line-icons/css/simple-line-icons.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/flags-icon/css/flag-icon.min.css'); ?>">
    <!-- END Template CSS-->

    <!-- START: Page CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/social-button/bootstrap-social.css'); ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/sweetalert/sweetalert.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/toastr/toastr.min.css'); ?>" />
    <!-- END: Page CSS-->

    <!-- START: Custom CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/css/main.css'); ?>">
    <!-- END: Custom CSS-->
</head>

<body id="main-container" class="default">

    <input type="hidden" name="base_url" id="base_url" value="<?= base_url("index.php"); ?>">
    <input type="hidden" name="api_url" id="api_url" value="<?= API_URI; ?>">

    <!-- START: Pre Loader-->
    <div class="se-pre-con">
        <div class="loader"></div>
    </div>
    <!-- END: Pre Loader-->

    <!-- START: Template JS-->
    <script src="<?= base_url('assets/dist/vendors/jquery/jquery-3.3.1.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/moment/moment.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/slimscroll/jquery.slimscroll.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/toastr/toastr.min.js'); ?>"></script>
    <script>
        var base_url = document.getElementById("base_url");
        var api_url = document.getElementById("api_url");
    </script>
    <!-- END: Template JS-->

    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window, document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '3542009505894280');
    fbq('track', 'SignUpMerchant');
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=3542009505894280&ev=SignUpMerchant&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->

    <!-- START: APP JS-->
    <script src="<?= base_url('assets/dist/js/app.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/js/auth/request.js'); ?>"></script>
    <!-- END: APP JS-->

    <!-- START: Main Content-->
    <?php
    // Load Secondary Menu
    echo ('<div id="header-fix" class="header">');
    $this->load->view("components/menus/secondary-menu");
    $this->load->view("components/menus/top-bar");
    echo ('</div>');
    ?>

    <div class="container card my-5">
        <div class="card-body">
            <div class="row justify-content-between align-items-center">
                <div class="col-12">
                    <!-- FORM -->
                    <form 
                        action="#" 
                        class="row row-eq-height my-2">

                        <div class="col-12">
                            <div class="form-group">
                                <img src="<?= base_url('assets/dist/images/logo.png'); ?>"
                                alt="" class="img-fluid">
                                <hr>
                            </div>
                        </div>

                        <div class="col-12">
                            <p>*Harap mengisi seluruh form</p>
                            <h3>Data Pengguna</h3>
                        </div>

                        <div class="col-12 col-sm-6">
                            <div class="form-group mb-3">
                                <label for="registration-full_name">Nama Lengkap</label>
                                <input class="form-control" 
                                    type="text" 
                                    id="registration-full_name" 
                                    required
                                    placeholder="Andro Example">
                            </div>
                        </div>

                        <div class="col-12 col-sm-6">
                            <div class="form-group mb-3">
                                <label for="registration-username">Username</label>
                                <input class="form-control" 
                                    type="text" 
                                    id="registration-username" 
                                    required
                                    placeholder="androxxx">
                            </div>
                        </div>

                        <div class="col-12 col-sm-6">
                            <div class="form-group mb-3">
                                <label for="registration-password">Password</label>
                                <input class="form-control" 
                                    type="password" 
                                    id="registration-password" 
                                    required
                                    placeholder="xxxxxxxxx">
                            </div>
                        </div>

                        <div class="col-12 col-sm-6">
                            <div class="form-group mb-3">
                                <label for="registration-gender">Jenis Kelamin</label>
                                <select class="form-control" 
                                    id="registration-gender" 
                                    required
                                    placeholder="xxxxxxxxx">
                                    <option value="l">Laki-laki</option>
                                    <option value="p">Perempuan</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-12 col-sm-6">
                            <div class="form-group mb-3">
                                <label for="registration-date_of_birth">Tanggal Lahir</label>
                                <input class="form-control" 
                                    type="date" 
                                    id="registration-date_of_birth" 
                                    required
                                    placeholder="dd MM YYYY">
                            </div>
                        </div>

                        <div class="col-12 col-sm-6">
                            <div class="form-group mb-3">
                                <label for="registration-address">Alamat</label>
                                <input class="form-control" 
                                    type="text" 
                                    id="registration-address" 
                                    required
                                    placeholder="Jl.xxxxxx">
                            </div>
                        </div>

                        <div class="col-12">
                            <hr>
                            <h3>Data Toko</h3>
                        </div>

                        <div class="col-12 col-sm-6">
                            <div class="form-group mb-3">
                                <label for="registration-identity">No Identitas (KTP)</label>
                                <input class="form-control" 
                                    type="number" 
                                    id="registration-identity" 
                                    required
                                    placeholder="12345xxx">
                            </div>
                        </div>

                        <div class="col-12 col-sm-6">
                            <div class="form-group mb-3">
                                <label for="registration-market_name">Nama Toko</label>
                                <input class="form-control" 
                                    type="text" 
                                    id="registration-market_name" 
                                    required
                                    placeholder="Toko Oleh-oleh">
                            </div>
                        </div>

                        <div class="col-12 col-sm-6">
                            <div class="form-group mb-3">
                                <label for="registration-market_phone">Nomor Telepon Toko</label>
                                <input class="form-control" 
                                    type="text" 
                                    id="registration-market_phone" 
                                    required
                                    placeholder="Toko Oleh-oleh">
                            </div>
                        </div>

                        <div class="col-12 col-sm-6">
                            <div class="form-group mb-3">
                                <label for="registration-market_address">Alamat Toko</label>
                                <input class="form-control" 
                                    type="text" 
                                    id="registration-market_address" 
                                    required
                                    placeholder="Jl.xxxxxx">
                            </div>
                        </div>

                        <div class="col-12 col-sm-6">
                            <div class="form-group mb-3">
                                <label for="registration-photo_user">Foto Pengguna</label>
                                <input class="form-control" 
                                    type="file" 
                                    id="registration-photo_user" 
                                    required
                                    placeholder="foto pengguna">
                            </div>
                        </div>

                        <div class="col-12 col-sm-6">
                            <div class="form-group mb-3">
                                <label for="registration-photo_market">Foto Toko</label>
                                <input class="form-control" 
                                    type="file" 
                                    id="registration-photo_market" 
                                    required
                                    placeholder="foto toko">
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group mb-0">
                                <div class="form-group mb-3">
                                    <button class="ml-auto btn btn-primary" type="button"
                                        id="btn-registration"> Daftar </button>

                                    <div id="btn-registration-loader"></div>
                                </div>
                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>
    <!-- END: Content-->

    <!-- START: Page Script JS-->
    <?php
    if (isset($error)) {
        $this->load->view("components/error-modal", array(
            "errorModalTitle" => $error["title"],
            "errorModalContent" => $error["content"],
            "errorModalDetail" => $error["details"]
        ));
    }

    // $this->load->view("components/modals/otp", array(
    //     "hideModal" => "show",
    //     "isSuccess" => true
    // ));
    ?>

    <script>
        (function($) {
            "use strict";
            $(window).on("load", function() {
                // Handle Login
                $("#btn-registration").on("click", function() {

                    var full_name = $('#registration-full_name').val();
                    var username = $('#registration-username').val();
                    var password = $('#registration-password').val();
                    var gender = $("#registration-gender").children("option:selected").val();
                    var birth_date = $('#registration-birth_date').val();
                    var address = $('#registration-address').val();
                    var no_identity = $('#registration-identity').val();
                    var market_address = $('#registration-market_address').val();
                    var market_name = $('#registration-market_name').val();
                    var market_phone_number = $('#registration-market_phone').val();
                    var foto_pengguna = $('#registration-photo_user').prop("files")[0];
                    var foto_toko = $('#registration-photo_market').prop("files")[0]

                    if (full_name == '') {
                        console.log("Harap isi nama lengkap");
                        toastr.error('Harap isi nama lengkap');
                        return;
                    }
                    if (username == '') {
                        console.log("Harap isi username");
                        toastr.error('Harap isi username');
                        return;
                    }
                    if (password == '') {
                        console.log("Harap isi password");
                        toastr.error('Harap isi password');
                        return;
                    }
                    if (gender == '') {
                        console.log("Harap isi jenis kelamin");
                        toastr.error('Harap isi jenis kelamin');
                        return;
                    }
                    if (birth_date == '') {
                        console.log("Harap isi tanggal lahir");
                        toastr.error('Harap isi tanggal lahir');
                        return;
                    }
                    if (address == '') {
                        console.log("Harap isi alamat");
                        toastr.error('Harap isi alamat');
                        return;
                    }
                    if (no_identity == '') {
                        console.log("Harap isi no ktp");
                        toastr.error('Harap isi no ktp');
                        return;
                    }
                    if (market_address == '') {
                        console.log("Harap isi alamat toko");
                        toastr.error('Harap isi alamat toko');
                        return;
                    }
                    if (market_name == '') {
                        console.log("Harap isi nama toko");
                        toastr.error('Harap isi nama toko');
                        return;
                    }
                    if (market_phone_number == '') {
                        console.log("Harap isi nomor telepon toko");
                        toastr.error('Harap isi nomor telepon toko');
                        return;
                    }
                    if (foto_pengguna == null) {
                        console.log("Harap isi foto pengguna");
                        toastr.error('Harap isi foto pengguna');
                        return;
                    }
                    if (foto_toko == null) {
                        console.log("Harap isi foto toko");
                        toastr.error('Harap isi foto toko');
                        return;
                    }

                    $("#btn-registration").hide();
                    $("#btn-registration-loader").html(`
                    <div class="loader-item"></div>
                    `);

                    var formData = new FormData()
                    formData.append("full_name", full_name)
                    formData.append("username", username)
                    formData.append("password", password)
                    formData.append("gender", gender)
                    formData.append("birth_date", birth_date)
                    formData.append("address", address)
                    formData.append("no_identity", no_identity)
                    formData.append("market_address", market_address)
                    formData.append("market_name", market_name)
                    formData.append("market_phone_number", market_phone_number)
                    formData.append("foto_pengguna", foto_pengguna)
                    formData.append("foto_toko", foto_toko)

                    $.ajax({
                        url: base_url.value + "/anonymous/RegistrationMerchant/do_registration",
                        data: formData,
                        type: "POST",
                        contentType: false,
                        processData: false,
                        success: function(response) {
                            console.log(response)
                            response = req.data(response)

                            if (response.code == 200) {
                                toastr.success("Berhasil melakukan pendaftaran");
                                setTimeout(() => {
                                    location.assign(base_url.value + "/general/auth/login")
                                }, 1000);
                            } else {
                                $("#btn-registration").show();
                                $("#btn-registration-loader").html(``);
                                toastr.error(response.message);
                            }
                        }
                    });
                });
            });
        })(jQuery);
    </script>
    <!-- END: Page Script JS-->
</body>

</html>