<?php
/**
 * HOW TO CALL PORTAL
 * $this->load->view("auth/pages/portal", array(
 *  "portalActionForm" => "#",
 *  "portalContentForm" => array(
 *      array(
 *          "id" => "username", 
 *          "label" => "Username", 
 *          "hint" => "Insert Username",
 *          "type": "text",
 *          "is_required" => true
 *      )
 *  ),
 *  "portalButtonForm" => "
 *  <div class="form-group mb-0">
 *      <button class="btn btn-github text-white" type="button"> Lupa Password </button>
 *      <button class="ml-auto btn btn-primary" type="submit"> Log In </button>
 *  </div>",
 *  "portalUtilityForm" => "
 *  <a class="btn btn-social btn-facebook text-white mb-2">
 *      <i class="icon-social-facebook align-middle"></i>
 *      Facebook
 *  </a>
 *  <a class="btn btn-social btn-google text-white mb-2">
 *      <i class="icon-social-google align-middle"></i>
 *      Google
 *  </a>
 *  
 *  <div class="mt-2">Belum punya akun? 
 *      <a href="<?= base_url('index.php/general/auth/registration'); ?>">Daftar sekarang</a>
 *  </div>"
 * ));
 */
?>
<div class="container card my-5">
    <div class="card-body">
        <div class="row justify-content-between align-items-center">
            <div class="col-12">
                <!-- FORM -->
                <form 
                    action="<?= isset($portalActionForm) ? $portalActionForm : '#'; ?>" 
                    class="row row-eq-height my-2">
                    <!-- BANNER LEFT -->
                    <div class="lock-image col-12 col-sm-6">
                        <img src="<?= base_url('assets/dist/images/ilustration.png'); ?>" style="max-width: 100%" />
                    </div>

                    <!-- FORM CONTENT -->
                    <div class="login-form col-12 col-sm-6">
                        <div class="form-group">
                            <img src="<?= base_url('assets/dist/images/logo.png'); ?>"
                            alt="" class="img-fluid">
                            <hr>
                        </div>
                        <div id="form-loader"></div>
                        <div id="form-portal">
                        
                        <?php
                            // Form Content
                            if (isset($portalContentForm)) {
                                foreach($portalContentForm as $item) {
                                    if ($item["type"] == "select") {
                                        ?>
                                        <div class="form-group mb-3" id="group-<?= $item['id'] ?>">
                                            <label for="<?= $item['id']; ?>"><?= $item['label']; ?></label>
                                            <select class="form-control" 
                                                type="<?= $item['type']; ?>" 
                                                id="<?= $item['id']; ?>" 
                                                <?= isset($item['is_required']) ? "required":""; ?> 
                                                placeholder="<?= $item['hint']; ?>">
                                                <?php
                                                    foreach($item["data"] as $itemData) {
                                                        ?>
                                                        <option value="<?= $itemData['value']; ?>"><?= $itemData['label']; ?></option>
                                                        <?php
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                        <?php
                                    } else {
                                        ?>
                                        <div class="form-group mb-3" id="group-<?= $item['id'] ?>">
                                            <label for="<?= $item['id']; ?>"><?= $item['label']; ?></label>
                                            <input class="form-control" 
                                                type="<?= $item['type']; ?>" 
                                                id="<?= $item['id']; ?>" 
                                                <?= isset($item['is_required']) ? "required":""; ?> 
                                                placeholder="<?= $item['hint']; ?>">
                                        </div>
                                        <?php
                                    }
                                }
                            }
                            
                            // Button Form
                            if (isset($portalButtonForm)) {
                                ?>
                                <div class="form-group mb-0">
                                    <?= $portalButtonForm; ?>
                                </div>
                                <?php
                            }

                            // Utility Form
                            if (isset($portalUtilityForm)) {
                                ?>
                                <div class="form-group mb-0">
                                    <?= $portalUtilityForm; ?>
                                </div>
                                <?php
                            }
                        ?>

                        </div>

                    </div>
                </form>

            </div>
        </div>
    </div>
</div>