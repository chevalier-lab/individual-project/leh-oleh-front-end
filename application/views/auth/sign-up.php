<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view('components/head'); ?>

        <!-- START: Template CSS-->
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/bootstrap/css/bootstrap.min.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.theme.min.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/simple-line-icons/css/simple-line-icons.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/flags-icon/css/flag-icon.min.css'); ?>"> 
        <!-- END Template CSS-->     

        <!-- START: Page CSS-->   
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/social-button/bootstrap-social.css'); ?>"/>   
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/sweetalert/sweetalert.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/toastr/toastr.min.css'); ?>"/>
        <!-- END: Page CSS-->

        <!-- START: Custom CSS-->
        <link rel="stylesheet" href="<?= base_url('assets/dist/css/main.css'); ?>">
        <!-- END: Custom CSS-->
    </head>
    <body id="main-container" class="default bg-white"
    oncontextmenu="return false;">

        <input type="hidden" name="base_url" 
            id="base_url" value="<?= base_url("index.php"); ?>">
        <input type="hidden" name="api_url" 
            id="api_url" value="<?= API_URI; ?>">

        <!-- START: Pre Loader-->
        <div class="se-pre-con">
            <div class="loader"></div>
        </div>
        <!-- END: Pre Loader-->

        <!-- START: Template JS-->
        <script src="<?= base_url('assets/dist/vendors/jquery/jquery-3.3.1.min.js'); ?>"></script>
        <script src="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.js'); ?>"></script>
        <script src="<?= base_url('assets/dist/vendors/moment/moment.js'); ?>"></script>
        <script src="<?= base_url('assets/dist/vendors/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
        <script src="<?= base_url('assets/dist/vendors/slimscroll/jquery.slimscroll.min.js'); ?>"></script>
        <script src="<?= base_url('assets/dist/vendors/toastr/toastr.min.js'); ?>"></script>
        <script>
            var base_url = document.getElementById("base_url");
            var api_url = document.getElementById("api_url");
        </script>
        <!-- END: Template JS--> 

        <!-- Facebook Pixel Code -->
        <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '3542009505894280');
        fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=3542009505894280&ev=PageView&noscript=1"
        /></noscript>
        <!-- End Facebook Pixel Code -->

        <!-- START: APP JS-->
        <script src="<?= base_url('assets/dist/js/app.js'); ?>"></script>
        <script src="<?= base_url('assets/dist/js/auth/request.js'); ?>"></script>
        <!-- END: APP JS-->

        <!-- START: Main Content-->
        <?php
            // Load Secondary Menu
            echo ('<div id="header-fix" class="header">');
                $this->load->view("components/menus/secondary-menu");
                $this->load->view("components/menus/top-bar");
            echo ('</div>');

            // Load Page
             $this->load->view("auth/pages/portal", array(
              "portalActionForm" => "#",
              "portalContentForm" => array(
                  array(
                      "id" => "registration-full_name", 
                      "label" => "Nama Lengkap", 
                      "hint" => "Andro Example",
                      "type" => "text",
                      "is_required" => true
                  ),
                  array(
                      "id" => "registration-email", 
                      "label" => "Email", 
                      "hint" => "androxxx@email.com",
                      "type" => "text",
                      "is_required" => true
                  ),
                  array(
                      "id" => "registration-username", 
                      "label" => "Username", 
                      "hint" => "androxxx",
                      "type" => "text",
                      "is_required" => true
                  ),
                  array(
                      "id" => "registration-password", 
                      "label" => "Password", 
                      "hint" => "xxxxxxxxx",
                      "type" => "password",
                      "is_required" => true
                  ),
                  array(
                      "id" => "registration-gender", 
                      "label" => "Jenis Kelamin", 
                      "hint" => "xxxxxxxxx",
                      "type" => "select",
                      "is_required" => true,
                      "data" => array(
                          array("label" => "Laki-laki", "value" => "l"),
                          array("label" => "Perempuan", "value" => "p")
                      )
                  ),
                  array(
                      "id" => "registration-date_of_birth", 
                      "label" => "Tanggal Lahir", 
                      "hint" => "dd MM YYYY",
                      "type" => "date",
                      "is_required" => true
                  ),
                  array(
                      "id" => "registration-address", 
                      "label" => "Alamat", 
                      "hint" => "Jl.xxxxxx",
                      "type" => "text",
                      "is_required" => true
                  )
              ),
              "portalButtonForm" => '
              <div class="form-group mb-3">
                  <button class="ml-auto btn btn-primary" type="button"
                    id="btn-registration"> Daftar </button>

                    <div id="btn-registration-loader"></div>
              </div>
              <p>----- Atau daftar dengan -----</p>',
              "portalUtilityForm" => '
              <a class="btn btn-social btn-facebook text-white mb-2"
                onclick="signInWithFacebook()">
                  <i class="icon-social-facebook align-middle"></i>
                  Facebook
              </a>
              <a class="btn btn-social btn-google text-white mb-2"
              onclick="signInWithGoogle()">
                  <i class="icon-social-google align-middle"></i>
                  Google
              </a>
              
              <div class="mt-2">Sudah punya akun? 
                  <a href="'.base_url("index.php/general/auth/login").'">Masuk sekarang</a>
              </div>'
             ));
        ?>
        <!-- END: Content-->

        <!-- START: Page Script JS-->
        <?php
            if (isset($error)) {
                $this->load->view("components/error-modal", array(
                    "errorModalTitle" => $error["title"],
                    "errorModalContent" => $error["content"],
                    "errorModalDetail" => $error["details"]
                ));
            }

            // $this->load->view("components/modals/otp", array(
            //     "hideModal" => "show",
            //     "isSuccess" => true
            // ));
        ?>

        <!-- The core Firebase JS SDK is always required and must be listed first -->
        <script src="https://www.gstatic.com/firebasejs/8.2.1/firebase-app.js"></script>

        <!-- TODO: Add SDKs for Firebase products that you want to use
            https://firebase.google.com/docs/web/setup#available-libraries -->
        <script src="https://www.gstatic.com/firebasejs/8.2.1/firebase-auth.js"></script>
        <script src="https://www.gstatic.com/firebasejs/8.2.1/firebase-analytics.js"></script>

        <script>
        // Your web app's Firebase configuration
        // For Firebase JS SDK v7.20.0 and later, measurementId is optional
        var firebaseConfig = {
            apiKey: "AIzaSyBwtK96JsLn0q5CntJs37eQmK0LsUubzc0",
            authDomain: "leholeh-26b25.firebaseapp.com",
            projectId: "leholeh-26b25",
            storageBucket: "leholeh-26b25.appspot.com",
            messagingSenderId: "57383597470",
            appId: "1:57383597470:web:6bb509b9535b02ae47128f",
            measurementId: "G-LQ2YXY4Q83"
        };
        var webConfig = {
            clientId: "57383597470-h44qso9ti8ur0nt0ii8tkqkq8eand1eu.apps.googleusercontent.com",
            clientSecret: "PyYAnkr52jH6prtaznSBryUx"
        }
        // Initialize Firebase
        firebase.initializeApp(firebaseConfig);
        firebase.analytics();
        </script>

        <script>
            (function ($) {
                "use strict";
                $(window).on("load", function () {
                    // Handle Login
                    $("#group-registration-email").hide()
                    $("#btn-registration").on("click", function() {

                        if ($("#registration-full_name").val() == "") {
                            toastr.error("Harap mengisi nama lengkap.");
                            return;
                        }
                        else if ($("#registration-username").val() == "") {
                            toastr.error("Harap mengisi username.");
                            return;
                        }
                        else if ($("#registration-password").val() == "") {
                            toastr.error("Harap mengisi password.");
                            return;
                        }
                        else if ($("#registration-date_of_birth").val() == "") {
                            toastr.error("Harap mengisi tanggal lahir.");
                            return;
                        }
                        else if ($("#registration-address").val() == "") {
                            toastr.error("Harap mengisi alamat.");
                            return;
                        }

                        var dataRaw = {
                            full_name: $("#registration-full_name").val(),
                            username: $("#registration-username").val(),
                            password: $("#registration-password").val(),
                            gender: $("#registration-gender").children("option:selected").val(),
                            birth_date: $("#registration-date_of_birth").val(),
                            address: $("#registration-address").val()
                        }

                        if ($("#registration-email").val() != "") {
                            dataRaw.email = $("#registration-email").val()
                        }

                        var raw = req.raw(dataRaw)

                        $("#btn-registration").hide();
                        $("#btn-registration-loader").html(`
                        <div class="loader-item"></div>
                        `);

                        console.log(raw)

                        var formData = new FormData()
                        formData.append("raw", raw)
                        
                        $.ajax({
                            url: base_url.value + "/general/auth/do_registration",
                            data: formData,
                            type: "POST",
                            contentType: false,
                            processData: false,
                            success: function(response) {
                                console.log(response)
                                response = req.data(response)

                                if (response.code == 200) {
                                    toastr.success("Berhasil melakukan pendaftaran");
                                    setTimeout(() => {
                                        location.assign(base_url.value + "/general/auth/login")
                                    }, 1000);
                                } else {

                                    if ($("#registration-email").val() != "") {
                                        firebase.auth().signOut().then(function() {
                                        // Sign-out successful.
                                        }).catch(function(error) {
                                        // An error happened.
                                        });
                                    }

                                    $("#btn-registration").show();
                                    $("#btn-registration-loader").html(``);
                                    $("#form-loader").html(``);
                                    $("#form-portal").show();

                                    $("#registration-full_name").val("")
                                    $("#registration-email").val("")
                                    $("#registration-username").val("")
                                    $("#registration-password").val("")
                                    
                                    $("#group-registration-email").hide()
                                    $("#group-registration-username").show()
                                    $("#group-registration-password").show()

                                    toastr.error(response.message);
                                }
                            }
                            // ,
                            // error: function(response) {
                            //     console.log("ERROR", response);
                            //     $("#btn-login").removeAttr("disabled");
                            // }
                        });
                    });
                });
            })(jQuery);

            function signInWithFacebook() {
                var provider = new firebase.auth.FacebookAuthProvider();

                $("#form-loader").html(`
                <div class="loader-item"></div>`);
                $("#form-portal").hide();

                firebase.auth().signInWithPopup(provider).then(function(result) {
                    // This gives you a Facebook Access Token. You can use it to access the Facebook API.
                    var token = result.credential.accessToken;
                    // The signed-in user info.
                    var user = result.user;
                    // ...
                    console.log("SUCCESS LOGIN", user);
                    console.log("SUCCESS LOGIN 2", result);

                    $("#registration-full_name").val(user.displayName)
                    $("#registration-email").val(user.email)
                    $("#registration-username").val(user.uid)
                    $("#registration-password").val(user.uid)

                    $("#group-registration-email").show()
                    $("#group-registration-username").hide()
                    $("#group-registration-password").hide()

                    signIn(1)

                }).catch(function(error) {
                    // Handle Errors here.
                    var errorCode = error.code;
                    var errorMessage = error.message;
                    // The email of the user's account used.
                    var email = error.email;
                    // The firebase.auth.AuthCredential type that was used.
                    var credential = error.credential;
                    // ...

                    $("#form-loader").html(``);
                    $("#form-portal").show();

                    $("#registration-full_name").val("")
                    $("#registration-email").val("")
                    $("#registration-username").val("")
                    $("#registration-password").val("")

                    $("#group-registration-email").hide()
                    $("#group-registration-username").show()
                    $("#group-registration-password").show()

                    console.log("FAILED LOGIN", error);
                    toastr.error("Gagal melakukan authentikasi dengan facebook");
                });

            }

            function signInWithGoogle() {
                var provider = new firebase.auth.GoogleAuthProvider();

                $("#form-loader").html(`
                <div class="loader-item"></div>`);
                $("#form-portal").hide();

                firebase.auth().signInWithPopup(provider).then(function(result) {
                    // This gives you a Google Access Token. You can use it to access the Google API.
                    var token = result.credential.accessToken;
                    // The signed-in user info.
                    var user = result.user;
                    // ...
                    console.log("SUCCESS LOGIN", user);
                    console.log("SUCCESS LOGIN 2", result);

                    $("#registration-full_name").val(user.displayName)
                    $("#registration-email").val(user.email)
                    $("#registration-username").val(user.uid)
                    $("#registration-password").val(user.uid)
                    
                    $("#group-registration-email").show()
                    $("#group-registration-username").hide()
                    $("#group-registration-password").hide()

                    $("#form-loader").html(``);
                    $("#form-portal").show();
                }).catch(function(error) {
                    // Handle Errors here.
                    var errorCode = error.code;
                    var errorMessage = error.message;
                    // The email of the user's account used.
                    var email = error.email;
                    // The firebase.auth.AuthCredential type that was used.
                    var credential = error.credential;

                    console.log("FAILED LOGIN", error);

                    $("#registration-full_name").val("")
                    $("#registration-email").val("")
                    $("#registration-username").val("")
                    $("#registration-password").val("")
                    
                    $("#group-registration-email").hide()
                    $("#group-registration-username").show()
                    $("#group-registration-password").show()

                    // ...
                    $("#form-loader").html(``);
                    $("#form-portal").show();
                    toastr.error("Gagal mendaftar dengan akun google")
                });

            }
        </script>
        <!-- END: Page Script JS-->
    </body>
</html>