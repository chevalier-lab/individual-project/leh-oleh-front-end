<?php
    // Detail Top Up Proof Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-create-top-up",
        "modalTitle" => "Tarik Saldo",
        "modalType" => "modal-md",
        "iconTitle" => "icon-plus",
        "modalActionForm" => "#",
        "modalContentForm" => '
            <div class="form-group mb-3">
                <label for="create-wallet">Pilih Dompet</label>
                <select id="create-wallet" class="form-control"> 
                </select>
            </div>
            <div class="form-group mb-3">
                <label for="create-account_bank">Pilih Akun Bank</label>
                <select id="create-account_bank" class="form-control"> 
                </select>
                <div id="account-bank-new" class="mt-2"></div>
            </div>
            <div class="form-group mb-3">
                <label for="create-balance">Nominal Penarikan</label>
                <input type="number" id="create-balance" class="form-control"
                placeholder="Contoh: 10000"/> 
                <sub class="text-danger">Minimum Withdraw Rp10.000</sub><br>
                <sub class="text-success">Saldo saat ini <strong id="create-current-balance"></strong></sub>
            </div>
        ',
        "modalButtonForm" => '<button type="button" class="btn btn-primary" 
        id="btn-create-top-up">Kirim
        <i class="icon-paper-plane"></i>
        </button>

        <div id="create-top-up-loader"></div>
        '
    ));
?>

<div class="card mb-3">
    <div class="card-header">
        <h4 class="card-title">Tari Saldo</h4>
    </div>
    <div class="card-body">

        <ul class="nav nav-tabs pl-2 pr-2 mb-3" id="list-topup-tab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="list-topup-active-tab" data-toggle="tab" href="#list-topup-active" 
                onclick="loadSwitch(0)"
                role="tab" aria-controls="list-topup-active" aria-selected="true">Tagihan</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="list-topup-history-tab" data-toggle="tab" 
                onclick="loadSwitch(1)"
                href="#list-topup-history" role="tab" aria-controls="list-topup-history" aria-selected="false">Riwayat Penarikan</a>
            </li>
        </ul>

        <div class="tab-content" id="list-topup-tab-content">

            <div class="tab-pane fade show active" id="list-topup-active" role="tabpanel" 
            aria-labelledby="list-topup-active-tab">
                <div class="pt-0 pb-0 pl-3 pr-3 mb-3 row">
                    <div class="input-group col-12 col-md-5 p-1">
                        <input type="text" class="form-control p-2 w-100 h-100 contact-search" 
                            placeholder="Cari ..."
                            id="lists-topup-active-search">
                            <div class="input-group-append">
                                <span class="btn btn-outline-primary input-group-text" id="lists-topup-active-button-search"><i class="icon-magnifier"
                                    aria-hidden="true"></i></span>
                            </div>
                    </div>
                    <div class="col-12 col-md-5 p-1">
                        <select class="form-control p-2 w-100 h-100 contact-search"
                            id="lists-topup-active-order">
                            <option value="0">Urutkan berdasarkan</option>
                            <option value="1">Nama (A-Z)</option>
                            <option value="2">Nama (Z-A)</option>
                        </select>
                    </div>
                    <div class="col-12 col-md-2 pl-1 pr-1 my-2">
                        <button data-toggle="modal" data-target="#modal-create-top-up" 
                        class="btn btn-outline-secondary btn-block"><i class="icon-plus"></i> Tarik Saldo</button>
                    </div>
                </div>

                <div class="table-responsive pr-1 pl-1 mb-0">

                <?php

                $this->load->view('components/table', array(
                    "idTable" => "lists-topup-active-table",
                    "isCustomThead" => true,
                    "thead" => '
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Dompet</th>
                        <th scope="col">Bank</th>
                        <th scope="col">Nominal Penarikan</th>
                        <th scope="col">Status</th>
                        <th scope="col">Tgl Buat</th>
                        <th scope="col">Tgl Ubah</th>
                    </tr>
                    ',
                    "tbody" => ''
                )) ?>
                
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="p-1">
                            <div class="btn-group" role="group" aria-label="Pagination Group">
                                <button class="btn btn-outline-secondary" type="button" 
                                id="lists-topup-active-prev"><i class="icon-arrow-left-circle"></i> Sebelumnya</button>
                                <button class="btn btn-outline-secondary" type="button" 
                                id="lists-topup-active-next">Selanjutnya <i class="icon-arrow-right-circle"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tab-pane fade" id="list-topup-history" role="tabpanel" 
            aria-labelledby="list-topup-history-tab">
                <div class="pt-0 pb-0 pl-3 pr-3 mb-3 row">
                    <div class="input-group col-12 col-md-6 p-1">
                        <input type="text" class="form-control p-2 w-100 h-100 contact-search" 
                            placeholder="Cari ..."
                            id="lists-topup-history-search">
                            <div class="input-group-append">
                                <span class="btn btn-outline-primary input-group-text" id="lists-topup-history-button-search"><i class="icon-magnifier"
                                    aria-hidden="true"></i></span>
                            </div>
                    </div>
                    <div class="col-12 col-md-6 p-1">
                        <select class="form-control p-2 w-100 h-100 contact-search"
                            id="lists-topup-history-order">
                            <option value="0">Urutkan berdasarkan</option>
                            <option value="1">Nama (A-Z)</option>
                            <option value="2">Nama (Z-A)</option>
                        </select>
                    </div>
                </div>

                <div class="table-responsive pr-1 pl-1 mb-0">

                <?php

                $this->load->view('components/table', array(
                    "idTable" => "lists-topup-history-table",
                    "isCustomThead" => true,
                    "thead" => '
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Bank</th>
                        <th scope="col">Dompet</th>
                        <th scope="col">Nominal Penarikan</th>
                        <th scope="col">Status</th>
                        <th scope="col">Tgl Buat</th>
                        <th scope="col">Tgl Ubah</th>
                    </tr>
                    ',
                    "tbody" => ''
                )) ?>
                
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="p-1">
                            <div class="btn-group" role="group" aria-label="Pagination Group">
                                <button class="btn btn-outline-secondary" type="button" 
                                id="lists-topup-history-prev"><i class="icon-arrow-left-circle"></i> Sebelumnya</button>
                                <button class="btn btn-outline-secondary" type="button" 
                                id="lists-topup-history-next">Selanjutnya <i class="icon-arrow-right-circle"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<script>

    function randomXToY(minVal,maxVal)
    {
        var randVal = minVal+(Math.random()*(maxVal-minVal));
        return Math.round(randVal);
    }

    var page = 0;
    var paginationClicked = false;
    var paginationDirection = "";
    var dataTagihanTopUp = []
    var selectedTagihanTopUp = null;
    var current_balance = 0;

    function loadSwitch(action) {
        if (action == 0) loadTagihanTopUp()
        else loadRiwayatTopUp()
    }

    (function ($) {
        "use strict";
        $(window).on("load", function () {
            loadTagihanTopUp()
            loadWallet()
            loadBankAccount()

            $("#btn-create-top-up").on("click", function() {
                var balance = $("#create-balance").val();
                balance = Number(balance)
                if (isNaN(balance)) {
                    toastr.error("Harap masukkan nominal yang benar");
                    return 
                } 
                
                if (balance < 10000) {
                    toastr.error("Minimal nominal Rp10.000");
                    return 
                }

                if (balance > current_balance) {
                    toastr.error("Nominal lebih besar dari pada saldo");
                    return 
                }

                $("#btn-create-top-up").hide();
                $("#create-top-up-loader").html(`
                <div class="loader-item"></div>
                `);

                var raw = req.raw({
                    id_u_user_wallet: $("#create-wallet").val(),
                    id_u_user_bank_account: $("#create-account_bank").val(),
                    balance_request: balance,
                    note: "Permintaan penarikan sejumlah" + req.money(balance)
                })

                var formData = new FormData();
                formData.append("raw", raw);

                $.ajax({
                    url: base_url.value + "/dashboard/m/managementWallets/request_withdraw",
                    data: formData,
                    type: "POST",
                    contentType: false,
                    processData: false,
                    success: function(response) {
                        console.log(response)
                        response = req.data(response)

                        if (response.code == 200) {
                            toastr.success("Berhasil melakukan penarikan saldo");

                            setTimeout(() => {
                                location.reload();
                            }, 1000);
                        } else {
                            $("#btn-create-top-up").show();
                            $("#create-top-up-loader").html(``);
                            toastr.error(response.message);
                        }
                    }
                });
            });
        });
    })(jQuery);


    function loadBankAccount() {
        $.ajax({
            url: base_url.value + "/dashboard/m/settings/load_bank_account",
            data: null,
            type: "GET",
            contentType: false,
            processData: false,
            success: function(response) {
                response = req.data(response)
                if (response.code == 200) {
                    var data = response.data;
                    
                    $("#create-account_bank").html(`${data.map(function(item) {
                        return `<option value="${item.id}">(${item.bank_name + " | " + item.account_number})</option>`
                    }).join('')}`);

                    if (data.length == 0 || data == null) {
                        $("#account-bank-new").html(`
                        <button class="btn btn-primary" type="button"
                        onclick="location.assign('${base_url.value + "/dashboard/m/settings/bank"}')">
                            Buat Akun Bank
                        </button>
                        `)
                    } else $("#account-bank-new").html(``)
                }
            }
            // ,
            // error: function(response) {
            //     console.log("ERROR", response);
            //     $("#btn-login").removeAttr("disabled");
            // }
        });
    }

    function loadWallet() {

        $.ajax({
            url: base_url.value + "/dashboard/m/managementWallets/current_balance",
            data: null,
            type: "GET",
            contentType: false,
            processData: false,
            success: function(response) {
                response = req.data(response)
                if (response.code == 200) {
                    var data = response.data;
                    current_balance = Number(data.balance)
                    $("#create-current-balance").text(req.money(Number(data.balance).toString(), "Rp "))
                    $("#create-wallet").html(`
                        <option value="${data.id}">${data.wallet_name}</option>
                    `);
                }
            }
            // ,
            // error: function(response) {
            //     console.log("ERROR", response);
            //     $("#btn-login").removeAttr("disabled");
            // }
        });
    }


    function loadTagihanTopUp() {
        var order = $("#lists-topup-active-order").val();
        var order_direction = "DESC";
        switch (Number(order)) {
            case 1:
                order = "bank_name";
                order_direction = "ASC";
                break;
            case 2:
                order = "bank_name";
                order_direction = "DESC";
                break;
            default:
                order = "id";
                order_direction = "DESC";
                break;
        }
        var raw = req.raw({
            page: page,
            search: $("#lists-topup-active-search").val(),
            order_by: order,
            order_direction: order_direction
        })

        console.log(raw)

        var formData = new FormData()
        formData.append("raw", raw)

        $.ajax({
            url: base_url.value + "/dashboard/m/managementWallets/load_bills_withdraw",
            data: formData,
            type: "POST",
            contentType: false,
            processData: false,
            success: function(response) {
                response = req.data(response)
                if (response.code == 200) {
                    dataTagihanTopUp = response.data;
                    renderTagihanTopUp()
                } else {
                    renderTagihanTopUp()
                }
            }
        });
    }

    function renderTagihanTopUp() {
        var data = dataTagihanTopUp;
        if (data.length == 0) {
            $("#lists-topup-active-table tbody").html(`
                <tr>
                    <td colspan="9">Belum ada data</td>
                </tr>
            `);
        } else {

            if (paginationClicked) {
                paginationClicked = false;
                if (data.length == 0) {
                    if (paginationDirection == "next")
                        page -= 1;
                    else page += 1;
                }
                paginationDirection = "";
                loadTagihanTopUp();
                return
            }

            var index = (page * 10) + 1;

            $("#lists-topup-active-table tbody").html(`${data.map(function(item) {

                var status = item.status;
                if (status == "1") status = "<span class='badge badge-primary'>Active</span>";
                else if (status == "2") status = "<span class='badge badge-warning'>Pending/Review</span>";
                else if (status == "0") status = "<span class='badge badge-danger'>InActive</span>";

                return `
                    <tr>
                        <td>${index++}</td>
                        <td>${item.wallet_name}</td>
                        <td>${item.bank_name}</td>
                        <td>${req.money(Number(item.balance_request).toString(), "Rp ")}</td>
                        <td>${status}</td>
                        <td>${item.created_at}</td>
                        <td>${item.updated_at}</td>
                    </tr>
                `;
            }).join('')}`);
        }
    }

    function detailTagihanTopUp(id) {
        var position = dataTagihanTopUp.findIndex(x => x.id == id)

        console.log(position)
        if (position > -1) {
            selectedTagihanTopUp = dataTagihanTopUp[position]
            
        }
    }

    function detailUploadTagihanTopUp(id) {
        var position = dataTagihanTopUp.findIndex(x => x.id == id)

        console.log(position)
        if (position > -1) {
            selectedTagihanTopUp = dataTagihanTopUp[position]
        }
    }

    function loadRiwayatTopUp() {
        var order = $("#lists-topup-history-order").val();
        var order_direction = "DESC";
        switch (Number(order)) {
            case 1:
                order = "bank_name";
                order_direction = "ASC";
                break;
            case 2:
                order = "bank_name";
                order_direction = "DESC";
                break;
            default:
                order = "id";
                order_direction = "DESC";
                break;
        }
        var raw = req.raw({
            page: page,
            search: $("#lists-topup-history-search").val(),
            order_by: order,
            order_direction: order_direction
        })

        console.log(raw)

        var formData = new FormData()
        formData.append("raw", raw)

        $.ajax({
            url: base_url.value + "/dashboard/m/managementWallets/load_withdraw",
            data: formData,
            type: "POST",
            contentType: false,
            processData: false,
            success: function(response) {
                response = req.data(response)
                if (response.code == 200) {
                    dataTagihanTopUp = response.data;
                    renderRiwayatTopUp(dataTagihanTopUp)
                }
            }
        });
    }

    function renderRiwayatTopUp(data) {
        if (data.length == 0) {
            $("#lists-topup-history-table tbody").html(`
                <tr>
                    <td colspan="9">Belum ada data</td>
                </tr>
            `);
        } else {

            if (paginationClicked) {
                paginationClicked = false;
                if (data.length == 0) {
                    if (paginationDirection == "next")
                        page -= 1;
                    else page += 1;
                }
                paginationDirection = "";
                renderRiwayatTopUp();
                return
            }

            var index = (page * 10) + 1;

            $("#lists-topup-history-table tbody").html(`${data.map(function(item) {

            var status = item.status;
            if (status == "1") status = "<span class='badge badge-primary'>Berhasil</span>";
            else if (status == "2") status = "<span class='badge badge-warning'>Pending/Review</span>";
            else if (status == "0") status = "<span class='badge badge-danger'>Batal</span>";

            return `
                <tr>
                    <td>${index++}</td>
                    <td>${item.bank_name}</td>
                    <td>${item.wallet_name}</td>
                    <td>${req.money(Number(item.balance_request).toString(), "Rp ")}</td>
                    <td>${status}</td>
                    <td>${(item.created_at != null) ? item.created_at : '-'}</td>
                    <td>${(item.updated_at != null) ? item.updated_at : '-'}</td>
                </tr>
            `;
            }).join('')}`);
        }
    }
</script>