<?php
    // Detail Top Up Proof Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-create-top-up",
        "modalTitle" => "Isi Ulang Saldo",
        "modalType" => "modal-md",
        "iconTitle" => "icon-plus",
        "modalActionForm" => "#",
        "modalContentForm" => '
            <div class="form-group mb-3">
                <label for="create-balance">Nominal Isi Ulang</label>
                <input type="number" id="create-balance" class="form-control"
                placeholder="Contoh: 10000"/> 
                <sub class="text-danger">Minimum Nominal Isi Ulang Rp10.000</sub>
            </div>
        ',
        "modalButtonForm" => '<button type="button" class="btn btn-primary" 
        id="create-top-up">Kirim
        <i class="icon-paper-plane"></i>
        </button>
        
        <div id="create-top-up-loader"></div>
        '
    ));
?>

<?php
    // Detail Top Up Proof Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-upload-proof-top-up",
        "modalTitle" => "Unggah Bukti Pembayaran",
        "modalType" => "modal-md",
        "iconTitle" => "icon-picture",
        "modalActionForm" => "#",
        "modalContentForm" => '
            <div class="form-group mb-3">
                <label for="upload-proof">Bukti pembayaran</label>
                <input type="file" id="upload-proof" class="form-control"/> 
            </div>
        ',
        "modalButtonForm" => '<button type="button" class="btn btn-primary" 
        id="upload-proof-top-up">Unggah
        <i class="icon-paper-plane"></i>
        </button>
        
        <div id="upload-proof-top-up-loader"></div>
        '
    ));
?>

<div class="card mb-3">
    <div class="card-header">
        <h4 class="card-title">Isi Ulang Saldo</h4>
    </div>
    <div class="card-body">

        <ul class="nav nav-tabs pl-2 pr-2 mb-3" id="list-topup-tab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="list-topup-active-tab" data-toggle="tab" href="#list-topup-active" 
                onclick="loadSwitch(0)"
                role="tab" aria-controls="list-topup-active" aria-selected="true">Tagihan</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="list-topup-history-tab" data-toggle="tab" 
                onclick="loadSwitch(1)"
                href="#list-topup-history" role="tab" aria-controls="list-topup-history" aria-selected="false">Riwayat Isi Ulang</a>
            </li>
        </ul>

        <div class="tab-content" id="list-topup-tab-content">

            <div class="tab-pane fade show active" id="list-topup-active" role="tabpanel" 
            aria-labelledby="list-topup-active-tab">
                <div class="pt-0 pb-0 pl-3 pr-3 mb-3 row">
                    <div class="input-group col-12 col-md-5 p-1">
                        <input type="text" class="form-control p-2 w-100 h-100 contact-search" 
                            placeholder="Cari ..."
                            id="lists-topup-active-search">
                            <div class="input-group-append">
                                <span class="btn btn-outline-primary input-group-text" id="lists-topup-active-button-search"><i class="icon-magnifier"
                                    aria-hidden="true"></i></span>
                            </div>
                    </div>
                    <div class="col-12 col-md-5 p-1">
                        <select class="form-control p-2 w-100 h-100 contact-search"
                            id="lists-topup-active-order">
                            <option value="0">Urutkan berdasarkan</option>
                            <option value="1">Nama (A-Z)</option>
                            <option value="2">Nama (Z-A)</option>
                        </select>
                    </div>
                    <div class="col-12 col-md-2 pl-1 pr-1 my-2">
                        <button data-toggle="modal" data-target="#modal-create-top-up" 
                        class="btn btn-outline-secondary btn-block"><i class="icon-plus"></i> Isi Ulang</button>
                    </div>
                </div>

                <div class="table-responsive pr-1 pl-1 mb-0">

                <?php

                $this->load->view('components/table', array(
                    "idTable" => "lists-topup-active-table",
                    "isCustomThead" => true,
                    "thead" => '
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Dompet</th>
                        <th scope="col">Saldo</th>
                        <th scope="col">Nominal Tagihan</th>
                        <th scope="col">Tgl Buat</th>
                        <th scope="col">Tgl Ubah</th>
                        <th scope="col">Tautan Pembayaran</th>
                        <th scope="col">Detail Tagihan</th>
                    </tr>
                    ',
                    "tbody" => ''
                )) ?>
                
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="p-1">
                            <div class="btn-group" role="group" aria-label="Pagination Group">
                                <button class="btn btn-outline-secondary" type="button" 
                                id="lists-topup-active-prev"><i class="icon-arrow-left-circle"></i> Sebelumnya</button>
                                <button class="btn btn-outline-secondary" type="button" 
                                id="lists-topup-active-next">Selanjutnya <i class="icon-arrow-right-circle"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tab-pane fade" id="list-topup-history" role="tabpanel" 
            aria-labelledby="list-topup-history-tab">
                <div class="pt-0 pb-0 pl-3 pr-3 mb-3 row">
                    <div class="input-group col-12 col-md-6 p-1">
                        <input type="text" class="form-control p-2 w-100 h-100 contact-search" 
                            placeholder="Cari ..."
                            id="lists-topup-history-search">
                            <div class="input-group-append">
                                <span class="btn btn-outline-primary input-group-text" id="lists-topup-history-button-search"><i class="icon-magnifier"
                                    aria-hidden="true"></i></span>
                            </div>
                    </div>
                    <div class="col-12 col-md-6 p-1">
                        <select class="form-control p-2 w-100 h-100 contact-search"
                            id="lists-topup-history-order">
                            <option value="0">Urutkan berdasarkan</option>
                            <option value="1">Nama (A-Z)</option>
                            <option value="2">Nama (Z-A)</option>
                        </select>
                    </div>
                </div>

                <div class="table-responsive pr-1 pl-1 mb-0">

                <?php

                $this->load->view('components/table', array(
                    "idTable" => "lists-topup-history-table",
                    "isCustomThead" => true,
                    "thead" => '
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Dompet</th>
                        <th scope="col">Saldo</th>
                        <th scope="col">Nominal Tagihan</th>
                        <th scope="col">Status</th>
                        <th scope="col">Tgl Buat</th>
                        <th scope="col">Tgl Ubah</th>
                    </tr>
                    ',
                    "tbody" => ''
                )) ?>
                
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="p-1">
                            <div class="btn-group" role="group" aria-label="Pagination Group">
                                <button class="btn btn-outline-secondary" type="button" 
                                id="lists-topup-history-prev"><i class="icon-arrow-left-circle"></i> Sebelumnya</button>
                                <button class="btn btn-outline-secondary" type="button" 
                                id="lists-topup-history-next">Selanjutnya <i class="icon-arrow-right-circle"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<script>

    function randomXToY(minVal,maxVal)
    {
        var randVal = minVal+(Math.random()*(maxVal-minVal));
        return Math.round(randVal);
    }

    var page = 0;
    var paginationClicked = false;
    var paginationDirection = "";
    var dataTagihanTopUp = [], banks = [];
    var selectedTagihanTopUp = null;

    function loadSwitch(action) {
        if (action == 0) loadTagihanTopUp()
        else loadRiwayatTopUp()
    }

    (function ($) {
        "use strict";
        $(window).on("load", function () {
            loadTagihanTopUp()
            loadBanks()

            $("#upload-proof-top-up").on("click", function() {
                
                if ($("#upload-proof").prop("files").length == 0) {
                    toastr.error("Harap mengunggah bukti pembayaran");
                    return 
                }

                $("#upload-proof-top-up").hide();
                $("#upload-proof-top-up-loader").html(`
                <div class="loader-item"></div>
                `);

                var formData = new FormData();
                formData.append("file-media", $("#upload-proof").prop("files")[0]);

                $.ajax({
                    url: base_url.value + "/dashboard/m/managementWallets/create_medias",
                    data: formData,
                    type: "POST",
                    contentType: false,
                    processData: false,
                    success: function(response) {
                        console.log(response)
                        response = req.data(response)

                        if (response.code == 200) {
                            var data = response.data;
                            uploadTagihanTopUp(data)
                        } else {
                            $("#upload-proof-top-up").show();
                            $("#upload-proof-top-up-loader").html(``);
                            toastr.error(response.message);
                        }
                    }
                });
            });

            $("#create-top-up").on("click", function() {
                var balance = $("#create-balance").val();
                balance = Number(balance)
                if (isNaN(balance)) {
                    toastr.error("Harap masukkan nominal yang benar");
                    return 
                } 
                
                if (balance < 10000) {
                    toastr.error("Minimal nominal Rp10.000");
                    return 
                }

                $("#create-top-up").hide();
                $("#create-top-up-loader").html(`
                <div class="loader-item"></div>
                `);

                var raw = req.raw({
                    balance_request: balance,
                    balance_transfer: (balance + randomXToY(101, 999)),
                    status: 2
                })

                var formData = new FormData();
                formData.append("raw", raw);

                $.ajax({
                    url: base_url.value + "/dashboard/m/managementWallets/request_top_up",
                    data: formData,
                    type: "POST",
                    contentType: false,
                    processData: false,
                    success: function(response) {
                        console.log(response)
                        response = req.data(response)

                        if (response.code == 200) {
                            toastr.success("Berhasil mengajukan isi ulang saldo");
                            sendToMidtrans(response.data.id, response.data.token, response.data.balance_transfer)
                        } else {
                            $("#create-top-up").show();
                            $("#create-top-up-loader").html(``);
                            toastr.error(response.message);
                        }
                    }
                });
            });
        });
    })(jQuery);

    function loadBanks() {
        $.ajax({
            url: base_url.value + "/services/contents/load_banks",
            data: null,
            type: "GET",
            contentType: false,
            processData: false,
            success: function(response) {
                response = req.data(response)
                if (response.code == 200) {
                    var data = response.data
                    banks = data;
                }
            }
        });
    }

    function loadAccountBank(idBank, idTarget) {
        $.ajax({
            url: base_url.value + "/general/publicServices/bank_account/" + idBank,
            data: null,
            type: "GET",
            contentType: false,
            processData: false,
            success: function(response) {
                response = req.data(response)
                if (response.code == 200) {
                    var data = response.data
                    if (data == null) {
                        $(idTarget).html(`
                            -
                        `);
                    }
                    else {
                        $(idTarget).html(`
                            ${data.account_number} - ${data.account_name}
                        `);
                    }
                } else {
                    toastr.error(response.message);
                    $(idTarget).html(`
                        -
                    `);
                }
            }
        });
    }

    function detailTagihanTopUp(tagihan, token) {
        $("#detail-tagihan-top-up").html(`
            <strong>Total yang harus di transfer</strong>
            <h3>
            ${tagihan}
            </h3>
            <br>
            <strong>Status Pembayaran</strong><br>
            <span id="${token}"></span>
            </div>
        `);
        checkStatusPayment(token)
    }

    function loadUtilityBank(idBank, idTarget) {
        $.ajax({
            url: base_url.value + "/general/publicServices/list_utility_bank/" + idBank,
            data: null,
            type: "GET",
            contentType: false,
            processData: false,
            success: function(response) {
                response = req.data(response)
                if (response.code == 200) {
                    var data = response.data
                    if (data.length == 0) $(idTarget).html(`
                        <p>Belum ada step transaksi untuk bank ini</p>
                    `);
                    else $(idTarget).html(`
                        <h5 id="${idBank + "-bank-account"}"></h5>
                        <ul class="list-group">
                        ${data.map(function(item, position) {
                            return `
                            <li class="list-group-item">
                            ${(position + 1)}
                            ${item.step}
                            </li>
                            `;
                        }).join('')}
                        </ul>
                    `);
                } else {
                    toastr.error(response.message);
                    $(idTarget).html(`
                        <p>Belum ada step transaksi untuk bank ini</p>
                    `);
                }
                
                loadAccountBank(idBank, "#" + idBank + "-bank-account")
            }
        });
    }

    function uploadTagihanTopUp(data) {
        var raw = req.raw({
            id: selectedTagihanTopUp.id,
            proof_id: data.id
        })

        var formData = new FormData();
        formData.append("raw", raw);

        $.ajax({
            url: base_url.value + "/dashboard/m/managementWallets/upload_proof",
            data: formData,
            type: "POST",
            contentType: false,
            processData: false,
            success: function(response) {
                console.log(response)
                response = req.data(response)

                if (response.code == 200) {
                    toastr.success("Berhasil mengunggah bukti pembayaran")
                    setTimeout(() => {
                        location.reload();
                    }, 1000);
                } else {
                    toastr.error(response.message);

                    $("#upload-proof-top-up").show();
                    $("#upload-proof-top-up-loader").html(``);
                }
            }
        });
    }

    function loadTagihanTopUp() {
        var order = $("#lists-topup-active-order").val();
        var order_direction = "DESC";
        switch (Number(order)) {
            case 1:
                order = "bank_name";
                order_direction = "ASC";
                break;
            case 2:
                order = "bank_name";
                order_direction = "DESC";
                break;
            default:
                order = "id";
                order_direction = "DESC";
                break;
        }
        var raw = req.raw({
            page: page,
            search: $("#lists-topup-active-search").val(),
            order_by: order,
            order_direction: order_direction
        })

        console.log(raw)

        var formData = new FormData()
        formData.append("raw", raw)

        $.ajax({
            url: base_url.value + "/dashboard/m/managementWallets/load_bills_top_up",
            data: formData,
            type: "POST",
            contentType: false,
            processData: false,
            success: function(response) {
                response = req.data(response)
                if (response.code == 200) {
                    dataTagihanTopUp = response.data;
                    renderTagihanTopUp()
                } else {
                    renderTagihanTopUp()
                }
            }
        });
    }

    function renderTagihanTopUp() {
        var data = dataTagihanTopUp;
        if (data.length == 0) {
            $("#lists-topup-active-table tbody").html(`
                <tr>
                    <td colspan="9">Belum ada data</td>
                </tr>
            `);
        } else {

            if (paginationClicked) {
                paginationClicked = false;
                if (data.length == 0) {
                    if (paginationDirection == "next")
                        page -= 1;
                    else page += 1;
                }
                paginationDirection = "";
                loadTagihanTopUp();
                return
            }

            var index = (page * 10) + 1;

            $("#lists-topup-active-table tbody").html(`${data.map(function(item) {
                return `
                    <tr>
                        <td class="align-middle">${index++}</td>
                        <td class="align-middle">${item.wallet_name}</td>
                        <td class="align-middle">${req.money(Number(item.balance_request).toString(), "Rp ")}</td>
                        <td class="align-middle">${req.money(Number(item.balance_transfer).toString(), "Rp ")}</td>
                        <td class="align-middle">${item.created_at}</td>
                        <td class="align-middle">${item.updated_at}</td>
                        <td class="align-middle">
                            <a href="${redirect_url.value}${item.token_payment}" target="_blank" class="btn btn-block btn-success p-2 text-white">Bayar Tagihan</a>
                        </td>
                        <td class="align-middle">
                            <span class="badge badge-primary p-2" style="cursor: pointer; display: block"
                            data-toggle="modal" data-target="#modal-detail-top-up" 
                            onclick="detailTagihanTopUp('${req.money(Number(item.balance_transfer).toString(), "Rp ")}', '${item.token}')"><i class="icon-tag"></i> Tagihan</span>
                        </td>
                    </tr>
                `;
            }).join('')}`);
        }
    }

    function detailUploadTagihanTopUp(id) {
        var position = dataTagihanTopUp.findIndex(x => x.id == id)

        console.log(position)
        if (position > -1) {
            selectedTagihanTopUp = dataTagihanTopUp[position]
        }
    }

    function loadRiwayatTopUp() {
        var order = $("#lists-topup-history-order").val();
        var order_direction = "DESC";
        switch (Number(order)) {
            case 1:
                order = "bank_name";
                order_direction = "ASC";
                break;
            case 2:
                order = "bank_name";
                order_direction = "DESC";
                break;
            default:
                order = "id";
                order_direction = "DESC";
                break;
        }
        var raw = req.raw({
            page: page,
            search: $("#lists-topup-history-search").val(),
            order_by: order,
            order_direction: order_direction
        })

        console.log(raw)

        var formData = new FormData()
        formData.append("raw", raw)

        $.ajax({
            url: base_url.value + "/dashboard/m/managementWallets/load_top_up",
            data: formData,
            type: "POST",
            contentType: false,
            processData: false,
            success: function(response) {
                response = req.data(response)
                if (response.code == 200) {
                    dataTagihanTopUp = response.data;
                    renderRiwayatTopUp(response.data)
                } else {
                    renderTagihanTopUp([])
                }
            }
        });
    }

    function renderRiwayatTopUp(data) {
        if (data.length == 0) {
            $("#lists-topup-active-table tbody").html(`
                <tr>
                    <td colspan="9">Belum ada data</td>
                </tr>
            `);
        } else {

            if (paginationClicked) {
                paginationClicked = false;
                if (data.length == 0) {
                    if (paginationDirection == "next")
                        page -= 1;
                    else page += 1;
                }
                paginationDirection = "";
                loadTagihanTopUp();
                return
            }

            var index = (page * 10) + 1;

            $("#lists-topup-history-table tbody").html(`${data.map(function(item) {

            var status = item.status;
            if (status == "1") status = "<span class='badge badge-primary'>Berhasil</span>";
            else if (status == "2") status = "<span class='badge badge-warning'>Pending/Review</span>";
            else if (status == "0") status = "<span class='badge badge-danger'>Batal</span>";

            console.log(item.balance_transfer)

            return `
                <tr>
                    <td>${index++}</td>
                    <td>${item.wallet_name}</td>
                    <td>${req.money(Number(item.balance_request).toString(), "Rp ")}</td>
                    <td>${req.money(Number(item.balance_transfer).toString(), "Rp ")}</td>
                    <td>${status}</td>
                    <td>${item.created_at}</td>
                    <td>${item.updated_at}</td>
                </tr>
            `;
            }).join('')}`);
        }
    }

    function sendToMidtrans(id, token_payment, balance_transfer){
        var raw = req.raw({
            transaction_details: {
                order_id: token_payment,
                gross_amount: balance_transfer
            },
            credit_card: {
                secure: true
            }
        })

        var formData = new FormData()
        formData.append("raw", raw)

        $.ajax({
            url: base_url.value + "/anonymous/cart/create_payment_link",
            data: formData,
            type: "POST",
            contentType: false,
            processData: false,
            success: function(response) {
                response = req.data(response)
                updateTokenTransaction(id, response.token)
            }
        });
    }

    function updateTokenTransaction(id, token) {
        var raw = req.raw({
            token_payment: token
        })

        var formData = new FormData()
        formData.append("raw", raw);

        $.ajax({
            url: base_url.value + "/dashboard/m/managementwallets/update_payment_token/" + id,
            data: formData,
            type: "POST",
            contentType: false,
            processData: false,
            success: function(response) {
                setTimeout(() => {
                    location.reload();
                }, 1000);
            }
        });
    }

    function checkStatusPayment(token){
        $(`#${token}`).html("Sedang memuat...")
        $.ajax({
            url: base_url.value + "/dashboard/m/managementwallets/check_status_payment/" + token,
            data: null,
            type: "GET",
            contentType: false,
            processData: false,
            success: function(response) {
                response = req.data(response)
                if (response.status_code == 200){
                    if(response.transaction_status == "settlement"){
                        $(`#${token}`).html(`<span class="badge outline-badge-warning">Menunggu Persetujuan Admin</span>`)
                    }
                }else {
                    $(`#${token}`).html(`<span class="badge outline-badge-secondary">Belum Melakukan Pembayaran</span>`)
                }
            }
        });
    }
</script>