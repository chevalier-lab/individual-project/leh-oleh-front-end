<div class="row">
    <div class="col-12 col-sm-6 col-xl-4 mt-3">
        <div class="card">
            <div class="card-body">
                <div class='d-flex px-0 px-lg-2 py-2 align-self-center'>
                    <span class="card-liner-icon mt-1">Rp</span>
                    <div class='card-liner-content'>
                        <h2 class="card-liner-title" id="income-today"></h2>
                        <h6 class="card-liner-subtitle">Penghasilan Hari Ini</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 col-sm-6 col-xl-4 mt-3">
        <div class="card">
            <div class="card-body">
                <div class='d-flex px-0 px-lg-2 py-2 align-self-center'>
                    <span class="card-liner-icon mt-1">Rp</span>
                    <div class='card-liner-content'>
                        <h2 class="card-liner-title" id="income-month"></h2>
                        <h6 class="card-liner-subtitle">Penghasilan Bulan Ini</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 col-sm-6 col-xl-4 mt-3">
        <div class="card">
            <div class="card-body">
                <div class='d-flex px-0 px-lg-2 py-2 align-self-center'>
                    <span class="card-liner-icon mt-1">Rp</span>
                    <div class='card-liner-content'>
                        <h2 class="card-liner-title" id="income-year"></h2>
                        <h6 class="card-liner-subtitle">Penghasilan Tahun Ini</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<h4 class="mt-4">Informasi Toko</h4>
<hr>
<div class="container-fluid" id="info-merchant"></div>

<script>
    $.ajax({
        url: base_url.value + "/dashboard/m/dashboard/income",
        data: null,
        type: "GET",
        contentType: false,
        processData: false,
        success: function(response) {
            response = req.data(response)
            if (response.code == 200) {
                var data = response.data
                $("#income-today").text(req.money(data.income_today != null ? Number(data.income_today).toString() : "0", "Rp "))
                $("#income-month").text(req.money(data.income_this_month != null ? Number(data.income_this_month).toString() : "0", "Rp "))
                $("#income-year").text(req.money(data.income_this_year != null ? Number(data.income_this_year).toString() : "0", "Rp "))
            }
        }
    });

    $.ajax({
        url: base_url.value + "/dashboard/m/dashboard/banner",
        data: null,
        type: "GET",
        contentType: false,
        processData: false,
        success: function(response) {
            response = req.data(response)
            if (response.code == 200) {
                var data = response.data
                $("#info-merchant").html(`
                    <h5>Produk</h5>
                    <div class="row">

                        <div class="col-12 col-sm-12 col-md-6 mt-3">
                            <div class="card">
                                <div class="card-body">
                                    <div class='d-flex px-0 px-lg-2 py-2 align-self-center'>
                                        <span class="card-liner-icon mt-1">${data.product.total_active}</span>
                                        <div class='card-liner-content'>
                                            <h2 class="card-liner-title">Produk</h2>
                                            <h6 class="card-liner-subtitle">Total aktif</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-sm-12 col-md-6 mt-3">
                            <div class="card">
                                <div class="card-body">
                                    <div class='d-flex px-0 px-lg-2 py-2 align-self-center'>
                                        <span class="card-liner-icon mt-1">${data.product.total_pending}</span>
                                        <div class='card-liner-content'>
                                            <h2 class="card-liner-title">Produk</h2>
                                            <h6 class="card-liner-subtitle">Total tertunda</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <h5 class="mt-4">Transaksi</h5>
                    <div class="row">

                        <div class="col-12 col-sm-12 col-md-3 mt-3">
                            <div class="card">
                                <div class="card-body">
                                    <div class='d-flex px-0 px-lg-2 py-2 align-self-center'>
                                        <span class="card-liner-icon mt-1">${data.transaction.total_payment_verified}</span>
                                        <div class='card-liner-content'>
                                            <h2 class="card-liner-title">Transaksi</h2>
                                            <h6 class="card-liner-subtitle">Total pembayaran terverifikasi</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-sm-12 col-md-3 mt-3">
                            <div class="card">
                                <div class="card-body">
                                    <div class='d-flex px-0 px-lg-2 py-2 align-self-center'>
                                        <span class="card-liner-icon mt-1">${data.transaction.total_process}</span>
                                        <div class='card-liner-content'>
                                            <h2 class="card-liner-title">Transaksi</h2>
                                            <h6 class="card-liner-subtitle">Total dalam proses</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-sm-12 col-md-3 mt-3">
                            <div class="card">
                                <div class="card-body">
                                    <div class='d-flex px-0 px-lg-2 py-2 align-self-center'>
                                        <span class="card-liner-icon mt-1">${data.transaction.total_finish}</span>
                                        <div class='card-liner-content'>
                                            <h2 class="card-liner-title">Transaksi</h2>
                                            <h6 class="card-liner-subtitle">Total selesai</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-sm-12 col-md-3 mt-3">
                            <div class="card">
                                <div class="card-body">
                                    <div class='d-flex px-0 px-lg-2 py-2 align-self-center'>
                                        <span class="card-liner-icon mt-1">${data.transaction.total_refund}</span>
                                        <div class='card-liner-content'>
                                            <h2 class="card-liner-title">Transaksi</h2>
                                            <h6 class="card-liner-subtitle">Total pengembalian dana</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <h5 class="mt-4">Komplain</h5>
                    <div class="row mb-4">

                        <div class="col-12 col-sm-12 col-md-3 mt-3">
                            <div class="card">
                                <div class="card-body">
                                    <div class='d-flex px-0 px-lg-2 py-2 align-self-center'>
                                        <span class="card-liner-icon mt-1">${data.complain.total_waiting}</span>
                                        <div class='card-liner-content'>
                                            <h2 class="card-liner-title">Komplain</h2>
                                            <h6 class="card-liner-subtitle">Total menunggu</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-sm-12 col-md-3 mt-3">
                            <div class="card">
                                <div class="card-body">
                                    <div class='d-flex px-0 px-lg-2 py-2 align-self-center'>
                                        <span class="card-liner-icon mt-1">${data.complain.total_verify}</span>
                                        <div class='card-liner-content'>
                                            <h2 class="card-liner-title">Komplain</h2>
                                            <h6 class="card-liner-subtitle">Total terverifikasi</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-sm-12 col-md-3 mt-3">
                            <div class="card">
                                <div class="card-body">
                                    <div class='d-flex px-0 px-lg-2 py-2 align-self-center'>
                                        <span class="card-liner-icon mt-1">${data.complain.total_process}</span>
                                        <div class='card-liner-content'>
                                            <h2 class="card-liner-title">Komplain</h2>
                                            <h6 class="card-liner-subtitle">Total diproses</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-sm-12 col-md-3 mt-3">
                            <div class="card">
                                <div class="card-body">
                                    <div class='d-flex px-0 px-lg-2 py-2 align-self-center'>
                                        <span class="card-liner-icon mt-1">${data.complain.total_finish}</span>
                                        <div class='card-liner-content'>
                                            <h2 class="card-liner-title">Komplain</h2>
                                            <h6 class="card-liner-subtitle">Total selesai</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-sm-12 col-md-3 mt-3">
                            <div class="card">
                                <div class="card-body">
                                    <div class='d-flex px-0 px-lg-2 py-2 align-self-center'>
                                        <span class="card-liner-icon mt-1">${data.complain.total_reject}</span>
                                        <div class='card-liner-content'>
                                            <h2 class="card-liner-title">Komplain</h2>
                                            <h6 class="card-liner-subtitle">Total dibatalkan</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                `);
            }
        }
    });
</script>