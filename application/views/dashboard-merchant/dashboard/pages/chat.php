<div class="card">
    <div class="card-header">
        <h4 class="card-title">Daftar Obrolan</h4>
    </div>
    <div class="card-body">
        <div class="table-responsive pr-1 pl-1 mb-0">
            <?php
            $this->load->view('components/table', array(
                "idTable" => "master-chats",
                "isCustomThead" => true,
                "thead" => '
                <tr>
                    <th scope="col" rowspan="2">#</th>
                    <th scope="col" rowspan="2">Nama</th>
                    <th scope="col" rowspan="2">Dibuat tanggal</th>
                    <th scope="col" rowspan="2">Perubahan terakhir</th>
                </tr>
                <tr>
                    <th>Lihat</th>
                </tr>
                ',
                "tbody" => ""
            )) ?>
        </div>
    </div>
</div>