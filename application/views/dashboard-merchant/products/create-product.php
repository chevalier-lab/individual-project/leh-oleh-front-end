<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('components/head'); ?>

    <!-- START: Template CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.theme.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/simple-line-icons/css/simple-line-icons.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/flags-icon/css/flag-icon.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/select2/css/select2.min.css') ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/select2/css/select2-bootstrap.min.css') ?>" />
    <!-- END Template CSS-->

    <!-- START: Page CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/social-button/bootstrap-social.css'); ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/sweetalert/sweetalert.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/toastr/toastr.min.css'); ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/fancybox/jquery.fancybox.min.css'); ?>" />
    <!-- END: Page CSS-->

    <!-- START: Custom CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/css/main.css'); ?>">
    <style>
        /* Chrome, Safari, Edge, Opera */
        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }

        /* Firefox */
        input[type=number] {
            -moz-appearance: textfield;
        }

        .cover-img {
            width: 100%;
            height: 200px;
            object-fit: cover;
        }
    </style>
    <!-- END: Custom CSS-->
</head>

<body id="main-container" class="default">

    <input type="hidden" name="base_url" id="base_url" value="<?= base_url("index.php"); ?>">
    <input type="hidden" name="api_url" id="api_url" value="<?= API_URI; ?>">

    <!-- START: Pre Loader-->
    <div class="se-pre-con">
        <div class="loader"></div>
    </div>
    <!-- END: Pre Loader-->

    <!-- START: Template JS-->
    <script src="<?= base_url('assets/dist/vendors/jquery/jquery-3.3.1.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/moment/moment.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/slimscroll/jquery.slimscroll.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/toastr/toastr.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/sweetalert/sweetalert.min.js');?>"></script>
    <!-- END: Template JS-->

    <!-- START: Page Vendor JS-->
    <script src="<?= base_url('assets/dist/js/auth/request.js'); ?>"></script>
    <script>
        var base_url = document.getElementById("base_url");
        var api_url = document.getElementById("api_url");

        checkIsAlreadySetupLocation();
    </script>
    <!-- END: Page Vendor JS-->

    <!-- START: Main Content-->
    <?php

    // Load Navigation Bar
    $this->load->view('components/menus/navbar-merchant');

    // Load Sidebar
    $this->load->view('components/menus/sidebar');

    // Load Page
    $this->load->view('dashboard/template', array(
        'page' => "Buat Produk",
        'pageMap' => array(
            array(
                "label" => "Produk",
                "is_current" => false
            ),
            array(
                "label" => "Buat Produk",
                "is_current" => true
            ),
        ),
        'pageURI' => "dashboard-merchant/products/pages/create-product",
    ));
    // Load Product View

    ?>
    <!-- END: Content-->

    <!-- START: Page Script JS-->
    <?php
    if (isset($error)) {
        $this->load->view("components/error-modal", array(
            "errorModalTitle" => $error["title"],
            "errorModalContent" => $error["content"],
            "errorModalDetail" => $error["details"]
        ));
    }

    ?>
    <!-- END: Page Script JS-->

    <!-- START: APP JS-->
    <script src="<?= base_url('assets/dist/js/app.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/js/home.script.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/select2/js/select2.full.min.js') ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/fancybox/jquery.fancybox.min.js') ?>"></script>

    <script>
        // Select2
        $(".select2").select2({
            theme: 'bootstrap4',
            width: 'style',
        });

        var selectedCategory = []
        var listSelectedCategory = []

        var selectedLocation = []
        var listSelectedLocation = []
        var listProvince = []

        var selectedPhoto = []

        // Initial Pagination
        var pageCategory = 0;
        var paginationClickedCategory = false;
        var paginationDirectionCategory = "";
        var pageLocation = 0;
        var paginationClickedLocation = false;
        var paginationDirectionLocation = "";

        (function($) {
            "use strict";
            $(window).on("load", function() {
                // Load Categories First Time
                loadCategories()
                loadLocations()

                // Not Selected Photo
                $('#grid').html('<h6 class="def col-12 text-center p-5">Belum ada foto produk, klik tombol dibawah untuk menambahkan</h6>')

                // Handle Search Categories
                $("#categories-search").on("keydown", function(event) {
                    if (event.keyCode == 32 || event.which == 32) loadCategories();
                    else if ($("#categories-search").val() == "") loadCategories();
                });

                $("#categories-button-search").on("click", function(event) {
                    loadCategories();
                });

                // Handle Search Locations
                $("#locations-search").on("keyup", function(event) {
                    loadLocations();
                });

                $("#locations-button-search").on("click", function(event) {
                    loadLocations();
                });

                // Handle Pagination Categories
                $("#list-category-prev").on("click", function() {
                    if (pageCategory > 0)
                        pageCategory -= 1;
                    paginationClickedCategory = true;
                    paginationDirectionCategory = "prev";
                    loadCategories();
                });

                $("#list-category-next").on("click", function() {
                    pageCategory += 1;
                    paginationClickedCategory = true;
                    paginationDirectionCategory = "next";
                    loadCategories();
                });

                // Handle Pagination Locations
                $("#list-location-prev").on("click", function() {
                    if (pageLocation > 0)
                        pageLocation -= 1;
                    paginationClickedLocation = true;
                    paginationDirectionLocation = "prev";
                    loadLocations();
                });

                $("#list-location-next").on("click", function() {
                    pageLocation += 1;
                    paginationClickedLocation = true;
                    paginationDirectionLocation = "next";
                    loadLocations();
                });

                // Create Cover Preview
                $("#create-cover-product").on("change", function(event) {
                    previewCover(event, "create-cover-preview")
                });

                // Handle Add Remove Photo
                $("#add-photo").on("change", function(event) {
                    var length = $("#add-photo").prop("files").length
                    $('h6').remove('.def')
                    for (var i = 0; i < length; i++) {
                        selectedPhoto.push($("#add-photo").prop("files")[i])
                        var nameFile = $("#add-photo").prop("files")[i].name
                        var elementPhoto = `
                            <div class="item col-12 col-md-6 col-lg-4 mb-4 cation-hover text-center Photography">
                                <div class="modImage">
                                    <img src="${window.URL.createObjectURL(this.files[i])}" alt="" class="portfolioImage img-fluid cover-img">
                                    <div class="d-flex">
                                        <a data-fancybox-group="gallery" href="${window.URL.createObjectURL(this.files[i])}" class="fancybox btn rounded-0 btn-dark w-50">View Large</a>
                                        <a href="javascript:void(0)" class="btn btn-danger rounded-0 w-50" onclick="deletePhotoSelected('${nameFile}',this.parentElement)">Delete</a>
                                    </div>
                                </div>
                            </div>
                            `
                        $('#grid').append(elementPhoto)
                        $('.fancybox').fancybox()
                    }
                });

                // Add Product
                $("#create-product").on("click", async function() {
                    if (selectedCategory.length == 0) {
                        toastr.error("Anda belum memilih kategori")
                    } else if (selectedLocation.length == 0) {
                        toastr.error("Anda belum memilih lokasi")
                    } else {
                        var categories = []
                        selectedCategory.forEach(function(item) {
                            categories.push(item.id)
                        });

                        var name = $("#name").val();
                        var jumlah = $('#jumlah').val();
                        var diskon = $('#diskon').val();
                        var harga_jual = $('#harga-jual').val();
                        var harga_beli = $('#harga-beli').val();
                        var description = $('#deskripsi').val();

                        if (name == "") {
                            toastr.error("Harap mengisi nama produk");
                            return
                        } else if (jumlah == "" || jumlah < 1) {
                            toastr.error("Harap mengisi jumlah produk");
                            return
                        } else if (diskon == "" || diskon < 0) {
                            diskon = 0
                        } else if (harga_jual == "" || harga_jual < 1) {
                            toastr.error("Harap mengisi harga jual");
                            return
                        } else if (harga_beli == "" || harga_beli < 1) {
                            toastr.error("Harap mengisi harga beli");
                            return
                        } else if (description == "") {
                            toastr.error("Harap mengisi deskripsi");
                            return
                        } else if ($("#create-cover-product").prop("files").length == 0) {
                            toastr.error("Harap memilih sampul produk");
                            return
                        } else if ($("#length").val() == "" || Number($("#length").val()) < 1) {
                            toastr.error("Harap mengisi panjang produk");
                            return
                        }
                        else if ($("#width").val() == "" || Number($("#width").val()) < 1) {
                            toastr.error("Harap mengisi lebar produk");
                            return
                        }
                        else if ($("#height").val() == "" || Number($("#height").val()) < 1) {
                            toastr.error("Harap mengisi tinggi produk");
                            return
                        }
                        else if ($("#weight").val() == "" || Number($("#weight").val()) < 1) {
                            toastr.error("Harap mengisi berat produk");
                            return
                        }

                        $("#create-product").hide();
                        $("#create-product-loader").html(`
                        <div class="loader-item"></div>
                        `);

                        var formData = new FormData()
                        formData.append("name", name)
                        formData.append("slug", name.toLowerCase().replace(" ", "-"))
                        formData.append("jumlah", jumlah)
                        formData.append("diskon", diskon)
                        formData.append("harga_jual", harga_jual)
                        formData.append("harga_beli", harga_beli)
                        formData.append("description", description)
                        formData.append("categories", JSON.stringify(categories))
                        formData.append("cover", $("#create-cover-product").prop("files")[0])

                        // START SYNCHRONOUS
                        let res = await post(base_url.value + "/dashboard/m/managementProducts/create_product", formData)
                        let response = await res.json()
                        var id_u_product = response.data
                        if (response.code == 200){
                            toastr.success("Berhasil membuat informasi produk")

                            // Add Product Dimension Info
                            var raw = req.raw({
                                id_u_product: id_u_product,
                                length: Number($("#length").val()),
                                width: Number($("#width").val()),
                                height: Number($("#height").val()),
                                weight: Number($("#weight").val()) / 1000
                            })

                            var formData = new FormData()
                            formData.append("raw", raw)

                            let res = await post(base_url.value + "/dashboard/m/managementProducts/create_product_dimension/", formData)
                            let response = await res.json()
                            if (response.code == 200)
                                toastr.success("Berhasil membuat dimensi produk")
                            else
                                toastr.error("Gagal membuat dimensi produk")

                            // Add Photo If Exist
                            if (selectedPhoto.length > 0) {
                                toastr.warning("Persiapan menambahkan " + selectedPhoto.length + " foto produk")
                                for (let i = 0; i < selectedPhoto.length; i++) {
                                    var photoData = new FormData()
                                    photoData.append("photo", selectedPhoto[i])
                                    let res = await post(base_url.value + "/dashboard/m/managementProducts/create_photo_products/" + id_u_product, photoData)
                                    let response = await res.json()
                                    if (response.code == 200)
                                        toastr.success("Berhasil menambahkan foto " + (i + 1))
                                    else
                                        toastr.error("Gagal menambahkan foto " + (i + 1))
                                }
                            }

                            // Add Location If Exist
                            if (selectedLocation.length > 0) {
                                var locations = []
                                selectedLocation.forEach(function(item) {
                                    locations.push(item.id)
                                });

                                toastr.warning("Persiapan menambahkan " + selectedLocation.length + " lokasi")
                                for (let i = 0; i < selectedLocation.length; i++) {
                                    var raw = req.raw({
                                        id_u_product: id_u_product,
                                        id_m_locations: locations[i],
                                    })
                                    var formData = new FormData()
                                    formData.append("raw", raw)
                                    let res = await post(base_url.value + "/dashboard/m/managementProducts/create_product_locations", formData)
                                    let response = await res.json()
                                    if (response.code == 200)
                                        toastr.success("Berhasil menambahkan " + (i + 1) + " lokasi")
                                    else
                                        toastr.error("Gagal menambahkan " + (i + 1) + " lokasi")
                                }
                            }

                            toastr.info("Berhasil menambahkan produk")
                            setTimeout(() => {
                                location.assign(base_url.value + "/dashboard/m/managementProducts")
                            }, 1000);
                        } else {
                            $("#create-product").show();
                            $("#create-product-loader").html(``);
                            toastr.error("Gagal membuat produk");
                        }
                        // END SYNCHRONOUS

                        // // Add product basic information
                        // $.ajax({
                        //     url: base_url.value + "/dashboard/m/managementProducts/create_product",
                        //     data: formData,
                        //     type: "POST",
                        //     contentType: false,
                        //     processData: false,
                        //     success: async function(response) {
                        //         $("#create-product").removeAttr("disabled");
                        //         response = req.data(response)
                        //         if (response.code == 200) {

                        //             var id_u_product = response.data

                        //             // Add Product Dimension Info
                        //             var raw = req.raw({
                        //                 id_u_product: id_u_product,
                        //                 length: Number($("#length").val()),
                        //                 width: Number($("#width").val()),
                        //                 height: Number($("#height").val()),
                        //                 weight: Number($("#weight").val()) / 1000
                        //             })

                        //             var formData = new FormData()
                        //             formData.append("raw", raw)
                        //             addProductDimension(formData)

                        //             // Add photo if exist
                        //             if (selectedPhoto.length > 0) {
                        //                 for (var i = 0; i < selectedPhoto.length; i++) {
                        //                     var photoData = new FormData()
                        //                     photoData.append("photo", selectedPhoto[i])
                        //                     addPhotoAsync(photoData, (i + 1), id_u_product)
                        //                 }
                        //             }

                        //             // Add product locations if exist
                        //             if (selectedLocation.length > 0) {
                        //                 var locations = []
                        //                 selectedLocation.forEach(function(item) {
                        //                     locations.push(item.id)
                        //                 });

                        //                 for (var i = 0; i < locations.length; i++) {
                        //                     var raw = req.raw({
                        //                         id_u_product: id_u_product,
                        //                         id_m_locations: locations[i],
                        //                     })

                        //                     var locationData = new FormData()
                        //                     locationData.append("raw", raw)

                        //                     addLocationAsync(locationData, (i + 1))
                        //                 }
                        //             }

                        //             toastr.info("Berhasil menambahkan produk")
                        //             setTimeout(() => {
                        //                 location.assign(base_url.value + "/dashboard/m/managementProducts")
                        //             }, 1000);
                        //         } else {
                        //             $("#create-product").show();
                        //             $("#create-product-loader").html(``);
                        //             toastr.error(response.message);
                        //         }
                        //     }
                        // });
                    }
                })
            });
        })(jQuery);

        // Upload products photo
        async function addPhotoAsync(photoData, index, id_u_product) {
            await $.ajax({
                url: base_url.value + "/dashboard/m/managementProducts/create_photo_products/" + id_u_product,
                data: photoData,
                type: "POST",
                contentType: false,
                processData: false,
                // success: function(response) {
                //     response = req.data(response)
                //     if (response.code == 200) {
                //         toastr.success("Success add photo " + index);
                //     } else {
                //         toastr.error("Failed add photo " + index);
                //     }
                // }
            });
        }

        // Upload products location
        async function addLocationAsync(locationData, index) {
            await $.ajax({
                url: base_url.value + "/dashboard/m/managementProducts/create_product_locations",
                data: locationData,
                type: "POST",
                contentType: false,
                processData: false,
                // success: function(response) {
                //     response = req.data(response)
                //     if (response.code == 200) {
                //         toastr.success("Success add location " + index);
                //     } else {
                //         toastr.error("Failed add location " + index);
                //     }
                // }
            });
        }

        // Handle Cover Start
        function previewCover(event, idElement) {
            var reader = new FileReader();
            reader.onload = function() {
                var output = document.getElementById(idElement);
                output.src = reader.result;
            }
            reader.readAsDataURL(event.target.files[0]);
            $(`#${idElement}`).addClass("mb-3")
        }

        // Remove Photo from array & element
        function deletePhotoSelected(name, element) {
            element.parentElement.parentElement.remove()
            for (var i = 0; i < selectedPhoto.length; i++) {
                if (selectedPhoto[i].name == name) {
                    selectedPhoto.splice(i, 1)
                }
            }
            if (selectedPhoto.length == 0) {
                $('#grid').html('<h6 class="def col-12 text-center p-5">Belum ada foto produk, klik tombol dibawah untuk menambahkan</h6>')
            }
        }

        // Function Categories
        function loadCategories() {
            renderToWaitingCategories()
            var raw = req.raw({
                page: pageCategory,
                search: $("#categories-search").val(),
                order_by: "id",
                order_direction: "DESC"
            })

            var formData = new FormData()
            formData.append("raw", raw)

            $.ajax({
                url: base_url.value + "/dashboard/m/managementProducts/load_categories",
                data: formData,
                type: "POST",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.code == 200) {
                        renderToTableCategories(response.data)
                    } else {
                        renderToTableCategories([])
                    }
                }
            });
        }

        function renderToWaitingCategories() {
            $("#list-categories tbody").html(`
                <tr>
                    <td class="align-middle" colspan="5">Sedang memuat data</td>
                </tr>
            `);
        }

        function renderToTableCategories(data = []) {
            if (paginationClickedCategory) {
                paginationClickedCategory = false;
                if (data.length == 0) {
                    if (paginationDirectionCategory == "next")
                        pageCategory -= 1;
                    else pageCategory += 1;
                }
                paginationDirectionCategory = "";
                loadCategories();
                return
            }

            var index = (pageCategory * 10) + 1;

            if (data.length == 0) {
                $("#list-categories tbody").html(`
                    <tr>
                        <td class="align-middle" colspan="4">Belum ada data kategori</td>
                    </tr>
                `);
                return;
            }

            $("#list-categories tbody").html(`${data.map(function(item) {
                if (item.is_visible == "1") {
                    return `
                    <tr>
                        <td class="align-middle">${index++}</td>
                        <td class="align-middle">${item.category}</td>
                        <td class="align-middle">${item.description}</td>
                        <td class="align-middle h4"><i class="icon ${item.icon}"></td>
                        <td class="align-middle">
                            <h4>
                                <a href="javascript:void(0)" class="text-success"><i class="icon-plus" onclick="selectIDCategory(${item.id}, '${item.category}')"></i></a>
                            </h4>
                        </td>
                    </tr>
                `;
                }
            }).join('')}`);
        }

        function selectIDCategory(idCategory, itemCategory) {
            var index = selectedCategory.map(function(x) {
                return x.id;
            }).indexOf(idCategory);
            if (index == -1) {
                selectedCategory.push({
                    id: idCategory,
                    name: itemCategory
                });
            } else {
                toastr.error("Kategori sudah dipilih")
            }
            renderSelectedCategory();
        }

        function renderSelectedCategory() {
            if (selectedCategory.length == 0) {
                $("#selected-categories").html('<p>Belum ada kategori yang dipilih</p>')
            } else {
                $("#selected-categories").html(`${selectedCategory.map(function(item, index) {
                return `<a href="javascript:void(0)" 
                onclick="removeIDCategory(${index})"
                class="badge badge-primary p-2 m-2">${item.name} <i class="icon-close"></i></a>`;
            }).join('')}`)
            }
        }

        function removeIDCategory(position) {
            if (position > -1) {
                selectedCategory.splice(position, 1);
                renderSelectedCategory();
            }
        }

        // Function Locations
        function loadLocations() {
            renderToWaitingLocations()
            if (listProvince.length == 0) {
                $.ajax({
                    url: base_url.value + "/dashboard/m/managementProducts/load_locations",
                    data: null,
                    type: "GET",
                    contentType: false,
                    processData: false,
                    success: function(response) {
                        response = req.data(response)
                        if (response.code == 200) {
                            listProvince = response.data
                            paginationStatic()
                        } else {
                            renderToTableLocations([])
                        }
                    }
                });
            } else {
                paginationStatic()
            }
        }

        function paginationStatic() {
            var sliceProvince = []
            if ($('#locations-search').val() != "") {
                sliceProvince = listProvince.filter(function(item) {
                    return item.label.toUpperCase().indexOf($('#locations-search').val().toUpperCase()) > -1;
                })
            } else {
                switch (pageLocation) {
                    case 0:
                        sliceProvince = listProvince.slice(0, 10)
                        break
                    case 1:
                        sliceProvince = listProvince.slice(10, 20)
                        break
                    case 2:
                        sliceProvince = listProvince.slice(20, 30)
                        break
                    case 3:
                        sliceProvince = listProvince.slice(30, 34)
                        break
                    default:
                        sliceProvince = []
                }
            }
            renderToTableLocations(sliceProvince)
        }

        function renderToWaitingLocations() {
            $("#list-locations tbody").html(`
                <tr>
                    <td class="align-middle" colspan="3">Sedang memuat data lokasi</td>
                </tr>
            `);
        }

        function renderToTableLocations(data = []) {
            if (paginationClickedLocation) {
                paginationClickedLocation = false;
                if (data.length == 0) {
                    if (paginationDirectionLocation == "next")
                        pageLocation -= 1;
                    else pageLocation += 1;
                }
                paginationDirectionLocation = "";
                loadLocations();
                return
            }

            var index = (pageLocation * 10) + 1;

            if (data.length == 0) {
                $("#list-locations tbody").html(`
                    <tr>
                        <td class="align-middle" colspan="3">Belum ada data provinsi</td>
                    </tr>
                `);
                return;
            }

            $("#list-locations tbody").html(`${data.map(function(item) {
                return `
                    <tr>
                        <td class="align-middle p-0">${index++}</td>
                        <td class="align-middle p-0">${item.province_name}</td>
                        <td class="align-middle p-0">
                            <h4 class="mt-2">
                                <a href="javascript:void(0)" class="text-success"><i class="icon-plus" onclick="selectIDLocation(${item.id}, '${item.province_name}')"></i></a>
                            </h4>
                        </td>
                    </tr>
                `;
            }).join('')}`);
        }

        function selectIDLocation(idLocation, itemLocation) {
            var index = selectedLocation.map(function(x) {
                return x.id;
            }).indexOf(idLocation);
            if (index == -1) {
                selectedLocation.push({
                    id: idLocation,
                    name: itemLocation
                });
            } else {
                toastr.error("Lokasi sudah dipilih")
            }
            renderSelectedLocation();
        }

        function renderSelectedLocation() {
            if (selectedLocation.length == 0) {
                $("#selected-locations").html('<p>Belum ada lokasi yang dipilih</p>')
            } else {
                $("#selected-locations").html(`${selectedLocation.map(function(item, index) {
                return `<a href="javascript:void(0)" 
                onclick="removeIDLocation(${index})"
                class="badge badge-primary p-2 m-2">${item.name} <i class="icon-close"></i></a>`;
            }).join('')}`)
            }
        }

        function removeIDLocation(position) {
            if (position > -1) {
                selectedLocation.splice(position, 1);
                renderSelectedLocation();
            }
        }

        // Create Product Dimension Info
        async function addProductDimension(formData) {
            await $.ajax({
                url: base_url.value + "/dashboard/m/managementProducts/create_product_dimension/",
                data: formData,
                type: "POST",
                contentType: false,
                processData: false,
            });
        }

        window.post = async function(url, data) {
            return await fetch(url, {
                method: "POST",
                body: data
            });
        }
    </script>
    <!-- END: APP JS-->
</body>

</html>