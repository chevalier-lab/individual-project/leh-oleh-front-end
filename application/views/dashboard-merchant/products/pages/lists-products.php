<!-- START: Card Data-->
<div class="row">
    <div class="col-12 mt-3">
        <div class="card">
            <div class="p-3 d-flex justify-content-between align-items-center">
                <div class="col-md-8 col-sm-6 card-header border-bottom p-1 d-flex">
                    <input type="text" class="form-control border-0 p-2 w-100 h-100 contact-search" placeholder="Cari ..." id="product-search">
                    <a href="javascript:void(0);" class="grid-style search-bar-menu" id="product-button-search"><i class="icon-magnifier"></i></a>
                </div>
                <a href="<?= base_url("index.php/dashboard/m/ManagementProducts/createProduct") ?>" class="bg-primary rounded text-white text-center">
                    <div class="p-2">
                        <i class="icon-plus align-middle text-white"></i>
                        <span>Produk Baru</span>
                    </div>
                </a>
            </div>
            <div class="card-body">
                <div id="container-list-product" class="row">

                </div>
                <div class="row mt-2">
                    <div class="col-12">
                        <div class="p-1">
                            <div class="btn-group" role="group" aria-label="Pagination Group">
                                <button class="btn btn-outline-secondary" type="button" id="location-prev"><i class="icon-arrow-left-circle"></i> Sebelumnya</button>
                                <button class="btn btn-outline-secondary" type="button" id="location-next">Selanjutnya <i class="icon-arrow-right-circle"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Card DATA-->