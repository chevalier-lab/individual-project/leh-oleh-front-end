<div class="col-12 mt-3">
    <div class="card">
        <div id="container-product" class="card-body">
            <h6 class="text-center">Sedang memuat data produk</h6>
        </div>
    </div>
</div>

<!-- <div class="col-12 mt-3 mb-3">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <ul class="nav nav-pills flex-column flex-sm-row justify-content-center ">
                        <li class="nav-item">
                            <a class="nav-link body-color h6 mb-0 active" data-toggle="tab" href="#Description"> Description </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link body-color h6 mb-0" data-toggle="tab" href="#Additional"> Additional Information</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link body-color h6 mb-0" data-toggle="tab" href="#Reviews">Reviews (3)</a>
                        </li>
                    </ul>
                    <div class="tab-content mt-5" id="myTabContent">
                        <div class="tab-pane fade show active" id="Description" role="tabpanel" aria-labelledby="Description">
                            <div class="row">
                                <div class="col-md-6">
                                    <p class="pb-3" lang="ca">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec,.</p>
                                    <p lang="ca">In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum.</p>
                                </div>
                                <div class="col-md-6">
                                    <p class="pb-3" lang="ca">Etiam sit ameta orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia.</p>
                                    <p lang="ca">Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. </p>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="Additional" role="tabpanel" aria-labelledby="Additional">
                            <div class="row">
                                <div class="col-md-6">
                                    <p class="pb-3" lang="ca">Etiam sit ameta orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia.</p>
                                    <p lang="ca">Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. </p>
                                </div>
                                <div class="col-md-6">
                                    <p class="pb-3" lang="ca">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec,.</p>
                                    <p lang="ca">In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum.</p>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="Reviews" role="tabpanel" aria-labelledby="Reviews">
                            <div class="row">
                                <div class="col-md-6">
                                    <p class="pb-3" lang="ca">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec,.</p>
                                    <p lang="ca">In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum.</p>
                                </div>
                                <div class="col-md-6">
                                    <p class="pb-3" lang="ca">Etiam sit ameta orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia.</p>
                                    <p lang="ca">Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> -->

<script>
    (function($) {
        "use strict";
        $(window).on("load", function() {
            $.ajax({
                url: base_url.value + "/dashboard/m/managementProducts/load_one_products/" + id_product.value,
                data: null,
                type: "GET",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    console.log(response.data)
                    if (response.code == 200) {
                        renderProducts(response.data)
                    } else {
                        toastr.error(response.message);
                    }
                }
            });
        });
    })(jQuery);

    function renderProducts(data = []) {
        $("#container-product").html(`
            <div class="row">
                <div class="col-md-12 col-lg-5">
                    <img style="width: 100%" class="img-fluid" alt="product detail" src="${data.uri}">
                </div>
                <div class="col-md-12 col-lg-7">
                    <div class="card-body border brd-gray border-top-0 border-right-0 border-left-0">
                        <h3 class="mb-0"><a href="javascript:void(0);" class="f-weight-500 text-primary">${data.product_name}</a></h3>
                    </div>
                    <div class="card-body border border-top-0 border-right-0 border-left-0">
                        <div class="clearfix">
                            <div class="float-left mr-2">
                                <ul class="list-inline mb-0">
                                    <li class="list-inline-item"><a href="javascript:void(0);" class="text-warning"><i class="icon-star"></i></a></li>
                                    <li class="list-inline-item"><a href="javascript:void(0);" class="text-warning"><i class="icon-star"></i></a></li>
                                    <li class="list-inline-item"><a href="javascript:void(0);" class="text-warning"><i class="icon-star"></i></a></li>
                                    <li class="list-inline-item"><a href="javascript:void(0);"><i class="icon-star"></i></a></li>
                                    <li class="list-inline-item"><a href="javascript:void(0);"><i class="icon-star"></i></a></li>
                                </ul>
                            </div>
                            <span>(3 customer reviews)</span>
                        </div>
                    </div>
                    <div class="card-body border brd-gray border-top-0 border-right-0 border-left-0">
                        <div class="row">
                            <div class="col-12">
                                ${displayPrice(data.price_selling, data.discount)}
                            </div>
                        </div>
                    </div>
                    <div class="card-body border brd-gray border-top-0 border-right-0 border-left-0">
                        <p class="mb-0" lang="ca">${data.description}</p>
                    </div>
                    <div class="card-body">
                        <ul class="list-unstyled">
                            <li class="font-weight-bold dark-color mb-2">Lokasi: ${data.location.map(function(item, position) {return `<span class="body-color font-weight-normal">${item.province_name} ${(position < (data.location.length - 1)) ? ", " : ""}</span>`}).join('')}
                            <li class="font-weight-bold dark-color mb-2">Kategori: ${data.categories.map(function(item) {return `<span class="body-color font-weight-normal"> ${item.category} </span>`})}</li>
                            <li class="font-weight-bold dark-color mb-2">Dimensi: 
                                <li class="body-color font-weight-normal">Panjang : ${data.info.length} cm</li>
                                <li class="body-color font-weight-normal">Lebar : ${data.info.width} cm</li>
                                <li class="body-color font-weight-normal">Tinggi : ${data.info.height} cm</li>
                                <li class="body-color font-weight-normal">Berat : ${data.info.weight} kg</li>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        `);
    }

    function displayPrice(price_selling, discount){
        if (discount != 0) {
            var priceAfterDiscount = Number(price_selling) - (Number(price_selling) * Number(discount) / 100);
            return `
            <div class="float-left">
                <h4 class="lato-font body-color mb-0"><del>${req.money(price_selling.toString(), "Rp ")} </del></h4>
            </div>
            <div class="float-left ml-2">
                <h4 class="lato-font mb-0 text-danger">${req.money(priceAfterDiscount.toString(), "Rp ")}</h4>(Discount ${discount}%)
            </div>
            `
        } else {
            return `
            <div class="float-left ml-2">
                <h4 class="lato-font mb-0">${req.money(price_selling.toString(), "Rp ")}</h4>
            </div>
            `
        }
    }
</script>