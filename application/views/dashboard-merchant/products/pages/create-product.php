<div class="col-12 mt-3">
    <div class="card">
        <div class="card-body">
            <div class="wizard mb-4">
                <div class="connecting-line"></div>
                <ul class="nav nav-tabs d-flex mb-3">
                    <li class="nav-item mr-auto">
                        <a class="nav-link position-relative round-tab text-left p-0 active border-0" data-toggle="tab" href="#id1">
                            <i class="icon-handbag position-relative text-white h5 mb-3"></i>
                            <small class="d-none d-md-block ">Informasi</small>
                        </a>
                    </li>
                    <li class="nav-item mx-auto">
                        <a class="nav-link position-relative round-tab text-sm-center text-left p-0 border-0" data-toggle="tab" href="#id2">
                            <i class="icon-picture position-relative text-white h5 mb-3"></i>
                            <small class="d-none d-md-block">Kelola Foto</small>
                        </a>
                    </li>
                    <li class="nav-item mx-auto">
                        <a class="nav-link position-relative round-tab text-sm-center text-left p-0 border-0" data-toggle="tab" href="#id3">
                            <i class="icon-pin position-relative text-white h5 mb-3"></i>
                            <small class="d-none d-md-block">Produk</small>
                        </a>
                    </li>
                    <li class="nav-item mx-auto">
                        <a class="nav-link position-relative round-tab text-sm-center text-left p-0 border-0" data-toggle="tab" href="#id4">
                            <i class="icon-tag position-relative text-white h5 mb-3"></i>
                            <small class="d-none d-md-block">Kategori</small>
                        </a>
                    </li>
                    <li class="nav-item ml-auto">
                        <a class="nav-link position-relative round-tab text-sm-center text-left p-0 border-0" data-toggle="tab" href="#id5">
                            <i class="icon-location-pin position-relative text-white h5 mb-3"></i>
                            <small class="d-none d-md-block">Lokasi</small>
                        </a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane fade active show" id="id1">
                        <h2 class="mt-2 text-center">Informasi Produk</h2>
                        <div class="form">
                            <form class="row">
                                <div class="form-group col-md-12">
                                    <label class=" ">Nama Produk</label>
                                    <input id="name" type="text" class="form-control bg-transparent" placeholder="">
                                </div>
                                <div class="form-group col-md-2">
                                    <label class=" ">Stok</label>
                                    <input id="jumlah" type="number" min="1" class="form-control bg-transparent" placeholder=""
                                    onkeyup="this.value = (isNaN(this.value)) ? 1 : this.value">
                                </div>
                                <div class="form-group col-md-2">
                                    <label class=" ">Diskon</label>
                                    <div class="input-group mb-3">
                                        <input id="diskon" class="form-control" type="number" oninput="javascript: if (this.value > 100) this.value = 100; else if (this.value < 0) this.value = 0" min="0" max="100" maxlength="3">
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="basic-addon2">%</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class=" ">Harga Modal</label>
                                    <input id="harga-beli" type="number" min="1" class="form-control bg-transparent" placeholder=""
                                    onkeyup="this.value = (isNaN(this.value)) ? 1 : this.value">
                                </div>
                                <div class="form-group col-md-4">
                                    <label class=" ">Harga Jual</label>
                                    <input id="harga-jual" type="number" min="1" class="form-control bg-transparent" placeholder=""
                                    onkeyup="this.value = (isNaN(this.value)) ? 1 : this.value">
                                </div>
                                <div class="col-md-4">
                                    <label class=" ">Sampul Produk</label>
                                    <div>
                                        <img id="create-cover-preview" src="" class="img-fluid img-thumbnail">
                                    </div>
                                    <label for="create-cover-product" class="file-upload btn btn-primary btn-block">
                                        <i class="fa fa-upload mr-2"></i>Pilih Sampul Produk
                                        <input id="create-cover-product" type="file" required>
                                    </label>
                                </div>
                                <div class="form-group col-md-8">
                                    <label class=" ">Deskripsi</label>
                                    <textarea id="deskripsi" cols="30" rows="10" class="form-control"></textarea>
                                </div>
                            </form>
                            <button type="submit" class="btn float-right btn-primary nexttab">Selanjutnya</button>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="id2">
                        <h2 class="mt-2 text-center">Kelola Foto Produk</h2>
                        <div id="grid" class="row">
                            
                        </div>
                        <label for="add-photo" class="file-upload btn btn-info btn-block">
                            <i class="fa fa-upload mr-2"></i>Tambah Foto Produk
                            <input id="add-photo" type="file" required multiple>
                        </label>
                        <div class="d-flex mt-4">
                            <button type="button" class="btn btn-primary prevtab">Sebelumnya</button>
                            <button type="button" class="btn btn-primary nexttab ml-auto">Selanjutnya</button>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="id3">
                        <h2 class="mt-2 text-center">Dimensi Produk</h2>
                        <div class="form">
                            <form class="row">
                                <div class="form-group col-md-3">
                                    <label class=" ">Panjang</label>
                                    <div class="input-group mb-3">
                                        <input id="length" value="0" class="form-control" type="number" oninput="javascript: if (this.value < 0) this.value = 0;">
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="basic-addon2">cm</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <label class=" ">Lebar</label>
                                    <div class="input-group mb-3">
                                        <input id="width" value="0" class="form-control" type="number" oninput="javascript: if (this.value < 0) this.value = 0;">
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="basic-addon2">cm</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <label class=" ">Tinggi</label>
                                    <div class="input-group mb-3">
                                        <input id="height" value="0" class="form-control" type="number" oninput="javascript: if (this.value < 0) this.value = 0;">
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="basic-addon2">cm</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <label class=" ">Berat</label>
                                    <div class="input-group mb-3">
                                        <input id="weight" value="0" class="form-control" type="number" oninput="javascript: if (this.value < 0) this.value = 0;">
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="basic-addon2">g</span>
                                        </div>
                                    </div>                                
                                </div>
                            </form>
                            <button type="submit" class="btn float-right btn-primary nexttab">Selanjutnya</button>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="id4">
                        <h2 class="mt-2 text-center">Kelola Kategori Produk</h2>
                        <div class="form p-2 text-center row">
                            <div class="col-md-6 p-1">
                                <div class="input-group col-12 p-1">
                                    <input type="text" class="form-control p-2 h-100 contact-search" placeholder="Search ..." id="categories-search">
                                    <div class="input-group-append">
                                        <span class="btn btn-outline-primary input-group-text" id="categories-button-search"><i class="icon-magnifier" aria-hidden="true"></i></span>
                                    </div>
                                </div>
                                <div class="table-responsive pr-1 pl-1 mb-0">

                                    <?php
                                    $this->load->view('components/table', array(
                                        "idTable" => "list-categories",
                                        "isCustomThead" => true,
                                        "thead" => '
                                            <tr>
                                                <th scope="col">#</th>
                                                <th scope="col">Kategori</th>
                                                <th scope="col">Deskripsi</th>
                                                <th scope="col">Ikon</th>
                                                <th scope="col">Aksi</th>
                                            </tr>
                                            ',
                                        "tbody" => ""
                                    )) ?>

                                </div>

                                <div class="row">
                                    <div class="col-12">
                                        <div class="p-1">
                                            <div class="btn-group" role="group" aria-label="Pagination Group">
                                                <button class="btn btn-outline-secondary" type="button" id="list-category-prev"><i class="icon-arrow-left-circle"></i> Sebelumnya</button>
                                                <button class="btn btn-outline-secondary" type="button" id="list-category-next">Selanjutnya <i class="icon-arrow-right-circle"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 d-flex justify-content-center align-items-center">
                                <div id="selected-categories">
                                    <h6>Silahkan pilih kategori pada tabel yang sudah disediakan dan tekan tombol <i class="icon-plus text-success"></i> untuk menambahkan</h6>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex">
                            <button type="button" class="btn btn-primary prevtab">Sebelumnya</button>
                            <button type="button" class="btn btn-primary nexttab ml-auto">Selanjutnya</button>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="id5">
                        <h2 class="mt-2 text-center">Kelola Lokasi Produk</h2>
                        <div class="form p-2 text-center row">
                            <div class="col-md-6 p-1">
                                <div class="input-group col-12 p-1">
                                    <input type="text" class="form-control p-2 h-100 contact-search" placeholder="Search ..." id="locations-search">
                                    <div class="input-group-append">
                                        <span class="btn btn-outline-primary input-group-text" id="locations-button-search"><i class="icon-magnifier" aria-hidden="true"></i></span>
                                    </div>
                                </div>
                                <div class="table-responsive pr-1 pl-1 mb-0">

                                    <?php
                                    $this->load->view('components/table', array(
                                        "idTable" => "list-locations",
                                        "isCustomThead" => true,
                                        "thead" => '
                                            <tr>
                                                <th scope="col">#</th>
                                                <th scope="col">Lokasi</th>
                                                <th scope="col">Aksi</th>
                                            </tr>
                                            ',
                                        "tbody" => ""
                                    )) ?>
                                </div>

                                <div class="row">
                                    <div class="col-12">
                                        <div class="p-1">
                                            <div class="btn-group" role="group" aria-label="Pagination Group">
                                                <button class="btn btn-outline-secondary" type="button" id="list-location-prev"><i class="icon-arrow-left-circle"></i> Sebelumnya</button>
                                                <button class="btn btn-outline-secondary" type="button" id="list-location-next">Selanjutnya <i class="icon-arrow-right-circle"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 d-flex justify-content-center align-items-center">
                                <div id="selected-locations">
                                    <h6>Silahkan pilih lokasi pada tabel yang sudah disediakan dan tekan tombol <i class="icon-plus text-success"></i> untuk menambahkan</h6>
                                </div>
                            </div>
                        </div>
                        <div class="form p-5 text-center">
                            <button type="submit" id="create-product" class="btn btn-primary">Simpan
                            <i class="icon-paper-plane"></i>
                            </button>
                            <div id="create-product-loader"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>