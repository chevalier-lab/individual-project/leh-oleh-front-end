<div class="col-12 mt-3">
    <div class="card">
        <h2 class="mt-2 text-center">Kelola Foto Produk</h2>
        <div id="grid" class="row p-3">

        </div>
        <div class="row m-2" id="button-button">
            <div class="col p-3">
                <label for="add-photo" class="file-upload btn btn-info btn-block">
                    <i class="fa fa-upload mr-2"></i>Tambah Foto Produk Baru
                    <input id="add-photo" type="file" required multiple>
                </label>
            </div>
            
        </div>
    </div>
</div>