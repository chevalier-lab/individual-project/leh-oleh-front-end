<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('components/head'); ?>

    <!-- START: Template CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.theme.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/simple-line-icons/css/simple-line-icons.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/flags-icon/css/flag-icon.min.css'); ?>">
    <!-- END Template CSS-->

    <!-- START: Page CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/social-button/bootstrap-social.css'); ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/sweetalert/sweetalert.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/toastr/toastr.min.css'); ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/select2/css/select2.min.css') ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/select2/css/select2-bootstrap.min.css') ?>" />
    <!-- END: Page CSS-->

    <!-- START: Custom CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/css/main.css'); ?>">
    <!-- END: Custom CSS-->

    <style>
        .cover-img {
            width: 100%;
            height: 200px;
            object-fit: cover;
        }
    </style>
</head>

<body id="main-container" class="default">

    <input type="hidden" name="base_url" id="base_url" value="<?= base_url("index.php"); ?>">
    <input type="hidden" name="api_url" id="api_url" value="<?= API_URI; ?>">

    <!-- START: Pre Loader-->
    <div class="se-pre-con">
        <div class="loader"></div>
    </div>
    <!-- END: Pre Loader-->

    <!-- START: Template JS-->
    <script src="<?= base_url('assets/dist/vendors/jquery/jquery-3.3.1.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/moment/moment.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/slimscroll/jquery.slimscroll.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/toastr/toastr.min.js'); ?>"></script>
    <script src="https://cdn.jsdelivr.net/npm/lozad/dist/lozad.min.js"></script>
    <script src="<?= base_url('assets/dist/vendors/sweetalert/sweetalert.min.js');?>"></script>
    <!-- END: Template JS-->

    <!-- START: Page Vendor JS-->
    <script src="<?= base_url('assets/dist/js/auth/request.js'); ?>"></script>
    <script>
        var base_url = document.getElementById("base_url");
        var api_url = document.getElementById("api_url");

        checkIsAlreadySetupLocation();
    </script>
    <!-- END: Page Vendor JS-->

    <!-- START: Main Content-->
    <?php

    // Load Navigation Bar
    $this->load->view('components/menus/navbar-merchant');

    // Load Sidebar
    $this->load->view('components/menus/sidebar');

    // Load Page
    $this->load->view('dashboard/template', array(
        'page' => "Produk Saya",
        'pageMap' => array(
            array(
                "label" => "Produk",
                "is_current" => false
            ),
            array(
                "label" => "Produk Saya",
                "is_current" => true
            ),
        ),
        'pageURI' => "dashboard-merchant/products/pages/lists-products",
    ));

    ?>
    <!-- END: Content-->

    <!-- START: Page Script JS-->
    <?php
    if (isset($error)) {
        $this->load->view("components/error-modal", array(
            "errorModalTitle" => $error["title"],
            "errorModalContent" => $error["content"],
            "errorModalDetail" => $error["details"]
        ));
    }

    // Update Product Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-edit-information",
        "modalTitle" => "Ubah Produk",
        "modalType" => "modal-md",
        "iconTitle" => "icon-check",
        "modalActionForm" => "#",
        "modalContentForm" => '
        <input type="hidden" id="id-product-information"/>
        <div class="row">
            <div class="form-group col-md-12 mb-3">
                <label for="product-name-information">Nama Produk</label>
                <input type="text" id="product-name-information" class="form-control"/> 
            </div>
            <div class="form-group col-md-6">
                <label for="product-harga-beli" >Harga Beli</label>
                <input id="product-harga-beli" type="number" min="1" class="form-control bg-transparent" placeholder="">
            </div>
            <div class="form-group col-md-6">
                <label for="product-harga-jual" >Harga Jual</label>
                <input id="product-harga-jual" type="number" min="1" class="form-control bg-transparent" placeholder="">
            </div>
            <div class="form-group col-md-6">
                <label for="product-qty" >Stok</label>
                <input id="product-qty" type="number" min="1" class="form-control bg-transparent" placeholder="">
            </div>
            <div class="form-group col-md-6">
                <label for="product-discount" >Diskon</label>
                <input id="product-discount" type="number" min="1" max="100" class="form-control bg-transparent" placeholder="">
            </div>
            <div class="form-group col-md-12">
                <label for="product-description-information">Deskripsi</label>
                <textarea id="product-description-information" cols="30" rows="10" class="form-control"></textarea>
            </div>
        </div>
        ',
        "modalButtonForm" => '<button id="product-information-button" type="button" class="btn btn-primary add-todo">Simpan
        <i class="icon-paper-plane"></i>
        </button>

        <div id="product-information-button-loader"></div>
        '
    ));

    // Update Category
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-edit-category",
        "modalTitle" => "Kelola Kategori",
        "modalType" => "modal-lg",
        "iconTitle" => "icon-check",
        "modalActionForm" => "#",
        "modalContentForm" => '
        <input type="hidden" id="id-product-categories"/>
        <div class="form p-2 text-center row">
            <div class="col-12 d-flex justify-content-center align-items-center">
                <div id="selected-categories">
                    
                </div>
            </div>
            <div class="col-12 p-1">
                <div class="input-group col-12 p-1">
                    <input type="text" class="form-control p-2 h-100 contact-search" placeholder="Search ..." id="categories-search">
                    <div class="input-group-append">
                        <span class="btn btn-outline-primary input-group-text" id="categories-button-search"><i class="icon-magnifier" aria-hidden="true"></i></span>
                    </div>
                </div>
                <div id="table-category"></div>
                <div class="row">
                    <div class="col-12">
                        <div class="p-1">
                            <div class="btn-group" role="group" aria-label="Pagination Group">
                                <button class="btn btn-outline-secondary" type="button" id="list-category-prev"><i class="icon-arrow-left-circle"></i> Sebelumnya</button>
                                <button class="btn btn-outline-secondary" type="button" id="list-category-next">Selanjutnya <i class="icon-arrow-right-circle"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>',
        "modalButtonForm" => '<button id="category-button" type="button" class="btn btn-primary add-todo">Simpan
        <i class="icon-paper-plane"></i>
        </button>
        
        <div id="category-button-loader"></div>
        '
    ));

    // Update Location
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-edit-location",
        "modalTitle" => "Kelola Lokasi",
        "modalType" => "modal-lg",
        "iconTitle" => "icon-check",
        "modalActionForm" => "#",
        "modalContentForm" => '
        <input type="hidden" id="id-product-locations"/>
        <div class="form p-2 text-center row">
            <div class="col-12 d-flex justify-content-center align-items-center">
                <div id="selected-locations">
                    
                </div>
            </div>
            <div class="col-12 p-1">
                <div class="input-group col-12 p-1">
                    <input type="text" class="form-control p-2 h-100 contact-search" placeholder="Search ..." id="locations-search">
                    <div class="input-group-append">
                        <span class="btn btn-outline-primary input-group-text" id="locations-button-search"><i class="icon-magnifier" aria-hidden="true"></i></span>
                    </div>
                </div>
                <div id="table-location"></div>
                <div class="row">
                    <div class="col-12">
                        <div class="p-1">
                            <div class="btn-group" role="group" aria-label="Pagination Group">
                                <button class="btn btn-outline-secondary" type="button" id="list-location-prev"><i class="icon-arrow-left-circle"></i> Prev</button>
                                <button class="btn btn-outline-secondary" type="button" id="list-location-next">Next <i class="icon-arrow-right-circle"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>',
        "modalButtonForm" => '<button id="location-button" type="button" class="btn btn-primary add-todo">Simpan
        <i class="icon-paper-plane"></i>
        </button>
        
        <div id="location-button-loader"></div>
        '
    ));

    // Set Discount Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-discount-product",
        "modalTitle" => "Diskon Produk",
        "modalType" => "modal-sm",
        "iconTitle" => "icon-check",
        "modalActionForm" => "#",
        "modalContentForm" => '
        <div class="form-group col-12">
        <input type="hidden" id="id-product-disc"/>
            <label>Diskon</label>
            <div class="input-group mb-3">
                <input id="set-discount" type="number" oninput="javascript: if (this.value > 100) this.value = 100;" min="0" max="100" maxlength="3" class="form-control">
                <div class="input-group-append">
                    <span class="input-group-text" id="basic-addon2">%</span>
                </div>
            </div>
        </div>
        ',
        "modalButtonForm" => '<button id="discount-button" type="button" class="btn btn-primary add-todo">Simpan
        <i class="icon-paper-plane"></i>
        </button>
        
        <div id="discount-button-loader"></div>
        '
    ));

    // Remove Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-remove-product",
        "modalTitle" => "Hapus Produk",
        "modalType" => "modal-md",
        "iconTitle" => "icon-close",
        "modalActionForm" => "#",
        "modalContentForm" => '
        <input type="hidden" id="id-product-delete"/>
        <p>Apakah anda yaking untuk menghapus <strong id="delete-product-name"></strong>?</p>
        ',
        "modalButtonForm" => '<button id="delete-product" type="button" class="btn btn-danger add-todo">Hapus
        <i class="icon-paper-plane"></i>
        </button>
        
        <div id="delete-product-loader"></div>
        '
    ));

    // Update Product Dimension Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-edit-dimension",
        "modalTitle" => "Ubah Dimensi Produk",
        "modalType" => "modal-md",
        "iconTitle" => "icon-check",
        "modalActionForm" => "#",
        "modalContentForm" => '
        <div id="dimension-alert"></div>
        <input type="hidden" id="id-product-dimension"/>
        <div class="row">
            <div class="form-group col-md-6">
                <label class=" ">Panjang</label>
                <div class="input-group mb-3">
                    <input id="length" value="0" class="form-control" type="number" oninput="javascript: if (this.value < 0) this.value = 0;">
                    <div class="input-group-append">
                        <span class="input-group-text" id="basic-addon2">cm</span>
                    </div>
                </div>
            </div>
            <div class="form-group col-md-6">
                <label class=" ">Lebar</label>
                <div class="input-group mb-3">
                    <input id="width" value="0" class="form-control" type="number" oninput="javascript: if (this.value < 0) this.value = 0;">
                    <div class="input-group-append">
                        <span class="input-group-text" id="basic-addon2">cm</span>
                    </div>
                </div>
            </div>
            <div class="form-group col-md-6">
                <label class=" ">Tinggi</label>
                <div class="input-group mb-3">
                    <input id="height" value="0" class="form-control" type="number" oninput="javascript: if (this.value < 0) this.value = 0;">
                    <div class="input-group-append">
                        <span class="input-group-text" id="basic-addon2">cm</span>
                    </div>
                </div>
            </div>
            <div class="form-group col-md-6">
                <label class=" ">Berat</label>
                <div class="input-group mb-3">
                    <input id="weight" value="0" class="form-control" type="number" oninput="javascript: if (this.value < 0) this.value = 0;">
                    <div class="input-group-append">
                        <span class="input-group-text" id="basic-addon2">g</span>
                    </div>
                </div>                                
            </div>
        </div>
        ',
        "modalButtonForm" => '<button id="product-dimension-button" type="button" class="btn btn-primary add-todo">Simpan
        <i class="icon-paper-plane"></i>
        </button>
        
        <div id="product-dimension-button-loader"></div>
        '
    ));

    ?>
    <!-- END: Page Script JS-->

    <!-- START: APP JS-->
    <script src="<?= base_url('assets/dist/js/app.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/js/home.script.js'); ?>"></script>
    <!-- END: APP JS-->

    <!-- START: Page Script JS-->
    <script src="<?= base_url('assets/dist/vendors/select2/js/select2.full.min.js') ?>"></script>
    <script src="<?= base_url('assets/dist/js/select2.script.js') ?>"></script>
    <!-- END: Page Script JS-->

</body>

</html>

<script>
    // Initial Pagination
    var selectedLocation = []
    var selectedCategory = [];
    var listSelectedLocation = []
    var listProvince = []

    var page = 0;
    var paginationClicked = false;
    var paginationDirection = "";

    var pageCategory = 0;
    var paginationClickedCategory = false;
    var paginationDirectionCategory = "";

    var pageLocation = 0;
    var paginationClickedLocation = false;
    var paginationDirectionLocation = "";

    (function($) {
        "use strict";
        $(window).on("load", function() {
            // Load Bank First Time
            loadMerchantProducts()

            // Handle Search
            $("#product-search").on("keydown", function(event) {
                if (event.keyCode == 32 || event.which == 32) loadMerchantProducts();
                else if ($("#product-search").val() == "") loadMerchantProducts();
            });

            $("#product-button-search").on("click", function(event) {
                loadMerchantProducts();
            });

            // Handle Search Categories
            $("#categories-search").on("keydown", function(event) {
                if (event.keyCode == 32 || event.which == 32) loadCategories();
                else if ($("#categories-search").val() == "") loadCategories();
            });

            $("#categories-button-search").on("click", function(event) {
                loadCategories();
            });

            // Handle Search Locations
            $("#locations-search").on("keyup", function(event) {
                loadLocations();
            });

            $("#locations-button-search").on("click", function(event) {
                loadLocations();
            });

            // Handle Pagination
            $("#location-prev").on("click", function() {
                if (page > 0)
                    page -= 1;
                paginationClicked = true;
                paginationDirection = "prev";
                loadMerchantProducts();
            });

            $("#location-next").on("click", function() {
                page += 1;
                paginationClicked = true;
                paginationDirection = "next";
                loadMerchantProducts();
            });

            // Handle Pagination Categories
            $("#list-category-prev").on("click", function() {
                if (pageCategory > 0)
                    pageCategory -= 1;
                paginationClickedCategory = true;
                paginationDirectionCategory = "prev";
                loadCategories();
            });

            $("#list-category-next").on("click", function() {
                pageCategory += 1;
                paginationClickedCategory = true;
                paginationDirectionCategory = "next";
                loadCategories();
            });

            // Handle Pagination Locations
            $("#list-location-prev").on("click", function() {
                if (pageLocation > 0)
                    pageLocation -= 1;
                paginationClickedLocation = true;
                paginationDirectionLocation = "prev";
                loadLocations();
            });

            $("#list-location-next").on("click", function() {
                pageLocation += 1;
                paginationClickedLocation = true;
                paginationDirectionLocation = "next";
                loadLocations();
            });

            // Handle Update Information Products
            $("#product-information-button").on("click", function() {

                var name = $("#product-name-information").val();
                var jumlah = $("#product-qty").val();
                var diskon = $("#product-discount").val();
                var harga_jual = $("#product-harga-jual").val();
                var harga_beli = $("#product-harga-beli").val();
                var description = $("#product-description-information").val();

                if (name == "") {
                    toastr.error("Harap mengisi nama produk");
                    return
                } else if (diskon == "" || diskon < 0) {
                    diskon = 0
                } else if (harga_jual == "" || harga_jual < 1) {
                    toastr.error("Harap mengisi harga jual");
                    return
                } else if (harga_beli == "" || harga_beli < 1) {
                    toastr.error("Harap mengisi harga beli");
                    return
                } else if (description == "") {
                    toastr.error("Harap mengisi deskripsi");
                    return
                }

                $("#product-information-button").hide();
                $("#product-information-button-loader").html(`
                <div class="loader-item"></div>
                `);

                var raw = req.raw({
                    product_name: name,
                    slug: name.toLowerCase().replace(" ", "-"),
                    description: description,
                    harga_beli: harga_beli,
                    harga_jual: harga_jual,
                    jumlah: jumlah,
                    diskon: diskon,
                })

                var formData = new FormData()
                formData.append("raw", raw)

                $.ajax({
                    url: base_url.value + "/dashboard/m/managementProducts/edit_product/" + $("#id-product-information").val(),
                    data: formData,
                    type: "POST",
                    contentType: false,
                    processData: false,
                    success: function(response) {

                        response = req.data(response)
                        if (response.code == 200) {
                            toastr.success("Berhasil mengubah informasi produk");
                            setTimeout(() => {
                                location.reload();
                            }, 1000);
                        } else {
                            $("#product-information-button").show();
                            $("#product-information-button-loader").html(``);
                            toastr.error(response.message);
                        }

                    }
                });
            });

            // Handle Edit Categories
            $("#category-button").on("click", function(e) {
                
                $("#category-button").hide();
                $("#category-button-loader").html(`
                <div class="loader-item"></div>
                `);

                var categories = []
                selectedCategory.forEach(function(item) {
                    categories.push(item.id)
                });

                if (categories.length == 0) {
                    e.preventDefault()
                    toastr.error("Pilih minimal 1 kategori");
                    $("#category-button").removeAttr("disabled");
                } else {

                    var raw = req.raw({
                        categories: categories,
                    })

                    var formData = new FormData()
                    formData.append("raw", raw)

                    $.ajax({
                        url: base_url.value + "/dashboard/m/managementProducts/edit_categories_product/" + $("#id-product-categories").val(),
                        data: formData,
                        type: "POST",
                        contentType: false,
                        processData: false,
                        success: function(response) {

                            response = req.data(response)
                            if (response.code == 200) {
                                toastr.success("Berhasil mengubah kategori produk");

                                location.reload();
                            } else {
                                toastr.error(response.message);

                                $("#category-button").show();
                                $("#category-button-loader").html(``);

                            }
                        }
                    });
                }
            });

            // Handle Discount
            $("#discount-button").on("click", function() {
                
                $("#discount-button").hide();
                $("#discount-button-loader").html(`
                <div class="loader-item"></div>
                `);

                var raw = req.raw({
                    discount: $("#set-discount").val(),
                })

                var formData = new FormData()
                formData.append("raw", raw)

                $.ajax({
                    url: base_url.value + "/dashboard/m/managementProducts/set_discount/" + $("#id-product-disc").val(),
                    data: formData,
                    type: "POST",
                    contentType: false,
                    processData: false,
                    success: function(response) {

                        response = req.data(response)
                        if (response.code == 200) {                            
                            toastr.success("Berhasil mengubah diskon produk");

                            setTimeout(() => {
                                location.reload();
                            }, 1000);
                        } else {
                            $("#discount-button").show();
                            $("#discount-button-loader").html(``);
                            toastr.error(response.message);
                        }
                    }
                });
            });

            // Handle Delete Product
            $("#delete-product").on("click", function() {
                
                $("#delete-product").hide();
                $("#delete-product-loader").html(`
                <div class="loader-item"></div>
                `);

                var raw = req.raw({
                    is_visible: 3,
                })

                var formData = new FormData()
                formData.append("raw", raw)

                $.ajax({
                    url: base_url.value + "/dashboard/m/managementProducts/delete_product/" + $("#id-product-delete").val(),
                    data: formData,
                    type: "POST",
                    contentType: false,
                    processData: false,
                    success: function(response) {
                        response = req.data(response)
                        if (response.code == 200) {
                            toastr.success("Berhasil menghapus produk");

                            setTimeout(() => {
                                location.reload();
                            }, 1000);
                        } else {
                            $("#delete-product").show();
                            $("#delete-product-loader").html(``);
                            toastr.error(response.message);
                        }
                    }
                });
            });

            // Handle Edit Locations
            $("#location-button").on("click", function(e) {
                
                $("#location-button").hide();
                $("#location-button-loader").html(`
                <div class="loader-item"></div>
                `);

                var locations = []
                selectedLocation.forEach(function(item) {
                    locations.push(item.id)
                });

                if (locations.length == 0) {
                    e.preventDefault()
                    toastr.error("Pilih minimal 1 lokasi");
                    
                    $("#location-button").show();
                    $("#location-button-loader").html(``);
                } else {

                    var raw = req.raw({
                        locations: locations,
                    })

                    var formData = new FormData()
                    formData.append("raw", raw)

                    $.ajax({
                        url: base_url.value + "/dashboard/m/managementProducts/edit_locations_product/" + $("#id-product-locations").val(),
                        data: formData,
                        type: "POST",
                        contentType: false,
                        processData: false,
                        success: function(response) {
                            $('form').find('input[type=text], input[type=password], input[type=number], input[type=email], textarea').val('');

                            response = req.data(response)
                            if (response.code == 200) {
                                toastr.success("Berhasil mengubah lokasi produk");

                                setTimeout(() => {
                                    location.reload();
                                }, 1000);

                            } else {
                                toastr.error(response.message);

                                $("#location-button").show();
                                $("#location-button-loader").html(``);
                            }
                        }
                    });
                }
            });

            // Handle Product Dimension
            $("#product-dimension-button").on("click", function() {

                if ($("#length").val() == "" || Number($("#length").val()) < 1) {
                    toastr.error("Harap mengisi panjang produk");
                    return
                }
                else if ($("#width").val() == "" || Number($("#width").val()) < 1) {
                    toastr.error("Harap mengisi lebar produk");
                    return
                }
                else if ($("#height").val() == "" || Number($("#height").val()) < 1) {
                    toastr.error("Harap mengisi tinggi produk");
                    return
                }
                else if ($("#weight").val() == "" || Number($("#weight").val()) < 1) {
                    toastr.error("Harap mengisi berat produk");
                    return
                }
                
                $("#product-dimension-button").hide();
                $("#product-dimension-button-loader").html(`
                <div class="loader-item"></div>
                `);

                // Add Product Dimension Info
                var raw = req.raw({
                    length: $("#length").val(),
                    width: $("#width").val(),
                    height: $("#height").val(),
                    weight: Number($("#weight").val()) / 1000
                })

                var formData = new FormData()
                formData.append("raw", raw)

                $.ajax({
                    url: base_url.value + "/dashboard/m/managementProducts/edit_product_dimension/" + $("#id-product-dimension").val(),
                    data: formData,
                    type: "POST",
                    contentType: false,
                    processData: false,
                    success: function(response) {

                        response = req.data(response)
                        if (response.code == 200) {
                            toastr.success("Berhasil mengubah dimension produk");

                            setTimeout(() => {
                                location.reload();
                            }, 1000);
                        } else {
                            toastr.error(response.message);

                            $("#product-dimension-button").show();
                            $("#product-dimension-button-loader").html(``);
                        }
                    }
                });
            });
        });
    })(jQuery);

    function loadMerchantProducts() {
        renderToWaiting()

        var raw = req.raw({
            page: page,
            search: $('#product-search').val(),
            order_by: "id",
            order_direction: "DESC"
        })

        var formData = new FormData()
        formData.append("raw", raw)

        $.ajax({
            url: base_url.value + "/dashboard/m/managementProducts/load_products",
            data: formData,
            type: "POST",
            contentType: false,
            processData: false,
            success: function(response) {
                response = req.data(response)
                if (response.code == 200) {
                    renderProduct(response.data)
                } else {
                    renderProduct([])
                }
            }
        });
    }

    function loadCategories() {
        renderToWaitingTable()

        var raw = req.raw({
            page: page,
            search: $('#categories-search').val(),
            order_by: "id",
            order_direction: "DESC"
        })

        var formData = new FormData()
        formData.append("raw", raw)

        $.ajax({
            url: base_url.value + "/dashboard/m/managementProducts/load_categories",
            data: formData,
            type: "POST",
            contentType: false,
            processData: false,
            success: function(response) {
                response = req.data(response)
                if (response.code == 200) {
                    renderTableCategory(response.data)
                } else {
                    renderTableCategory([])
                }
            }
        });
    }

    function renderToWaiting() {
        $("#container-list-product").html(`
            <h5 class="text-center col-12">Sedang memuat data produk</h5>
        `);
    }

    function renderProduct(data = []) {

        if (paginationClicked) {
            paginationClicked = false;
            if (data.length == 0) {
                if (paginationDirection == "next")
                    page -= 1;
                else page += 1;
            }
            paginationDirection = "";
            loadMerchantProducts();
            return
        }

        var index = (page * 10) + 1;

        if (data.length == 0) {
            $("#container-list-product").html(`
                <p class="text-center col-12">Belum ada data produk, tekan <span class="icon-plus"></span> untuk menambahkan</p>
            `);
            return;
        }

        $("#container-list-product").html(`${data.map(function(item) {
            return `
                <div class="col-md-3 col-sm-6 mt-2">
                    <div class="card">
                        <img class="card-img-top lozad cover-img" data-src="${item.uri}" 
                        src="${base_url.value + '/../assets/dist/images/broken-256.png'}"
                        alt="${item.label}">
                        <div class="card-body">
                        <p class="mb-2"><a href="<?= base_url("index.php/dashboard/m/ManagementProducts/detailProduct/") ?>${item.id}" class="font-weight-bold text-primary">${item.product_name}</a></p>
                        ${displayPrice(item.price_selling, item.discount)}
                            <div class="btn-group mb-1 mt-2 d-block">
                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Aksi</button>
                                <div class="dropdown-menu p-0">
                                    <a class="dropdown-item" data-toggle="modal" data-target="#modal-edit-information" href="javascript:void(0);" onclick="loadInformation(${item.id})">Informasi</a>
                                    <a class="dropdown-item" data-toggle="modal" data-target="#modal-edit-dimension" href="javascript:void(0);" onclick="loadDimension(${item.id})">Dimensi Produk</a>
                                    <a class="dropdown-item" href="<?= base_url('index.php/dashboard/m/managementProducts/managePhotos/') ?>${item.id}">Foto</a>
                                    <a class="dropdown-item" data-toggle="modal" data-target="#modal-edit-category" href="javascript:void(0);" onclick="loadSelectedCategory(${item.id})">Kategori</a>
                                    <a class="dropdown-item" data-toggle="modal" data-target="#modal-edit-location" href="javascript:void(0);" onclick="loadSelectedLocation(${item.id})">Lokasi</a>
                                    <a class="dropdown-item" data-toggle="modal" data-target="#modal-discount-product" href="javascript:void(0);" onclick="setDiscount(${item.id}, ${item.discount})">Diskon</a>
                                    <a class="dropdown-item" data-toggle="modal" data-target="#modal-remove-product" href="javascript:void(0);" onclick="removeProduct(${item.id},'${item.product_name}')">Hapus</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            `;
        }).join('')}`);

        setInterval(() => {
            const observer = lozad();
            observer.observe();
        }, 2000);
    }

    function displayPrice(price_selling, discount) {
        if (discount != 0) {
            var priceAfterDiscount = Number(price_selling) - (Number(price_selling) * Number(discount) / 100);
            return `
            <div class="d-inline-block"><del>${req.money(Number(price_selling).toString(), "Rp ")} </del></div>
            <div class="d-inline-block text-danger">${req.money(Number(priceAfterDiscount).toString(), "Rp ")}</div>
            `
        } else {
            return `
            <div class="d-inline-block">${req.money(Number(price_selling).toString(), "Rp ")}</div>
            `
        }
    }

    function setDiscount(idProduct, discount) {
        $('form').find('input[type=number]').val(discount);
        $("#id-product-disc").val(idProduct);
    }

    function removeProduct(idProduct, item) {
        $("#id-product-delete").val(idProduct);
        $("#delete-product-name").text(item)
    }

    function loadInformation(idProduct) {
        $('form').find('input[type=text], input[type=password], input[type=number], input[type=email], textarea').val('');
        
        $.ajax({
            url: base_url.value + "/dashboard/m/managementProducts/load_one_products/" + idProduct,
            type: "GET",
            contentType: false,
            processData: false,
            success: function(response) {
                response = req.data(response)
                var product = response.data
                if (response.code == 200) {
                    $("#id-product-information").val(product.id)
                    $("#product-name-information").val(product.product_name)
                    $("#product-harga-jual").val(product.price_selling)
                    $("#product-harga-beli").val(product.price_default)
                    $("#product-qty").val(product.qty)
                    $("#product-discount").val(product.discount)
                    $("#product-description-information").val(product.description)
                } else {
                    toastr.error("Gagal Mengambil Informasi Produk")
                }
            }
        });
    }

    // Function Manage Category
    function loadSelectedCategory(idProduct) {
        $('#id-product-categories').val(idProduct)
        $('#selected-categories').html('<p>Sedang memuat data kategori</p>')
        selectedCategory = []
        loadCategories()
        $.ajax({
            url: base_url.value + "/dashboard/m/managementProducts/load_one_products/" + idProduct,
            type: "GET",
            contentType: false,
            processData: false,
            success: function(response) {
                response = req.data(response)
                var categories = response.data.categories
                if (response.code == 200) {
                    categories.map(function(item) {
                        selectIDCategory(parseInt(item.id_m_categories), item.category)
                    })
                } else {
                    toastr.error("Gagal Mengambil Informasi Produk")
                }
            }
        });
    }

    function renderToWaitingTable() {
        $("#table-category").html(`
            <td class="align-middle">Sedang memuat data kategori</td>
        `);
    }

    function renderTableCategory(data = []) {

        if (paginationClicked) {
            paginationClicked = false;
            if (data.length == 0) {
                if (paginationDirection == "next")
                    page -= 1;
                else page += 1;
            }
            paginationDirection = "";
            loadCategories();
            return
        }

        var index = (page * 10) + 1;

        if (data.length == 0) {
            $("#table-category").html(`
                <p class="text-center col-12">Belum ada data kategori, tekan <span class="icon-plus"></span> untuk menambahkan</p>
            `);
            return;
        }
        $("#table-category").html(`
            <div class="table-responsive">
                <div class="row">
                    <div class="col-12">
                        <table class="display table table-bordered table-hover mx-2" style="width:100%">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Kategori</th>
                                    <th scope="col">Deskripsi</th>
                                    <th scope="col">Ikon</th>
                                    <th scope="col">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                ${data.map(function(item) {
                                    if (item.is_visible == "1") {
                                        return `
                                        <tr>
                                            <td class="align-middle">${index++}</td>
                                            <td class="align-middle">${item.category}</td>
                                            <td class="align-middle">${item.description}</td>
                                            <td class="align-middle h4"><i class="icon ${item.icon}"></td>
                                            <td class="align-middle">
                                                <h4>
                                                    <a href="javascript:void(0)" class="text-success"><i class="icon-plus" onclick="selectIDCategory(${item.id}, '${item.category}')"></i></a>
                                                </h4>
                                            </td>
                                        </tr>
                                    `;
                                    }
                                }).join('')}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        `)
    }

    function selectIDCategory(idCategory, itemCategory) {
        var index = selectedCategory.map(function(x) {
            return x.id;
        }).indexOf(idCategory);
        if (index == -1) {
            selectedCategory.push({
                id: idCategory,
                name: itemCategory
            });
        } else {
            toastr.error("Kategori sudah dipilih")
        }
        renderSelectedCategory();
    }

    function removeIDCategory(position) {
        if (position > -1) {
            selectedCategory.splice(position, 1);

            renderSelectedCategory();
        }
    }

    function renderSelectedCategory() {
        if (selectedCategory.length == 0) {
            $("#selected-categories").html('<p>Belum ada kategori yang dipilih</p>')
        } else {
            $("#selected-categories").html(`${selectedCategory.map(function(item, index) {
                return `<a href="javascript:void(0)" 
                onclick="removeIDCategory(${index})"
                class="badge badge-primary p-2 m-2">${item.name} <i class="icon-close"></i></a>`;
            }).join('')}`)
        }
    }

    // Function Manage Location
    function loadSelectedLocation(idProduct) {
        $('#id-product-locations').val(idProduct)
        $('#selected-locations').html('<p>Sedang memuat data lokasi</p>')
        selectedLocation = []
        loadLocations()
        $.ajax({
            url: base_url.value + "/dashboard/m/managementProducts/load_one_products/" + idProduct,
            type: "GET",
            contentType: false,
            processData: false,
            success: function(response) {
                response = req.data(response)
                console.log(response)
                var locations = response.data.location
                if (locations.length == 0) {
                    $("#selected-locations").html('<p>Belum ada lokasi yang dipilih</p>')
                }
                if (response.code == 200) {
                    locations.map(function(item) {
                        selectIDLocation(parseInt(item.id_m_locations), item.province_name)
                    })
                } else {
                    toastr.error("Gagal Mengambil Informasi Produk")
                }
            }
        });
    }

    function loadLocations() {
        renderToWaitingLocations()
        if (listProvince.length == 0) {
            $.ajax({
                url: base_url.value + "/dashboard/m/managementProducts/load_locations",
                data: null,
                type: "GET",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.code == 200) {
                        listProvince = response.data
                        paginationStatic()
                    } else {
                        renderToTableLocations([])
                    }
                }
            });
        } else {
            paginationStatic()
        }
    }

    function paginationStatic() {
        var sliceProvince = []
        if ($('#locations-search').val() != "") {
            sliceProvince = listProvince.filter(function(item) {
                return item.label.toUpperCase().indexOf($('#locations-search').val().toUpperCase()) > -1;
            })
        } else {
            switch (pageLocation) {
                case 0:
                    sliceProvince = listProvince.slice(0, 10)
                    break
                case 1:
                    sliceProvince = listProvince.slice(10, 20)
                    break
                case 2:
                    sliceProvince = listProvince.slice(20, 30)
                    break
                case 3:
                    sliceProvince = listProvince.slice(30, 34)
                    break
                default:
                    sliceProvince = []
            }
        }
        renderToTableLocations(sliceProvince)
    }

    function renderToWaitingLocations() {
        $("#table-location").html(`
            <tr>
                <td class="align-middle" colspan="3">Sedang memuat data lokasi</td>
            </tr>
        `);
    }

    function renderToTableLocations(data = []) {
        if (paginationClickedLocation) {
            paginationClickedLocation = false;
            if (data.length == 0) {
                if (paginationDirectionLocation == "next")
                    pageLocation -= 1;
                else pageLocation += 1;
            }
            paginationDirectionLocation = "";
            loadLocations();
            return
        }

        var index = (pageLocation * 10) + 1;

        if (data.length == 0) {
            $("#table-location").html(`
                    <tr>
                        <td class="align-middle" colspan="3">Belum ada data provinsi</td>
                    </tr>
                `);
            return;
        }

        $("#table-location").html(`
            <div class="table-responsive">
            <div class="row">
                <div class="col-12">
                    <table class="display table table-bordered table-hover mx-2" style="width:100%">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Lokasi</th>
                                <th scope="col">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            ${data.map(function(item) {
                                    return `
                                    <tr>
                                        <td class="align-middle p-0">${index++}</td>
                                        <td class="align-middle p-0">${item.province_name}</td>
                                        <td class="align-middle p-0">
                                            <h4 class="mt-2">
                                                <a href="javascript:void(0)" class="text-success"><i class="icon-plus" onclick="selectIDLocation(${item.id}, '${item.province_name}')"></i></a>
                                            </h4>
                                        </td>
                                    </tr>
                                `;
                            }).join('')}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        `);
    }

    function selectIDLocation(idLocation, itemLocation) {
        var index = selectedLocation.map(function(x) {
            return x.id;
        }).indexOf(idLocation);
        if (index == -1) {
            selectedLocation.push({
                id: idLocation,
                name: itemLocation
            });
        } else {
            toastr.error("Lokasi sudah dipilih")
        }
        renderSelectedLocation();
    }

    function renderSelectedLocation() {
        if (selectedLocation.length == 0) {
            $("#selected-locations").html('<p>Belum ada lokasi yang dipilih</p>')
        } else {
            $("#selected-locations").html(`${selectedLocation.map(function(item, index) {
                return `<a href="javascript:void(0)" 
                onclick="removeIDLocation(${index})"
                class="badge badge-primary p-2 m-2">${item.name} <i class="icon-close"></i></a>`;
            }).join('')}`)
        }
    }

    function removeIDLocation(position) {
        if (position > -1) {
            selectedLocation.splice(position, 1);
            renderSelectedLocation();
        }
    }

    // Add products location
    function addLocation(locationData) {
        $.ajax({
            url: base_url.value + "/dashboard/m/managementProducts/create_product_locations",
            data: locationData,
            type: "POST",
            contentType: false,
            processData: false,
        });
    }

    // Delete products location
    function deleteLocation(idLocationProduct) {
        $.ajax({
            url: base_url.value + "/dashboard/m/managementProducts/delete_product_locations/" + idLocationProduct,
            data: null,
            type: "GET",
            contentType: false,
            processData: false,
        });
    }

    // Load Product Dimension
    function loadDimension(idProduct) {
        $('form').find('input[type=text], input[type=password], input[type=number], input[type=email], textarea').val('');
        $.ajax({
            url: base_url.value + "/dashboard/m/managementProducts/load_product_dimension/" + idProduct,
            type: "GET",
            contentType: false,
            processData: false,
            success: function(response) {
                response = req.data(response)
                var product = response.data
                if (response.code == 200) {
                    if (product == null) {
                        $("#dimension-alert").html(`
                            <div class="alert alert-primary" role="alert">
                                Belum mengisi dimensi produk
                            </div>
                        `);
                    } else {
                        $("#dimension-alert").html(``);
                        $("#id-product-dimension").val(product.id_u_product)
                        $("#length").val(product.length)
                        $("#width").val(product.width)
                        $("#height").val(product.height)
                        $("#weight").val(Number(product.weight) * 1000)
                    }
                } else {
                    toastr.error("Gagal Mengambil Informasi Produk")
                }
            }
        });
    }
</script>