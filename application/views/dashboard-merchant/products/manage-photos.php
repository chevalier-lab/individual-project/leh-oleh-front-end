<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('components/head'); ?>

    <!-- START: Template CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.theme.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/simple-line-icons/css/simple-line-icons.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/flags-icon/css/flag-icon.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/select2/css/select2.min.css') ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/select2/css/select2-bootstrap.min.css') ?>" />
    <!-- END Template CSS-->

    <!-- START: Page CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/social-button/bootstrap-social.css'); ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/sweetalert/sweetalert.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/toastr/toastr.min.css'); ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/fancybox/jquery.fancybox.min.css'); ?>" />
    <!-- END: Page CSS-->

    <!-- START: Custom CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/css/main.css'); ?>">
    <style>
        /* Chrome, Safari, Edge, Opera */
        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }

        /* Firefox */
        input[type=number] {
            -moz-appearance: textfield;
        }

        .cover-img {
            width: 100%;
            height: 200px;
            object-fit: cover;
        }
    </style>
    <!-- END: Custom CSS-->
</head>

<body id="main-container" class="default">

    <input type="hidden" name="base_url" id="base_url" value="<?= base_url("index.php"); ?>">
    <input type="hidden" name="api_url" id="api_url" value="<?= API_URI; ?>">
    <input type="hidden" name="id_product" id="id_product" value="<?= $idProduct; ?>">
    <input type="hidden" name="id_u_product" id="id_u_product">

    <!-- START: Pre Loader-->
    <div class="se-pre-con">
        <div class="loader"></div>
    </div>
    <!-- END: Pre Loader-->

    <!-- START: Template JS-->
    <script src="<?= base_url('assets/dist/vendors/jquery/jquery-3.3.1.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/moment/moment.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/slimscroll/jquery.slimscroll.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/toastr/toastr.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/sweetalert/sweetalert.min.js'); ?>"></script>
    <!-- END: Template JS-->

    <!-- START: Page Vendor JS-->
    <script src="<?= base_url('assets/dist/js/auth/request.js'); ?>"></script>
    <script>
        var base_url = document.getElementById("base_url");
        var api_url = document.getElementById("api_url");
        var id_product = document.getElementById("id_product");

        checkIsAlreadySetupLocation();
    </script>
    <!-- END: Page Vendor JS-->

    <!-- START: Main Content-->
    <?php

    // Load Navigation Bar
    $this->load->view('components/menus/navbar-merchant');

    // Load Sidebar
    $this->load->view('components/menus/sidebar');

    // Load Page
    $this->load->view('dashboard/template', array(
        'page' => "Kelola Foto",
        'pageMap' => array(
            array(
                "label" => "Produk",
                "is_current" => false
            ),
            array(
                "label" => "Kelola Foto",
                "is_current" => true
            ),
        ),
        'pageURI' => "dashboard-merchant/products/pages/manage-photos",
    ));
    // Load Product View

    ?>
    <!-- END: Content-->

    <!-- START: Page Script JS-->
    <?php
    if (isset($error)) {
        $this->load->view("components/error-modal", array(
            "errorModalTitle" => $error["title"],
            "errorModalContent" => $error["content"],
            "errorModalDetail" => $error["details"]
        ));
    }

    // Change Cover Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-edit-cover",
        "modalTitle" => "Ubah Cover",
        "iconTitle" => "icon-pencil",
        "modalActionForm" => "#",
        "modalContentForm" => '
        <input type="hidden" id="id-cover" class="form-control"/>
        <div class="mb-2">
            <img id="edit-cover-preview" src="" class="img-fluid">
        </div>
        <label for="edit-cover" 
            class="file-upload btn btn-primary btn-block">
                <i class="fa fa-upload mr-2"></i>Pilih Foto ...
                <input id="edit-cover" type="file" required>
        </label>
        ',
        "modalButtonForm" => '<button type="button" class="btn btn-success add-todo" id="button-edit-cover" disabled="disabled">Simpan
        <i class="icon-paper-plane"></i>
        </button>'
    ));

    // Delete Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-delete-photo",
        "modalTitle" => "Hapus Foto",
        "iconTitle" => "icon-trash",
        "modalActionForm" => "#",
        "modalContentForm" => '
        <input type="hidden" id="id-delete-photo" class="form-control"/>
        <p>Apa anda yaking ingin menghapus foto ini?</p>
        <div class="mb-2">
            <img id="delete-photo-preview" src="" class="img-fluid">
        </div>
        ',
        "modalButtonForm" => '<button type="button" class="btn btn-danger add-todo" id="delete-photo">Hapus
        <i class="icon-paper-plane"></i>
        </button>'
    ));

    ?>
    <!-- END: Page Script JS-->

    <!-- START: APP JS-->
    <script src="<?= base_url('assets/dist/js/app.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/js/home.script.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/select2/js/select2.full.min.js') ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/fancybox/jquery.fancybox.min.js') ?>"></script>
    <script>
        var selectedPhoto = [];
        var change = 0;

        (function($) {
            "use strict";
            $(window).on("load", function() {
                loadPhotos()

                $('#grid').html('<h6 class="def col-12 text-center p-5">Sedang mengambil data foto produk</h6>')

                // Cover Edit Preview
                $("#edit-cover").on("change", function(event) {
                    $('#button-edit-cover').removeAttr("disabled");
                    previewCover(event, "edit-cover-preview")
                });

                // Handle Add Remove Photo
                $("#add-photo").on("change", function(event) {
                    var length = $("#add-photo").prop("files").length
                    for (var i = 0; i < length; i++) {
                        selectedPhoto.push($("#add-photo").prop("files")[i])
                        var nameFile = $("#add-photo").prop("files")[i].name
                        var elementPhoto = `
                            <div class="item col-12 col-md-6 col-lg-4 mb-4 cation-hover text-center Photography">
                                <div class="modImage">
                                    <img src="${window.URL.createObjectURL(this.files[i])}" alt="" class="portfolioImage img-fluid cover-img">
                                    <div class="d-flex">
                                        <a data-fancybox-group="gallery" href="${window.URL.createObjectURL(this.files[i])}" class="fancybox btn rounded-0 btn-dark w-50">View Large</a>
                                        <a href="javascript:void(0)" class="btn btn-danger rounded-0 w-50" onclick="deletePhotoSelected('${nameFile}',this.parentElement)">Delete</a>
                                    </div>
                                </div>
                            </div>
                            `
                        $('#grid').append(elementPhoto)
                        $('.fancybox').fancybox()
                        if (selectedPhoto.length != 0 && change == 0) {
                            change++
                            $('#button-button').append(`
                            <div class="col p-3">
                                <button type="submit" id="button-save" class="file-upload btn btn-success btn-block">Simpan Foto Produk Baru</button>
                            </div>
                            `)
                        }
                    }
                });

                // Update Cover
                $("#button-edit-cover").on("click", function() {
                    $("#button-edit-cover").attr("disabled", true);

                    var formData = new FormData();
                    formData.append("cover", $("#edit-cover").prop("files")[0]);

                    $.ajax({
                        url: base_url.value + "/dashboard/m/managementProducts/edit_cover/" + $('#id-cover').val(),
                        data: formData,
                        type: "POST",
                        contentType: false,
                        processData: false,
                        success: function(response) {
                            response = req.data(response)

                            $("#button-edit-cover").removeAttr("disabled");
                            $("#modal-edit-cover").modal('hide')

                            if (response.code == 200) {
                                toastr.success("Berhasil mengubah cover produk");
                                // Refresh Page
                                setTimeout(() => {
                                    location.assign(base_url.value + "/dashboard/m/managementProducts/managePhotos/" + id_product.value)
                                }, 1000);
                            } else {
                                toastr.error(response.message);
                            }
                        }
                    });
                });

                // Delete Photo
                $("#delete-photo").on("click", function() {
                    $("#delete-photo").attr("disabled", true);
                    $.ajax({
                        url: base_url.value + "/dashboard/m/managementProducts/delete_product_photo/" + $('#id-delete-photo').val(),
                        data: null,
                        type: "POST",
                        contentType: false,
                        processData: false,
                        success: function(response) {
                            response = req.data(response)

                            $("#delete-photo").removeAttr("disabled");
                            $("#modal-delete-photo").modal('hide')

                            if (response.code == 200) {
                                toastr.success("Berhasil menghapus foto produk");
                                // Refresh Page
                                setTimeout(() => {
                                    location.assign(base_url.value + "/dashboard/m/managementProducts/managePhotos/" + id_product.value)
                                }, 1000);
                            } else {
                                toastr.error(response.message);
                            }
                        }
                    });
                });

                // Upload New Photos
                $("#button-button").on("click", "#button-save",  async function() {
                    $("#button-save").attr("disabled", true);

                    // START SYNCHRONOUS
                    toastr.warning("Persiapan menyimpan " + selectedPhoto.length + " foto produk")
                    for (let i = 0; i < selectedPhoto.length; i++) {
                        var photoData = new FormData()
                        photoData.append("photo", selectedPhoto[i])
                        let res = await post(base_url.value + "/dashboard/m/managementProducts/create_photo_products/" + $('#id_u_product').val(), photoData)
                        let response = await res.json()
                        if (response.code == 200)
                            toastr.success("Berhasil Menyimpan foto " + (i + 1))
                        else
                            toastr.error("Gagal menyimpan foto " + (i + 1))
                        if (selectedPhoto.length == i + 1) {
                            setTimeout(() => {
                                location.assign(base_url.value + "/dashboard/m/managementProducts/managePhotos/" + id_product.value)
                            }, 1000);
                        }
                    }
                    
                    // Promise.all(
                    //     selectedPhoto.map((photo, index) => {
                    //             var photoData = new FormData()
                    //             photoData.append("photo", photo)
                    //             return post(
                    //                 base_url.value + "/dashboard/m/managementProducts/create_photo_products/" + $('#id_u_product').val(), 
                    //                 photoData)
                    //                 .then(res=> res.json()
                    //             )
                    //         }
                    //     )
                    // ).then(function(response){
                    //         response.forEach(result => {
                    //             if(result.code == 200){

                    //             }
                    //         })
                    //         toastr.success("Berhasil Menyimpan foto")
                    //         // setTimeout(() => {
                    //         //     location.assign(base_url.value + "/dashboard/m/managementProducts/managePhotos/" + id_product.value)
                    //         // }, 1000);
                    //     }
                    // )
                    // END SYNCHRONOUS

                    // for (var i = 0; i < selectedPhoto.length; i++) {
                    //     var photoData = new FormData()
                    //     photoData.append("photo", selectedPhoto[i])
                    //     addPhotoAsync(photoData, $('#id_u_product').val())
                    // }
                    // toastr.success("Berhasil menambahkan foto produk")
                    // setTimeout(() => {
                    //     location.assign(base_url.value + "/dashboard/m/managementProducts/managePhotos/" + id_product.value)
                    // }, 3000);
                });
            });
        })(jQuery);

        // Handle Cover Start
        function previewCover(event, idElement) {
            var reader = new FileReader();
            reader.onload = function() {
                var output = document.getElementById(idElement);
                output.src = reader.result;
            }
            reader.readAsDataURL(event.target.files[0]);
            $(`#${idElement}`).addClass("mb-3")
        }

        function changeCover(id_u_product, imageUrl) {
            $('#edit-cover-preview').attr("src", imageUrl);
            $('#id-cover').val(id_u_product);
        }

        function loadPhotos() {
            $.ajax({
                url: base_url.value + "/dashboard/m/managementProducts/load_one_products/" + id_product.value,
                type: "GET",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    var data = response.data
                    if (response.code == 200) {
                        $('#id_u_product').val(data.id_u_product)
                        loadCover(data.uri, data.id_u_product)
                        loadPhotosData(data.photos)
                    } else {
                        toastr.error("Gagal Mengambil Foto Produk")
                    }
                }
            });
        }

        function loadCover(imageUrl, id_u_product) {
            var elementPhoto = `
                <div class="item col-12 col-md-6 col-lg-4 mb-4 cation-hover text-center Photography">
                    <div class="modImage">
                        <img src="${imageUrl}" alt="" class="portfolioImage img-fluid cover-img">
                        <div class="d-flex">
                            <a data-fancybox-group="gallery" href="${imageUrl}" class="fancybox btn rounded-0 btn-dark w-50">View Large</a>
                            <a class="btn btn-success rounded-0 w-50" data-toggle="modal" data-target="#modal-edit-cover" href="javascript:void(0);" onclick="changeCover(${id_u_product}, '${imageUrl}')">Edit</a>
                        </div>
                    </div>
                </div>
            `
            $('#grid').append(elementPhoto)
            $('h6').remove('.def')
            $('.fancybox').fancybox()
        }

        function loadPhotosData(photos = []) {
            for (var i = 0; i < photos.length; i++) {
                var elementPhoto = `
                    <div class="item col-12 col-md-6 col-lg-4 mb-4 cation-hover text-center Photography">
                        <div class="modImage">
                            <img src="${photos[i].uri}" alt="" class="portfolioImage img-fluid cover-img">
                            <div class="d-flex">
                                <a data-fancybox-group="gallery" href="${photos[i].uri}" class="fancybox btn rounded-0 btn-dark w-50">View Large</a>
                                <a class="btn btn-danger rounded-0 w-50" data-toggle="modal" data-target="#modal-delete-photo" href="javascript:void(0);" onclick="deletePhoto('${photos[i].uri}', ${photos[i].id_u_product_photos})">Delete</a>
                            </div>
                        </div>
                    </div>
                `
                $('#grid').append(elementPhoto)
                $('.fancybox').fancybox()
            }
        }

        function deletePhoto(imageUrl, idPhoto) {
            $('#delete-photo-preview').attr("src", imageUrl);
            $('#id-delete-photo').val(idPhoto);
        }

        async function addPhotoAsync(photoData, id_u_product) {
            await $.ajax({
                url: base_url.value + "/dashboard/m/managementProducts/create_photo_products/" + id_u_product,
                data: photoData,
                type: "POST",
                contentType: false,
                processData: false,
                success: function(response) {
                    console.log(response)
                }
            });
        }

        // Remove Photo from array & element
        function deletePhotoSelected(name, element) {
            element.parentElement.parentElement.remove()
            for (var i = 0; i < selectedPhoto.length; i++) {
                if (selectedPhoto[i].name == name) {
                    selectedPhoto.splice(i, 1)
                }
            }
            if (selectedPhoto.length == 0) {
                let buttonSave = document.getElementById('button-button')
                buttonSave.removeChild(buttonSave.lastElementChild)
                change--
            }
        }

        window.post = async function(url, data) {
            return await fetch(url, {
                method: "POST",
                body: data
            });
        }
    </script>
    <!-- END: APP JS-->
</body>

</html>