<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('components/head'); ?>

    <!-- START: Template CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.theme.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/simple-line-icons/css/simple-line-icons.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/flags-icon/css/flag-icon.min.css'); ?>">
    <!-- END Template CSS-->

    <!-- START: Page CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/social-button/bootstrap-social.css'); ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/sweetalert/sweetalert.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/toastr/toastr.min.css'); ?>"/>
    <!-- END: Page CSS-->

    <!-- START: Custom CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/css/main.css'); ?>">
    <!-- END: Custom CSS-->
</head>

<body id="main-container" class="default">

    <input type="hidden" name="base_url" 
        id="base_url" value="<?= base_url("index.php"); ?>">
    <input type="hidden" name="api_url" 
        id="api_url" value="<?= API_URI; ?>">

    <!-- START: Pre Loader-->
    <div class="se-pre-con">
        <div class="loader"></div>
    </div>
    <!-- END: Pre Loader-->

    <!-- START: Template JS-->
    <script src="<?= base_url('assets/dist/vendors/jquery/jquery-3.3.1.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/moment/moment.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/slimscroll/jquery.slimscroll.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/toastr/toastr.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/sweetalert/sweetalert.min.js');?>"></script>
    <!-- END: Template JS-->

    <!-- START: Page Vendor JS-->
    <script src="<?= base_url('assets/dist/js/auth/request.js'); ?>"></script>
    <script>
        var base_url = document.getElementById("base_url");
        var api_url = document.getElementById("api_url");
        checkIsAlreadySetupLocation();
    </script>
    <!-- END: Page Vendor JS-->

    <!-- START: Main Content-->
    <?php

    // Load Navigation Bar
    $this->load->view('components/menus/navbar-merchant');

    // Load Sidebar
    $this->load->view('components/menus/sidebar');

    // Load Page
    $this->load->view('dashboard/template', array(
        'page' => "Daftar Transaksi",
        'pageMap' => array(
            array(
                "label" => "Transaksi",
                "is_current" => false
            ),
            array(
                "label" => "Daftar Transaksi",
                "is_current" => true
            ),
        ),
        'pageURI' => "dashboard-merchant/transactions/pages/lists-transaction",
    ));

    ?>
    <!-- END: Content-->

    <!-- START: Page Script JS-->
    <?php
    if (isset($error)) {
        $this->load->view("components/error-modal", array(
            "errorModalTitle" => $error["title"],
            "errorModalContent" => $error["content"],
            "errorModalDetail" => $error["details"]
        ));
    }

    // Detail Transaction Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-detail-transaction",
        "modalTitle" => "Detail Transaksi",
        "modalType" => "modal-xl",
        "iconTitle" => "icon-eye",
        "modalActionForm" => "#",
        "modalContentForm" => '
            <div class="row"
                id="detail-transaction">
            </div>
        ',
        "modalButtonForm" => '
            <div id="detail-transaction-action"></div>
        '
    ));

    // Detail Transaction Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-pickup-transaction",
        "modalTitle" => "Proses Pengantaran",
        "modalType" => "modal-md",
        "iconTitle" => "icon-eye",
        "modalActionForm" => "#",
        "modalContentForm" => '
            <div class="row"
                id="pickup-transaction">
            </div>
        ',
        "modalButtonForm" => '
            <div id="pickup-transaction-action"></div>
        '
    ));

    // Update Status Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-update-status",
        "modalTitle" => "Ubah Status",
        "modalType" => "modal-md",
        "iconTitle" => "icon-pencil",
        "modalActionForm" => "#",
        "modalContentForm" => '
        <input type="hidden" id="id-transaction" val>
        <input type="hidden" id="val-status">
        <p>Apa anda yakin ingin mengubah status transaksi <strong id="update-status-text"></strong> ?</p>
        ',
        "modalButtonForm" => '<button type="button" class="btn btn-primary add-todo" id="update-status">Ubah Status
        <i class="icon-paper-plane"></i>
        </button>'
    ));

    ?>
    <!-- END: Page Script JS-->

    <!-- START: APP JS-->
    <script src="<?= base_url('assets/dist/js/app.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/js/home.script.js'); ?>"></script>

    <script>
        // Initial Pagination
        var page = 0;
        var paginationClicked = false;
        var paginationDirection = "";
        var transactions = [], kurir = [];
        var shipper = null, locationShipper = null;

        (function ($) {
            "use strict";
            $(window).on("load", function () {
                // Load Transaction First Time
                loadTransaction()

                // Handle Order By
                $("#lists-transaction-order").on("change", function() {
                    loadTransaction();
                });

                $("#transaction-status").on("change", function() {
                    loadTransaction();
                });

                // Handle Search
                $("#lists-transaction-search").on("keydown", function(event) {
                    if (event.keyCode == 32 || event.which == 32) loadTransaction();
                    else if ($("#lists-transaction-search").val() == "") loadTransaction();
                });

                $("#lists-transaction-button-search").on("click", function(event) {
                    loadTransaction();
                });

                // Handle Pagination
                $("#lists-transaction-prev").on("click", function() {
                    if (page > 0)
                        page -= 1;
                    paginationClicked = true;
                    paginationDirection = "prev";
                    loadTransaction();
                });

                $("#lists-transaction-next").on("click", function() {
                    page += 1;
                    paginationClicked = true;
                    paginationDirection = "next";
                    loadTransaction();
                });

                // Update Status Transaction
                $("#update-status").on("click", function() {
                    $("#update-status").attr("disabled", true);

                    var raw = req.raw({
                        status: $('#val-status').val(),
                    })

                    var formData = new FormData()
                    formData.append("raw", raw)

                    $.ajax({
                        url: base_url.value + "/dashboard/m/managementTransactions/update_status_transaction/" + $('#id-transaction').val(),
                        data: formData,
                        type: "POST",
                        contentType: false,
                        processData: false,
                        success: function(response) {
                            console.log(response)
                            response = req.data(response)

                            $("#update-status").removeAttr("disabled");
                            $("#modal-update-status").modal('hide')

                            console.log(response)

                            if (response.code == 200) {
                                loadTransaction()
                                toastr.success("Berhasil mengubah status")
                            } else {
                                toastr.error("Gagal mengubah status")
                            }
                            
                        }
                    });
                });
            });
        })(jQuery);

        function loadTransaction() {
            renderToWaiting()

            var order = $("#lists-transaction-order").val();
            var order_direction = "DESC";
            switch (Number(order)) {
                case 1: 
                    order =  "uuti.full_name";
                    order_direction = "ASC";
                break;
                case 2: 
                    order =  "uuti.full_name";
                    order_direction = "DESC";
                break;
                case 3: 
                    order =  "uut.created_at";
                    order_direction = "ASC";
                break;
                case 4: 
                    order =  "uut.updated_at";
                    order_direction = "DESC";
                break;
                default:
                    order =  "id";
                    order_direction = "DESC";
                break;
            }
            var raw = req.raw({
                page: page,
                search: $("#lists-transaction-search").val(),
                order_by: order,
                order_direction: order_direction,
                status: $("#transaction-status").val()
            })

            var formData = new FormData()
            formData.append("raw", raw)

            $.ajax({
                url: base_url.value + "/dashboard/m/managementTransactions/load_transaction",
                data: formData,
                type: "POST",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.code == 200) {
                        transactions = response.data
                        renderToTable(response.data)
                    } else {
                        renderToTable([])
                    }
                }
                // ,
                // error: function(response) {
                //     console.log("ERROR", response);
                //     $("#btn-login").removeAttr("disabled");
                // }
            });
        }

        function renderToWaiting() {
            $("#lists-transaction tbody").html(`
                <tr>
                    <td class="align-middle" colspan="9">Sedang memuat data</td>
                </tr>
            `);
        }

        function renderToTable(data=[]) {
            
            if (paginationClicked) {
                paginationClicked = false;
                if (data.length == 0) {
                    if (paginationDirection == "next") 
                        page -= 1;
                    else page += 1;
                }
                paginationDirection = "";
                loadTransaction();
                return 
            }

            var index = (page * 10) + 1;

            if (data.length == 0) {
                $("#lists-transaction tbody").html(`
                    <tr>
                        <td class="align-middle" colspan="9">Belum ada data
                    </tr>
                `);
                return;
            }

            $("#lists-transaction tbody").html(`${data.map(function(item) {
                var status = Number(item.status);
                switch (status) {
                    case 0: status = '<span class="badge outline-badge-secondary">Belum membayar</span>'; break;
                    case 1: status = '<span class="badge outline-badge-info">Pembayaran Tervalidasi</span>'; break;
                    case 2: status = '<span class="badge outline-badge-warning">Pesanan Diproses</span>'; break;
                    case 3: status = '<span class="badge outline-badge-success">Pesanan Selesai</span>'; break;
                    default: status = '<span class="badge outline-badge-danger">Pesanan Dikembalikan</span>'; break;
                }
                return `
                    <tr>
                        <td class="align-middle">${index++}</td>
                        <td class="align-middle">${item.full_name}</td>
                        <td class="align-middle">${item.transaction_token}</td>
                        <td class="align-middle">${req.money(Number(item.total_price).toString(), "Rp ")}</td>
                        <td class="align-middle">${status}</td>
                        <td class="align-middle">${item.created_at}</td>
                        <td class="align-middle">${item.updated_at}</td>
                        <td class="align-middle">
                            <h4>
                                <a href="javascript:void(0);" data-toggle="modal" data-target="#modal-detail-transaction" 
                                onclick="loadDetailTransaction(${item.id})"
                                class="btn text-primary"><i class="icon-eye"></i></a>
                            </h4>
                        </td>
                        
                    </tr>
                `;
                // <td class="align-middle">
                //             <div class="btn-group mb-3">
                //                 <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Update Status</button>
                //                 <div class="dropdown-menu p-0" style="">
                //                     <a class="dropdown-item" data-toggle="modal" data-target="#modal-update-status" href="javascript:void(0);" onclick="loadIdTransaction(${item.id}, 0, this.textContent)">Not payment yet</a>
                //                     <a class="dropdown-item" data-toggle="modal" data-target="#modal-update-status" href="javascript:void(0);" onclick="loadIdTransaction(${item.id}, 1, this.textContent)">Payment verified</a>
                //                     <a class="dropdown-item" data-toggle="modal" data-target="#modal-update-status" href="javascript:void(0);" onclick="loadIdTransaction(${item.id}, 2, this.textContent)">Process</a>
                //                 </div>
                //             </div>
                //         </td>
            }).join('')}`);
        }

        function loadDetailTransaction(id) {
            var index = -1;

            transactions.forEach(function(item, position) {
                if (item.id == id) index = position;
            })

            transaction = transactions[index]

            $.ajax({
                url: base_url.value + "/dashboard/m/managementTransactions/detail_transaction/" + id,
                data: null,
                type: "GET",
                contentType: false,
                processData: false,
                success: function(response) {
                    console.log(response)
                    response = req.data(response)
                    if (response.code == 200) {
                        var data = response.data
                        var status = Number(transaction.status);
                        switch (status) {
                            case 0: status = '<span class="badge outline-badge-secondary">Belum membayar</span>'; break;
                            case 1: status = '<span class="badge outline-badge-info">Pembayaran Tervalidasi</span>'; break;
                            case 2: status = '<span class="badge outline-badge-warning">Pesanan Diproses</span>'; break;
                            case 3: status = '<span class="badge outline-badge-success">Pesanan Selesai</span>'; break;
                            default: status = '<span class="badge outline-badge-danger">Pesanan Dikembalikan</span>'; break;
                        }

                        if (transaction.status == 2) {
                            if (data.products.length > 0) {
                                shipper = {
                                    shipper_order_id: data.products[0].shipper_order_id,
                                    shipper_order_active: data.products[0].shipper_order_active,
                                    shipper_is_request_pickup: data.products[0].shipper_is_request_pickup,
                                    shipper_agent_id: data.products[0].shipper_agent_id,
                                    shipper_agent_name: data.products[0].shipper_agent_name
                                }
                                if (data.products[0].shipper_order_active == 0) {
                                    $("#detail-transaction-action").html(`
                                        <button class="btn btn-success" type="button"
                                        onclick="activationOrder(${id})">Aktifkan Ongkir</button>
                                    `);
                                } else {
                                    // <button class="btn btn-success" type="button">Batalkan Ongkir</button>
                                    loadDetailTransactionShipper(id)
                                }
                            } else {
                                shipper = null
                                $("#detail-transaction-action").html(``);
                            }
                        } else {
                            shipper = null
                            $("#detail-transaction-action").html(``);
                        }

                        $("#detail-transaction").html(`
                        <div class="col-12 col-md-4 col-sm-12">
                            <h5>Informasi Transaksi</h5>
                            <strong>ID TRX</strong>
                            <div class="mb-2">${transaction.transaction_token}</div>
                            <strong>Total Harga</strong>
                            <div class="mb-2">${transaction.total_price}</div>
                            <strong>Status TRX</strong>
                            <div class="mb-2">${status}</div>
                            <strong>Status Aktif Ongkir</strong>
                            <div class="mb-2">${(shipper != null) ? (shipper.shipper_order_active == 0) ? 
                                `<span class="badge outline-badge-secondary">Belum Diaktifkan</span>` : 
                                `<span class="badge outline-badge-success">Aktif</span>`
                                : `<span class="badge outline-badge-secondary">Belum Diaktifkan</span>`}</div>
                            <strong>Status Proses Pengantaran</strong>
                            <div class="mb-2">${(shipper != null) ? (shipper.shipper_is_request_pickup == 0) ? 
                                `<span class="badge outline-badge-secondary">Belum Diproses</span>` : 
                                `<span class="badge outline-badge-success">Diproses</span>`
                                : `<span class="badge outline-badge-secondary">Belum Diproses</span>`}</div>
                            <strong>Tanggal Buat</strong>
                            <div class="mb-2">${transaction.created_at}</div>
                            <strong>Tanggal Ubah</strong>
                            <div class="mb-2">${transaction.updated_at}</div>
                        </div>
                        <div class="col-12 col-md-4 col-sm-12">
                            <h5>Informasi Pengguna</h5>
                            <strong>Nama Pengguna</strong>
                            <div class="mb-2">${transaction.full_name}</div>
                            <strong>Nomor Telepon</strong>
                            <div class="mb-2">${transaction.phone_number}</div>
                            <strong>Email</strong>
                            <div class="mb-2">${transaction.email}</div>
                            
                            <div id="detail-courier"></div>
                        </div>
                        <div class="col-12 col-md-4 col-sm-12">
                            <h5>Lokasi Pengguna</h5>
                            <strong>Provinsi</strong>
                            <div class="mb-2">${transaction.province}</div>
                            <strong>Kota</strong>
                            <div class="mb-2">${transaction.city}</div>
                            <strong>Kecamatan</strong>
                            <div class="mb-2">${transaction.suburb}</div>
                            <strong>Kelurahan</strong>
                            <div class="mb-2">${transaction.area}</div>
                            <strong>Kode POS</strong>
                            <div class="mb-2">${transaction.postcode}</div>
                            <strong>Alamat</strong>
                            <div class="mb-2">${transaction.address}</div>
                        </div>

                        <div class="col-12">
                        <hr>
                        </div>

                        <div class="col-12 table-responsive">
                            <table class="display table table-bordered table-hover">
                                <thead>
                                    <th>ID TRX PRODUK</th>
                                    <th>Foto Produk</th>
                                    <th>Produk</th>
                                    <th>Jumlah</th>
                                    <th>Sub Total Harga</th>
                                    <th>Status</th>
                                    <th>Aksi</th>
                                </thead>
                                <tbody>
                                    
                                    ${data.products.map(function(item) {
                                        var statusNumber = Number(item.status);
                                        var status;
                                        switch (statusNumber) {
                                            case 0: status = '<span class="badge outline-badge-secondary">Belum membayar</span>'; break;
                                            case 1: status = '<span class="badge outline-badge-info">Pembayaran Tervalidasi</span>'; break;
                                            case 2: status = '<span class="badge outline-badge-warning">Pesanan Diproses</span>'; break;
                                            case 3: status = '<span class="badge outline-badge-success">Pesanan Selesai</span>'; break;
                                            default: status = '<span class="badge outline-badge-danger">Pesanan Dikembalikan</span>'; break;
                                        }

                                        return `
                                        <tr>
                                            <td>${item.transaction_token}</td>
                                            <td>
                                                <img src="${item.uri}" alt="${item.label}"
                                                    style="max-width: 80px">
                                            </td>
                                            <td>${item.product_name}</td>
                                            <td>${item.qty}</td>
                                            <td>${req.money(Number(item.sub_total_price).toString(), "Rp ")}</td>
                                            <td>${status}</td>
                                            <td>
                                            <div class="btn-group mb-3">
                                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" ${statusNumber == 2 ? 'disabled' : '' }>Update Status</button>
                                                <div class="dropdown-menu p-0" style="">
                                                    <a class="dropdown-item" href="javascript:void(0);" 
                                                    onclick="changeStatusProduct(${id}, ${item.id}, 2, this.textContent)">Proses</a>
                                                </div>
                                            </div>
                                            </td>
                                        </tr>
                                        `;
                                    }).join('')}

                                </tbody>
                            </table>
                        </div>
                        `)
                    } else {
                        $("modal-detail-transaction").modal('hide');
                        toastr.error(response.message);
                    }
                }
                // ,
                // error: function(response) {
                //     console.log("ERROR", response);
                //     $("#btn-login").removeAttr("disabled");
                // }
            });
        }

        function loadDetailTransactionShipper(id) {
            $('#detail-courier').html('Sedang memuat data kurir')
            // Load Agen
            $.ajax({
                url: base_url.value + "/services/shipper/orders/load_order_detail/" + shipper.shipper_order_id,
                data: null,
                type: "GET",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    console.log(response)

                    // console.log(response)

                    if (response.status == "success") {
                        var data = response.data
                        if (data.statusCode == 200) {
                            locationShipper = data.order.detail.origin
                            courierInfo = data.order.detail.courier
                            if (shipper.shipper_is_request_pickup == 0){
                                $("#detail-transaction-action").html(`
                                    <button class="btn btn-primary" type="button"
                                    data-dismiss="modal"
                                    data-toggle="modal"
                                    data-target="#modal-pickup-transaction"
                                    onclick="preparePickup(${id})">Proses Pengantaran</button>
                                `);

                                $('#detail-courier').html(`
                                    <h5 class="mt-5">Informasi Pengiriman</h5>
                                    <strong>Kurir</strong>
                                    <div class="mb-2">${courierInfo.name} - ${courierInfo.rate_name}</div>
                                    <strong>Durasi Pengiriman</strong>
                                    <div class="mb-2">${courierInfo.min_day} - ${courierInfo.max_day} hari</div>
                                    <strong>Ongkos Kirim</strong>
                                    <div class="mb-2">${req.money(Number(courierInfo.rate.value).toString(), "Rp ")}</div>
                                `)
                            }
                            else $("#detail-transaction-action").html(``);
                        } else {
                            toastr.error("Gagal memuat detail transaksi")
                        }
                    } else {
                        toastr.error("Gagal memuat detail transaksi")
                    }
                    
                }
            });
        }

        function preparePickup(parentID) {
            
            // Load Agen
            $.ajax({
                url: base_url.value + "/services/shipper/pickupRequest/load_agents/" + locationShipper.suburbID,
                data: null,
                type: "GET",
                contentType: false,
                processData: false,
                success: function(response) {
                    console.log(response)
                    response = req.data(response)

                    // console.log(response)

                    if (response.status == "success") {
                        var data = response.data
                        if (data.statusCode == 200) {
                            kurir = data.data;
                            $("#pickup-transaction").html(`
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="">Agen Ongkir</label>
                                        <select class="form-control" onchange="(this.value != '') ? detailPickup(${parentID}, this.value) : ''">
                                            <option value="" disabled selected>Pilih Agen</option>
                                            ${kurir.map(function(item) {
                                                return `<option value="${item.agent.id}">${item.agent.name}</option>`;
                                            }).join('')}
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12" id="pickup-transaction-preview"></div>
                            `);
                        } else {
                            $("#pickup-transaction").html(``);
                            toastr.error("Gagal memuat agen ongkir")
                        }
                    } else {
                        $("#pickup-transaction").html(``);
                        toastr.error("Gagal memuat agen ongkir")
                    }
                    
                }
            });
        }

        function detailPickup(parentID, id) {
            var index = -1;

            kurir.forEach(function(item, position) {
                if (item.agent.id == id) index = position;
            })

            var kurirSelected = kurir[index]

            $("#pickup-transaction-preview").html(`
                <strong>Nama Agen</strong>
                <div class="mb-2">${kurirSelected.agent.name}</div>
                <strong>Kota</strong>
                <div class="mb-2">${kurirSelected.city.name}</div>
                <strong>Nama PJ</strong>
                <div class="mb-2">${kurirSelected.contact.name}</div>
                <strong>Nomor Telepon PJ</strong>
                <div class="mb-2">${kurirSelected.contact.phone}</div>

                <button class="btn btn-primary btn-block" type="button"
                id="send-pickup-order"
                onclick="pickupOrder(${parentID}, ${id})">Proses Pengantaran</button>
            `);
        }

        function pickupOrder(parentID, id) {
            var index = -1;

            kurir.forEach(function(item, position) {
                if (item.agent.id == id) index = position;
            })

            var kurirSelected = kurir[index]

            var raw = req.raw({
                idOrders: shipper.shipper_order_id,
                agent_id: kurirSelected.agent.id,
                agent_name: kurirSelected.agent.name
            })

            console.log(raw)

            var formData = new FormData()
            formData.append("raw", raw)

            $("#send-pickup-order").attr("disabled", true)

            $.ajax({
                url: base_url.value + "/dashboard/m/managementTransactions/pickup_order",
                data: formData,
                type: "POST",
                contentType: false,
                processData: false,
                success: function(response) {
                    console.log(response)
                    response = req.data(response)

                    console.log(response)

                    if (response.code == 200) {
                        toastr.success("Berhasil meminta pengantaran order")
                        setTimeout(() => {
                            location.reload()
                        }, 1000)
                    } else {
                        toastr.error("Gagal mengaktifkan ongkir")
                        $("#send-pickup-order").removeAttr("disabled")
                    }
                    
                }
            });
        }

        function activationOrder(parentID) {
            var raw = req.raw({
                id_order: shipper.shipper_order_id
            })

            var formData = new FormData()
            formData.append("raw", raw)

            $.ajax({
                url: base_url.value + "/dashboard/m/managementTransactions/activation_order",
                data: formData,
                type: "POST",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)

                    if (response.code == 200) {
                        loadDetailTransaction(parentID)
                        loadTransaction()
                        toastr.success("Berhasil mengaktifkan ongkir")
                    } else {
                        toastr.error("Gagal mengaktifkan ongkir")
                    }
                    
                }
            });
        }

        function changeStatusProduct(parentID, id, status, text){
            var raw = req.raw({
                status: status
            })

            console.log(raw)

            var formData = new FormData()
            formData.append("raw", raw)

            $.ajax({
                url: base_url.value + "/dashboard/m/managementTransactions/update_status_transaction/" + id,
                data: formData,
                type: "POST",
                contentType: false,
                processData: false,
                success: function(response) {
                    console.log(response)
                    response = req.data(response)

                    console.log(response)

                    if (response.code == 200) {
                        loadDetailTransaction(parentID)
                        loadTransaction()
                        toastr.success("Berhasil mengubah status")
                    } else {
                        toastr.error("Gagal mengubah status")
                    }
                    
                }
            });
        }

        function loadIdTransaction(id, status, text){
            $('#id-transaction').val(id)
            $('#val-status').val(status)
            $('#update-status-text').text(text);
        }
    </script>
    <!-- END: APP JS-->
</body>

</html>