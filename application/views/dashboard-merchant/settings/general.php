<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('components/head'); ?>

    <!-- START: Template CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.theme.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/simple-line-icons/css/simple-line-icons.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/flags-icon/css/flag-icon.min.css'); ?>">
    <!-- END Template CSS-->

    <!-- START: Page CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/social-button/bootstrap-social.css'); ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/sweetalert/sweetalert.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/toastr/toastr.min.css'); ?>"/>
    <!-- END: Page CSS-->

    <!-- START: Custom CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/css/main.css'); ?>">
    <!-- END: Custom CSS-->
</head>

<body id="main-container" class="default">

    <input type="hidden" name="base_url" 
        id="base_url" value="<?= base_url("index.php"); ?>">
    <input type="hidden" name="api_url" 
        id="api_url" value="<?= API_URI; ?>">

    <!-- START: Pre Loader-->
    <div class="se-pre-con">
        <div class="loader"></div>
    </div>
    <!-- END: Pre Loader-->

    <!-- START: Template JS-->
    <script src="<?= base_url('assets/dist/vendors/jquery/jquery-3.3.1.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/moment/moment.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/slimscroll/jquery.slimscroll.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/toastr/toastr.min.js'); ?>"></script>
        <script src="https://cdn.jsdelivr.net/npm/lozad/dist/lozad.min.js"></script>
    <!-- END: Template JS-->

    <!-- START: Page Vendor JS-->
    <script src="<?= base_url('assets/dist/js/auth/request.js'); ?>"></script>
    <script>
        var base_url = document.getElementById("base_url");
        var api_url = document.getElementById("api_url");
    </script>
    <!-- END: Page Vendor JS-->

    <!-- START: Main Content-->
    <?php

    // Load Navigation Bar
    $this->load->view('components/menus/navbar-merchant');

    // Load Sidebar
    $this->load->view('components/menus/sidebar');

    // Load Page
    $this->load->view('dashboard/template', array(
        'page' => "Toko Saya",
        'pageMap' => array(
            array(
                "label" => "Pengaturan",
                "is_current" => false
            ),
            array(
                "label" => "Toko Saya",
                "is_current" => true
            )
        ),
        'pageURI' => "dashboard-merchant/settings/pages/general",
    ));

    // Change Logo
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-change-logo",
        "modalTitle" => "Ubah Gambar Toko",
        "modalType" => "modal-md",
        "iconTitle" => "icon-picture",
        "modalActionForm" => "#",
        "modalContentForm" => '
        <div>
            <img id="widget-selected-logo" src="" class="img-fluid">
        </div>
        <label for="fileUpload" 
            class="file-upload btn btn-primary btn-block">
                <i class="fa fa-upload mr-2"></i>Pilih gambar ...
                <input id="fileUpload" type="file">
        </label>
        ',
        "modalButtonForm" => '<button type="button"
        id="btn-change-logo"
        class="btn btn-primary add-todo">Simpan
        <i class="icon-paper-plane"></i>
        </button>
        
        <div id="change-logo-loader"></div>
        '
    ));

    ?>
    <!-- END: Content-->

    <!-- START: Page Script JS-->
    <?php
    if (isset($error)) {
        $this->load->view("components/error-modal", array(
            "errorModalTitle" => $error["title"],
            "errorModalContent" => $error["content"],
            "errorModalDetail" => $error["details"]
        ));
    }

    ?>
    <!-- END: Page Script JS-->

    <!-- START: APP JS-->
    <script src="<?= base_url('assets/dist/js/app.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/js/home.script.js'); ?>"></script>

    <script>
        // Initial Pagination
        var page = 0;
        var paginationClicked = false;
        var paginationDirection = "";

        var merchant;
        var provinsi = [], kota = [], kecamatan = [], kelurahan = [];

        (function ($) {
            "use strict";
            $(window).on("load", function () {
                // Load Bank First Time
                loadProvinsi();

                $("#fileUpload").on("change", function(event) {
                    var reader = new FileReader();
                    reader.onload = function()
                    {
                        var output = document.getElementById('widget-selected-logo');
                        output.src = reader.result;
                    }
                    reader.readAsDataURL(event.target.files[0]);
                    $("#widget-selected-logo").addClass("mb-3")
                });

                $("#btn-change-logo").on("click", function() {

                    var formData = new FormData()
                    var cover = $("#fileUpload")[0].files
                    if (cover.length == 0) {
                        toastr.error("Harap pilih logo terlebih dahulu");
                        return
                    } else {

                        $("#btn-change-logo").hide();
                        $("#change-logo-loader").html(`
                        <div class="loader-item"></div>
                        `);

                        formData.append("photo", cover[0])
                        $.ajax({
                            url: base_url.value + "/dashboard/m/Settings/edit_logo",
                            data: formData,
                            type: "POST",
                            contentType: false,
                            processData: false,
                            success: function(response) {
                                console.log(response)
                                response = req.data(response)

                                if (response.code != 200) {
                                    toastr.error(response.message);
                                    $("#btn-change-logo").show();
                                    $("#change-logo-loader").html(``);
                                } else {
                                    toastr.success("Berhasil mengubah foto toko");

                                    setTimeout(() => {
                                        location.assign(base_url.value + "/dashboard/m/Settings")
                                    }, 1000);
                                }
                            }
                            // ,
                            // error: function(response) {
                            //     console.log("ERROR", response);
                            //     $("#btn-login").removeAttr("disabled");
                            // }
                        });
                    }
                })

                $("#general-btn-save").on("click", function() {

                    var application_name = $("#create-application_name").val()
                    var address = $("#create-address").val()
                    var phone_number = $("#create-phone_number").val()

                    if (application_name == "") {
                        toastr.error("Harap isi nama toko terlebih dahulu");
                        return
                    } else if (phone_number == "") {
                        toastr.error("Harap isi nomor telepon terlebih dahulu");
                        return
                    } else if (address == "") {
                        toastr.error("Harap isi alamat terlebih dahulu");
                        return
                    }

                    $("#general-btn-save").hide();
                    $("#general-btn-save-loader").html(`
                    <div class="loader-item"></div>
                    `);

                    var raw = req.raw({
                        market_name: application_name,
                        market_address: address,
                        market_phone_number: phone_number
                    });

                    var formData = new FormData()
                    formData.append("raw", raw)

                    $.ajax({
                        url: base_url.value + "/dashboard/m/settings/update_general",
                        data: formData,
                        type: "POST",
                        contentType: false,
                        processData: false,
                        success: function(response) {
                            console.log(response)
                            response = req.data(response)

                            if (response.code == 200) {

                                toastr.success("Berhasil mengubah data toko");
                                setTimeout(() => {
                                    location.assign(base_url.value + "/dashboard/m/Settings")
                                }, 1000);

                            } else {
                                $("#general-btn-save").show();
                                $("#general-btn-save-loader").html(``);
                                toastr.error(response.message);
                            }
                        }
                        // ,
                        // error: function(response) {
                        //     console.log("ERROR", response);
                        //     $("#btn-login").removeAttr("disabled");
                        // }
                    });
                });

                $("#location-btn-save").on("click", function() {

                    var id_provinsi = $("#location-province").val()
                    var id_kota = $("#location-city").val()
                    var id_kecamatan = $("#location-substr").val()
                    var id_kelurahan = $("#location-area").val()
                    var kodepos = $("#location-postcode").val()
                    var alamat = $("#location-address").val()

                    var selected_province, selected_city, selected_subsurb, selected_area;

                    var index = -1;
                    provinsi.forEach(function(item, position) {
                        if (item.id == id_provinsi) index = position;
                    });

                    if (index > -1) selected_province = provinsi[index];
                    else {
                        console.log("Provinsi tidak ditemukan")
                        toastr.error("Harap pilih provinsi terlebih dahulu");
                        index = -1;
                        return;
                    }
                    
                    index = -1;
                    kota.forEach(function(item, position) {
                        if (item.id == id_kota) index = position;
                    });

                    if (index > -1) selected_city = kota[index];
                    else {
                        console.log("Kota tidak ditemukan")
                        toastr.error("Harap pilih kota terlebih dahulu");
                        index = -1;
                        return;
                    }

                    index = -1;
                    kecamatan.forEach(function(item, position) {
                        if (item.id == id_kecamatan) index = position;
                    });

                    if (index > -1) selected_subsurb = kecamatan[index];
                    else {
                        console.log("Kecamatan tidak ditemukan")
                        toastr.error("Harap pilih kecamatan terlebih dahulu");
                        index = -1;
                        return;
                    }

                    index = -1;
                    kelurahan.forEach(function(item, position) {
                        if (item.id == id_kelurahan) index = position;
                    });

                    if (index > -1) selected_area = kelurahan[index];
                    else {
                        console.log("Kelurahan tidak ditemukan")
                        toastr.error("Harap pilih kelurahan terlebih dahulu");
                        index = -1;
                        return;
                    }

                    if (alamat == "") {
                        console.log("Alamat tidak ditemukan")
                        toastr.error("Harap masukkan alamat terlebih dahulu");
                        index = -1;
                        return;
                    }

                    var raw = req.raw({
                        id_u_user_is_merchant: merchant.id,
                        province_id: id_provinsi,
                        province: selected_province.name,
                        city_id: id_kota,
                        city: selected_city.name,
                        suburb_id: id_kecamatan,
                        suburb: selected_subsurb.name,
                        area_id: id_kelurahan,
                        area: selected_area.name,
                        postcode: kodepos,
                        address: alamat
                    });

                    $("#location-btn-save").hide();
                    $("#location-btn-save-loader").html(`
                    <div class="loader-item"></div>
                    `);

                    var url = base_url.value + "/dashboard/m/settings/create_location";
                    if (merchant.location != null) url = base_url.value + "/dashboard/m/settings/update_location/" + merchant.id;

                    var formData = new FormData()
                    formData.append("raw", raw)

                    $.ajax({
                        url: url,
                        data: formData,
                        type: "POST",
                        contentType: false,
                        processData: false,
                        success: function(response) {
                            console.log(response)
                            response = req.data(response)

                            if (response.code == 200) {

                                toastr.success("Berhasil mengubah lokasi toko");
                                setTimeout(() => {
                                    location.assign(base_url.value + "/dashboard/m/settings")
                                }, 1000);

                            } else {
                                $("#location-btn-save").show();
                                $("#location-btn-save-loader").html(``);
                                toastr.error(response.message);
                            }
                        }
                        // ,
                        // error: function(response) {
                        //     console.log("ERROR", response);
                        //     $("#btn-login").removeAttr("disabled");
                        // }
                    });
                });

            });
        })(jQuery);

        function loadProvinsi() {
            $.ajax({
                url: base_url.value + "/services/shipper/locations/load_province",
                data: null,
                type: "GET",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.status == "success") {
                        provinsi = response.data.rows;
                        $("#location-province").html(`
                        <option value="" selected disabled>Pilih Provinsi</option>
                        ${provinsi.map(function(item) {
                            return `<option value="${item.id}">${item.name}</option>`;
                        }).join('')}`);
                    }

                    loadGeneral();
                }
                // ,
                // error: function(response) {
                //     console.log("ERROR", response);
                //     $("#btn-login").removeAttr("disabled");
                // }
            });
        }

        function loadCity(id, callback) {
            $.ajax({
                url: base_url.value + "/services/shipper/locations/load_city?id=" + id,
                data: null,
                type: "GET",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.status == "success") {
                        kota = response.data.rows;
                        $("#location-city").html(`
                        <option value="" selected disabled>Pilih Kota</option>
                        ${kota.map(function(item) {
                            return `<option value="${item.id}">${item.name}</option>`;
                        }).join('')}`);

                        callback();
                    }
                }
                // ,
                // error: function(response) {
                //     console.log("ERROR", response);
                //     $("#btn-login").removeAttr("disabled");
                // }
            });
        }

        function loadSubstr(id, callback) {
            $.ajax({
                url: base_url.value + "/services/shipper/locations/load_suburbs/" + id,
                data: null,
                type: "GET",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.status == "success") {
                        kecamatan = response.data.rows;
                        $("#location-substr").html(`
                        <option value="" selected disabled>Pilih Kecamatan</option>
                        ${kecamatan.map(function(item) {
                            return `<option value="${item.id}">${item.name}</option>`;
                        }).join('')}`);
                    }

                    callback();
                }
                // ,
                // error: function(response) {
                //     console.log("ERROR", response);
                //     $("#btn-login").removeAttr("disabled");
                // }
            });
        }

        function loadArea(id, callback) {
            $.ajax({
                url: base_url.value + "/services/shipper/locations/load_areas/" + id,
                data: null,
                type: "GET",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.status == "success") {
                        kelurahan = response.data.rows;
                        $("#location-area").html(`
                        <option value="" selected disabled>Pilih Kelurahan</option>
                        ${kelurahan.map(function(item) {
                            return `<option value="${item.id}">${item.name}</option>`;
                        }).join('')}`);

                        callback();
                    }
                }
                // ,
                // error: function(response) {
                //     console.log("ERROR", response);
                //     $("#btn-login").removeAttr("disabled");
                // }
            });
        }

        function loadPostCode(id) {
            var kodepos = "";
            var index = -1;
            kelurahan.forEach(function(item, position) {
                if (item.id == id) index = position;
            });
            kodepos = kelurahan[index].postcode;
            $("#location-postcode").val(kodepos);
        }

        function loadGeneral() {
            renderToWaiting()

            $.ajax({
                url: base_url.value + "/dashboard/m/settings/load_general",
                data: null,
                type: "GET",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.code == 200) {
                        merchant = response.data;
                        renderToTable(response.data)

                        if (merchant.location != null) {
                            var lokasi = merchant.location;
                            $("#location-province").val(lokasi.province_id)
                            loadCity(lokasi.province_id, function() {
                                $("#location-city").val(lokasi.city_id)

                                loadSubstr(lokasi.city_id, function() {
                                    $("#location-substr").val(lokasi.suburb_id)

                                    loadArea(lokasi.suburb_id, function() {
                                        $("#location-area").val(lokasi.area_id)
                                        $("#location-postcode").val(lokasi.postcode)
                                        $("#location-address").val(lokasi.address)
                                    });
                                });
                            });
                        }
                    } else {
                        renderToTable(null)
                    }
                }
                // ,
                // error: function(response) {
                //     console.log("ERROR", response);
                //     $("#btn-login").removeAttr("disabled");
                // }
            });
        }

        function renderToWaiting() {
            $("#lists-pages tbody").html(`
                <tr>
                    <td class="align-middle" colspan="11">Sedang memuat data</td>
                </tr>
            `);
        }

        function renderToTable(data=null) {
            
            if (data == null) return

            $("#general-logo").html(`
                <img 
                    alt="" class="img-fluid lozad"
                    data-src="${data.market_uri}"
                    src="${base_url.value + '/../assets/dist/images/broken-256.png'}">
            `);
            $("#create-application_name").val(data.market_name)
            $("#create-address").val(data.market_address)
            $("#create-phone_number").val(data.market_phone_number)

            setInterval(() => {
                const observer = lozad();
                observer.observe();
            }, 2000);
        }
    </script>
    <!-- END: APP JS-->
</body>

</html>