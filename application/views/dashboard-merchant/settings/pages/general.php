<div class="row">
    <div class="col-12 col-md-3">
        <div class="card">
            <div class="card-body" id="general-logo">
            </div>
        </div>
    </div>
    <div class="col-12 col-md-9">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-12 mb-3">
                        <button class="btn btn-primary"
                        data-toggle="modal" data-target="#modal-change-logo">Ganti Gambar Toko</button>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group mb-3">
                            <label for="create-application_name">Nama Toko</label>
                            <input type="text" id="create-application_name" 
                                name="create-application_name" 
                                class="form-control" placeholder="Contoh: Leh Oleh"/> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group mb-3">
                            <label for="create-phone_number">Nomor Telepon</label>
                            <input type="text" id="create-phone_number" 
                                name="create-phone_number" 
                                class="form-control" placeholder="Contoh: 6282119189xxx"/> 
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group mb-3">
                            <label for="create-address">Alamat</label>
                            <textarea type="text" id="create-address" 
                                name="create-address" 
                                class="form-control" placeholder="Contoh: Jl. Telekomunikasi No.01"></textarea>
                        </div>
                    </div>
                    <div class="col-12">
                    <button class="btn btn-primary"
                        type="button"
                        id="general-btn-save">Simpan <span class="icon-paper-plane"></span></button>
                    <div id="general-btn-save-loader"></div>
                    </div>
                </div>
            </div>
        </div>

        <br>

        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Lokasi Toko</h4>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group mb-3">
                            <label for="location-province">Provinsi</label>
                            <select id="location-province" class="form-control" onchange="loadCity(this.value, function(){})">
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group mb-3">
                            <label for="location-city">Kota</label>
                            <select id="location-city" class="form-control" onchange="loadSubstr(this.value, function(){})">
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group mb-3">
                            <label for="location-substr">Kecamatan</label>
                            <select id="location-substr" class="form-control" onchange="loadArea(this.value, function(){})">
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group mb-3">
                            <label for="location-area">Kelurahan</label>
                            <select id="location-area" class="form-control" onchange="loadPostCode(this.value)">
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group mb-3">
                            <label for="location-postcode">Kode POS</label>
                            <input type="text" id="location-postcode" class="form-control" disabled>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group mb-3">
                            <label for="location-address">Alamat</label>
                            <textarea type="text" id="location-address" 
                                name="location-address" 
                                class="form-control" placeholder="Contoh: Jl. Telekomunikasi No.01"></textarea>
                        </div>
                    </div>
                    <div class="col-12">
                    <button class="btn btn-primary"
                        type="button"
                        id="location-btn-save">Simpan <span class="icon-paper-plane"></span></button>
                    <div id="location-btn-save-loader"></div>
                    </div>
                </div>
            </div>
        </div>

        <br>

    </div>
</div>