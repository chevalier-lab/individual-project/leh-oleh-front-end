<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('components/head'); ?>

    <!-- START: Template CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.theme.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/simple-line-icons/css/simple-line-icons.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/flags-icon/css/flag-icon.min.css'); ?>">
    <!-- END Template CSS-->

    <!-- START: Page CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/social-button/bootstrap-social.css'); ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/sweetalert/sweetalert.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/vendors/toastr/toastr.min.css'); ?>"/>
    <!-- END: Page CSS-->

    <!-- START: Custom CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/dist/css/main.css'); ?>">
    <!-- END: Custom CSS-->
</head>

<body id="main-container" class="default">

    <input type="hidden" name="base_url" 
        id="base_url" value="<?= base_url("index.php"); ?>">
    <input type="hidden" name="api_url" 
        id="api_url" value="<?= API_URI; ?>">

    <!-- START: Pre Loader-->
    <div class="se-pre-con">
        <div class="loader"></div>
    </div>
    <!-- END: Pre Loader-->

    <!-- START: Template JS-->
    <script src="<?= base_url('assets/dist/vendors/jquery/jquery-3.3.1.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/jquery-ui/jquery-ui.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/moment/moment.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/slimscroll/jquery.slimscroll.min.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/vendors/toastr/toastr.min.js'); ?>"></script>
    <script src="https://cdn.jsdelivr.net/npm/lozad/dist/lozad.min.js"></script>
    <script src="<?= base_url('assets/dist/vendors/sweetalert/sweetalert.min.js');?>"></script>
    <!-- END: Template JS-->

    <!-- START: Page Vendor JS-->
    <script src="<?= base_url('assets/dist/js/auth/request.js'); ?>"></script>
    <script>
        var base_url = document.getElementById("base_url");
        var api_url = document.getElementById("api_url");
        checkIsAlreadySetupLocation();
    </script>
    <!-- END: Page Vendor JS-->

    <!-- START: Main Content-->
    <?php

    // Load Navigation Bar
    $this->load->view('components/menus/navbar-merchant');

    // Load Sidebar
    $this->load->view('components/menus/sidebar');

    // Load Page
    $this->load->view('dashboard/template', array(
        'page' => "Profil",
        'pageMap' => array(
            array(
                "label" => "Pengaturan",
                "is_current" => false
            ),
            array(
                "label" => "Profil",
                "is_current" => true
            )
        ),
        'pageURI' => "dashboard-merchant/settings/pages/profile",
    ));



    // Create Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-edit-profile",
        "modalTitle" => "Ubah Profil",
        "modalType" => "modal-lg",
        "iconTitle" => "icon-pencil",
        "modalActionForm" => "#",
        "modalContentForm" => '
        <div class="row">
            <div class="col-md-6">
                <div class="form-group mb-3">
                    <label for="create-full_name">Nama Lengkap</label>
                    <input type="text" id="create-full_name" 
                        name="create-full_name" 
                        class="form-control" placeholder="Contoh: Andy Maulana"/> 
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group mb-3">
                    <label for="create-username">Username</label>
                    <input type="text" id="create-username" 
                        disabled
                        name="create-username" 
                        class="form-control" placeholder="Contoh: aitekxxx"/> 
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group mb-3">
                    <label for="create-address">Alamat</label>
                    <input type="text" id="create-address" 
                        name="create-address" 
                        class="form-control" placeholder="Contoh: Jl. Telekomunikasi no.xxx"/> 
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group mb-3">
                    <label for="create-email">Email</label>
                    <input type="email" id="create-email" 
                        name="create-email" 
                        class="form-control" placeholder="Contoh: aitek@email.com"/> 
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group mb-3">
                    <label for="create-phone_number">Nomor Telepon</label>
                    <input type="number" id="create-phone_number" 
                        name="create-phone_number" 
                        class="form-control" placeholder="Contoh: 6282119189xxx"/> 
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group mb-3">
                    <label for="create-birth_of_date">Tanggal Lahir</label>
                    <input type="date" id="create-birth_of_date" 
                        name="create-birth_of_date" 
                        class="form-control"/> 
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group mb-3">
                    <label for="create-type">Jenis Pengguna</label>
                    <select id="create-type" 
                        name="create-type" 
                        disabled
                        class="form-control">
                    </select>
                </div>
            </div>
        </div>
        ',
        "modalButtonForm" => '<button type="button" 
        id="btn-edit-profile"
        class="btn btn-primary add-todo">Edit Profile</button>

        <div id="edit-profile-loader"></div>'
    ));

    // Banned Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-delete-account",
        "modalTitle" => "Hapus Akun",
        "modalType" => "modal-md",
        "iconTitle" => "icon-close",
        "modalActionForm" => "#",
        "modalContentForm" => '
        <p>Apa anda yakin ingin menghapus akun anda?</p>
        ',
        "modalButtonForm" => '<button type="button" 
        id="btn-delete-account"
        class="btn btn-danger add-todo">Hapus
        <i class="icon-paper-plane"></i>
        </button>

        <div id="delete-account-loader"></div>'
    ));

    // Change Photo Profile
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-change-photo",
        "modalTitle" => "Ubah Foto Profil",
        "modalType" => "modal-md",
        "iconTitle" => "icon-picture",
        "modalActionForm" => "#",
        "modalContentForm" => '
        <div>
            <img id="widget-selected-photo" src="" class="img-fluid">
        </div>
        <label for="fileUpload" 
            class="file-upload btn btn-primary btn-block">
                <i class="fa fa-upload mr-2"></i>Pilih Foto ...
                <input id="fileUpload" type="file">
        </label>
        ',
        "modalButtonForm" => '<button type="button" 
        id="btn-change-photo"
        class="btn btn-primary add-todo">Simpan
        <i class="icon-paper-plane"></i>
        </button>

        <div id="change-photo-loader"></div>'
    ));

    ?>
    <!-- END: Content-->

    <!-- START: Page Script JS-->
    <?php
    if (isset($error)) {
        $this->load->view("components/error-modal", array(
            "errorModalTitle" => $error["title"],
            "errorModalContent" => $error["content"],
            "errorModalDetail" => $error["details"]
        ));
    }

    ?>
    <!-- END: Page Script JS-->

    <!-- START: APP JS-->
    <script src="<?= base_url('assets/dist/js/app.js'); ?>"></script>
    <script src="<?= base_url('assets/dist/js/home.script.js'); ?>"></script>

    <script>
        // Initial Pagination
        var page = 0;
        var paginationClicked = false;
        var paginationDirection = "";

        (function ($) {
            "use strict";
            $(window).on("load", function () {
                // Load Bank First Time
                loadProfile()

                $("#btn-delete-account").on("click", function() {
                    
                    $("#btn-delete-account").hide();
                    $("#delete-account-loader").html(`
                    <div class="loader-item"></div>
                    `);

                    $.ajax({
                        url: base_url.value + "/dashboard/m/Settings/block_profile/",
                        data: null,
                        type: "GET",
                        contentType: false,
                        processData: false,
                        success: function(response) {
                            console.log(response)
                            response = req.data(response)

                            if (response.code != 200) {
                                toastr.error(response.message);
                                $("#btn-delete-account").hide();
                                $("#delete-account-loader").html(``);
                            } else {
                                toastr.success("Berhasil menghapus profil");
                                setTimeout(() => {
                                    location.assign(base_url.value + "/dashboard/m/settings/profile")
                                }, 1000);
                            }
                        }
                        // ,
                        // error: function(response) {
                        //     console.log("ERROR", response);
                        //     $("#btn-login").removeAttr("disabled");
                        // }
                    });
                })

                $("#fileUpload").on("change", function(event) {
                    var reader = new FileReader();
                    reader.onload = function()
                    {
                        var output = document.getElementById('widget-selected-photo');
                        output.src = reader.result;
                    }
                    reader.readAsDataURL(event.target.files[0]);
                    $("#widget-selected-photo").addClass("mb-3")
                });

                $("#btn-change-photo").on("click", function() {

                    var formData = new FormData()
                    var cover = $("#fileUpload")[0].files
                    if (cover.length == 0) {
                        toastr.error("Harap pilih photo terlebih dahulu");
                        return
                    } else {
                        $("#btn-change-photo").hide();
                        $("#change-photo-loader").html(`
                        <div class="loader-item"></div>
                        `);

                        formData.append("photo", cover[0])
                        $.ajax({
                            url: base_url.value + "/dashboard/m/Settings/edit_photo/",
                            data: formData,
                            type: "POST",
                            contentType: false,
                            processData: false,
                            success: function(response) {
                                console.log(response)
                                response = req.data(response)

                                if (response.code != 200) {
                                    toastr.error(response.message);
                                    $("#btn-change-photo").show();
                                    $("#change-photo-loader").html(``);
                                } else {
                                    toastr.success("Berhasil mengubah foto profil");
                                    setTimeout(() => {
                                        location.assign(base_url.value + "/dashboard/m/settings/profile")
                                    }, 1000);
                                }
                            }
                            // ,
                            // error: function(response) {
                            //     console.log("ERROR", response);
                            //     $("#btn-login").removeAttr("disabled");
                            // }
                        });
                    }
                })

                $("#btn-edit-profile").on("click", function() {

                    var full_name = $("#create-full_name").val()
                    var email = $("#create-email").val()
                    var alamat = $("#create-address").val()
                    var phone_number = $("#create-phone_number").val()
                    var tgl_lahir = $("#create-birth_of_date").val()

                    if (full_name == "") {
                        toastr.error("Harap isi nama lengkap terlebih dahulu");
                        return
                    } else if (phone_number == "") {
                        toastr.error("Harap isi nomor telepon terlebih dahulu");
                        return
                    } else if (alamat == "") {
                        toastr.error("Harap isi alamat terlebih dahulu");
                        return
                    } else if (tgl_lahir == "") {
                        toastr.error("Harap isi tanggal lahir terlebih dahulu");
                        return
                    } else if (email == "") {
                        toastr.error("Harap isi email terlebih dahulu");
                        return
                    }

                    $("#btn-edit-profile").hide();
                    $("#edit-profile-loader").html(`
                    <div class="loader-item"></div>
                    `);

                    var raw = req.raw({
                        full_name: full_name,
                        alamat: alamat,
                        tgl_lahir: tgl_lahir,
                        phone_number: phone_number,
                        email: email
                    });

                    var formData = new FormData()
                    formData.append("raw", raw)

                    $.ajax({
                        url: base_url.value + "/dashboard/m/settings/update_profile",
                        data: formData,
                        type: "POST",
                        contentType: false,
                        processData: false,
                        success: function(response) {
                            console.log(response)
                            response = req.data(response)

                            if (response.code == 200) {

                                toastr.success("Berhasil mengubah profil");
                                setTimeout(() => {
                                    location.assign(base_url.value + "/dashboard/m/settings/profile")
                                }, 1000);

                            } else {
                                $("#btn-edit-profile").show();
                                $("#edit-profile-loader").html(``);
                                toastr.error(response.message);
                            }
                        }
                        // ,
                        // error: function(response) {
                        //     console.log("ERROR", response);
                        //     $("#btn-login").removeAttr("disabled");
                        // }
                    });
                });
            });
        })(jQuery);

        function loadProfile() {
            renderToWaiting()

            $.ajax({
                url: base_url.value + "/dashboard/m/settings/load_profile",
                data: null,
                type: "GET",
                contentType: false,
                processData: false,
                success: function(response) {
                    response = req.data(response)
                    if (response.code == 200) {
                        renderToTable(response.data)
                    } else {
                        renderToTable(null)
                    }
                }
                // ,
                // error: function(response) {
                //     console.log("ERROR", response);
                //     $("#btn-login").removeAttr("disabled");
                // }
            });
        }

        function renderToWaiting() {
            $("#lists-pages tbody").html(`
                <tr>
                    <td class="align-middle" colspan="11">Sedang memuat data</td>
                </tr>
            `);
        }

        function renderToTable(data=null) {
            
            if (data == null) return

            var type = Number(data.type)
            switch(type) {
                case 0: type = "Guest"; break;
                case 1: type = "Toko"; break;
                case 2: type = "Administrator"; break;
                case 3: type = "Banned"; 
                    location.assign(base_url.value + "/general/auth/do_logout");
                break;
            }

            $("#profile-face").html(`
                <img 
                    alt="" class="img-fluid lozad"
                    data-src="${data.uri}"
                    src="${base_url.value + '/../assets/dist/images/broken-256.png'}">
            `);

            $("#profile-full_name").text(data.full_name);
            $("#profile-username").text(data.username);
            $("#profile-address").text(data.alamat);
            $("#profile-birth_of_date").text(data.tgl_lahir);
            $("#profile-email").text(data.email);
            $("#profile-phone_number").text(data.phone_number);
            $("#profile-type").text(type);

            $("#btn-modal-edit-profile").on("click", function() {
                $("#create-full_name").val(data.full_name)
                $("#create-username").val(data.username)
                $("#create-address").val(data.alamat)
                $("#create-birth_of_date").val(data.tgl_lahir)
                $("#create-email").val(data.email)
                $("#create-phone_number").val(data.phone_number)
                $("#create-type").html(`<option value="${data.type}">${type}</option>`);
            })

            setInterval(() => {
                const observer = lozad();
                observer.observe();
            }, 2000);
        }
    </script>
    <!-- END: APP JS-->
</body>

</html>