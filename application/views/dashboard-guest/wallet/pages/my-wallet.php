<div class="card mb-3">
    <div class="card-header">
        <h4 class="card-title">Dompet Saya</h4>
    </div>
    <div class="card-body">

        <div class="table-responsive pr-1 pl-1 mb-0">
        <?php

        $this->load->view('components/table', array(
            "idTable" => "my-wallet",
            "isCustomThead" => true,
            "thead" => '
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nama Dompet</th>
                <th scope="col">Saldo</th>
                <th scope="col">Status</th>
            </tr>
            ',
            "tbody" => ''
        )) ?>

        </div>

    </div>
</div>

<script>
// Initial Pagination
    var page = 0;
    var paginationClicked = false;
    var paginationDirection = "";

    (function ($) {
        "use strict";
        $(window).on("load", function () {
            // Load Bank First Time
            loadWallet()
        });
    })(jQuery);

    function loadWallet() {
        renderToWaiting()

        $.ajax({
            url: base_url.value + "/dashboard/g/managementWallets/current_balance",
            data: null,
            type: "GET",
            contentType: false,
            processData: false,
            success: function(response) {
                response = req.data(response)
                if (response.code == 200) {
                    renderToTable(response.data)
                } else {
                    renderToTable()
                }
            }
            // ,
            // error: function(response) {
            //     console.log("ERROR", response);
            //     $("#btn-login").removeAttr("disabled");
            // }
        });
    }

    function renderToWaiting() {
        $("#my-wallet tbody").html(`
            <tr>
                <td class="align-middle" colspan="4">Sedang memuat data</td>
            </tr>
        `);
    }

    function renderToTable(data=[]) {

        if (data.length == 0 || data == null) {
            $("#my-wallet tbody").html(`
                <tr>
                    <td class="align-middle" colspan="4">Belum ada data, 
                    tekan
                </tr>
            `);
            return;
        }

        var status = data.is_visible;
        if (status == "1") status = "<span class='badge badge-primary'>Aktif</span>";
        else if (status == "2") status = "<span class='badge badge-warning'>Pending/Review</span>";
        else if (status == "0") status = "<span class='badge badge-danger'>Banned</span>";

        $("#my-wallet tbody").html(`
            <tr>
                <td class="align-middle">1</td>
                <td class="align-middle">${data.wallet_name}</td>
                <td class="align-middle">${req.money(Number(data.balance).toString(), "Rp ")}</td>
                <td class="align-middle">${status}</td>
            </tr>
        `);
    }
</script>