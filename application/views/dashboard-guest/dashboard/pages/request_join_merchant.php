<div class="row mb-4">
    <div class="col-12 col-md-6 col-sm-12">
        
        <div class="card mb-4">
            <div class="card-body">
                <h5 class="card-title">Status Pengajuan Toko</h5>
                <div id="status-join-merchant"></div>
            </div>
        </div>

    </div>
    <div class="col-12 col-md-6 col-sm-12">
        <div class="card mb-4">
            <div class="card-body">

                <h5 class="card-title">Pengajuan Toko</h5>

                <div class="form-group mb-3">
                    <label for="create-no_ktp">No KTP</label>
                    <input type="number" id="create-no_ktp" class="form-control"
                    placeholder="Contoh: 1301198513"/> 
                </div>
                <div class="form-group mb-3">
                    <label for="create-nama_toko">Nama Toko</label>
                    <input type="text" id="create-nama_toko" class="form-control"
                    placeholder="Contoh: Dewi Cell"/> 
                </div>
                <div class="form-group mb-3">
                    <label for="create-no_hp">Nomor Telepon Toko</label>
                    <input type="text" id="create-no_hp" class="form-control"
                    placeholder="Contoh: 082119189xxx"/> 
                </div>
                <div class="form-group mb-3">
                    <label for="create-alamat">Alamat</label>
                    <input type="text" id="create-alamat" class="form-control"
                    placeholder="Contoh: Jl. Telekomunikasi no.xx"/> 
                </div>
                <div class="form-group mb-3">
                    <label for="create-foto_ktp">Foto KTP</label>
                    <input type="file" id="create-foto_ktp" class="form-control"/> 
                </div>
                <div class="form-group mb-3">
                    <label for="create-foto_pengguna">Foto Pengguna</label>
                    <input type="file" id="create-foto_pengguna" class="form-control"/> 
                </div>
                <div class="form-group mb-3">
                    <label for="create-foto_toko">Foto Toko</label>
                    <input type="file" id="create-foto_toko" class="form-control"/> 
                </div>

                <div class="form-group">
                    <div id="join-merchant-loader"></div>
                    <button type="button" class="btn btn-primary"
                    id="btn-join-merchant">
                        Ajukan
                        <i class="icon-paper-plane"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

(function($) {
    "use strict";
    $(window).on("load", function() {
        loadStatus()

        $("#btn-join-merchant").on("click", function() {
            
            $("#btn-join-merchant").hide();
            $("#join-merchant-loader").html(`
            <div class="loader-item"></div>
            `);

            if ($("#create-no_ktp").val() == "") {
                toastr.error("Harap mengisi nomor ktp");
                $("#btn-join-merchant").show();
                    $("#join-merchant-loader").html(`
                    `);
                return
            }
            else if ($("#create-nama_toko").val() == "") {
                toastr.error("Harap mengisi nama toko");
                $("#btn-join-merchant").show();
                    $("#join-merchant-loader").html(`
                    `);
                return
            }
            else if ($("#create-no_hp").val() == "") {
                toastr.error("Harap mengisi nomor telepon toko");
                $("#btn-join-merchant").show();
                    $("#join-merchant-loader").html(`
                    `);
                return
            }
            else if ($("#create-alamat").val() == "") {
                toastr.error("Harap mengisi alamat toko");
                $("#btn-join-merchant").show();
                    $("#join-merchant-loader").html(`
                    `);
                return
            }
            else if ($("#create-foto_ktp").prop("files").length == 0) {
                toastr.error("Harap mengisi mengunggah foto ktp");
                $("#btn-join-merchant").show();
                    $("#join-merchant-loader").html(`
                    `);
                return
            }
            else if ($("#create-foto_pengguna").prop("files").length == 0) {
                toastr.error("Harap mengisi mengunggah foto pengguna");
                $("#btn-join-merchant").show();
                    $("#join-merchant-loader").html(`
                    `);
                return
            }
            else if ($("#create-foto_toko").prop("files").length == 0) {
                toastr.error("Harap mengisi mengunggah foto toko");
                $("#btn-join-merchant").show();
                    $("#join-merchant-loader").html(`
                    `);
                return
            }

            swal({
                title: "Ajukan Join Merchant",
                text: "Apa anda yakin sudah mengisi form seluruhnya?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: 'btn-primary',
                confirmButtonText: 'Ajukan',
                cancelButtonText: "Batal",
                closeOnConfirm: true,
                closeOnCancel: false
            }, function(isConfirm){
                if (isConfirm){
                    doAjukanJoinMerchant()
                } else {
                    swal("Gagal!", "Pengajuan dibatalkan", "error");
                    $("#btn-join-merchant").show();
                    $("#join-merchant-loader").html(`
                    `);
                }
            });
        });
    });
})(jQuery);

function doAjukanJoinMerchant() {
    var formData = new FormData()
    formData.append("no_ktp", $("#create-no_ktp").val())
    formData.append("foto_ktp", $("#create-foto_ktp").prop("files")[0])
    formData.append("foto_pengguna", $("#create-foto_pengguna").prop("files")[0])
    formData.append("foto_toko", $("#create-foto_toko").prop("files")[0])
    formData.append("alamat", $("#create-alamat").val())
    formData.append("no_hp", $("#create-no_hp").val())
    formData.append("nama_toko", $("#create-nama_toko").val())

    $.ajax({
        url: base_url.value + "/dashboard/g/dashboard/create_join_merchant",
        data: formData,
        type: "POST",
        contentType: false,
        processData: false,
        success: function(response) {
            response = req.data(response)
            if (response.code == 200) {
                swal("Berhasil!", "Mengajukan permohonan", "success");
                location.reload();
            } else {
                swal("Gagal!", "Mengajukan permohonan", "error");
                $("#btn-join-merchant").show();
                $("#join-merchant-loader").html(`
                `);
            }
            loadStatus()
        }
    });
}

function loadStatus() {
    $.ajax({
        url: base_url.value + "/dashboard/g/dashboard/load_status_merchant",
        data: null,
        type: "GET",
        contentType: false,
        processData: false,
        success: function(response) {
            response = req.data(response)
            if (response.code == 200) {
                var data = response.data
                var status = Number(data.is_visible)
                if (status == 0) {
                    $("#status-join-merchant").html(`
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <strong>Mohon maaf</strong> permintaan pengajuan sebagai toko anda ditolak.
                        <button onclick="deleteRequestMerchant()" class="btn btn-danger">Hapus Permohonan</button>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    `);
                    $("#btn-join-merchant").hide()
                } else if (status == 1) {
                    $("#status-join-merchant").html(`
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>Berhasil</strong> permintaan pengajuan sebagai toko anda diterima, silahkan logout dan login kembali.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    `);
                } else {
                    $("#status-join-merchant").html(`
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        <strong>Sedang memproses</strong> permintaan pengajuan sebagai toko anda sedang diproses.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    `);
                    $("#btn-join-merchant").hide()
                }
            } else if (response.code == 404) {
                $("#status-join-merchant").html(`
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    <strong>Mohon maaf</strong> akun anda belum terdaftar sebagai toko, silahkan isi formulir Pengajuan Toko, untuk mendaftar sebagai toko.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                `);
            }
        }
    });
}

function deleteRequestMerchant() {
    $.ajax({
        url: base_url.value + "/dashboard/g/dashboard/delete_request_merchant",
        data: null,
        type: "GET",
        contentType: false,
        processData: false,
        success: function(response) {
            response = req.data(response)
            if (response.code == 200) {
                toastr.success("Berhasil menghapus permohonan");
            } else {
                toastr.error(response.message);
            }
            loadStatus()
        }
    });
}

</script>