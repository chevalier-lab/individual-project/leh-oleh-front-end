<div class="card mb-4">
    <div class="card-body">
        <h5 class="card-title">Selamat Datang di Leh Oleh</h5>
        <p class="card-text">Ini adalah halaman dashboard untuk pengguna, berikut lintasan untuk anda.</p>
        
        <div class="row">
        <?php
            if (isset($sidebar)) {
                foreach ($sidebar as $sideItem) {
                    ?>
                    <div class="col-12 col-md-4">
                        <p>
                            <i class="<?= $sideItem['icon']; ?> mr-1"></i> 
                            <strong><?= $sideItem['label']; ?></strong>
                        </p>
                    <?php
                    foreach ($sideItem['items'] as $item) {
                        ?>
                        <a href="<?= $item['link']; ?>" class="btn btn-outline-primary mb-2">
                        <i class="<?= $item['icon']; ?>"></i>  <?= $item['label']; ?>
                        </a>
                        <?php
                    } ?>
                    </div>
                    <?php
                }
            }
        ?>
        </div>

    </div>
</div>

<div class="row">
    <div class="col-12 col-md-8">

        <h4>Promo Untuk Anda</h4>

        <?php
            $this->load->view("components/general/slider");
        ?>

    </div>

    <div class="col-12 col-md-4">

        <?php
            $this->load->view("anonymous/content/last-post");
        ?>

    </div>

</div>

<script>
    (function($) {
        "use strict";
        $(window).on("load", function() {
            // Load Banner
            loadBanner()
        });
    })(jQuery);

    var banner = [];

    function loadBanner() {
        $.ajax({
            url: base_url.value + "/anonymous/landing/load_banner",
            data: null,
            type: "GET",
            contentType: false,
            processData: false,
            success: function(response) {
                response = req.data(response)
                if (response.code == 200) {
                    var data = response.data
                    banner = data;
                    setupBanner();
                    // for (let j = 0; j < data.length; j++) {
                    //     $(`<div class="carousel-item"><img class="d-block w-100" src="${data[j].uri}"></div>`).appendTo('.carousel-inner');
                    //     $('<li data-target="#carouselExampleCaption" data-slide-to="' + j + '"></li>').appendTo('.carousel-indicators')
                    // }

                    // $('.carousel-item').first().addClass('active');
                    // $('.carousel-indicators > li').first().addClass('active');
                    // $('#carouselExampleCaption').carousel();
                }
            }
        });
    }

    function setupBanner() {
        $('.owl-carousel').html(`${banner.map(function(item) {
            return `
                <img class="owl-lazy" data-src="${item.uri}" alt="">
            `;
        }).join('')}`);

        $('.owl-carousel').owlCarousel({
            items: 1,
            lazyLoad: true,
            lazyLoadEager: 1,
            loop: true,
            margin: 10,
            autoHeight: true,
            autoplay: true,
            autoplayTimeout: 10000,
            autoplayHoverPause: true,
            stagePadding: 50,
            nav: false,
            dots: false
        });
    }
</script>