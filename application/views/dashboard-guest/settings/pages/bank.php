<?php
    // Create Bank Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-create-bank",
        "modalTitle" => "Buat Akun Bank",
        "modalType" => "modal-md",
        "iconTitle" => "icon-plus",
        "modalActionForm" => "#",
        "modalContentForm" => '
            <div class="form-group mb-3">
                <label for="create-bank_choice">Pilih Bank</label>
                <select id="create-bank_choice" class="form-control">
                </select>
            </div>
            <div class="form-group mb-3">
                <label for="create-account_number">Nomor Rekening</label>
                <input type="number" id="create-account_number" class="form-control"
                placeholder="Contoh: 1301198513"/> 
            </div>
            <div class="form-group mb-3">
                <label for="create-account_name">Nama Akun</label>
                <input type="text" id="create-account_name" class="form-control"
                placeholder="Contoh: Andy Maulana"/> 
            </div>
        ',
        "modalButtonForm" => '<button type="button" class="btn btn-primary" 
        id="btn-create-bank">Simpan
        <i class="icon-paper-plane"></i>
        </button>
        <div id="create-bank-loader"></div>
        '
    ));

    // Update Bank Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-update-bank",
        "modalTitle" => "Ubah Akun Bank",
        "modalType" => "modal-md",
        "iconTitle" => "icon-plus",
        "modalActionForm" => "#",
        "modalContentForm" => '
            <div class="form-group mb-3">
                <label for="update-bank_choice">Pilih Bank</label>
                <select id="update-bank_choice" class="form-control">
                </select>
            </div>
            <div class="form-group mb-3">
                <label for="update-account_number">Nomor Rekening</label>
                <input type="number" id="update-account_number" class="form-control"
                placeholder="Contoh: 1301198513"/> 
            </div>
            <div class="form-group mb-3">
                <label for="update-account_name">Nama Akun</label>
                <input type="text" id="update-account_name" class="form-control"
                placeholder="Contoh: Andy Maulana"/> 
            </div>
            <div class="form-group mb-3">
                <label for="update-bank_choice">Status Akun Bank</label>
                <select id="update-bank_status" class="form-control">
                    <option value="0">Tidak Aktif</option>
                    <option value="1">Aktif</option>
                    <option value="2">Pending / Review</option>
                </select>
            </div>
        ',
        "modalButtonForm" => '<button type="button" class="btn btn-primary" 
        id="btn-update-bank">Simpan
        <i class="icon-paper-plane"></i>
        </button>
        <div id="update-bank-loader"></div>
        '
    ));
?>

<?php
    // Detail Top Up Proof Modal
    $this->load->view("components/modals/form", array(
        "hideModal" => "hide",
        "idElement" => "modal-delete-bank",
        "modalTitle" => "Hapus Akun Bank",
        "modalType" => "modal-lg",
        "iconTitle" => "icon-eye",
        "modalActionForm" => "#",
        "modalContentForm" => '
            <p>Apa anda yakin ingin menghapus <strong id="delete-bank_name"></strong>?</p>
        ',
        "modalButtonForm" => '
            <button class="btn btn-danger"
            type="button"
            id="btn-delete-bank">Hapus</button>
            <div id="delete-bank-loader"></div>
        '
    ));
?>

<div class="card mb-3">
    <div class="card-header">
        <h4 class="card-title">Akun Bank</h4>
    </div>
    <div class="card-body">
        <div class="pt-0 pb-0 pl-3 pr-3 mb-3 row">
            <div class="input-group col-12 col-md-5 p-1">
                <input type="text" class="form-control p-2 w-100 h-100 contact-search" 
                    placeholder="Cari ..."
                    id="lists-bank-search">
                    <div class="input-group-append">
                        <span class="btn btn-outline-primary input-group-text" id="lists-bank-button-search"><i class="icon-magnifier"
                            aria-hidden="true"></i></span>
                    </div>
            </div>
            <div class="col-12 col-md-5 p-1">
                <select class="form-control p-2 w-100 h-100 contact-search"
                    id="lists-bank-order">
                    <option value="0">Urutkan berdasarkan</option>
                    <option value="1">Nama (A-Z)</option>
                    <option value="2">Nama (Z-A)</option>
                </select>
            </div>
            <div class="col-12 col-md-2 pl-1 pr-1 my-2">
                <button data-toggle="modal" data-target="#modal-create-bank" 
                class="btn btn-outline-secondary btn-block"><i class="icon-plus"></i> Akun Bank</button>
            </div>
        </div>

        <div class="table-responsive pr-1 pl-1 mb-0">

        <?php

        $this->load->view('components/table', array(
            "idTable" => "lists-bank-table",
            "isCustomThead" => true,
            "thead" => '
            <tr>
                <th scope="col" rowspan="2">#</th>
                <th scope="col" rowspan="2">Bank</th>
                <th scope="col" rowspan="2">Nomor Rekening</th>
                <th scope="col" rowspan="2">Nama Akun</th>
                <th scope="col" rowspan="2">Status</th>
                <th scope="col" rowspan="2">Tgl Buat</th>
                <th scope="col" colspan="2">Aksi</th>
            </tr>
            <tr>
                <td>Ubah</td>
                <td>Hapus</td>
            </tr>
            ',
            "tbody" => ''
        )) ?>
        
        </div>

        <div class="row">
            <div class="col-12">
                <div class="p-1">
                    <div class="btn-group" role="group" aria-label="Pagination Group">
                        <button class="btn btn-outline-secondary" type="button" 
                        id="lists-bank-prev"><i class="icon-arrow-left-circle"></i> Prev</button>
                        <button class="btn btn-outline-secondary" type="button" 
                        id="lists-bank-next">Next <i class="icon-arrow-right-circle"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

    function randomXToY(minVal,maxVal)
    {
        var randVal = minVal+(Math.random()*(maxVal-minVal));
        return Math.round(randVal);
    }

    var page = 0;
    var paginationClicked = false;
    var paginationDirection = "";
    var dataBankAccount = []
    var selectedTagihanTopUp = null;

    (function ($) {
        "use strict";
        $(window).on("load", function () {
            loadBank()

            loadBankAccount()

            $("#btn-create-bank").on("click", function() {
                var raw = req.raw({
                    id_m_banks: $("#create-bank_choice").val(),
                    account_number: $("#create-account_number").val(),
                    account_name: $("#create-account_name").val(),
                })

                if ($("#create-bank_choice").val() == "") {
                    toastr.error("Harap pilih bank");
                    return
                }
                else if ($("#create-account_number").val() == "") {
                    toastr.error("Harap masukan nomor rekening");
                    return
                }
                else if ($("#create-account_name").val() == "") {
                    toastr.error("Harap masukan nama akun");
                    return
                }

                $("#btn-create-bank").hide();
                $("#create-bank-loader").html(`
                <div class="loader-item"></div>
                `);

                var formData = new FormData();
                formData.append("raw", raw);

                $.ajax({
                    url: base_url.value + "/dashboard/g/settings/create_bank_account",
                    data: formData,
                    type: "POST",
                    contentType: false,
                    processData: false,
                    success: function(response) {
                        response = req.data(response)
                        if (response.code == 200) {
                            toastr.success("Berhasil membuat akun bank");
                            setTimeout(() => {
                                location.reload();
                            }, 1000);
                        } else {
                            $("#btn-create-bank").show();
                            $("#create-bank-loader").html(``);
                            toastr.error(response.message);
                        }
                        $("#modal-create-bank").modal("hide");
                    }
                    // ,
                    // error: function(response) {
                    //     console.log("ERROR", response);
                    //     $("#btn-login").removeAttr("disabled");
                    // }
                });
            })
            
            $("#btn-update-bank").on("click", function() {
                updateBankAccount()
            })
            
            $("#btn-delete-bank").on("click", function() {
                deleteBankAccount()
            })
        });
    })(jQuery);

    function loadBank() {
        $.ajax({
            url: base_url.value + "/dashboard/g/settings/load_banks",
            data: null,
            type: "GET",
            contentType: false,
            processData: false,
            success: function(response) {
                response = req.data(response)
                if (response.code == 200) {
                    var data = response.data;
                    $("#create-bank_choice").html(`${data.map(function(item, position) {
                        return `<option value="${item.id}">${item.bank_name}</option>`
                    }).join('')}`);

                    $("#update-bank_choice").html(`${data.map(function(item, position) {
                        return `<option value="${item.id}">${item.bank_name}</option>`
                    }).join('')}`);
                } else {
                    
                }
            }
            // ,
            // error: function(response) {
            //     console.log("ERROR", response);
            //     $("#btn-login").removeAttr("disabled");
            // }
        });
    }

    function loadBankAccount() {
        $.ajax({
            url: base_url.value + "/dashboard/g/settings/load_bank_account",
            data: null,
            type: "GET",
            contentType: false,
            processData: false,
            success: function(response) {
                response = req.data(response)
                if (response.code == 200) {
                    var data = response.data;
                    dataBankAccount = data;
                } else {
                    dataBankAccount = [];
                }

                renderToTable();
            }
            // ,
            // error: function(response) {
            //     console.log("ERROR", response);
            //     $("#btn-login").removeAttr("disabled");
            // }
        });
    }

    function renderToTable() {
        var data = dataBankAccount;
        if (data.length == 0) {
            $("#lists-bank-table tbody").html(`
                <tr>
                    <td colspan="8">Belum ada data, tekan + untuk menambahkan.</td>
                </tr>
            `)
        } else {
            $("#lists-bank-table tbody").html(`${data.map(function(item, position) {
                var status = Number(item.is_visible);
                if (status == "1") status = "<span class='badge badge-primary'>Aktif</span>";
                else if (status == "2") status = "<span class='badge badge-warning'>Pending/Review</span>";
                else if (status == "0") status = "<span class='badge badge-danger'>Tidak Aktif</span>";

                return `
                    <tr>
                        <td scope="col">${(position + 1)}</td>
                        <td scope="col">${item.bank_name}</td>
                        <td scope="col">${item.account_number}</td>
                        <td scope="col">${item.account_name}</td>
                        <td scope="col">${status}</td>
                        <td scope="col">${(item.created_at != null) ? item.created_at : '-'}</td>
                        <td scope="col">
                            <center>
                            <button class="btn btn-warning"
                            data-toggle="modal" data-target="#modal-update-bank"
                            onclick="setUpdateBankAccount(${position})"><i class="icon-pencil"></i></button>
                            </center>
                        </td>
                        <td scope="col">
                            <center>
                            <button class="btn btn-danger"
                            data-toggle="modal" data-target="#modal-delete-bank"
                            onclick="setDeleteBankAccount(${position})"><i class="icon-trash"></i></button>
                            </center>
                        </td>
                    </tr>
                `;
            }).join('')}`);
        }
    }

    function setUpdateBankAccount(position) {
        selectedTagihanTopUp = dataBankAccount[position]

        $("#update-bank_choice").val(selectedTagihanTopUp.id_m_banks)
        $("#update-account_number").val(selectedTagihanTopUp.account_number)
        $("#update-account_name").val(selectedTagihanTopUp.account_name)
        $("#update-bank_status").val(Number(selectedTagihanTopUp.is_visible))
    }

    function setDeleteBankAccount(position) {
        selectedTagihanTopUp = dataBankAccount[position]
        $("#delete-bank_name").text(selectedTagihanTopUp.account_number);
    }

    function updateBankAccount() {
        var id = selectedTagihanTopUp.id;
        var raw = req.raw({
            id_m_banks: $("#update-bank_choice").val(),
            account_number: $("#update-account_number").val(),
            account_name: $("#update-account_name").val(),
            is_visible: $("#update-bank_status").val()
        })

        $("#btn-update-bank").hide();
        $("#update-bank-loader").html(`
        <div class="loader-item"></div>
        `);

        var formData = new FormData();
        formData.append("raw", raw);

        $.ajax({
            url: base_url.value + "/dashboard/g/settings/update_bank_account/" + id,
            data: formData,
            type: "POST",
            contentType: false,
            processData: false,
            success: function(response) {
                response = req.data(response)
                if (response.code == 200) {
                    toastr.success("Berhasil memperbaharui akun bank");
                    setTimeout(() => {
                                location.reload();
                            }, 1000);;
                } else {
                    toastr.error(response.message);
                    $("#btn-update-bank").show();
                    $("#update-bank-loader").html(``);
                }
                $("#modal-update-bank").modal("hide");
            }
            // ,
            // error: function(response) {
            //     console.log("ERROR", response);
            //     $("#btn-login").removeAttr("disabled");
            // }
        });
    }

    function deleteBankAccount() {
        var id = selectedTagihanTopUp.id;


        $("#btn-delete-bank").hide();
        $("#delete-bank-loader").html(`
        <div class="loader-item"></div>
        `);

        $.ajax({
            url: base_url.value + "/dashboard/g/settings/delete_bank_account/" + id,
            data: null,
            type: "GET",
            contentType: false,
            processData: false,
            success: function(response) {
                response = req.data(response)
                if (response.code == 200) {
                    toastr.success("Berhasil menghapus akun bank");
                    setTimeout(() => {
                                location.reload();
                            }, 1000);;
                } else {
                    toastr.error(response.message);
                    $("#btn-delete-bank").show();
                    $("#delete-bank-loader").html(``);
                }
                $("#modal-delete-bank").modal("hide");
            }
            // ,
            // error: function(response) {
            //     console.log("ERROR", response);
            //     $("#btn-login").removeAttr("disabled");
            // }
        });
    }
</script>