<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SidebarMerchant extends CI_Model {
    // Get Menus
    public function getMenus() {
        return array(
            $this->getDashboard(),
            $this->getManageProduct(),
            $this->getManageTransaction(),
            $this->getManageWallet(),
            $this->getSettings()
        );
    }

    // Get Sidebar Dashboard
    private function getDashboard() {
        return array(
            "label" => "Dasbor",
            "icon" => "icon-home",
            "items" => array(
                array(
                    "label" => "Beranda",
                    "icon" => "icon-home",
                    "link" => base_url("index.php/dashboard/m/dashboard/index")
                ),
                array(
                    "label" => "Catatan Aktifitas",
                    "icon" => "icon-directions",
                    "link" => base_url("index.php/dashboard/m/dashboard/logActivity")
                ),
                array(
                    "label" => "Obrolan Toko",
                    "icon" => "icon-bubble",
                    "link" => base_url("index.php/dashboard/m/dashboard/chatMerchant")
                )
            )
        );
    }

    // Get Sidebar Manage Products
    private function getManageProduct() {
        return array(
            "label" => "Produk",
            "icon" => "icon-basket-loaded",
            "items" => array(
                array(
                    "label" => "Produk Saya",
                    "icon" => "icon-present",
                    "link" => base_url("index.php/dashboard/m/managementProducts/index")
                ),
            )
        );
    }

    // Get Sidebar Manage Transaction
    private function getManageTransaction() {
        return array(
            "label" => "Transaksi",
            "icon" => "icon-directions",
            "items" => array(
                array(
                    "label" => "Daftar Transaksi",
                    "icon" => "icon-directions",
                    "link" => base_url("index.php/dashboard/m/managementTransactions/index")
                ),
            )
        );
    }

    // Get Sidebar Manage Wallets
    private function getManageWallet() {
        return array(
            "label" => "Dompet",
            "icon" => "icon-wallet",
            "items" => array(
                array(
                    "label" => "Dompet Saya",
                    "icon" => "icon-people",
                    "link" => base_url("index.php/dashboard/m/managementWallets/index")
                ),
                array(
                    "label" => "Isi Ulang Saldo",
                    "icon" => "icon-cloud-upload",
                    "link" => base_url("index.php/dashboard/m/managementWallets/listsTopUp")
                ),
                array(
                    "label" => "Tarik Saldo",
                    "icon" => "icon-credit-card",
                    "link" => base_url("index.php/dashboard/m/managementWallets/listsWithdraw")
                )
            )
        );
    }

    // Get Sidebar Settings
    private function getSettings() {
        return array(
            "label" => "Pengaturan",
            "icon" => "icon-settings",
            "items" => array(
                array(
                    "label" => "Toko Saya",
                    "icon" => "icon-settings",
                    "link" => base_url("index.php/dashboard/m/settings/index"),
                    "is_active" => false
                ),
                array(
                    "label" => "Profil",
                    "icon" => "icon-settings",
                    "link" => base_url("index.php/dashboard/m/settings/profile")
                ),
                array(
                    "label" => "Akun Bank",
                    "icon" => "icon-paypal",
                    "link" => base_url("index.php/dashboard/m/settings/bank"),
                    "is_active" => false
                )
            )
        );
    }
}