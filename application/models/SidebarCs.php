<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SidebarCs extends CI_Model {
    // Get Menus
    public function getMenus() {
        return array(
            $this->getDashboard(),
            $this->getSettings()
        );
    }

    // Get Sidebar Dashboard
    private function getDashboard() {
        return array(
            "label" => "Dasbor",
            "icon" => "icon-home",
            "items" => array(
                array(
                    "label" => "Daftar Chat",
                    "icon" => "icon-bubble",
                    "link" => base_url("index.php/dashboard/cs/dashboard"),
                    "is_active" => false
                )
            )
        );
    }

    // Get Sidebar Settings
    private function getSettings() {
        return array(
            "label" => "Pengaturan",
            "icon" => "icon-settings",
            "items" => array(
                array(
                    "label" => "Profil",
                    "icon" => "icon-settings",
                    "link" => base_url("index.php/dashboard/cs/settings"),
                    "is_active" => false
                ),
            )
        );
    }
}