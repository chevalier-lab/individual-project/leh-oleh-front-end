<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sidebar extends CI_Model {
    // Get Menus
    public function getMenus() {
        return array(
            $this->getDashboard(),
            $this->getManageUser(),
            $this->getManageProduct(),
            $this->getManageTransaction(),
            $this->getManageWallet(),
            $this->getManagePagesPosts(),
            $this->getMasterData(),
            $this->getSettings()
        );
    }

    // Get Sidebar Dashboard
    private function getDashboard() {
        return array(
            "label" => "Dasbor",
            "icon" => "icon-home",
            "items" => array(
                array(
                    "label" => "Beranda",
                    "icon" => "icon-home",
                    "link" => base_url("index.php/dashboard/a/dashboard/index"),
                    "is_active" => false
                ),
                array(
                    "label" => "Catatan Aktivitas",
                    "icon" => "icon-directions",
                    "link" => base_url("index.php/dashboard/a/dashboard/logActivity"),
                    "is_active" => false
                ),
                array(
                    "label" => "Layanan Pelanggan",
                    "icon" => "icon-bubble",
                    "link" => base_url("index.php/dashboard/a/dashboard/chatCS"),
                    "is_active" => false
                )
            )
        );
    }

    // Get Sidebar Manage User
    private function getManageUser() {
        return array(
            "label" => "Pengguna",
            "icon" => "icon-people",
            "items" => array(
                array(
                    "label" => "Daftar Pengguna",
                    "icon" => "icon-list",
                    "link" => base_url("index.php/dashboard/a/managementUsers/index"),
                    "is_active" => false
                ),
                array(
                    "label" => "Pengguna Terblokir",
                    "icon" => "icon-user-unfollow",
                    "link" => base_url("index.php/dashboard/a/managementUsers/listsUsersBanned"),
                    "is_active" => false
                ),
                array(
                    "label" => "Permintaan Toko",
                    "icon" => "icon-envelope-letter",
                    "link" => base_url("index.php/dashboard/a/managementUsers/listsJoinMerchant"),
                    "is_active" => false
                )
            )
        );
    }

    // Get Sidebar Manage Products
    private function getManageProduct() {
        return array(
            "label" => "Produk",
            "icon" => "icon-basket-loaded",
            "items" => array(
                array(
                    "label" => "Daftar Toko",
                    "icon" => "icon-people",
                    "link" => base_url("index.php/dashboard/a/managementProducts/listsMerchant"),
                    "is_active" => false
                ),
                array(
                    "label" => "Daftar Produk",
                    "icon" => "icon-present",
                    "link" => base_url("index.php/dashboard/a/managementProducts/index"),
                    "is_active" => false
                ),
                array(
                    "label" => "Daftar Produk Baru",
                    "icon" => "icon-present",
                    "link" => base_url("index.php/dashboard/a/managementProducts/listsNewProducts"),
                    "is_active" => false
                )
            )
        );
    }

    // Get Sidebar Manage Transaction
    private function getManageTransaction() {
        return array(
            "label" => "Transaksi",
            "icon" => "icon-directions",
            "items" => array(
                array(
                    "label" => "Daftar Transaksi",
                    "icon" => "icon-directions",
                    "link" => base_url("index.php/dashboard/a/managementTransactions/index"),
                    "is_active" => false
                ),
                array(
                    "label" => "Daftar Ulasan",
                    "icon" => "icon-trophy",
                    "link" => base_url("index.php/dashboard/a/managementTransactions/listsReview"),
                    "is_active" => false
                ),
                array(
                    "label" => "Daftar Komplain",
                    "icon" => "icon-speech",
                    "link" => base_url("index.php/dashboard/a/managementTransactions/listsComplain"),
                    "is_active" => false
                ),
                array(
                    "label" => "Daftar Pembatalan",
                    "icon" => "icon-share-alt",
                    "link" => base_url("index.php/dashboard/a/managementTransactions/listsRefund"),
                    "is_active" => false
                )
            )
        );
    }

    // Get Sidebar Manage Wallets
    private function getManageWallet() {
        return array(
            "label" => "Dompet",
            "icon" => "icon-wallet",
            "items" => array(
                array(
                    "label" => "Daftar Dompet Pengguna",
                    "icon" => "icon-people",
                    "link" => base_url("index.php/dashboard/a/managementWallets/index"),
                    "is_active" => false
                ),
                array(
                    "label" => "Daftar Isi Ulang",
                    "icon" => "icon-cloud-upload",
                    "link" => base_url("index.php/dashboard/a/managementWallets/listsTopUp"),
                    "is_active" => false
                ),
                array(
                    "label" => "Daftar Tarik Saldo",
                    "icon" => "icon-credit-card",
                    "link" => base_url("index.php/dashboard/a/managementWallets/listsWithdraw"),
                    "is_active" => false
                )
            )
        );
    }

    // Get Sidebar Manage Pages And Posts
    private function getManagePagesPosts() {
        return array(
            "label" => "Halaman dan Pos",
            "icon" => "icon-layers",
            "items" => array(
                array(
                    "label" => "Daftar Halaman",
                    "icon" => "icon-docs",
                    "link" => base_url("index.php/dashboard/a/managementPages/index"),
                    "is_active" => false
                ),
                array(
                    "label" => "Daftar Pos",
                    "icon" => "icon-book-open",
                    "link" => base_url("index.php/dashboard/a/managementPosts/index"),
                    "is_active" => false
                )
            )
        );
    }

    // Get Sidebar Master Data
    private function getMasterData() {
        return array(
            "label" => "Data Master",
            "icon" => "icon-drawer",
            "items" => array(
                array(
                    "label" => "Bank",
                    "icon" => "icon-paypal",
                    "link" => base_url("index.php/dashboard/a/masterData/index"),
                    "is_active" => false
                ),
                array(
                    "label" => "Menu",
                    "icon" => "icon-grid",
                    "link" => base_url("index.php/dashboard/a/masterData/menus"),
                    "is_active" => false
                ),
                array(
                    "label" => "Kategori",
                    "icon" => "icon-equalizer",
                    "link" => base_url("index.php/dashboard/a/masterData/categories"),
                    "is_active" => false
                ),
                array(
                    "label" => "Label",
                    "icon" => "icon-tag",
                    "link" => base_url("index.php/dashboard/a/masterData/tags"),
                    "is_active" => false
                ),
                array(
                    "label" => "Ikon",
                    "icon" => "icon-game-controller",
                    "link" => base_url("index.php/dashboard/a/masterData/icons"),
                    "is_active" => false
                ),
                array(
                    "label" => "Media",
                    "icon" => "icon-picture",
                    "link" => base_url("index.php/dashboard/a/masterData/medias"),
                    "is_active" => false
                ),
                array(
                    "label" => "Dompet",
                    "icon" => "icon-wallet",
                    "link" => base_url("index.php/dashboard/a/masterData/wallets"),
                    "is_active" => false
                ),
                array(
                    "label" => "Jasa Pengiriman",
                    "icon" => "icon-loop",
                    "link" => base_url("index.php/dashboard/a/masterData/deliveryService"),
                    "is_active" => false
                ),
                array(
                    "label" => "Masukan",
                    "icon" => "icon-action-redo",
                    "link" => base_url("index.php/dashboard/a/masterData/feedback"),
                    "is_active" => false
                ),
                array(
                    "label" => "FAQ",
                    "icon" => "icon-question",
                    "link" => base_url("index.php/dashboard/a/masterData/faq"),
                    "is_active" => false
                )
            )
        );
    }

    // Get Sidebar Settings
    private function getSettings() {
        return array(
            "label" => "Pengaturan",
            "icon" => "icon-settings",
            "items" => array(
                array(
                    "label" => "Umum",
                    "icon" => "icon-settings",
                    "link" => base_url("index.php/dashboard/a/settings/index"),
                    "is_active" => false
                ),
                array(
                    "label" => "Cabang",
                    "icon" => "icon-settings",
                    "link" => base_url("index.php/dashboard/a/settings/branch"),
                    "is_active" => false
                ),
                array(
                    "label" => "Media Sosial",
                    "icon" => "icon-settings",
                    "link" => base_url("index.php/dashboard/a/settings/socialMedia"),
                    "is_active" => false
                ),
                array(
                    "label" => "Profil",
                    "icon" => "icon-settings",
                    "link" => base_url("index.php/dashboard/a/settings/profile"),
                    "is_active" => false
                )
            )
        );
    }
}