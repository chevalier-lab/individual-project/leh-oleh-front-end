<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Navbar extends CI_Model {

    // Get Menus
    public function getMenus() {
        return array(
            "notification" => $this->getNotification(),
            "user" => $this->getUser()
        );
    }

    // Get User Menu
    private function getUser() {
        return array(
            "label" => "Profile",
            "face" => base_url('assets/dist/images/author.jpg'),
            "items" => array(
                array(
                    "label" => "Ubah Profil",
                    "icon" => "icon-pencil",
                    "link" => ""
                ),
                array(
                    "label" => "Lihat Profil",
                    "icon" => "icon-user",
                    "link" => ""
                ),
                array(
                    "label" => "Keluar",
                    "icon" => "icon-logout",
                    "link" => base_url("index.php/general/auth/do_logout")
                )
            )
        );  
    }

    // Get Notification Menu
    private function getNotification() {
        return array(
            "label" => "Notification",
            "icon" => "icon-bell",
            "items" => array()
        );
    }
}