<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SidebarGuest extends CI_Model {
    // Get Menus
    public function getMenus() {
        return array(
            $this->getDashboard(),
            $this->getManageWallet(),
            $this->getSettings()
        );
    }

    // Get Sidebar Dashboard
    private function getDashboard() {
        return array(
            "label" => "Dasbor",
            "icon" => "icon-home",
            "items" => array(
                array(
                    "label" => "Beranda",
                    "icon" => "icon-home",
                    "link" => base_url("index.php/dashboard/g/dashboard/index"),
                    "is_active" => false
                ),
                array(
                    "label" => "Daftar Sebagai Toko",
                    "icon" => "icon-directions",
                    "link" => base_url("index.php/dashboard/g/dashboard/requestMerchant"),
                    "is_active" => false
                )
            )
        );
    }

    // Get Sidebar Manage Wallets
    private function getManageWallet() {
        return array(
            "label" => "Dompet",
            "icon" => "icon-wallet",
            "items" => array(
                array(
                    "label" => "Dompet Saya",
                    "icon" => "icon-people",
                    "link" => base_url("index.php/dashboard/g/managementWallets/index"),
                    "is_active" => false
                ),
                array(
                    "label" => "Isi Ulang Saldo",
                    "icon" => "icon-cloud-upload",
                    "link" => base_url("index.php/dashboard/g/managementWallets/listsTopUp"),
                    "is_active" => false
                ),
                array(
                    "label" => "Tarik Saldo",
                    "icon" => "icon-credit-card",
                    "link" => base_url("index.php/dashboard/g/managementWallets/listsWithdraw"),
                    "is_active" => false
                )
            )
        );
    }

    // Get Sidebar Settings
    private function getSettings() {
        return array(
            "label" => "Pengaturan",
            "icon" => "icon-settings",
            "items" => array(
                array(
                    "label" => "Profil",
                    "icon" => "icon-settings",
                    "link" => base_url("index.php/dashboard/g/settings"),
                    "is_active" => false
                ),

                array(
                    "label" => "Akun Bank",
                    "icon" => "icon-paypal",
                    "link" => base_url("index.php/dashboard/g/settings/bank"),
                    "is_active" => false
                )
            )
        );
    }
}