<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ChatMerchant extends CI_Controller {

    // Public Variable
    public $session, $custom_curl, $fileUpload;
    public $meta, $error, $auth;
	
	public function __construct() {
        parent::__construct();

        $this->meta = array(
            "title" => "",
            "description" => "",
            "robots" => ""
        );

        $this->error = array(
            "title" => "Something went wrong",
            "content" => "",
            "details" => array()
        );

        // Load Helper
        $this->session = new Session_helper();
        $this->custom_curl = new Mycurl_helper("");
        $this->fileUpload = new Upload_file_helper(
            array(
                "file_type" => array(
                    "png",
                    "jpg",
                    "jpeg"
                ),
                "max_size"  => 200000000
            )
        );

        // Check Auth
        $this->checkAuth();
	}

    // ==========================================================
    // AUTH PROCESS
    // ==========================================================

    private function checkAuth()
    {
        if ($this->session->check_session("auth")) {
            $this->auth = $this->session->get_session("auth");
        } else {
            die(json_encode(array("code" => 401, "message"=> "Unauthorize")));
        }
    }

    // ==========================================================
    // BACK-END PROCESS
    // ==========================================================


    // Load Chat Room CS
    public function list_chat_room_merchant($page)
    {

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->createCurl(API_URI .
            "chat/merchant/room?page=$page&order-by=id&order-direction=DESC");

        print_r($this->custom_curl->__tostring());

    }

    // Merchant Chat Detail
    public function detail_chat_room($id)
    {
        $page = $this->input->post_get("page") ?: "0";

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->createCurl(API_URI .
            "chat/merchant/room/$id?page=$page&order-by=id&order-direction=ASC");

        print_r($this->custom_curl->__tostring());

    }

    // Load Chat Room Guest
    public function list_chat_room_guest($page)
    {

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->createCurl(API_URI .
            "chat/guest/room?page=$page&order-by=id&order-direction=DESC");

        print_r($this->custom_curl->__tostring());

    }

    // Check Room Guest
    public function check_chat_room($id_merchant)
    {

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->createCurl(API_URI .
            "chat/guest/room/$id_merchant");

        print_r($this->custom_curl->__tostring());

    }

    // Load Chat Chat Send Message
    public function create_chat_room()
    {

        $raw = $this->input->post_get("raw", TRUE);

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token,
            "Content-Type: application/json"
        ));

        $this->custom_curl->setPost($raw);

        $this->custom_curl->createCurl(API_URI .
            "chat/merchant/room");

        print_r($this->custom_curl->__tostring());

    }

    // Load Chat Chat Send Message
    public function send_message($id)
    {

        $raw = $this->input->post_get("raw", TRUE);

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token,
            "Content-Type: application/json"
        ));

        $this->custom_curl->setPost($raw);

        $this->custom_curl->createCurl(API_URI .
            "chat/merchant/room/$id/send");

        print_r($this->custom_curl->__tostring());

    }

    // Update Room
    public function update_status_room($id)
    {
        $raw = $this->input->post_get("raw", TRUE) ?: "";

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token,
            "Content-Type: application/json"
        ));

        $this->custom_curl->setPut($raw);

        $this->custom_curl->createCurl(API_URI .
            "chat/merchant/room/$id");

        print_r($this->custom_curl->__tostring());

    }
}
