<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faq extends CI_Controller {

    // Public Variable
    public $session, $custom_curl, $fileUpload;
    public $meta, $error, $auth;
	
	public function __construct() {
        parent::__construct();

        $this->meta = array(
            "title" => "",
            "description" => "",
            "robots" => ""
        );

        $this->error = array(
            "title" => "Something went wrong",
            "content" => "",
            "details" => array()
        );

        // Load Helper
        $this->session = new Session_helper();
        $this->custom_curl = new Mycurl_helper("");
        $this->fileUpload = new Upload_file_helper(
            array(
                "file_type" => array(
                    "png",
                    "jpg",
                    "jpeg"
                ),
                "max_size"  => 200000000
            )
        );

        // Check Auth
        $this->checkAuth();
	}

    // ==========================================================
    // AUTH PROCESS
    // ==========================================================

    private function checkAuth()
    {
        if ($this->session->check_session("auth")) {
            $this->auth = $this->session->get_session("auth");
        }
    }

    // ==========================================================
    // BACK-END PROCESS
    // ==========================================================


    // Load Chat Room CS
    public function filter()
    {

        $page = $this->input->post_get("page", TRUE) ?: 0;
        $search = $this->input->post_get("search", TRUE) ?: "";
        $order_by = $this->input->post_get("order-by", TRUE) ?: "id";
        $order_direction = $this->input->post_get("order-direction", TRUE) ?: "ASC";

        $this->custom_curl->createCurl(API_URI .
            "admin/manage/faq?page=$page&search=$search&order-by=$order_by&order-direction=$order_direction");

        print_r($this->custom_curl->__tostring());

    }
}
