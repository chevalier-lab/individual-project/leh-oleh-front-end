<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Daerah extends CI_Controller {

    // Public Variable
    public $session, $custom_curl, $fileUpload;
    public $meta, $error, $auth;
	
	public function __construct() {
        parent::__construct();

        $this->meta = array(
            "title" => "",
            "description" => "",
            "robots" => ""
        );

        $this->error = array(
            "title" => "Something went wrong",
            "content" => "",
            "details" => array()
        );

        // Load Helper
        $this->session = new Session_helper();
        $this->custom_curl = new Mycurl_helper("");
        $this->fileUpload = new Upload_file_helper(
            array(
                "file_type" => array(
                    "png",
                    "jpg",
                    "jpeg"
                ),
                "max_size"  => 200000000
            )
        );
	}

    // ==========================================================
    // BACK-END PROCESS
    // ==========================================================


    // Load Provinsi
    public function load_province()
    {

        $this->custom_curl->createCurl(API_URI_DAERAH .
            "provinsi");

        print_r($this->custom_curl->__tostring());

    }

    // Load City
    public function load_city($id)
    {

        $this->custom_curl->createCurl(API_URI_DAERAH .
            "kabupaten/$id");

        print_r($this->custom_curl->__tostring());

    }

    // Load Kecamatan
    public function load_district($id)
    {

        $this->custom_curl->createCurl(API_URI_DAERAH .
            "kecamatan/$id");

        print_r($this->custom_curl->__tostring());

    }

    // Load Kelurahan
    public function load_village($id)
    {

        $this->custom_curl->createCurl(API_URI_DAERAH .
            "kelurahan/$id");

        print_r($this->custom_curl->__tostring());

    }
}
