<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chats extends CI_Controller {

    // Public Variable
    public $session, $custom_curl, $fileUpload;
    public $meta, $error, $auth;
	
	public function __construct() {
        parent::__construct();

        $this->meta = array(
            "title" => "",
            "description" => "",
            "robots" => ""
        );

        $this->error = array(
            "title" => "Something went wrong",
            "content" => "",
            "details" => array()
        );

        // Load Helper
        $this->session = new Session_helper();
        $this->custom_curl = new Mycurl_helper("");
        $this->fileUpload = new Upload_file_helper(
            array(
                "file_type" => array(
                    "png",
                    "jpg",
                    "jpeg"
                ),
                "max_size"  => 200000000
            )
        );

        // Check Auth
        $this->checkAuth();
	}

    // ==========================================================
    // AUTH PROCESS
    // ==========================================================

    private function checkAuth()
    {
        if ($this->session->check_session("auth")) {
            $this->auth = $this->session->get_session("auth");
        } else {
            die(json_encode(array("code" => 401, "message"=> "Unauthorize")));
        }
    }

    // ==========================================================
    // BACK-END PROCESS
    // ==========================================================


    // Load Chat Room CS
    public function list_chat_room_cs()
    {

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->createCurl(API_URI .
            "chat/room/cs");

        print_r($this->custom_curl->__tostring());

    }

    // Load Chat Room Guest
    public function list_chat_room_guest()
    {

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->createCurl(API_URI .
            "chat/room/guest");

        print_r($this->custom_curl->__tostring());

    }

    // Load Chat By ID
    public function list_chat_room_by_id($id)
    {

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->createCurl(API_URI .
            "chat/room/$id");

        print_r($this->custom_curl->__tostring());

    }

    // Load Chat Check User Type
    public function list_chat_room_check()
    {

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->createCurl(API_URI .
            "chat/room/check");

        print_r($this->custom_curl->__tostring());

    }

    // Load Chat Chat Detail
    public function list_chat_detail($id)
    {

        // $raw = $this->input->post("raw", TRUE) ?: "";
        // if ($raw == "") {
        //     die(json_encode(array("code" => 401, "message"=> "ID Room Need")));
        // }

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        // $this->custom_curl->setPost($raw);

        $this->custom_curl->createCurl(API_URI .
            "chat/room/detail/$id");

        print_r($this->custom_curl->__tostring());

    }

    // Load Chat Chat Send Message
    public function send_message()
    {

        $raw = $this->input->post_get("raw", TRUE);

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token,
            "Content-Type: application/json"
        ));

        $this->custom_curl->setPost($raw);

        $this->custom_curl->createCurl(API_URI .
            "chat/room/detail");

        print_r($this->custom_curl->__tostring());

    }

    // Load Detail Room
    public function room_detail_cs($id)
    {

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->createCurl(API_URI .
            "chat/room/cs/$id");

        print_r($this->custom_curl->__tostring());

    }

    // Update Room
    public function update_status_room()
    {
        $raw = $this->input->post_get("raw", TRUE) ?: "";

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token,
            "Content-Type: application/json"
        ));

        $this->custom_curl->setPut($raw);

        $this->custom_curl->createCurl(API_URI .
            "room");

        print_r($this->custom_curl->__tostring());

    }
}
