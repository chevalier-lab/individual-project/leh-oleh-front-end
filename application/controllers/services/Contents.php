<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contents extends CI_Controller {

    // Public Variable
    public $session, $custom_curl, $fileUpload;
    public $meta, $error, $auth;
	
	public function __construct() {
        parent::__construct();

        $this->meta = array(
            "title" => "",
            "description" => "",
            "robots" => ""
        );

        $this->error = array(
            "title" => "Something went wrong",
            "content" => "",
            "details" => array()
        );

        // Load Helper
        $this->session = new Session_helper();
        $this->custom_curl = new Mycurl_helper("");
        $this->fileUpload = new Upload_file_helper(
            array(
                "file_type" => array(
                    "png",
                    "jpg",
                    "jpeg"
                ),
                "max_size"  => 200000000
            )
        );
	}

    // ==========================================================
    // BACK-END PROCESS
    // ==========================================================


    // Load Sosmed
    public function load_sosmed()
    {
        // $this->custom_curl->setHeader(array(
        //     "Authorization: " . $this->auth->token
        // ));
        $this->custom_curl->createCurl(API_URI .
            "guest/general/sosmed");

        print_r($this->custom_curl->__tostring());
    }

    // Load Banks
    public function load_banks()
    {
        // $this->custom_curl->setHeader(array(
        //     "Authorization: " . $this->auth->token
        // ));
        $this->custom_curl->createCurl(API_URI .
            "master/banks/guest");

        print_r($this->custom_curl->__tostring());
    }

    // Load Setting
    public function load_setting()
    {
        $this->custom_curl->createCurl(API_URI .
            "admin/setting/general/guest");

        print_r($this->custom_curl->__tostring());
    }
}
