<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notification extends CI_Controller {

    // Public Variable
    public $session, $custom_curl, $fileUpload;
    public $meta, $error, $auth;
	
	public function __construct() {
        parent::__construct();

        $this->meta = array(
            "title" => "",
            "description" => "",
            "robots" => ""
        );

        $this->error = array(
            "title" => "Something went wrong",
            "content" => "",
            "details" => array()
        );

        // Load Helper
        $this->session = new Session_helper();
        $this->custom_curl = new Mycurl_helper("");

        // Check Auth
        $this->checkAuth();
	}

    // ==========================================================
    // AUTH PROCESS
    // ==========================================================

    private function checkAuth()
    {
        if ($this->session->check_session("auth")) {
            $this->auth = $this->session->get_session("auth");
        } else {
            die(json_encode(array("code" => 401, "message"=> "Unauthorize")));
        }
    }

    // ==========================================================
    // BACK-END PROCESS
    // ==========================================================


    // Load Notification
    public function list()
    {

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->createCurl(API_URI .
            "notifications?page=0&order-by=id&order-direction=DESC&is-read=0");

        print_r($this->custom_curl->__tostring());

    }

    // Update Room
    public function update_is_read($id)
    {

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token,
            "Content-Type: application/json"
        ));

        $this->custom_curl->setPut(json_encode(array()));

        $this->custom_curl->createCurl(API_URI .
            "notifications/$id");

        print_r($this->custom_curl->__tostring());

    }
}
