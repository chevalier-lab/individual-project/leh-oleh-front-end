<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rates extends CI_Controller {

    // Public Variable
    public $session, $custom_curl, $fileUpload;
    public $meta, $error, $auth;
	
	public function __construct() {
        parent::__construct();

        $this->meta = array(
            "title" => "",
            "description" => "",
            "robots" => ""
        );

        $this->error = array(
            "title" => "Something went wrong",
            "content" => "",
            "details" => array()
        );

        // Load Helper
        $this->session = new Session_helper();
        $this->custom_curl = new Mycurl_helper("");
        $this->fileUpload = new Upload_file_helper(
            array(
                "file_type" => array(
                    "png",
                    "jpg",
                    "jpeg"
                ),
                "max_size"  => 200000000
            )
        );
	}

    // ==========================================================
    // BACK-END PROCESS
    // ==========================================================


    // Load Provinsi
    public function load_domestic_rates()
    {

        $query = "?apiKey=" . API_KEY_SHIPPER;
        $o = $this->input->post_get("o", TRUE) ?: "";
        $d = $this->input->post_get("d", TRUE) ?: "";
        $wt = $this->input->post_get("wt", TRUE) ?: "";
        $v = $this->input->post_get("v", TRUE) ?: "";
        $l = $this->input->post_get("l", TRUE) ?: "";
        $w = $this->input->post_get("w", TRUE) ?: "";
        $h = $this->input->post_get("h", TRUE) ?: "";
        $cod = $this->input->post_get("cod", TRUE) ?: "";
        $type = $this->input->post_get("type", TRUE) ?: "";
        $originCoord = $this->input->post_get("originCoord", TRUE) ?: "";
        $destinationCoord = $this->input->post_get("destinationCoord", TRUE) ?: "";
        $order = $this->input->post_get("order", TRUE) ?: "";
        $creatingOrder = $this->input->post_get("creatingOrder", TRUE) ?: "";
        $bestPrices = $this->input->post_get("bestPrices", TRUE) ?: "";
        $serviceType = $this->input->post_get("serviceType", TRUE) ?: "";

        if (!empty($o)) $query .= "&o=" . $o;
        if (!empty($d)) $query .= "&d=" . $d;
        if (!empty($wt)) $query .= "&wt=" . $wt;
        if (!empty($v)) $query .= "&v=" . $v;
        if (!empty($l)) $query .= "&l=" . $l;
        if (!empty($w)) $query .= "&w=" . $w;
        if (!empty($h)) $query .= "&h=" . $h;
        if (!empty($cod)) $query .= "&cod=" . $cod;
        if (!empty($type)) $query .= "&type=" . $type;
        if (!empty($originCoord)) $query .= "&originCoord=" . $originCoord;
        if (!empty($destinationCoord)) $query .= "&destinationCoord=" . $destinationCoord;
        if (!empty($order)) $query .= "&order=" . $order;
        if (!empty($creatingOrder)) $query .= "&creatingOrder=" . $creatingOrder;
        if (!empty($bestPrices)) $query .= "&bestPrices=" . $bestPrices;
        if (!empty($serviceType)) $query .= "&serviceType=" . $serviceType;

        $this->custom_curl->setHeader(array(
            "User-Agent: Shipper/"
        ));

        $this->custom_curl->createCurl(API_SHIPPER . 
        "/public/v1/domesticRates" . $query);

        print_r($this->custom_curl->__tostring());

    }
}
