<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PickupRequest extends CI_Controller {

    // Public Variable
    public $session, $custom_curl, $fileUpload;
    public $meta, $error, $auth;
	
	public function __construct() {
        parent::__construct();

        $this->meta = array(
            "title" => "",
            "description" => "",
            "robots" => ""
        );

        $this->error = array(
            "title" => "Something went wrong",
            "content" => "",
            "details" => array()
        );

        // Load Helper
        $this->session = new Session_helper();
        $this->custom_curl = new Mycurl_helper("");
        $this->fileUpload = new Upload_file_helper(
            array(
                "file_type" => array(
                    "png",
                    "jpg",
                    "jpeg"
                ),
                "max_size"  => 200000000
            )
        );
	}

    // ==========================================================
    // BACK-END PROCESS
    // ==========================================================


    // Load Provinsi
    public function pickup_request()
    {

        $req = array();
        $datePickup = $this->input->post_get("datePickup", TRUE) ?: "";
        $orderIds = $this->input->post_get("orderIds", TRUE) ?: "";
        $agentId = $this->input->post_get("agentId", TRUE) ?: "";

        if (!empty($datePickup)) $req['datePickup'] = $datePickup;
        if (!empty($orderIds)) $req['orderIds'] = $orderIds;
        if (!empty($agentId)) $req['agentId'] = $agentId;

        $this->custom_curl->setHeader(array(
            "Content-Type: application/x-www-form-urlencoded",
            "User-Agent: Shipper/"
        ));

        $this->custom_curl->setPost($req);

        $this->custom_curl->createCurl(API_SHIPPER . 
        "/public/v1/pickup?apiKey=" . API_KEY_SHIPPER);

        print_r($this->custom_curl->__tostring());

    }

    // Load Provinsi
    public function cancel_pickup_request()
    {

        $req = array();
        $orderIds = $this->input->post_get("orderIds", TRUE) ?: "";

        if (!empty($orderIds)) $req['orderIds'] = $orderIds;

        $this->custom_curl->setHeader(array(
            "Content-Type: application/x-www-form-urlencoded",
            "User-Agent: Shipper/"
        ));

        $this->custom_curl->setPut($req);

        $this->custom_curl->createCurl(API_SHIPPER . 
        "/public/v1/pickup/cancel?apiKey=" . API_KEY_SHIPPER);

        print_r($this->custom_curl->__tostring());

    }

    // Load Provinsi
    public function load_agents($id)
    {

        $this->custom_curl->setHeader(array(
            "User-Agent: Shipper/"
        ));
        
        $this->custom_curl->createCurl(API_SHIPPER . 
        "/public/v1/agents?apiKey=" . API_KEY_SHIPPER . "&suburbId=" . $id);

        print_r($this->custom_curl->__tostring());

    }
}
