<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Locations extends CI_Controller {

    // Public Variable
    public $session, $custom_curl, $fileUpload;
    public $meta, $error, $auth;
	
	public function __construct() {
        parent::__construct();

        $this->meta = array(
            "title" => "",
            "description" => "",
            "robots" => ""
        );

        $this->error = array(
            "title" => "Something went wrong",
            "content" => "",
            "details" => array()
        );

        // Load Helper
        $this->session = new Session_helper();
        $this->custom_curl = new Mycurl_helper("");
        $this->fileUpload = new Upload_file_helper(
            array(
                "file_type" => array(
                    "png",
                    "jpg",
                    "jpeg"
                ),
                "max_size"  => 200000000
            )
        );
	}

    // ==========================================================
    // BACK-END PROCESS
    // ==========================================================


    // Load Provinsi
    public function load_province()
    {

        $this->custom_curl->setHeader(array(
            "User-Agent: Shipper/"
        ));
        
        $this->custom_curl->createCurl(API_SHIPPER . 
        "/public/v1/provinces?apiKey=" . API_KEY_SHIPPER);

        print_r($this->custom_curl->__tostring());

    }

    // Load City
    public function load_city()
    {

        $id = $this->input->post_get("id", TRUE) ?: "";
        $query = "?apiKey=" . API_KEY_SHIPPER;

        if (empty($id)) $query .= "&origin=all";
        else $query .= "&province=$id";

        $this->custom_curl->setHeader(array(
            "User-Agent: Shipper/"
        ));
        
        $this->custom_curl->createCurl(API_SHIPPER .
            "/public/v1/cities" . $query);

        print_r($this->custom_curl->__tostring());

    }

    // Load Kecamatan
    public function load_suburbs($id)
    {

        $this->custom_curl->setHeader(array(
            "User-Agent: Shipper/"
        ));
        
        $this->custom_curl->createCurl(API_SHIPPER .
            "/public/v1/suburbs?apiKey=".API_KEY_SHIPPER."&city=$id");

        print_r($this->custom_curl->__tostring());

    }

    // Load Kelurahan
    public function load_areas($id)
    {

        $this->custom_curl->setHeader(array(
            "User-Agent: Shipper/"
        ));
        
        $this->custom_curl->createCurl(API_SHIPPER .
            "/public/v1/areas?apiKey=".API_KEY_SHIPPER."&suburb=$id");

        print_r($this->custom_curl->__tostring());

    }

    // Load Kelurahan
    public function load_by_search($search)
    {

        $this->custom_curl->setHeader(array(
            "User-Agent: Shipper/"
        ));
        
        $this->custom_curl->createCurl(API_SHIPPER .
            "/public/v1/details/".$search."?apiKey=" . API_KEY_SHIPPER);

        print_r($this->custom_curl->__tostring());

    }
}
