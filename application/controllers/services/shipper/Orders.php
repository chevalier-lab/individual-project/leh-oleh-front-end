<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orders extends CI_Controller {

    // Public Variable
    public $session, $custom_curl, $fileUpload;
    public $meta, $error, $auth;
	
	public function __construct() {
        parent::__construct();

        $this->meta = array(
            "title" => "",
            "description" => "",
            "robots" => ""
        );

        $this->error = array(
            "title" => "Something went wrong",
            "content" => "",
            "details" => array()
        );

        // Load Helper
        $this->session = new Session_helper();
        $this->custom_curl = new Mycurl_helper("");
        $this->fileUpload = new Upload_file_helper(
            array(
                "file_type" => array(
                    "png",
                    "jpg",
                    "jpeg"
                ),
                "max_size"  => 200000000
            )
        );
	}

    // ==========================================================
    // BACK-END PROCESS
    // ==========================================================


    // Load Provinsi
    public function create_order()
    {

        $raw = $this->input->post_get("raw", TRUE);

        $this->custom_curl->setHeader(array(
            "Content-Type: application/json",
            "User-Agent: Shipper/"
        ));

        $this->custom_curl->setPost($raw);

        $this->custom_curl->createCurl(API_SHIPPER . 
        "/public/v1/orders/domestics?apiKey=" . API_KEY_SHIPPER);

        print_r($this->custom_curl->__tostring());

    }

    // Load Provinsi
    public function activation_order($id)
    {

        $this->custom_curl->setHeader(array(
            "Content-Type: application/x-www-form-urlencoded",
            "User-Agent: Shipper/"
        ));

        $this->custom_curl->setPost(array(
            "active" => 1
        ));

        $this->custom_curl->createCurl(API_SHIPPER . 
        "/public/v1/activations/$id?apiKey=" . API_KEY_SHIPPER);

        print_r($this->custom_curl->__tostring());

    }

    // Load Provinsi
    public function update_order($id)
    {

        $req = array();
        $wt = $this->input->post_get("wt", TRUE) ?: "";
        $l = $this->input->post_get("l", TRUE) ?: "";
        $w = $this->input->post_get("w", TRUE) ?: "";
        $h = $this->input->post_get("h", TRUE) ?: "";

        $this->custom_curl->setHeader(array(
            "Content-Type: application/x-www-form-urlencoded",
            "User-Agent: Shipper/"
        ));

        if (!empty($wt)) $req['wt'] = $wt;
        if (!empty($l)) $req['l'] = $l;
        if (!empty($w)) $req['w'] = $w;
        if (!empty($h)) $req['h'] = $h;

        $this->custom_curl->setPut($req);

        $this->custom_curl->createCurl(API_SHIPPER . 
        "/public/v1/orders/$id?apiKey=" . API_KEY_SHIPPER);

        print_r($this->custom_curl->__tostring());

    }

    // Load Provinsi
    public function cancel_order($id)
    {

        $req = array();

        $this->custom_curl->setHeader(array(
            "Content-Type: application/x-www-form-urlencoded",
            "User-Agent: Shipper/"
        ));

        $this->custom_curl->setPut($req);

        $this->custom_curl->createCurl(API_SHIPPER . 
        "/public/v1/orders/$id/cancel?apiKey=" . API_KEY_SHIPPER);

        print_r($this->custom_curl->__tostring());

    }

    // Load Provinsi
    public function load_order_detail($id)
    {

        $this->custom_curl->setHeader(array(
            "User-Agent: Shipper/"
        ));
        
        $this->custom_curl->createCurl(API_SHIPPER . 
        "/public/v1/orders/$id?apiKey=" . API_KEY_SHIPPER);

        print_r($this->custom_curl->__tostring());

    }

    // Load Provinsi
    public function awb_generate($id)
    {

        $this->custom_curl->setHeader(array(
            "User-Agent: Shipper/"
        ));
        
        $this->custom_curl->createCurl(API_SHIPPER . 
        "/public/v1/awbs/generate?apiKey=" . API_KEY_SHIPPER . "&oid=" . $id);

        print_r($this->custom_curl->__tostring());

    }

    // Load Provinsi
    public function load_tracking_id($id)
    {

        $this->custom_curl->setHeader(array(
            "User-Agent: Shipper/"
        ));
        
        $this->custom_curl->createCurl(API_SHIPPER . 
        "/public/v1/orders?apiKey=" . API_KEY_SHIPPER . "&id=" . $id);

        print_r($this->custom_curl->__tostring());

    }

    // Load Provinsi
    public function load_track_status()
    {

        $this->custom_curl->setHeader(array(
            "User-Agent: Shipper/"
        ));
        
        $this->custom_curl->createCurl(API_SHIPPER . 
        "/public/v1/logistics/status?apiKey=" . API_KEY_SHIPPER);

        print_r($this->custom_curl->__tostring());

    }
}
