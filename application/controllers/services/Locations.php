<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Locations extends CI_Controller {

    // Public Variable
    public $session, $custom_curl, $fileUpload;
    public $meta, $error, $auth;
	
	public function __construct() {
        parent::__construct();

        $this->meta = array(
            "title" => "",
            "description" => "",
            "robots" => ""
        );

        $this->error = array(
            "title" => "Something went wrong",
            "content" => "",
            "details" => array()
        );

        // Load Helper
        $this->session = new Session_helper();
        $this->custom_curl = new Mycurl_helper("");
        $this->fileUpload = new Upload_file_helper(
            array(
                "file_type" => array(
                    "png",
                    "jpg",
                    "jpeg"
                ),
                "max_size"  => 200000000
            )
        );

        // Check Auth
        $this->checkAuth();
	}

    // ==========================================================
    // AUTH PROCESS
    // ==========================================================

    private function checkAuth()
    {
        if ($this->session->check_session("auth")) {
            $this->auth = $this->session->get_session("auth");
        } else {
            die(json_encode(array("code" => 401, "message"=> "Unauthorize")));
        }
    }

    // Load Provinsi
    public function lists()
    {

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->createCurl(API_URI .
            "guest/location");

        print_r($this->custom_curl->__tostring());

    }

    // Update Provinsi
    public function activate($id)
    {

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->setPut(json_encode(array()));
        $this->custom_curl->createCurl(API_URI .
            "guest/location/actived/$id");

        print_r($this->custom_curl->__tostring());

    }
}
