<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	// Public Variable
    public $session, $custom_curl;
	public $meta, $error;
	
	public function __construct() {
        parent::__construct();

        $this->meta = array(
            "title" => "",
            "description" => "",
            "robots" => ""
        );

        $this->error = array(
            "title" => "Something went wrong",
            "content" => "",
            "details" => array()
        );

        // Load Helper
		$this->session = new Session_helper();
	}

    // ==========================================================
    // AUTH PROCESS
    // ==========================================================

    private function checkAuth() {
        redirect(base_url("index.php/anonymous/landing/index"));
    }

	public function index()
	{
		// Check Auth
		$this->checkAuth();
	}
}
