<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Settings extends CI_Controller
{

    // Public Variable
    public $session, $custom_curl;
    public $meta, $error, $customNavbar;

    public function __construct()
    {
        parent::__construct();

        $this->meta = array(
            "title" => "",
            "description" => "",
            "robots" => ""
        );

        $this->error = array(
            "title" => "Something went wrong",
            "content" => "",
            "details" => array()
        );

        // Load Model
        $this->load->model('Navbar');
        $this->load->model('SidebarMerchant');

        // Load Helper
        $this->session = new Session_helper();

        // Load Helper
        $this->session = new Session_helper();
        $this->custom_curl = new Mycurl_helper("");
        $this->fileUpload = new Upload_file_helper(
            array(
                "file_type" => array(
                    "png",
                    "jpg",
                    "jpeg"
                ),
                "max_size"  => 200000000
            )
        );

        $this->customNavbar = $this->Navbar->getMenus();
        $this->customNavbar["user"]["items"][0]["link"] = base_url("index.php/dashboard/m/settings/profile");
        $this->customNavbar["user"]["items"][1]["link"] = base_url("index.php/dashboard/m/settings/profile");

        // Check Auth
        $this->checkAuth();
    }

    // ==========================================================
    // AUTH PROCESS
    // ==========================================================

    private function checkAuth()
    {
        if (!$this->session->check_session("auth")) {
            redirect(base_url("index.php/general/auth/login"));
        } else {
            $this->auth = $this->session->get_session("auth");
            switch ($this->auth->level) {
                case "admin":
                    redirect(base_url("index.php/dashboard/a/dashboard"));
                    break;
                case "toko":
                    break;
                case "guest":
                    redirect(base_url("index.php/dashboard/g/dashboard"));
                    break;
                case "cs":
                    redirect(base_url("index.php/dashboard/cs/dashboard"));
                    break;
            }
        }
    }

    public function index()
    {
        // Check Auth
        // $this->checkAuth();
        // Setup Meta
        $this->meta["title"] = "Toko Leholeh | Umum";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        $sidebar = $this->SidebarMerchant->getMenus();
        $sidebar[4]["items"][0]["is_active"] = true;

        // Load General
        $this->load->view('dashboard-merchant/settings/general', array(
            "meta" => $this->meta,
            "navbar" => $this->customNavbar,
            "sidebar" => $sidebar
        ));
    }

    public function profile()
    {
        // Setup Meta
        $this->meta["title"] = "Toko Leholeh | Profil";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        $sidebar = $this->SidebarMerchant->getMenus();
        $sidebar[4]["items"][1]["is_active"] = true;

        // Load Profile
        $this->load->view('dashboard-merchant/settings/profile', array(
            "meta" => $this->meta,
            "navbar" => $this->customNavbar,
            "sidebar" => $sidebar
        ));
    }

    public function bank()
    {
        // Check Auth
        // $this->checkAuth();
        // Setup Meta
        $this->meta["title"] = "Toko Leholeh | Akun Bank";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";
        
        $sidebar = $this->SidebarMerchant->getMenus();
        $sidebar[4]["items"][2]["is_active"] = true;

        // Load Home
        $this->load->view('dashboard-merchant/settings/bank', array(
            "meta" => $this->meta,
            "navbar" => $this->Navbar->getMenus(),
            "sidebar" => $sidebar
        ));
    }

    // ==========================================================
    // BACK-END PROCESS
    // ==========================================================

    // Load Banks
    public function load_banks()
    {
        $raw = $this->input->post_get("raw") ?: "";
        $page = 0;
        $search = "";
        $order_by = "bank_name";
        $order_direction = "DESC";

        if (!empty($raw)) {
            $raw = json_decode($raw);
            $page = $raw->page;
            $search = $raw->search;
            $order_by = $raw->order_by;
            $order_direction = $raw->order_direction;
        }

        $search = explode(" ", $search);
        $search = implode("%20", $search);

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->createCurl(API_URI .
            "api/admin/master/Banks/getBanks?page=$page&search=$search&order-by=$order_by&order-direction=$order_direction");

        print_r($this->custom_curl->__tostring());
    }

    // Create Bank Account
    public function create_bank_account()
    {
        $raw = $this->input->post("raw") ?: "";
        if (empty($raw)) die();

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->setPost($raw);
        $this->custom_curl->createCurl(API_URI .
            "guest/banks");

        print_r($this->custom_curl->__tostring());
    }

    // Load Banks
    public function load_bank_account()
    {
        $raw = $this->input->post_get("raw") ?: "";
        $page = 0;
        $search = "";
        $order_by = "bank_name";
        $order_direction = "DESC";

        if (!empty($raw)) {
            $raw = json_decode($raw);
            $page = $raw->page;
            $search = $raw->search;
            $order_by = $raw->order_by;
            $order_direction = $raw->order_direction;
        }

        $search = explode(" ", $search);
        $search = implode("%20", $search);

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->createCurl(API_URI .
            "guest/banks?page=$page&search=$search&order-by=$order_by&order-direction=$order_direction");

        print_r($this->custom_curl->__tostring());
    }

    // Detail Banks
    public function detail_bank_account($id)
    {

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->createCurl(API_URI .
            "guest/banks/$id");

        print_r($this->custom_curl->__tostring());
    }

    // Update Bank Account
    public function update_bank_account($id)
    {
        $raw = $this->input->post("raw") ?: "";
        if (empty($raw)) die();

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->setPut($raw);
        $this->custom_curl->createCurl(API_URI .
            "guest/banks/$id");

        print_r($this->custom_curl->__tostring());
    }

    // Delete Bank Account
    public function delete_bank_account($id)
    {

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->setDelete();
        $this->custom_curl->createCurl(API_URI .
            "guest/banks/$id");

        print_r($this->custom_curl->__tostring());
    }

    // Load General
    public function load_general()
    {
        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->createCurl(API_URI .
            "merchant/manage/general/my-market");

        print_r($this->custom_curl->__tostring());
    }

    // Update Status Transaction
    public function update_general() {
        $raw = $this->input->post_get("raw") ?: "";

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->setPut($raw);

        $this->custom_curl->createCurl(API_URI . 
            "merchant/manage/general/my-market");

        print_r($this->custom_curl->__tostring());
    }

    // Edit Logo
    public function edit_logo() {
        $photo = $this->fileUpload->do_upload("photo");

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token,
            'Content-Type: multipart/form-data'
        ));
        $cfile = curl_file_create($photo["file_location"],$_FILES["photo"]["type"],$photo['file_name']);

        $this->custom_curl->setPost(
            array(
                'photo' => $cfile
            )
        );
        $this->custom_curl->createCurl(API_URI . "merchant/manage/general/market-photo");

        print_r($this->custom_curl->__tostring()); 
    }

    // Load Profile
    public function load_profile()
    {
        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->createCurl(API_URI .
            "admin/setting/profile");

        print_r($this->custom_curl->__tostring());
    }

    // Update Profile
    public function update_profile()
    {

        $raw = $this->input->post("raw") ?: "";
        if (empty($raw)) die();

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->setPut($raw);
        $this->custom_curl->createCurl(API_URI .
            "admin/setting/profile");

        print_r($this->custom_curl->__tostring());
    }

    // Block Profile
    public function block_profile()
    {

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->setPut(null);
        $this->custom_curl->createCurl(API_URI .
            "admin/setting/block-user");

        print_r($this->custom_curl->__tostring());
    }

    // Edit Photo
    public function edit_photo() {
        $photo = $this->fileUpload->do_upload("photo");

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token,
            'Content-Type: multipart/form-data'
        ));
        $cfile = curl_file_create($photo["file_location"],$_FILES["photo"]["type"],$photo['file_name']);

        $this->custom_curl->setPost(
            array(
                'photo' => $cfile
            )
        );
        $this->custom_curl->createCurl(API_URI . "admin/setting/profile-picture");

        print_r($this->custom_curl->__tostring()); 
    }

    // Merchant Locations
    // Create
    public function create_location()
    {

        $raw = $this->input->post("raw") ?: "";
        if (empty($raw)) die();

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->setPost($raw);
        $this->custom_curl->createCurl(API_URI .
            "auth/merchant/location");

        print_r($this->custom_curl->__tostring());
    }

    // Update
    public function update_location($id)
    {

        $raw = $this->input->post("raw") ?: "";
        if (empty($raw)) die();

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->setPut($raw);
        $this->custom_curl->createCurl(API_URI .
            "auth/merchant/location/$id");

        print_r($this->custom_curl->__tostring());
    }
}
