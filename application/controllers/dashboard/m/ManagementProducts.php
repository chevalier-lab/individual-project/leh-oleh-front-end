<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ManagementProducts extends CI_Controller
{

    // Public Variable
    public $session, $custom_curl;
    public $meta, $error, $customNavbar;

    public function __construct()
    {
        parent::__construct();

        $this->meta = array(
            "title" => "",
            "description" => "",
            "robots" => ""
        );

        $this->error = array(
            "title" => "Something went wrong",
            "content" => "",
            "details" => array()
        );

        // Load Model
        $this->load->model('Navbar');
        $this->load->model('SidebarMerchant');

        // Load Helper
        $this->session = new Session_helper();

        // Load Helper
        $this->session = new Session_helper();
        $this->custom_curl = new Mycurl_helper("");
        $this->fileUpload = new Upload_file_helper(
            array(
                "file_type" => array(
                    "png",
                    "jpg",
                    "jpeg"
                ),
                "max_size"  => 200000000
            )
        );

        $this->customNavbar = $this->Navbar->getMenus();
        $this->customNavbar["user"]["items"][0]["link"] = base_url("index.php/dashboard/m/settings/profile");
        $this->customNavbar["user"]["items"][1]["link"] = base_url("index.php/dashboard/m/settings/profile");

        // Check Auth
        $this->checkAuth();
    }

    // ==========================================================
    // AUTH PROCESS
    // ==========================================================

    private function checkAuth()
    {
        if (!$this->session->check_session("auth")) {
            redirect(base_url("index.php/general/auth/login"));
        } else {
            $this->auth = $this->session->get_session("auth");
            switch ($this->auth->level) {
                case "admin":
                    redirect(base_url("index.php/dashboard/a/dashboard"));
                    break;
                case "toko":
                    break;
                case "guest":
                    redirect(base_url("index.php/dashboard/g/dashboard"));
                    break;
                case "cs":
                    redirect(base_url("index.php/dashboard/cs/dashboard"));
                    break;
            }
        }
    }

    public function index()
    {
        // Check Auth
        // $this->checkAuth();
        // Setup Meta
        $this->meta["title"] = "Toko Leholeh | Daftar Produk";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        $sidebar = $this->SidebarMerchant->getMenus();
        $sidebar[1]["items"][0]["is_active"] = true;

        // Load Lists Products
        $this->load->view('dashboard-merchant/products/lists-products', array(
            "meta" => $this->meta,
            "navbar" => $this->customNavbar,
            "sidebar" => $sidebar,
        ));
    }

    public function detailProduct($id)
    {
        $this->meta["title"] = "Toko Leholeh | Detail Produk";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        $sidebar = $this->SidebarMerchant->getMenus();
        $sidebar[1]["items"][0]["is_active"] = true;

        // Load Detail Product
        $this->load->view('dashboard-merchant/products/details-product', array(
            "meta" => $this->meta,
            "navbar" => $this->customNavbar,
            "sidebar" => $sidebar,
            "idProduct" => $id
        ));
    }

    public function createProduct()
    {
        $this->meta["title"] = "Toko Leholeh | Buat Produk Baru";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        $sidebar = $this->SidebarMerchant->getMenus();
        $sidebar[1]["items"][0]["is_active"] = true;

        // Load Detail Product
        $this->load->view('dashboard-merchant/products/create-product', array(
            "meta" => $this->meta,
            "navbar" => $this->customNavbar,
            "sidebar" => $sidebar,
        ));
    }

    public function managePhotos($id)
    {
        $this->meta["title"] = "Toko Leholeh | Kelola Foto";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        $sidebar = $this->SidebarMerchant->getMenus();
        $sidebar[1]["items"][0]["is_active"] = true;

        // Load Manage Photos
        $this->load->view('dashboard-merchant/products/manage-photos', array(
            "meta" => $this->meta,
            "navbar" => $this->Navbar->getMenus(),
            "sidebar" => $sidebar,
            "idProduct" => $id
        ));
    }

    // ==========================================================
    // BACK END PROCESS
    // ==========================================================

    // Create Product
    public function create_product()
    {
        $photo = $this->fileUpload->do_upload("cover");

        $name = $this->input->post("name", TRUE) ?: "";
        $slug = $this->input->post("slug", TRUE) ?: "";
        $jumlah = $this->input->post("jumlah", TRUE) ?: "";
        $diskon = $this->input->post("diskon", TRUE) ?: "";
        $hargaJual = $this->input->post("harga_jual", TRUE) ?: "";
        $hargaBeli = $this->input->post("harga_beli", TRUE) ?: "";
        $description = $this->input->post("description", TRUE) ?: "";
        $categories = $this->input->post("categories", TRUE) ?: "";

        // print_r($photo);
        // die();
        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token,
            'Content-Type: multipart/form-data'
        ));
        $cfile = curl_file_create($photo["file_location"], $_FILES["cover"]["type"], $photo['file_name']);

        $this->custom_curl->setPost(
            array(
                'name' => $name,
                'slug' => $slug,
                'jumlah' => $jumlah,
                'diskon' => $diskon,
                'harga_jual' => $hargaJual,
                'harga_beli' => $hargaBeli,
                'description' => $description,
                'categories' => $categories,
                'cover' => $cfile,
            )
        );

        $this->custom_curl->createCurl(API_URI . "merchant/manage/products");

        print_r($this->custom_curl->__tostring());
    }

    // Edit Information Product
    public function edit_product($id)
    {
        $raw = $this->input->post_get("raw") ?: "";

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token,
        ));

        $this->custom_curl->setPut(
            $raw
        );

        $this->custom_curl->createCurl(API_URI . "merchant/manage/products/$id");

        print_r($this->custom_curl->__tostring());
    }

    // Edit Cover Product
    public function edit_cover($id)
    {
        $photo = $this->fileUpload->do_upload("cover");

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token,
            'Content-Type: multipart/form-data'
        ));
        $cfile = curl_file_create($photo["file_location"], $_FILES["cover"]["type"], $photo['file_name']);

        $this->custom_curl->setPost(
            array(
                'id_u_product' => $id,
                'cover' => $cfile,
            )
        );

        $this->custom_curl->createCurl(API_URI . "merchant/manage/products/$id/cover");

        print_r($this->custom_curl->__tostring());
    }
    

    // Load Categories
    public function load_categories()
    {
        $raw = $this->input->post_get("raw") ?: "";
        $page = 0;
        $search = "";
        $order_by = "category";
        $order_direction = "DESC";

        if (!empty($raw)) {
            $raw = json_decode($raw);
            $page = $raw->page;
            $search = $raw->search;
            $order_by = $raw->order_by;
            $order_direction = $raw->order_direction;
        }

        $search = explode(" ", $search);
        $search = implode("%20", $search);

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->createCurl(API_URI .
            "api/admin/master/Categories/getCategories?page=$page&search=$search&order-by=$order_by&order-direction=$order_direction");

        print_r($this->custom_curl->__tostring());
    }

    // Edit Categories Product
    public function edit_categories_product($id)
    {
        $raw = $this->input->post_get("raw") ?: "";

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token,
        ));

        $this->custom_curl->setPut(
            $raw
        );

        $this->custom_curl->createCurl(API_URI . "merchant/manage/products/$id/categories");

        print_r($this->custom_curl->__tostring());
    }

    // Load Products
    public function load_products()
    {
        $raw = $this->input->post_get("raw") ?: "";
        $page = 0;
        $search = "";
        $order_by = "id";
        $order_direction = "DESC";

        if (!empty($raw)) {
            $raw = json_decode($raw);
            $page = $raw->page;
            $search = $raw->search;
            $order_by = $raw->order_by;
            $order_direction = $raw->order_direction;
        }

        $search = explode(" ", $search);
        $search = implode("%20", $search);

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->createCurl(API_URI .
            "merchant/manage/products?page=$page&search=$search&order-by=$order_by&order-direction=$order_direction");

        print_r($this->custom_curl->__tostring());
    }

    public function load_one_products($id)
    {
        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->createCurl(API_URI .
            "merchant/manage/products/$id");

        print_r($this->custom_curl->__tostring());
    }

    // Set Discount
    public function set_discount($id)
    {
        $raw = $this->input->post_get("raw") ?: "";

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token,
        ));

        $this->custom_curl->setPut(
            $raw
        );

        $this->custom_curl->createCurl(API_URI . "merchant/manage/products/$id/discount");

        print_r($this->custom_curl->__tostring());
    }

    // Delete Product
    public function delete_product($id)
    {
        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token,
        ));

        $this->custom_curl->setDelete();
        $this->custom_curl->createCurl(API_URI . "merchant/manage/products/$id/remove");

        print_r($this->custom_curl->__tostring());
    }

    // Create Photo Products
    public function create_photo_products($id)
    {
        $photo = $this->fileUpload->do_upload("photo");
        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token,
            'Content-Type: multipart/form-data'
        ));

        $cfile = curl_file_create($photo["file_location"], $_FILES["photo"]["type"], $photo['file_name']);

        $this->custom_curl->setPost(
            array(
                'id_u_product' => $id,
                'photo' => $cfile,
                'slug' => 'slug'
            )
        );
        $this->custom_curl->createCurl(API_URI . "merchant/manage/products/image");

        print_r($this->custom_curl->__tostring());
    }

    // Load Locations
    public function load_locations()
    {   
        $this->custom_curl->createCurl(API_URI .
            "guest/general/locations");

        print_r($this->custom_curl->__tostring());
    }

    // Load Product Locations
    public function load_product_locations()
    {   
        $this->custom_curl->createCurl(API_URI .
            "guest/general/locations");

        print_r($this->custom_curl->__tostring());
    }

    // Create Product Locations
    public function create_product_locations()
    {
        $raw = $this->input->post_get("raw") ?: "";

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token,
        ));

        $this->custom_curl->setPost($raw);

        $this->custom_curl->createCurl(API_URI . "merchant/manage/products/locations");

        print_r($this->custom_curl->__tostring());
    }

    // Delete Product Locations
    public function delete_product_locations($id)
    {

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token,
        ));

        $this->custom_curl->setDelete();

        $this->custom_curl->createCurl(API_URI . "merchant/manage/products/locations/$id");

        print_r($this->custom_curl->__tostring());
    }

    // Delete Product Photo
    public function delete_product_photo($id)
    {
        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token,
        ));

        $this->custom_curl->setDelete();

        $this->custom_curl->createCurl(API_URI . "merchant/manage/products/$id/image");

        print_r($this->custom_curl->__tostring());
    }

    // Edit Locations Product
    public function edit_locations_product($id)
    {
        $raw = $this->input->post_get("raw") ?: "";

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token,
        ));

        $this->custom_curl->setPut(
            $raw
        );

        $this->custom_curl->createCurl(API_URI . "/merchant/manage/products/$id/location");

        print_r($this->custom_curl->__tostring());
    }

    // Create Product Dimension Info
    public function create_product_dimension()
    {
        $raw = $this->input->post_get("raw") ?: "";

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token,
        ));

        $this->custom_curl->setPost($raw);

        $this->custom_curl->createCurl(API_URI . "/merchant/product-info");

        print_r($this->custom_curl->__tostring());
    }

    // Load Product Dimension Info
    public function load_product_dimension($id)
    {
        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token,
        ));

        $this->custom_curl->createCurl(API_URI . "/merchant/product-info/$id");

        print_r($this->custom_curl->__tostring());
    }

    // Edit Product Dimension Info
    public function edit_product_dimension($id)
    {
        $raw = $this->input->post_get("raw") ?: "";

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token,
        ));

        $this->custom_curl->setPut(
            $raw
        );

        $this->custom_curl->createCurl(API_URI . "/merchant/product-info/$id");

        print_r($this->custom_curl->__tostring());
    }
}
