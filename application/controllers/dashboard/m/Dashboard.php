<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{

    // Public Variable
    public $session, $custom_curl;
    public $meta, $error, $customNavbar;

    public function __construct()
    {
        parent::__construct();

        $this->meta = array(
            "title" => "",
            "description" => "",
            "robots" => ""
        );

        $this->error = array(
            "title" => "Something went wrong",
            "content" => "",
            "details" => array()
        );

        // Load Model
        $this->load->model('Navbar');
        $this->load->model('SidebarMerchant');

        // Load Helper
        $this->session = new Session_helper();

        // Load Helper
        $this->session = new Session_helper();
        $this->custom_curl = new Mycurl_helper("");
        $this->fileUpload = new Upload_file_helper(
            array(
                "file_type" => array(
                    "png",
                    "jpg",
                    "jpeg"
                ),
                "max_size"  => 200000000
            )
        );

        $this->customNavbar = $this->Navbar->getMenus();
        $this->customNavbar["user"]["items"][0]["link"] = base_url("index.php/dashboard/m/settings/profile");
        $this->customNavbar["user"]["items"][1]["link"] = base_url("index.php/dashboard/m/settings/profile");

        // Check Auth
        $this->checkAuth();
    }

    // ==========================================================
    // AUTH PROCESS
    // ==========================================================

    private function checkAuth()
    {
        if (!$this->session->check_session("auth")) {
            redirect(base_url("index.php/general/auth/login"));
        } else {
            $this->auth = $this->session->get_session("auth");
            switch ($this->auth->level) {
                case "admin":
                    redirect(base_url("index.php/dashboard/a/dashboard"));
                    break;
                case "toko":
                    break;
                case "guest":
                    redirect(base_url("index.php/dashboard/g/dashboard"));
                    break;
                case "cs":
                    redirect(base_url("index.php/dashboard/cs/dashboard"));
                    break;
            }
        }
    }

    public function index()
    {
        // Check Auth
        // $this->checkAuth();
        // Setup Meta
        $this->meta["title"] = "Toko Leholeh | Beranda";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        $sidebar = $this->SidebarMerchant->getMenus();
        $sidebar[0]["items"][0]["is_active"] = true;

        // Load Home
        $this->load->view('dashboard-merchant/dashboard/home', array(
            "meta" => $this->meta,
            "navbar" => $this->customNavbar,
            "sidebar" => $sidebar
        ));
    }

    public function logActivity()
    {
        $this->meta["title"] = "Toko Leholeh | Catatan Aktivitas";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        $sidebar = $this->SidebarMerchant->getMenus();
        $sidebar[0]["items"][1]["is_active"] = true;

        // Load Log Activity
        $this->load->view('dashboard-merchant/dashboard/log-activity', array(
            "meta" => $this->meta,
            "navbar" => $this->customNavbar,
            "sidebar" => $sidebar
        ));
    }

    public function chatMerchant()
    {
        $this->meta["title"] = "Toko Leholeh | Layanan Pelanggan";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        $sidebar = $this->SidebarMerchant->getMenus();
        $sidebar[0]["items"][2]["is_active"] = true;

        // Load Chat CS
        $this->load->view('dashboard-merchant/dashboard/chat', array(
            "meta" => $this->meta,
            "navbar" => $this->customNavbar,
            "sidebar" => $sidebar
        ));
    }

    public function detailRoomChat($idRoom)
    {
        $this->meta["title"] = "Toko Leholeh | Ruang Obrolan";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        $sidebar = $this->SidebarMerchant->getMenus();
        $sidebar[0]["items"][2]["is_active"] = true;

        // Load Chat CS
        $this->load->view('dashboard-merchant/dashboard/chat-room', array(
            "meta" => $this->meta,
            "navbar" => $this->customNavbar,
            "sidebar" => $sidebar,
            "idRoom" => $idRoom
        ));
    }

    // ==========================================================
    // BACK END PROCESS
    // ==========================================================

    // Load Banner
    public function banner() {

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->createCurl(API_URI . 
            "merchant/dashboard/banner");

        print_r($this->custom_curl->__tostring());
    }

    // Load Income
    public function income() {

        $date = date("Y-m-d");

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->createCurl(API_URI . 
            "merchant/dashboard/income?date=$date");

        print_r($this->custom_curl->__tostring());
    }

    // Load Log
    public function load_log() {
        $raw = $this->input->post_get("raw") ?: "";
        $page = 0;
        $search = "";
        $order_by = "id";
        $order_direction = "DESC";

        if (!empty($raw)) {
            $raw = json_decode($raw);
            $page = $raw->page;
            $search = $raw->search;
            $order_by = $raw->order_by;
            $order_direction = $raw->order_direction;
        } 

        $search = explode(" ", $search);
        $search = implode("%20", $search);

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->createCurl(API_URI . 
            "merchant/dashboard/last-logs?page=$page&search=$search&order-by=$order_by&order-direction=$order_direction");

        print_r($this->custom_curl->__tostring());
    }
}
