<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ManagementTransactions extends CI_Controller
{

    // Public Variable
    public $session, $custom_curl;
    public $meta, $error, $customNavbar;

    public function __construct()
    {
        parent::__construct();

        $this->meta = array(
            "title" => "",
            "description" => "",
            "robots" => ""
        );

        $this->error = array(
            "title" => "Something went wrong",
            "content" => "",
            "details" => array()
        );

        // Load Model
        $this->load->model('Navbar');
        $this->load->model('SidebarMerchant');

        // Load Helper
        $this->session = new Session_helper();

        // Load Helper
        $this->session = new Session_helper();
        $this->custom_curl = new Mycurl_helper("");
        $this->fileUpload = new Upload_file_helper(
            array(
                "file_type" => array(
                    "png",
                    "jpg",
                    "jpeg"
                ),
                "max_size"  => 200000000
            )
        );

        $this->customNavbar = $this->Navbar->getMenus();
        $this->customNavbar["user"]["items"][0]["link"] = base_url("index.php/dashboard/m/settings/profile");
        $this->customNavbar["user"]["items"][1]["link"] = base_url("index.php/dashboard/m/settings/profile");

        // Check Auth
        $this->checkAuth();
    }

    // ==========================================================
    // AUTH PROCESS
    // ==========================================================

    private function checkAuth()
    {
        if (!$this->session->check_session("auth")) {
            redirect(base_url("index.php/general/auth/login"));
        } else {
            $this->auth = $this->session->get_session("auth");
            switch ($this->auth->level) {
                case "admin":
                    redirect(base_url("index.php/dashboard/a/dashboard"));
                    break;
                case "toko":
                    break;
                case "guest":
                    redirect(base_url("index.php/dashboard/g/dashboard"));
                    break;
                case "cs":
                    redirect(base_url("index.php/dashboard/cs/dashboard"));
                    break;
            }
        }
    }

    public function index()
    {
        $this->meta["title"] = "Toko Leholeh | Kelola Transaksi";
        $sidebar = $this->SidebarMerchant->getMenus();
        $sidebar[2]["items"][0]["is_active"] = true;

        // Load Lists Transactions
        $this->load->view('dashboard-merchant/transactions/lists-transaction', array(
            "meta" => $this->meta,
            "navbar" => $this->customNavbar,
            "sidebar" => $sidebar,
        ));
    }

    // ==========================================================
    // BACK END PROCESS
    // ==========================================================

    // Load Lists Transaction
    public function load_transaction() {
        $raw = $this->input->post_get("raw") ?: "";
        $page = 0;
        $search = "";
        $order_by = "id";
        $order_direction = "DESC";
        $status = 1;

        if (!empty($raw)) {
            $raw = json_decode($raw);
            $page = $raw->page;
            $search = $raw->search;
            $order_by = $raw->order_by;
            $order_direction = $raw->order_direction;
            $status = $raw->status;
        } 

        $search = explode(" ", $search);
        $search = implode("%20", $search);

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->createCurl(API_URI . 
            "merchant/manage/transaction?page=$page&search=$search&order-by=$order_by&order-direction=$order_direction&status=$status");

        print_r($this->custom_curl->__tostring());
    }

    // Load Detail Transaction
    public function detail_transaction($id) {

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->createCurl(API_URI . 
            "merchant/manage/transaction/$id");

        print_r($this->custom_curl->__tostring());
    }

    // Update Status Transaction
    public function update_status_transaction($id) {
        $raw = $this->input->post_get("raw") ?: "";

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->setPut($raw);

        $this->custom_curl->createCurl(API_URI . 
            "merchant/manage/transaction/$id");

        print_r($this->custom_curl->__tostring());
    }

    // Activation Order Transaction
    public function activation_order() {
        $raw = $this->input->post_get("raw") ?: "";

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->setPost($raw);

        $this->custom_curl->createCurl(API_URI . 
            "merchant/order-transaction/activation");

        print_r($this->custom_curl->__tostring());
    }

    // Cancle Order Transaction
    public function cancel_order() {
        $raw = $this->input->post_get("raw") ?: "";

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->setPost($raw);

        $this->custom_curl->createCurl(API_URI . 
            "merchant/order-transaction/cancel");

        print_r($this->custom_curl->__tostring());
    }

    // Cancle Order Transaction
    public function pickup_order() {
        $raw = $this->input->post_get("raw") ?: "";

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->setPost($raw);

        $this->custom_curl->createCurl(API_URI . 
            "merchant/order-transaction/pickup");

        print_r($this->custom_curl->__tostring());
    }
}
