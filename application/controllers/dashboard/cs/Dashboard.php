<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{

    // Public Variable
    public $session, $custom_curl, $fileUpload;
    public $meta, $error, $auth;

    public function __construct()
    {
        parent::__construct();

        $this->meta = array(
            "title" => "",
            "description" => "",
            "robots" => ""
        );

        $this->error = array(
            "title" => "Something went wrong",
            "content" => "",
            "details" => array()
        );

        // Load Model
        $this->load->model('Navbar');
        $this->load->model('SidebarCs');

        // Load Helper
        $this->session = new Session_helper();
        $this->custom_curl = new Mycurl_helper("");
        $this->fileUpload = new Upload_file_helper(
            array(
                "file_type" => array(
                    "png",
                    "jpg",
                    "jpeg"
                ),
                "max_size"  => 200000000
            )
        );

        // Check Auth
        $this->checkAuth();
    }

    // ==========================================================
    // AUTH PROCESS
    // ==========================================================

    private function checkAuth()
    {
        if (!$this->session->check_session("auth")) {
            redirect(base_url("index.php/general/auth/login"));
        } else {
            $this->auth = $this->session->get_session("auth");
            switch ($this->auth->level) {
                case "admin":
                    redirect(base_url("index.php/dashboard/a/dashboard"));
                    break;
                case "toko":
                    redirect(base_url("index.php/dashboard/m/dashboard"));
                    break;
                case "guest":
                    redirect(base_url("index.php/dashboard/g/dashboard"));
                    break;
                case "cs":
                    break;
            }
        }
    }

    public function index()
    {
        $this->meta["title"] = "CS Leholeh | Daftar Obrolan";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        $sidebar = $this->SidebarCs->getMenus();
        $sidebar[0]["items"][0]["is_active"] = true;

        // Load Chat CS
        $this->load->view('dashboard-cs/dashboard/chat-cs', array(
            "meta" => $this->meta,
            "navbar" => $this->Navbar->getMenus(),
            "sidebar" => $sidebar
        ));
    }

    public function detailRoomChat($idRoom)
    {
        $this->meta["title"] = "CS Leholeh | Ruang Obrolan";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        $sidebar = $this->SidebarCs->getMenus();
        $sidebar[0]["items"][0]["is_active"] = true;

        // Load Chat CS
        $this->load->view('dashboard-cs/dashboard/chat-room', array(
            "meta" => $this->meta,
            "navbar" => $this->Navbar->getMenus(),
            "sidebar" => $sidebar,
            "idRoom" => $idRoom
        ));
    }
}