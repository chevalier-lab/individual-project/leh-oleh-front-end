<?php
defined('BASEPATH') or exit('No direct script access allowed');

class MasterData extends CI_Controller
{

    // Public Variable
    public $session, $custom_curl, $fileUpload;
    public $meta, $error, $auth;

    public function __construct()
    {
        parent::__construct();

        $this->meta = array(
            "title" => "",
            "description" => "",
            "robots" => ""
        );

        $this->error = array(
            "title" => "Something went wrong",
            "content" => "",
            "details" => array()
        );

        // Load Model
        $this->load->model('Navbar');
        $this->load->model('Sidebar');
        $this->load->model('Icons');

        // Load Helper
        $this->session = new Session_helper();
        $this->custom_curl = new Mycurl_helper("");
        $this->fileUpload = new Upload_file_helper(
            array(
                "file_type" => array(
                    "png",
                    "jpg",
                    "jpeg"
                ),
                "max_size"  => 200000000
            )
        );

        // Check Auth
        $this->checkAuth();
    }

    // ==========================================================
    // AUTH PROCESS
    // ==========================================================

    private function checkAuth()
    {
        if (!$this->session->check_session("auth")) {
            redirect(base_url("index.php/general/auth/login"));
        } else {
            $this->auth = $this->session->get_session("auth");
            switch ($this->auth->level) {
                case "admin":
                    break;
                case "toko":
                    redirect(base_url("index.php/dashboard/m/dashboard"));
                    break;
                case "guest":
                    redirect(base_url("index.php/dashboard/g/dashboard"));
                    break;
                case "cs":
                    redirect(base_url("index.php/dashboard/cs/dashboard"));
                    break;
            }
        }
    }

    public function index()
    {
        // Check Auth
        // $this->checkAuth();
        // Setup Meta
        $this->meta["title"] = "Admin Leholeh | Bank";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        $sidebar = $this->Sidebar->getMenus();
        $sidebar[6]["items"][0]["is_active"] = true;

        // Load Bank
        $this->load->view('dashboard/master-data/bank', array(
            "meta" => $this->meta,
            "navbar" => $this->Navbar->getMenus(),
            "sidebar" => $sidebar,
        ));
    }

    public function menus()
    {
        // Setup Meta
        $this->meta["title"] = "Admin Leholeh | Menu";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        $sidebar = $this->Sidebar->getMenus();
        $sidebar[6]["items"][1]["is_active"] = true;

        // Load Menus
        $this->load->view('dashboard/master-data/menus', array(
            "meta" => $this->meta,
            "navbar" => $this->Navbar->getMenus(),
            "sidebar" => $sidebar,
        ));
    }

    public function categories()
    {
        // Setup Meta
        $this->meta["title"] = "Admin Leholeh | Kategori";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        $sidebar = $this->Sidebar->getMenus();
        $sidebar[6]["items"][2]["is_active"] = true;

        // Load Categories
        $this->load->view('dashboard/master-data/categories', array(
            "meta" => $this->meta,
            "navbar" => $this->Navbar->getMenus(),
            "sidebar" => $sidebar,
        ));
    }

    public function tags()
    {
        // Setup Meta
        $this->meta["title"] = "Admin Leholeh | Label";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        $sidebar = $this->Sidebar->getMenus();
        $sidebar[6]["items"][3]["is_active"] = true;

        // Load Tags
        $this->load->view('dashboard/master-data/tags', array(
            "meta" => $this->meta,
            "navbar" => $this->Navbar->getMenus(),
            "sidebar" => $sidebar,
        ));
    }

    public function icons()
    {
        // Setup Meta
        $this->meta["title"] = "Admin Leholeh | Ikon";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        $sidebar = $this->Sidebar->getMenus();
        $sidebar[6]["items"][4]["is_active"] = true;

        // Load Icons
        $this->load->view('dashboard/master-data/icons', array(
            "meta" => $this->meta,
            "navbar" => $this->Navbar->getMenus(),
            "sidebar" => $sidebar,
            "icons" => $this->Icons->getIcons(),
        ));
    }

    public function medias()
    {
        // Setup Meta
        $this->meta["title"] = "Admin Leholeh | Media";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        $sidebar = $this->Sidebar->getMenus();
        $sidebar[6]["items"][5]["is_active"] = true;

        // Load Medias
        $this->load->view('dashboard/master-data/medias', array(
            "meta" => $this->meta,
            "navbar" => $this->Navbar->getMenus(),
            "sidebar" => $sidebar,
        ));
    }

    public function wallets()
    {
        // Setup Meta
        $this->meta["title"] = "Admin Leholeh | Dompet";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        $sidebar = $this->Sidebar->getMenus();
        $sidebar[6]["items"][6]["is_active"] = true;

        // Load Wallets
        $this->load->view('dashboard/master-data/wallets', array(
            "meta" => $this->meta,
            "navbar" => $this->Navbar->getMenus(),
            "sidebar" => $sidebar,
        ));
    }

    public function deliveryService()
    {
        // Setup Meta
        $this->meta["title"] = "Admin Leholeh | Jasa Pengiriman";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        $sidebar = $this->Sidebar->getMenus();
        $sidebar[6]["items"][7]["is_active"] = true;

        // Load Delivery Service
        $this->load->view('dashboard/master-data/delivery-service', array(
            "meta" => $this->meta,
            "navbar" => $this->Navbar->getMenus(),
            "sidebar" => $sidebar
        ));
    }

    public function feedback()
    {
        $this->meta["title"] = "Admin Leholeh | Masukan";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        $sidebar = $this->Sidebar->getMenus();
        $sidebar[6]["items"][8]["is_active"] = true;

        // Load Chat CS
        $this->load->view('dashboard/master-data/feedback', array(
            "meta" => $this->meta,
            "navbar" => $this->Navbar->getMenus(),
            "sidebar" => $sidebar
        ));
    }

    public function faq()
    {
        $this->meta["title"] = "Admin Leholeh | FAQ";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        $sidebar = $this->Sidebar->getMenus();
        $sidebar[6]["items"][9]["is_active"] = true;

        // Load Chat CS
        $this->load->view('dashboard/master-data/faq', array(
            "meta" => $this->meta,
            "navbar" => $this->Navbar->getMenus(),
            "sidebar" => $sidebar,
            "full_name" => $this->auth->full_name
        ));
    }

    // ==========================================================
    // BACK-END PROCESS
    // ==========================================================

    // Load Banks
    public function load_banks()
    {
        $raw = $this->input->post_get("raw") ?: "";
        $page = 0;
        $search = "";
        $order_by = "bank_name";
        $order_direction = "DESC";

        if (!empty($raw)) {
            $raw = json_decode($raw);
            $page = $raw->page;
            $search = $raw->search;
            $order_by = $raw->order_by;
            $order_direction = $raw->order_direction;
        }

        $search = explode(" ", $search);
        $search = implode("%20", $search);

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->createCurl(API_URI .
            "api/admin/master/Banks/getBanks?page=$page&search=$search&order-by=$order_by&order-direction=$order_direction");

        print_r($this->custom_curl->__tostring());
    }

    // Create Bank
    public function create_bank()
    {
        $photo = $this->fileUpload->do_upload("logo");
        $name = $this->input->post("name", TRUE) ?: "";
        // print_r($photo);
        // die();
        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token,
            'Content-Type: multipart/form-data'
        ));
        $cfile = curl_file_create($photo["file_location"], $_FILES["logo"]["type"], $photo['file_name']);

        $this->custom_curl->setPost(
            array(
                'logo' => $cfile,
                'name' => $name
            )
        );
        $this->custom_curl->createCurl(API_URI . "api/admin/master/Banks/create");

        print_r($this->custom_curl->__tostring());
    }

    // Load Detail Bank
    public function load_detail_bank($id)
    {
        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->createCurl(API_URI .
            "api/admin/master/Banks/getBank/$id");

        print_r($this->custom_curl->__tostring());
    }

    // Edit Bank
    public function edit_bank($id)
    {
        $raw = $this->input->post_get("raw") ?: "";

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token,
        ));

        $this->custom_curl->setPost(
            $raw
        );

        $this->custom_curl->createCurl(API_URI . "api/admin/master/Banks/update/$id");

        print_r($this->custom_curl->__tostring());
    }

    // Edit Logo Bank
    public function edit_logo_bank($id)
    {
        $logo = $this->fileUpload->do_upload("logo");

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token,
            'Content-Type: multipart/form-data'
        ));
        $cfile = curl_file_create($logo["file_location"], $_FILES["logo"]["type"], $logo['file_name']);

        $this->custom_curl->setPost(
            array(
                'logo' => $cfile
            )
        );
        $this->custom_curl->createCurl(API_URI . "api/admin/master/Banks/changeLogo/$id");

        print_r($this->custom_curl->__tostring());
    }

    // Delete Bank
    public function delete_bank($id)
    {
        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->setDelete();
        $this->custom_curl->createCurl(API_URI . "api/admin/master/Banks/delete/$id");

        print_r($this->custom_curl->__tostring());
    }

    // Utility Bank
    // Create
    public function create_utility_bank()
    {
        $raw = $this->input->post_get("raw") ?: "";
        // print_r($photo);
        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->setPost($raw);
        $this->custom_curl->createCurl(API_URI . "master/ubank");

        print_r($this->custom_curl->__tostring());
    }

    // Get Lists
    public function list_utility_bank($id_m_banks)
    {
        // print_r($photo);
        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->createCurl(API_URI . "master/ubank/$id_m_banks");

        print_r($this->custom_curl->__tostring());
    }

    // Update
    public function update_utility_bank($id_u_bank)
    {
        $raw = $this->input->post_get("raw") ?: "";
        // print_r($photo);
        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->setPut($raw);
        $this->custom_curl->createCurl(API_URI . "master/ubank/$id_u_bank");

        print_r($this->custom_curl->__tostring());
    }

    // Delete
    public function delete_utility_bank($id_u_bank)
    {
        // print_r($photo);
        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->setDelete();
        $this->custom_curl->createCurl(API_URI . "master/ubank/$id_u_bank");

        print_r($this->custom_curl->__tostring());
    }

    // Bank Account
    // Create
    public function create_bank_account()
    {
        $raw = $this->input->post_get("raw") ?: "";
        // print_r($photo);
        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->setPost($raw);
        $this->custom_curl->createCurl(API_URI . "master/bank/account");

        print_r($this->custom_curl->__tostring());
    }

    // Get Lists
    public function list_bank_account($id_m_banks)
    {
        // print_r($photo);
        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->createCurl(API_URI . "master/bank/account/$id_m_banks");

        print_r($this->custom_curl->__tostring());
    }

    // Update
    public function update_bank_account($id_u_bank_account)
    {
        $raw = $this->input->post_get("raw") ?: "";
        // print_r($photo);
        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->setPut($raw);
        $this->custom_curl->createCurl(API_URI . "master/bank/account/$id_u_bank_account");

        print_r($this->custom_curl->__tostring());
    }

    // Delete
    public function delete_bank_account($id_u_bank_account)
    {
        // print_r($photo);
        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->setDelete();
        $this->custom_curl->createCurl(API_URI . "master/bank/account/$id_u_bank_account");

        print_r($this->custom_curl->__tostring());
    }

    // Load Categories
    public function load_categories()
    {
        $raw = $this->input->post_get("raw") ?: "";
        $page = 0;
        $search = "";
        $order_by = "category";
        $order_direction = "DESC";

        if (!empty($raw)) {
            $raw = json_decode($raw);
            $page = $raw->page;
            $search = $raw->search;
            $order_by = $raw->order_by;
            $order_direction = $raw->order_direction;
        }

        $search = explode(" ", $search);
        $search = implode("%20", $search);

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->createCurl(API_URI .
            "api/admin/master/Categories/getCategories?page=$page&search=$search&order-by=$order_by&order-direction=$order_direction");

        print_r($this->custom_curl->__tostring());
    }

    // Create Category
    public function create_categories()
    {
        $raw = $this->input->post_get("raw") ?: "";

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token,
        ));

        $this->custom_curl->setPost($raw);
        $this->custom_curl->createCurl(API_URI . "api/admin/master/Categories/create");

        print_r($this->custom_curl->__tostring());
    }

    // Load Detail Categories
    public function load_detail_category($id)
    {
        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->createCurl(API_URI .
            "api/admin/master/Categories/getCategory/$id");

        print_r($this->custom_curl->__tostring());
    }

    // Edit Categories
    public function edit_category($id)
    {
        $raw = $this->input->post_get("raw") ?: "";

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token,
        ));

        $this->custom_curl->setPost($raw);

        $this->custom_curl->createCurl(API_URI . "api/admin/master/Categories/update/$id");

        print_r($this->custom_curl->__tostring());
    }

    // Delete Categories
    public function delete_categories($id)
    {
        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->setDelete();
        $this->custom_curl->createCurl(API_URI . "api/admin/master/Categories/delete/$id");

        print_r($this->custom_curl->__tostring());
    }

    // Load Icons
    public function load_icons()
    {
        $raw = $this->input->post_get("raw") ?: "";
        $page = -1;
        $search = "";
        $order_by = "icon";
        $order_direction = "DESC";

        if (!empty($raw)) {
            $raw = json_decode($raw);
            $page = $raw->page;
            $search = $raw->search;
            $order_by = $raw->order_by;
            $order_direction = $raw->order_direction;
        }

        $search = explode(" ", $search);
        $search = implode("%20", $search);

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        if($page == -1){
            $this->custom_curl->createCurl(API_URI .
                "api/admin/master/Icons/listData?search=$search&order-by=$order_by&order-direction=$order_direction");
        }else{
            $this->custom_curl->createCurl(API_URI .
                "api/admin/master/Icons/listData?page=$page&search=$search&order-by=$order_by&order-direction=$order_direction");
        }

        print_r($this->custom_curl->__tostring());
    }

    // Create Icons
    public function create_icons()
    {
        $raw = $this->input->post_get("raw") ?: "";

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token,
        ));

        $this->custom_curl->setPost($raw);
        $this->custom_curl->createCurl(API_URI . "api/admin/master/Icons/create");

        print_r($this->custom_curl->__tostring());
    }

    // Load Detail Icon
    public function load_detail_icon($id)
    {
        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->createCurl(API_URI .
            "api/admin/master/Icons/getIconById/$id");

        print_r($this->custom_curl->__tostring());
    }

    // Edit Icon
    public function edit_icon($id)
    {
        $raw = $this->input->post_get("raw", TRUE) ?: "";

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->setPut($raw);
        $this->custom_curl->createCurl(API_URI . "api/admin/master/Icons/update/$id");

        print_r($this->custom_curl->__tostring());
    }

    // Delete Icons
    public function delete_icons($id)
    {
        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->setDelete();
        $this->custom_curl->createCurl(API_URI . "api/admin/master/Icons/delete/$id");

        print_r($this->custom_curl->__tostring());
    }

    // Load Medias
    public function load_medias()
    {
        $raw = $this->input->post_get("raw") ?: "";
        $page = 0;
        $search = "";
        $order_by = "label";
        $order_direction = "DESC";

        if (!empty($raw)) {
            $raw = json_decode($raw);
            $page = $raw->page;
            $search = $raw->search;
            $order_by = $raw->order_by;
            $order_direction = $raw->order_direction;
        }

        $search = explode(" ", $search);
        $search = implode("%20", $search);

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->createCurl(API_URI .
            "api/admin/master/Medias/listData?page=$page&search=$search&order-by=$order_by&order-direction=$order_direction");

        print_r($this->custom_curl->__tostring());
    }

    // Create Medias
    public function create_medias()
    {
        $photo = $this->fileUpload->do_upload("file-media");
        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token,
            'Content-Type: multipart/form-data'
        ));
        $cfile = curl_file_create($photo["file_location"], $_FILES["file-media"]["type"], $photo['file_name']);

        $this->custom_curl->setPost(
            array(
                'photo' => $cfile,
            )
        );
        $this->custom_curl->createCurl(API_URI . "api/admin/master/Medias/create");

        print_r($this->custom_curl->__tostring());
    }

    // Load Detail Media
    public function load_detail_media($id)
    {
        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->createCurl(API_URI .
            "api/admin/master/Medias/getMediaById/$id");

        print_r($this->custom_curl->__tostring());
    }

    // Edit Is Visible
    public function edit_visible_media($id)
    {
        $raw = $this->input->post_get("raw", TRUE) ?: "";

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token,
        ));

        $this->custom_curl->setPut($raw);
        $this->custom_curl->createCurl(API_URI . "api/admin/master/Medias/updateVisibleMedia/$id");

        print_r($this->custom_curl->__tostring());
    }

    // Edit Media
    public function edit_media($id)
    {
        $media = $this->fileUpload->do_upload("media");

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token,
            'Content-Type: multipart/form-data'
        ));
        $cfile = curl_file_create($media["file_location"], $_FILES["media"]["type"], $media['file_name']);

        $this->custom_curl->setPost(
            array(
                'photo' => $cfile
            )
        );
        $this->custom_curl->createCurl(API_URI . "api/admin/master/Medias/update/$id");

        print_r($this->custom_curl->__tostring());
    }

    // Delete Media
    public function delete_medias($id)
    {
        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->setDelete();
        $this->custom_curl->createCurl(API_URI . "api/admin/master/Medias/delete/$id");

        print_r($this->custom_curl->__tostring());
    }

    // Load Menu
    public function load_menus()
    {
        $raw = $this->input->post_get("raw") ?: "";
        $page = 0;
        $search = "";
        $order_by = "menu";
        $order_direction = "DESC";

        if (!empty($raw)) {
            $raw = json_decode($raw);
            $page = $raw->page;
            $search = $raw->search;
            $order_by = $raw->order_by;
            $order_direction = $raw->order_direction;
        }

        $search = explode(" ", $search);
        $search = implode("%20", $search);

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->createCurl(API_URI .
            "api/admin/master/Menus/getMenus?page=$page&search=$search&order-by=$order_by&order-direction=$order_direction");

        print_r($this->custom_curl->__tostring());
    }

    // Create Menu
    public function create_menus()
    {
        $raw = $this->input->post_get("raw") ?: "";

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token,
        ));

        $this->custom_curl->setPost($raw);
        $this->custom_curl->createCurl(API_URI . "api/admin/master/Menus/create");

        print_r($this->custom_curl->__tostring());
    }

    // Load Detail Menu
    public function load_detail_menu($id)
    {
        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->createCurl(API_URI .
            "api/admin/master/Menus/getMenu/$id");

        print_r($this->custom_curl->__tostring());
    }

    // Edit Menu
    public function edit_menu($id)
    {
        $raw = $this->input->post_get("raw", TRUE) ?: "";

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->setPut($raw);
        $this->custom_curl->createCurl(API_URI . "api/admin/master/Menus/update/$id");

        print_r($this->custom_curl->__tostring());
    }

    // Delete Menu
    public function delete_menus($id)
    {
        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->setDelete();
        $this->custom_curl->createCurl(API_URI . "api/admin/master/Menus/delete/$id");

        print_r($this->custom_curl->__tostring());
    }

    // Load Tags
    public function load_tags()
    {
        $raw = $this->input->post_get("raw") ?: "";
        $page = 0;
        $search = "";
        $order_by = "tag";
        $order_direction = "DESC";

        if (!empty($raw)) {
            $raw = json_decode($raw);
            $page = $raw->page;
            $search = $raw->search;
            $order_by = $raw->order_by;
            $order_direction = $raw->order_direction;
        }

        $search = explode(" ", $search);
        $search = implode("%20", $search);

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->createCurl(API_URI .
            "api/admin/master/Tags/listData?page=$page&search=$search&order-by=$order_by&order-direction=$order_direction");

        print_r($this->custom_curl->__tostring());
    }

    // Create Tags
    public function create_tags()
    {
        $raw = $this->input->post_get("raw") ?: "";

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token,
        ));

        $this->custom_curl->setPost($raw);
        $this->custom_curl->createCurl(API_URI . "api/admin/master/Tags/create");

        print_r($this->custom_curl->__tostring());
    }

    // Load Detail Tag
    public function load_detail_tag($id)
    {
        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->createCurl(API_URI .
            "api/admin/master/Tags/getTagById/$id");

        print_r($this->custom_curl->__tostring());
    }

    // Edit Tag
    public function edit_tag($id)
    {
        $raw = $this->input->post_get("raw", TRUE) ?: "";

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->setPut($raw);
        $this->custom_curl->createCurl(API_URI . "api/admin/master/Tags/update/$id");

        print_r($this->custom_curl->__tostring());
    }

    // Delete tags
    public function delete_tags($id)
    {
        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->setDelete();
        $this->custom_curl->createCurl(API_URI . "api/admin/master/Tags/delete/$id");

        print_r($this->custom_curl->__tostring());
    }

    // Load Wallets
    public function load_wallets()
    {
        $raw = $this->input->post_get("raw") ?: "";
        $page = 0;
        $search = "";
        $order_by = "wallet_name";
        $order_direction = "DESC";

        if (!empty($raw)) {
            $raw = json_decode($raw);
            $page = $raw->page;
            $search = $raw->search;
            $order_by = $raw->order_by;
            $order_direction = $raw->order_direction;
        }

        $search = explode(" ", $search);
        $search = implode("%20", $search);

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->createCurl(API_URI .
            "api/admin/master/Wallets/listData?page=$page&search=$search&order-by=$order_by&order-direction=$order_direction");

        print_r($this->custom_curl->__tostring());
    }

    // Create Wallets
    public function create_wallets()
    {
        $photo = $this->fileUpload->do_upload("logo");
        $name = $this->input->post("name", TRUE) ?: "";
        $description = $this->input->post("description", TRUE) ?: "";
        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token,
            'Content-Type: multipart/form-data'
        ));
        $cfile = curl_file_create($photo["file_location"], $_FILES["logo"]["type"], $photo['file_name']);

        $this->custom_curl->setPost(
            array(
                'logo' => $cfile,
                'name' => $name,
                'desc' => $description
            )
        );
        $this->custom_curl->createCurl(API_URI . "api/admin/master/Wallets/create");

        print_r($this->custom_curl->__tostring());
    }

    // Load Detail Wallet
    public function load_detail_wallet($id)
    {
        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->createCurl(API_URI .
            "api/admin/master/Wallets/getById/$id");

        print_r($this->custom_curl->__tostring());
    }

    // Edit Wallet
    public function edit_wallet($id)
    {
        $raw = $this->input->post_get("raw") ?: "";

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token,
        ));

        $this->custom_curl->setPost(
            $raw
        );

        $this->custom_curl->createCurl(API_URI . "api/admin/master/Wallets/update/$id");

        print_r($this->custom_curl->__tostring());
    }

    // Edit Logo Wallet
    public function edit_logo_wallet($id)
    {
        $logo = $this->fileUpload->do_upload("logo");

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token,
            'Content-Type: multipart/form-data'
        ));
        $cfile = curl_file_create($logo["file_location"], $_FILES["logo"]["type"], $logo['file_name']);

        $this->custom_curl->setPost(
            array(
                'logo' => $cfile
            )
        );
        $this->custom_curl->createCurl(API_URI . "api/admin/master/Wallets/updateCover/$id");

        print_r($this->custom_curl->__tostring());
    }

    // Delete Bank
    public function delete_wallet($id)
    {
        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->setDelete();
        $this->custom_curl->createCurl(API_URI . "api/admin/master/Wallets/delete/$id");

        print_r($this->custom_curl->__tostring());
    }

    // Load Feedback
    public function load_feedback()
    {
        $raw = $this->input->post_get("raw") ?: "";
        $page = 0;
        $order_by = "";
        $order_direction = "DESC";

        if (!empty($raw)) {
            $raw = json_decode($raw);
            $page = $raw->page;
            $order_by = $raw->order_by;
            $order_direction = $raw->order_direction;
        }

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->createCurl(API_URI .
            "admin/feedback?page=$page&order-by=$order_by&order-direction=$order_direction");

        print_r($this->custom_curl->__tostring());
    }

    // Load Detail Feedback
    public function load_detail_feedback($id)
    {
        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->createCurl(API_URI .
            "admin/feedback/$id");

        print_r($this->custom_curl->__tostring());
    }

    // Edit Categories
    public function edit_feedback($id)
    {
        $raw = $this->input->post_get("raw") ?: "";

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token,
        ));

        $this->custom_curl->setPut($raw);

        $this->custom_curl->createCurl(API_URI . "admin/feedback/$id");

        print_r($this->custom_curl->__tostring());
    }

    // Delete Feedback
    public function delete_feedback($id)
    {
        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->setDelete();
        $this->custom_curl->createCurl(API_URI . "admin/feedback/$id");

        print_r($this->custom_curl->__tostring());
    }

    // Create FAQ
    public function create_faq()
    {
        $raw = $this->input->post_get("raw") ?: "";

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token,
        ));

        $this->custom_curl->setPost($raw);
        $this->custom_curl->createCurl(API_URI . "admin/manage/faq");

        print_r($this->custom_curl->__tostring());
    }

    // Load FAQ
    public function load_faq()
    {
        $raw = $this->input->post_get("raw") ?: "";
        $page = 0;
        $search = "";
        $order_by = "id";
        $order_direction = "ASC";

        if (!empty($raw)) {
            $raw = json_decode($raw);
            $page = $raw->page;
            $search = $raw->search;
            $order_by = $raw->order_by;
            $order_direction = $raw->order_direction;
        }

        $search = explode(" ", $search);
        $search = implode("%20", $search);

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->createCurl(API_URI .
            "admin/manage/faq?page=$page&search=$search&order-by=$order_by&order-direction=$order_direction");

        print_r($this->custom_curl->__tostring());
    }

    // Load Detail FAQ
    public function load_detail_faq($id)
    {
        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->createCurl(API_URI .
            "admin/manage/faq/$id");

        print_r($this->custom_curl->__tostring());
    }

    // Edit FAQ
    public function edit_faq($id)
    {
        $raw = $this->input->post_get("raw") ?: "";

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token,
        ));

        $this->custom_curl->setPut($raw);

        $this->custom_curl->createCurl(API_URI . "admin/manage/faq/$id");

        print_r($this->custom_curl->__tostring());
    }

    // Delete FAQ
    public function delete_faq($id)
    {
        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->setDelete();
        $this->custom_curl->createCurl(API_URI . "admin/manage/faq/$id");

        print_r($this->custom_curl->__tostring());
    }
}