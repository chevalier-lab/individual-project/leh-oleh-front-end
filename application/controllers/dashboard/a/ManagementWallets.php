<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ManagementWallets extends CI_Controller
{

    // Public Variable
    public $session, $custom_curl, $fileUpload;
    public $meta, $error, $auth;

    public function __construct()
    {
        parent::__construct();

        $this->meta = array(
            "title" => "",
            "description" => "",
            "robots" => ""
        );

        $this->error = array(
            "title" => "Something went wrong",
            "content" => "",
            "details" => array()
        );

        // Load Model
        $this->load->model('Navbar');
        $this->load->model('Sidebar');

        // Load Helper
        $this->session = new Session_helper();
        $this->custom_curl = new Mycurl_helper("");
        $this->fileUpload = new Upload_file_helper(
            array(
                "file_type" => array(
                    "png",
                    "jpg",
                    "jpeg"
                ),
                "max_size"  => 200000000
            )
        );

        // Check Auth
        $this->checkAuth();
    }

    // ==========================================================
    // AUTH PROCESS
    // ==========================================================

    private function checkAuth()
    {
        if (!$this->session->check_session("auth")) {
            redirect(base_url("index.php/general/auth/login"));
        } else {
            $this->auth = $this->session->get_session("auth");
            switch ($this->auth->level) {
                case "admin":
                    break;
                case "toko":
                    redirect(base_url("index.php/dashboard/m/dashboard"));
                    break;
                case "guest":
                    redirect(base_url("index.php/dashboard/g/dashboard"));
                    break;
                case "cs":
                    redirect(base_url("index.php/dashboard/cs/dashboard"));
                    break;
            }
        }
    }

    public function index()
    {
        // Check Auth
        // $this->checkAuth();
        // Setup Meta
        $this->meta["title"] = "Admin Leholeh | Daftar Dompet Pengguna";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        $sidebar = $this->Sidebar->getMenus();
        $sidebar[4]["items"][0]["is_active"] = true;

        // Data Dummy
        $dataTable = array(
            array(
                'full_name' => 'Feby',
                'wallet' => 'Leh-oleh Pay',
                'balance' => 100000,
                'status' => 1,
                'created_at' => '2020-08-07 19:00:00',
                'updated_at' => '2020-08-07 19:00:00'
            )
        );

        // Load Lists Wallet Users
        $this->load->view('dashboard/wallet/lists-wallet-user', array(
            "meta" => $this->meta,
            "navbar" => $this->Navbar->getMenus(),
            "sidebar" => $sidebar,
            "dataTable" => $dataTable
        ));
    }

    public function listsTopUp()
    {
        // Setup Meta
        $this->meta["title"] = "Admin Leholeh | Daftar Isi Ulang";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        $sidebar = $this->Sidebar->getMenus();
        $sidebar[4]["items"][1]["is_active"] = true;

        // Data Dummy
        $dataTable = array(
            array(
                'full_name' => 'Feby',
                'wallet' => 'Leh-oleh Pay',
                'balance_request' => 100000,
                'balance_transfer' => 120000,
                'status' => 2,
                'created_at' => '2020-08-07 19:00:00',
                'updated_at' => '2020-08-07 19:00:00'
            )
        );

        // Load Lists Top Up
        $this->load->view('dashboard/wallet/lists-topup', array(
            "meta" => $this->meta,
            "navbar" => $this->Navbar->getMenus(),
            "sidebar" => $sidebar,
            "dataTable" => $dataTable
        ));
    }

    public function listsWithdraw()
    {
        // Setup Meta
        $this->meta["title"] = "Admin Leholeh | Daftar Tarik Saldo";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";
        $sidebar = $this->Sidebar->getMenus();
        $sidebar[4]["items"][2]["is_active"] = true;

        // Load Lists Withdraw
        $this->load->view('dashboard/wallet/lists-withdraw', array(
            "meta" => $this->meta,
            "navbar" => $this->Navbar->getMenus(),
            "sidebar" => $sidebar,
        ));
    }

    // ==========================================================
    // BACK END PROCESS
    // ==========================================================

    // Load Lists Users Wallet
    public function load_users_wallet() {
        $raw = $this->input->post_get("raw") ?: "";
        $page = 0;
        $search = "";
        $order_by = "id";
        $order_direction = "DESC";

        if (!empty($raw)) {
            $raw = json_decode($raw);
            $page = $raw->page;
            $search = $raw->search;
            $order_by = $raw->order_by;
            $order_direction = $raw->order_direction;
        } 

        $search = explode(" ", $search);
        $search = implode("%20", $search);

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->createCurl(API_URI . 
            "admin/manage/wallet?page=$page&search=$search&order-by=$order_by&order-direction=$order_direction");

        print_r($this->custom_curl->__tostring());
    }

    // Load Lists Users Wallet Top Up
    public function load_top_up() {
        $raw = $this->input->post_get("raw") ?: "";
        $page = 0;
        $search = "";
        $order_by = "id";
        $order_direction = "DESC";

        if (!empty($raw)) {
            $raw = json_decode($raw);
            $page = $raw->page;
            $search = $raw->search;
            $order_by = $raw->order_by;
            $order_direction = $raw->order_direction;
        } 

        $search = explode(" ", $search);
        $search = implode("%20", $search);

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->createCurl(API_URI . 
            "admin/manage/wallet/top-up?page=$page&search=$search&order-by=$order_by&order-direction=$order_direction");

        print_r($this->custom_curl->__tostring());
    }

    // Change Status Users Wallet Top Up
    public function change_status_top_up($id) {
        $raw = $this->input->post_get("raw") ?: "";

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->setPut($raw);
        $this->custom_curl->createCurl(API_URI . 
            "admin/manage/wallet/top-up/$id");

        print_r($this->custom_curl->__tostring());
    }

    // Change Status Users Wallet Top Up
    public function detail_top_up($id) {
        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->createCurl(API_URI . 
            "admin/manage/wallet/top-up/$id");

        print_r($this->custom_curl->__tostring());
    }

    // Load Lists Users Wallet Withdraw
    public function load_withdraw() {
        $raw = $this->input->post_get("raw") ?: "";
        $page = 0;
        $search = "";
        $order_by = "id";
        $order_direction = "DESC";

        if (!empty($raw)) {
            $raw = json_decode($raw);
            $page = $raw->page;
            $search = $raw->search;
            $order_by = $raw->order_by;
            $order_direction = $raw->order_direction;
        } 

        $search = explode(" ", $search);
        $search = implode("%20", $search);

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->createCurl(API_URI . 
            "admin/manage/wallet/withdraw?page=$page&search=$search&order-by=$order_by&order-direction=$order_direction");

        print_r($this->custom_curl->__tostring());
    }
            
    // Change Status Users Wallet Wtithdraw
    public function change_status_withdraw($id) {
        $raw = $this->input->post_get("raw") ?: "";

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->setPut($raw);

        $this->custom_curl->createCurl(API_URI . 
            "admin/manage/wallet/withdraw/$id");

        print_r($this->custom_curl->__tostring());
    }
    
    // Change Status Users Wallet Withdraw
    public function detail_withdraw($id) {
        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->createCurl(API_URI . 
            "admin/manage/wallet/withdraw/$id");

        print_r($this->custom_curl->__tostring());
    }

    // Detail Wallet User
    public function detail_wallet($id) {
        $raw = $this->input->post_get("raw") ?: "";
        $page = 0;
        $search = "";
        $order_by = "id";
        $order_direction = "DESC";

        if (!empty($raw)) {
            $raw = json_decode($raw);
            $page = $raw->page;
            $search = $raw->search;
            $order_by = $raw->order_by;
            $order_direction = $raw->order_direction;
        } 

        $search = explode(" ", $search);
        $search = implode("%20", $search);

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->createCurl(API_URI . 
            "admin/manage/wallet/$id?page=$page&search=$search&order-by=$order_by&order-direction=$order_direction");

        print_r($this->custom_curl->__tostring());
    }

    // Check Status Payment Midtrans
    public function check_status_payment($orderId)
    {

        $this->custom_curl->setHeader(array(
            "Authorization: Basic " . base64_encode(API_KEY_MIDTRANS.":"),
            "Content-type: application/json",
            "Accept: application/json"
        ));

        $this->custom_curl->createCurl(API_MIDTRANS_2 . "/v2/$orderId/status");

        print_r($this->custom_curl->__tostring());

    }
}
