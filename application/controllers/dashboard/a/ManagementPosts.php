<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ManagementPosts extends CI_Controller
{

    // Public Variable
    public $session, $custom_curl, $fileUpload;
    public $meta, $error, $auth;

    public function __construct()
    {
        parent::__construct();

        $this->meta = array(
            "title" => "",
            "description" => "",
            "robots" => ""
        );

        $this->error = array(
            "title" => "Something went wrong",
            "content" => "",
            "details" => array()
        );

        // Load Model
        $this->load->model('Navbar');
        $this->load->model('Sidebar');

        // Load Helper
        $this->session = new Session_helper();
        $this->custom_curl = new Mycurl_helper("");
        $this->fileUpload = new Upload_file_helper(
            array(
                "file_type" => array(
                    "png",
                    "jpg",
                    "jpeg"
                ),
                "max_size"  => 200000000
            )
        );

        // Check Auth
        $this->checkAuth();
    }

    // ==========================================================
    // AUTH PROCESS
    // ==========================================================

    private function checkAuth()
    {
        if (!$this->session->check_session("auth")) {
            redirect(base_url("index.php/general/auth/login"));
        } else {
            $this->auth = $this->session->get_session("auth");
            switch ($this->auth->level) {
                case "admin":
                    break;
                case "toko":
                    redirect(base_url("index.php/dashboard/m/dashboard"));
                    break;
                case "guest":
                    redirect(base_url("index.php/dashboard/g/dashboard"));
                    break;
                case "cs":
                    redirect(base_url("index.php/dashboard/cs/dashboard"));
                    break;
            }
        }
    }

    public function index()
    {
        // Setup Meta
        $this->meta["title"] = "Admin Leholeh | Daftar Pos";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        $sidebar = $this->Sidebar->getMenus();
        $sidebar[5]["items"][1]["is_active"] = true;

        // Data Dummy
        $dataTable = array(
            array(
                'title' => 'Hello World',
                'slug' => 'hello-world',
                'is_visible' => 1,
                'updated_at' => '2020-06-30 18:00:00'
            )
        );

        // Load Lists Posts
        $this->load->view('dashboard/pages-and-posts/lists-posts', array(
            "meta" => $this->meta,
            "navbar" => $this->Navbar->getMenus(),
            "sidebar" => $sidebar,
            "dataTable" => $dataTable
        ));
    }

    public function preview($id)
    {

        // Setup Meta
        $this->meta["title"] = "Admin Leholeh | Pratinjau Pos";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        $sidebar = $this->Sidebar->getMenus();
        $sidebar[5]["items"][1]["is_active"] = true;

        // Load Lists Pages
        $this->load->view('dashboard/pages-and-posts/preview-post', array(
            "meta" => $this->meta,
            "navbar" => $this->Navbar->getMenus(),
            "sidebar" => $sidebar,
            "post_id" => $id
        ));
    }

    public function create()
    {
        // Check Auth
        // $this->checkAuth();
        // Setup Meta
        $this->meta["title"] = "Admin Leholeh | Buat Pos Baru";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        $sidebar = $this->Sidebar->getMenus();
        $sidebar[5]["items"][1]["is_active"] = true;

        // Load Lists Pages
        $this->load->view('dashboard/pages-and-posts/new-post', array(
            "meta" => $this->meta,
            "navbar" => $this->Navbar->getMenus(),
            "sidebar" => $this->Sidebar->getMenus()
        ));
    }

    public function edit($id)
    {
        // Check Auth
        // $this->checkAuth();
        // Setup Meta
        $this->meta["title"] = "Admin Leholeh | Ubah Pos";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        $sidebar = $this->Sidebar->getMenus();
        $sidebar[5]["items"][1]["is_active"] = true;

        // Load Lists Pages
        $this->load->view('dashboard/pages-and-posts/edit-post', array(
            "meta" => $this->meta,
            "navbar" => $this->Navbar->getMenus(),
            "sidebar" => $sidebar,
            "post_id" => $id
        ));
    }

    // ==========================================================
    // BACK END PROCESS
    // ==========================================================

    // Load Lists Posts
    public function load_posts()
    {
        $raw = $this->input->post_get("raw") ?: "";
        $page = 0;
        $search = "";
        $order_by = "m_posts.id";
        $order_direction = "DESC";

        if (!empty($raw)) {
            $raw = json_decode($raw);
            $page = $raw->page;
            $search = $raw->search;
            $order_by = $raw->order_by;
            $order_direction = $raw->order_direction;
        }

        $search = explode(" ", $search);
        $search = implode("%20", $search);

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->createCurl(API_URI .
            "admin/manage/posts?page=$page&search=$search&order-by=$order_by&order-direction=$order_direction");

        print_r($this->custom_curl->__tostring());
    }

    // Load Detail Post
    public function load_detail_post($id)
    {
        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->createCurl(API_URI .
            "admin/manage/posts/$id");

        print_r($this->custom_curl->__tostring());
    }

    // Create Post
    public function create_post()
    {
        $photo = $this->fileUpload->do_upload("cover");
        $title = $this->input->post("title", TRUE) ?: "";
        $slug = $this->input->post("slug", TRUE) ?: "";
        $content = $this->input->post("content") ?: "";
        $tags = $this->input->post("tags", TRUE) ?: "";

        // print_r($photo);
        // die();
        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token,
            'Content-Type: multipart/form-data'
        ));
        $cfile = curl_file_create($photo["file_location"], $_FILES["cover"]["type"], $photo['file_name']);

        $this->custom_curl->setPost(
            array(
                'cover' => $cfile,
                'title' => $title,
                'slug' => $slug,
                'content' => $content,
                "tag" => $tags
            )
        );
        $this->custom_curl->createCurl(API_URI . "admin/manage/posts/");

        print_r($this->custom_curl->__tostring());
    }

    // Delete Post
    public function delete_post($id)
    {
        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->setDelete();
        $this->custom_curl->createCurl(API_URI . "admin/manage/posts/$id");

        print_r($this->custom_curl->__tostring());
    }

    // Edit Post
    public function edit_post($id) {
        
        $title = $this->input->post("title", TRUE) ?: "";
        $slug = $this->input->post("slug", TRUE) ?: "";
        $content = $this->input->post("content") ?: "";
        $tags = $this->input->post("tags", TRUE) ?: "";
        $is_visible = $this->input->post("is_visible", TRUE) ?: "";

        // print_r($photo);
        // die();
        if (isset($_FILES["cover"]["name"])) {
            $photo = $this->fileUpload->do_upload("cover");
            // print_r($photo);
            // die();
            $this->custom_curl->setHeader(array(
                "Authorization: " . $this->auth->token,
                'Content-Type: multipart/form-data'
            ));
            $cfile = curl_file_create($photo["file_location"], $_FILES["cover"]["type"], $photo['file_name']);

            $this->custom_curl->setPost(
                array(
                    'cover' => $cfile,
                    'title' => $title,
                    'slug' => $slug,
                    'content' => $content,
                    "tag" => $tags,
                    "is_visible" => $is_visible
                )
            );

            $this->custom_curl->createCurl(API_URI . "admin/manage/posts/$id");
    
            print_r($this->custom_curl->__tostring()); 

        } else {
            $this->custom_curl->setHeader(array(
                "Authorization: " . $this->auth->token
            ));
    
            $this->custom_curl->setPost(array(
                "title" => $title,
                "slug" => $slug,
                "content" => $content,
                "tag" => $tags,
                "is_visible" => $is_visible
            ));
            $this->custom_curl->createCurl(API_URI . "admin/manage/posts/$id");
    
            print_r($this->custom_curl->__tostring()); 
        }
    }
}
