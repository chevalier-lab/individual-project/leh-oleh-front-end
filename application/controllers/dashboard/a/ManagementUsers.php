<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ManagementUsers extends CI_Controller
{

    // Public Variable
    public $session, $custom_curl, $fileUpload;
    public $meta, $error, $auth;

    public function __construct()
    {
        parent::__construct();

        $this->meta = array(
            "title" => "",
            "description" => "",
            "robots" => ""
        );

        $this->error = array(
            "title" => "Something went wrong",
            "content" => "",
            "details" => array()
        );

        // Load Model
        $this->load->model('Navbar');
        $this->load->model('Sidebar');

        // Load Helper
        $this->session = new Session_helper();
        $this->custom_curl = new Mycurl_helper("");
        $this->fileUpload = new Upload_file_helper(
            array(
                "file_type" => array(
                    "png",
                    "jpg",
                    "jpeg"
                ),
                "max_size"  => 200000000
            )
        );

        // Check Auth
        $this->checkAuth();
    }

    // ==========================================================
    // AUTH PROCESS
    // ==========================================================

    private function checkAuth()
    {
        if (!$this->session->check_session("auth")) {
            redirect(base_url("index.php/general/auth/login"));
        } else {
            $this->auth = $this->session->get_session("auth");
            switch ($this->auth->level) {
                case "admin":
                    break;
                case "toko":
                    redirect(base_url("index.php/dashboard/m/dashboard"));
                    break;
                case "guest":
                    redirect(base_url("index.php/dashboard/g/dashboard"));
                    break;
                case "cs":
                    redirect(base_url("index.php/dashboard/cs/dashboard"));
                    break;
            }
        }
    }

    public function index()
    {
        
        // Setup Meta
        $this->meta["title"] = "Admin Leholeh | Daftar Pengguna";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        $sidebar = $this->Sidebar->getMenus();
        $sidebar[1]["items"][0]["is_active"] = true;

        // Load Lists Users
        $this->load->view('dashboard/users/lists-users', array(
            "meta" => $this->meta,
            "navbar" => $this->Navbar->getMenus(),
            "sidebar" => $sidebar
        ));
    }

    public function listsUsersBanned()
    {
        // Setup Meta
        $this->meta["title"] = "Admin Leholeh | Daftar Pengguna Terblokir";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        $sidebar = $this->Sidebar->getMenus();
        $sidebar[1]["items"][1]["is_active"] = true;

        // Data Dummy
        $dataTable = array(
            array(
                'full_name' => 'Feby',
                'gender' => 'Perempuan',
                'address' => 'Jl. Sukamulya No.71',
                'birth_of_date' => '15 November 2004',
                'email' => 'feby23@gmail.com',
                'phone_number' => '6282119189691',
                'type' => 3
            )
        );

        // Load Lists Users Banned
        $this->load->view('dashboard/users/lists-users-banned', array(
            "meta" => $this->meta,
            "navbar" => $this->Navbar->getMenus(),
            "sidebar" => $sidebar,
        ));
    }

    public function listsJoinMerchant()
    {
        // Setup Meta
        $this->meta["title"] = "Admin Leholeh | Daftar Permintaan Toko";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        $sidebar = $this->Sidebar->getMenus();
        $sidebar[1]["items"][2]["is_active"] = true;

        // Data Dummy
        $dataTable = array(
            array(
                'full_name' => 'Feby',
                'gender' => 'Perempuan',
                'address' => 'Jl. Sukamulya No.71',
                'birth_of_date' => '15 November 2004',
                'email' => 'feby23@gmail.com',
                'phone_number' => '6282119189691',
                'type' => 0
            )
        );

        // Load Lists Join Merchant
        $this->load->view('dashboard/users/lists-join-merchant', array(
            "meta" => $this->meta,
            "navbar" => $this->Navbar->getMenus(),
            "sidebar" => $sidebar,
        ));
    }

    // ==========================================================
    // BACK END PROCESS
    // ==========================================================

    // Load Lists Users
    public function load_users() {
        $raw = $this->input->post_get("raw") ?: "";
        $page = 0;
        $search = "";
        $order_by = "id";
        $order_direction = "DESC";

        if (!empty($raw)) {
            $raw = json_decode($raw);
            $page = $raw->page;
            $search = $raw->search;
            $order_by = $raw->order_by;
            $order_direction = $raw->order_direction;
        } 

        $search = explode(" ", $search);
        $search = implode("%20", $search);

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->createCurl(API_URI . 
            "admin/manage/users?page=$page&search=$search&order-by=$order_by&order-direction=$order_direction");

        print_r($this->custom_curl->__tostring());
    }

    // Load Lists Users
    public function load_banned_users() {
        $raw = $this->input->post_get("raw") ?: "";
        $page = 0;
        $search = "";
        $order_by = "id";
        $order_direction = "DESC";

        if (!empty($raw)) {
            $raw = json_decode($raw);
            $page = $raw->page;
            $search = $raw->search;
            $order_by = $raw->order_by;
            $order_direction = $raw->order_direction;
        } 

        $search = explode(" ", $search);
        $search = implode("%20", $search);

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->createCurl(API_URI . 
            "admin/manage/users/blocked?page=$page&search=$search&order-by=$order_by&order-direction=$order_direction");

        print_r($this->custom_curl->__tostring());
    }

    // Load Lists Users
    public function load_request_users() {
        $raw = $this->input->post_get("raw") ?: "";
        $page = 0;
        $search = "";
        $order_by = "id";
        $order_direction = "DESC";

        if (!empty($raw)) {
            $raw = json_decode($raw);
            $page = $raw->page;
            $search = $raw->search;
            $order_by = $raw->order_by;
            $order_direction = $raw->order_direction;
        } 

        $search = explode(" ", $search);
        $search = implode("%20", $search);

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->createCurl(API_URI . 
            "admin/manage/users/request?page=$page&search=$search&order-by=$order_by&order-direction=$order_direction");

        print_r($this->custom_curl->__tostring());
    }

    // Create User
    public function create_user() {
        $raw = $this->input->post_get("raw", TRUE) ?: "";

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->setPost($raw);
        $this->custom_curl->createCurl(API_URI . "auth/register");

        print_r($this->custom_curl->__tostring()); 
    }

    // Load Detail Users
    public function load_detail_user($id) {

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->createCurl(API_URI . 
            "admin/manage/users/$id");

        print_r($this->custom_curl->__tostring());
    }

    // Create User
    public function change_type_user($id) {
        $raw = $this->input->post_get("raw", TRUE) ?: "";

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->setPut($raw);
        $this->custom_curl->createCurl(API_URI . "admin/manage/users/$id/type");

        print_r($this->custom_curl->__tostring()); 
    }

    // Detail User Request As Merchant
    public function detail_user_request($id) {

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->createCurl(API_URI . "admin/manage/merchant/$id/request");

        print_r($this->custom_curl->__tostring()); 
    }

    // Detail User Request As Merchant
    public function change_status_user_request($id) {
        $raw = $this->input->post_get("raw", TRUE) ?: "";

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->setPut($raw);
        $this->custom_curl->createCurl(API_URI . "admin/manage/merchant/$id/status");

        print_r($this->custom_curl->__tostring()); 
    }

    // Reject User Request As Merchant
    public function reject_reason_user_request() {
        $raw = $this->input->post_get("raw", TRUE) ?: "";

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->setPost($raw);
        $this->custom_curl->createCurl(API_URI . "admin/manage/merchant/reject");

        print_r($this->custom_curl->__tostring()); 
    }
}
