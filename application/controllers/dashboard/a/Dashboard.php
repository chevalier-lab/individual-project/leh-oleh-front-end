<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{

    // Public Variable
    public $session, $custom_curl, $fileUpload;
    public $meta, $error, $auth;

    public function __construct()
    {
        parent::__construct();

        $this->meta = array(
            "title" => "",
            "description" => "",
            "robots" => ""
        );

        $this->error = array(
            "title" => "Something went wrong",
            "content" => "",
            "details" => array()
        );

        // Load Model
        $this->load->model('Navbar');
        $this->load->model('Sidebar');

        // Load Helper
        $this->session = new Session_helper();
        $this->custom_curl = new Mycurl_helper("");
        $this->fileUpload = new Upload_file_helper(
            array(
                "file_type" => array(
                    "png",
                    "jpg",
                    "jpeg"
                ),
                "max_size"  => 200000000
            )
        );

        // Check Auth
        $this->checkAuth();
    }

    // ==========================================================
    // AUTH PROCESS
    // ==========================================================

    private function checkAuth()
    {
        if (!$this->session->check_session("auth")) {
            redirect(base_url("index.php/general/auth/login"));
        } else {
            $this->auth = $this->session->get_session("auth");
            switch ($this->auth->level) {
                case "admin":
                    break;
                case "toko":
                    redirect(base_url("index.php/dashboard/m/dashboard"));
                    break;
                case "guest":
                    redirect(base_url("index.php/dashboard/g/dashboard"));
                    break;
                case "cs":
                    redirect(base_url("index.php/dashboard/cs/dashboard"));
                    break;
            }
        }
    }

    public function index()
    {
        // Setup Meta
        $this->meta["title"] = "Admin Leholeh | Beranda";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";
        
        $sidebar = $this->Sidebar->getMenus();
        $sidebar[0]["items"][0]["is_active"] = true;

        // Load Home
        $this->load->view('dashboard/administrator/home', array(
            "meta" => $this->meta,
            "navbar" => $this->Navbar->getMenus(),
            "sidebar" => $sidebar
        ));
    }

    public function logActivity()
    {
        $this->meta["title"] = "Admin Leholeh | Catatan Aktivitas";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        $sidebar = $this->Sidebar->getMenus();
        $sidebar[0]["items"][1]["is_active"] = true;

        // Load Log Activity
        $this->load->view('dashboard/administrator/log-activity', array(
            "meta" => $this->meta,
            "navbar" => $this->Navbar->getMenus(),
            "sidebar" => $sidebar
        ));
    }

    public function chatCS()
    {
        $this->meta["title"] = "Admin Leholeh | Layanan Pelanggan";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        $sidebar = $this->Sidebar->getMenus();
        $sidebar[0]["items"][2]["is_active"] = true;

        // Load Chat CS
        $this->load->view('dashboard/administrator/chat-cs', array(
            "meta" => $this->meta,
            "navbar" => $this->Navbar->getMenus(),
            "sidebar" => $sidebar
        ));
    }

    public function detailRoomChat($idRoom)
    {
        $this->meta["title"] = "Admin Leholeh | Ruang Obrolan";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        $sidebar = $this->Sidebar->getMenus();
        $sidebar[0]["items"][2]["is_active"] = true;

        // Load Chat CS
        $this->load->view('dashboard/administrator/chat-room', array(
            "meta" => $this->meta,
            "navbar" => $this->Navbar->getMenus(),
            "sidebar" => $sidebar,
            "idRoom" => $idRoom
        ));
    }

    // ==========================================================
    // BACK END PROCESS
    // ==========================================================

    // Load Income
    public function income($type) {

        $date = date("Y-m-d");
        switch($type) {
            case 2: $date = date("Y-m"); break;
            case 3: $date = date("Y"); break;
        }

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->createCurl(API_URI . 
            "admin/dashboard/income/harian?date=$date");

        print_r($this->custom_curl->__tostring());
    }

    // Load Log
    public function load_log() {
        $raw = $this->input->post_get("raw") ?: "";
        $page = 0;
        $search = "";
        $order_by = "id";
        $order_direction = "DESC";

        if (!empty($raw)) {
            $raw = json_decode($raw);
            $page = $raw->page;
            $search = $raw->search;
            $order_by = $raw->order_by;
            $order_direction = $raw->order_direction;
        } 

        $search = explode(" ", $search);
        $search = implode("%20", $search);

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->createCurl(API_URI . 
            "admin/dashboard/last-log?page=$page&search=$search&order-by=$order_by&order-direction=$order_direction");

        print_r($this->custom_curl->__tostring());
    }
}
