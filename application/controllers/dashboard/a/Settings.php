<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Settings extends CI_Controller
{

    // Public Variable
    public $session, $custom_curl, $fileUpload;
    public $meta, $error, $auth;

    public function __construct()
    {
        parent::__construct();

        $this->meta = array(
            "title" => "",
            "description" => "",
            "robots" => ""
        );

        $this->error = array(
            "title" => "Something went wrong",
            "content" => "",
            "details" => array()
        );

        // Load Model
        $this->load->model('Navbar');
        $this->load->model('Sidebar');

        // Load Helper
        $this->session = new Session_helper();
        $this->custom_curl = new Mycurl_helper("");
        $this->fileUpload = new Upload_file_helper(
            array(
                "file_type" => array(
                    "png",
                    "jpg",
                    "jpeg"
                ),
                "max_size"  => 200000000
            )
        );

        // Check Auth
        $this->checkAuth();
    }

    // ==========================================================
    // AUTH PROCESS
    // ==========================================================

    private function checkAuth()
    {
        if (!$this->session->check_session("auth")) {
            redirect(base_url("index.php/general/auth/login"));
        } else {
            $this->auth = $this->session->get_session("auth");
            switch ($this->auth->level) {
                case "admin":
                    break;
                case "toko":
                    redirect(base_url("index.php/dashboard/m/dashboard"));
                    break;
                case "guest":
                    redirect(base_url("index.php/dashboard/g/dashboard"));
                    break;
                case "cs":
                    redirect(base_url("index.php/dashboard/cs/dashboard"));
                    break;
            }
        }
    }

    public function index()
    {
        // Check Auth
        // $this->checkAuth();
        // Setup Meta
        $this->meta["title"] = "Admin Leholeh | Umum";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        $sidebar = $this->Sidebar->getMenus();
        $sidebar[7]["items"][0]["is_active"] = true;

        // Load General
        $this->load->view('dashboard/settings/general', array(
            "meta" => $this->meta,
            "navbar" => $this->Navbar->getMenus(),
            "sidebar" => $sidebar
        ));
    }

    public function branch()
    {
        // Setup Meta
        $this->meta["title"] = "Admin Leholeh | Cabang";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        $sidebar = $this->Sidebar->getMenus();
        $sidebar[7]["items"][1]["is_active"] = true;

        // Load Branch
        $this->load->view('dashboard/settings/branch', array(
            "meta" => $this->meta,
            "navbar" => $this->Navbar->getMenus(),
            "sidebar" => $sidebar
        ));
    }

    public function socialMedia()
    {
        // Setup Meta
        $this->meta["title"] = "Admin Leholeh | Sosial Media";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        $sidebar = $this->Sidebar->getMenus();
        $sidebar[7]["items"][2]["is_active"] = true;

        // Data Dummy
        $dataTable = array(
            'fab fa-facebook', 'fab fa-twitter '
        );

        // Load Social Media
        $this->load->view('dashboard/settings/social-media', array(
            "meta" => $this->meta,
            "navbar" => $this->Navbar->getMenus(),
            "sidebar" => $sidebar,
            "dataTable" => $dataTable
        ));
    }

    public function profile()
    {
        // Setup Meta
        $this->meta["title"] = "Admin Leholeh | Profil";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        $sidebar = $this->Sidebar->getMenus();
        $sidebar[7]["items"][3]["is_active"] = true;

        // Load Profile
        $this->load->view('dashboard/settings/profile', array(
            "meta" => $this->meta,
            "navbar" => $this->Navbar->getMenus(),
            "sidebar" => $sidebar
        ));
    }

    // ==========================================================
    // BACK-END PROCESS
    // ==========================================================

    // Load General
    public function load_general()
    {
        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->createCurl(API_URI .
            "admin/setting/general");

        print_r($this->custom_curl->__tostring());
    }

    // Load Profile
    public function load_profile()
    {
        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->createCurl(API_URI .
            "admin/setting/profile");

        print_r($this->custom_curl->__tostring());
    }

    // Load Sosmed
    public function load_sosmed()
    {
        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->createCurl(API_URI .
            "admin/setting/sosmed");

        print_r($this->custom_curl->__tostring());
    }

    // Create Sosmed
    public function create_sosmed()
    {
        $raw = $this->input->post("raw") ?: "";
        if (empty($raw)) die();

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->setPost($raw);
        $this->custom_curl->createCurl(API_URI .
            "admin/setting/sosmed");

        print_r($this->custom_curl->__tostring());
    }

    // Update Sosmed
    public function update_sosmed($id)
    {
        $raw = $this->input->post("raw") ?: "";
        if (empty($raw)) die();

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->setPut($raw);
        $this->custom_curl->createCurl(API_URI .
            "admin/setting/sosmed/$id");

        print_r($this->custom_curl->__tostring());
    }

    // Delete Sosmed
    public function delete_sosmed($id)
    {
        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->setDelete();
        $this->custom_curl->createCurl(API_URI .
            "admin/setting/sosmed/$id");

        print_r($this->custom_curl->__tostring());
    }

    // Update General
    public function update_general()
    {

        $raw = $this->input->post("raw") ?: "";
        if (empty($raw)) die();

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->setPut($raw);
        $this->custom_curl->createCurl(API_URI .
            "admin/setting/general");

        print_r($this->custom_curl->__tostring());
    }

    // Update Profile
    public function update_profile()
    {

        $raw = $this->input->post("raw") ?: "";
        if (empty($raw)) die();

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->setPut($raw);
        $this->custom_curl->createCurl(API_URI .
            "admin/setting/profile");

        print_r($this->custom_curl->__tostring());
    }

    // Block Profile
    public function block_profile()
    {

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->setPut(null);
        $this->custom_curl->createCurl(API_URI .
            "admin/setting/block-user");

        print_r($this->custom_curl->__tostring());
    }

    // Edit Logo
    public function edit_logo() {
        $photo = $this->fileUpload->do_upload("logo");

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token,
            'Content-Type: multipart/form-data'
        ));
        $cfile = curl_file_create($photo["file_location"],$_FILES["logo"]["type"],$photo['file_name']);

        $this->custom_curl->setPost(
            array(
                'logo' => $cfile
            )
        );
        $this->custom_curl->createCurl(API_URI . "admin/setting/general");

        print_r($this->custom_curl->__tostring()); 
    }

    // Edit Photo
    public function edit_photo() {
        $photo = $this->fileUpload->do_upload("photo");

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token,
            'Content-Type: multipart/form-data'
        ));
        $cfile = curl_file_create($photo["file_location"],$_FILES["photo"]["type"],$photo['file_name']);

        $this->custom_curl->setPost(
            array(
                'photo' => $cfile
            )
        );
        $this->custom_curl->createCurl(API_URI . "admin/setting/profile-picture");

        print_r($this->custom_curl->__tostring()); 
    }
}
