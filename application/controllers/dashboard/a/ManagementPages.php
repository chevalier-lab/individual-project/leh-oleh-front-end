<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ManagementPages extends CI_Controller
{

    // Public Variable
    public $session, $custom_curl, $fileUpload;
    public $meta, $error, $auth;

    public function __construct()
    {
        parent::__construct();

        $this->meta = array(
            "title" => "",
            "description" => "",
            "robots" => ""
        );

        $this->error = array(
            "title" => "Something went wrong",
            "content" => "",
            "details" => array()
        );

        // Load Model
        $this->load->model('Navbar');
        $this->load->model('Sidebar');

        // Load Helper
        $this->session = new Session_helper();
        $this->custom_curl = new Mycurl_helper("");
        $this->fileUpload = new Upload_file_helper(
            array(
                "file_type" => array(
                    "png",
                    "jpg",
                    "jpeg"
                ),
                "max_size"  => 200000000
            )
        );

        // Check Auth
        $this->checkAuth();
    }

    // ==========================================================
    // AUTH PROCESS
    // ==========================================================

    private function checkAuth()
    {
        if (!$this->session->check_session("auth")) {
            redirect(base_url("index.php/general/auth/login"));
        } else {
            $this->auth = $this->session->get_session("auth");
            switch ($this->auth->level) {
                case "admin":
                    break;
                case "toko":
                    redirect(base_url("index.php/dashboard/m/dashboard"));
                    break;
                case "guest":
                    redirect(base_url("index.php/dashboard/g/dashboard"));
                    break;
                case "cs":
                    redirect(base_url("index.php/dashboard/cs/dashboard"));
                    break;
            }
        }
    }

    public function index()
    {
        // Setup Meta
        $this->meta["title"] = "Admin Leholeh | Daftar Halaman";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        $sidebar = $this->Sidebar->getMenus();
        $sidebar[5]["items"][0]["is_active"] = true;

        // Data Dummy
        $dataTable = array(
            array(
                'title' => 'Hello World',
                'slug' => 'hello-world',
                'is_visible' => 1,
                'updated_at' => '2020-06-30 18:00:00'
            )
        );

        // Load Lists Pages
        $this->load->view('dashboard/pages-and-posts/lists-pages', array(
            "meta" => $this->meta,
            "navbar" => $this->Navbar->getMenus(),
            "sidebar" => $sidebar,
            "dataTable" => $dataTable
        ));
    }

    public function preview($id)
    {
        // Setup Meta
        $this->meta["title"] = "Admin Leholeh | Pratinjau Halaman";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        $sidebar = $this->Sidebar->getMenus();
        $sidebar[5]["items"][0]["is_active"] = true;

        // print_r($id);
        // die();

        // Load Lists Pages
        $this->load->view('dashboard/pages-and-posts/preview-page', array(
            "meta" => $this->meta,
            "navbar" => $this->Navbar->getMenus(),
            "sidebar" => $sidebar,
            "page_id" => $id
        ));
    }

    public function create()
    {
        // Check Auth
        // $this->checkAuth();
        // Setup Meta
        $this->meta["title"] = "Admin Leholeh | Buat Halaman Baru";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        $sidebar = $this->Sidebar->getMenus();
        $sidebar[5]["items"][0]["is_active"] = true;

        // Load Lists Pages
        $this->load->view('dashboard/pages-and-posts/new-page', array(
            "meta" => $this->meta,
            "navbar" => $this->Navbar->getMenus(),
            "sidebar" => $this->Sidebar->getMenus()
        ));
    }

    public function edit($id)
    {
        // Check Auth
        // $this->checkAuth();
        // Setup Meta
        $this->meta["title"] = "Admin Leholeh | Ubah Halaman";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        $sidebar = $this->Sidebar->getMenus();
        $sidebar[5]["items"][0]["is_active"] = true;

        // Load Lists Pages
        $this->load->view('dashboard/pages-and-posts/edit-page', array(
            "meta" => $this->meta,
            "navbar" => $this->Navbar->getMenus(),
            "sidebar" => $sidebar,
            "page_id" => $id
        ));
    }

    // ==========================================================
    // BACK END PROCESS
    // ==========================================================

    // Load Lists Pages
    public function load_pages() {
        $raw = $this->input->post_get("raw") ?: "";
        $page = 0;
        $search = "";
        $order_by = "m_pages.id";
        $order_direction = "DESC";

        if (!empty($raw)) {
            $raw = json_decode($raw);
            $page = $raw->page;
            $search = $raw->search;
            $order_by = $raw->order_by;
            $order_direction = $raw->order_direction;
        } 

        $search = explode(" ", $search);
        $search = implode("%20", $search);

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->createCurl(API_URI . 
            "admin/manage/pages?page=$page&search=$search&order-by=$order_by&order-direction=$order_direction");

        print_r($this->custom_curl->__tostring());
    }

    // Load Detail Page
    public function load_detail_page($id) {
        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->createCurl(API_URI . 
            "admin/manage/pages/$id");

        print_r($this->custom_curl->__tostring());
    }

    // Create Page
    public function create_page() {
        $photo = $this->fileUpload->do_upload("cover");
        $title = $this->input->post("title", TRUE) ?: "";
        $slug = $this->input->post("slug", TRUE) ?: "";
        $content = $this->input->post("content") ?: "";
        $tags = $this->input->post("tags", TRUE) ?: "";

        // print_r($photo);
        // die();
        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token,
            'Content-Type: multipart/form-data'
        ));
        $cfile = curl_file_create($photo["file_location"],$_FILES["cover"]["type"],$photo['file_name']);

        $this->custom_curl->setPost(
            array(
                'cover' => $cfile,
                'title' => $title,
                'slug' => $slug,
                'content' => $content,
                "tags" => $tags
            )
        );
        $this->custom_curl->createCurl(API_URI . "admin/manage/pages");

        print_r($this->custom_curl->__tostring()); 
    }

    // Create Page Tag
    public function create_page_tag() {
        $raw = $this->input->post_get("raw", TRUE) ?: "";

        // print_r($photo);
        // die();
        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->setPost($raw);
        $this->custom_curl->createCurl(API_URI . "admin/manage/pages/tag");

        print_r($this->custom_curl->__tostring()); 
    }

    // Delete Page
    public function delete_page($id) {
        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->setDelete();
        $this->custom_curl->createCurl(API_URI . "admin/manage/pages/$id");

        print_r($this->custom_curl->__tostring()); 
    }

    // Delete Page
    public function delete_page_tag($id) {
        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->setDelete();
        $this->custom_curl->createCurl(API_URI . "admin/manage/pages/tag/$id");

        print_r($this->custom_curl->__tostring()); 
    }

    // Edit Cover
    public function edit_cover($id) {
        $photo = $this->fileUpload->do_upload("cover");

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token,
            'Content-Type: multipart/form-data'
        ));
        $cfile = curl_file_create($photo["file_location"],$_FILES["cover"]["type"],$photo['file_name']);

        $this->custom_curl->setPost(
            array(
                'cover' => $cfile
            )
        );
        $this->custom_curl->createCurl(API_URI . "admin/manage/pages/$id/cover");

        print_r($this->custom_curl->__tostring()); 
    }

    // Edit Page
    public function edit_page($id) {
        $raw = $this->input->post_get("raw", TRUE) ?: "";

        // print_r($photo);
        // die();
        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->setPut($raw);
        $this->custom_curl->createCurl(API_URI . "admin/manage/pages/$id");

        print_r($this->custom_curl->__tostring()); 
    }

}
