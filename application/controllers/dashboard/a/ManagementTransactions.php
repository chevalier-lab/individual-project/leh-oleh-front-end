<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ManagementTransactions extends CI_Controller
{

    // Public Variable
    public $session, $custom_curl, $fileUpload;
    public $meta, $error, $auth;

    public function __construct()
    {
        parent::__construct();

        $this->meta = array(
            "title" => "",
            "description" => "",
            "robots" => ""
        );

        $this->error = array(
            "title" => "Something went wrong",
            "content" => "",
            "details" => array()
        );

        // Load Model
        $this->load->model('Navbar');
        $this->load->model('Sidebar');

        // Load Helper
        $this->session = new Session_helper();
        $this->custom_curl = new Mycurl_helper("");
        $this->fileUpload = new Upload_file_helper(
            array(
                "file_type" => array(
                    "png",
                    "jpg",
                    "jpeg"
                ),
                "max_size"  => 200000000
            )
        );

        // Check Auth
        $this->checkAuth();
    }

    // ==========================================================
    // AUTH PROCESS
    // ==========================================================

    private function checkAuth()
    {
        if (!$this->session->check_session("auth")) {
            redirect(base_url("index.php/general/auth/login"));
        } else {
            $this->auth = $this->session->get_session("auth");
            switch ($this->auth->level) {
                case "admin":
                    break;
                case "toko":
                    redirect(base_url("index.php/dashboard/m/dashboard"));
                    break;
                case "guest":
                    redirect(base_url("index.php/dashboard/g/dashboard"));
                    break;
                case "cs":
                    redirect(base_url("index.php/dashboard/cs/dashboard"));
                    break;
            }
        }
    }

    public function index()
    {
        // $this->checkAuth();
        // Setup Meta
        $this->meta["title"] = "Admin Leholeh | Daftar Transaksi";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        $sidebar = $this->Sidebar->getMenus();
        $sidebar[3]["items"][0]["is_active"] = true;

        // Load Lists Transactions
        $this->load->view('dashboard/transactions/lists-transaction', array(
            "meta" => $this->meta,
            "navbar" => $this->Navbar->getMenus(),
            "sidebar" => $sidebar,
        ));
    }

    public function listsReview()
    {
        // $this->checkAuth();
        // Setup Meta
        $this->meta["title"] = "Admin Leholeh | Daftar Ulasan";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        $sidebar = $this->Sidebar->getMenus();
        $sidebar[3]["items"][1]["is_active"] = true;
        
        // Load Lists Review
        $this->load->view('dashboard/transactions/lists-review', array(
            "meta" => $this->meta,
            "navbar" => $this->Navbar->getMenus(),
            "sidebar" => $sidebar,
        ));
    }

    public function listsComplain()
    {
        // $this->checkAuth();
        // Setup Meta
        $this->meta["title"] = "Admin Leholeh | Daftar Komplain";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        $sidebar = $this->Sidebar->getMenus();
        $sidebar[3]["items"][2]["is_active"] = true;

        // Load Lists Complain
        $this->load->view('dashboard/transactions/lists-complain', array(
            "meta" => $this->meta,
            "navbar" => $this->Navbar->getMenus(),
            "sidebar" => $sidebar
        ));
    }

    public function listsRefund()
    {
        // $this->checkAuth();
        // Setup Meta
        $this->meta["title"] = "Admin Leholeh | Daftar Pembatalan Transaksi";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        $sidebar = $this->Sidebar->getMenus();
        $sidebar[3]["items"][3]["is_active"] = true;

        // Load Lists Complain
        $this->load->view('dashboard/transactions/lists-refund', array(
            "meta" => $this->meta,
            "navbar" => $this->Navbar->getMenus(),
            "sidebar" => $sidebar
        ));
    }

    // ==========================================================
    // BACK END PROCESS
    // ==========================================================

    // Load Lists Transaction
    public function load_transaction() {
        $raw = $this->input->post_get("raw") ?: "";
        $page = 0;
        $search = "";
        $order_by = "id";
        $order_direction = "DESC";

        if (!empty($raw)) {
            $raw = json_decode($raw);
            $page = $raw->page;
            $search = $raw->search;
            $order_by = $raw->order_by;
            $order_direction = $raw->order_direction;
        } 

        $search = explode(" ", $search);
        $search = implode("%20", $search);

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->createCurl(API_URI . 
            "admin/manage/transaction?page=$page&search=$search&order-by=$order_by&order-direction=$order_direction");

        print_r($this->custom_curl->__tostring());
    }

    // Load Detail Transaction
    public function detail_transaction($id) {

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->createCurl(API_URI . 
            "admin/manage/transaction/$id");

        print_r($this->custom_curl->__tostring());
    }

    // Load Review Transaction
    public function review_transaction() {

        $raw = $this->input->post_get("raw") ?: "";
        $page = 0;
        $search = "";
        $order_by = "id";
        $order_direction = "DESC";

        if (!empty($raw)) {
            $raw = json_decode($raw);
            $page = $raw->page;
            $search = $raw->search;
            $order_by = $raw->order_by;
            $order_direction = $raw->order_direction;
        } 

        $search = explode(" ", $search);
        $search = implode("%20", $search);

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->createCurl(API_URI . 
            "admin/manage/transaction/review?page=$page&search=$search&order-by=$order_by&order-direction=$order_direction");

        print_r($this->custom_curl->__tostring());
    }

    // Load Complain Transaction
    public function complain_transaction() {

        $raw = $this->input->post_get("raw") ?: "";
        $page = 0;
        $search = "";
        $order_by = "id";
        $order_direction = "DESC";

        if (!empty($raw)) {
            $raw = json_decode($raw);
            $page = $raw->page;
            $search = $raw->search;
            $order_by = $raw->order_by;
            $order_direction = $raw->order_direction;
        } 

        $search = explode(" ", $search);
        $search = implode("%20", $search);

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->createCurl(API_URI . 
            "admin/manage/transaction/complain?page=$page&search=$search&order-by=$order_by&order-direction=$order_direction");

        print_r($this->custom_curl->__tostring());
    }

    // Update Status Transaction
    public function update_status_transaction($id) {
        $raw = $this->input->post_get("raw") ?: "";

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->setPut($raw);

        $this->custom_curl->createCurl(API_URI . 
            "/admin/manage/transaction/$id");

        print_r($this->custom_curl->__tostring());
    }

    // Update Status Product
    public function update_status_product($id) {
        $raw = $this->input->post_get("raw") ?: "";

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->setPut($raw);

        $this->custom_curl->createCurl(API_URI . 
            "/admin/manage/transaction/product/$id");

        print_r($this->custom_curl->__tostring());
    }

    // Load Refund Transaction
    public function load_refund_transaction() {

        $raw = $this->input->post_get("raw") ?: "";
        $page = 0;
        $order_by = "id";
        $order_direction = "DESC";
        $trx_id = "";
        $full_name = "";

        if (!empty($raw)) {
            $raw = json_decode($raw);
            $page = $raw->page;
            $trx_id = $raw->trx_id;
            $full_name = $raw->full_name;
            $order_by = $raw->order_by;
            $order_direction = $raw->order_direction;
        } 

        $full_name = explode(" ", $full_name);
        $full_name = implode("%20", $full_name);

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->createCurl(API_URI . 
            "admin/manage/transaction/refund?page=$page&order-by=$order_by&order-direction=$order_direction&trx-id=$trx_id&full_name=$full_name");

        print_r($this->custom_curl->__tostring());
    }

    public function refund_transaction()
    {
        $raw = $this->input->post_get("raw") ?: "";

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token,
        ));

        $this->custom_curl->setPost($raw);
        $this->custom_curl->createCurl(API_URI . "admin/manage/transaction/refund/change-status");

        print_r($this->custom_curl->__tostring());
    }
}
