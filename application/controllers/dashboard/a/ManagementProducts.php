<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ManagementProducts extends CI_Controller
{

    // Public Variable
    public $session, $custom_curl, $fileUpload;
    public $meta, $error, $auth;

    public function __construct()
    {
        parent::__construct();

        $this->meta = array(
            "title" => "",
            "description" => "",
            "robots" => ""
        );

        $this->error = array(
            "title" => "Something went wrong",
            "content" => "",
            "details" => array()
        );

        // Load Model
        $this->load->model('Navbar');
        $this->load->model('Sidebar');

        // Load Helper
        $this->session = new Session_helper();
        $this->custom_curl = new Mycurl_helper("");
        $this->fileUpload = new Upload_file_helper(
            array(
                "file_type" => array(
                    "png",
                    "jpg",
                    "jpeg"
                ),
                "max_size"  => 200000000
            )
        );

        // Check Auth
        $this->checkAuth();
    }

    // ==========================================================
    // AUTH PROCESS
    // ==========================================================

    private function checkAuth()
    {
        if (!$this->session->check_session("auth")) {
            redirect(base_url("index.php/general/auth/login"));
        } else {
            $this->auth = $this->session->get_session("auth");
            switch ($this->auth->level) {
                case "admin":
                    break;
                case "toko":
                    redirect(base_url("index.php/dashboard/m/dashboard"));
                    break;
                case "guest":
                    redirect(base_url("index.php/dashboard/g/dashboard"));
                    break;
                case "cs":
                    redirect(base_url("index.php/dashboard/cs/dashboard"));
                    break;
            }
        }
    }

    public function index()
    {
        // Check Auth
        // $this->checkAuth();
        // Setup Meta
        $this->meta["title"] = "Admin Leholeh | Daftar Produk";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        $sidebar = $this->Sidebar->getMenus();
        $sidebar[2]["items"][1]["is_active"] = true;

        // Load Lists Products
        $this->load->view('dashboard/products/lists-products', array(
            "meta" => $this->meta,
            "navbar" => $this->Navbar->getMenus(),
            "sidebar" => $sidebar,
        ));
    }

    public function listsMerchant()
    {
        // Setup Meta
        $this->meta["title"] = "Admin Leholeh | Daftar Toko";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        $sidebar = $this->Sidebar->getMenus();
        $sidebar[2]["items"][0]["is_active"] = true;

        // Load Lists Merchant
        $this->load->view('dashboard/products/lists-merchant', array(
            "meta" => $this->meta,
            "navbar" => $this->Navbar->getMenus(),
            "sidebar" => $sidebar,
        ));
    }

    public function listProductMerchant($id)
    {
        $this->meta["title"] = "Admin Leholeh | Daftar Produk Toko";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        $sidebar = $this->Sidebar->getMenus();
        $sidebar[2]["items"][0]["is_active"] = true;

        // Load Lists Products Merchant
        $this->load->view('dashboard/products/lists-products-merchant', array(
            "meta" => $this->meta,
            "navbar" => $this->Navbar->getMenus(),
            "sidebar" => $sidebar,
            "id_merchant" => $id
        ));
    }

    public function listsNewProducts()
    {
        // Setup Meta
        $this->meta["title"] = "Admin Leholeh | Daftar Produk Baru";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        $sidebar = $this->Sidebar->getMenus();
        $sidebar[2]["items"][2]["is_active"] = true;

        // Load Lists New Products
        $this->load->view('dashboard/products/lists-new-products', array(
            "meta" => $this->meta,
            "navbar" => $this->Navbar->getMenus(),
            "sidebar" => $sidebar,
        ));
    }

    // ==========================================================
    // BACK END PROCESS
    // ==========================================================

    // Load Lists Merchant
    public function load_merchant() {
        $raw = $this->input->post_get("raw") ?: "";
        $page = 0;
        $search = "";
        $order_by = "id";
        $order_direction = "DESC";

        if (!empty($raw)) {
            $raw = json_decode($raw);
            $page = $raw->page;
            $search = $raw->search;
            $order_by = $raw->order_by;
            $order_direction = $raw->order_direction;
        } 

        $search = explode(" ", $search);
        $search = implode("%20", $search);

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->createCurl(API_URI . 
            "admin/manage/merchants?page=$page&search=$search&order-by=$order_by&order-direction=$order_direction");

        print_r($this->custom_curl->__tostring());
    }

    // Load Lists Merchant Product
    public function load_merchant_product($id) {
        $raw = $this->input->post_get("raw") ?: "";
        $page = 0;
        $search = "";
        $order_by = "id";
        $order_direction = "DESC";

        if (!empty($raw)) {
            $raw = json_decode($raw);
            $page = $raw->page;
            $search = $raw->search;
            $order_by = $raw->order_by;
            $order_direction = $raw->order_direction;
        }

        $search = explode(" ", $search);
        $search = implode("%20", $search);

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->createCurl(API_URI . 
            "guest/merchant/$id/products?page=$page&search=$search&order-by=$order_by&order-direction=$order_direction");

        print_r($this->custom_curl->__tostring());
    }

    // Load Detail Merchant
    public function load_detail_merchant($id) {

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->createCurl(API_URI . 
            "guest/merchant/$id");

        print_r($this->custom_curl->__tostring());
    }

    // Load Lists Product Validate
    public function load_product_validate() {
        $raw = $this->input->post_get("raw") ?: "";
        $page = 0;
        $search = "";
        $order_by = "id";
        $order_direction = "DESC";

        if (!empty($raw)) {
            $raw = json_decode($raw);
            $page = $raw->page;
            $search = $raw->search;
            $order_by = $raw->order_by;
            $order_direction = $raw->order_direction;
        } 

        $search = explode(" ", $search);
        $search = implode("%20", $search);

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->createCurl(API_URI .
            "admin/manage/products-validated?page=$page&search=$search&order-by=$order_by&order-direction=$order_direction");

        print_r($this->custom_curl->__tostring());
    }

    // Load Lists Product Pending
    public function load_product_pending() {
        $raw = $this->input->post_get("raw") ?: "";
        $page = 0;
        $search = "";
        $order_by = "id";
        $order_direction = "DESC";

        if (!empty($raw)) {
            $raw = json_decode($raw);
            $page = $raw->page;
            $search = $raw->search;
            $order_by = $raw->order_by;
            $order_direction = $raw->order_direction;
        } 

        $search = explode(" ", $search);
        $search = implode("%20", $search);

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->createCurl(API_URI . 
            "admin/manage/products-pending?page=$page&search=$search&order-by=$order_by&order-direction=$order_direction");

        print_r($this->custom_curl->__tostring());
    }

    // Remove Product
    public function remove_product($id)
    {
        $raw = $this->input->post_get("raw", TRUE) ?: "";

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token,
        ));

        $this->custom_curl->setPut($raw);
        $this->custom_curl->createCurl(API_URI . "merchant/manage/products/$id/remove");

        print_r($this->custom_curl->__tostring());
    }

    // Change Status Product
    public function change_status_product()
    {
        $raw = $this->input->post_get("raw", TRUE) ?: "";

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token,
        ));

        $this->custom_curl->setPut($raw);
        $this->custom_curl->createCurl(API_URI . "admin/manage/products/");

        print_r($this->custom_curl->__tostring());
    }

    // Load Detail Product
    public function load_detail_product($id)
    {   

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token,
        ));
        
        $this->custom_curl->createCurl(API_URI .
            "guest/products/$id");

        print_r($this->custom_curl->__tostring());
    }
}
