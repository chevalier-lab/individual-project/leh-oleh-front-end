<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{

    // Public Variable
    public $session, $custom_curl, $fileUpload;
    public $meta, $error, $auth;

    public function __construct()
    {
        parent::__construct();

        $this->meta = array(
            "title" => "",
            "description" => "",
            "robots" => ""
        );

        $this->error = array(
            "title" => "Something went wrong",
            "content" => "",
            "details" => array()
        );

        // Load Model
        $this->load->model('Navbar');
        $this->load->model('SidebarGuest');

        // Load Helper
        $this->session = new Session_helper();
        $this->custom_curl = new Mycurl_helper("");
        $this->fileUpload = new Upload_file_helper(
            array(
                "file_type" => array(
                    "png",
                    "jpg",
                    "jpeg"
                ),
                "max_size"  => 200000000
            )
        );

        // Check Auth
        $this->checkAuth();
    }

    // ==========================================================
    // AUTH PROCESS
    // ==========================================================

    private function checkAuth()
    {
        if (!$this->session->check_session("auth")) {
            redirect(base_url("index.php/general/auth/login"));
        } else {
            $this->auth = $this->session->get_session("auth");
            switch ($this->auth->level) {
                case "admin":
                    redirect(base_url("index.php/dashboard/a/dashboard"));
                    break;
                case "toko":
                    redirect(base_url("index.php/dashboard/m/dashboard"));
                    break;
                case "guest":
                    break;
                case "cs":
                    redirect(base_url("index.php/dashboard/cs/dashboard"));
                    break;
            }
        }
    }

    public function index()
    {
        // Check Auth
        // $this->checkAuth();
        // Setup Meta
        $this->meta["title"] = "Guest Leholeh | Beranda";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";
        
        $sidebar = $this->SidebarGuest->getMenus();
        $sidebar[0]["items"][0]["is_active"] = true;

        // Load Home
        $this->load->view('dashboard-guest/dashboard/home', array(
            "meta" => $this->meta,
            "navbar" => $this->Navbar->getMenus(),
            "sidebar" => $sidebar
        ));
    }

    public function requestMerchant()
    {
        // Check Auth
        // $this->checkAuth();
        // Setup Meta
        $this->meta["title"] = "Guest Leholeh | Permintaan Toko";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";
        
        $sidebar = $this->SidebarGuest->getMenus();
        $sidebar[0]["items"][1]["is_active"] = true;

        // Load Home
        $this->load->view('dashboard-guest/dashboard/request_merchant', array(
            "meta" => $this->meta,
            "navbar" => $this->Navbar->getMenus(),
            "sidebar" => $sidebar
        ));
    }

    public function logActivity()
    {
        $this->meta["title"] = "Guest Leholeh | Catatan Aktivitas";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";
        
        $sidebar = $this->SidebarGuest->getMenus();
        $sidebar[0]["items"][0]["is_active"] = true;

        // Load Home
        $this->load->view('dashboard-guest/dashboard/log-activity', array(
            "meta" => $this->meta,
            "navbar" => $this->Navbar->getMenus(),
            "sidebar" => $sidebar
        ));
    }

    // ==========================================================
    // BACK-END PROCESS
    // ==========================================================

    // Load Log
    public function load_log() {
        $raw = $this->input->post_get("raw") ?: "";
        $page = 0;
        $search = "";
        $order_by = "id";
        $order_direction = "DESC";

        if (!empty($raw)) {
            $raw = json_decode($raw);
            $page = $raw->page;
            $search = $raw->search;
            $order_by = $raw->order_by;
            $order_direction = $raw->order_direction;
        } 

        $search = explode(" ", $search);
        $search = implode("%20", $search);

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->createCurl(API_URI . 
            "admin/dashboard/last-log?page=$page&search=$search&order-by=$order_by&order-direction=$order_direction");

        print_r($this->custom_curl->__tostring());
    }

    // Request Join Merchant
    public function create_join_merchant()
    {
        $foto_ktp = $this->fileUpload->do_upload("foto_ktp");
        $foto_pengguna = $this->fileUpload->do_upload("foto_pengguna");
        $foto_toko = $this->fileUpload->do_upload("foto_toko");
        $no_ktp = $this->input->post("no_ktp", TRUE) ?: "";
        $alamat = $this->input->post("alamat", TRUE) ?: "";
        $no_hp = $this->input->post("no_hp", TRUE) ?: "";
        $nama_toko = $this->input->post("nama_toko", TRUE) ?: "";
        // print_r($photo);
        // die();
        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token,
            'Content-Type: multipart/form-data'
        ));

        $foto_ktp = curl_file_create($foto_ktp["file_location"], $_FILES["foto_ktp"]["type"], $foto_ktp['file_name']);
        $foto_pengguna = curl_file_create($foto_pengguna["file_location"], $_FILES["foto_pengguna"]["type"], $foto_pengguna['file_name']);
        $foto_toko = curl_file_create($foto_toko["file_location"], $_FILES["foto_toko"]["type"], $foto_toko['file_name']);

        $this->custom_curl->setPost(
            array(
                'foto_ktp' => $foto_ktp,
                'foto_pengguna' => $foto_pengguna,
                'foto_toko' => $foto_toko,
                'no_ktp' => $no_ktp,
                'alamat' => $alamat,
                'no_hp' => $no_hp,
                'nama_toko' => $nama_toko
            )
        );
        $this->custom_curl->createCurl(API_URI . "guest/merchant/request");

        print_r($this->custom_curl->__tostring());
    }

    // Load Status Merchant
    public function load_status_merchant() {

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->createCurl(API_URI . 
            "guest/merchant/status");

        print_r($this->custom_curl->__tostring());
    }

    // Delete Request Merchant
    public function delete_request_merchant() {

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->setDelete();
        $this->custom_curl->createCurl(API_URI . 
            "guest/merchant");

        print_r($this->custom_curl->__tostring());
    }

}