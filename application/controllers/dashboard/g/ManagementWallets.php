<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ManagementWallets extends CI_Controller
{

    // Public Variable
    public $session, $custom_curl, $fileUpload;
    public $meta, $error, $auth;

    public function __construct()
    {
        parent::__construct();

        $this->meta = array(
            "title" => "",
            "description" => "",
            "robots" => ""
        );

        $this->error = array(
            "title" => "Something went wrong",
            "content" => "",
            "details" => array()
        );

        // Load Model
        $this->load->model('Navbar');
        $this->load->model('SidebarGuest');

        // Load Helper
        $this->session = new Session_helper();
        $this->custom_curl = new Mycurl_helper("");
        $this->fileUpload = new Upload_file_helper(
            array(
                "file_type" => array(
                    "png",
                    "jpg",
                    "jpeg"
                ),
                "max_size"  => 200000000
            )
        );

        // Check Auth
        $this->checkAuth();
    }

    // ==========================================================
    // AUTH PROCESS
    // ==========================================================

    private function checkAuth()
    {
        if (!$this->session->check_session("auth")) {
            redirect(base_url("index.php/general/auth/login"));
        } else {
            $this->auth = $this->session->get_session("auth");
            switch ($this->auth->level) {
                case "admin":
                    redirect(base_url("index.php/dashboard/a/dashboard"));
                    break;
                case "toko":
                    redirect(base_url("index.php/dashboard/m/dashboard"));
                    break;
                case "guest":
                    break;
                case "cs":
                    redirect(base_url("index.php/dashboard/cs/dashboard"));
                    break;
            }
        }
    }

    public function index()
    {
        // Check Auth
        // $this->checkAuth();
        // Setup Meta
        $this->meta["title"] = "Guest Leholeh | Dompetku";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";
        
        $sidebar = $this->SidebarGuest->getMenus();
        $sidebar[1]["items"][0]["is_active"] = true;

        // Load Home
        $this->load->view('dashboard-guest/wallet/my-wallet', array(
            "meta" => $this->meta,
            "navbar" => $this->Navbar->getMenus(),
            "sidebar" => $sidebar
        ));
    }

    public function listsTopUp()
    {
        // Check Auth
        // $this->checkAuth();
        // Setup Meta
        $this->meta["title"] = "Guest Leholeh | Isi Ulang";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";
        
        $sidebar = $this->SidebarGuest->getMenus();
        $sidebar[1]["items"][1]["is_active"] = true;

        // Load Home
        $this->load->view('dashboard-guest/wallet/lists-topup', array(
            "meta" => $this->meta,
            "navbar" => $this->Navbar->getMenus(),
            "sidebar" => $sidebar
        ));
    }

    public function listsWithdraw()
    {
        // Check Auth
        // $this->checkAuth();
        // Setup Meta
        $this->meta["title"] = "Guest Leholeh | Penarikan Saldo";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";
        
        $sidebar = $this->SidebarGuest->getMenus();
        $sidebar[1]["items"][2]["is_active"] = true;

        // Load Home
        $this->load->view('dashboard-guest/wallet/lists-withdraw', array(
            "meta" => $this->meta,
            "navbar" => $this->Navbar->getMenus(),
            "sidebar" => $sidebar
        ));
    }

    // ==========================================================
    // BACK-END PROCESS
    // ==========================================================

    // Load Current Balance
    public function current_balance()
    {

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->createCurl(API_URI .
            "guest/wallet/balance");

        print_r($this->custom_curl->__tostring());

    }

    // Load Top Up History
    public function load_top_up()
    {
        $raw = $this->input->post_get("raw") ?: "";
        $page = 0;
        $search = "";
        $order_by = "id";
        $order_direction = "DESC";

        if (!empty($raw)) {
            $raw = json_decode($raw);
            $page = $raw->page;
            $search = $raw->search;
            $order_by = $raw->order_by;
            $order_direction = $raw->order_direction;
        } 

        $search = explode(" ", $search);
        $search = implode("%20", $search);

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->createCurl(API_URI .
            "guest/wallet/top-up/history?page=$page&search=$search&order-by=$order_by&order-direction=$order_direction");

        print_r($this->custom_curl->__tostring());

    }

    // Load Biils
    public function load_bills_top_up()
    {
        $raw = $this->input->post_get("raw") ?: "";
        $page = 0;
        $search = "";
        $order_by = "id";
        $order_direction = "DESC";

        if (!empty($raw)) {
            $raw = json_decode($raw);
            $page = $raw->page;
            $search = $raw->search;
            $order_by = $raw->order_by;
            $order_direction = $raw->order_direction;
        } 

        $search = explode(" ", $search);
        $search = implode("%20", $search);

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->createCurl(API_URI .
            "guest/wallet/top-up/bills?page=$page&search=$search&order-by=$order_by&order-direction=$order_direction");

        print_r($this->custom_curl->__tostring());

    }

    // Request Top Up
    public function request_top_up() {
        $raw = $this->input->post_get("raw", TRUE) ?: "";

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->setPost($raw);
        $this->custom_curl->createCurl(API_URI . "guest/wallet/top-up");

        print_r($this->custom_curl->__tostring()); 
    }

    // Request Top Up
    public function upload_proof() {
        $raw = $this->input->post_get("raw", TRUE) ?: "";

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->setPut($raw);
        $this->custom_curl->createCurl(API_URI . "guest/wallet/top-up/proof");

        print_r($this->custom_curl->__tostring()); 
    }

    // Create Medias
    public function create_medias()
    {
        $photo = $this->fileUpload->do_upload("file-media");
        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token,
            'Content-Type: multipart/form-data'
        ));
        $cfile = curl_file_create($photo["file_location"], $_FILES["file-media"]["type"], $photo['file_name']);

        $this->custom_curl->setPost(
            array(
                'photo' => $cfile,
            )
        );
        $this->custom_curl->createCurl(API_URI . "api/admin/master/Medias/create");

        print_r($this->custom_curl->__tostring());
    }

    // Request Withdraw
    public function request_withdraw() {
        $raw = $this->input->post_get("raw", TRUE) ?: "";

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->setPost($raw);
        $this->custom_curl->createCurl(API_URI . "guest/wallet/withdraw/request");

        print_r($this->custom_curl->__tostring()); 
    }

    // Load Withdraw History
    public function load_withdraw()
    {
        $raw = $this->input->post_get("raw") ?: "";
        $page = 0;
        $search = "";
        $order_by = "id";
        $order_direction = "DESC";

        if (!empty($raw)) {
            $raw = json_decode($raw);
            $page = $raw->page;
            $search = $raw->search;
            $order_by = $raw->order_by;
            $order_direction = $raw->order_direction;
        } 

        $search = explode(" ", $search);
        $search = implode("%20", $search);

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->createCurl(API_URI .
            "guest/wallet/withdraw/history?page=$page&search=$search&order-by=$order_by&order-direction=$order_direction");

        print_r($this->custom_curl->__tostring());

    }

    // Load Biils
    public function load_bills_withdraw()
    {
        $raw = $this->input->post_get("raw") ?: "";
        $page = 0;
        $search = "";
        $order_by = "id";
        $order_direction = "DESC";

        if (!empty($raw)) {
            $raw = json_decode($raw);
            $page = $raw->page;
            $search = $raw->search;
            $order_by = $raw->order_by;
            $order_direction = $raw->order_direction;
        } 

        $search = explode(" ", $search);
        $search = implode("%20", $search);

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->createCurl(API_URI .
            "guest/wallet/withdraw?page=$page&search=$search&order-by=$order_by&order-direction=$order_direction");

        print_r($this->custom_curl->__tostring());

    }

    // Load Biils
    public function detail_withdraw($id)
    {

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->createCurl(API_URI .
            "guest/wallet/withdraw/$id");

        print_r($this->custom_curl->__tostring());

    }

    // Update Payment Token
    public function update_payment_token($id)
    {
        $raw = $this->input->post_get("raw") ?: "";

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->setPut($raw);

        $this->custom_curl->createCurl(API_URI . "guest/wallet/top-up/$id/payment-token");

        print_r($this->custom_curl->__tostring());
    }

    // Check Status Payment Midtrans
    public function check_status_payment($orderId)
    {

        $this->custom_curl->setHeader(array(
            "Authorization: Basic " . base64_encode(API_KEY_MIDTRANS.":"),
            "Content-type: application/json",
            "Accept: application/json"
        ));

        $this->custom_curl->createCurl(API_MIDTRANS_2 . "/v2/$orderId/status");

        print_r($this->custom_curl->__tostring());

    }
}