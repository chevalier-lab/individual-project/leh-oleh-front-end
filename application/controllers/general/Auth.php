<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

    // Public Variable
    public $session, $custom_curl;
    public $meta, $error;

    public function __construct() {
        parent::__construct();

        $this->meta = array(
            "title" => "",
            "description" => "",
            "robots" => ""
        );

        $this->error = array(
            "title" => "Something went wrong",
            "content" => "",
            "details" => array()
        );

        // Load Helper
        $this->session = new Session_helper();
        $this->custom_curl = new Mycurl_helper("");
    }

    // ==========================================================
    // FRONT-END PROCESS
    // ==========================================================

    private function checkAuth() {
        // Check is already authenticate
        if ($this->session->check_session("auth")) {
            $auth = $this->session->get_session("auth");
            $this->route_to_user_type($auth);
        }
    }

    // Registration Page
    public function registration() {
        $this->checkAuth();
        
        // Setup Meta
        $this->meta["title"] = "Leholeh | Daftar";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        // Load Sign in
        $this->load->view('auth/sign-up', array(
            "meta" => $this->meta
        ));
    }

    // Login Page
    public function login() {
        $this->checkAuth();

        // Setup Meta
        $this->meta["title"] = "Leholeh | Masuk";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        // Load Sign in
        $this->load->view('auth/sign-in', array(
            "meta" => $this->meta
        ));
    }

    // Lost Password Page
    public function lost_password() {

    }

    // Reset Password
    public function reset_password() {

    }

    // ==========================================================
    // BACK-END PROCESS
    // ==========================================================

    // Route To Type User Auth
    private function route_to_user_type($auth) {
        switch($auth->level) {
            case "admin":
                redirect(base_url("index.php/dashboard/a/dashboard"));
                break;
            case "toko":
                redirect(base_url("index.php/dashboard/m/dashboard"));
                break;
            case "guest":
                redirect(base_url("index.php/anonymous/landing"));
                break;
            case "cs":
                redirect(base_url("index.php/dashboard/cs/dashboard"));
                break;
        }
    }

    // Set Atuh
    public function set_auth() {
        $raw = $this->input->post_get("raw", TRUE) ?: "";
        if (!empty($raw)) {
            $auth = json_decode($raw);
            $this->session->add_session("auth", $auth);
            $this->route_to_user_type($auth);
        } else {
            echo "Failed to login, something went wrong 
                <a href='".base_url('index.php/general/auth/login')."'>Login</a>";
        }
    }

    // Do Login
    public function do_login() {
        // $this->custom_curl->setHeader(array(
        //     "Authorization: " . $this->auth['token']
        // ));
        $raw = $this->input->post("raw", TRUE) ?: "";
        $this->custom_curl->setPost($raw);
        $this->custom_curl->createCurl(API_URI . "api/auth/User/login");

        // Load Sign in
        print_r($this->custom_curl->__tostring());
    }

    // Do Registration
    public function do_registration() {
        // $this->custom_curl->setHeader(array(
        //     "Authorization: " . $this->auth['token']
        // ));
        $raw = $this->input->post("raw", TRUE) ?: "";
        $this->custom_curl->setPost($raw);
        $this->custom_curl->createCurl(API_URI . "api/auth/User/register");

        // Load Sign in
        print_r($this->custom_curl->__tostring());
    }

    // Do Logout
    public function do_logout() {
        $this->session->remove_session("auth");
        $this->session->destroy_session();
        
        redirect(base_url("index.php/general/auth/login"));
    }
}