<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faq extends CI_Controller {

    // Public Variable
    public $session, $custom_curl, $fileUpload;
    public $meta, $error, $auth;
	
	public function __construct() {
        parent::__construct();

        $this->meta = array(
            "title" => "",
            "description" => "",
            "robots" => ""
        );

        $this->error = array(
            "title" => "Something went wrong",
            "content" => "",
            "details" => array()
        );

        // Load Helper
        $this->session = new Session_helper();
        $this->custom_curl = new Mycurl_helper("");
        $this->fileUpload = new Upload_file_helper(
            array(
                "file_type" => array(
                    "png",
                    "jpg",
                    "jpeg"
                ),
                "max_size"  => 200000000
            )
        );

        // Check Auth
        $this->checkAuth();
	}

    // ==========================================================
    // AUTH PROCESS
    // ==========================================================

    private function checkAuth()
    {
        if ($this->session->check_session("auth")) {
            $this->auth = $this->session->get_session("auth");
        }
    }

	public function index()
	{
		$this->meta["title"] = "Leholeh - F.A.Q";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        // Load chat
        $this->load->view('anonymous/faq/index', array(
            "meta" => $this->meta,
            "auth" => isset($this->auth) ? $this->auth : "",
            "main_menu" => 0
        ));
    }
}