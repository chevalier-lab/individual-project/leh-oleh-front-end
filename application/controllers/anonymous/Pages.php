<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends CI_Controller {

	// Public Variable
    public $session, $custom_curl;
	public $meta, $error, $auth;
	
	public function __construct() {
        parent::__construct();

        $this->meta = array(
            "title" => "",
            "description" => "",
            "robots" => ""
        );

        $this->error = array(
            "title" => "Something went wrong",
            "content" => "",
            "details" => array()
        );

        // Load Helper
        $this->session = new Session_helper();
        $this->custom_curl = new Mycurl_helper("");

        // Check Auth
        $this->checkAuth();
	}

    // ==========================================================
    // AUTH PROCESS
    // ==========================================================

    private function checkAuth() {
		if ($this->session->check_session("auth")) {
            $this->auth = $this->session->get_session("auth");
		}
    }

	public function index()
	{
		$this->meta["title"] = "Leholeh";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        // Load Log Activity
        $this->load->view('anonymous/landing', array(
            "meta" => $this->meta,
            "auth" => isset($this->auth) ? $this->auth : "",
            "main_menu" => 0
        ));
	}

	public function contact()
	{
		$this->meta["title"] = "Leholeh";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        $mainMenu = $this->input->get("main_menu", TRUE) ?: -1;

        // Load Log Activity
        $this->load->view('anonymous/pages/contact-us', array(
            "meta" => $this->meta,
            "auth" => isset($this->auth) ? $this->auth : "",
            "main_menu" => $mainMenu
        ));
    }
    
    public function detail($id) {
		$this->meta["title"] = "Leholeh";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        $mainMenu = $this->input->get("main_menu", TRUE) ?: -1;

        // Load Log Activity
        $this->load->view('anonymous/pages/detail', array(
            "meta" => $this->meta,
            "auth" => isset($this->auth) ? $this->auth : "",
            "main_menu" => $mainMenu,
            "post_id" => $id
        ));
    }

    // ==========================================================
    // BACK-END PROCESS
    // ==========================================================

    // Load Detail Post
    public function load_detail_page($id)
    {
        $this->custom_curl->createCurl(API_URI .
            "guest/general/pages/$id");
        print_r($this->custom_curl->__tostring());
    }

    // Create Feedback
    public function create_feedback()
    {
        $raw = $this->input->post_get("raw") ?: "";

        $this->custom_curl->setPost($raw);
        $this->custom_curl->createCurl(API_URI . "feedback");

        print_r($this->custom_curl->__tostring());
    }
}
