<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

	// Public Variable
    public $session, $custom_curl;
	public $meta, $error, $auth;
	
	public function __construct() {
        parent::__construct();

        $this->meta = array(
            "title" => "",
            "description" => "",
            "robots" => ""
        );

        $this->error = array(
            "title" => "Something went wrong",
            "content" => "",
            "details" => array()
        );

        // Load Helper
		$this->session = new Session_helper();
        $this->custom_curl = new Mycurl_helper("");

        // Check Auth
        $this->checkAuth();
	}

    // ==========================================================
    // AUTH PROCESS
    // ==========================================================

    private function checkAuth() {
		if ($this->session->check_session("auth")) {
            $this->auth = $this->session->get_session("auth");
        } else {
            die(json_encode(array("code" => 401, "message"=> "Unauthorize")));
        }
    }

	public function index()
	{
		$this->meta["title"] = "Leholeh";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        // Load Log Activity
        $this->load->view('anonymous/profile/my-profile', array(
            "meta" => $this->meta
        ));
    }
    
    // Load Profile
    public function load_profile()
    {
        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));
        $this->custom_curl->createCurl(API_URI .
            "admin/setting/profile");

        print_r($this->custom_curl->__tostring());
    }
}
