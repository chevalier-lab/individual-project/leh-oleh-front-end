<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends CI_Controller {

    // Public Variable
    public $session, $custom_curl, $fileUpload;
    public $meta, $error, $auth;
	
	public function __construct() {
        parent::__construct();

        $this->meta = array(
            "title" => "",
            "description" => "",
            "robots" => ""
        );

        $this->error = array(
            "title" => "Something went wrong",
            "content" => "",
            "details" => array()
        );

        // Load Helper
        $this->session = new Session_helper();
        $this->custom_curl = new Mycurl_helper("");
        $this->fileUpload = new Upload_file_helper(
            array(
                "file_type" => array(
                    "png",
                    "jpg",
                    "jpeg"
                ),
                "max_size"  => 200000000
            )
        );

        // Check Auth
        $this->checkAuth();
	}

    // ==========================================================
    // AUTH PROCESS
    // ==========================================================

    private function checkAuth()
    {
        if ($this->session->check_session("auth")) {
            $this->auth = $this->session->get_session("auth");
        } else {
            redirect(base_url('index.php/general/auth/login'));
        }
    }

	public function index()
	{
		$this->meta["title"] = "Leholeh";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        // Load Log Activity
        $this->load->view('anonymous/cart/lists', array(
            "meta" => $this->meta,
            "auth" => isset($this->auth) ? $this->auth : "",
            "main_menu" => 0
        ));
	}

	public function checkout()
	{
		$this->meta["title"] = "Leholeh";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        // Load Log Activity
        $this->load->view('anonymous/cart/checkout', array(
            "meta" => $this->meta,
            "auth" => isset($this->auth) ? $this->auth : "",
            "main_menu" => 0
        ));
	}

    // ==========================================================
    // BACK-END PROCESS
    // ==========================================================


    // Load Cart Active
    public function load_cart_active()
    {

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->createCurl(API_URI .
            "guest/cart");

        print_r($this->custom_curl->__tostring());

    }

    // Add To Cart
    public function add_to_cart() {
        $raw = $this->input->post_get("raw", TRUE) ?: "";

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->setPost($raw);
        $this->custom_curl->createCurl(API_URI . "guest/cart/pesan");

        print_r($this->custom_curl->__tostring()); 
    }

    // Edit Change QTY
    public function change_qty($id)
    {
        $raw = $this->input->post_get("raw") ?: "";

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token,
        ));

        $this->custom_curl->setPut($raw);

        $this->custom_curl->createCurl(API_URI . "guest/cart/product/$id");

        print_r($this->custom_curl->__tostring());
    }

    // DELETE CART
    public function delete_cart($id)
    {

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token,
        ));

        $this->custom_curl->setDelete();

        $this->custom_curl->createCurl(API_URI . "guest/cart/$id");

        print_r($this->custom_curl->__tostring());
    }

    // Add To Cart
    public function do_checkout() {
        $raw = $this->input->post_get("raw", TRUE) ?: "";

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->setPost($raw);
        $this->custom_curl->createCurl(API_URI . "guest/cart/checkout");

        print_r($this->custom_curl->__tostring()); 
    }

    // Load Location Active Location User
    public function load_current_location_user()
    {

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->createCurl(API_URI .
            "guest/location/current");

        print_r($this->custom_curl->__tostring());

    }

    public function update_current_location_user($id)
    {

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->setPut("");
        $this->custom_curl->createCurl(API_URI .
            "guest/location/actived/$id");

        print_r($this->custom_curl->__tostring());

    }

    // Load All Location User
    public function load_location_user()
    {

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->createCurl(API_URI .
            "guest/location");

        print_r($this->custom_curl->__tostring());

    }

    // Create New Location Uer
    public function add_new_location()
    {
        $raw = $this->input->post_get("raw") ?: "";

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token,
        ));

        $this->custom_curl->setPost($raw);

        $this->custom_curl->createCurl(API_URI . "guest/location");

        print_r($this->custom_curl->__tostring());
    }

    // Create Payment Link
    public function create_payment_link()
    {
        $raw = $this->input->post_get("raw") ?: "";

        $this->custom_curl->setHeader(array(
            "Authorization: Basic " . base64_encode(API_KEY_MIDTRANS.":"),
            "Content-type: application/json",
            "Accept: application/json"
        ));

        $this->custom_curl->setPost($raw);

        $this->custom_curl->createCurl(API_MIDTRANS . "/snap/v1/transactions");

        print_r($this->custom_curl->__tostring());
    }

    // Update Payment Token
    public function update_payment_token($id)
    {
        $raw = $this->input->post_get("raw") ?: "";

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->setPut($raw);

        $this->custom_curl->createCurl(API_URI . "guest/order/$id/payment-token");

        print_r($this->custom_curl->__tostring());
    }
    
    // Create Order
    public function create_order()
    {
        $raw = $this->input->post_get("raw") ?: "";

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token,
        ));

        $this->custom_curl->setPost($raw);

        $this->custom_curl->createCurl(API_URI . "merchant/order-transaction");

        print_r($this->custom_curl->__tostring());
    }
}
