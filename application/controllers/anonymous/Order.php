<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller {

    // Public Variable
    public $session, $custom_curl, $fileUpload;
    public $meta, $error, $auth;
	
	public function __construct() {
        parent::__construct();

        $this->meta = array(
            "title" => "",
            "description" => "",
            "robots" => ""
        );

        $this->error = array(
            "title" => "Something went wrong",
            "content" => "",
            "details" => array()
        );

        // Load Helper
        $this->session = new Session_helper();
        $this->custom_curl = new Mycurl_helper("");
        $this->fileUpload = new Upload_file_helper(
            array(
                "file_type" => array(
                    "png",
                    "jpg",
                    "jpeg"
                ),
                "max_size"  => 200000000
            )
        );

        // Check Auth
        $this->checkAuth();
	}

    // ==========================================================
    // AUTH PROCESS
    // ==========================================================

    private function checkAuth()
    {
        if ($this->session->check_session("auth")) {
            $this->auth = $this->session->get_session("auth");
        } else {
            redirect(base_url('index.php/general/auth/login'));
        }
    }

	public function index()
	{
		$this->meta["title"] = "Leholeh";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        // Load Log Activity
        $this->load->view('anonymous/order/lists', array(
            "meta" => $this->meta,
            "auth" => isset($this->auth) ? $this->auth : "",
            "main_menu" => 0
        ));
	}

	public function detail($id)
	{
		$this->meta["title"] = "Leholeh";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        // Load Log Activity
        $this->load->view('anonymous/order/detail', array(
            "meta" => $this->meta,
            "auth" => isset($this->auth) ? $this->auth : "",
            "order_id" => $id,
            "main_menu" => 0
        ));
	}

    // ==========================================================
    // BACK-END PROCESS
    // ==========================================================

    // Load Order Active
    public function load_order_active()
    {
        $raw = $this->input->post_get("raw") ?: "";
        $page = 0;
        $search = "";
        $order_by = "id";
        $order_direction = "DESC";

        if (!empty($raw)) {
            $raw = json_decode($raw);
            $page = $raw->page;
            $search = $raw->search;
            $order_by = $raw->order_by;
            $order_direction = $raw->order_direction;
        } 

        $search = explode(" ", $search);
        $search = implode("%20", $search);

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->createCurl(API_URI .
            "guest/order?page=$page&search=$search&order-by=$order_by&order-direction=$order_direction");

        print_r($this->custom_curl->__tostring());

    }

    // Load Order History
    public function load_order_history()
    {
        $raw = $this->input->post_get("raw") ?: "";
        $page = 0;
        $search = "";
        $order_by = "id";
        $order_direction = "DESC";

        if (!empty($raw)) {
            $raw = json_decode($raw);
            $page = $raw->page;
            $search = $raw->search;
            $order_by = $raw->order_by;
            $order_direction = $raw->order_direction;
        } 

        $search = explode(" ", $search);
        $search = implode("%20", $search);

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->createCurl(API_URI .
            "guest/order/history?page=$page&search=$search&order-by=$order_by&order-direction=$order_direction");

        print_r($this->custom_curl->__tostring());

    }

    // Detail Order
    public function detail_order($id)
    {
        
        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->createCurl(API_URI .
            "guest/order/$id");

        print_r($this->custom_curl->__tostring());

    }

    // Review Product
    public function review_product() {
        $raw = $this->input->post_get("raw", TRUE) ?: "";

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->setPost($raw);
        $this->custom_curl->createCurl(API_URI . "guest/order/review");

        print_r($this->custom_curl->__tostring()); 
    }

    // Complain Product
    public function complain_product() {
        $raw = $this->input->post_get("raw", TRUE) ?: "";

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->setPost($raw);
        $this->custom_curl->createCurl(API_URI . "guest/order/complain");

        print_r($this->custom_curl->__tostring()); 
    }

    // Create Payment Link
    public function get_status_payment($token)
    {
        $this->custom_curl->setHeader(array(
            "Authorization: Basic " . base64_encode(API_KEY_MIDTRANS.":"),
            "Content-type: application/json",
            "Accept: application/json"
        ));

        $this->custom_curl->createCurl(API_MIDTRANS_2 . "/v2/$token/status");

        print_r($this->custom_curl->__tostring());
    }

    // Update Status Transaction
    public function update_status_transaction($id) {
        $raw = $this->input->post_get("raw") ?: "";

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token
        ));

        $this->custom_curl->setPut($raw);

        $this->custom_curl->createCurl(API_URI . 
            "merchant/manage/transaction/$id");

        print_r($this->custom_curl->__tostring());
    }

    // Request Refund
    public function request_refund()
    {
        $raw = $this->input->post_get("raw") ?: "";

        $this->custom_curl->setHeader(array(
            "Authorization: " . $this->auth->token,
        ));

        $this->custom_curl->setPost($raw);
        $this->custom_curl->createCurl(API_URI . "/guest/order/refund");

        print_r($this->custom_curl->__tostring());
    }
}
