<?php
defined('BASEPATH') or exit('No direct script access allowed');

class RegistrationMerchant extends CI_Controller
{

    // Public Variable
    public $session, $custom_curl;
    public $meta, $error;

    public function __construct()
    {
        parent::__construct();

        $this->meta = array(
            "title" => "",
            "description" => "",
            "robots" => ""
        );

        $this->error = array(
            "title" => "Something went wrong",
            "content" => "",
            "details" => array()
        );

        // Load Helper
        $this->session = new Session_helper();
        $this->custom_curl = new Mycurl_helper("");
        $this->fileUpload = new Upload_file_helper(
            array(
                "file_type" => array(
                    "png",
                    "jpg",
                    "jpeg"
                ),
                "max_size"  => 200000000
            )
        );
    }

    // ==========================================================
    // FRONT-END PROCESS
    // ==========================================================

    private function checkAuth()
    {
        // Check is already authenticate
        if ($this->session->check_session("auth")) {
            $auth = $this->session->get_session("auth");
            $this->route_to_user_type($auth);
        }
    }

    // Registration Page
    public function index()
    {
        $this->checkAuth();

        // Setup Meta
        $this->meta["title"] = "Leholeh | Daftar Toko";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        // Load Sign in
        $this->load->view('auth/sign-up-merchant', array(
            "meta" => $this->meta
        ));
    }

    // ==========================================================
    // BACK-END PROCESS
    // ==========================================================

    // Route To Type User Auth
    private function route_to_user_type($auth)
    {
        switch ($auth->level) {
            case "admin":
                redirect(base_url("index.php/dashboard/a/dashboard"));
                break;
            case "toko":
                redirect(base_url("index.php/dashboard/m/dashboard"));
                break;
            case "guest":
                redirect(base_url("index.php/anonymous/landing"));
                break;
        }
    }

    // Set Atuh
    public function set_auth()
    {
        $raw = $this->input->post_get("raw", TRUE) ?: "";
        if (!empty($raw)) {
            $auth = json_decode($raw);
            $this->session->add_session("auth", $auth);
            $this->route_to_user_type($auth);
        } else {
            echo "Failed to login, something went wrong 
                <a href='" . base_url('index.php/general/auth/login') . "'>Login</a>";
        }
    }

    // Do Registration
    public function do_registration()
    {

        $full_name = $this->input->post("full_name", TRUE) ?: "";
        $username = $this->input->post("username", TRUE) ?: "";
        $password = $this->input->post("password", TRUE) ?: "";
        $gender =$this->input->post("gender", TRUE) ?: "";
        $birth_date = $this->input->post("birth_date", TRUE) ?: "";
        $address = $this->input->post("address", TRUE) ?: "";
        $no_identity = $this->input->post("no_identity", TRUE) ?: "";
        $market_address = $this->input->post("market_address", TRUE) ?: "";
        $market_name = $this->input->post("market_name", TRUE) ?: "";
        $market_phone_number = $this->input->post("market_phone_number", TRUE) ?: "";
        $foto_pengguna = $this->fileUpload->do_upload("foto_pengguna");
        $foto_toko = $this->fileUpload->do_upload("foto_toko");

        $this->custom_curl->setHeader(array(
            'Content-Type: multipart/form-data'
        ));

        $cPhotoUser = curl_file_create($foto_pengguna["file_location"], $_FILES["foto_pengguna"]["type"], $foto_pengguna['file_name']);
        $cPhotoMarket = curl_file_create($foto_toko["file_location"], $_FILES["foto_toko"]["type"], $foto_toko['file_name']);

        $this->custom_curl->setPost(
            array(
                'full_name' => $full_name,
                'username' => $username,
                'password' => $password,
                'gender' => $gender,
                'birth_date' => $birth_date,
                'address' => $address,
                'no_identity' => $no_identity,
                'market_address' => $market_address,
                'market_name' => $market_name,
                'market_phone_number' => $market_phone_number,
                'foto_pengguna' => $cPhotoUser,
                'foto_toko' => $cPhotoMarket,
            )
        );

        $this->custom_curl->createCurl(API_URI . "auth/register/merchant");

        // Load Sign in
        print_r($this->custom_curl->__tostring());
    }
}
