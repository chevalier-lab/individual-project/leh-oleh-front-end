<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Landing extends CI_Controller
{

    // Public Variable
    public $session, $custom_curl, $fileUpload;
    public $meta, $error, $auth;
    public function __construct()
    {
        parent::__construct();

        $this->meta = array(
            "title" => "",
            "description" => "",
            "robots" => ""
        );

        $this->error = array(
            "title" => "Something went wrong",
            "content" => "",
            "details" => array()
        );

        // Load Helper
        $this->session = new Session_helper();
        $this->custom_curl = new Mycurl_helper("");
        $this->fileUpload = new Upload_file_helper(
            array(
                "file_type" => array(
                    "png",
                    "jpg",
                    "jpeg"
                ),
                "max_size"  => 200000000
            )
        );

        // Check Auth
        $this->checkAuth();
    }

    // ==========================================================
    // AUTH PROCESS
    // ==========================================================

    private function checkAuth()
    {
        if ($this->session->check_session("auth")) {
            $this->auth = $this->session->get_session("auth");
        }
    }

    public function index()
    {
        $this->meta["title"] = "Leholeh";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        // Load Log Activity
        $this->load->view('anonymous/landing', array(
            "meta" => $this->meta,
            "auth" => isset($this->auth) ? $this->auth : "",
            "main_menu" => 0
        ));
    }

    public function detailMerchant($id)
    {
        $this->meta["title"] = "Leholeh | Detail Toko";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        // Load Log Activity
        $this->load->view('anonymous/merchant/detail', array(
            "meta" => $this->meta,
            "auth" => isset($this->auth) ? $this->auth : "",
            "main_menu" => 0,
            "id_merchant" => $id
        ));
    }

    public function joinWithUs()
    {
        $this->meta["title"] = "Leholeh | Bergabung dengan kami";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        // Load Log Activity
        $this->load->view('anonymous/pages/merchant-ad', array(
            "meta" => $this->meta,
            "auth" => isset($this->auth) ? $this->auth : "",
            "main_menu" => 0,
        ));
    }

    // ==========================================================
    // BACK-END PROCESS
    // ==========================================================

    // Load To List Product Using Category
    public function set_category_product_list($id)
    {
        $this->session->add_session("category", $id);
        redirect(base_url('index.php/anonymous/product'));
    }

    // Load To List Product Using Searching
    public function set_search_product_list() {
        $search = $this->input->post_get("search") ?: "";
        $this->session->add_session("search", $search);
        redirect(base_url('index.php/anonymous/product'));
    }


    // Load New Products
    public function load_new_products()
    {
        $raw = $this->input->post_get("raw") ?: "";
        $page = 0;
        $search = "";
        $order_by = "id";
        $order_direction = "DESC";

        if (!empty($raw)) {
            $raw = json_decode($raw);
            $page = $raw->page;
            $search = $raw->search;
            $order_by = $raw->order_by;
            $order_direction = $raw->order_direction;
        }

        $search = explode(" ", $search);
        $search = implode("%20", $search);

        $this->custom_curl->createCurl(API_URI .
            "guest/products?page=$page&search=$search&order-by=$order_by&order-direction=$order_direction");

        print_r($this->custom_curl->__tostring());
    }

    // Load Main
    public function load_main_menu()
    {
        $this->custom_curl->createCurl(API_URI .
            "guest/general/main-menu");

        print_r($this->custom_curl->__tostring());
    }

    // Load Secondary
    public function load_secondary_menu()
    {
        $this->custom_curl->createCurl(API_URI .
            "guest/general/secondary-menu");

        print_r($this->custom_curl->__tostring());
    }

    // Load Categories
    public function load_categories()
    {
        $this->custom_curl->createCurl(API_URI .
            "guest/general/categories");

        print_r($this->custom_curl->__tostring());
    }

    // Load Banner
    public function load_banner()
    {
        $this->custom_curl->createCurl(API_URI .
            "guest/general/banner");

        print_r($this->custom_curl->__tostring());
    }

    // Load Product By Location
    public function load_product_location()
    {
        $limit = 4;
        $raw = $this->input->post_get("raw") ?: "";

        if (!empty($raw)) {
            $raw = json_decode($raw);
            $limit = $raw->limit;
        }

        $this->custom_curl->createCurl(API_URI .
            "guest/products/location?limit=$limit");
            
        print_r($this->custom_curl->__tostring());
        
    }
    
    // Load Locations
    public function load_locations()
    {   
        $this->custom_curl->createCurl(API_URI .
            "guest/general/locations");

        print_r($this->custom_curl->__tostring());
    }

    // Load Merchant
    public function load_merchants() {
        $this->custom_curl->createCurl(API_URI .
            "guest/merchant");

        print_r($this->custom_curl->__tostring());
    }

    // Load Random
    public function load_merchant_random() {
        $this->custom_curl->createCurl(API_URI .
            "guest/merchant/random");

        print_r($this->custom_curl->__tostring());
    }

    // Detail Merchant
    public function detail_merchants($id) {
        $this->custom_curl->createCurl(API_URI .
            "guest/merchant/$id");

        print_r($this->custom_curl->__tostring());
    }

    // Detail Merchant
    public function load_product_merchant($id) {

        $raw = $this->input->post_get("raw") ?: "";
        $page = 0;
        $search = "";
        $order_by = "id";
        $order_direction = "DESC";

        if (!empty($raw)) {
            $raw = json_decode($raw);
            $page = $raw->page;
            $search = $raw->search;
            $order_by = $raw->order_by;
            $order_direction = $raw->order_direction;
        }

        $search = explode(" ", $search);
        $search = implode("%20", $search);

        $this->custom_curl->createCurl(API_URI .
            "guest/merchant/$id/products?page=$page&search=$search&order-by=$order_by&order-direction=$order_direction");

        print_r($this->custom_curl->__tostring());
    }
}
