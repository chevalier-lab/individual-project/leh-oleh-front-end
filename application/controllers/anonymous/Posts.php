<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Posts extends CI_Controller {

	// Public Variable
    public $session, $custom_curl;
	public $meta, $error, $auth;
	
	public function __construct() {
        parent::__construct();

        $this->meta = array(
            "title" => "",
            "description" => "",
            "robots" => ""
        );

        $this->error = array(
            "title" => "Something went wrong",
            "content" => "",
            "details" => array()
        );

        // Load Helper
        $this->session = new Session_helper();
        $this->custom_curl = new Mycurl_helper("");

        // Check Auth
        $this->checkAuth();
	}

    // ==========================================================
    // AUTH PROCESS
    // ==========================================================

    private function checkAuth() {
		if ($this->session->check_session("auth")) {
            $this->auth = $this->session->get_session("auth");
		}
    }

	public function index()
	{
		$this->meta["title"] = "Leholeh";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        $mainMenu = $this->input->get("main_menu", TRUE) ?: -1;

        // Load Log Activity
        $this->load->view('anonymous/posts/lists', array(
            "meta" => $this->meta,
            "auth" => isset($this->auth) ? $this->auth : "",
            "main_menu" => $mainMenu
        ));
    }

	public function detail($id)
	{
		$this->meta["title"] = "Leholeh";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        $mainMenu = $this->input->get("main_menu", TRUE) ?: -1;

        // Load Log Activity
        $this->load->view('anonymous/posts/detail', array(
            "meta" => $this->meta,
            "post_id" => $id,
            "auth" => isset($this->auth) ? $this->auth : "",
            "main_menu" => $mainMenu
        ));
    }
    
    // ==========================================================
    // BACK-END PROCESS
    // ==========================================================

    // Load Posts
    public function load_posts()
    {
        $raw = $this->input->post_get("raw") ?: "";
        $page = 0;
        $search = "";
        $order_by = "id";
        $order_direction = "DESC";

        if (!empty($raw)) {
            $raw = json_decode($raw);
            $page = $raw->page;
            $search = $raw->search;
            $order_by = $raw->order_by;
            $order_direction = $raw->order_direction;
        }

        $search = explode(" ", $search);
        $search = implode("%20", $search);

        $this->custom_curl->createCurl(API_URI .
            "guest/general/posts?page=$page&order-by=$order_by&order-direction=$order_direction&search=$search");

        print_r($this->custom_curl->__tostring());
    }

    // Load Detail Post
    public function load_detail_post($id)
    {
        $this->custom_curl->createCurl(API_URI .
            "guest/general/posts/$id");
        print_r($this->custom_curl->__tostring());
    }
}