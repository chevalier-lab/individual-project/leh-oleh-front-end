<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

    // Public Variable
    public $session, $custom_curl, $fileUpload;
    public $meta, $error, $auth;
	
	public function __construct() {
        parent::__construct();

        $this->meta = array(
            "title" => "",
            "description" => "",
            "robots" => ""
        );

        $this->error = array(
            "title" => "Something went wrong",
            "content" => "",
            "details" => array()
        );

        // Load Helper
        $this->session = new Session_helper();
        $this->custom_curl = new Mycurl_helper("");
        $this->fileUpload = new Upload_file_helper(
            array(
                "file_type" => array(
                    "png",
                    "jpg",
                    "jpeg"
                ),
                "max_size"  => 200000000
            )
        );

        // Check Auth
        $this->checkAuth();
	}

    // ==========================================================
    // AUTH PROCESS
    // ==========================================================

    private function checkAuth() {
		if ($this->session->check_session("auth")) {
			$this->auth = $this->session->get_session("auth");
		}
    }

	public function index()
	{
		$this->meta["title"] = "Leholeh";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        $category = "";
        $search = "";

        if ($this->session->check_session("category")) {
			$category = $this->session->get_session("category");
        }
        
        if ($this->session->check_session("search")) {
			$search = $this->session->get_session("search");
        }

        // Load Log Activity
        $this->load->view('anonymous/product/lists', array(
            "meta" => $this->meta,
            "auth" => isset($this->auth) ? $this->auth : "",
            "category" => $category,
            "search" => $search,
            "main_menu" => -1
        ));
    }
    
    public function detail($id=0)
	{
		$this->meta["title"] = "Leholeh";
        $this->meta["description"] = "";
        $this->meta["robots"] = "";

        // Load Log Activity
        $this->load->view('anonymous/product/detail', array(
            "meta" => $this->meta,
            "id_product" => $id,
            "auth" => isset($this->auth) ? $this->auth : "",
            "main_menu" => -1
        ));
    }

    // ==========================================================
    // BACK-END PROCESS
    // ==========================================================

    // Load Detail Product
    public function load_detail_product($id)
    {   
        $this->custom_curl->createCurl(API_URI .
            "guest/products/$id");

        print_r($this->custom_curl->__tostring());
    }

    // Load Filter Product
    public function load_products() {
        $location = $this->input->post_get("location") ?: "";
        $category = $this->input->post_get("category") ?: "";
        $page = $this->input->post_get("page") ?: 0;
        $search = $this->input->post_get("search") ?: "";
        $order_by = $this->input->post_get("order_by") ?: "id";
        $order_direction = $this->input->post_get("order_direction") ?: "DESC";

        $arrayCategoryLocation = array();

        if ($location != "") { 
            $location = json_decode($location); 
            if (count($location) > 0) {
                $arrayCategoryLocation["location"] = $location;
            }
        }
        if ($category != "") {
            $category = json_decode($category); 
            if (count($category) > 0) {
                $arrayCategoryLocation["category"] = $category;
            }
        }

        $this->custom_curl->setPost(json_encode($arrayCategoryLocation));
        
        $this->custom_curl->createCurl(API_URI .
            "guest/products/filter?page=$page&search=$search&order-by=$order_by&order-direction=$order_direction");

        print_r($this->custom_curl->__tostring());
    }
}
